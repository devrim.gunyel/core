package lcsb.mapviewer.services;

public class AuthenticationTokenExpireException extends SecurityException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public AuthenticationTokenExpireException(String message) {
    super(message);
  }

}
