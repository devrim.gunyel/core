package lcsb.mapviewer.services.interfaces;

import java.awt.geom.Point2D;
import java.util.List;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.user.User;

/**
 * Service responsible for comments functionality.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ICommentService {
  /**
   * Adds comment to the map.
   * 
   * @param pinned
   *          parameter that defines if comment should be visible on the map.
   * @param object
   *          determines which object is commented - it could be null; in this
   *          case it means that a comment is general one.
   * @param name
   *          name of person that comments
   * @param email
   *          email of the person that comments
   * @param content
   *          content of the comment
   * @param submodel
   *          on which submodel we comment on
   * @param coordinates
   *          where exactly the comment should be placed on
   * 
   * @return comment object created based on the data provided as parameters
   */
  Comment addComment(String name, String email, String content, Point2D coordinates, Object object,
      boolean pinned, Model submodel, User owner);

  /**
   * This method remove comment. Comment is not removed from the system, only
   * 'deleted' flag is set.
   * 
   * @param loggedUser
   *          user that wants to remove the comment
   * @param commentId
   *          identifier of the comment that user wants to remove
   * @param reason
   *          why user wants to remove the comment
   */
  void deleteComment(User loggedUser, String commentId, String reason);

  void deleteComment(Comment comment, String reason);

  List<Comment> getCommentsByProject(Project project);

  /**
   * Returns number of comments in the system.
   * 
   * @return number of comments in the system
   */
  long getCommentCount();

  /**
   * Removes comments from the model.
   * 
   * @param model
   *          from which model we want to remove comments
   */
  void removeCommentsForModel(Model model);

  /**
   * Removes comments from the model.
   * 
   * @param model
   *          from which model we want to remove comments
   */
  void removeCommentsForModel(ModelData model);

  Comment getCommentById(String commentId);
  
  User getOwnerByCommentId(String commentId);

}
