package lcsb.mapviewer.services.interfaces;

import java.util.List;

import lcsb.mapviewer.common.FrameworkVersion;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;

/**
 * Service used for accessing and modifying configuration parameters.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IConfigurationService {

  /**
   * Returns value of the configuration parameter.
   * 
   * @param type
   *          configuration parameter
   * @return value of the configuration parameter given in type
   */
  String getConfigurationValue(ConfigurationElementType type);

  /**
   * Sets configuration parameter.
   * 
   * @param type
   *          which configuration parameter
   * @param value
   *          what value
   */
  void setConfigurationValue(ConfigurationElementType type, String value);

  /**
   * Removes configuration parameter.
   * 
   * @param type
   *          which configuration parameter
   */
  void deleteConfigurationValue(ConfigurationElementType type);

  /**
   * Returns information about all configuration parameters with current values.
   * 
   * @return list of all configuration parameters with current values
   */
  List<ConfigurationOption> getAllValues();

  /**
   * Updates configuration parameters.
   * 
   * @param values
   *          new values of the parameters
   */
  void updateConfiguration(List<ConfigurationOption> values);

  /**
   * Returns {@link lcsb.mapviewer.common.Configuration#systemVersion git version}
   * from which the system was built.
   * 
   * @param baseDir
   *          directory where the system is placed
   * @return git version
   */
  String getSystemSvnVersion(String baseDir);

  /**
   * Returns estimated current memory usage (in MB).
   * 
   * @return estimated current memory usage (in MB)
   */
  Long getMemoryUsage();

  /**
   * Returns max memory available to JVM (in MB).
   * 
   * @return max memory available to JVM (in MB)
   */
  Long getMaxMemory();

  /**
   * Returns currently build framework version.
   * 
   * @param baseDir
   *          directory where the system is placed
   * @return currently build {@link FrameworkVersion framework version}
   * @throws InvalidSvnVersionException
   *           thrown when the git version of the build system is invalid
   */
  FrameworkVersion getSystemVersion(String baseDir);

  /**
   * Returns system {@link lcsb.mapviewer.common.Configuration#systemBuild build
   * date}.
   * 
   * @param baseDir
   *          directory where the system is placed
   * @return system build date
   */
  String getSystemBuild(String baseDir);

  /**
   * Returns {@link lcsb.mapviewer.common.Configuration#systemBuildVersion git
   * hash} of build process.
   * 
   * @param baseDir
   *          directory where the system is placed
   * @return git hash
   */
  String getSystemGitVersion(String baseDir);

  ConfigurationOption getValue(ConfigurationElementType type);

  ConfigurationOption getValue(PrivilegeType type);
}
