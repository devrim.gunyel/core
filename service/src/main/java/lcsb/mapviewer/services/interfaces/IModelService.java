package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Service that manages models (maps).
 * 
 * @author Piotr Gawron
 * 
 */
public interface IModelService {

  /**
   * Returns the newest model for the project identified by the
   * {@link lcsb.mapviewer.model.Project#projectId project identifier}.
   * 
   * @param projectId
   *          {@link lcsb.mapviewer.model.Project#projectId project identifier}
   * @return the newest model for the project
   */
  Model getLastModelByProjectId(String projectId);

  /**
   * Caches information about all pubmed articles for the given model.
   * 
   * @param model
   *          model in which we are caching the data
   * @param updater
   *          callback interface that update progress information
   */
  void cacheAllPubmedIds(Model model, IProgressUpdater updater);

  /**
   * Removes model from application cache.
   * 
   * @param model
   *          model to remove
   */
  void removeModelFromCache(Model model);

  /**
   * Caches information about all miriam resources in the model.
   * 
   * @param model
   *          model in which we are caching the data
   * @param updater
   *          callback interface that update progress information
   */
  void cacheAllMiriamLinks(Model model, IProgressUpdater updater);

  /**
   * Removes model from application cache.
   * 
   * @param model
   *          model to remove
   */
  void removeModelFromCache(ModelData model);

  /**
   * Removes model from application cache.
   * 
   * @param projectId
   *          identifier of the project
   */
  void removeModelFromCacheByProjectId(String projectId);

  void updateModel(ModelData model);
}
