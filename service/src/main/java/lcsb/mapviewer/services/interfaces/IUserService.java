package lcsb.mapviewer.services.interfaces;

import java.util.*;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;

public interface IUserService {

  void addUser(User user);

  void updateUser(User user);

  void deleteUser(User user);

  User getUserByLogin(String login);

  List<User> getUsers();

  void grantUserPrivilege(User user, PrivilegeType type);

  void grantUserPrivilege(User user, PrivilegeType type, String objectId);

  void revokeUserPrivilege(User user, PrivilegeType type);

  void revokeUserPrivilege(User user, PrivilegeType type, String objectId);

  void grantDefaultPrivileges(User user);

  void grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType type, String objectId);

  /**
   * When an object is deleted we have to manually remove the access rights to it
   * for every user to avoid pollution. Hibernate delete cascading does not work
   * here, because security is completely decoupled from the business logic.
   * 
   * @param privilegeType
   *          The concerned privilege domain.
   * @param objectId
   *          The objectId for which to remove all access rights.
   *
   */
  void revokeObjectDomainPrivilegesForAllUsers(PrivilegeType privilegeType, String objectId);

  /**
   * Returns {@link ColorExtractor} that transform overlay values into colors for
   * given user.
   *
   * @param user
   *          {@link User} for which {@link ColorExtractor} will be obtained
   * @return {@link ColorExtractor} that transform overlay values into colors for
   *         given user
   */
  ColorExtractor getColorExtractorForUser(User user);

  Map<String, Boolean> ldapAccountExistsForLogin(Collection<User> logins);
}