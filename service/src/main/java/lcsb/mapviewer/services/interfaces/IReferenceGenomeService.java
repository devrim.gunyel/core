package lcsb.mapviewer.services.interfaces;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.*;

/**
 * Service used to maintain reference genome data.
 * 
 * @author Piotr Gawron
 *
 */
public interface IReferenceGenomeService {

  /**
   * Adds reference genome to the system.
   * 
   * @param type
   *          {@link ReferenceGenomeType type} of reference genome
   * @param organism
   *          organism for which reference genome is added
   * @param version
   *          version of the reference genome
   * @param customUrl
   *          url from which reference genome should be downloaded
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws ReferenceGenomeConnectorException
   *           thrown when reference genome already exists or there is problem
   *           with adding genome
   */
  void addReferenceGenome(ReferenceGenomeType type, MiriamData organism, String version, String customUrl)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException;

  /**
   * Returns list of organisms available for {@link ReferenceGenomeType reference
   * genome type}.
   * 
   * @param type
   *          type of reference genome
   * @return list of available organisms
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with accessing information about
   *           reference genomes
   */
  List<MiriamData> getOrganismsByReferenceGenomeType(ReferenceGenomeType type) throws ReferenceGenomeConnectorException;

  /**
   * Returns list of genome versions available for organism of specific
   * {@link ReferenceGenomeType reference genome type}.
   * 
   * @param type
   *          type of reference genome
   * @param organism
   *          organism for which we check genome versions
   * @return list of available organisms
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with accessing information about
   *           reference genomes
   */
  List<String> getAvailableGenomeVersions(ReferenceGenomeType type, MiriamData organism)
      throws ReferenceGenomeConnectorException;

  /**
   * Returns url to the file that describes genome.
   * 
   * @param type
   *          type of reference genome
   * @param organism
   *          organism of reference genome
   * @param version
   *          version of reference genome
   * @return url to the file that describes genome
   */
  String getUrlForGenomeVersion(ReferenceGenomeType type, MiriamData organism, String version);

  /**
   * Returns list of all downloaded reference genomes.
   * 
   * @return list of all downloaded reference genomes
   */
  List<ReferenceGenome> getDownloadedGenomes();

  /**
   * Removes reference genome from the system.
   * 
   * @param genome
   *          genome to be removed
   * @throws IOException
   *           thrown when there is a problem with removing genome
   */
  void removeGenome(ReferenceGenome genome) throws IOException;

  /**
   * Adds gene mapping to reference genome.
   * 
   * @param referenceGenome
   *          reference genome where we add mapping
   * @param name
   *          name of the mapping
   * @param url
   *          url to the file that describes mapping
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with manipulating information about
   *           reference genome
   */
  void addReferenceGenomeGeneMapping(ReferenceGenome referenceGenome, String name, String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException;

  /**
   * Removes gene mapping for reference genome.
   * 
   * @param mapping
   *          mapping to be removed
   * @throws IOException
   *           thrown when there is a problem with removing file
   */
  void removeReferenceGenomeGeneMapping(ReferenceGenomeGeneMapping mapping) throws IOException;

  /**
   * Returns {@link ReferenceGenome} for specific reference genome.
   * 
   * @param organism
   *          organism of reference genome
   * @param genomeType
   *          reference genome type
   * @param version
   *          version of the reference genome
   * @return {@link ReferenceGenome} for specific reference genome
   */
  ReferenceGenome getReferenceGenomeViewByParams(MiriamData organism, ReferenceGenomeType genomeType, String version);

  ReferenceGenome getReferenceGenomeById(int id);
}
