package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;

public interface IPrivilegeService {

  Privilege getPrivilege(PrivilegeType type);

  Privilege getPrivilege(PrivilegeType type, String objectId);

  void updatePrivilege(Privilege privilege);

}
