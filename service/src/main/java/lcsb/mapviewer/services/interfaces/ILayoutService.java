package lcsb.mapviewer.services.interfaces;

import java.io.*;
import java.util.List;
import java.util.Map;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.utils.EmailSender;

/**
 * Service that manages layouts of the map.
 *
 * @author Piotr Gawron
 *
 */
public interface ILayoutService {

  /**
   * Removes layout from the system.
   *
   * @param layout
   *          layout to remove
   * @param homeDir
   *          directory where the system is deployed
   * @throws IOException
   *           thrown when there are problems with removing layout files
   */
  void removeLayout(Layout layout, String homeDir) throws IOException;

  /**
   * Updates data about the layout.
   *
   * @param layout
   *          layout to update
   */
  void updateLayout(Layout layout);

  /**
   * Create layout based on the data in the parameter.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} necessary to create layout
   * @return object that refers to created layout
   * @throws IOException
   *           thrown when there are problems with files
   * @throws InvalidColorSchemaException
   *           if the coloring source is invalid
   */
  Layout createLayout(CreateLayoutParams params) throws IOException, InvalidColorSchemaException;

  /**
   * Create layout based on the data in the parameter. Layout will contain set of
   * images that can be further visualized in google maps API/OpenLayers library.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} necessary to create layout
   * @return object that refers to created layout
   * @throws IOException
   *           thrown when there are problems with files
   * @throws CommandExecutionException
   *           if the coloring source is invalid
   * @throws InvalidColorSchemaException
   *           if the coloring source is invalid
   */
  Layout createLayoutWithImages(CreateLayoutParams params)
      throws IOException, CommandExecutionException, InvalidColorSchemaException;

  /**
   * Returns a list of pairs {@link Element} - {@link ColorSchema} that are
   * visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Element} - {@link ColorSchema} that are
   *         visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}
   */
  List<Pair<Element, ColorSchema>> getAliasesForLayout(Model model, int layoutId);

  /**
   * Returns a list of pairs {@link Reaction} - {@link ColorSchema}} that are
   * visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Reaction} - {@link ColorSchema} that are
   *         visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}
   */
  List<Pair<Reaction, ColorSchema>> getReactionsForLayout(Model model, int layoutId);

  /**
   * Returns mapping between {@link lcsb.mapviewer.model.map.species.Element
   * Alias}/ {@link lcsb.mapviewer.model.map.reaction.Reaction Reaction} and
   * {@link ColorSchema} used for coloring object in the layout given in the
   * parameter.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Element} - {@link ColorSchema} that are
   *         visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}
   */
  Map<Object, ColorSchema> getElementsForLayout(Model model, Integer layoutId);

  Pair<Element, ColorSchema> getFullAliasForLayout(Model model, Integer id, int layoutId);

  Pair<Reaction, ColorSchema> getFullReactionForLayout(Model model, Integer id, int layoutId);

  /**
   * Returns {@link EmailSender} used by the service.
   *
   * @return {@link EmailSender} used by the service
   */
  EmailSender getEmailSender();

  /**
   * Sets {@link EmailSender} used by the service.
   *
   * @param emailSender
   *          {@link EmailSender} used by the service
   */
  void setEmailSender(EmailSender emailSender);

  /**
   * Returns list of custom layouts.
   *
   * @param project
   *          project where the layouts lay on
   * @return list of custom layouts
   */
  List<Layout> getLayoutsByProject(Project project);

  Layout getLayoutById(Integer overlayId);

  User getOverlayCreator(String overlayId);

  void setLayoutDao(LayoutDao layoutDao);

  /**
   * Parameters used for creation of the layout.
   *
   * @author Piotr Gawron
   *
   */
  class CreateLayoutParams {

    /**
     * Size of the buffer used to copy stream data.
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * Name of the layout.
     *
     */
    private String name;

    /**
     * Description of the layout.
     *
     */
    private String description;

    /**
     * Where the graphic files should be saved.
     *
     */
    private String directory;

    /**
     *
     * {@link Project} for which the layout is created.
     */
    private Project project;

    /**
     * Who creates the layout.
     *
     */
    private User user;

    /**
     *
     * Input stream with coloring information data in the stream should correspond
     * to a file that can be parsed by
     * {@link lcsb.mapviewer.services.utils.ColorSchemaXlsxReader ColorSchemaReader}
     * . This is a copy of original input stream, so we can read this input stream
     * multiple times.
     */
    private ByteArrayOutputStream colorInputStreamCopy;

    /**
     * Name of the file used as input (if available).
     */
    private String layoutFileName;

    /**
     * Determines if creation of the output files should be done asynchronously who
     * creates the layout.
     *
     */
    private boolean async = false;

    private boolean googleLicenseConsent = false;

    /**
     * Type of the uploaded file.
     */
    private ColorSchemaType colorSchemaType;

    /**
     * @return the name
     * @see #name
     */
    public String getName() {
      return name;
    }

    /**
     * @param name
     *          the name to set
     * @see #name
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams name(String name) {
      this.name = name;
      return this;
    }

    /**
     * @return the directory
     * @see #directory
     */
    public String getDirectory() {
      return directory;
    }

    /**
     * @param directory
     *          the directory to set
     * @see #directory
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams directory(String directory) {
      this.directory = directory;
      return this;
    }

    /**
     * @return the project
     * @see #project
     */
    public Project getProject() {
      return project;
    }

    /**
     * @param project
     *          the project to set
     * @see #project
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams project(Project project) {
      this.project = project;
      return this;
    }

    /**
     * @return the user
     * @see #user
     */
    public User getUser() {
      return user;
    }

    /**
     * @param user
     *          the user to set
     * @see #user
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams user(User user) {
      this.user = user;
      return this;
    }

    /**
     * @return the colorInputStream
     * @see #colorInputStream
     */
    public InputStream getColorInputStream() {
      return new ByteArrayInputStream(colorInputStreamCopy.toByteArray());
    }

    /**
     * @param colorInputStream
     *          the colorInputStream to set
     * @see #colorInputStream
     * @return {@link CreateLayoutParams} object
     * @throws IOException
     *           thrown when there is a problem with accessing input stream
     */
    public CreateLayoutParams colorInputStream(InputStream colorInputStream) throws IOException {
      if (colorInputStream == null) {
        this.colorInputStreamCopy = null;
      } else {
        this.colorInputStreamCopy = new ByteArrayOutputStream();

        byte[] buffer = new byte[BUFFER_SIZE];
        int len;
        while ((len = colorInputStream.read(buffer)) > -1) {
          this.colorInputStreamCopy.write(buffer, 0, len);
        }
        this.colorInputStreamCopy.flush();
      }
      return this;
    }

    /**
     * @return the async
     * @see #async
     */
    public boolean isAsync() {
      return async;
    }

    /**
     * @param async
     *          the async to set
     * @see #async
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams async(boolean async) {
      this.async = async;
      return this;
    }

    /**
     * @return the googleLicenseConsent
     * @see #googleLicenseConsent
     */
    public boolean isGoogleLicenseConsent() {
      return googleLicenseConsent;
    }

    /**
     * @param googleLicenseConsent
     *          the googleLicenseConsent to set
     * @see #googleLicenseConsent
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams googleLicenseConsent(boolean googleLicenseConsent) {
      this.googleLicenseConsent = googleLicenseConsent;
      return this;
    }

    /**
     * @return the description
     * @see #description
     */
    public String getDescription() {
      return description;
    }

    /**
     * @param description
     *          the description to set
     * @see #description
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams description(String description) {
      this.description = description;
      return this;
    }

    /**
     *
     * @return {@link #colorSchemaType}
     */
    public ColorSchemaType getColorSchemaType() {
      return colorSchemaType;
    }

    /**
     * @param colorSchemaType
     *          the colorSchemaType to set
     * @see #colorSchemaType
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams colorSchemaType(ColorSchemaType colorSchemaType) {
      this.colorSchemaType = colorSchemaType;
      return this;
    }

    /**
     * @return the layoutFileName
     * @see #layoutFileName
     */
    public String getLayoutFileName() {
      return layoutFileName;
    }

    /**
     * @param layoutFileName
     *          the layoutFileName to set
     * @return {@link CreateLayoutParams} object
     * @see #layoutFileName
     */
    public CreateLayoutParams layoutFileName(String layoutFileName) {
      this.layoutFileName = layoutFileName;
      return this;
    }
  }

}
