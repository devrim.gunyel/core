package lcsb.mapviewer.services.interfaces;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Service that gives access to search functionalities.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ISearchService {

  /**
   * Search for elements on the map by query. Returns many possibilities from best
   * matching to less matching possibility.
   * 
   * @param model
   *          model on which we perform search
   * @param query
   *          the query
   * @param limit
   *          maximum number of results
   * @param perfectMatch
   *          should the match be perfect
   * @param ipAddress
   *          ip address of a client who is searching (used for logging purpose)
   * @return list of objects that matches the query sorted by the match quality
   */
  List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch, String ipAddress);

  List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch);

  /**
   * Returns the closest elements to the coordinates on the model.
   * 
   * @param model
   *          model on which the search is performed
   * @param point
   *          coordinates where search is performed
   * @param numberOfElements
   *          how many closest elements should be returned
   * @return list of the closest elements
   */
  List<BioEntity> getClosestElements(Model model, Point2D point, int numberOfElements, boolean perfectHit,
      Collection<String> types);

  /**
   * Returns list of autocomplete strings for the partial query.
   * 
   * @param model
   *          on which model the search is performed
   * @param query
   *          partial query used for autocomplete
   * @return list of strings that could be used for autocomplete the query
   */
  List<String> getAutocompleteList(Model model, String query);

  String[] getSuggestedQueryList(Model model);

}
