package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;

public interface IFileService {

  UploadedFileEntry getById(Integer id);

  User getOwnerByFileId(Integer id);

}
