package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.statistics.SearchType;

/**
 * Service that manages search history.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ISearchHistoryService {
  /**
   * Adds search query entry into database.
   * 
   * @param query
   *          what has been searched for
   * @param type
   *          type of the search
   * @param ipAddress
   *          ip address that was searching
   * @param projectId
   *          identifier of the project where search was performed
   */
  void addQuery(String query, SearchType type, String ipAddress, String projectId);
}
