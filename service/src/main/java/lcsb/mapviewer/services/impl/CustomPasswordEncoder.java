package lcsb.mapviewer.services.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomPasswordEncoder implements PasswordEncoder {

  private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
  private CustomMd5PasswordEncoder md5PasswordEncoder = new CustomMd5PasswordEncoder();

  @Override
  public String encode(CharSequence rawPassword) {
    return bCryptPasswordEncoder.encode(md5PasswordEncoder.encode(rawPassword));
  }

  @Override
  public boolean matches(CharSequence rawPassword, String encodedPassword) {
    return bCryptPasswordEncoder.matches(md5PasswordEncoder.encode(rawPassword), encodedPassword);
  }

}
