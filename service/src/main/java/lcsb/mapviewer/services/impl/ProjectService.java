package lcsb.mapviewer.services.impl;

import java.io.*;
import java.util.*;
import java.util.concurrent.CountDownLatch;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.hibernate.HibernateException;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.common.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.*;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.*;
import lcsb.mapviewer.services.overlay.AnnotatedObjectTreeRow;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import lcsb.mapviewer.services.search.drug.IDrugService;
import lcsb.mapviewer.services.search.mirna.IMiRNAService;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.BuildInLayout;

/**
 * Implementation of the project service. It allows to manage and access project
 * data.
 *
 * @author Piotr Gawron
 *
 */
@Transactional
@Service
public class ProjectService implements IProjectService {

  /**
   * Size of the artificial buffer that will be released when
   * {@link OutOfMemoryError} is thrown to gain some free memory and report
   * problem.
   */
  private static final int OUT_OF_MEMORY_BACKUP_BUFFER_SIZE = 10000;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(ProjectService.class);

  /**
   * Data access object for projects.
   */
  private ProjectDao projectDao;

  /**
   * Data access object for models.
   */
  private ModelDao modelDao;

  /**
   * Data access object for users.
   */
  private UserDao userDao;

  /**
   * Service that allows to access and manage models.
   */
  private IModelService modelService;

  /**
   * Service that allows to access and manage comments.
   */
  private ICommentService commentService;

  /**
   * Service that manages and gives access to configuration parameters.
   */
  private IConfigurationService configurationService;

  /**
   * Services that manages and gives access to user information.
   */
  private IUserService userService;

  /**
   * Services that access data about chemicals.
   */
  private IChemicalService chemicalService;

  /**
   * Services that access data about drugs.
   */
  private IDrugService drugService;

  /**
   * Services that access data about mirna.
   */
  private IMiRNAService mirnaService;

  /**
   * Module that allows to annotate maps.
   */
  private ModelAnnotator modelAnnotator;

  private LayoutDao layoutDao;

  /**
   * Utils that help to manage the sessions in custom multi-threaded
   * implementation.
   */
  private DbUtils dbUtils;

  /**
   * Access point and parser for the online ctd database.
   */
  private MeSHParser meshParser;

  /**
   * Access point and parser for the online ctd database.
   */
  private TaxonomyBackend taxonomyBackend;

  /**
   * Class that helps to generate images for google maps API.
   */
  private MapGenerator generator = new MapGenerator();

  @Autowired
  public ProjectService(ProjectDao projectDao,
      ModelDao modelDao,
      UserDao userDao,
      IModelService modelService,
      ICommentService commentService,
      IConfigurationService configurationService,
      IUserService userService,
      IChemicalService chemicalService,
      IDrugService drugService,
      IMiRNAService mirnaService,
      ModelAnnotator modelAnnotator,
      DbUtils dbUtils,
      MeSHParser meshParser,
      TaxonomyBackend taxonomyBackend,
      LayoutDao layoutDao) {
    this.projectDao = projectDao;
    this.modelDao = modelDao;
    this.userDao = userDao;
    this.modelService = modelService;
    this.commentService = commentService;
    this.configurationService = configurationService;
    this.userService = userService;
    this.chemicalService = chemicalService;
    this.drugService = drugService;
    this.mirnaService = mirnaService;
    this.modelAnnotator = modelAnnotator;
    this.dbUtils = dbUtils;
    this.meshParser = meshParser;
    this.taxonomyBackend = taxonomyBackend;
    this.layoutDao = layoutDao;
  }

  @Override
  public Project getProjectByProjectId(String name) {
    return projectDao.getProjectByProjectId(name);
  }

  @Override
  public boolean projectExists(String projectName) {
    if (projectName == null || projectName.equals("")) {
      return false;
    }
    return projectDao.isProjectExistsByName(projectName);
  }

  @Override
  public List<Project> getAllProjects() {
    return projectDao.getAll();
  }

  @Override
  public void removeProject(final Project p, final String dir, final boolean async) {
    final String homeDir;
    if (dir != null) {
      if (p.getDirectory() != null) {
        homeDir = dir + "/../map_images/" + p.getDirectory() + "/";
      } else {
        homeDir = dir + "/../map_images/";
      }
    } else {
      if (p.getDirectory() != null) {
        homeDir = p.getDirectory() + "/";
      } else {
        homeDir = null;
      }
    }
    updateProjectStatus(p, ProjectStatus.REMOVING, 0, new CreateProjectParams());
    Thread computations = new Thread(new Runnable() {

      @Override
      public void run() {
        if (async) {
          // because we are running this in separate thread we need to open a
          // new session for db connection
          dbUtils.createSessionForCurrentThread();
        }

        Project project = projectDao.getById(p.getId());

        try {
          String email = null;
          MapGenerator mapGenerator = new MapGenerator();
          for (ModelData originalModel : project.getModels()) {
            List<ModelData> models = new ArrayList<>();
            models.add(originalModel);
            for (ModelSubmodelConnection connection : originalModel.getSubmodels()) {
              models.add(connection.getSubmodel());
            }
            modelService.removeModelFromCache(originalModel);
            for (ModelData model : models) {
              logger.debug("Remove model: " + model.getId());
              commentService.removeCommentsForModel(model);

            }
            for (Layout layout : layoutDao.getLayoutsByProject(project)) {
              try {
                mapGenerator.removeLayout(layout, homeDir);
                layout.setProject(null);
                layoutDao.delete(layout);
              } catch (IOException e) {
                logger.error(
                    "Problem with removing directory for layout: " + layout.getId() + "; " + layout.getTitle(), e);
              }
            }
            email = project.getNotifyEmail();
          }
          if (homeDir != null) {
            File homeDirFile = new File(homeDir);
            if (homeDirFile.exists()) {
              logger.debug("Removing project directory: " + homeDirFile.getAbsolutePath());
              try {
                FileUtils.deleteDirectory(homeDirFile);
              } catch (IOException e) {
                logger.error("Problem with removing diriectory", e);
              }
            }
          }

          projectDao.delete(project);
          if (async) {
            projectDao.commit();
          }

          logger.info("Project " + project.getProjectId() + " removed successfully.");

          if (email != null) {
            try {
              sendSuccesfullRemoveEmail(project.getProjectId(), email);
            } catch (MessagingException e) {
              logger.error("Problem with sending remove email.", e);
            }
          }
          modelService.removeModelFromCacheByProjectId(p.getProjectId());

        } catch (HibernateException e) {
          logger.error("Problem with database", e);
          handleHibernateExceptionRemovingReporting(project, e);
        } finally {
          if (async) {
            // close the transaction for this thread
            dbUtils.closeSessionForCurrentThread();
          }
        }

      }
    });

    if (async) {
      computations.start();
    } else {
      computations.run();
    }

  }

  @Override
  public void addProject(Project project) {
    projectDao.add(project);

  }

  @Override
  public void createProject(final CreateProjectParams params) {
    // this count down is used to wait for asynchronous thread to initialize
    // data in the db (probably it would be better to move the initialization to
    // main thread)
    final CountDownLatch waitForInitialData = new CountDownLatch(1);

    final List<Throwable> problems = new ArrayList<>();

    Thread computations = new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          if (params.isAsync()) {
            // because we are running this in separate thread we need to open a
            // new session for db connection
            dbUtils.createSessionForCurrentThread();
          }

          Project project = createProjectFromParams(params);
          projectDao.add(project);
          if (params.isAsync()) {
            projectDao.commit();
          }
          waitForInitialData.countDown();
          double[] outOfMemoryBuffer;
          MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
          try {
            logger.debug("Running: " + params.getProjectId() + "; " + params.getProjectFile());
            outOfMemoryBuffer = new double[OUT_OF_MEMORY_BACKUP_BUFFER_SIZE];
            for (int i = 0; i < OUT_OF_MEMORY_BACKUP_BUFFER_SIZE; i++) {
              outOfMemoryBuffer[i] = Math.random() * OUT_OF_MEMORY_BACKUP_BUFFER_SIZE;
            }

            UploadedFileEntry file = params.getProjectFile();
            project.setInputData(file);

            createModel(params, project);
            Model originalModel = project.getModels().iterator().next().getModel();
            new CreateHierarchyCommand(originalModel, generator.computeZoomLevels(originalModel),
                generator.computeZoomFactor(originalModel)).execute();
            for (Model model : originalModel.getSubmodels()) {
              new CreateHierarchyCommand(model, generator.computeZoomLevels(model), generator.computeZoomFactor(model))
                  .execute();
            }

            createImages(project, params);

            for (Layout layout : project.getLayouts()) {
              for (DataOverlayImageLayer imageLayer : layout.getDataOverlayImageLayers()) {
                String[] tmp = imageLayer.getDirectory().split("[\\\\/]");
                imageLayer.setDirectory(tmp[tmp.length - 1]);
              }
            }

            projectDao.update(project);

            if (params.isAnalyzeAnnotations()) {
              analyzeAnnotations(originalModel, params);
            }
            MinervaLoggerAppender.unregisterLogEventStorage(appender);
            project.addLogEntries(createLogEntries(appender));

            if (params.isCacheModel()) {
              cacheData(originalModel, params);
            }

            updateProjectStatus(project, ProjectStatus.DONE, IProgressUpdater.MAX_PROGRESS, params);
            if (project.getNotifyEmail() != null && !project.getNotifyEmail().equals("")) {
              try {
                sendSuccesfullEmail(originalModel);
              } catch (MessagingException e) {
                logger.error(e, e);
              }
            }

            logger.info("Project " + project.getProjectId() + " created successfully.");
            modelService.getLastModelByProjectId(project.getProjectId());
          } catch (HibernateException e) {
            outOfMemoryBuffer = null;
            logger.error("Problem with database", e);
            handleHibernateExceptionReporting(params, e);
          } catch (Exception e) {
            outOfMemoryBuffer = null;
            handleCreateProjectException(params, e);
          } catch (OutOfMemoryError oome) {
            // release some memory
            outOfMemoryBuffer = null;
            logger.error("Out of memory", oome);
            if (project != null) {
              ProjectLogEntry entry = new ProjectLogEntry();
              entry.setType(ProjectLogEntryType.OUT_OF_MEMORY);
              entry.setSeverity("ERROR");
              entry.setContent("Out of memory: " + oome.getMessage());
              project.addLogEntry(entry);
            }
            updateProjectStatus(project, ProjectStatus.FAIL, IProgressUpdater.MAX_PROGRESS, params);
          } finally {
            if (params.isAsync()) {
              // close the transaction for this thread
              dbUtils.closeSessionForCurrentThread();
            }
            MinervaLoggerAppender.unregisterLogEventStorage(appender);
          }
        } catch (Throwable t) {
          problems.add(t);
          waitForInitialData.countDown();
        }
      }

    });
    if (params.isAsync()) {
      computations.start();
    } else {
      computations.run();
    }

    try {
      waitForInitialData.await();
    } catch (InterruptedException e1) {
      problems.add(e1);
    }
    if (problems.size() > 0) {
      throw new RuntimeException(problems.get(0));
    }

  }

  @Override
  public TreeNode createClassAnnotatorTree(User user) {

    UserAnnotationSchema annotationSchema = prepareUserAnnotationSchema(user);

    ElementUtils elementUtils = new ElementUtils();

    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();

    Class<?> clazz = top.getClazz();
    top.setData(annotationSchema.requiresAtLeastOneAnnotation(clazz));
    List<AnnotatorData> annotators = annotationSchema.getAnnotatorsForClass(clazz);
    if (annotators == null) {
      annotators = new ArrayList<>();
    }
    TreeNode root = new DefaultTreeNode(new AnnotatedObjectTreeRow(top, modelAnnotator.getAvailableAnnotators(clazz),
        modelAnnotator.getAnnotatorsFromCommonNames(annotators),
        annotationSchema.getValidAnnotations(clazz), annotationSchema.getRequiredAnnotations(clazz)), null);

    root.setExpanded(true);

    Queue<Pair<ClassTreeNode, TreeNode>> nodes = new LinkedList<Pair<ClassTreeNode, TreeNode>>();
    nodes.add(new Pair<ClassTreeNode, TreeNode>(top, root));
    // create children

    Queue<TreeNode> expandParents = new LinkedList<TreeNode>();

    while (!nodes.isEmpty()) {
      Pair<ClassTreeNode, TreeNode> element = nodes.poll();

      for (ClassTreeNode node : element.getLeft().getChildren()) {

        clazz = node.getClazz();
        node.setData(annotationSchema.requiresAtLeastOneAnnotation(clazz));
        annotators = annotationSchema.getAnnotatorsForClass(clazz);
        if (annotators == null) {
          annotators = new ArrayList<>();
        }
        AnnotatedObjectTreeRow data = new AnnotatedObjectTreeRow(node, modelAnnotator.getAvailableAnnotators(clazz),
            modelAnnotator.getAnnotatorsFromCommonNames(annotators),
            annotationSchema.getValidAnnotations(clazz), annotationSchema.getRequiredAnnotations(clazz));
        TreeNode treeNode = new DefaultTreeNode(data, element.getRight());
        nodes.add(new Pair<ClassTreeNode, TreeNode>(node, treeNode));
        if (data.getUsedAnnotators().size() > 0 || data.getValidAnnotators().size() > 0) {
          expandParents.add(treeNode);
        }
      }
    }
    while (!expandParents.isEmpty()) {
      TreeNode node = expandParents.poll();
      if (node.getParent() != null && !node.getParent().isExpanded()) {
        node.getParent().setExpanded(true);
        expandParents.add(node.getParent());
      }
    }

    return root;

  }

  @Override
  public void updateClassAnnotatorTreeForUser(User user, TreeNode annotatorsTree, boolean sbgnFormat,
      boolean networkLayoutAsDefault) {
    User dbUser = userDao.getById(user.getId());
    if (dbUser.getAnnotationSchema() == null) {
      dbUser.setAnnotationSchema(new UserAnnotationSchema());
    }
    UserAnnotationSchema annotationSchema = dbUser.getAnnotationSchema();

    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(annotatorsTree);
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();
      queue.addAll(node.getChildren());
      AnnotatedObjectTreeRow data = (AnnotatedObjectTreeRow) node.getData();
      annotationSchema.addClassAnnotator(new UserClassAnnotators(data.getClazz(), data.getUsedAnnotators()));
      annotationSchema.addClassRequiredAnnotations(
          new UserClassRequiredAnnotations(data.getClazz(), data.getRequiredAnnotations()));
      annotationSchema
          .addClassValidAnnotations(new UserClassValidAnnotations(data.getClazz(), data.getValidAnnotations()));
    }
    annotationSchema.setSbgnFormat(sbgnFormat);
    annotationSchema.setNetworkLayoutAsDefault(networkLayoutAsDefault);
    userService.updateUser(dbUser);
    user.setAnnotationSchema(annotationSchema);
  }

  @Override
  public void updateProject(Project project) {
    projectDao.update(project);
  }

  /**
   * Retrieves (or creates) annotation schema for a given user.
   *
   * @param user
   *          for this users {@link UserAnnotationSchema} will be prepared
   * @return {@link UserAnnotationSchema} for {@link User}
   */
  public UserAnnotationSchema prepareUserAnnotationSchema(User user) {
    UserAnnotationSchema annotationSchema = null;
    if (user != null) {
      annotationSchema = userDao.getById(user.getId()).getAnnotationSchema();
      if (annotationSchema != null && annotationSchema.getClassAnnotators().size() == 0) {
        for (UserClassAnnotators uca : modelAnnotator.createDefaultAnnotatorSchema().getClassAnnotators()) {
          annotationSchema.addClassAnnotator(uca);
        }
      }
    }
    if (annotationSchema == null) {
      annotationSchema = modelAnnotator.createDefaultAnnotatorSchema();

      ElementUtils elementUtils = new ElementUtils();

      ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();

      Map<Class<? extends BioEntity>, Set<MiriamType>> validMiriam = modelAnnotator.getDefaultValidClasses();
      Map<Class<? extends BioEntity>, Set<MiriamType>> requiredMiriam = modelAnnotator.getDefaultRequiredClasses();

      Queue<ClassTreeNode> nodes = new LinkedList<ClassTreeNode>();
      nodes.add(top);

      while (!nodes.isEmpty()) {
        ClassTreeNode element = nodes.poll();
        annotationSchema.addClassAnnotator(new UserClassAnnotators(element.getClazz(),
            modelAnnotator.getDefaultAnnotators(element.getClazz())));
        annotationSchema.addClassValidAnnotations(
            new UserClassValidAnnotations(element.getClazz(), validMiriam.get(element.getClazz())));
        annotationSchema.addClassRequiredAnnotations(
            new UserClassRequiredAnnotations(element.getClazz(), requiredMiriam.get(element.getClazz())));
        nodes.addAll(element.getChildren());
      }
      if (user != null) {
        User dbUser = userDao.getById(user.getId());
        dbUser.setAnnotationSchema(annotationSchema);
        userDao.update(dbUser);
      }
    }
    return annotationSchema;
  }

  /**
   * When we encountered hibernate exception we need to handle error reporting
   * differently (hibernate session is broken). This method handles such case when
   * hibernate exception occurred when removing project.
   *
   * @param originalProject
   *          project that was being removed
   * @param exception
   */
  protected void handleHibernateExceptionRemovingReporting(Project originalProject, HibernateException exception) {
    // we need to open separate thread because current one thrown db exception
    // and transaction is corrupted and will be rolledback
    Thread reportInSeparateThread = new Thread(new Runnable() {

      @Override
      public void run() {
        dbUtils.createSessionForCurrentThread();
        try {
          // we need to get the project from db, because session where
          // originalProject was retrieved is broken
          Project project = getProjectByProjectId(originalProject.getProjectId());

          ProjectLogEntry entry = new ProjectLogEntry();
          entry.setType(ProjectLogEntryType.OTHER);
          entry.setSeverity("ERROR");
          entry.setContent("Severe problem with removing object. Underlaying eror:\n" + exception.getMessage()
              + "\nMore information can be found in log file.");

          project.addLogEntry(entry);

          project.setStatus(ProjectStatus.FAIL);
          projectDao.update(project);
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }

    });
    reportInSeparateThread.start();
  }

  /**
   * This method creates set of images for the model layouts.
   *
   * @param project
   *          project for which we create layout images
   * @param params
   *          configuration parameters including set of layouts to generate
   * @throws IOException
   *           thrown when there are problems with generating files
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   * @throws CommandExecutionException
   *           thrown when one of the files describing layouts is invalid
   */
  protected void createImages(final Project project, final CreateProjectParams params)
      throws IOException, DrawingException, CommandExecutionException {
    if (!params.isImages()) {
      return;
    }
    updateProjectStatus(project, ProjectStatus.GENERATING_IMAGES, 0, params);

    int projectSize = project.getLayouts().size();
    int counter = 0;
    for (int i = 0; i < project.getLayouts().size(); i++) {
      Layout layout = project.getLayouts().get(i);
      if (layout.getInputData() == null) {
        final double imgCounter = counter;
        final double finalSize = projectSize;
        IProgressUpdater updater = new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
            updateProjectStatus(project, ProjectStatus.GENERATING_IMAGES,
                IProgressUpdater.MAX_PROGRESS * imgCounter / finalSize + progress / finalSize, params);
          }
        };
        generateImagesForBuiltInOverlay(params, layout, updater);
      }
      counter++;
    }
  }

  private void generateImagesForBuiltInOverlay(final CreateProjectParams params, Layout layout,
      IProgressUpdater updater) throws CommandExecutionException, IOException, DrawingException {
    for (DataOverlayImageLayer imageLayer : layout.getDataOverlayImageLayers()) {
      String directory = imageLayer.getDirectory();
      Model model = imageLayer.getModel().getModel();
      Model output = model;
      if (layout.isHierarchicalView()) {
        output = new CopyCommand(model).execute();
        new SetFixedHierarchyLevelCommand(output, layout.getHierarchyViewLevel()).execute();
      }
      if (layout.getTitle().equals(BuildInLayout.CLEAN.getTitle())) {
        output = new CopyCommand(model).execute();
        new ClearColorModelCommand(output).execute();
      }
      MapGeneratorParams imgParams = generator.new MapGeneratorParams().directory(directory).sbgn(params.isSbgnFormat())
          .nested(layout.isHierarchicalView()).updater(updater);
      imgParams.model(output);
      generator.generateMapImages(imgParams);
    }
  }

  /**
   * Creates project. Loads model from the input and run PostLoadModification.
   *
   * @param params
   *          params used to create model
   * @param dbProject
   *          project where the model should be placed
   * @throws InvalidInputDataExecption
   *           thrown when there is a problem with input file
   */
  protected void createModel(final CreateProjectParams params, Project dbProject)
      throws InvalidInputDataExecption, ConverterException {
    User dbUser = userDao.getById(params.getUser().getId());
    UserAnnotationSchema userAnnotationSchema = dbUser.getAnnotationSchema();
    if (userAnnotationSchema == null) {
      userAnnotationSchema = new UserAnnotationSchema();
    }

    ModelData modelData = modelDao.getLastModelForProjectIdentifier(params.getProjectId(), false);
    if (modelData != null) {
      throw new InvalidArgumentException("Model with the given name already exists");
    }

    final Project project = dbProject;
    updateProjectStatus(project, ProjectStatus.PARSING_DATA, 0.0, params);

    if (params.isComplex()) {
      try {
        Class<? extends Converter> clazz = CellDesignerXmlParser.class;
        if (params.getParser() != null) {
          clazz = params.getParser().getClass();
        }
        ComplexZipConverter parser = new ComplexZipConverter(clazz);
        ComplexZipConverterParams complexParams;
        complexParams = new ComplexZipConverterParams().zipFile(params.getProjectFile());
        complexParams.visualizationDir(params.getProjectDir());
        for (ZipEntryFile entry : params.getZipEntries()) {
          complexParams.entry(entry);
        }
        ProjectFactory projectFactory = new ProjectFactory(parser);
        projectFactory.create(complexParams, dbProject);
      } catch (IOException e) {
        throw new InvalidInputDataExecption(e);
      }
    } else {
      Converter parser = params.getParser();
      if (parser == null) {
        throw new InvalidArgumentException("Parser is undefined");
      }
      InputStream input = new ByteArrayInputStream(params.getProjectFile().getFileContent());
      Model model = parser
          .createModel(new ConverterParams().inputStream(input, params.getProjectFile().getOriginalFileName())
              .sizeAutoAdjust(params.isAutoResize()).sbgnFormat(params.isSbgnFormat()));
      model.setName(params.getProjectName());
      project.addModel(model);
    }
    Model topModel = project.getModels().iterator().next().getModel();

    Set<Model> models = new HashSet<>();
    models.add(topModel);
    for (ModelSubmodelConnection connection : topModel.getSubmodelConnections()) {
      models.add(connection.getSubmodel().getModel());
    }
    for (Model m : models) {
      for (Layer l : m.getLayers()) {
        l.setVisible(false);
      }
    }

    assignZoomLevelDataToModel(topModel);
    dbProject.setNotifyEmail(params.getNotifyEmail());

    updateProjectStatus(project, ProjectStatus.UPLOADING_TO_DB, 0.0, params);
    dbUtils.setAutoFlush(false);
    projectDao.update(project);
    dbUtils.setAutoFlush(true);
    projectDao.flush();

    List<BuildInLayout> buildInLayouts = new ArrayList<>();
    if (params.isNetworkLayoutAsDefault()) {
      buildInLayouts.add(BuildInLayout.NORMAL);
      buildInLayouts.add(BuildInLayout.NESTED);
    } else {
      buildInLayouts.add(BuildInLayout.NESTED);
      buildInLayouts.add(BuildInLayout.NORMAL);
    }
    buildInLayouts.add(BuildInLayout.CLEAN);

    // reverse the order of build in layouts, so we can insert them at the
    // beginning of list of layouts (the order will be the same)
    Collections.reverse(buildInLayouts);

    for (BuildInLayout buildInLayout : buildInLayouts) {
      int submodelId = 0;
      Layout topLayout = new Layout(buildInLayout.getTitle(), true);
      topLayout.setColorSchemaType(ColorSchemaType.GENERIC);
      topLayout.setStatus(LayoutStatus.NA);
      topLayout.setProgress(0.0);
      topLayout.setHierarchicalView(buildInLayout.isNested());
      project.addLayout(0, topLayout);

      DataOverlayImageLayer imageLayer = new DataOverlayImageLayer(topModel,
          params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + submodelId + "/");
      submodelId++;
      topLayout.addDataOverlayImageLayer(imageLayer);

      List<Layout> semanticLevelOverlays = new ArrayList<>();
      if (params.isSemanticZoomContainsMultipleLayouts() && buildInLayout == BuildInLayout.NESTED) {
        for (int i = 0; i <= topModel.getZoomLevels(); i++) {
          Layout semanticOverlay = new Layout(buildInLayout.getTitle() + "-" + i, true);
          semanticOverlay.setColorSchemaType(ColorSchemaType.GENERIC);

          String directory = params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + "-" + i + "-"
              + topModel.getId() + "/";
          semanticOverlay.addDataOverlayImageLayer(new DataOverlayImageLayer(topModel, directory));

          semanticOverlay.setStatus(LayoutStatus.NA);
          semanticOverlay.setProgress(0.0);
          semanticOverlay.setHierarchicalView(buildInLayout.isNested());
          semanticOverlay.setHierarchyViewLevel(i);
          semanticOverlay.setProject(topModel.getProject());
          semanticLevelOverlays.add(semanticOverlay);
          project.addLayout(1, semanticOverlay);
        }
      }
      for (ModelSubmodelConnection connection : topModel.getSubmodelConnections()) {
        String directory = params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + submodelId + "/";
        topLayout.addDataOverlayImageLayer(new DataOverlayImageLayer(connection.getSubmodel(), directory));

        assignZoomLevelDataToModel(connection.getSubmodel().getModel());
        if (params.isSemanticZoomContainsMultipleLayouts() && buildInLayout == BuildInLayout.NESTED) {
          for (int i = 0; i < semanticLevelOverlays.size(); i++) {
            Layout semanticOverlay = semanticLevelOverlays.get(i);
            directory = params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + "-" + i + "-"
                + submodelId + "/";
            semanticOverlay.addDataOverlayImageLayer(new DataOverlayImageLayer(connection.getSubmodel(), directory));
          }
        }
        submodelId++;
      }
    }
    int order = 0;
    for (Layout l : project.getLayouts()) {
      l.setOrderIndex(order++);
    }
    projectDao.update(project);

    if (params.isUpdateAnnotations()) {
      Map<Class<?>, List<ElementAnnotator>> annotators = null;
      if (params.getAnnotatorsMap() != null) {
        annotators = new HashMap<Class<?>, List<ElementAnnotator>>();
        for (Class<?> clazz : params.getAnnotatorsMap().keySet()) {
          annotators.put(clazz, modelAnnotator.getAnnotatorsFromCommonNames(params.getAnnotatorsMap().get(clazz)));
        }
      }
      logger.debug("Updating annotations");
      modelAnnotator.performAnnotations(topModel, new IProgressUpdater() {

        @Override
        public void setProgress(final double progress) {
          updateProjectStatus(project, ProjectStatus.ANNOTATING, progress, params);
        }
      }, userAnnotationSchema);
      logger.debug("Annotations updated");
    }
    logger.debug("Model created");

  }

  private void assignZoomLevelDataToModel(Model topModel) throws InvalidInputDataExecption {
    Integer maxZoomLevels = Integer
        .parseInt(configurationService.getValue(ConfigurationElementType.MAX_NUMBER_OF_MAP_LEVELS).getValue());

    Integer zoomLevels = generator.computeZoomLevels(topModel);
    if (zoomLevels > maxZoomLevels) {
      throw new InvalidInputDataExecption(
          "Map " + topModel.getName() + " too big. You can change the max number of map zoom levels in configuration.");
    }
    topModel.setZoomLevels(zoomLevels);
    topModel.setTileSize(MapGenerator.TILE_SIZE);

  }

  /**
   * Updates status of the generating project.
   *
   * @param project
   *          project that is generated
   * @param status
   *          what is the current status
   * @param progress
   *          what is the progress
   * @param params
   *          parameters used for project creation
   */
  private void updateProjectStatus(Project project, ProjectStatus status, double progress, CreateProjectParams params) {
    if (project != null) {
      if (!status.equals(project.getStatus())
          || (Math.abs(progress - project.getProgress()) > IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION)) {
        project.setStatus(status);
        project.setProgress(progress);
        projectDao.update(project);
        if (params.isAsync()) {
          projectDao.commit();
        }
      }
    } else {
      logger.debug("status: " + status + ", " + progress);
    }
  }

  /**
   * Cache pubmed data for the model.
   *
   * @param originalModel
   *          model for which we want to cache data.
   * @param params
   *          parameters used for model generation
   */
  private void cacheData(final Model originalModel, final CreateProjectParams params) {
    modelService.cacheAllPubmedIds(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING, progress, params);
      }
    });
    modelService.cacheAllMiriamLinks(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_MIRIAM, progress, params);
      }
    });

    logger.warn("Cache of chemicals data is disabled");

    // chemicalService.cacheDataForModel(originalModel, new IProgressUpdater() {
    // @Override
    // public void setProgress(double progress) {
    // updateProjectStatus(originalModel.getProject(),
    // ProjectStatus.CACHING_CHEMICAL, progress, params);
    // }
    // });

    drugService.cacheDataForModel(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_DRUG, progress, params);
      }
    });

    mirnaService.cacheDataForModel(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_MI_RNA, progress, params);
      }
    });

  }

  /**
   * @return the projectDao
   * @see #projectDao
   */
  public ProjectDao getProjectDao() {
    return projectDao;
  }

  /**
   * @param projectDao
   *          the projectDao to set
   * @see #projectDao
   */
  public void setProjectDao(ProjectDao projectDao) {
    this.projectDao = projectDao;
  }

  /**
   * Analyzes annotation of the model
   *
   * @param originalModel
   *          model to analyze
   * @param params
   *          parameters used for model generation
   */
  protected void analyzeAnnotations(final Model originalModel, final CreateProjectParams params) {
    logger.debug("Analyze annotations");
    Collection<? extends ProblematicAnnotation> improperAnnotations = modelAnnotator
        .findImproperAnnotations(originalModel, new IProgressUpdater() {

          @Override
          public void setProgress(double progress) {
            updateProjectStatus(originalModel.getProject(), ProjectStatus.VALIDATING_MIRIAM, progress, params);
          }
        }, params.getValidAnnotations());

    for (ProblematicAnnotation problematicAnnotation : improperAnnotations) {
      logger.warn(problematicAnnotation.getLogMarker(), problematicAnnotation.getMessage());
    }

    Collection<? extends ProblematicAnnotation> missingAnnotations = modelAnnotator
        .findMissingAnnotations(originalModel, params.getRequiredAnnotations());

    for (ProblematicAnnotation problematicAnnotation : missingAnnotations) {
      logger.warn(problematicAnnotation.getLogMarker(), problematicAnnotation.getMessage());
    }

    logger.debug("Analyze finished");
  }

  /**
   * Sends notification email that map was removed.
   *
   * @param projectId
   *          identifier of the project
   * @param email
   *          notification email
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(String projectId, String email) throws MessagingException {
    EmailSender emailSender = new EmailSender(configurationService);
    emailSender.sendEmail("Minerva notification", "Map " + projectId + " was successfully removed.<br/>", email);
  }

  /**
   * Sends email about unsuccessful project creation.
   *
   * @param projectName
   *          name of the project
   * @param email
   *          email where we want to send information
   * @param e
   *          exception that caused problem
   */
  private void sendUnsuccesfullEmail(String projectName, String email, Exception e) {
    EmailSender emailSender = new EmailSender(configurationService);
    StringBuilder content = new StringBuilder("");
    content.append("There was a problem with generating ").append(projectName).append(" map.<br/>");
    content.append(e.getClass().getName()).append(": ").append(e.getMessage());
    try {
      emailSender.sendEmail("Minerva notification", content.toString(), email);
    } catch (MessagingException e1) {
      logger.error(e1);
    }
  }

  /**
   * Sends email about successful project creation.
   *
   * @param originalModel
   *          model that was created
   * @throws MessagingException
   *           exception thrown when there is a problem with sending email
   */
  protected void sendSuccesfullEmail(Model originalModel) throws MessagingException {
    EmailSender emailSender = new EmailSender(configurationService);
    emailSender.sendEmail("Minerva notification",
        "Your map " + originalModel.getProject().getProjectId() + " was generated successfully.<br/>",
        originalModel.getProject().getNotifyEmail());
  }

  /**
   * This method handles situation when sever db error appeared during uploading
   * of the project into database.
   *
   * @param params
   *          parameters used to create project
   * @param e
   *          exception that occurred during uploading of the project
   */
  private void handleHibernateExceptionReporting(CreateProjectParams params, HibernateException e) {
    // we need to open separate thread because current one thrown db exception
    // and transaction is corrupted and will be rolledback
    Thread reportInSeparateThread = new Thread(new Runnable() {

      @Override
      public void run() {
        dbUtils.createSessionForCurrentThread();

        try {
          Project project = getProjectByProjectId(params.getProjectId());

          ProjectLogEntry entry = new ProjectLogEntry();
          entry.setType(ProjectLogEntryType.DATABASE_PROBLEM);
          entry.setSeverity("ERROR");
          entry.setContent("Problem with uploading to database. "
              + "You might violated some unhandled constraints or you run out of memory. Underlaying eror:\n"
              + e.getMessage() + "\nMore information can be found in log file.");

          project.addLogEntry(entry);
          project.setStatus(ProjectStatus.FAIL);
          projectDao.update(project);
        } catch (Exception e) {
          logger.error(e, e);
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }

    });
    reportInSeparateThread.start();

  }

  /**
   * Method that handles exception reporting during creation of a project.
   *
   * @param params
   *          set of parameters used to create project
   * @param e
   *          exception that caused problems
   */
  private void handleCreateProjectException(final CreateProjectParams params, Exception e) {
    logger.error("Problem with uploading project " + params.getProjectId(), e);
    Project p = projectDao.getProjectByProjectId(params.getProjectId());
    if (p != null) {
      ProjectLogEntry entry = new ProjectLogEntry();
      entry.setType(ProjectLogEntryType.OTHER);
      entry.setSeverity("ERROR");
      entry.setContent("Problem with uploading map: " + e.getMessage() + ". More details can be found in log file.");
      p.addLogEntry(entry);
    }
    updateProjectStatus(p, ProjectStatus.FAIL, IProgressUpdater.MAX_PROGRESS, params);

    String email = params.getNotifyEmail();
    String projectName = params.getProjectId();

    if (email != null) {
      sendUnsuccesfullEmail(projectName, email, e);
    }
  }

  private Project createProjectFromParams(final CreateProjectParams params) {
    Project project = new Project(params.getProjectId());
    project.setOwner(userDao.getUserByLogin(params.getUser().getLogin()));
    project.setName(params.getProjectName());
    if (params.getProjectDir() == null) {
      logger.warn("Project directory not set");
      project.setDirectory(null);
    } else {
      project.setDirectory(new File(params.getProjectDir()).getName());
    }
    project.setSbgnFormat(params.isSbgnFormat());

    MiriamData disease = null;
    if (params.getDisease() != null && !params.getDisease().isEmpty()) {
      disease = new MiriamData(MiriamType.MESH_2012, params.getDisease());
    }
    MiriamData organism = null;
    if (params.getOrganism() != null && !params.getOrganism().isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, params.getOrganism());
    }
    try {
      if (meshParser.isValidMeshId(disease)) {
        project.setDisease(disease);
      } else {
        logger.warn("No valid disease is provided for project:" + project.getName());
      }
    } catch (AnnotatorException e1) {
      logger.warn("Problem with accessing mesh db. More info in logs.", e1);
    }

    try {
      if (taxonomyBackend.getNameForTaxonomy(organism) != null) {
        project.setOrganism(organism);
      } else {
        logger.warn(project.getProjectId() + "\tNo valid organism is provided for project. " + organism);
      }
    } catch (TaxonomySearchException e) {
      logger.warn("Problem with accessing taxonomy db. More info in logs.", e);
    }

    project.setMapCanvasType(params.getMapCanvasType());
    project.setVersion(params.getVersion());
    return project;
  }

  Set<ProjectLogEntry> createLogEntries(MinervaLoggerAppender appender) {
    Set<ProjectLogEntry> result = new HashSet<>();
    for (LogEvent event : appender.getWarnings()) {
      result.add(createLogEntry("WARNING", event));
    }
    for (LogEvent event : appender.getErrors()) {
      result.add(createLogEntry("ERROR", event));
    }
    result.remove(null);
    return result;
  }

  private ProjectLogEntry createLogEntry(String severity, LogEvent event) {
    if (event.getMarker() instanceof IgnoredLogMarker) {
      return null;
    }
    ProjectLogEntry entry = null;
    if (event.getMarker() != null) {
      if (event.getMarker() instanceof LogMarker) {
        entry = ((LogMarker) event.getMarker()).getEntry();
      }
    }
    if (entry == null) {
      entry = new ProjectLogEntry();
      entry.setType(ProjectLogEntryType.OTHER);
    }
    entry.setSeverity(severity);
    entry.setContent(event.getMessage().getFormattedMessage());
    return entry;
  }

}
