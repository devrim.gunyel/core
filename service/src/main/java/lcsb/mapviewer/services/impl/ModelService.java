package lcsb.mapviewer.services.impl;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

/**
 * Implementation of the service that manages models.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class ModelService implements IModelService {

  /**
   * List of cached models.
   */
  private static Map<String, Model> models = new HashMap<String, Model>();
  /**
   * List of models that are currently being loaded from database.
   */
  private static Set<String> modelsInLoadStage = new HashSet<>();
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(ModelService.class);
  /**
   * Service that manages and gives access to user information.
   */
  private IUserService userService;

  /**
   * Data access object for models.
   */
  private ModelDao modelDao;

  private ProjectDao projectDao;

  /**
   * Local backend to the pubmed data.
   */
  private PubmedParser backend;

  /**
   * Connector used for accessing data from miriam registry.
   */
  private MiriamConnector miriamConnector;

  @Autowired
  public ModelService(IUserService userService,
      ModelDao modelDao,
      ProjectDao projectDao,
      PubmedParser backend,
      MiriamConnector miriamConnector) {
    this.userService = userService;
    this.modelDao = modelDao;
    this.projectDao = projectDao;
    this.backend = backend;
    this.miriamConnector = miriamConnector;
  }

  @Override
  public Model getLastModelByProjectId(String projectName) {
    if (projectName == null) {
      return null;
    }

    // check if model is being loaded by another thread
    boolean waitForModel = false;
    do {
      synchronized (modelsInLoadStage) {
        waitForModel = modelsInLoadStage.contains(projectName);
      }
      // if model is being loaded then wait until it's loaded
      if (waitForModel) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          logger.fatal(e, e);
        }
      }
    } while (waitForModel);

    Model model = models.get(projectName);
    if (model == null) {
      try {
        // mark model as being load stage, so other threads that want to access
        // it will wait
        synchronized (modelsInLoadStage) {
          modelsInLoadStage.add(projectName);
        }
        logger.debug("Unknown model, trying to load the model into memory.");

        ModelData modelData = modelDao.getLastModelForProjectIdentifier(projectName, false);

        if (modelData == null) {
          logger.debug("Model doesn't exist");
          return null;
        }
        logger.debug("Model loaded from db.");
        model = new ModelFullIndexed(modelData);

        // this is a trick to load all required subelements of the model... ;/
        // lets copy model - it will access all elements...
        new CopyCommand(model).execute();

        for (ModelData m : model.getProject().getModels()) {
          new CopyCommand(m.getModel()).execute();
        }

        logger.debug("Model loaded successfullly");
        models.put(projectName, model);
      } finally {
        // model is not being loaded anymore
        synchronized (modelsInLoadStage) {
          modelsInLoadStage.remove(projectName);
        }
      }
    }
    return model;
  }

  @Override
  public void cacheAllPubmedIds(Model model, IProgressUpdater updater) {
    logger.debug("Caching pubmed ids...");
    if (model != null) {
      Set<Integer> pubmedIds = new HashSet<>();

      for (Element element : model.getElements()) {
        for (MiriamData md : element.getMiriamData()) {
          if (MiriamType.PUBMED.equals(md.getDataType())) {
            try {
              pubmedIds.add(Integer.parseInt(md.getResource()));
            } catch (NumberFormatException e) {
              logger.error("Problem with parsing: " + e.getMessage(), e);
            }
          }
        }
      }
      for (Reaction reaction : model.getReactions()) {
        for (MiriamData md : reaction.getMiriamData()) {
          if (MiriamType.PUBMED.equals(md.getDataType())) {
            try {
              pubmedIds.add(Integer.parseInt(md.getResource()));
            } catch (NumberFormatException e) {
              logger.error("Problem with parsing: " + e.getMessage(), e);
            }
          }
        }
      }
      double amount = pubmedIds.size();
      double counter = 0;
      for (Integer id : pubmedIds) {
        try {
          Article art = backend.getPubmedArticleById(id);
          if (art == null) {
            logger.warn("Cannot find pubmed article. Pubmed_id = " + id);
          }
        } catch (PubmedSearchException e) {
          logger.warn("Problem with accessing info about pubmed: " + id, e);
        }
        counter++;
        updater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
      }
    }
    logger.debug("Caching finished");
  }

  @Override
  public void removeModelFromCache(Model model) {
    models.remove(model.getProject().getProjectId());

  }

  @Override
  public void cacheAllMiriamLinks(Model model, IProgressUpdater updater) {
    logger.debug("Caching miriam ids...");
    if (model != null) {
      Set<MiriamData> pubmedIds = new HashSet<MiriamData>();

      for (Element element : model.getElements()) {
        pubmedIds.addAll(element.getMiriamData());
      }
      for (Reaction reaction : model.getReactions()) {
        pubmedIds.addAll(reaction.getMiriamData());
      }
      double amount = pubmedIds.size();
      double counter = 0;
      for (MiriamData md : pubmedIds) {
        miriamConnector.getUrlString(md);
        counter++;
        updater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
      }
    }
    logger.debug("Caching finished");

  }

  @Override
  public void removeModelFromCache(ModelData model) {
    models.remove(model.getProject().getProjectId());
  }

  @Override
  public void removeModelFromCacheByProjectId(String projectId) {
    models.remove(projectId);
  }

  @Override
  public void updateModel(ModelData model) {
    Project project = model.getProject();
    if (project == null) {
      for (SubmodelConnection sc : model.getParentModels()) {
        if (sc instanceof ModelSubmodelConnection) {
          project = ((ModelSubmodelConnection) sc).getParentModel().getModel().getProject();
          if (project != null) {
            break;
          }
        }
      }
    }

    Model topCachedData = getLastModelByProjectId(project.getProjectId());
    Model cachedData = topCachedData.getSubmodelById(model.getId());
    cachedData.setDefaultCenterX(model.getDefaultCenterX());
    cachedData.setDefaultCenterY(model.getDefaultCenterY());
    cachedData.setDefaultZoomLevel(model.getDefaultZoomLevel());
    modelDao.update(model);

  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the projectDao
   * @see #projectDao
   */
  public ProjectDao getProjectDao() {
    return projectDao;
  }

  /**
   * @param projectDao
   *          the projectDao to set
   * @see #projectDao
   */
  public void setProjectDao(ProjectDao projectDao) {
    this.projectDao = projectDao;
  }

}
