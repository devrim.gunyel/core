package lcsb.mapviewer.services.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.genome.*;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import lcsb.mapviewer.services.utils.ReferenceGenomeExistsException;

/**
 * Service managing reference genomes.
 * 
 * @author Piotr Gawron
 *
 */
@Transactional
@Service
public class ReferenceGenomeService implements IReferenceGenomeService {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ReferenceGenomeService.class);

  /**
   * Class responsible for connection to {@link ReferenceGenomeType#UCSC}
   * database.
   */
  private UcscReferenceGenomeConnector ucscReferenceGenomeConnector;

  /**
   * Data access object for {@link ReferenceGenome} objects.
   */
  private ReferenceGenomeDao referenceGenomeDao;

  @Autowired
  public ReferenceGenomeService(UcscReferenceGenomeConnector ucscReferenceGenomeConnector,
      ReferenceGenomeDao referenceGenomeDao) {
    this.ucscReferenceGenomeConnector = ucscReferenceGenomeConnector;
    this.referenceGenomeDao = referenceGenomeDao;
  }

  @Override
  public void addReferenceGenome(ReferenceGenomeType type, MiriamData organism, String version, String customUrl)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    for (ReferenceGenome genome : getDownloadedGenomes()) {
      if (genome.getType().equals(type) && genome.getOrganism().equals(organism)
          && genome.getVersion().equals(version)) {
        throw new ReferenceGenomeExistsException("Selected reference genome already downloaded");
      }
    }
    getReferenceGenomeConnector(type).downloadGenomeVersion(organism, version, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    }, true, customUrl);
  }

  @Override
  public List<MiriamData> getOrganismsByReferenceGenomeType(ReferenceGenomeType type)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableOrganisms();
  }

  @Override
  public List<String> getAvailableGenomeVersions(ReferenceGenomeType type, MiriamData organism)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableGenomeVersion(organism);
  }

  @Override
  public String getUrlForGenomeVersion(ReferenceGenomeType type, MiriamData organism, String version) {
    try {
      return getReferenceGenomeConnector(type).getGenomeVersionFile(organism, version);
    } catch (FileNotAvailableException e) {
      return null;
    }
  }

  @Override
  public List<ReferenceGenome> getDownloadedGenomes() {
    return referenceGenomeDao.getAll();
  }

  @Override
  public void removeGenome(ReferenceGenome genome) throws IOException {
    getReferenceGenomeConnector(genome.getType()).removeGenomeVersion(genome.getOrganism(), genome.getVersion());
  }

  @Override
  public void addReferenceGenomeGeneMapping(ReferenceGenome referenceGenome, String name, String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    getReferenceGenomeConnector(referenceGenome.getType()).downloadGeneMappingGenomeVersion(referenceGenome, name,
        new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, true, url);

  }

  @Override
  public void removeReferenceGenomeGeneMapping(ReferenceGenomeGeneMapping genome) throws IOException {
    getReferenceGenomeConnector(genome.getReferenceGenome().getType()).removeGeneMapping(genome);
  }

  @Override
  public ReferenceGenome getReferenceGenomeViewByParams(
      MiriamData miriamData, ReferenceGenomeType genomeType, String version) {
    List<ReferenceGenome> list = referenceGenomeDao.getByType(genomeType);
    for (ReferenceGenome referenceGenome : list) {
      if (referenceGenome.getOrganism().equals(miriamData) && referenceGenome.getVersion().equals(version)) {
        return referenceGenome;
      }
    }
    return null;
  }

  @Override
  public ReferenceGenome getReferenceGenomeById(int id) {
    return referenceGenomeDao.getById(id);
  }

  /**
   * Return {@link ReferenceGenomeConnector} implementation for given reference
   * genome type.
   *
   * @param type
   *          type of reference genome
   * @return {@link ReferenceGenomeConnector} implementation for given reference
   *         genome type
   */
  private ReferenceGenomeConnector getReferenceGenomeConnector(ReferenceGenomeType type) {
    if (type == ReferenceGenomeType.UCSC) {
      return ucscReferenceGenomeConnector;
    } else {
      throw new InvalidArgumentException("Unknown reference genome type: " + type);
    }
  }

}
