package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IFileService;

@Transactional
@Service
public class FileService implements IFileService {

  private UploadedFileEntryDao uploadedFileEntryDao;

  @Autowired
  public FileService(UploadedFileEntryDao uploadedFileEntryDao) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
  }

  @Override
  public UploadedFileEntry getById(Integer id) {
    return uploadedFileEntryDao.getById(id);
  }

  @Override
  public User getOwnerByFileId(Integer id) {
    if (id != null) {
      UploadedFileEntry entry = uploadedFileEntryDao.getById(id);
      if (entry != null && entry.getOwner() != null) {
        // it's lazy initialized
        entry.getOwner().getLogin();
        return uploadedFileEntryDao.getById(id).getOwner();
      }
    }
    return null;

  }

}
