package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.interfaces.IMiriamService;

/**
 * Implementation of service responsible for accessing miriam registry.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class MiriamService implements IMiriamService {

  /**
   * Object accessing <a href= "http://www.ebi.ac.uk/miriam/main/" >miriam
   * registry</a>.
   */
  private MiriamConnector miriamConnector;

  @Autowired
  public MiriamService(MiriamConnector miriamConnector) {
    this.miriamConnector = miriamConnector;
  }

  @Override
  public String getUrlForMiriamData(MiriamData md) {
    return miriamConnector.getUrlString(md);
  }

  @Override
  public MiriamType getTypeForUri(String uri) {
    return MiriamType.getTypeByUri(uri);
  }

  /**
   * @return the miriamConnector
   * @see #miriamConnector
   */
  public MiriamConnector getMiriamConnector() {
    return miriamConnector;
  }

  /**
   * @param miriamConnector
   *          the miriamConnector to set
   * @see #miriamConnector
   */
  public void setMiriamConnector(MiriamConnector miriamConnector) {
    this.miriamConnector = miriamConnector;
  }

}
