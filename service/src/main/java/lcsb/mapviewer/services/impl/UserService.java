package lcsb.mapviewer.services.impl;

import java.awt.*;
import java.util.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.*;

@Transactional
@Service
public class UserService implements IUserService {

  private static Logger logger = LogManager.getLogger(UserService.class);
  private UserDao userDao;
  private ILdapService ldapService;
  private IConfigurationService configurationService;
  private IPrivilegeService privilegeService;
  private ProjectDao projectDao;

  @Autowired
  public UserService(UserDao userDao,
      ILdapService ldapService,
      IConfigurationService configurationService,
      IPrivilegeService privilegeService,
      ProjectDao projectDao) {
    this.userDao = userDao;
    this.ldapService = ldapService;
    this.configurationService = configurationService;
    this.privilegeService = privilegeService;
    this.projectDao = projectDao;
  }

  @Override
  public void addUser(User user) {
    userDao.add(user);
    logger.info("User " + user.getLogin() + " created successfully");
  }

  @Override
  public void updateUser(User user) {
    userDao.update(user);
  }

  @Override
  public void deleteUser(User user) {
    String login = user.getLogin();
    userDao.delete(user);
    logger.info("User " + login + " removed successfully");
  }

  @Override
  public User getUserByLogin(String login) {
    return userDao.getUserByLogin(login);
  }

  @Override
  public List<User> getUsers() {
    return userDao.getAll();
  }

  @Override
  public void grantUserPrivilege(User user, PrivilegeType type) {
    user = getUserByLogin(user.getLogin());
    user.addPrivilege(privilegeService.getPrivilege(type));
    userDao.update(user);
  }

  @Override
  public void grantUserPrivilege(User user, PrivilegeType type, String objectId) {
    user = getUserByLogin(user.getLogin());
    user.addPrivilege(privilegeService.getPrivilege(type, objectId));
    userDao.update(user);
  }

  @Override
  public void revokeUserPrivilege(User user, PrivilegeType type) {
    user = getUserByLogin(user.getLogin());
    user.removePrivilege(privilegeService.getPrivilege(type));
    userDao.update(user);
  }

  @Override
  public void revokeUserPrivilege(User user, PrivilegeType type, String objectId) {
    user = getUserByLogin(user.getLogin());
    user.removePrivilege(privilegeService.getPrivilege(type, objectId));
    userDao.update(user);
  }

  @Override
  public void grantDefaultPrivileges(User user) {
    Arrays.stream(ConfigurationElementType.values())
        .filter(type -> type.getGroup() == ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES)
        .filter(type -> Boolean.valueOf(configurationService.getConfigurationValue(type)))
        .forEach(type -> grantPrivilegesForDefaultConfigurationElementType(user, type));
  }

  @Override
  public void grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType type, String objectId) {
    getUsers().stream()
        .filter(u -> u.getPrivileges().contains(new Privilege(type, "*")))
        .forEach(u -> grantUserPrivilege(u, type, objectId));
  }

  @Override
  public void revokeObjectDomainPrivilegesForAllUsers(PrivilegeType privilegeType, String objectId) {
    userDao.getAll().forEach(user -> revokeUserPrivilege(user, privilegeType, objectId));
  }

  @Override
  public ColorExtractor getColorExtractorForUser(User loggedUser) {
    Color colorMin = null;
    Color colorMax = null;
    Color colorSimple = null;
    if (loggedUser != null) {
      User dbUser = getUserByLogin(loggedUser.getLogin());
      if (dbUser != null) {
        colorMin = dbUser.getMinColor();
        colorMax = dbUser.getMaxColor();
        colorSimple = dbUser.getSimpleColor();
      }
    }
    ColorParser parser = new ColorParser();

    if (colorMin == null) {
      colorMin = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MIN_COLOR_VAL));
    }
    if (colorMax == null) {
      colorMax = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MAX_COLOR_VAL));
    }
    if (colorSimple == null) {
      colorSimple = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.SIMPLE_COLOR_VAL));
    }
    return new ColorExtractor(colorMin, colorMax, colorSimple);
  }

  @Override
  public Map<String, Boolean> ldapAccountExistsForLogin(Collection<User> logins) {
    Map<String, Boolean> result = new HashMap<>();
    for (User user : logins) {
      result.put(user.getLogin(), false);
    }

    if (ldapService.isValidConfiguration()) {
      try {
        List<String> ldapUserNames = ldapService.getUsernames();
        for (String string : ldapUserNames) {
          String ldapLogin = string.toLowerCase();
          if (result.keySet().contains(ldapLogin)) {
            result.put(ldapLogin, true);
          }
        }
      } catch (LDAPException e) {
        logger.error("Problem with accessing LDAP directory", e);
      }
    }
    return result;
  }

  private void grantPrivilegesForDefaultConfigurationElementType(User user, ConfigurationElementType type) {
    switch (type) {
    case DEFAULT_CAN_CREATE_OVERLAYS:
      grantUserPrivilege(user, PrivilegeType.CAN_CREATE_OVERLAYS);
      break;
    case DEFAULT_READ_PROJECT:
      grantUserPrivilege(user, PrivilegeType.READ_PROJECT, "*");
      projectDao.getAll()
          .forEach(project -> grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId()));
      break;
    case DEFAULT_WRITE_PROJECT:
      grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "*");
      projectDao.getAll()
          .forEach(project -> grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, project.getProjectId()));
      break;
    default:
      throw new IllegalArgumentException("The configuration element type '+" + type + "' is not handled.");
    }
  }

}
