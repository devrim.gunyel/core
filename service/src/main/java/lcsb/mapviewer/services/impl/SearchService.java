package lcsb.mapviewer.services.impl;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.statistics.SearchType;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.services.interfaces.ISearchHistoryService;
import lcsb.mapviewer.services.interfaces.ISearchService;
import lcsb.mapviewer.services.utils.SearchIndexer;

/**
 * This is implementation of the service that returns information about species,
 * paths etc in the model.
 *
 *
 * @author Piotr Gawron
 *
 */
@Transactional
@Service
public class SearchService implements ISearchService {

  /**
   * Prefix used in search by name interface to limit results only to species.
   */
  public static final String SPECIES_SEARCH_PREFIX = "species";

  /**
   * Prefix used in search by name interface to limit results only to reactions.
   */
  public static final String REACTION_SEARCH_PREFIX = "reaction";

  /**
   * Prefix used in search by name interface to limit results only to
   * {@link Element}.
   */
  private static final String ELEMENT_SEARCH_PREFIX = "element";

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SearchService.class.getName());

  /**
   * Data access object for models.
   */
  private ModelDao modelDao;

  /**
   * Service used for managing search history.
   */
  private ISearchHistoryService searchHistoryService;

  /**
   * Object used for indexing elements on the map.
   */
  private SearchIndexer searchIndexer = new SearchIndexer();

  /**
   * This object maps class of elements on the map into short string prefixes used
   * in the search query. For instance elements of the {@link Complex} class could
   * be searched with the prefix "complex" (example query: "complex:alpha
   * subcomplex").
   */
  private Map<Class<? extends Element>, String> speciesSearchPrefix = new HashMap<>();

  /**
   * This object maps short string prefixes into class of elements on the map that
   * will be searched. For instance prefix "complex" might refer to
   * {@link Complex} class and and example query would look like: "complex:alpha
   * subcomplex".
   */
  private Map<String, Class<? extends Element>> speciesSearchReversePrefix = new HashMap<>();
  /**
   * Object containing autocomplete lists for models. For every model the map
   * between String and List is stored. The key in this map is incomplete typed
   * word, and value list contains suggestions that should appear for this word.
   */
  private Map<Model, Map<String, List<String>>> autoCompleteLists = new HashMap<Model, Map<String, List<String>>>();

  /**
   * Default constructor that set list of prefixes used in the search queries.
   */
  public SearchService(ModelDao modelDao, ISearchHistoryService searchHistoryService) {
    this.modelDao = modelDao;
    this.searchHistoryService = searchHistoryService;
    addSearchPrefix("complex", Complex.class);
    addSearchPrefix("degrded", Degraded.class);
    addSearchPrefix("drug", Drug.class);
    addSearchPrefix("gene", Gene.class);
    addSearchPrefix("ion", Ion.class);
    addSearchPrefix("phenotype", Phenotype.class);
    addSearchPrefix("protein", Protein.class);
    addSearchPrefix("rna", Rna.class);
    addSearchPrefix("molecule", SimpleMolecule.class);
    addSearchPrefix("unknown", Unknown.class);
  }

    /**
   * Adds search prefix for an element class.
   *
   * @param prefix
   *          string prefix used in search queries
   * @param clazz
   *          class for which this prefix stands
   */
  private void addSearchPrefix(String prefix, Class<? extends Element> clazz) {
    speciesSearchPrefix.put(clazz, prefix);
    speciesSearchReversePrefix.put(prefix, clazz);
  };

  /**
   * Return the list of elements that match the query (sorted by match score)
   * limited to the element of class defined by type.
   *
   * @param model
   *          model where the search is performed
   * @param query
   *          query string
   * @param limit
   *          maximum number of elements that can be returned by the search engine
   * @param perfectMatch
   *          should only perfect match be allowed
   * @param type
   *          class type of the elements that limits the results
   * @return list of object found for the query string sorted by the score value
   */
  protected List<BioEntity> searchByIndexedQuery(Model model, String query, int limit, Boolean perfectMatch,
      Class<? extends Element> type) {
    List<BioEntity> result = new ArrayList<>();
    if (query.startsWith(REACTION_SEARCH_PREFIX)) {
      return getReactionById(model, query.replaceFirst(REACTION_SEARCH_PREFIX, "").toLowerCase());
    } else if (query.startsWith(SPECIES_SEARCH_PREFIX)) {
      Element element = model.getElementByElementId(query.replaceFirst(SPECIES_SEARCH_PREFIX, ""));
      if (element != null) {
        result.add(element);
      }
    } else if (query.startsWith(ELEMENT_SEARCH_PREFIX)) {
      return getElementById(model, query.replaceFirst(ELEMENT_SEARCH_PREFIX, "").toLowerCase());
    } else {
      Set<Element> aliases = model.getElements();

      List<SearchResult> sortedResults = new ArrayList<>();
      for (Element alias : aliases) {
        if (type.isAssignableFrom(alias.getClass())) {
          List<SearchIndex> indexes = alias.getSearchIndexes();
          if (indexes.size() == 0) {
            indexes = searchIndexer.createIndexForAlias(alias);
            alias.setSearchIndexes(indexes);
          }
          for (SearchIndex searchIndex : indexes) {
            double score = searchIndexer.match(query, searchIndex);
            if (perfectMatch != null && perfectMatch) {
              if (query.equals(searchIndex.getValue())) {
                SearchResult sResult = new SearchResult();
                sResult.setObj(alias);
                sResult.setScore(1);
                sortedResults.add(sResult);
                break;
              }
            } else if (score > 0 || (query.equals("") && type != Element.class)) {
              SearchResult sResult = new SearchResult();
              sResult.setObj(alias);
              sResult.setScore(score);
              if (score == 0) {
                sResult.setScore(alias.getId());
              }
              sortedResults.add(sResult);
              break;
            }
          }
        }
        if (result.size() >= limit) {
          break;

        }
      }
      Collections.sort(sortedResults);
      for (SearchResult searchResult : sortedResults) {
        if (searchResult.getObj() instanceof Reaction) {
          result.add(searchResult.getObj());
        } else if (searchResult.getObj() instanceof Element) {
          result.add(searchResult.getObj());
        } else {
          throw new InvalidStateException("Unknown class type: " + searchResult.getObj().getClass());
        }
        if (result.size() >= limit) {
          break;
        }
      }
    }
    return result;
  }

  /**
   * Returns list with the reaction with a given id. If reaction with such id
   * doesn't exist then empty list is returned.
   *
   * @param model
   *          where the search is performed
   * @param reactionId
   *          id of the reaction
   * @return list that contains reaction with given id (or empty list if such
   *         reaction doesn't exist)
   */
  private List<BioEntity> getReactionById(Model model, String reactionId) {
    Set<Reaction> reactions = model.getReactions();
    for (Reaction reaction : reactions) {
      if (searchIndexer.getQueryStringForIndex(reaction.getIdReaction().toLowerCase(), new ArrayList<>())
          .equals(reactionId)) {
        return reactionToResultList(reaction);
      }
      if (Integer.toString(reaction.getId()).equals(reactionId)) {
        return reactionToResultList(reaction);
      }
    }
    return new ArrayList<>();
  }

  /**
   * Returns list with the element with a given id. If element with such id
   * doesn't exist then empty list is returned.
   *
   * @param topModel
   *          where the search is performed
   * @param elementId
   *          id of the element
   * @return list that contains element with given id (or empty list if such
   *         element doesn't exist)
   */
  private List<BioEntity> getElementById(Model topModel, String elementId) {
    List<BioEntity> result = new ArrayList<>();

    Set<Model> models = new HashSet<>();
    models.add(topModel);
    models.addAll(topModel.getSubmodels());
    for (Model model : models) {
      for (Element element : model.getElements()) {
        if (searchIndexer.getQueryStringForIndex(element.getElementId().toLowerCase(), new ArrayList<>())
            .equals(elementId)) {
          result.add(element);
        } else if (Integer.toString(element.getId()).equals(elementId)) {
          result.add(element);
        }
      }
    }
    return result;
  }

  /**
   * Transform {@link Reaction} into set of result entries.
   *
   * @param reaction
   *          reaction to be transformed
   * @return set of result entries for all element of the {@link Reaction}
   */
  private List<BioEntity> reactionToResultList(Reaction reaction) {
    List<BioEntity> result = new ArrayList<>();
    result.add(reaction);
    for (ReactionNode node : reaction.getReactionNodes()) {
      result.add(node.getElement());
    }
    return result;
  }

  @Override
  public List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch,
      String ipAddress) {
    query = query.toLowerCase();
    if (query == null) {
      throw new InvalidArgumentException("Invalid query: null");
    }

    MiriamData mt = getMiriamTypeForQuery(query);
    List<BioEntity> result = new ArrayList<>();
    if (mt != null) {
      result.addAll(searchByMiriam(model, mt, limit));
    } else {
      String indexQuery = searchIndexer.getQueryStringForIndex(query, speciesSearchReversePrefix.keySet());
      Class<? extends Element> type = searchIndexer.getTypeForQuery(query, speciesSearchReversePrefix);

      List<BioEntity> partResult = searchByIndexedQuery(model, indexQuery, limit, perfectMatch, type);

      for (ModelSubmodelConnection connection : model.getSubmodelConnections()) {
        partResult.addAll(searchByIndexedQuery(connection.getSubmodel().getModel(), indexQuery,
            limit - partResult.size(), perfectMatch, type));
        if (partResult.size() >= limit) {
          break;
        }
      }

      result.addAll(partResult);
    }
    if (model.getProject() != null && ipAddress != null) {
      searchHistoryService.addQuery(query, SearchType.GENERAL, ipAddress, model.getProject().getProjectId());
    }
    return result;
  }

  @Override
  public List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch) {
    return searchByQuery(model, query, limit, perfectMatch, null);
  }

  @Override
  public List<BioEntity> getClosestElements(Model model, Point2D point, int numberOfElements, boolean perfectHit,
      Collection<String> types) {
    List<BioEntity> result = new ArrayList<>();

    // probably this could be improved algorithmically, right now all objects
    // are sorted by distance, and numberOfElements closest are chosen as a list
    // of results
    List<DistanceToObject> tmpList = new ArrayList<>();
    for (Reaction reaction : model.getReactions()) {
      if (types.size() == 0 || types.contains(reaction.getStringType().toLowerCase())) {
        tmpList.add(new DistanceToObject(reaction, point));
      }
    }
    for (Element element : model.getElements()) {
      if ((element instanceof Species && types.size() == 0) || types.contains(element.getStringType().toLowerCase())) {
        tmpList.add(new DistanceToObject(element, point));
      }
    }
    Collections.sort(tmpList);
    int size = Math.min(tmpList.size(), numberOfElements);
    for (int i = 0; i < size; i++) {
      if (!perfectHit) {
        result.add(tmpList.get(i).getReference());
      } else if (tmpList.get(i).getDistance() < Configuration.EPSILON) {
        result.add(tmpList.get(i).getReference());
      }
    }
    return result;
  }

  @Override
  public List<String> getAutocompleteList(Model model, String query) {
    Map<String, List<String>> autoCompleteMap = autoCompleteLists.get(model);
    if (autoCompleteMap == null) {
      autoCompleteMap = createAutocompleteMap(model);
      autoCompleteLists.put(model, autoCompleteMap);
    }
    List<String> result = autoCompleteMap.get(query.toLowerCase().trim());
    if (result == null) {
      result = new ArrayList<String>();
    }
    return result;
  }

  @Override
  public String[] getSuggestedQueryList(Model model) {
    Set<String> possibilities = new HashSet<String>();
    for (Element alias : model.getElements()) {
      if (alias instanceof Species) {
        possibilities.addAll(getSearchPossibilitiesForAlias(alias));
      }
    }
    for (ModelSubmodelConnection connection : model.getSubmodelConnections()) {
      for (Element alias : connection.getSubmodel().getModel().getElements()) {
        if (alias instanceof Species) {
          possibilities.addAll(getSearchPossibilitiesForAlias(alias));
        }
      }
    }
    String[] sortedPossibilites = new String[possibilities.size()];
    int index = 0;
    for (String string : possibilities) {
      sortedPossibilites[index++] = string;
    }
    Arrays.sort(sortedPossibilites);
    return sortedPossibilites;
  }

  /**
   * Returns elements that are annotated with the given miriam data.
   *
   * @param topModel
   *          model where elements are looked for
   * @param md
   *          miriam annotation to identify interesting elements
   * @param limit
   *          max number of elements to find
   * @return elements that are annotated with the given miriam data
   */
  private List<BioEntity> searchByMiriam(Model topModel, MiriamData md, int limit) {
    List<BioEntity> result = new ArrayList<>();
    Set<Model> models = new HashSet<>();
    models.add(topModel);
    models.addAll(topModel.getSubmodels());
    List<BioEntity> elements = new ArrayList<>();
    for (Model model : models) {
      elements.addAll(model.getElementsByAnnotation(md));
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    for (BioEntity obj : elements) {
      if (result.size() >= limit) {
        break;
      }
      if (obj instanceof Element) {
        result.add((Element) obj);
      } else if (obj instanceof Reaction) {
        result.addAll(reactionToResultList((Reaction) obj));
      } else {
        throw new InvalidClassException("Unknown class: " + obj.getClass());
      }
    }
    return result;
  }

  /**
   * Tries to transform query into {@link MiriamData}.
   *
   * @param string
   *          query to transform
   * @return {@link MiriamData} that described query or null if query cannot be
   *         converted
   */
  protected MiriamData getMiriamTypeForQuery(String string) {
    for (MiriamType mt : MiriamType.values()) {
      if (string.startsWith(mt.toString().toLowerCase() + ":")) {
        return new MiriamData(mt, string.substring(mt.toString().length() + 1));
      }
    }
    return null;
  }

  /**
   * Creates autocomplete map for a model. The key in this map is incomplete typed
   * word, and value list contains suggestions that should appear for this word.
   *
   * @param model
   *          model for which the autocomplete map is created
   * @return autocomplete map for a model. The key in this map is incomplete typed
   *         word, and value list contains suggestions that should appear for this
   *         word.
   */
  private Map<String, List<String>> createAutocompleteMap(Model model) {
    Map<String, List<String>> result = new HashMap<String, List<String>>();

    String[] sortedPossibilites = getSuggestedQueryList(model);
    for (int i = 0; i < sortedPossibilites.length; i++) {
      String mainString = sortedPossibilites[i];
      for (int j = 0; j < mainString.length(); j++) {
        String substring = mainString.substring(0, j + 1);
        if (result.get(substring) != null) {
          continue;
        }

        List<String> list = new ArrayList<String>();
        for (int k = 0; k < Configuration.getAutocompleteSize(); k++) {
          if (k + i >= sortedPossibilites.length) {
            break;
          } else if (sortedPossibilites[k + i].startsWith(substring)) {
            list.add(sortedPossibilites[k + i]);
          }
        }

        // by default, the selection in autocomplete is set on the first
        // element, so if the first element is not equals to the query then add
        // the element
        if (!list.get(0).equalsIgnoreCase(substring)) {
          list.add(0, substring);
        }

        result.put(substring, list);
      }
    }
    return result;
  }

  /**
   * Returns the list of all human readable search possibilities for alias.
   *
   * @param element
   *          object for which we look for a list of searchable strings
   * @return the list of all human readable search possibilities for alias
   */
  private List<String> getSearchPossibilitiesForAlias(Element element) {
    List<String> result = new ArrayList<String>();
    result.add(element.getName().trim().toLowerCase().replace(",", ""));
    for (String string : element.getSynonyms()) {
      result.add(string.trim().toLowerCase().replace(",", ""));
    }
    for (String string : element.getFormerSymbols()) {
      result.add(string.trim().toLowerCase().replace(",", ""));
    }
    String name = element.getFullName();
    if (name != null) {
      result.add(name.trim().toLowerCase().replace(",", ""));
    }
    return result;
  }

  /**
   * @return the modelDao
   */
  public ModelDao getModelDao() {
    return modelDao;
  }

  /**
   * @param modelDao
   *          the modelDao to set
   */
  public void setModelDao(ModelDao modelDao) {
    this.modelDao = modelDao;
  }

  /**
   * @return the searchHistoryService
   */
  public ISearchHistoryService getSearchHistoryService() {
    return searchHistoryService;
  }

  /**
   * @param searchHistoryService
   *          the searchHistoryService to set
   */
  public void setSearchHistoryService(ISearchHistoryService searchHistoryService) {
    this.searchHistoryService = searchHistoryService;
  }

/**
   * Private class that defines internal search result element. Contains reference
   * to original result and match score of the result.
   *
   * @author Piotr Gawron
   *
   */
  private class SearchResult implements Comparable<SearchResult> {
    /**
     * Reference to the original object.
     */
    private BioEntity obj;

    /**
     * Score of the result.
     */
    private double score;

    @Override
    public int compareTo(SearchResult o) {
      if (score == o.getScore()) {
        return obj.getId() - o.getObj().getId();
      } else if (score < o.getScore()) {
        return 1;
      } else {
        return -1;
      }
    }

    /**
     * @return the obj
     */
    public BioEntity getObj() {
      return obj;
    }

    /**
     * @param obj
     *          the obj to set
     */
    public void setObj(BioEntity obj) {
      this.obj = obj;
    }

    /**
     * @return the score
     */
    public double getScore() {
      return score;
    }

    /**
     * @param score
     *          the score to set
     */
    public void setScore(double score) {
      this.score = score;
    }
  }

  /**
   * This class represents distance between object and some point. It's designed
   * to help sort objects by their distance to some point. It It contains two
   * fields: object reference and distance.
   *
   * @author Piotr Gawron
   *
   */
  private class DistanceToObject implements Comparable<DistanceToObject> {
    /**
     * Reference to the object.
     */
    private BioEntity reference;

    /**
     * Distance between the object and some point.
     */
    private double distance;

    /**
     * Constructor for reaction objects.
     *
     * @param reaction
     *          reaction reference to store
     * @param point
     *          point from which the distance will be computed
     */
    DistanceToObject(Reaction reaction, Point2D point) {
      reference = reaction;
      distance = reaction.getDistanceFromPoint(point);
    }

    /**
     * Constructor for alias objects.
     *
     * @param alias
     *          alias reference to store
     * @param point
     *          point from which the distance will be computed
     */
    DistanceToObject(Element alias, Point2D point) {
      reference = alias;
      distance = alias.getDistanceFromPoint(point);
    }

    @Override
    public int compareTo(DistanceToObject arg0) {
      if (arg0.getDistance() < getDistance()) {
        return 1;
      } else if (arg0.getDistance() > getDistance()) {
        return -1;
      } else {
        if (arg0.getReference().getZ() > getReference().getZ()) {
          return 1;
        } else if (arg0.getReference().getZ() < getReference().getZ()) {
          return -1;
        } else {
          return 0;
        }
      }
    }

    /**
     * @return the reference
     * @see #reference
     */
    public BioEntity getReference() {
      return reference;
    }

    /**
     * @return the distance
     * @see #distance
     */
    public double getDistance() {
      return distance;
    }

  }

}