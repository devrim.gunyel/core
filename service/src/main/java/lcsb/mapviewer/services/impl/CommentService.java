package lcsb.mapviewer.services.impl;

import java.awt.geom.Point2D;
import java.util.*;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.ObjectUtils;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.services.interfaces.*;
import lcsb.mapviewer.services.utils.EmailSender;

/**
 * This class is responsible for all services connected with comments
 * functionality.
 *
 * @author Piotr Gawron
 *
 */

@Transactional
@Service
public class CommentService implements ICommentService {

  /**
   * Max distance between two points on map for which two comments should be
   * considered as referring to the same point.
   */
  private static final double COMMENT_POINT_DISTANCE_EPSILON = 0.02;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(CommentService.class);

  /**
   * Data access object fir comments.
   */
  private CommentDao commentDao;

  /**
   * Service for operations on Model object.
   */
  private IModelService modelService;

  /**
   * Service for operations on User object.
   */
  private IUserService userService;

  /**
   * Service for operations on Configuration object.
   */
  private IConfigurationService configurationService;

  @Autowired
  public CommentService(CommentDao commentDao,
      IModelService modelService,
      IUserService userService,
      IConfigurationService configurationService) {
    this.commentDao = commentDao;
    this.modelService = modelService;
    this.userService = userService;
    this.configurationService = configurationService;
  }

  @Override
  public Comment addComment(String name, String email, String content, Point2D coordinates, Object object,
      boolean pinned, Model submodel, User owner) {
    Comment comment = new Comment();
    comment.setName(name);
    comment.setEmail(email);
    comment.setContent(content);
    comment.setModel(submodel);
    comment.setCoordinates(coordinates);
    if (object != null) {
      comment.setTableName(object.getClass());
      comment.setTableId(ObjectUtils.getIdOfObject(object));
    }
    comment.setUser(owner);
    comment.setPinned(pinned);
    commentDao.add(comment);

    try {
      Project project = submodel.getProject();
      if (project == null) {
        for (SubmodelConnection sc : submodel.getParentModels()) {
          if (sc instanceof ModelSubmodelConnection) {
            project = ((ModelSubmodelConnection) sc).getParentModel().getModel().getProject();
            if (project != null) {
              break;
            }
          }
        }
      }

      EmailSender emailSender = new EmailSender(configurationService);
      emailSender.sendEmail("New comment appeard in the " + project.getProjectId(), content, project.getNotifyEmail());
    } catch (MessagingException e) {
      logger.error("Problem with sending email.", e);
    }

    return comment;
  }

  @Override
  public void deleteComment(User loggedUser, String commentId, String reason) {
    int id = -1;
    try {
      id = Integer.parseInt(commentId);

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    Comment comment = commentDao.getById(id);
    if (comment == null) {
      logger.error("Invalid comment id: " + commentId);
    } else {
      comment.setDeleted(true);
      comment.setRemoveReason(reason);
      commentDao.update(comment);
    }
  }

  @Override
  public void deleteComment(Comment comment, String reason) {
    comment.setDeleted(true);
    comment.setRemoveReason(reason);
    commentDao.update(comment);
  }

  @Override
  public List<Comment> getCommentsByProject(Project project) {
    List<Comment> comments = new ArrayList<>();
    for (ModelData model : project.getModels()) {
      List<Comment> modelComments = commentDao.getCommentByModel(model, null, null);
      comments.addAll(modelComments);
    }

    return comments;
  }

  @Override
  public long getCommentCount() {
    return commentDao.getCount();
  }

  @Override
  public void removeCommentsForModel(Model model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  @Override
  public void removeCommentsForModel(ModelData model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  @Override
  public Comment getCommentById(String commentId) {
    int id = -1;
    try {
      id = Integer.parseInt(commentId);
    } catch (NumberFormatException e) {
    }
    return commentDao.getById(id);
  }

  /**
   * This method returns all comments for model agregated by commented elements.
   *
   * @param model
   *          object to which we are looking for comments
   * @param pinned
   *          this parameter defines what kind of comments we want to see:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not visible on the map</li>
   *          <li>true - comments that are visible on the map</li>
   *          </ul>
   * @return list of comments grouped by commented elements
   */
  protected List<List<Comment>> getAgregatedComments(Model model, Boolean pinned) {
    List<Comment> comments = commentDao.getCommentByModel(model, pinned, false);
    List<List<Comment>> result = new ArrayList<>();

    Map<String, List<Comment>> objects = new HashMap<>();
    for (Comment comment : comments) {
      if (comment.getTableName() == null) {
        List<Comment> list = new ArrayList<>();
        list.add(comment);
        result.add(list);
      } else {
        String id = comment.getTableId() + "," + comment.getTableName();

        List<Comment> list = objects.get(id);
        if (list == null) {
          list = new ArrayList<>();
          objects.put(id, list);
          result.add(list);
        }
        list.add(comment);
      }
    }
    return result;
  }

  /**
   * @return the commentDao
   * @see #commentDao
   */
  public CommentDao getCommentDao() {
    return commentDao;
  }

  /**
   * @param commentDao
   *          the commentDao to set
   * @see #commentDao
   */
  public void setCommentDao(CommentDao commentDao) {
    this.commentDao = commentDao;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService
   *          the modelService to set
   * @see #modelService
   */
  public void setModelService(IModelService modelService) {
    this.modelService = modelService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * Checks if identifier of the points refer to the same point.
   *
   * @param uniqueId
   *          identifier assigned by the java code
   * @param objectId
   *          identifier assigned by the javascript code (browser)
   * @return <code>true</code> if both identifiers refer to the same point,
   *         <code>false</code> otherwise
   */
  protected boolean equalPoints(String uniqueId, String objectId) {
    String tmp = uniqueId.substring(uniqueId.indexOf("[") + 1, uniqueId.indexOf("]"));
    String[] ids = tmp.split(",");
    Double x1 = Double.valueOf(ids[0].trim());
    Double y1 = Double.valueOf(ids[1].trim());

    tmp = objectId.substring(objectId.indexOf("(") + 1, objectId.indexOf(")"));
    ids = tmp.split(",");
    Double x2 = Double.valueOf(ids[0].trim());
    Double y2 = Double.valueOf(ids[1].trim());

    return !(Math.abs(x1 - x2) > COMMENT_POINT_DISTANCE_EPSILON)
        && !(Math.abs(y1 - y2) > COMMENT_POINT_DISTANCE_EPSILON);
  }

  @Override
  public User getOwnerByCommentId(String commentId) {
    Comment comment = getCommentById(commentId);
    if (comment == null || comment.getUser() == null) {
      return null;
    }
    // fetch lazy data
    comment.getUser().getLogin();
    return comment.getUser();
  }

}
