package lcsb.mapviewer.services.impl;

import java.awt.Color;
import java.io.IOException;
import java.util.*;
import java.util.Comparator;

import javax.mail.MessagingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.common.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.interfaces.*;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

/**
 * Implementation of the layout service.
 *
 * @author Piotr Gawron
 *
 */
@Transactional
@Service
public class LayoutService implements ILayoutService {

  private static final Comparator<? super Pair<? extends BioEntity, ColorSchema>> ELEMENT_PAIR_COMPARATOR = Comparator
      .comparingInt(o -> o.getLeft().getId());

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(LayoutService.class);

  /**
   * Layout data access object.
   */
  private LayoutDao layoutDao;

  /**
   * Service that manages and gives access to user information.
   */
  private IUserService userService;

  /**
   * Service that manages and gives access to configuration parameters.
   */
  private IConfigurationService configurationService;

  /**
   * Utility class that helps to manage the sessions in custom multi-threaded
   * implementation.
   */
  private DbUtils dbUtils;

  /**
   * Object that sends emails.
   */
  private EmailSender emailSender;

  @Autowired
  public LayoutService(LayoutDao layoutDao,
      IUserService userService,
      IConfigurationService configurationService,
      DbUtils dbUtils) {
    this.layoutDao = layoutDao;
    this.userService = userService;
    this.configurationService = configurationService;
    this.dbUtils = dbUtils;
  }

  @Override
  public void removeLayout(Layout layout, final String homeDir) {
    final String dir;
    if (homeDir != null) {
      if (layout.getProject().getDirectory() != null) {
        dir = homeDir + "/../map_images/" + layout.getProject().getDirectory() + "/";
      } else {
        dir = homeDir + "/../map_images/";
      }
    } else {
      dir = null;
    }

    String projectId = layout.getProject().getProjectId();
    final String email;
    User user = layout.getCreator();
    if (user != null) {
      email = user.getEmail();
    } else {
      email = null;
    }

    layout.getProject().removeLayout(layout);
    layoutDao.delete(layout);

    logger.info("Data overlay " + layout.getId() + " removed");

    if (email != null) {
      Thread sendEmailThread = new Thread(() -> {
        try {
          sendSuccesfullRemoveEmail(projectId, layout.getTitle(), email);
        } catch (MessagingException e) {
          logger.error(e);
        }
      });
      sendEmailThread.start();
    }
    Thread removeFilesThread = new Thread(() -> {
      MapGenerator generator = new MapGenerator();
      try {
        generator.removeLayout(layout, dir);
      } catch (IOException e) {
        logger.error(e);
      }

    });
    removeFilesThread.start();
  }

  @Override
  public void updateLayout(Layout layout) {
    layoutDao.update(layout);
  }

  @Override
  public Layout createLayout(final CreateLayoutParams params) throws IOException, InvalidColorSchemaException {
    ColorSchemaReader reader = new ColorSchemaReader();
    Map<String, String> parameters = TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream());
    ColorSchemaType colorSchemaType = params.getColorSchemaType();
    if (colorSchemaType == null) {
      String type = parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE);
      if (type != null) {
        colorSchemaType = ColorSchemaType.valueOf(type);
      }
    }
    if (colorSchemaType == null) {
      colorSchemaType = ColorSchemaType.GENERIC;
    }

    if (parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) == null && params.getColorSchemaType() != null) {
      parameters.put(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE, params.getColorSchemaType().name());
    }
    final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(), parameters);

    // check if we can color our model using this schema,
    // if not then exception will be thrown and passed up
    try {
      Model model = params.getProject().getModels().iterator().next().getModel();
      Model copy = new CopyCommand(model).execute();
      new ColorModelCommand(copy, schemas, userService.getColorExtractorForUser(params.getUser())).execute();
    } catch (CommandExecutionException e) {
      throw new InvalidColorSchemaException(e);
    }

    layoutDao.flush();

    Layout topLayout = new Layout(params.getName(), false);
    topLayout.setColorSchemaType(colorSchemaType);
    if (params.getUser() == null) {
      topLayout.setPublicLayout(true);
    } else {
      topLayout.setPublicLayout(false);
    }
    topLayout.setGoogleLicenseConsent(params.isGoogleLicenseConsent());
    topLayout.setStatus(LayoutStatus.OK);
    topLayout.setProgress(0.0);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(params.getColorInputStream()));
    fileEntry.setOriginalFileName(params.getLayoutFileName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    fileEntry.setOwner(params.getUser());
    topLayout.setInputData(fileEntry);
    topLayout.setDescription(params.getDescription());
    topLayout.setProject(params.getProject());
    topLayout.setCreator(params.getUser());
    layoutDao.add(topLayout);

    Thread sendEmailThread = new Thread(() -> {
      try {
        // open transaction for this thread
        dbUtils.createSessionForCurrentThread();
        sendSuccessfullGenerationEmail(params, schemas);
      } catch (MessagingException e) {
        logger.error("Problem with sending email", e);
      } finally {
        dbUtils.closeSessionForCurrentThread();
      }
    });

    logger.info("Data overlay " + topLayout.getId() + " created successfully");
    sendEmailThread.start();
    return topLayout;
  }

  @Override
  public Layout createLayoutWithImages(final CreateLayoutParams params)
      throws IOException, InvalidColorSchemaException, CommandExecutionException {
    ColorSchemaReader reader = new ColorSchemaReader();
    final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(),
        TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));
    Model model = params.getProject().getModels().iterator().next().getModel();
    final Model colorModel = new CopyCommand(model).execute();
    new ColorModelCommand(model, schemas, userService.getColorExtractorForUser(params.getUser())).execute();
    String[] tmp = params.getDirectory().split("[\\\\/]");
    String simpleDir = tmp[tmp.length - 1];

    layoutDao.flush();

    boolean customSession = false;
    if (params.isAsync()) {
      customSession = !dbUtils.isCustomSessionForCurrentThread();
      if (customSession) {
        dbUtils.createSessionForCurrentThread();
      }
    }
    final Layout topLayout = new Layout(params.getName(), false);
    topLayout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, simpleDir));
    if (params.getUser() == null) {
      topLayout.setPublicLayout(true);
    } else {
      topLayout.setPublicLayout(false);
    }
    topLayout.setStatus(LayoutStatus.NA);
    topLayout.setProgress(0.0);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(params.getColorInputStream()));
    fileEntry.setOriginalFileName(params.getLayoutFileName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    fileEntry.setOwner(params.getUser());
    topLayout.setInputData(fileEntry);
    topLayout.setDescription(params.getDescription());
    topLayout.setCreator(params.getUser());
    layoutDao.add(topLayout);
    topLayout.getDataOverlayImageLayers().iterator().next().setDirectory(simpleDir + topLayout.getId());

    layoutDao.update(topLayout);

    int index = 0;
    for (ModelSubmodelConnection connection : model.getSubmodelConnections()) {
      topLayout.addDataOverlayImageLayer(
          new DataOverlayImageLayer(connection.getSubmodel().getModel(), simpleDir + topLayout.getId() + "-" + index));
      index++;
      layoutDao.update(topLayout);
    }

    if (params.isAsync()) {
      if (customSession) {
        dbUtils.closeSessionForCurrentThread();
      } else {
        layoutDao.commit();
      }
    }
    final int layoutId = topLayout.getId();
    final int models = 1 + colorModel.getSubmodels().size();
    Thread computations = new Thread(() -> {
      if (params.isAsync()) {
        // open transaction for this thread
        dbUtils.createSessionForCurrentThread();
      }

      try {
        MapGenerator generator = new MapGenerator();
        int count = 0;
        for (DataOverlayImageLayer imageLayer : topLayout.getDataOverlayImageLayers()) {

          final int counted = count;
          MapGeneratorParams imgParams = generator.new MapGeneratorParams()
              .model(colorModel.getSubmodelById(imageLayer.getModel().getId()))
              .directory(imageLayer.getDirectory()).updater(new IProgressUpdater() {
                private int lastProgress = -1;

                @Override
                public void setProgress(double progress) {
                  progress = progress / ((double) models) + ((double) counted * MAX_PROGRESS) / ((double) models);
                  if (((int) progress) != lastProgress) {
                    Layout layout = layoutDao.getById(layoutId);
                    lastProgress = (int) progress;
                    layout.setProgress(progress);
                    layout.setStatus(LayoutStatus.GENERATING);
                    layoutDao.update(layout);
                    if (params.isAsync()) {
                      layoutDao.commit();
                    }
                  }
                }
              });
          imgParams.sbgn(params.getProject().isSbgnFormat());
          generator.generateMapImages(imgParams);
          count++;
        }
        Layout layout = layoutDao.getById(layoutId);
        layout.setProgress(IProgressUpdater.MAX_PROGRESS);
        layout.setStatus(LayoutStatus.OK);
        layoutDao.update(layout);
        if (params.isAsync()) {
          layoutDao.commit();
        }

        try {
          sendSuccessfullGenerationEmail(params, schemas);
        } catch (MessagingException e) {
          logger.error("Problem with sending email", e);
        }
        logger.info("Data overlay " + layout.getId() + " created successfully");
      } catch (Exception e) {
        logger.error("Problem with creating layout " + layoutId, e);

        Layout layout = layoutDao.getById(layoutId);
        layout.setProgress(IProgressUpdater.MAX_PROGRESS);
        layout.setStatus(LayoutStatus.FAILURE);
        layoutDao.update(layout);

        try {
          getEmailSender().sendEmail("Minerva status",
              "Project: " + params.getProject().getProjectId() + ". There was a problem with generating layout "
                  + params.getName() + ". Contact administrator to solve this issue.",
              params.getUser());
        } catch (MessagingException e1) {
          logger.error("Problem with sending email", e1);
        }
      }
      if (params.isAsync()) {
        // close the transaction for this thread
        dbUtils.closeSessionForCurrentThread();
      }
    });

    if (params.isAsync()) {
      computations.start();
    } else {
      computations.run();
    }
    return topLayout;
  }

  @Override
  public List<Pair<Element, ColorSchema>> getAliasesForLayout(Model model, int layoutId) {
    try {
      Collection<ColorSchema> schemas = createColorSchemaCollection(layoutId);
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      List<Pair<Element, ColorSchema>> result = new ArrayList<>();
      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Element) {
          result.add(new Pair<>((Element) entry.getKey(), entry.getValue()));
        }
      }
      result.sort(LayoutService.ELEMENT_PAIR_COMPARATOR);
      return result;
    } catch (InvalidColorSchemaException | IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public List<Pair<Reaction, ColorSchema>> getReactionsForLayout(Model model, int layoutId) {
    try {
      Collection<ColorSchema> schemas = createColorSchemaCollection(layoutId);
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      List<Pair<Reaction, ColorSchema>> result = new ArrayList<>();
      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Reaction) {
          result.add(new Pair<>((Reaction) entry.getKey(), entry.getValue()));
        }
      }
      result.sort(LayoutService.ELEMENT_PAIR_COMPARATOR);
      return result;
    } catch (InvalidColorSchemaException | IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public Map<Object, ColorSchema> getElementsForLayout(Model model, Integer layoutId) {
    try {
      Collection<ColorSchema> schemas = createColorSchemaCollection(layoutId);
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      return command.getModifiedElements();
    } catch (InvalidColorSchemaException | IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public Pair<Element, ColorSchema> getFullAliasForLayout(Model model, Integer id, int layoutId) {
    try {
      Collection<ColorSchema> schemas = createColorSchemaCollection(layoutId);
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));

      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Element) {
          Element alias = (Element) entry.getKey();
          if (id.equals(alias.getId())) {
            return new Pair<>(alias, entry.getValue());
          }
        }
      }
      return null;
    } catch (InvalidColorSchemaException | IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public Pair<Reaction, ColorSchema> getFullReactionForLayout(Model model, Integer id, int layoutId) {
    try {
      Collection<ColorSchema> schemas = createColorSchemaCollection(layoutId);
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));

      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Reaction) {
          Reaction alias = (Reaction) entry.getKey();
          if (id.equals(alias.getId())) {
            return new Pair<>(alias, entry.getValue());
          }
        }
      }
      return null;
    } catch (InvalidColorSchemaException | IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public EmailSender getEmailSender() {
    if (emailSender == null) {
      emailSender = new EmailSender(configurationService);
    }
    return emailSender;
  }

  @Override
  public void setEmailSender(EmailSender emailSender) {
    this.emailSender = emailSender;
  }

  @Override
  public List<Layout> getLayoutsByProject(Project project) {
    List<Layout> result = new ArrayList<>();
    List<Layout> overlays = layoutDao.getLayoutsByProject(project);
    result.addAll(overlays);
    result.sort(Layout.ID_COMPARATOR);
    return result;
  }

  @Override
  public Layout getLayoutById(Integer overlayId) {
    return layoutDao.getById(overlayId);
  }

  public void setLayoutDao(LayoutDao layoutDao) {
    this.layoutDao = layoutDao;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * Sends notification email that layout was removed.
   *
   * @param projectId
   *          identifier of the project
   * @param layoutName
   *          name of the layout
   * @param email
   *          notification email
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(String projectId, String layoutName, String email)
      throws MessagingException {
    getEmailSender().sendEmail("Minerva notification", "Layout " + layoutName +
        " in map " + projectId + " was successfully removed.<br/>", email);
  }

  /**
   * Sends notification email that layout was generated.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} used for layout creation
   * @param schemas
   *          set of schemas used in coloring
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccessfullGenerationEmail(final CreateLayoutParams params, final Collection<ColorSchema> schemas)
      throws MessagingException {
    StringBuilder content = new StringBuilder("Project: " + params.getProject().getProjectId() + ".\n");
    content.append("Layout " + params.getName() + " generated successfully.\n");
    content.append("Result of coloring:<br/>");

    String table = prepareTableResult(schemas, new ColorSchemaReader());
    content.append(table);
    String email = params.getProject().getNotifyEmail();
    if (params.getUser() != null) { // send email to a user who owns the layout
      getEmailSender().sendEmail("Minerva notification", content.toString(), params.getUser());

      // send email to the model owner
      if (email != null && !email.equals(params.getUser().getEmail())) {
        content = new StringBuilder();
        String username = "UNKNOWN";
        if (params.getUser() != null) {
          username = params.getUser().getLogin();
        }
        content.append("User ")
            .append(username)
            .append(" created layout in ")
            .append(params.getProject().getProjectId())
            .append(" map.");

        getEmailSender().sendEmail("Minerva notification", content.toString(), email);
      }
    } else if (email != null) {
      // if nobody owns the layout then send it to the model owner
      getEmailSender().sendEmail("Minerva notification", content.toString(), email);
    }
  }

  /**
   * Prepares table with statistics about coloring.
   *
   * @param schemas
   *          schemas that were used for coloring
   * @param scr
   *          interface that returns list of columns that should be printed
   * @return table with statistics about coloring
   */
  protected String prepareTableResult(Collection<ColorSchema> schemas, ColorSchemaReader scr) {
    StringBuilder sb = new StringBuilder();

    Collection<ColorSchemaColumn> columns = scr.getSetColorSchemaColumns(schemas);

    for (ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        sb.append(column.getColumnName())
            .append("\t");
      }
    }
    sb.append("matches<br/>\n");

    for (ColorSchema originalSchema : schemas) {
      for (ColorSchema schema : splitColorSchema(originalSchema)) {
        sb.append(prepareTableRow(columns, schema));
      }
    }

    return sb.toString();
  }

  /**
   * {@link ColorSchema} sometimes contains merged value from few rows. This
   * method split single {@link ColorSchema} object to simple flat objects that
   * can be serialiazed in a simple tab separated file.
   *
   * @param originalSchema
   *          original {@link ColorSchema} objcet that we want to spli into flat
   *          objects
   * @return {@link List} of flat schema objects obtained from input
   */
  private List<ColorSchema> splitColorSchema(ColorSchema originalSchema) {
    List<ColorSchema> result = new ArrayList<>();
    if (originalSchema instanceof GenericColorSchema) {
      result.add(originalSchema);
    } else if (originalSchema instanceof GeneVariationColorSchema) {
      for (GeneVariation gv : ((GeneVariationColorSchema) originalSchema).getGeneVariations()) {
        GeneVariationColorSchema copy = (GeneVariationColorSchema) originalSchema.copy();
        copy.getGeneVariations().clear();
        copy.addGeneVariation(gv.copy());
        result.add(copy);
      }
    } else {
      throw new InvalidArgumentException("Unknown class type: " + originalSchema.getClass());
    }
    return result;
  }

  /**
   * Prepares tab separated {@link String} represenation of {@link ColorSchema}.
   *
   * @param columns
   *          columns that should be outputed in the result String
   * @param schema
   *          input ColorSchema that is going to be transformed into String
   *          representation
   * @return tab separated {@link String} represenation of {@link ColorSchema}
   */
  protected String prepareTableRow(Collection<ColorSchemaColumn> columns, ColorSchema schema) {
    StringBuilder sb = new StringBuilder();
    for (ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        if (schema instanceof GenericColorSchema) {
          sb.append(prepareTableCellForGenericSchema((GenericColorSchema) schema, column));
        } else if (schema instanceof GeneVariationColorSchema) {
          sb.append(prepareTableCellForGeneVariationSchema((GeneVariationColorSchema) schema, column));
        } else {
          throw new InvalidArgumentException("Unknown schema type: " + schema.getClass());
        }
      }
    }
    sb.append(schema.getMatches()).append("<br/>\n");
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GenericColorSchema} that should
   * appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema
   *          object for which data will be returned
   * @param column
   *          column for which data should be returned
   * @return {@link String} representing data of {@link GenericColorSchema} that
   *         should appear in a given {@link ColorSchemaColumn}
   */
  protected String prepareTableCellForGenericSchema(GenericColorSchema schema, ColorSchemaColumn column) {
    StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MAP_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (Class<? extends Element> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GeneVariationColorSchema} that
   * should appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema
   *          object for which data will be returned
   * @param column
   *          column for which data should be returned
   * @return {@link String} representing data of {@link GeneVariationColorSchema}
   *         that should appear in a given {@link ColorSchemaColumn}
   */
  protected String prepareTableCellForGeneVariationSchema(GeneVariationColorSchema schema, ColorSchemaColumn column) {
    StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MAP_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (Class<? extends Element> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(schema.getGeneVariations().get(0).getPosition() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else if (column.equals(ColorSchemaColumn.ORIGINAL_DNA)) {
      sb.append(schema.getGeneVariations().get(0).getOriginalDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.ALTERNATIVE_DNA)) {
      sb.append(schema.getGeneVariations().get(0).getModifiedDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.REFERENCE_GENOME_TYPE)) {
      sb.append(schema.getGeneVariations().get(0).getReferenceGenomeType() + "\t");
    } else if (column.equals(ColorSchemaColumn.REFERENCE_GENOME_VERSION)) {
      sb.append(schema.getGeneVariations().get(0).getReferenceGenomeVersion() + "\t");
    } else if (column.equals(ColorSchemaColumn.CONTIG)) {
      sb.append(schema.getGeneVariations().get(0).getContig() + "\t");
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  /**
   * Returns byte array containing data from original input file that was used to
   * generate the layout.
   *
   * @param layoutId
   *          identifier of layout for which we want to retrieve original file
   *          data
   * @return original data file for given layout, if such file is not stored in
   *         database (compatibility reasons) then null is returned
   */
  private byte[] getInputDataForLayout(int layoutId) {
    Layout layout = getLayoutById(layoutId);
    if (layout == null) {
      return null;
    } else {
      if (layout.getInputData() != null) {
        return layout.getInputData().getFileContent();
      } else {
        return null;
      }
    }
  }

  private Collection<ColorSchema> createColorSchemaCollection(int overlayId)
      throws IOException, InvalidColorSchemaException {
    ColorSchemaReader reader = new ColorSchemaReader();
    Layout overlay = getLayoutById(overlayId);
    return reader.readColorSchema(getInputDataForLayout(overlayId), overlay.getColorSchemaType());
  }

  @Override
  public User getOverlayCreator(String overlayId) {
    User result = null;
    if (NumberUtils.isCreatable(overlayId)) {
      Layout overlay = layoutDao.getById(Integer.valueOf(overlayId));
      if (overlay != null) {
        result = overlay.getCreator();
      }
    }
    if (result != null) {
      // fetch lazy initialized object
      result.getLogin();
    }
    return result;
  }
}
