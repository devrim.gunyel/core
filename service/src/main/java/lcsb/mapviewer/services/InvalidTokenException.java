package lcsb.mapviewer.services;

public class InvalidTokenException extends SecurityException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public InvalidTokenException(String message) {
    super(message);
  }

}
