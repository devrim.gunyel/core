package lcsb.mapviewer.services.utils;

import java.io.IOException;
import java.util.*;

import org.primefaces.model.TreeNode;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.services.overlay.AnnotatedObjectTreeRow;

/**
 * Set of parameters used during creation of new project.
 * 
 * @author Piotr Gawron
 * 
 */
public class CreateProjectParams {

  /**
   * User defined project identifier.
   */
  private String projectId;

  /**
   * Name of the project.
   */
  private String projectName;

  /**
   * Disease associated to the project.
   */
  private String disease;

  /**
   * Organism associated to the project.
   */
  private String organism;

  /**
   * Path to a file used as a model.
   */
  private UploadedFileEntry projectFile;

  private Converter parser;

  /**
   * Is the project a complex multi-file project.
   */
  private boolean complex;
  private boolean semanticZoomContainsMultipleLayouts = false;

  /**
   * List of zip entries in the complex model definition.
   */
  private List<ZipEntryFile> zipEntries = new ArrayList<>();

  /**
   * Should the elements be annotated by the external resources.
   */
  private boolean updateAnnotations = false;

  /**
   * Do we want to generate images for all available layers.
   */
  private boolean images = false;

  /**
   * Should the map be autoresized after processing (to trim unnecessary margins).
   */
  private boolean autoResize = true;

  /**
   * Should the data from external resources but linked to model be cached.
   */
  private boolean cacheModel = true;

  /**
   * Should the process of creation of the project be asynchronous (in separate
   * thread).
   */
  private boolean async = false;

  /**
   * Should the map be displayed in SBGN format.
   */
  private boolean sbgnFormat = false;

  /**
   * Is the {@link lcsb.mapviewer.services.utils.data.BuildInLayout#NORMAL}
   * default {@link lcsb.mapviewer.model.map.layout.Layout Layout} when generating
   * new project.
   */
  private boolean networkLayoutAsDefault = false;

  /**
   * Do we want to analyze annotations after processing of the map. If yes then in
   * the email with summary of the generation process information about improper
   * annotations will be sent.
   */
  private boolean analyzeAnnotations = false;

  /**
   * Email address that should be notified after everything is done.
   */
  private String notifyEmail = null;

  /**
   * Description of the project.
   */
  private String description = "";

  /**
   * Version of the map.
   */
  private String version = "0";

  /**
   * Directory with the static images that will be stored on server. This
   * directory is relative and it's a simple unique name within folder with
   * images.
   */
  private String projectDir;

  private User user;

  /**
   * Map that contains information what kind of annotators should be used for
   * specific class.
   */
  private Map<Class<?>, List<AnnotatorData>> annotatorsMap = null;

  /**
   * Map that contains information which {@link MiriamType miriam types} are valid
   * for which class.
   */
  private Map<Class<? extends BioEntity>, Set<MiriamType>> validAnnotations = null;

  /**
   * Map that contains information which {@link MiriamType miriam types} are
   * obligatory for which class.
   */
  private Map<Class<? extends BioEntity>, Set<MiriamType>> requiredAnnotations = null;

  private MapCanvasType mapCanvasType = MapCanvasType.OPEN_LAYERS;

  /**
   * @param projectId
   *          the projectId to set
   * @return object with all parameters
   * @see #projectId
   */
  public CreateProjectParams projectId(String projectId) {
    this.projectId = projectId;
    return this;
  }

  public CreateProjectParams parser(Converter parser) {
    this.parser = parser;
    return this;
  }

  /**
   * @param notifyEmail
   *          the notifyEmail to set
   * @return object with all parameters
   * @see #notifyEmail
   */
  public CreateProjectParams notifyEmail(String notifyEmail) {
    this.notifyEmail = notifyEmail;
    return this;
  }

  /**
   * @param autoResize
   *          the autoResize to set
   * @return object with all parameters
   * @see #autoResize
   */
  public CreateProjectParams autoResize(boolean autoResize) {
    this.autoResize = autoResize;
    return this;
  }

  public CreateProjectParams autoResize(String value) {
    return this.autoResize("true".equalsIgnoreCase(value));
  }

  /**
   * Sets input stream from which projects should be generated.
   * 
   * @param fileEntry
   *          uploaded file that should be used to create a project
   * @return object with all parameters
   * @throws IOException
   *           thrown if there are some problem with the stream
   */
  public CreateProjectParams projectFile(UploadedFileEntry fileEntry) throws IOException {
    this.projectFile = fileEntry;
    return this;
  }

  /**
   * @param async
   *          the async to set
   * @return object with all parameters
   * @see #async
   */
  public CreateProjectParams async(boolean async) {
    this.async = async;
    return this;
  }

  /**
   * @param analyzeAnnotations
   *          the analyzeAnnotations to set
   * @return object with all parameters
   * @see #analyzeAnnotations
   */
  public CreateProjectParams analyzeAnnotations(boolean analyzeAnnotations) {
    this.analyzeAnnotations = analyzeAnnotations;
    return this;
  }

  /**
   * @param updateAnnotations
   *          the updateAnnotations to set
   * @return object with all parameters
   * @see #updateAnnotations
   */
  public CreateProjectParams annotations(boolean updateAnnotations) {
    this.updateAnnotations = updateAnnotations;
    return this;
  }

  /**
   * @param images
   *          the images to set
   * @return object with all parameters
   * @see #images
   */
  public CreateProjectParams images(boolean images) {
    this.images = images;
    return this;
  }

  /**
   * @param description
   *          the description to set
   * @return object with all parameters
   * @see #description
   */
  public CreateProjectParams description(String description) {
    this.description = description;
    return this;
  }

  /**
   * @param version
   *          the version to set
   * @return object with all parameters
   * @see #version
   */
  public CreateProjectParams version(String version) {
    this.version = version;
    return this;
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the notifyEmail
   * @see #notifyEmail
   */
  public String getNotifyEmail() {
    return notifyEmail;
  }

  /**
   * @return the analyzeAnnotations
   * @see #analyzeAnnotations
   */
  public boolean isAnalyzeAnnotations() {
    return analyzeAnnotations;
  }

  /**
   * @return the async
   * @see #async
   */
  public boolean isAsync() {
    return async;
  }

  /**
   * @return the autoResize
   * @see #autoResize
   */
  public boolean isAutoResize() {
    return autoResize;
  }

  /**
   * @return the images
   * @see #images
   */
  public boolean isImages() {
    return images;
  }

  /**
   * @return the updateAnnotations
   * @see #updateAnnotations
   */
  public boolean isUpdateAnnotations() {
    return updateAnnotations;
  }

  /**
   * @return the projectFile
   * @see #projectFile
   */
  public UploadedFileEntry getProjectFile() {
    return projectFile;
  }

  /**
   * @return the projectId
   * @see #projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @return the cacheModel
   * @see #cacheModel
   */
  public boolean isCacheModel() {
    return cacheModel;
  }

  /**
   * @param cacheModel
   *          the cacheModel to set
   * @see #cacheModel
   * @return object with all parameters
   */
  public CreateProjectParams cacheModel(boolean cacheModel) {
    this.cacheModel = cacheModel;
    return this;
  }

  public CreateProjectParams cacheModel(String value) {
    return this.cacheModel("true".equalsIgnoreCase(value));
  }

  /**
   * @return the submodels
   * @see #zipEntries
   */
  public List<ZipEntryFile> getZipEntries() {
    return zipEntries;
  }

  /**
   * @param entry
   *          the submodel to add
   * @see #zipEntries
   * @return object with all parameters
   */
  public CreateProjectParams addZipEntry(ZipEntryFile entry) {
    if (entry != null) {
      this.zipEntries.add(entry);
    }
    return this;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public boolean isComplex() {
    return complex;
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   * @see #sbgnFormat
   */
  public CreateProjectParams sbgnFormat(boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
    return this;
  }

  public CreateProjectParams sbgnFormat(String value) {
    return this.sbgnFormat("true".equalsIgnoreCase(value));
  }

  /**
   * @param complex
   *          the complex to set
   * @see #complex
   * @return object with all parameters
   */
  public CreateProjectParams complex(boolean complex) {
    this.complex = complex;
    return this;
  }

  /**
   * Creates {@link #annotatorsMap}.
   * 
   * @param annotatorsTree
   *          tree containing information about annotators for given classes
   */
  public void annotatorsMap(TreeNode annotatorsTree) {
    Map<Class<?>, List<AnnotatorData>> map = new HashMap<>();
    Queue<TreeNode> queue = new LinkedList<>();
    queue.add(annotatorsTree);
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();

      map.put(((AnnotatedObjectTreeRow) node.getData()).getClazz(),
          ((AnnotatedObjectTreeRow) node.getData()).getUsedAnnotators());

      for (TreeNode node2 : node.getChildren()) {
        queue.add(node2);
      }
    }
    annotatorsMap = map;
  }

  /**
   * @return the annotatorsMap
   * @see #annotatorsMap
   */
  public Map<Class<?>, List<AnnotatorData>> getAnnotatorsMap() {
    return annotatorsMap;
  }

  /**
   * Creates {@link #validAnnotations}.
   * 
   * @param annotatorsTree
   *          tree containing information about valid {@link MiriamType miriam
   *          types}
   */
  @SuppressWarnings("unchecked")
  public void validAnnotations(TreeNode annotatorsTree) {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map = new HashMap<Class<? extends BioEntity>, Set<MiriamType>>();
    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(annotatorsTree);
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();
      Set<MiriamType> set = new HashSet<MiriamType>();

      TreeNode parent = node;
      while (parent != null) {
        set.addAll(((AnnotatedObjectTreeRow) parent.getData()).getValidAnnotations());
        parent = parent.getParent();
      }
      map.put((Class<? extends BioEntity>) ((AnnotatedObjectTreeRow) node.getData()).getClazz(), set);

      for (TreeNode node2 : node.getChildren()) {
        queue.add(node2);
      }
    }
    validAnnotations = map;
  }

  /**
   * Creates {@link #requiredAnnotations}.
   * 
   * @param annotatorsTree
   *          tree containing information about required {@link MiriamType miriam
   *          types}
   */
  @SuppressWarnings("unchecked")
  public void requiredAnnotations(TreeNode annotatorsTree) {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map = new HashMap<Class<? extends BioEntity>, Set<MiriamType>>();
    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(annotatorsTree);
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();
      boolean valid = ((AnnotatedObjectTreeRow) node.getData()).getRequire();
      Set<MiriamType> set = new HashSet<MiriamType>();

      TreeNode parent = node;
      while (parent != null) {
        set.addAll(((AnnotatedObjectTreeRow) parent.getData()).getRequiredAnnotations());
        if (((AnnotatedObjectTreeRow) parent.getData()).getRequire()) {
          valid = true;
        }
        parent = parent.getParent();
      }
      if (!valid) {
        set = null;
      }
      map.put((Class<? extends BioEntity>) ((AnnotatedObjectTreeRow) node.getData()).getClazz(), set);

      for (TreeNode node2 : node.getChildren()) {
        queue.add(node2);
      }
    }
    requiredAnnotations = map;
  }

  /**
   * Returns {@link #validAnnotations}.
   * 
   * @return {@link #validAnnotations}.
   */
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getValidAnnotations() {
    return validAnnotations;
  }

  /**
   * Returns {@link #requiredAnnotations}.
   * 
   * @return {@link #requiredAnnotations}.
   */
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getRequiredAnnotations() {
    return requiredAnnotations;
  }

  /**
   * Sets {@link #projectDir}.
   * 
   * @param directory
   *          new {@link #projectDir} value
   * @return instance of this class with new value set
   */
  public CreateProjectParams projectDir(String directory) {
    this.projectDir = directory;
    return this;
  }

  /**
   * @return the projectDir
   * @see #projectDir
   */
  public String getProjectDir() {
    return projectDir;
  }

  /**
   * 
   * @param projectName
   *          new {@link #projectName}
   * @return instance of this class with new value set
   */
  public CreateProjectParams projectName(String projectName) {
    this.projectName = projectName;
    return this;
  }

  /**
   * @return the projectName
   * @see #projectName
   */
  public String getProjectName() {
    return projectName;
  }

  /**
   * @param value
   *          the networkLayoutAsDefault to set
   * @see #networkLayoutAsDefault
   * @return instance of this class with new value set
   */
  public CreateProjectParams networkLayoutAsDefault(boolean value) {
    this.networkLayoutAsDefault = value;
    return this;

  }

  /**
   * @return the networkLayoutAsDefault
   * @see #networkLayoutAsDefault
   */
  public boolean isNetworkLayoutAsDefault() {
    return networkLayoutAsDefault;
  }

  /**
   * @return disease MESH code.
   */
  public String getDisease() {
    return disease;
  }

  /**
   * @param disease
   *          the code of the disease.
   * @return updated params object.
   */
  public CreateProjectParams projectDisease(String disease) {
    this.disease = disease;
    return this;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public String getOrganism() {
    return organism;
  }

  /**
   * @param organism
   *          the organism to set
   * @return updated params object.
   */
  public CreateProjectParams projectOrganism(String organism) {
    this.organism = organism;
    return this;
  }

  public User getUser() {
    return user;
  }

  public CreateProjectParams setUser(User user) {
    this.user = user;
    return this;
  }

  public CreateProjectParams semanticZoomContainsMultipleLayouts(boolean semanticZoomContainsMultipleLayouts) {
    this.semanticZoomContainsMultipleLayouts = semanticZoomContainsMultipleLayouts;
    return this;
  }

  public CreateProjectParams semanticZoomContainsMultipleLayouts(String value) {
    return this.semanticZoomContainsMultipleLayouts("true".equals(value));
  }

  public boolean isSemanticZoomContainsMultipleLayouts() {
    return this.semanticZoomContainsMultipleLayouts;
  }

  public Converter getParser() {
    return this.parser;
  }

  public CreateProjectParams annotations(String value) {
    return this.annotations("true".equalsIgnoreCase(value));
  }

  public CreateProjectParams analyzeAnnotations(String value) {
    return this.analyzeAnnotations("true".equalsIgnoreCase(value));
  }

  public MapCanvasType getMapCanvasType() {
    return mapCanvasType;
  }

  public CreateProjectParams mapCanvasType(MapCanvasType mapCanvasType) {
    this.mapCanvasType = mapCanvasType;
    return this;
  }

}
