package lcsb.mapviewer.services.utils.data;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.model.map.layout.*;

/**
 * This enum defines which columns are available for defining
 * {@link lcsb.mapviewer.model.map.layout.ColorSchema}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ColorSchemaColumn {

  /**
   * Name of the element.
   */
  NAME("gene_name", new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  GENE_NAME("name", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Name of the map.
   */
  MAP_NAME("model_name", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Value that will be transformed into new color.
   * 
   * @see ColorSchemaColumn#COLOR
   */
  VALUE(new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * In which compartment the element should be located.
   */
  COMPARTMENT(new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Class type of the element.
   */
  TYPE(new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * New element/reaction color.
   */
  COLOR(new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Identifier of the element.
   */
  IDENTIFIER("references", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Element identifier.
   */
  ELEMENT_IDENTIFIER("elementIdentifier", new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * Element identifier.
   */
  @Deprecated
  REACTION_IDENTIFIER("reactionIdentifier", new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * New line width of the reaction.
   */
  LINE_WIDTH("lineWidth", new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * Position where gene variants starts.
   */
  POSITION(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Original DNA of the variant.
   */
  ORIGINAL_DNA(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Alternative DNA of the variant.
   */
  ALTERNATIVE_DNA(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Short description of the entry.
   */
  DESCRIPTION(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT, ColorSchemaType.GENERIC }),

  /**
   * What's the {@link ReferenceGenomeType}.
   */
  @Deprecated
  REFERENCE_GENOME_TYPE(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * {@link ReferenceGenome#version Version} of the reference genome.
   */
  @Deprecated
  REFERENCE_GENOME_VERSION(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Contig where variant was observed.
   */
  CONTIG(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  CHROMOSOME(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  ALLEL_FREQUENCY("allele_frequency", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  VARIANT_IDENTIFIER(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }),

  /**
   * Should the direction of reaction be reversed.
   */
  REVERSE_REACTION("reverseReaction", new ColorSchemaType[] { ColorSchemaType.GENERIC }),

  /**
   * Optional amino acid change in the variant.
   */
  AMINO_ACID_CHANGE(new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT });

  /**
   * Human readable title used in input file.
   */
  private String deprecatedColumnName;
  /**
   * Set of types where column is allowed.
   */
  private Set<ColorSchemaType> types = new HashSet<>();

  /**
   * Constructor that creates enum entry with deprecated column name.
   *
   * @param deprecatedColumnName
   *          {@link #deprecatedColumnName}
   * @param types
   *          list of {@link ColorSchemaType types} where this column is allowed
   * @deprecated As of release 12.3, replaced by
   *             {@link #ColorSchemaColumn(ColorSchemaType[])}
   */
  @Deprecated
  ColorSchemaColumn(String deprecatedColumnName, ColorSchemaType[] types) {
    this.deprecatedColumnName = deprecatedColumnName;
    for (ColorSchemaType colorSchemaType : types) {
      this.types.add(colorSchemaType);
    }
  }

  /**
   * Default constructor that creates enum entry.
   *
   * @param deprecatedColumnName
   *          {@link #deprecatedColumnName}
   * @param types
   *          list of {@link ColorSchemaType types} where this column is allowed
   */
  ColorSchemaColumn(ColorSchemaType[] types) {
    for (ColorSchemaType colorSchemaType : types) {
      this.types.add(colorSchemaType);
    }
  }

  /**
   * 
   * @return {@link #deprecatedColumnName}
   * @deprecated As of release 12.3, replaced by {@link #getColumnName()}
   */
  @Deprecated
  public String getDepractedColumnName() {
    return deprecatedColumnName;
  }

  public String getColumnName() {
    return this.name().toLowerCase();
  }

  /**
   * @return the types
   * @see #types
   */
  public Set<ColorSchemaType> getTypes() {
    return types;
  }

}
