/**
 * This package contains structures used for passing chemical related data about
 * elements to the client.
 * <p/>
 * Be carefull when refactoring classes in this package. Data are accessed not
 * directly (not via managable code) by client side.
 */
package lcsb.mapviewer.services.search.chemical;
