package lcsb.mapviewer.services.search.mirna;

import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.services.search.IDbSearchService;

/**
 * Service for accessing information about miRNA.
 * 
 * @author Ayan Rota
 * 
 */
public interface IMiRNAService extends IDbSearchService<MiRNA> {

}
