package lcsb.mapviewer.services.search.mirna;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.MiRNAParser;
import lcsb.mapviewer.annotation.services.MiRNASearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.DbSearchService;

/**
 * Implementation of the service that allows access information about miRNAs.
 * 
 * @author Ayan Rota
 * 
 */
@Transactional
@Service
public class MiRNAService extends DbSearchService implements IMiRNAService {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(MiRNAService.class);

  /**
   * Access point and parser for the online ctd database.
   */
  private MiRNAParser miRNAParser;

  /**
   * Object used to get information about {@link MiriamType#HGNC} annotations and
   * transformations.
   */
  private HgncAnnotator hgncAnnotator;

  public MiRNAService(MiRNAParser miRNAParser, HgncAnnotator hgncAnnotator) {
    super();
    this.miRNAParser = miRNAParser;
    this.hgncAnnotator = hgncAnnotator;
  }

  @Override
  public MiRNA getByName(String name, DbSearchCriteria searchCriteria) {
    if ("".equals(name)) {
      return null;
    }
    try {
      List<String> names = new ArrayList<>();
      names.add(name);
      List<MiRNA> miRNAs = miRNAParser.getMiRnasByNames(names);
      if (miRNAs.size() > 0) {
        removeUnknownOrganisms(miRNAs.get(0), searchCriteria.getOrganisms());
        return miRNAs.get(0);
      }
      return null;
      // return miRNAViewFactory.create(miRNAs.get(0),
      // searchCriteria.getModel(), searchCriteria.getColorSet());
    } catch (MiRNASearchException e) {
      logger.error("Problem with accessing mirna database", e);
      return null;
    }
  }

  @Override
  public List<MiRNA> getForTargets(Collection<Element> targets, DbSearchCriteria searchCriteria) {
    List<MiRNA> mirnaList = new ArrayList<MiRNA>();
    Set<MiriamData> targetsMiriam = new HashSet<MiriamData>();
    for (Element element : targets) {
      if (element instanceof Protein || element instanceof Gene || element instanceof Rna) {
        boolean hgncFound = false;
        for (MiriamData md : element.getMiriamData()) {
          if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
            targetsMiriam.add(md);
            hgncFound = true;
          } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
            // ad also entrez in case of mouse, rat, etc
            targetsMiriam.add(md);
          }

        }
        if (!hgncFound) {
          MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
          try {
            if (hgncAnnotator.isValidHgncMiriam(md)) {
              targetsMiriam.add(md);
            }
          } catch (AnnotatorException e) {
            logger.error("Problem with accessing HGNC database", e);
          }
        }
      }
    }
    try {
      mirnaList = miRNAParser.getMiRnaListByTargets(targetsMiriam);
    } catch (MiRNASearchException e) {
      logger.error("Problem with accessing mirna database", e);
    }

    for (MiRNA mirna : mirnaList) {
      removeUnknownOrganisms(mirna, searchCriteria.getOrganisms());
    }

    Collections.sort(mirnaList, new MiRNA.NameComparator());
    return mirnaList;
  }

  @Override
  public void cacheDataForModel(Model originalModel, IProgressUpdater iProgressUpdater) {
    logger.debug("Caching mirna queries...");
    double progress = 0.0;
    iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
    Set<MiriamData> targetMiriams = new HashSet<>();
    List<Model> models = new ArrayList<>();
    models.add(originalModel);
    models.addAll(originalModel.getSubmodels());
    for (Model model : models) {
      for (BioEntity element : model.getBioEntities()) {
        for (MiriamData md : element.getMiriamData()) {
          if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
            targetMiriams.add(md);
          } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
            targetMiriams.add(md);
          }
        }
      }
    }
    double counter = 0.0;
    int step = Math.max(targetMiriams.size() / 100, 1);
    List<MiriamData> currentList = new ArrayList<>();
    for (MiriamData md : targetMiriams) {
      counter += 1;
      currentList.add(md);
      if (currentList.size() >= step) {
        logger.debug("check mirna target: " + currentList);
        try {
          List<MiRNA> chemicalList = miRNAParser.getMiRnaListByTargets(currentList);
          for (MiRNA chemical : chemicalList) {
            cacheMiriamData(chemical);
          }
        } catch (MiRNASearchException | AnnotatorException e) {
          logger.error("Problem with accessing info about mirna for target: " + md, e);
        }
        progress = counter / (double) targetMiriams.size();
        iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
        currentList.clear();
      }
    }
  }

  /**
   * Removes targets for unknown organisms from the mirna.
   *
   * @param mirna
   *          mirna from which we want to remove targets
   * @param organisms
   *          organisms that should be kept
   */
  private void removeUnknownOrganisms(MiRNA mirna, List<MiriamData> organisms) {
    if (organisms.size() > 0) {
      List<Target> toRemove = new ArrayList<Target>();
      for (Target target : mirna.getTargets()) {
        boolean remove = true;
        for (MiriamData organism : organisms) {
          if (target.getOrganism() == null) {
            remove = false;
          } else if (target.getOrganism().equals(organism)) {
            remove = false;
          }
        }
        if (remove) {
          logger.debug("Target " + target.getName() + " removed from list because results are limited to organisms: "
              + organisms);
          toRemove.add(target);
        }
      }
      mirna.getTargets().removeAll(toRemove);
    }
  }

}
