package lcsb.mapviewer.services.search.drug;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.statistics.SearchType;
import lcsb.mapviewer.services.interfaces.ISearchHistoryService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.DbSearchService;

/**
 * Implementation of the service that allows access information about drugs.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class DrugService extends DbSearchService implements IDrugService {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(DrugService.class);

  /**
   * Access point and parser for the online drugbank database.
   */
  private DrugbankHTMLParser drugBankParser;

  /**
   * Access point and parser for the online ChEMBL database.
   */
  private ChEMBLParser chEMBLParser;

  /**
   * Object used to get information about {@link MiriamType#HGNC} annotations and
   * transformations.
   */
  private HgncAnnotator hgncAnnotator;

  /**
   * Service that manages search history.
   */
  private ISearchHistoryService searchHistoryService;

  @Autowired
  public DrugService(DrugbankHTMLParser drugBankParser,
      ChEMBLParser chEMBLParser,
      HgncAnnotator hgncAnnotator,
      ISearchHistoryService searchHistoryService) {
    this.drugBankParser = drugBankParser;
    this.chEMBLParser = chEMBLParser;
    this.hgncAnnotator = hgncAnnotator;
    this.searchHistoryService = searchHistoryService;
  }

  /**
   * Removes targets for unknown organisms from the drug.
   * 
   * @param drug
   *          drug from which we want to remove targets
   * @param organisms
   *          organisms that should be kept
   */
  private void removeUnknownOrganisms(Drug drug, List<MiriamData> organisms) {
    if (organisms.size() > 0) {
      List<Target> toRemove = new ArrayList<Target>();
      for (Target target : drug.getTargets()) {
        boolean remove = true;
        for (MiriamData organism : organisms) {
          if (target.getOrganism() == null) {
            remove = false;
          } else if (target.getOrganism().equals(organism)) {
            remove = false;
          }
        }
        if (remove) {
          logger.debug(
              "Target " + drug.getName() + " removed from list because results are limited to organisms: " + organisms);
          toRemove.add(target);
        }
      }
      drug.getTargets().removeAll(toRemove);
    }
  }

  /**
   * Merges two drugs (typically from different databases) into one
   * representation.
   * 
   * @param drug
   *          first drug to merge
   * @param drug2
   *          second drug to merge
   * @return merged drug
   */
  private Drug mergeDrugs(Drug drug, Drug drug2) {
    if (drug == null) {
      return drug2;
    }
    if (drug2 == null) {
      return drug;
    }

    Drug result = new Drug(drug);
    result.getSources().addAll(drug2.getSources());
    if (result.getName() == null || result.getName().trim().equals("")) {
      result.setName(drug2.getName());
    }
    if (result.getDescription() == null || result.getDescription().trim().equals("")) {
      result.setDescription(drug2.getDescription());
    }
    for (String synonym : drug2.getSynonyms()) {
      if (!result.getSynonyms().contains(synonym)) {
        result.addSynonym(synonym);
      }
    }
    for (String brandName : drug2.getBrandNames()) {
      if (!result.getBrandNames().contains(brandName)) {
        result.addBrandName(brandName);
      }
    }

    if (result.getBloodBrainBarrier().equals("YES")) {
      if (drug2.getBloodBrainBarrier().equals("NO")) {
        logger.warn("Problem with merging drugs. Blood Brain Barrier contains opposite values: "
            + result.getBloodBrainBarrier() + ", " + drug2.getBloodBrainBarrier());
        result.setBloodBrainBarrier("N/A");
      }
    } else if (result.getBloodBrainBarrier().equals("NO")) {
      if (drug2.getBloodBrainBarrier().equals("YES")) {
        logger.warn("Problem with merging drugs. Blood Brain Barrier contains opposite values: "
            + result.getBloodBrainBarrier() + ", " + drug2.getBloodBrainBarrier());
        result.setBloodBrainBarrier("N/A");
      }
    } else if (result.getBloodBrainBarrier().equals("N/A")) {
      result.setBloodBrainBarrier(drug.getBloodBrainBarrier());
    } else {
      logger.warn("Unknown status of drug BBB:" + result.getBloodBrainBarrier());
    }

    if (result.getApproved() == null) {
      result.setApproved(drug2.getApproved());
    } else if (drug2.getApproved() != null) {
      if (!drug2.getApproved().equals(result.getApproved())) {
        logger.warn("Problem with merging drugs. Approved contains opposite values: " + result.getApproved() + ", "
            + drug2.getApproved());
        result.setApproved(null);
      }
    }

    result.getTargets().addAll(drug2.getTargets());
    return result;
  }

  @Override
  public Drug getByName(String name, DbSearchCriteria searchCriteria) {
    if (name.trim().equals("")) {
      return null;
    }
    DrugAnnotation secondParser = chEMBLParser;
    Drug drug = null;
    try {
      drug = drugBankParser.findDrug(name);
      
      if (drug==null) {
        drug = chEMBLParser.findDrug(name);
        secondParser = drugBankParser;
      }
      if (drug != null) {
        removeUnknownOrganisms(drug, searchCriteria.getOrganisms());
      }
    } catch (DrugSearchException e) {
      logger.error("Problem with accessing drugBank parser", e);
    }
    try {
      Set<String> foundDrugs = new HashSet<String>();
      Set<String> searchNames = new HashSet<String>();
      // two step searching in Chembl:
      // * search for name
      searchNames.add(name);
      if (drug != null && drug.getName() != null) {
        // * search for name of a drug in drugbank
        searchNames.add(drug.getName());
        // * search for all synonyms found in drugbank
        // searchNames.addAll(drug.getSynonyms());
      }
      for (String string : searchNames) {
        Drug drug2 = secondParser.findDrug(string);
        if (drug2 != null) {
          removeUnknownOrganisms(drug2, searchCriteria.getOrganisms());
          // don't add drugs that were already found
          if (!foundDrugs.contains(drug2.getSources().get(0).getResource())) {
            drug = mergeDrugs(drug, drug2);
            foundDrugs.add(drug2.getSources().get(0).getResource());
          }
        }
      }
    } catch (DrugSearchException e) {
      logger.error("Problem with accessing ChEMBL parser", e);
    }
    if (searchCriteria.getIpAddress() != null && searchCriteria.getProject() != null) {
      searchHistoryService.addQuery(name, SearchType.GENERAL, searchCriteria.getIpAddress(),
          searchCriteria.getProject().getProjectId());
    }

    return drug;
  }

  @Override
  public List<Drug> getForTargets(Collection<Element> targets, DbSearchCriteria searchCriteria) {
    List<Drug> drugList = new ArrayList<>();
    Set<MiriamData> targetsMiriam = new HashSet<>();
    for (Element target : targets) {
      if (target instanceof Protein || target instanceof Gene || target instanceof Rna) {
        boolean hgncFound = false;
        for (MiriamData md : target.getMiriamData()) {
          if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
            targetsMiriam.add(md);
            hgncFound = true;
          }
        }
        if (!hgncFound) {
          MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, target.getName());
          try {
            if (hgncAnnotator.isValidHgncMiriam(md)) {
              targetsMiriam.add(md);
            }
          } catch (AnnotatorException e) {
            logger.error("Problem with accessing HGNC database", e);
          }
        }
      }
    }
    try {
      List<Drug> drugs = drugBankParser.getDrugListByTargets(targetsMiriam, searchCriteria.getOrganisms());
      drugList.addAll(drugs);
    } catch (DrugSearchException e) {
      logger.error("Problem with accessing drugBank parser", e);
    }

    try {
      List<Drug> drugs = chEMBLParser.getDrugListByTargets(targetsMiriam, searchCriteria.getOrganisms());
      drugList.addAll(drugs);
    } catch (DrugSearchException e) {
      logger.error("Problem with accessing chembl parser", e);
    }

    for (Drug drug : drugList) {
      removeUnknownOrganisms(drug, searchCriteria.getOrganisms());
    }

    Collections.sort(drugList, new Drug.NameComparator());
    return drugList;
  }

  @Override
  public void cacheDataForModel(Model originalModel, IProgressUpdater iProgressUpdater) {
    logger.debug("Caching drug queries...");
    double progress = 0.0;
    iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
    MiriamData disease = originalModel.getProject().getDisease();
    if (disease != null) {
      Set<MiriamData> targetMiriams = new HashSet<>();
      List<Model> models = new ArrayList<>();
      models.add(originalModel);
      models.addAll(originalModel.getSubmodels());
      for (Model model : models) {
        for (BioEntity element : model.getBioEntities()) {
          for (MiriamData md : element.getMiriamData()) {
            if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
              targetMiriams.add(md);
            }
          }
        }
      }
      double counter = 0.0;
      List<MiriamData> organisms = new ArrayList<>();
      for (MiriamData md : targetMiriams) {
        try {
          List<Drug> chemicalList = drugBankParser.getDrugListByTarget(md, organisms);
          for (Drug chemical : chemicalList) {
            cacheMiriamData(chemical);
          }
        } catch (DrugSearchException | AnnotatorException e) {
          logger.error("Problem with accessing info about drugbank for target: " + md, e);
        }
        try {
          List<Drug> chemicalList = chEMBLParser.getDrugListByTarget(md, organisms);
          for (Drug chemical : chemicalList) {
            cacheMiriamData(chemical);
          }
        } catch (DrugSearchException | AnnotatorException e) {
          logger.error("Problem with accessing info about chembl for target: " + md, e);
        }
        counter += 1;
        progress = counter / (double) targetMiriams.size();
        iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
      }
    }
  }

  /**
   * @return the drugBankParser
   */
  public DrugbankHTMLParser getDrugBankParser() {
    return drugBankParser;
  }

  /**
   * @param drugBankParser
   *          the drugBankParser to set
   */
  public void setDrugBankParser(DrugbankHTMLParser drugBankParser) {
    this.drugBankParser = drugBankParser;
  }

  /**
   * @return the chEMBLParser
   */
  public ChEMBLParser getChEMBLParser() {
    return chEMBLParser;
  }

  /**
   * @param chEMBLParser
   *          the chEMBLParser to set
   */
  public void setChEMBLParser(ChEMBLParser chEMBLParser) {
    this.chEMBLParser = chEMBLParser;
  }

  /**
   * @return the searchHistoryService
   */
  public ISearchHistoryService getSearchHistoryService() {
    return searchHistoryService;
  }

  /**
   * @param searchHistoryService
   *          the searchHistoryService to set
   */
  public void setSearchHistoryService(ISearchHistoryService searchHistoryService) {
    this.searchHistoryService = searchHistoryService;
  }

  @Override
  public List<String> getSuggestedQueryList(Project project, MiriamData organism) throws DrugSearchException {
    Set<String> resultSet = new HashSet<>();
    for (String string : drugBankParser.getSuggestedQueryList(project, organism)) {
      resultSet.add(string.toLowerCase());
    }
    for (String string : chEMBLParser.getSuggestedQueryList(project, organism)) {
      resultSet.add(string.toLowerCase());
    }

    List<String> result = new ArrayList<>();
    result.addAll(resultSet);
    Collections.sort(result);
    return result;
  }
}
