package lcsb.mapviewer.services.overlay;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class AnnotatedObjectTreeRowTest extends ServiceTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new AnnotatedObjectTreeRow(new ClassTreeNode(Object.class)));
  }

}
