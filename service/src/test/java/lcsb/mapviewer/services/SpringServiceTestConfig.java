package lcsb.mapviewer.services;

import org.springframework.context.annotation.*;
import org.springframework.security.core.session.SessionRegistryImpl;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringServiceConfig.class, SpringPersistConfig.class, SpringAnnotationConfig.class })
public class SpringServiceTestConfig {

  @Bean
  public SessionRegistryImpl sessionRegistry() {
    return new SessionRegistryImpl();
  }

}
