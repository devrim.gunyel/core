package lcsb.mapviewer.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.services.impl.AllImplServiceTests;
import lcsb.mapviewer.services.overlay.AllOverlayTests;
import lcsb.mapviewer.services.search.AllSearchTests;
import lcsb.mapviewer.services.utils.AllUtilsTests;

@RunWith(Suite.class)
@SuiteClasses({ AllImplServiceTests.class,
    AllOverlayTests.class,
    AllSearchTests.class,
    AllUtilsTests.class,
})
public class AllServicesTests {

};