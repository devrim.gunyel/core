package lcsb.mapviewer.services.search.chemical;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;

@Ignore("ctd decided to block us due to too many requests")
public class ChemicalServiceTest extends ServiceTestFunctions {
  Logger logger = LogManager.getLogger(ChemicalServiceTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testgetChemicalByName() throws Exception {
    MiriamData pdDiseaseID = new MiriamData(MiriamType.MESH_2012, "D010300");
    String name = "GSSG";
    Chemical result = chemicalService.getByName(name, new DbSearchCriteria().disease(pdDiseaseID));
    assertNotNull(result);
    assertEquals("D019803", result.getChemicalId().getResource());

    assertEquals("No warnings expected.", 0, getWarnings().size());
  }

  @Test
  public void testSearchByElements() throws Exception {
    List<Element> elements = new ArrayList<>();
    Rna protein = new Rna("id");
    protein.setName("GCH1");
    MiriamData dystoniaDisease = new MiriamData(MiriamType.MESH_2012, "C538007");

    elements.add(protein);

    List<Chemical> chemicals = chemicalService.getForTargets(elements,
        new DbSearchCriteria().disease(dystoniaDisease));
    assertNotNull(chemicals);
    assertTrue(chemicals.size() > 0);

    assertEquals("No warnings expected.", 0, getWarnings().size());
  }

}
