package lcsb.mapviewer.services.search.mirna;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;

public class MiRNAServiceTest extends ServiceTestFunctions {
  @Autowired
  protected IMiRNAService miRNAService;
  Logger logger = LogManager.getLogger(MiRNAServiceTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCacheStub() throws Exception {
    Model model = new ModelFullIndexed(null);
    miRNAService.cacheDataForModel(model, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        logger.debug("Progress: " + progress);
      }
    });
  }

  @Test
  public void testFindMiRNAFilteredByOrganism() throws Exception {
    MiRNA test = miRNAService.getByName("hsa-miR-29a-3p",
        new DbSearchCriteria().organisms(TaxonomyBackend.HUMAN_TAXONOMY));
    assertNotNull(test);
    for (Target t : test.getTargets()) {
      for (MiriamData md : t.getGenes()) {
        assertEquals("Only hgnc symbols should be used for gene annotations", MiriamType.HGNC_SYMBOL,
            md.getDataType());
      }
    }
  }

}
