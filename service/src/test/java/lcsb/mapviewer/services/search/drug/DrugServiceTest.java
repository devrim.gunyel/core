package lcsb.mapviewer.services.search.drug;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;

public class DrugServiceTest extends ServiceTestFunctions {
  Logger logger = LogManager.getLogger(DrugServiceTest.class);

  @Autowired
  ElementDao elementDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetEmpty() throws Exception {
    Drug drug = drugService.getByName("blablablabla", new DbSearchCriteria());
    assertNull(drug);
  }

  @Test
  public void testGetIstodax() throws Exception {
    try {
      Drug drug = drugService.getByName("istodax", new DbSearchCriteria());
      assertEquals(2, drug.getSources().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargets() throws Exception {
    long count = searchHistoryDao.getCount();
    Model model = new ModelFullIndexed(null);
    Project project = new Project();
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    project.setProjectId("TesT");
    project.addModel(model);
    Drug drug = drugService.getByName("Diazoxide", new DbSearchCriteria().project(project).ipAddress("ip"));
    assertNotNull(drug);
    assertNotNull(drug.getName());
    assertFalse(drug.getName().trim().equals(""));
    assertNotNull(drug.getDescription());
    assertFalse(drug.getDescription().trim().equals(""));

    String hgnc1 = "SLC12A3";
    String hgnc2 = "KCNJ8";

    boolean hgnc1Exists = false;
    boolean hgnc2Exists = false;

    for (Target target : drug.getTargets()) {
      for (MiriamData row : target.getGenes()) {
        if (row.getResource().equalsIgnoreCase(hgnc1))
          hgnc1Exists = true;
        if (row.getResource().equalsIgnoreCase(hgnc2))
          hgnc2Exists = true;
      }
    }

    assertTrue("Target " + hgnc1 + " doesn't exist in target list but should", hgnc1Exists);
    assertTrue("Target " + hgnc2 + " doesn't exist in target list but should", hgnc2Exists);

    long count2 = searchHistoryDao.getCount();

    assertEquals(count + 1, count2);

    assertEquals("YES", drug.getBloodBrainBarrier());
  }

  @Test
  public void testGetTargetsBySynonym() throws Exception {
    // search by synonym
    Drug drug = drugService.getByName("Amantidine", new DbSearchCriteria().ipAddress("ip"));
    // search by name
    Drug drug2 = drugService.getByName("Amantadine", new DbSearchCriteria().ipAddress("ip"));
    assertNotNull(drug);
    assertNotNull(drug.getName());
    assertFalse(drug.getName().trim().equals(""));
    assertNotNull(drug.getDescription());
    assertFalse(drug.getDescription().trim().equals(""));

    // number of targets should be the same
    assertEquals(drug.getTargets().size(), drug2.getTargets().size());
    assertTrue(drug.getTargets().size() > 0);
  }

  @Test
  public void testFindDrugSelegiline() throws Exception {
    Drug test = drugService.getByName("Selegiline",
        new DbSearchCriteria().ipAddress("ip").organisms(TaxonomyBackend.HUMAN_TAXONOMY));
    assertNotNull(test);
  }

  @Test
  public void testAnnotationsInDrug() throws Exception {
    Drug test = drugService.getByName("Selegiline", new DbSearchCriteria().organisms(TaxonomyBackend.HUMAN_TAXONOMY));
    assertNotNull(test);
    for (Target target : test.getTargets()) {
      for (MiriamData md : target.getGenes()) {
        assertNull(md.getAnnotator());
      }
    }
  }

  @Test
  public void testDornaseAplha() throws Exception {
    Drug drug = drugService.getByName("Dornase alpha", new DbSearchCriteria());
    assertNotNull(drug.getName());
    assertEquals("N/A", drug.getBloodBrainBarrier());
  }

  @Test
  public void testRapamycin() throws Exception {
    Drug drug = drugService.getByName("Rapamycin", new DbSearchCriteria());
    assertNotNull(drug.getName());
    assertEquals("NO", drug.getBloodBrainBarrier());
  }

  @Test
  public void testSearchByElements() throws Exception {
    List<Element> elements = new ArrayList<>();
    List<Drug> drugs = drugService.getForTargets(elements, new DbSearchCriteria());
    assertNotNull(drugs);
    assertEquals(0, drugs.size());

    Protein protein = new GenericProtein("id");
    protein.setName("DRD2");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    elements.add(protein);

    drugs = drugService.getForTargets(elements, new DbSearchCriteria());
    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test
  public void testSearchByElements3() throws Exception {
    List<Element> elements = new ArrayList<>();
    Protein protein = new GenericProtein("id");
    protein.setName("SIRT3");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    protein.addMiriamData(new MiriamData(MiriamType.ENTREZ, "23410"));
    elements.add(protein);
    List<Drug> drugs = drugService.getForTargets(elements, new DbSearchCriteria());

    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test
  public void testObjectToDrugTargetList() throws Exception {
    Drug drug = drugService.getByName("AMANTADINE", new DbSearchCriteria());
    Drug drug2 = drugBankHTMLParser.findDrug("AMANTADINE");

    assertTrue(drug2.getTargets().size() <= drug.getTargets().size());
  }

  @Test
  public void testAspirinToDrugTargetList() throws Exception {
    Drug drug = drugService.getByName("Aspirin", new DbSearchCriteria());
    Drug drug2 = drugBankHTMLParser.findDrug("Aspirin");

    assertTrue(drug2.getTargets().size() <= drug.getTargets().size());
  }

  @Test
  public void testAspirinSynonyms() throws Exception {
    Drug drug = drugService.getByName("Aspirin", new DbSearchCriteria());

    Set<String> synonyms = new HashSet<String>();
    for (String string : drug.getSynonyms()) {
      assertFalse("Duplicate entry in drug synonym: " + string, synonyms.contains(string));
      synonyms.add(string);
    }
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    List<String> result = drugService.getSuggestedQueryList(new Project(), null);
    assertEquals(0, result.size());
  }

}
