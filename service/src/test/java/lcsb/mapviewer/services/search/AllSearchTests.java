package lcsb.mapviewer.services.search;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.services.search.chemical.ChemicalServiceTest;
import lcsb.mapviewer.services.search.drug.AllSearchDrugTests;
import lcsb.mapviewer.services.search.mirna.MiRNAServiceTest;

@RunWith(Suite.class)
@SuiteClasses({ AllSearchDrugTests.class,
    ChemicalServiceTest.class,
    DbSearchServiceTest.class,
    ElementMatcherTest.class,
    MiRNAServiceTest.class,
})
public class AllSearchTests {

}
