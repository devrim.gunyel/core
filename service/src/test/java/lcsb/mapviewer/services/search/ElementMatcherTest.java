package lcsb.mapviewer.services.search;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.*;

import lcsb.mapviewer.annotation.data.*;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class ElementMatcherTest extends ServiceTestFunctions {

  ElementMatcher elementMatcher = new ElementMatcher();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testMatchOther() {
    Target target = new Target();

    target.setType(TargetType.OTHER);
    assertFalse(elementMatcher.elementMatch(target, new GenericProtein("s1")));
  }

  @Test
  @Ignore("ctd decided to block us due to too many requests")
  public void testElementMatch() {
    String geneName = "GCH1";
    MiriamData dystoniaDisease = new MiriamData(MiriamType.MESH_2012, "C538007");
    Chemical view = chemicalService.getByName("Levodopa", new DbSearchCriteria().disease(dystoniaDisease));
    Target target = null;
    for (Target t : view.getInferenceNetwork()) {
      for (MiriamData row : t.getGenes()) {
        if (row.getResource().equals(geneName)) {
          target = t;
        }
      }
    }

    Element element = new Rna("id");
    element.setName(geneName);

    assertTrue(elementMatcher.elementMatch(target, element));

  }

}
