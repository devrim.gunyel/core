package lcsb.mapviewer.services.utils;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

public class ColorSchemaReaderTest extends ServiceTestFunctions {
  Logger logger = LogManager.getLogger(ColorSchemaReaderTest.class);

  ColorSchemaReader reader;

  @Before
  public void setUp() throws Exception {
    reader = new ColorSchemaReader();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testReadSchema() throws Exception {
    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/enricoData/ageing.txt");

    assertNotNull(schemas);
    assertEquals(412, schemas.size());

    schemas = reader.readColorSchema("testFiles/enricoData/ge001.txt");

    assertNotNull(schemas);
    assertEquals(3057, schemas.size());

    schemas = reader.readColorSchema("testFiles/enricoData/ge005.txt");

    assertNotNull(schemas);
    assertEquals(4338, schemas.size());
  }

  @Test
  public void testReadGeneVariantsSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<ColorSchema> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testReadGeneVariantByIdSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_id.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<ColorSchema> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(1, schemas.size());
  }

  private byte[] fileToByteArray(File f) throws FileNotFoundException, IOException {
    InputStream in = new FileInputStream(f);

    byte[] buff = new byte[8000];

    int bytesRead = 0;

    ByteArrayOutputStream bao = new ByteArrayOutputStream();

    while ((bytesRead = in.read(buff)) != -1) {
      bao.write(buff, 0, bytesRead);
    }
    in.close();
    bao.close();

    byte[] data = bao.toByteArray();
    return data;
  }

  @Test
  public void testReadGeneVariantsWithAminoAcidChange() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_aa_change.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<ColorSchema> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(2, schemas.size());
    GeneVariationColorSchema schema = (GeneVariationColorSchema) schemas.iterator().next();
    boolean aaChangeFound = false;
    for (GeneVariation geneVariant : schema.getGeneVariations()) {
      if (geneVariant.getAminoAcidChange() != null && !geneVariant.getAminoAcidChange().trim().isEmpty()) {
        aaChangeFound = true;
      }
    }
    assertTrue("Amino acid are not present in parsed data", aaChangeFound);
  }

  @Test
  public void testReadGeneVariantsWithNewNamingSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_new_naming.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<ColorSchema> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testReadGeneVariantsSchemaWithAF() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_all_freq.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<ColorSchema> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(2, schemas.size());
  }

  @Test(expected = InvalidColorSchemaException.class)
  public void testReadInvalidGeneVariantsSchema() throws Exception {
    reader.readColorSchema("testFiles/coloring/gene_variants_invalid_genome.txt");
  }

  @Test
  public void testReadSchema2() throws Exception {
    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/goodSchema.txt");

    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test(expected = InvalidColorSchemaException.class)
  public void testReadSchema3() throws Exception {
    reader.readColorSchema("testFiles/coloring/wrongSchema.txt");
  }

  @Test
  public void testProblematicStephanSchema3() throws Exception {
    reader.readColorSchema("testFiles/coloring/problematicSchema.txt");
  }

  @Test
  public void testReadReactionSchema() throws Exception {
    Collection<ColorSchema> collection = reader.readColorSchema("testFiles/coloring/reactionSchema.txt");
    assertEquals(1, collection.size());
    ColorSchema schema = collection.iterator().next();
    assertEquals("re1", schema.getElementId());
    assertEquals(3.0, schema.getLineWidth(), EPSILON);
    assertEquals(Color.RED, schema.getColor());
  }

  @Test(timeout = 15000)
  public void testNextVersionReadSchema() throws Exception {
    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/goodLayout.v=1.0.txt");

    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testColoring3() throws Exception {
    ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

    Model model = getModelForFile("testFiles/coloring/protein_to_color.xml", false);

    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/problematicSchema.txt");
    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    factory.execute();

    assertFalse(model.getElementByElementId("sa1").getFillColor().equals(Color.WHITE));
  }

  @Test
  public void testColoringWithValueOrColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\n" +
        "s1\t#ff0000\t\n" +
        "s2\t\t1.0\n";
    Collection<ColorSchema> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(2, schemas.size());
  }

  @Test
  public void testElementsByType() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "type\tname\tvalue\n" +
        "protein\t\t1.0\n";
    Collection<ColorSchema> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
  }

  @Test(expected = InvalidColorSchemaException.class)
  public void testColoringWithInvalidValueAndColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\ns1\t#ff0000\t1.0";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test(expected = InvalidColorSchemaException.class)
  public void testColoringWithEmptyColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\ns1\tx";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test
  public void testColoringWithInvalidValueAndColor2() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\ns1\t\t";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test
  public void testSchemasWithId() throws Exception {
    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/schemaWithIdentifiers.txt");
    for (ColorSchema colorSchema : schemas) {
      for (MiriamData md : colorSchema.getMiriamData()) {
        assertNotNull(md.getResource());
        assertFalse(md.getResource().isEmpty());
      }
    }
  }

  @Test
  public void testSchemasWithIdSnakeCase() throws Exception {
    Collection<ColorSchema> schemas = reader
        .readColorSchema("testFiles/coloring/schema_with_identifiers_in_snake_case.txt");
    for (ColorSchema colorSchema : schemas) {
      for (MiriamData md : colorSchema.getMiriamData()) {
        assertNotNull(md.getResource());
        assertFalse(md.getResource().isEmpty());
      }
    }
  }

  @Test
  public void testSchemasWithEmptyNameAndId() throws Exception {
    Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/schemaIdWithoutName.txt");
    for (ColorSchema colorSchema : schemas) {
      for (MiriamData md : colorSchema.getMiriamData()) {
        assertFalse(md.getResource().isEmpty());
        assertNull(colorSchema.getName());
      }
    }
  }

  @Test
  public void testSimpleNameSchemas() throws Exception {
    String input = "#header\ns1\ns2\n";
    Collection<ColorSchema> schemas = reader
        .readSimpleNameColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
    assertEquals(2, schemas.size());
  }

  @Test
  public void testParseSpeciesTypesForEmptyString() throws Exception {
    List<Class<? extends Element>> classes = reader.parseSpeciesTypes("", null);
    assertEquals(0, classes.size());
  }

  @Test
  public void testColoringWithModelName() throws Exception {
    String input = "name\tcolor\tmodel_name\n" +
        "s1\t#ff0000\txxx\n";
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    Collection<ColorSchema> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
    assertNotNull(schemas.iterator().next().getModelName());
  }

  @Test
  public void testColoringWithMapName() throws Exception {
    String input = "name\tcolor\tmap_name\n" +
        "s1\t#ff0000\txxx\n";
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    Collection<ColorSchema> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
    assertNotNull(schemas.iterator().next().getModelName());
  }

  @Test
  public void testGetDeprecatedColumns() throws Exception {
    Layout overlay = new Layout();
    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent("blabla\nbasd\n".getBytes("UTF-8"));
    overlay.setInputData(file);
    assertEquals(0, reader.getDeprecatedColumns(overlay).size());
  }

  @Test
  public void testGetDeprecatedColumnsForValidGeneticColumns() throws Exception {
    Layout overlay = new Layout();
    overlay.setColorSchemaType(ColorSchemaType.GENETIC_VARIANT);
    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent((ColorSchemaColumn.POSITION.name() + "\tgene_name\n").getBytes("UTF-8"));
    overlay.setInputData(file);
    assertEquals(0, reader.getDeprecatedColumns(overlay).size());
  }

  @Test
  public void testGetDeprecatedColumnsForDeprecatedGeneticColumns() throws Exception {
    Layout overlay = new Layout();
    overlay.setColorSchemaType(ColorSchemaType.GENETIC_VARIANT);
    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent((ColorSchemaColumn.POSITION.name() + "\tname\n").getBytes("UTF-8"));
    overlay.setInputData(file);
    assertEquals(1, reader.getDeprecatedColumns(overlay).size());
  }

  @Test
  public void testGetDeprecatedColumnsForOverlayWithHeader() throws Exception {
    Layout overlay = new Layout();
    UploadedFileEntry file = new UploadedFileEntry();
    String content = "#" + ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE + "=" + ColorSchemaType.GENERIC + "\n" +
        ColorSchemaColumn.VALUE.name() + "\tname\n";
    file.setFileContent((content).getBytes("UTF-8"));
    overlay.setInputData(file);
    assertEquals(0, reader.getDeprecatedColumns(overlay).size());
  }

  @Test(expected = InvalidColorSchemaException.class)
  public void testReadColorSchemaWithInvalidType() throws Exception {
    FileInputStream fis = new FileInputStream("testFiles/coloring/invalidType.txt");
    FileInputStream fis2 = new FileInputStream("testFiles/coloring/invalidType.txt");
    ColorSchemaReader reader = new ColorSchemaReader();
    reader.readColorSchema(fis, TextFileUtils.getHeaderParametersFromFile(fis2));

  }
}
