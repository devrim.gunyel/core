package lcsb.mapviewer.services.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ColorSchemaReaderTest.class,
    EmailSenderTest.class,
})
public class AllUtilsTests {

}
