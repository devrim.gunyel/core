package lcsb.mapviewer.services;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.DrugbankHTMLParser;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.persist.dao.map.statistics.SearchHistoryDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.*;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import lcsb.mapviewer.services.search.drug.IDrugService;

@Transactional
@Rollback(false)
@ContextConfiguration(classes = SpringServiceTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class ServiceTestFunctions {

  protected static String ADMIN_BUILT_IN_LOGIN = "admin";

  private static Map<String, Model> models = new HashMap<String, Model>();
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  public double EPSILON = 1e-6;
  @Autowired
  protected DrugbankHTMLParser drugBankHTMLParser;
  @Autowired
  protected ModelAnnotator modelAnnotator;
  @Autowired
  protected IConfigurationService configurationService;

  @Autowired
  protected IModelService modelService;

  @Autowired
  protected ILayoutService layoutService;

  @Autowired
  protected SearchHistoryDao searchHistoryDao;

  @Autowired
  protected PasswordEncoder passwordEncoder;

  @Autowired
  protected IUserService userService;

  @Autowired
  protected ISearchService searchService;

  @Autowired
  protected IDrugService drugService;

  @Autowired
  protected IChemicalService chemicalService;

  @Autowired
  protected IProjectService projectService;

  @Autowired
  protected ProjectDao projectDao;

  @Autowired
  protected ModelDao modelDao;

  @Autowired
  protected ICommentService commentService;

  @Autowired
  protected UserDao userDao;

  @Autowired
  protected CommentDao commentDao;

  @Autowired
  protected DbUtils dbUtils;
  private Logger logger = LogManager.getLogger(ServiceTestFunctions.class);
  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
    dbUtils.setAutoFlush(false);
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrors() {
    return appender.getErrors();
  }

  protected List<LogEvent> getInfos() {
    return appender.getInfos();
  }

  protected User createUser() {
    return createUser("john.doe");
  }

  protected User createUser(String login) {
    User user = userDao.getUserByLogin(login);
    if (user != null) {
      logger.debug("remove user");
      userDao.delete(user);
      userDao.flush();
    }
    user = new User();
    user.setName("John");
    user.setSurname("Doe");
    user.setEmail("john.doe@uni.lu");
    user.setLogin(login);
    user.setCryptedPassword(passwordEncoder.encode("passwd"));
    userDao.add(user);
    return user;
  }

  protected Model getModelForFile(String fileName, boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      return new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
    }
    Model result = ServiceTestFunctions.models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: " + fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      ServiceTestFunctions.models.put(fileName, result);
    }
    return result;
  }

  public File createTempDirectory() throws IOException {
    final File temp;

    temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

    if (!(temp.delete())) {
      throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
    }

    if (!(temp.mkdir())) {
      throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
    }

    return (temp);
  }
}
