package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.SimpleMessage;
import org.apache.poi.util.IOUtils;
import org.junit.*;
import org.mockito.Mockito;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.converter.ComplexZipConverter;
import lcsb.mapviewer.converter.ComplexZipConverterParams;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.*;
import lcsb.mapviewer.model.*;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.layout.DataOverlayImageLayer;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.overlay.AnnotatedObjectTreeRow;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.data.BuildInLayout;

@Rollback(true)
public class ProjectServiceTest extends ServiceTestFunctions {

  private final String tmpResultDir = "tmp/";
  @Autowired
  ElementDao aliasDao;
  @Autowired
  LayoutDao layoutDao;
  private Logger logger = LogManager.getLogger(ProjectServiceTest.class);
  private ZipEntryFileFactory zefFactory = new ZipEntryFileFactory();
  private String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAllProjects() {
    List<Project> projects = projectService.getAllProjects();

    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectDao.evict(project);

    List<Project> projects2 = projectService.getAllProjects();

    assertEquals(projects.size() + 1, projects2.size());

    for (Project tmpProject : projects2) {
      if (!projects.contains(tmpProject)) {
        assertEquals(projectId, tmpProject.getProjectId());
      }
    }

    Project project2 = projectDao.getProjectByProjectId(projectId);
    assertNotNull(project2);
    assertNotEquals(project2, project);
    assertEquals(project.getId(), project2.getId());

    projectDao.delete(project2);
  }

  @Test
  public void testUpdater() throws Exception {
    projectService
        .createProject(createProjectParams("testFiles/centeredAnchorInModifier.xml").analyzeAnnotations(true));
    Project project = projectService.getProjectByProjectId(projectId);
    assertNotNull(project);
    assertEquals(ProjectStatus.DONE, project.getStatus());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateComplex() throws Exception {
    try {
      ZipEntryFile entry1 = new ModelZipEntryFile("main.xml", "main", true, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry2 = new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry3 = new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry4 = new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry5 = new ModelZipEntryFile("mapping.xml", "mapping", false, true, SubmodelType.UNKNOWN);
      projectService.createProject(createProjectParams("testFiles/complexModel/complex_model.zip").addZipEntry(entry1)
          .addZipEntry(entry2).addZipEntry(entry3).addZipEntry(entry4).addZipEntry(entry5)
          .semanticZoomContainsMultipleLayouts(true).images(true));
      Project project = projectService.getProjectByProjectId(projectId);

      Model model = modelService.getLastModelByProjectId(projectId);
      assertNotNull(model);
      assertEquals("main", model.getName());
      assertEquals(3, model.getSubmodelConnections().size());

      Model s1Model = null;
      Model s2Model = null;
      Model s3Model = null;

      for (ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
        if (submodel.getName().equals("s1")) {
          s1Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s2")) {
          s2Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s3")) {
          s3Model = submodel.getSubmodel().getModel();
        }
      }
      assertNotNull(s1Model);
      assertNotNull(s2Model);
      assertNotNull(s3Model);

      Element al1 = model.getElementByElementId("sa1");
      assertNotNull(al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, al1.getSubmodel().getType());
      assertEquals(s1Model, al1.getSubmodel().getSubmodel().getModel());

      Element al2 = model.getElementByElementId("sa2");
      assertNull(al2.getSubmodel());

      Element al4 = model.getElementByElementId("sa4");
      assertNotNull(al4.getSubmodel());
      assertEquals(SubmodelType.PATHWAY, al4.getSubmodel().getType());
      assertEquals(s2Model, al4.getSubmodel().getSubmodel().getModel());

      Element s1_al1 = s1Model.getElementByElementId("sa1");
      assertNotNull(s1_al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, s1_al1.getSubmodel().getType());
      assertEquals(s3Model, s1_al1.getSubmodel().getSubmodel().getModel());

      assertEquals(ProjectStatus.DONE, project.getStatus());

      Collection<Layout> layouts = layoutDao.getLayoutsByProject(project);

      // now check layouts
      for (Layout l : layouts) {
        if (l.getInputData() == null) {
          assertEquals(model.getSubmodels().size() + 1, l.getDataOverlayImageLayers().size());
        } else {
          assertEquals(0, l.getDataOverlayImageLayers().size());
        }
      }

      // and now check layouts (check if every submodel have them, and point
      // into different directory)

      int semanticOverlays = model.getZoomLevels() + 1;
      int overlays = BuildInLayout.values().length + semanticOverlays;
      assertEquals(overlays, layouts.size());
      Set<String> directories = new HashSet<>();
      for (DataOverlayImageLayer imageLayer : layouts.iterator().next().getDataOverlayImageLayers()) {
        directories.add(imageLayer.getDirectory());
      }
      assertEquals(1 + model.getSubmodels().size(), directories.size());
    } finally {
      Project project = projectService.getProjectByProjectId(projectId);
      if (project != null) {
        projectService.removeProject(project, null, false);
      }
    }
  }

  @Test
  public void testGenerateImages() throws Exception {
    File f = createTempDirectory();
    String path = f.getAbsolutePath() + "/" + tmpResultDir;
    try {
      Model model = getModelForFile("testFiles/sample.xml", false);
      Project project = new Project();
      project.addModel(model);

      Layout layout = new Layout(BuildInLayout.NORMAL.getTitle(), false);
      layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, path + "/normal"));
      project.addLayout(layout);
      layout = new Layout(BuildInLayout.NESTED.getTitle(), false);
      layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, path + "/nested"));
      project.addLayout(layout);

      CreateProjectParams params = createProjectParams("testFiles/sample.xml").images(true).projectDir(tmpResultDir);

      ProjectService pService = new ProjectService(null, null, null, null, null, null, null, null, null, null, null,
          null,
          null, null, null);
      pService.setProjectDao(Mockito.mock(ProjectDao.class));

      pService.createImages(project, params);

      File tmp = new File(path + "/normal");
      assertTrue(tmp.exists());

      tmp = new File(path + "/nested");
      assertTrue(tmp.exists());
    } finally {
      f.delete();
    }

  }

  @Test
  public void testGenerateImagesInSubmodels() throws Exception {
    File f = createTempDirectory();
    String path = f.getAbsolutePath() + "/" + tmpResultDir;
    try {
      Model model = getModelForFile("testFiles/sample.xml", false);

      Model model2 = getModelForFile("testFiles/sample.xml", false);

      Project project = new Project();
      project.addModel(model);
      project.addModel(model2);

      Layout layout = new Layout(BuildInLayout.NORMAL.getTitle(), false);
      layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, path + "/normal_A"));
      project.addLayout(layout);
      layout = new Layout(BuildInLayout.NESTED.getTitle(), false);
      layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model2, path + "/nested_B"));
      project.addLayout(layout);

      model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.UNKNOWN, "name"));

      CreateProjectParams params = createProjectParams("testFiles/sample.xml").images(true).projectDir(tmpResultDir);

      ProjectService pService = new ProjectService(null, null, null, null, null, null, null, null, null, null, null,
          null, null, null, null);
      pService.setProjectDao(Mockito.mock(ProjectDao.class));

      pService.createImages(project, params);

      File tmp = new File(path + "/normal_A");
      assertTrue(tmp.exists());

      tmp = new File(path + "/nested_B");
      assertTrue(tmp.exists());
    } finally {
      f.delete();
    }

  }

  @Test
  public void testCopyComplexWithCompartments() throws Exception {
    ZipEntryFile entry1 = new ModelZipEntryFile("main.xml", "main", true, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry2 = new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry3 = new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry4 = new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry5 = new ModelZipEntryFile("mapping.xml", "mapping", false, true, SubmodelType.UNKNOWN);

    CreateProjectParams params = createProjectParams("testFiles/complexModel/complex_model_with_compartment.zip")
        .addZipEntry(entry1).addZipEntry(entry2).addZipEntry(entry3).addZipEntry(entry4).addZipEntry(entry5)
        .analyzeAnnotations(false);

    ComplexZipConverter parser = new ComplexZipConverter(CellDesignerXmlParser.class);
    ComplexZipConverterParams complexParams = new ComplexZipConverterParams().zipFile(params.getProjectFile());
    for (ZipEntryFile entry : params.getZipEntries()) {
      complexParams.entry(entry);
    }
    Model model = parser.createModel(complexParams);

    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(3, model.getSubmodelConnections().size());

    int oldSize = 0;
    for (Model m : model.getSubmodels()) {
      oldSize += m.getParentModels().size();
    }

    new CopyCommand(model).execute();

    int newSize = 0;
    for (Model m : model.getSubmodels()) {
      newSize += m.getParentModels().size();
    }
    assertEquals("Submodels doesn't match after copying", oldSize, newSize);
  }

  @Test
  public void testAnnotatorTree() throws Exception {
    TreeNode tree = projectService.createClassAnnotatorTree(null);
    assertNotNull(tree);

    AnnotatedObjectTreeRow dataNode = (AnnotatedObjectTreeRow) tree.getData();

    assertEquals(BioEntity.class, dataNode.getClazz());

    Queue<TreeNode> nodes = new LinkedList<TreeNode>();
    nodes.add(tree);
    boolean annotatorsFound = false;
    while (!nodes.isEmpty()) {
      TreeNode node = nodes.poll();
      dataNode = (AnnotatedObjectTreeRow) node.getData();
      if (dataNode.getValidAnnotators().size() > 0) {
        annotatorsFound = true;
      }
      for (TreeNode n : node.getChildren()) {
        nodes.add(n);
      }
    }
    assertTrue(annotatorsFound);
  }

  @Test
  public void testEmptyAnnotatorTreeForUser() throws Exception {
    User user = createUser();
    UserAnnotationSchema schema = modelAnnotator.createDefaultAnnotatorSchema();
    for (UserClassAnnotators classAnnotators : schema.getClassAnnotators()) {
      classAnnotators.setAnnotators(new ArrayList<>());
    }
    user.setAnnotationSchema(schema);
    userDao.update(user);

    TreeNode tree = projectService.createClassAnnotatorTree(user);
    assertNotNull(tree);

    AnnotatedObjectTreeRow dataNode = (AnnotatedObjectTreeRow) tree.getData();

    assertEquals(BioEntity.class, dataNode.getClazz());

    Queue<TreeNode> nodes = new LinkedList<TreeNode>();
    nodes.add(tree);
    boolean annotatorsFound = false;
    while (!nodes.isEmpty()) {
      TreeNode node = nodes.poll();
      dataNode = (AnnotatedObjectTreeRow) node.getData();
      assertEquals("Annotators found for class: " + dataNode.getClazz(), 0, dataNode.getUsedAnnotators().size());
      if (dataNode.getValidAnnotators().size() > 0) {
        annotatorsFound = true;
      }
      for (TreeNode n : node.getChildren()) {
        nodes.add(n);
      }
    }
    assertTrue(annotatorsFound);

    userDao.delete(user);
  }

  @Test
  public void testAnnotatorTreeForUser() throws Exception {
    User user = createUser();
    UserAnnotationSchema schema = new UserAnnotationSchema();
    schema.addClassAnnotator(
        new UserClassAnnotators(Protein.class, modelAnnotator.getDefaultAnnotators(Protein.class)));
    user.setAnnotationSchema(schema);
    userDao.update(user);

    TreeNode tree = projectService.createClassAnnotatorTree(user);
    assertNotNull(tree);

    AnnotatedObjectTreeRow dataNode = (AnnotatedObjectTreeRow) tree.getData();

    assertEquals(BioEntity.class, dataNode.getClazz());

    Queue<TreeNode> nodes = new LinkedList<>();
    nodes.add(tree);
    boolean annotatorsFound = false;
    while (!nodes.isEmpty()) {
      TreeNode node = nodes.poll();
      dataNode = (AnnotatedObjectTreeRow) node.getData();
      if (dataNode.getUsedAnnotators().size() > 0) {
        annotatorsFound = true;
      }
      for (TreeNode n : node.getChildren()) {
        nodes.add(n);
      }
    }
    assertTrue(annotatorsFound);

    userDao.delete(user);
  }

  @Test
  public void testEmptyValidMiriamTreeForUser() throws Exception {
    User user = createUser();
    UserAnnotationSchema schema = new UserAnnotationSchema();
    user.setAnnotationSchema(schema);
    userDao.update(user);

    TreeNode tree = projectService.createClassAnnotatorTree(user);
    assertNotNull(tree);

    AnnotatedObjectTreeRow dataNode = (AnnotatedObjectTreeRow) tree.getData();

    assertEquals(BioEntity.class, dataNode.getClazz());

    Queue<TreeNode> nodes = new LinkedList<TreeNode>();
    nodes.add(tree);
    boolean annotatorsFound = false;
    while (!nodes.isEmpty()) {
      TreeNode node = nodes.poll();
      dataNode = (AnnotatedObjectTreeRow) node.getData();
      assertEquals("Valid miriam found for class: " + dataNode.getClazz(), 0, dataNode.getValidAnnotations().size());
      assertEquals("Required miriam found for class: " + dataNode.getClazz(), 0,
          dataNode.getRequiredAnnotations().size());
      if (dataNode.getMissingValidAnnotations().size() > 0 && dataNode.getMissingRequiredAnnotations().size() > 0) {
        annotatorsFound = true;
      }
      for (TreeNode n : node.getChildren()) {
        nodes.add(n);
      }
    }
    assertTrue(annotatorsFound);

    userDao.delete(user);
  }

  @Test
  public void testValidMiriamTreeForUser() throws Exception {
    User user = createUser();
    UserAnnotationSchema schema = new UserAnnotationSchema();
    schema.addClassValidAnnotations(new UserClassValidAnnotations(Protein.class, MiriamType.values()));
    user.setAnnotationSchema(schema);
    userDao.update(user);
    userDao.flush();

    TreeNode tree = projectService.createClassAnnotatorTree(user);
    assertNotNull(tree);

    AnnotatedObjectTreeRow dataNode = (AnnotatedObjectTreeRow) tree.getData();

    assertEquals(BioEntity.class, dataNode.getClazz());

    Queue<TreeNode> nodes = new LinkedList<>();
    nodes.add(tree);
    boolean annotatorsFound = false;
    while (!nodes.isEmpty()) {
      TreeNode node = nodes.poll();
      dataNode = (AnnotatedObjectTreeRow) node.getData();
      if (dataNode.getValidAnnotations().size() > 0) {
        annotatorsFound = true;
      }
      for (TreeNode n : node.getChildren()) {
        nodes.add(n);
      }
    }
    assertTrue(annotatorsFound);

    userDao.delete(user);
  }

  protected Project createComplexProject(String projectId, String filename) throws IOException, SecurityException {
    CreateProjectParams params = createProjectParams(filename);

    ZipFile zipFile = new ZipFile(filename);
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      ZipEntryFile e = zefFactory.createZipEntryFile(entry, zipFile);
      params.addZipEntry(e);
    }
    zipFile.close();

    projectService.createProject(params);
    Project project = projectService.getProjectByProjectId(projectId);
    return project;
  }

  @Test
  public void testCreateComplexWithImages() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_images.zip";
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Cannot find overview images the model", 2, project.getOverviewImages().size());
    assertEquals("Number of layouts doesn't match", BuildInLayout.values().length, project.getLayouts().size());
    assertEquals(BuildInLayout.NESTED.getTitle(), project.getLayouts().get(0).getTitle());
    assertEquals(BuildInLayout.NORMAL.getTitle(), project.getLayouts().get(1).getTitle());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateComplexWithLayoutOrder() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_images.zip";
    CreateProjectParams params = createProjectParams(filename);

    ZipFile zipFile = new ZipFile(filename);
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      params.addZipEntry(zefFactory.createZipEntryFile(entry, zipFile));
    }
    zipFile.close();

    projectService.createProject(params.networkLayoutAsDefault(true));
    Project project = projectService.getProjectByProjectId(projectId);

    Model model = modelService.getLastModelByProjectId(projectId);
    for (OverviewImage oi : project.getOverviewImages()) {
      for (OverviewLink ol : oi.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          logger.debug(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Cannot find overview images the model", 2, project.getOverviewImages().size());
    assertEquals("Number of layouts doesn't match", BuildInLayout.values().length, project.getLayouts().size());
    assertEquals(BuildInLayout.NESTED.getTitle(), project.getLayouts().get(1).getTitle());
    assertEquals(BuildInLayout.NORMAL.getTitle(), project.getLayouts().get(0).getTitle());
    projectService.removeProject(project, null, false);
  }

  private CreateProjectParams createProjectParams(String filename) throws FileNotFoundException, IOException {
    InputStream is = new FileInputStream(filename);
    return createProjectParams(filename, is);
  }

  private CreateProjectParams createProjectParams(String filename, InputStream is) throws IOException {
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setLocalPath(filename);
    byte[] data = IOUtils.toByteArray(is);
    fileEntry.setLength(data.length);
    fileEntry.setFileContent(data);

    return new CreateProjectParams()
        .setUser(createUser("admin"))
        .projectId(projectId)
        .version("")
        .complex(filename.endsWith("zip"))
        .projectFile(fileEntry).annotations(true).images(false).async(false).projectDir(tmpResultDir)
        .analyzeAnnotations(true).parser(new CellDesignerXmlParser());
  }

  @Test(timeout = 15000)
  public void testCreateComplexWithLayouts() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_layouts.zip";
    int overlaysInZipFile = 1;
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Number of layouts doesn't match", BuildInLayout.values().length + overlaysInZipFile,
        project.getLayouts().size());
    boolean additionalFound = false;

    for (Layout layout : project.getLayouts()) {
      if ("example name".equals(layout.getTitle())) {
        additionalFound = true;
        assertEquals("layout description", layout.getDescription());
        for (DataOverlayImageLayer imageLayer : layout.getDataOverlayImageLayers()) {
          assertTrue(imageLayer.getDirectory().contains("example"));
        }
      }
    }
    assertTrue("Cannot find layout with predefined name from file", additionalFound);
    projectService.removeProject(project, null, false);
    assertNull(projectService.getProjectByProjectId(projectId));
  }

  @Test
  public void testCreateComplexWithSumbaps() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_submaps.zip";
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Number of layouts doesn't match", BuildInLayout.values().length, project.getLayouts().size());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateEmptyComplex() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model.zip";
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateEmptyComplex2() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model2.zip";
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateEmptyComplex3() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model3.zip";
    Project project = createComplexProject(projectId, filename);

    Model model = modelService.getLastModelByProjectId(projectId);
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testUpdateProjectWithoutModel() throws Exception {
    createUser();

    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectService.updateProject(project);
    projectService.removeProject(project, null, false);
  }

  @Test
  public void uploadToBigFile() throws Exception {
    Integer value = Integer
        .parseInt(configurationService.getValue(ConfigurationElementType.MAX_NUMBER_OF_MAP_LEVELS).getValue());
    double size = MapGenerator.TILE_SIZE * Math.pow(2, value) + 1;
    Model model = new ModelFullIndexed(null);
    model.setWidth(size);
    model.setHeight(size);
    String xml = new CellDesignerXmlParser().model2String(model);
    InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

    CreateProjectParams params = createProjectParams("sample.xml", is);

    projectService.createProject(params);

    Project project = projectService.getProjectByProjectId(projectId);
    assertEquals(ProjectStatus.FAIL, project.getStatus());
    projectService.removeProject(project, null, false);
  }

  @Test
  public void testCreateLogEntries() throws Exception {
    Protein protein = new GenericProtein("id");
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.WARN)
        .setMessage(new SimpleMessage("Hello world"))
        .setMarker(new LogMarker(ProjectLogEntryType.OTHER, protein))
        .build());
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.ERROR)
        .setMessage(new SimpleMessage("Hello world 2"))
        .setMarker(new LogMarker(ProjectLogEntryType.OTHER, protein))
        .build());
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.ERROR)
        .setMessage(new SimpleMessage("Hello world 3"))
        .build());

    ProjectService service = Mockito.mock(ProjectService.class, Mockito.CALLS_REAL_METHODS);
    Set<ProjectLogEntry> entries = service.createLogEntries(appender);
    assertEquals(3, entries.size());
  }

  @Test
  public void testCreateIgnoredLogEntries() throws Exception {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.WARN)
        .setMessage(new SimpleMessage("Hello world"))
        .setMarker(LogMarker.IGNORE)
        .build());
    ProjectService service = Mockito.mock(ProjectService.class, Mockito.CALLS_REAL_METHODS);
    Set<ProjectLogEntry> entries = service.createLogEntries(appender);
    assertEquals(0, entries.size());
  }

}
