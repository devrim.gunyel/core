package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.ILayoutService.CreateLayoutParams;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

public class LayoutServiceTest extends ServiceTestFunctions {
  Logger logger = LogManager.getLogger(LayoutServiceTest.class);

  Project project = null;
  Model model = null;

  String projectId = "Some_id";
  String clearProjectId = "Some_id2";

  @Autowired
  LayoutDao layoutDao;

  EmailSender originalSender;

  @Autowired
  ILayoutService layoutService;
  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);
  private User user, user2;

  @Before
  public void setUp() throws Exception {
    // we use custom threads because in layout service there is commit method
    // called, and because of that hibernate session injected by spring cannot
    // commit at the end of the test case

    dbUtils.createSessionForCurrentThread();
    dbUtils.setAutoFlush(false);

    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }

    project = projectDao.getProjectByProjectId(clearProjectId);
    if (project != null) {
      projectDao.delete(project);
    }

    project = new Project();
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

    project.addModel(model);
    project.setProjectId(projectId);

    projectService.addProject(project);

    user = createUser();
    user2 = createUser("john.doe.bis");

    originalSender = layoutService.getEmailSender();
    layoutService.setEmailSender(Mockito.mock(EmailSender.class));
  }

  @After
  public void tearDown() throws Exception {
    userService.deleteUser(user2);
    userService.deleteUser(user);
    projectService.removeProject(project, null, false);

    // close session
    dbUtils.closeSessionForCurrentThread();

    layoutService.setEmailSender(originalSender);
  }

  @Test
  public void testGetCustomLayouts() throws Exception {
    List<Layout> layouts = layoutService.getLayoutsByProject(project);
    assertNotNull(layouts);
    assertEquals(0, layouts.size());

    CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).user(user);
    Layout row = layoutService.createLayout(params);

    assertNotNull(row);
    assertTrue(row.getId() > 0);

    layouts = layoutService.getLayoutsByProject(project);
    assertEquals(1, layouts.size());

    layoutService.removeLayout(row, null);
  }

  @Test
  public void testUpdateLayout() throws Exception {
    List<Layout> layouts = layoutService.getLayoutsByProject(project);
    assertNotNull(layouts);
    assertEquals(0, layouts.size());

    CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).user(user);
    Layout row = layoutService.createLayout(params);

    layouts = layoutService.getLayoutsByProject(project);
    assertEquals(1, layouts.size());
    assertEquals("Test", layouts.get(0).getTitle());

    row.setTitle("New name");

    layoutService.updateLayout(row);

    layouts = layoutService.getLayoutsByProject(project);
    assertEquals(1, layouts.size());
    assertEquals("New name", layouts.get(0).getTitle());

    layoutService.removeLayout(row, null);
  }

  @Test(timeout = 15000)
  public void testCreateAsyncLayout() throws Exception {
    List<Layout> layouts = layoutService.getLayoutsByProject(project);
    assertNotNull(layouts);
    assertEquals(0, layouts.size());

    CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).user(user).async(true);

    long logCounter = getInfos().size();
    Layout row = layoutService.createLayout(params);

    assertNotNull(row);
    assertTrue(row.getId() > 0);

    Layout layoutDb = layoutDao.getById(row.getId());
    do {
      Thread.sleep(200);
      layoutDao.refresh(layoutDb);
    } while (layoutDb.getStatus() != LayoutStatus.OK);

    layouts = layoutService.getLayoutsByProject(project);
    assertEquals(1, layouts.size());

    long logCounter2 = getInfos().size();
    assertTrue("Log didn't appeard after creating layout", logCounter < logCounter2);

    Layout layout = layouts.get(0);
    logger.debug(layout.getStatus() + ", " + layout.getProgress());

    assertFalse("Layout images weren't generated...", layout.getStatus().equals(LayoutStatus.NA));

    logCounter = getInfos().size();
    layoutService.removeLayout(layout, null);
    logCounter2 = getInfos().size();

    assertTrue("Log didn't appeard after removing layout", logCounter < logCounter2);
  }

  @Test
  public void testAddLayoutToComplexModel() throws Exception {
    Project localProject = new Project();
    localProject.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model localModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
      Model localSubmodelModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

      ModelSubmodelConnection connection = new ModelSubmodelConnection(localSubmodelModel, SubmodelType.UNKNOWN);
      connection.setName("AS");
      localModel.addSubmodelConnection(connection);

      localProject.addModel(localModel);
      localProject.setProjectId(clearProjectId);

      projectService.addProject(localProject);

      CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(localProject)
          .colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).user(user);

      layoutService.createLayout(params);

      Collection<Layout> layouts = layoutDao.getLayoutsByProject(localProject);

      assertTrue(layouts.size() > 0);

      for (Layout layout : layouts) {
        if (layout.getInputData() == null) {
          assertTrue(layout.getDataOverlayImageLayers().size() > 0);
        }
        for (DataOverlayImageLayer dataOverlayImageLayer : layout.getDataOverlayImageLayers()) {
          File f1 = new File(dataOverlayImageLayer.getDirectory() + "/2/0/0.PNG");
          assertTrue(f1.exists());
        }
      }
    } finally {
      projectService.removeProject(localProject, null, false);
    }
  }

  @Test
  public void testInputDataInLayout() throws Exception {
    List<Layout> layouts = layoutService.getLayoutsByProject(project);
    assertNotNull(layouts);
    assertEquals(0, layouts.size());

    CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).user(user).async(false);

    Layout row = layoutService.createLayout(params);

    assertNotNull(row);
    assertTrue(row.getId() > 0);
    assertNotNull(row.getInputData());
    byte[] inputData = row.getInputData().getFileContent();
    assertNotNull(inputData);
    byte[] originalData = IOUtils.toByteArray(new FileInputStream("testFiles/enricoData/ge001.txt"));
    assertEquals(new String(originalData, StandardCharsets.UTF_8), new String(inputData, StandardCharsets.UTF_8));

    layoutService.removeLayout(row, null);
  }

  @Test
  public void testGetLayoutAliases() throws Exception {
    List<Layout> layouts = layoutService.getLayoutsByProject(project);
    assertNotNull(layouts);
    assertEquals(0, layouts.size());

    ByteArrayInputStream bais = new ByteArrayInputStream(
        "name	reactionIdentifier	Color\ns1		#CC0000\ns2		#CCCC00\n	re1	#CCCC00\n"
            .getBytes(StandardCharsets.UTF_8));
    CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
        .colorInputStream(bais).user(user).async(false);

    Layout row = layoutService.createLayout(params);

    List<?> result = layoutService.getAliasesForLayout(model, row.getId());

    assertTrue(result.size() > 0);

    List<?> result2 = layoutService.getReactionsForLayout(model, row.getId());

    assertEquals(1, result2.size());

    layoutService.removeLayout(row, null);
  }

  @Test
  public void testGetLayoutAliasesForInvalidAlias() throws Exception {
    Project project = new Project("p_id");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    try {
      Model model = new CellDesignerXmlParser()
          .createModel(new ConverterParams().filename("testFiles/coloring/problematicModel2.xml"));

      project.addModel(model);
      projectService.addProject(project);

      FileInputStream fis = new FileInputStream("testFiles/coloring/problematicSchema2.txt");
      CreateLayoutParams params = new CreateLayoutParams().name("Test").directory("testDir").project(project)
          .colorInputStream(fis).async(false);
      ColorSchemaReader reader = new ColorSchemaReader();
      final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(),
          TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));

      ColorModelCommand command = new ColorModelCommand(model, schemas, colorExtractor);
      command.execute();

      command.getModifiedElements();
    } finally {
      projectDao.delete(project);
    }
  }

  @Test
  public void testEmptyPreprareTableResultForGeneVariant() throws Exception {
    class CSR extends ColorSchemaReader {
      public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
        List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
        for (ColorSchemaColumn csc : ColorSchemaColumn.values()) {
          if (csc.getTypes().contains(ColorSchemaType.GENETIC_VARIANT)) {
            result.add(csc);
          }
        }
        return result;
      }
    }
    ;
    List<ColorSchema> schemas = new ArrayList<ColorSchema>();
    ColorSchema cs = new GeneVariationColorSchema();
    schemas.add(cs);

    LayoutService ls = new LayoutService(null, null, null, null);
    String result = ls.prepareTableResult(schemas, new CSR());
    assertNotNull(result);
  }

  @Test
  public void testDataOverlayWithType() throws Exception {
    String layoutId = "Test";
    CreateLayoutParams params = new CreateLayoutParams().name(layoutId).directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/gene_variants_without_type.txt")).user(user)
        .colorSchemaType(ColorSchemaType.GENETIC_VARIANT);

    Layout layout = layoutService.createLayout(params);
    Map<Object, ColorSchema> result = layoutService.getElementsForLayout(model, layout.getId());
    assertNotNull(result);
    layoutService.removeLayout(layout, null);
  }

  @Test
  public void testDataOverlayWithConflictingType() throws Exception {
    String layoutId = "Test";
    CreateLayoutParams params = new CreateLayoutParams().name(layoutId).directory("testDir").project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/gene_variants.txt")).user(user)
        .colorSchemaType(ColorSchemaType.GENERIC);

    Layout layout = layoutService.createLayout(params);
    Map<Object, ColorSchema> result = layoutService.getElementsForLayout(model, layout.getId());
    assertNotNull(result);
    layoutService.removeLayout(layout, null);
  }

}
