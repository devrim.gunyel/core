package lcsb.mapviewer.services.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CommentServiceTest.class,
    ConfigurationServiceTest.class,
    CustomMd5PasswordEncoderTest.class,
    LayoutServiceTest.class,
    LdapServiceTest.class,
    ProjectServiceTest.class,
    SearchServiceTest.class,
    UserServiceTest.class
})
public class AllImplServiceTests {

}
