package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.utils.SearchIndexer;

public class SearchServiceTest extends ServiceTestFunctions {
  static Logger logger = LogManager.getLogger(SearchServiceTest.class);

  SearchIndexer indexer = new SearchIndexer();

  Map<Class<?>, String> speciesSearchPrefix = new HashMap<Class<?>, String>();
  Map<String, Class<?>> speciesSearchReversePrefix = new HashMap<String, Class<?>>();

  int elementCounter = 0;

  @Before
  public void setUp() throws Exception {
    addSearchPrefix("complex", Complex.class);
    addSearchPrefix("degrded", Degraded.class);
    addSearchPrefix("drug", Drug.class);
    addSearchPrefix("gene", Gene.class);
    addSearchPrefix("ion", Ion.class);
    addSearchPrefix("phenotype", Phenotype.class);
    addSearchPrefix("protein", Protein.class);
    addSearchPrefix("rna", Rna.class);
    addSearchPrefix("molecule", SimpleMolecule.class);
    addSearchPrefix("unknown", Unknown.class);
  }

  private void addSearchPrefix(String prefix, Class<?> clazz) {
    speciesSearchPrefix.put(clazz, prefix);
    speciesSearchReversePrefix.put(prefix, clazz);
  }

  protected Model createFullModel() throws Exception {
    Model model = getModelForFile("testFiles/searchModel.xml", false);
    Project project = new Project("unknown project");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    
    model.setProject(project);

    return model;
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSearchByName() throws Exception {
    Model model = createFullModel();
    long count = searchHistoryDao.getCount();
    List<BioEntity> result = searchService.searchByQuery(model, "reaction:re21", 50, null, "127.0.0.1");

    assertNotNull(result);
    assertEquals(4, result.size());
    Reaction reaction = (Reaction) result.get(0);
    assertTrue(reaction.getIdReaction().contains("re21"));

    long count2 = searchHistoryDao.getCount();

    assertEquals(count + 1, count2);
  }

  @Test
  public void testSearchByName2() throws Exception {
    Model model = createFullModel();
    List<BioEntity> result = searchService.searchByQuery(model, "BAD:BCL-2", 50, null, "127.0.0.1");

    assertNotNull(result);
    assertTrue(result.size() > 0);
    Species alias = (Species) result.get(0);
    assertEquals("BAD:BCL-2", alias.getName());
  }

  @Test
  public void testSearchById() throws Exception {
    Model model = createFullModel();
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_HAS_VERSION, MiriamType.CAS, "123");
    model.getElements().iterator().next().addMiriamData(md);
    List<BioEntity> global = searchService.searchByQuery(model, "CAS:123", 50, null, "127.0.0.1");
    assertNotNull(global);
    assertEquals(1, global.size());
  }

  @Test
  public void testSearchByElementId() throws Exception {
    Model model = createFullModel();
    Element element = model.getElements().iterator().next();
    element.setId(907);
    List<BioEntity> global = searchService.searchByQuery(model, "element:907", 50, null, "127.0.0.1");
    assertNotNull(global);
    assertEquals(1, global.size());
  }

  @Test
  public void testSearchByUnknwonElementId() throws Exception {
    Model model = createFullModel();
    Element element = model.getElements().iterator().next();
    element.setId(907);
    List<BioEntity> global = searchService.searchByQuery(model, "species:90111117", 50, null, "127.0.0.1");
    assertEquals(0, global.size());
  }

  @Test
  public void testSearchByName4() throws Exception {
    Model model = createFullModel();
    List<BioEntity> result = searchService.searchByQuery(model, "BC", 50, true, "127.0.0.1");

    assertNotNull(result);
    assertEquals(0, result.size());

    result = searchService.searchByQuery(model, "BC", 50, false, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);

    result = searchService.searchByQuery(model, "BAD:BCL-2", 50, true, "127.0.0.1");
    assertNotNull(result);
    assertEquals(1, result.size());
    Species alias = (Species) result.get(0);
    assertEquals("BAD:BCL-2", alias.getName());
  }

  @Test
  public void testQueryName() throws Exception {
    Model model = createFullModel();
    List<BioEntity> result = searchService.searchByQuery(model,
        indexer.getQueryStringForIndex("reaction:re21", new HashSet<>()), 50, null, "127.0.0.1");

    assertNotNull(result);
    assertEquals(4, result.size());
    Reaction reaction = (Reaction) result.get(0);
    assertEquals("re21", reaction.getIdReaction());
    assertTrue(result.get(1) instanceof Species);
    assertTrue(result.get(2) instanceof Species);
    assertTrue(result.get(3) instanceof Species);
  }

  @Test
  public void testQueryReactionId() throws Exception {
    Model model = createFullModel();
    model.getReactions().iterator().next().setIdReaction("R_x1");
    List<BioEntity> result = searchService.searchByQuery(model,
        indexer.getQueryStringForIndex("reaction:r_x1", new HashSet<>()), 50, null, "127.0.0.1");

    assertNotNull(result);
    assertTrue(result.size() > 0);
    Reaction reaction = (Reaction) result.get(0);
    assertTrue(reaction.getIdReaction().contains("R_x1"));
  }

  @Test
  public void testQueryReactionId2() throws Exception {
    Model model = createFullModel();
    Reaction reaction = model.getReactions().iterator().next();
    reaction.setId(154);
    reaction.setIdReaction("test-name");
    List<BioEntity> result = searchService.searchByQuery(model,
        indexer.getQueryStringForIndex("reaction:154", new HashSet<>()), 50, null, "127.0.0.1");

    assertNotNull(result);
    assertTrue(result.size() > 0);
    Reaction resultReaction = (Reaction) result.get(0);
    assertTrue(resultReaction.getIdReaction().contains("test-name"));
  }

  @Test
  public void testQueryName2() throws Exception {
    Model model = createFullModel();
    model.getModelData().setId(-13);
    List<BioEntity> result = searchService.searchByQuery(model,
        indexer.getQueryStringForIndex("BCL-2", speciesSearchReversePrefix.keySet()), 50, null, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);
    Species alias = (Species) result.get(0);
    assertTrue("Invalid alias: " + alias.getName(), alias.getName().toUpperCase().contains("BCL"));
  }

  @Test
  public void testQueryName3() throws Exception {
    Model model = createFullModel();
    List<BioEntity> result = searchService.searchByQuery(model, "fdhgjkhdsfsdhfgfhsd", 50, null, "127.0.0.1");

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void testQueryName4() throws Exception {
    Model model = createFullModel();
    List<BioEntity> result = searchService.searchByQuery(model,
        indexer.getQueryStringForIndex("protein:BCL-2", speciesSearchReversePrefix.keySet()), 50, null, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);
    Species alias = (Species) result.get(0);
    assertTrue("Invalid alias: " + alias.getName(), alias.getName().toUpperCase().contains("BCL"));
  }

  @Test
  public void testSearchClosest() throws Exception {
    Model model = getModelForFile("testFiles/graph_path_example3.xml", true);
    List<BioEntity> elements = searchService.getClosestElements(model, new Point2D.Double(0, 0), 5, false,
        new ArrayList<>());
    assertNotNull(elements);
    assertEquals(5, elements.size());
    assertTrue(elements.get(0) instanceof Species);
    Species sAlias = (Species) elements.get(0);
    assertEquals("s1", sAlias.getName());
    assertTrue(elements.get(1) instanceof Reaction);
    Reaction reaction = (Reaction) elements.get(1);
    assertEquals("re1", reaction.getIdReaction());
    assertTrue(elements.get(2) instanceof Species);
    sAlias = (Species) elements.get(2);
    assertEquals("s3", sAlias.getName());
    assertTrue(elements.get(3) instanceof Reaction);
    reaction = (Reaction) elements.get(3);
    assertEquals("re2", reaction.getIdReaction());
    assertTrue(elements.get(4) instanceof Species);
    sAlias = (Species) elements.get(4);
    assertEquals("s2", sAlias.getName());
  }

  @Test
  public void testSearchClosestByZIndex() throws Exception {
    Model model = new ModelFullIndexed(null);
    GenericProtein protein1 = createProtein();
    protein1.setZ(0);
    model.addElement(protein1);
    GenericProtein protein2 = createProtein();
    protein2.setZ(1);
    model.addElement(protein2);
    GenericProtein protein3 = createProtein();
    protein3.setZ(-1);
    model.addElement(protein3);

    List<BioEntity> elements = searchService.getClosestElements(model, protein2.getCenter(), 5, false,
        new ArrayList<>());
    assertNotNull(elements);
    assertEquals(3, elements.size());
    assertEquals(protein2, elements.get(0));
    assertEquals(protein1, elements.get(1));
    assertEquals(protein3, elements.get(2));
  }

  private GenericProtein createProtein() {
    GenericProtein protein1 = new GenericProtein("s" + elementCounter++);
    protein1.setWidth(20);
    protein1.setHeight(20);
    protein1.setX(5);
    protein1.setY(5);
    protein1.setZ(0);
    return protein1;
  }

  @Test
  public void testSearchClosestWithEmptyModel() throws Exception {
    Model model = new ModelFullIndexed(null);
    List<BioEntity> elements = searchService.getClosestElements(model, new Point2D.Double(0, 0), 5, false,
        new ArrayList<>());
    assertNotNull(elements);
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetAutocompleList() {
    Model model = new ModelFullIndexed(null);
    Species proteinAlias = new GenericProtein("a");
    proteinAlias.setName("PROT identifier");
    proteinAlias.getSynonyms().add("PROT synonym");
    proteinAlias.getSynonyms().add("another synonym");
    proteinAlias.setFullName("Protein common name");
    model.addElement(proteinAlias);

    SimpleMolecule alias = new SimpleMolecule("a2");
    alias.setName("Molecule2");
    model.addElement(alias);

    int oldVal = Configuration.getAutocompleteSize();
    Configuration.setAutocompleteSize(5);

    List<String> list = searchService.getAutocompleteList(model, "P");
    assertNotNull(list);
    assertEquals(4, list.size());
    assertTrue(list.contains("PROT identifier".toLowerCase()));
    assertTrue(list.contains("PROT synonym".toLowerCase()));
    assertTrue(list.contains("Protein common name".toLowerCase()));

    list = searchService.getAutocompleteList(model, "PROTEIN");
    assertNotNull(list);
    assertEquals(2, list.size());
    assertTrue(list.contains("Protein common name".toLowerCase()));

    list = searchService.getAutocompleteList(model, "m");
    assertNotNull(list);
    assertEquals(2, list.size());
    assertTrue(list.contains("Molecule2".toLowerCase()));

    list = searchService.getAutocompleteList(model, "blablabla");
    assertNotNull(list);
    assertEquals(0, list.size());

    Configuration.setAutocompleteSize(oldVal);
  }

  @Test
  public void testGetAutocompleList2() {
    Model model = new ModelFullIndexed(null);
    GenericProtein proteinAlias = new GenericProtein("a1");
    proteinAlias.setName("PROT identifier");
    proteinAlias.setNotes("Synonyms: PROT synonym, another synonym\nName: Protein common name");
    model.addElement(proteinAlias);

    SimpleMolecule alias = new SimpleMolecule("a2");
    alias.setName("Molecule2");
    model.addElement(alias);

    List<String> list = searchService.getAutocompleteList(model, "PROT identifier");
    assertNotNull(list);
    assertEquals(1, list.size());
    assertTrue(list.get(0).equalsIgnoreCase("PROT identifier"));
  }

  @Test
  public void testSearchGenericProtein() throws Exception {
    Model model = getModelForFile("testFiles/generic.xml", true);

    List<String> list = searchService.getAutocompleteList(model, "generic");
    assertNotNull(list);
    assertEquals(2, list.size());
    assertTrue(list.contains("generic"));
    assertTrue(list.contains("generic protein"));

    List<BioEntity> result = searchService.searchByQuery(model, "generic protein", 10, false, null);
    assertNotNull(result);

    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetMiriamTypeForQuery() throws Exception {
    MiriamData md = new SearchService(null, null).getMiriamTypeForQuery("hgnc:SNCA");
    assertNotNull(md);
    assertEquals(MiriamType.HGNC, md.getDataType());
    assertEquals("SNCA", md.getResource());

    md = new SearchService(null, null).getMiriamTypeForQuery("reactome:REACT_15128");
    assertNotNull(md);
    assertEquals(MiriamType.REACTOME, md.getDataType());
    assertEquals("REACT_15128", md.getResource());
  }

  @Test
  public void testSearchByMiriamInSubmap() throws Exception {
    String query = "HGNC_SYMBOL:SNCA";
    Model model = new ModelFullIndexed(null);
    Model submodel = new ModelFullIndexed(null);
    GenericProtein protein = new GenericProtein("s1");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    submodel.addElement(protein);
    model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));

    List<BioEntity> result = searchService.searchByQuery(model, query, 50, null, "127.0.0.1");

    assertEquals(1, result.size());
  }

  @Test
  public void testGetMiriamTypeForQuery2() throws Exception {
    MiriamData md = new SearchService(null, null).getMiriamTypeForQuery("UNKNOWN_HGNC:SNCA");
    assertNull(md);
  }

}
