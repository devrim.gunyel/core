package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.*;

public class OkServiceMock implements IExternalService {

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("Test service", "unknown");
    status.setStatus(ExternalServiceStatusType.OK);
    return status;
  }

}
