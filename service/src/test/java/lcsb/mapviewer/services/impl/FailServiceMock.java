package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.*;

public class FailServiceMock implements IExternalService {

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("Test service", "unknown");
    status.setStatus(ExternalServiceStatusType.DOWN);
    return status;
  }

}
