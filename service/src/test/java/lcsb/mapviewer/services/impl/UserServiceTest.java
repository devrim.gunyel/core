package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.security.PrivilegeDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IUserService;

@Transactional
@Rollback
public class UserServiceTest extends ServiceTestFunctions {

  @Autowired
  private IUserService userService;

  @Autowired
  private PrivilegeDao privilegeDao;

  @Test
  public void grantExistingPrivilegeDoesNotProduceDuplicates() {
    User user1 = new User();
    user1.setLogin("test_user_1");
    user1.setCryptedPassword("***");
    userService.addUser(user1);

    User user2 = new User();
    user2.setLogin("test_user_2");
    user2.setCryptedPassword("***");
    userService.addUser(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    privilegeDao.add(privilege);

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));
    assertEquals(count, privilegeDao.getCount());
  }

  @Test
  public void grantNonYetExistingPrivilegeTwiceDoesNotConflict() {
    User user1 = new User();
    user1.setLogin("test_user_1");
    user1.setCryptedPassword("***");
    userService.addUser(user1);

    User user2 = new User();
    user2.setLogin("test_user_2");
    user2.setCryptedPassword("***");
    userService.addUser(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));
    assertEquals(count + 1, privilegeDao.getCount());
  }

  @Test
  public void revokePrivilegesWorks() {
    User user = new User();
    user.setLogin("test_user");
    user.setCryptedPassword("***");
    userService.addUser(user);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
    assertTrue(user.getPrivileges().contains(privilege));

    userService.revokeUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
    assertFalse(user.getPrivileges().contains(privilege));
  }

  @Test
  public void revokePrivilegesDoesNotThrowIfPrivilegeNotExists() {
    User user = new User();
    user.setLogin("test_user");
    user.setCryptedPassword("***");
    userService.addUser(user);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    assertFalse(user.getPrivileges().contains(privilege));

    userService.revokeUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
  }

  @Test
  public void revokePrivilegeGloballyWorks() {
    User user1 = new User();
    user1.setLogin("test_user_1");
    user1.setCryptedPassword("***");
    userService.addUser(user1);

    User user2 = new User();
    user2.setLogin("test_user_2");
    user2.setCryptedPassword("***");
    userService.addUser(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertEquals(count + 1, privilegeDao.getCount());
    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));

    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.WRITE_PROJECT, "foo");

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));
  }

  @Test
  public void testAddUser() throws Exception {
    long logCount = getInfos().size();
    User user2 = new User();
    user2.setCryptedPassword("");
    user2.setLogin("login");
    userService.addUser(user2);
    assertNotNull(user2.getId());
    long logCount2 = getInfos().size();
    assertEquals("Log entry is missing for add user event", logCount + 1, logCount2);
    userDao.evict(user2);
    User user3 = userService.getUserByLogin(user2.getLogin());
    assertNotNull(user3);
    userService.deleteUser(user3);
    user3 = userService.getUserByLogin(user2.getLogin());
    assertNull(user3);

    long logCount3 = getInfos().size();
    assertEquals("Log entry is missing for remove user event", logCount2 + 1, logCount3);
  }

}
