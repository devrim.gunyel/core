package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.ServiceTestFunctions;

@Rollback(true)
public class CommentServiceTest extends ServiceTestFunctions {
  static Logger logger = LogManager.getLogger(CommentServiceTest.class);
  Model model;
  Project project;
  Element alias;
  Element alias2;
  private String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    dbUtils.setAutoFlush(true);
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    project.setProjectId(projectId);
    model = getModelForFile("testFiles/centeredAnchorInModifier.xml", false);
    model.setTileSize(128);
    Set<Element> aliases = model.getElements();
    alias = null;
    alias2 = null;
    for (Element nAlias : aliases) {
      alias2 = alias;
      alias = nAlias;
    }
    project.addModel(model);
    projectDao.add(project);

    projectDao.evict(project);
    modelDao.evict(model);

    project = projectDao.getById(project.getId());
    model = new ModelFullIndexed(modelDao.getLastModelForProjectIdentifier(project.getProjectId(), false));
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void testGetAgregatedComments() throws Exception {
    commentService.addComment("John Doe", "a@a.pl", "Conteneta 1", new Point2D.Double(0, 1), alias, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    commentService.addComment("John Doe", "a@a.pl", "Contenetb 2", new Point2D.Double(0, 2), alias, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    commentService.addComment("John Doe", "a@a.pl", "Contenetc 3", new Point2D.Double(0, 3), alias2, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    commentService.addComment("John Doe", "a@a.pl", "Contenetc 4", new Point2D.Double(0, 4), null, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    commentService.addComment("John Doe", "a@a.pl", "Contenetc 5", new Point2D.Double(0, 5), null, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    CommentService service = new CommentService(null, null, null, null);
    service.setCommentDao(commentDao);
    List<List<Comment>> comments = service.getAgregatedComments(model, null);
    assertEquals(4, comments.size());
    int size = 0;
    for (List<Comment> list : comments) {
      size += list.size();
    }
    assertEquals(5, size);

    List<Comment> allComments = commentDao.getCommentByModel(model, null, null);
    assertEquals(5, allComments.size());
    for (Comment comment : allComments) {
      commentDao.delete(comment);
    }
  }

  @Test
  public void testAddComment() throws Exception {
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("a", "b", "c", new Point2D.Double(0, 0), null, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testAddCommentForReaction() throws Exception {
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("a", "b", "c", new Point2D.Double(0, 0),
        model.getReactions().iterator().next(), false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testAddCommentForAlias() throws Exception {
    Element alias = model.getElementByElementId("sa1");
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("a", "b", "c", new Point2D.Double(0, 0), alias, false, model, userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testComparePointCommentId() throws Exception {
    CommentService cs = new CommentService(null, null, null, null);
    assertTrue(cs.equalPoints("Point2D.Double[117.685546875, 204.6923828125001]", "(117.69, 204.69)"));
  }

  @Test
  public void testComparePointCommentId2() throws Exception {
    CommentService cs = new CommentService(null, null, null, null);
    assertFalse(cs.equalPoints("Point2D.Double[118.685546875, 204.6923828125001]", "(117.69, 204.69)"));
  }

  @Test
  public void testComparePointCommentId3() throws Exception {
    CommentService cs = new CommentService(null, null, null, null);
    assertFalse(cs.equalPoints("Point2D.Double[117.685546875, 205.6923828125001]", "(117.69, 204.69)"));
  }

}
