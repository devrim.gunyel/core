package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.FrameworkVersion;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.services.ServiceTestFunctions;

@Rollback(true)
public class ConfigurationServiceTest extends ServiceTestFunctions {
  Logger logger = LogManager.getLogger(ConfigurationServiceTest.class);

  @Before
  public void setUp() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @After
  public void tearDown() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @Test
  public void testGetUpdate() throws Exception {
    String address = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    configurationService.deleteConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    String newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    assertEquals(ConfigurationElementType.EMAIL_ADDRESS.getDefaultValue(), newAddress);
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, "hello@world");
    newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    assertEquals("hello@world", newAddress);

    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, address);
  }

  @Test
  public void testList() throws Exception {
    List<ConfigurationOption> list = configurationService.getAllValues();
    assertEquals(ConfigurationElementType.values().length, list.size());
  }

  @Test
  public void testGetSystemVersion() throws Exception {
    FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testNormal/");
    assertNotNull(view);
    assertEquals("100", view.getGitVersion());
    assertEquals("202", view.getVersion());
  }

  @Test
  public void testGetSystemVersion2() throws Exception {
    FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testModified/");
    assertNotNull(view);
    assertEquals("100:105", view.getGitVersion());
  }

  @Test
  public void testGetSystemVersion3() throws Exception {
    FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testCorrectSvn/");
    assertNotNull(view);
    assertEquals("117", view.getGitVersion());
  }

  @Test
  public void testGetConfiguratioElemntForPrivilege() throws Exception {
    for (PrivilegeType type : PrivilegeType.values()) {
      ConfigurationOption value = configurationService.getValue(type);
      if (Project.class.equals(type.getPrivilegeObjectType()) || type.equals(PrivilegeType.CAN_CREATE_OVERLAYS)) {
        assertNotNull("No default value for privilege " + type.getDescription(), value);
        assertNotNull(value.getValue());
      } else {
        assertNull(value);
      }
    }
  }

}
