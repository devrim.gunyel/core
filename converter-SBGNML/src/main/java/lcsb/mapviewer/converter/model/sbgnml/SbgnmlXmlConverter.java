package lcsb.mapviewer.converter.model.sbgnml;

import java.io.*;

import javax.xml.bind.JAXBException;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.core.LogEvent;
import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Sbgn;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.model.map.model.Model;

@Component
public class SbgnmlXmlConverter extends Converter {

  @Override
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption, ConverterException {
    SbgnmlXmlParser parser = new SbgnmlXmlParser();
    File inputFile = null;
    try {
      inputFile = inputStream2File(params.getInputStream());
      return parser.createModel(params.getFilename(), inputFile);
    } catch (IOException e) {
      throw new ConverterException("Failed to convert input stream to file.", e);
    } finally {
      assert inputFile == null || inputFile.delete();
    }
  }

  @Override
  public String model2String(Model model) throws ConverterException {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    Sbgn exportedData;
    StringBuilder notes = new StringBuilder();
    try {
      SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
      exportedData = exporter.toSbgnml(model);
      if (!appender.getWarnings().isEmpty()) {
        for (LogEvent event : appender.getWarnings()) {
          notes.append("\n");
          notes.append(event.getMessage().getFormattedMessage());
        }
      }
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      SbgnUtil.writeTo(exportedData, baos);

      String xml = new String(baos.toByteArray(), "UTF-8");
      if (notes.length() > 0) {
        int position = xml.indexOf("<map language");
        if (position < 0) {
          throw new ConverterException("problematic output sbgn. Cannot find map tag");
        }
        String notesNode = "<notes><html:body xmlns:html=\"http://www.w3.org/1999/xhtml\">" +
            StringEscapeUtils.escapeXml10(notes.toString()) + "\n</html:body></notes> ";
        xml = xml.substring(0, position) + notesNode + xml.substring(position);
      }
      return xml;
    } catch (IOException | JAXBException e) {
      throw new ConverterException(e);
    }
  }

  @Override
  public String getCommonName() {
    return "SBGN-ML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.TEXT;
  }

  @Override
  public String getFileExtension() {
    return "sbgn";
  }

}
