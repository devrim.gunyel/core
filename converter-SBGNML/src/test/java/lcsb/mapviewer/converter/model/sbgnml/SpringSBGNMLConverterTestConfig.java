package lcsb.mapviewer.converter.model.sbgnml;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringPersistConfig.class })
public class SpringSBGNMLConverterTestConfig {

}
