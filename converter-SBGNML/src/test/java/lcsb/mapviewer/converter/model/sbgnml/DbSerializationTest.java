package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.ModelDao;

@RunWith(Parameterized.class)
public class DbSerializationTest extends SbgnmlTestFunctions {

  static ApplicationContext applicationContext;

  Logger logger = LogManager.getLogger(DbSerializationTest.class.getName());

  private String  fileName;

  public DbSerializationTest(String filePath) {
    this.fileName = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    applicationContext = new AnnotationConfigApplicationContext(
        SpringSBGNMLConverterTestConfig.class);

    Collection<Object[]> data = new ArrayList<Object[]>();
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/VANTEDdiagram.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/activated_stat1alpha_induction_of_the_irf1_gene.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/adh.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/clone-marker.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/glycolysis.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlCellDesignerInompatible/insulin-like_growth_factor_signaling.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlCellDesignerInompatible/neuronal_muscle_signalling.sbgn"});
    data.add(new Object[] {"testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotypeTest.sbgn"});
    return data;
  }

  @Test
  @Transactional
  public void dbSerializationTest() throws Exception {
    ModelDao modelDao = applicationContext.getBean(ModelDao.class);
    
    SbgnmlXmlParser parser = new SbgnmlXmlParser();

    Model model = parser.createModel(fileName, new File(fileName));

    modelDao.add(model);
    modelDao.flush();
    modelDao.commit();
    modelDao.evict(model);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
    modelDao.delete(model2);
  }

  @Before
  public void setUp() throws Exception {
    DbUtils dbUtils = applicationContext.getBean(DbUtils.class);
    // we use custom threads because in layoutservice there is commit method
    // called, and because of that hibernate session injected by spring
    // cannot
    // commit at the end of the test case

    dbUtils.createSessionForCurrentThread();

  }

  @After
  public void tearDown() throws Exception {
    DbUtils dbUtils = applicationContext.getBean(DbUtils.class);
    // close session
    dbUtils.closeSessionForCurrentThread();
  }

}
