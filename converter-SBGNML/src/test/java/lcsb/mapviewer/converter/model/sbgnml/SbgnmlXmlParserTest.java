package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.ReactionComparator;

@RunWith(Parameterized.class)
public class SbgnmlXmlParserTest extends SbgnmlTestFunctions {

  Logger logger = LogManager.getLogger();

  private Path filePath;

  public SbgnmlXmlParserTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/sbgnmlParserTestFiles/sbgnmlFiles")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".sbgn")
          && !fPath.getFileName().toString().startsWith("ASTHMA")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    String dir = Files.createTempDirectory("sbgn-temp-images-dir").toFile().getAbsolutePath();

    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));

    // Create and display image of parsed map
    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    NormalImageGenerator nig = new PngImageGenerator(params);
    String pngFilePath = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat(".png");
    nig.saveToFile(pngFilePath);

    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    String xmlString = cellDesignerXmlParser.model2String(model);

    String cellDesignerFilePath = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat(".xml");
    PrintWriter out = new PrintWriter(cellDesignerFilePath);
    out.print(xmlString);
    out.close();

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

    AbstractImageGenerator.Params params2 = new AbstractImageGenerator.Params().height(model2.getHeight())
        .width(model2.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model2);
    NormalImageGenerator nig2 = new PngImageGenerator(params2);
    String pngFilePath2 = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat("_2.png");
    nig2.saveToFile(pngFilePath2);

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    comparator.getReactionSetComparator().setObjectComparator(new ReactionComparator(1.0, true));
    assertEquals(0, comparator.compare(model, model2));
    FileUtils.deleteDirectory(new File(dir));
  }

}
