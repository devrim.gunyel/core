package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class SbgnmlSerializationTest extends SbgnmlTestFunctions {

  Logger logger = LogManager.getLogger();

  private Path filePath;

  public SbgnmlSerializationTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/CellDesigner-serializable")).forEach(fPath -> {
      if (Files.isRegularFile(fPath)) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void modelSerialization() throws Exception {
    Converter converter = new CellDesignerXmlParser();
    Converter sbgnmlConverter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));

    String sbgn = sbgnmlConverter.model2String(model);
    Model model2 = sbgnmlConverter.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(sbgn.getBytes(StandardCharsets.UTF_8))));

    model2.setIdModel(model.getIdModel());
    model2.setName(model.getName());
    for (Drawable d : model.getDrawables()) {
      d.setZ(null);
    }
    for (Drawable d : model2.getDrawables()) {
      d.setZ(null);
    }
    for (Species e: model.getSpeciesList()) {
      e.setInitialAmount(null);
      e.setInitialConcentration(null);
      e.setCharge(null);
      e.setPositionToCompartment(null);
    }
    model.getUnits().clear();
    
    for (Species e: model2.getSpeciesList()) {
      e.setInitialAmount(null);
      e.setInitialConcentration(null);
      e.setCharge(null);
      e.setPositionToCompartment(null);
    }
    
    assertEquals("Import/export shouldn't produce any wanrnings", 0, super.getWarnings().size());
    ModelComparator mc = new ModelComparator(0.001);
    assertEquals(0, mc.compare(model, model2));
  }

}
