package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbgnmlXmlExporterTest extends SbgnmlTestFunctions {

  Logger logger = LogManager.getLogger(SbgnmlXmlExporterTest.class.getName());

  private Path filePath;

  public SbgnmlXmlExporterTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/sbgnmlParserTestFiles/sbgnmlFiles")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".sbgn")) {
        data.add(new Object[] { fPath});
      }
    });
    Files.walk(Paths.get("testFiles/sbgnmlCellDesignerInompatible")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".sbgn")) {
        data.add(new Object[] { fPath});
      }
    });
    return data;
  }

  private void parseAndExport(Path filePath) throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams()
        .filename(filePath.toAbsolutePath().toString()));

    converter.model2File(model, File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath());
  }

  @Test
  public void test() throws Exception {
    parseAndExport(filePath);
  }

}
