package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * This class represent modification residue in a Species.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerModificationResidue implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(CellDesignerModificationResidue.class.getName());

  /**
   * Identifier of the modification. Must be unique in single map model.
   */
  private String idModificationResidue = "";

  /**
   * Name of the modification.
   */
  private String name = "";

  private Boolean active = null;

  private ModificationType modificationType;

  /**
   * Position on the species in graphic representation.
   */
  private Double pos;

  /**
   * Some strange parameter in CellDesigner. No idea what is it for.
   */
  private String side = "";

  /**
   * State in which this modification is.
   */
  private ModificationState state = null;

  /**
   * Where this modification is located (on which side of the border).
   */
  private Double angle = null;

  /**
   * How big is this modification (used only for some types of the modification).
   */
  private Double size = null;

  /**
   * Species to which this modification belong to.
   */
  private CellDesignerSpecies<?> species;

  /**
   * Default constructor.
   */
  public CellDesignerModificationResidue() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param mr
   *          original object from which data is taken
   */
  public CellDesignerModificationResidue(CellDesignerModificationResidue mr) {
    this.idModificationResidue = mr.idModificationResidue;
    this.name = mr.name;
    this.angle = mr.angle;
    this.active = mr.active;
    this.size = mr.size;
    this.side = mr.side;
    this.pos = mr.pos;
    this.state = mr.state;
    this.modificationType = mr.modificationType;
  }

  /**
   * Constructor that creates object from model representation of modification.
   * 
   * @param mr
   *          model representation of {@link ModificationResidue}
   */
  public CellDesignerModificationResidue(ModificationResidue mr) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(mr.getSpecies(), false);
    this.idModificationResidue = mr.getIdModificationResidue();
    this.name = mr.getName();
    this.angle = converter.getAngleForPoint(mr.getSpecies(), mr.getPosition());
    this.pos = converter.getCellDesignerPositionByCoordinates(mr);
    if (mr instanceof Residue) {
      this.modificationType = ModificationType.RESIDUE;
      this.state = ((Residue) mr).getState();
    } else if (mr instanceof ModificationSite) {
      this.modificationType = ModificationType.MODIFICATION_SITE;
      this.state = ((ModificationSite) mr).getState();
    } else if (mr instanceof CodingRegion) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.CODING_REGION;
    } else if (mr instanceof ProteinBindingDomain) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.PROTEIN_BINDING_DOMAIN;
    } else if (mr instanceof RegulatoryRegion) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.REGULATORY_REGION;
    } else if (mr instanceof TranscriptionSite) {
      TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
      this.size = converter.getCellDesignerSize(mr);
      this.active = transcriptionSite.getActive();
      if (transcriptionSite.getDirection().equals("LEFT")) {
        this.modificationType = ModificationType.TRANSCRIPTION_SITE_LEFT;
      } else {
        this.modificationType = ModificationType.TRANSCRIPTION_SITE_RIGHT;
      }
    } else if (mr instanceof BindingRegion) {
      this.modificationType = ModificationType.BINDING_REGION;
      if (Math.abs(mr.getPosition().getX() - mr.getSpecies().getX()) < Configuration.EPSILON) {
        this.size = ((BindingRegion) mr).getHeight() / mr.getSpecies().getHeight();
      } else if (Math
          .abs(mr.getPosition().getX() - mr.getSpecies().getX() - mr.getSpecies().getWidth()) < Configuration.EPSILON) {
        this.size = ((BindingRegion) mr).getHeight() / mr.getSpecies().getHeight();
      } else {
        this.size = ((BindingRegion) mr).getWidth() / mr.getSpecies().getWidth();
      }

    } else {
      throw new InvalidArgumentException("Unknown modification type: " + mr.getClass());
    }
  }

  /**
   * Default constructor.
   * 
   * @param id
   *          identifier of the modification residue
   */
  public CellDesignerModificationResidue(String id) {
    this.idModificationResidue = id;
  }

  /**
   * Updates fields in the object with the data given in the parameter
   * modification.
   * 
   * @param mr
   *          modification residue from which data will be used to update
   */
  public void update(CellDesignerModificationResidue mr) {
    if (mr.getName() != null && !mr.getName().equals("")) {
      this.name = mr.name;
    }
    if (mr.getAngle() != null) {
      this.angle = mr.angle;
    }
    if (mr.getActive() != null) {
      this.active = mr.active;
    }
    if (mr.getSize() != null) {
      this.size = mr.size;
    }
    if (mr.getSide() != null && !mr.getSide().equals("")) {
      this.side = mr.side;
    }
    if (mr.getState() != null) {
      this.state = mr.state;
    }
    if (mr.getPos() != null) {
      this.setPos(mr.getPos());
    }
    if (mr.getModificationType() != null) {
      this.setModificationType(mr.modificationType);
    }
  }

  @Override
  public String toString() {
    String result = getIdModificationResidue() + "," + getName() + "," + getState() + "," + getAngle() + "," + getSize()
        + getPos() + "," + "," + getSide() + ",";
    return result;
  }

  /**
   * Creates copy of the object.
   * 
   * @return copy of the object.
   */
  public CellDesignerModificationResidue copy() {
    if (this.getClass() == CellDesignerModificationResidue.class) {
      return new CellDesignerModificationResidue(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the id
   * @see #idModificationResidue
   */
  public String getIdModificationResidue() {
    return idModificationResidue;
  }

  /**
   * @param idModificationResidue
   *          the id to set
   * @see #idModificationResidue
   */
  public void setIdModificationResidue(String idModificationResidue) {
    this.idModificationResidue = idModificationResidue;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the side
   * @see #side
   */
  public String getSide() {
    return side;
  }

  /**
   * @param side
   *          the side to set
   * @see #side
   */
  public void setSide(String side) {
    this.side = side;
  }

  /**
   * @return the state
   * @see #state
   */
  public ModificationState getState() {
    return state;
  }

  /**
   * @param state
   *          the state to set
   * @see #state
   */
  public void setState(ModificationState state) {
    this.state = state;
  }

  /**
   * @return the angle
   * @see #angle
   */
  public Double getAngle() {
    return angle;
  }

  /**
   * Sets {@link #angle} .
   *
   * @param text
   *          angle in text format
   */
  public void setAngle(String text) {
    try {
      if (text != null && !text.equals("")) {
        angle = Double.parseDouble(text);
      } else {
        angle = null;
      }
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid angle: " + text, e);
    }

  }

  /**
   * @param angle
   *          the angle to set
   * @see #angle
   */
  public void setAngle(Double angle) {
    this.angle = angle;
  }

  /**
   * @return the size
   * @see #size
   */
  public Double getSize() {
    return size;
  }

  /**
   * Sets {@link #size}.
   *
   * @param text
   *          size in text format.
   */
  public void setSize(String text) {
    try {
      if (text != null && !text.equals("")) {
        size = Double.parseDouble(text);
      } else {
        size = null;
      }
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid size: " + text, e);
    }
  }

  /**
   * @param size
   *          the size to set
   * @see #size
   */
  public void setSize(Double size) {
    this.size = size;
  }

  /**
   * @return the species
   * @see #species
   */
  public CellDesignerSpecies<?> getSpecies() {
    return species;
  }

  /**
   * @param species
   *          the species to set
   * @see #species
   */
  public void setSpecies(CellDesignerSpecies<?> species) {
    this.species = species;
  }

  /**
   * Creates model representation of {@link ModificationResidue}.
   * 
   * @return {@link ModificationResidue} representing this object in a model
   */
  public ModificationResidue createModificationResidue(Element element) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(element, false);

    if (modificationType == null) {
      throw new InvalidArgumentException("No type information for modification: " + idModificationResidue);
    } else if (modificationType.equals(ModificationType.RESIDUE)) {
      return createResidue(element, converter);
    } else if (modificationType.equals(ModificationType.BINDING_REGION)) {
      return createBindingRegion(element, converter);
    } else if (modificationType.equals(ModificationType.MODIFICATION_SITE)) {
      return createModificationSite(element, converter);
    } else if (modificationType.equals(ModificationType.CODING_REGION)) {
      return createCodingRegion(element, converter);
    } else if (modificationType.equals(ModificationType.PROTEIN_BINDING_DOMAIN)) {
      return createProteinBindingDomain(element, converter);
    } else if (modificationType.equals(ModificationType.REGULATORY_REGION)) {
      return createRegulatoryRegion(element, converter);
    } else if (modificationType.equals(ModificationType.TRANSCRIPTION_SITE_LEFT)
        || modificationType.equals(ModificationType.TRANSCRIPTION_SITE_RIGHT)) {
      return createTranscriptionSite(element, converter);
    } else {
      throw new InvalidArgumentException("Unknown modification type: " + modificationType);
    }

  }

  private ProteinBindingDomain createProteinBindingDomain(Element element, CellDesignerAliasConverter converter) {
    ProteinBindingDomain result = new ProteinBindingDomain();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    result.setPosition(converter.getCoordinatesByPosition(element, pos, result.getWidth()));
    return result;
  }

  private BindingRegion createBindingRegion(Element element, CellDesignerAliasConverter converter) {
    BindingRegion result = new BindingRegion();
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    result.setPosition(converter.getResidueCoordinates(element, angle));
    if (Math.abs(result.getPosition().getX() - element.getX()) < Configuration.EPSILON) {
      result.setHeight(element.getHeight() * size);
    } else if (Math.abs(result.getPosition().getX() - element.getX() - element.getWidth()) < Configuration.EPSILON) {
      result.setHeight(element.getHeight() * size);
    } else {
      result.setWidth(element.getWidth() * size);
    }
    return result;
  }

  private CodingRegion createCodingRegion(Element element, CellDesignerAliasConverter converter) {
    CodingRegion result = new CodingRegion();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    if (pos == null) {
      pos = angle;
    }
    result.setPosition(converter.getCoordinatesByPosition(element, pos, result.getWidth()));
    return result;
  }

  private RegulatoryRegion createRegulatoryRegion(Element element, CellDesignerAliasConverter converter) {
    RegulatoryRegion result = new RegulatoryRegion();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    result.setPosition(converter.getCoordinatesByPosition(element, angle, result.getWidth()));
    return result;
  }

  private TranscriptionSite createTranscriptionSite(Element element, CellDesignerAliasConverter converter) {
    TranscriptionSite result = new TranscriptionSite();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    result.setActive(active);
    result.setPosition(converter.getCoordinatesByPosition(element, angle, result.getWidth()));
    if (modificationType.equals(ModificationType.TRANSCRIPTION_SITE_LEFT)) {
      result.setDirection("LEFT");
    } else {
      result.setDirection("RIGHT");
    }
    return result;
  }

  private ModificationSite createModificationSite(Element element, CellDesignerAliasConverter converter) {
    ModificationSite result = new ModificationSite();
    result.setState(this.getState());
    result.setIdModificationResidue(this.getIdModificationResidue());
    result.setName(this.getName());
    if (angle == null) {
      angle = pos;
    }
    if (angle == null) {
      logger.warn("Angle is not defined using 0 as default");
      angle = 0.0;
    }
    result.setPosition(converter.getCoordinatesByPosition(element, angle));
    return result;
  }

  private Residue createResidue(Element element, CellDesignerAliasConverter converter) {
    Residue result = new Residue();
    result.setState(this.getState());
    result.setIdModificationResidue(this.getIdModificationResidue());
    result.setName(this.getName());
    if (angle == null) {
      logger.warn("Angle is not defined using 0 as default");
      angle = 0.0;
    }
    result.setPosition(converter.getResidueCoordinates(element, angle));
    return result;
  }

  public ModificationType getModificationType() {
    return modificationType;
  }

  public void setModificationType(ModificationType modificationType) {
    this.modificationType = modificationType;
  }

  /**
   * @return the pos
   * @see #pos
   */
  public Double getPos() {
    return pos;
  }

  /**
   * Sets position from the string.
   *
   * @param text
   *          position to parse and set
   * @see #pos
   */
  public void setPos(String text) {
    try {
      pos = Double.parseDouble(text);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid pos: " + text, e);
    }
  }

  /**
   * @param pos
   *          the pos to set
   * @see #pos
   */
  public void setPos(Double pos) {
    this.pos = pos;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public void setActive(String text) {
    if (text == null) {
      this.active = null;
    } else {
      this.active = "true".equalsIgnoreCase(text);
    }
  }

}
