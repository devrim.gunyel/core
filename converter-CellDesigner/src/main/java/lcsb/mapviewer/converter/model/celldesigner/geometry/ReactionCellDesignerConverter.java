package lcsb.mapviewer.converter.model.celldesigner.geometry;

/**
 * Class that provides CellDesigner specific graphical information for Reaction.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public final class ReactionCellDesignerConverter {
  /**
   * What is the size of rectangle that is drawn in center point of the reaction.
   */
  public static final double RECT_SIZE = 10;

  /**
   * Default constructor that prevents instantiation.
   */
  protected ReactionCellDesignerConverter() {

  }
}
