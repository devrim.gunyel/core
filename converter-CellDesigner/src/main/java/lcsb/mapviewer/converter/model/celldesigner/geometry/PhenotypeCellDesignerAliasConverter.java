package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.*;
import java.util.ArrayList;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class that provides CellDesigner specific graphical information for
 * Phenotype. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class PhenotypeCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Species> {

  /**
   * Default constructor.
   * 
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected PhenotypeCellDesignerAliasConverter(boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(Species alias, CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    return getPolygonTransformation().getPointOnPolygonByAnchor(getPointsForAlias(alias), anchor);
  }

  @Override
  public PathIterator getBoundPathIterator(Species alias) {
    return getPhenotypePath(alias).getPathIterator(new AffineTransform());
  }

  /**
   * Returns shape of the Phenotype as a list of points.
   *
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getPointsForAlias(Element alias) {
    ArrayList<Point2D> list = new ArrayList<Point2D>();

    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();

    // CHECKSTYLE:OFF
    list.add(new Point2D.Double(x, y + height / 2));
    list.add(new Point2D.Double(x + width / 6, y));
    list.add(new Point2D.Double(x + width / 2, y));
    list.add(new Point2D.Double(x + width * 5 / 6, y));
    list.add(new Point2D.Double(x + width, y + height / 2));
    list.add(new Point2D.Double(x + width * 5 / 6, y + height));
    list.add(new Point2D.Double(x + width / 2, y + height));
    list.add(new Point2D.Double(x + width / 6, y + height));
    // CHECKSTYLE:ON
    return list;
  }

  /**
   * Returns shape of the Phenotype as a GeneralPath object.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return GeneralPath object defining border of the given alias
   */
  private GeneralPath getPhenotypePath(Element alias) {
    // CHECKSTYLE:OFF
    GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 6);
    path.moveTo(alias.getX() + alias.getWidth() / 6, alias.getY());
    path.lineTo(alias.getX() + alias.getWidth() * 5 / 6, alias.getY());
    path.lineTo(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight() / 2);
    path.lineTo(alias.getX() + alias.getWidth() * 5 / 6, alias.getY() + alias.getHeight());
    path.lineTo(alias.getX() + alias.getWidth() / 6, alias.getY() + alias.getHeight());
    path.lineTo(alias.getX(), alias.getY() + alias.getHeight() / 2);
    // CHECKSTYLE:ON
    path.closePath();
    return path;
  }

}
