package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * Factory util that creates {@link PolylineData} objects from CellDesigner
 * structure.
 * 
 * @author Piotr Gawron
 * 
 */
public final class PolylineDataFactory {

  /**
   * Default constructor that prevent instantiation.
   */
  private PolylineDataFactory() {
  }

  /**
   * Creates {@link PolylineData} object from two CellDesigner base points and
   * list of points (in CellDesigner format).
   * 
   * @param baseA
   *          first base point
   * @param baseB
   *          second base point
   * @param points
   *          intermediate points in CellDesigner format
   * @return {@link PolylineData} object representing input data
   */
  public static PolylineData createPolylineDataFromEditPoints(Point2D baseA, Point2D baseB, List<Point2D> points) {
    List<Point2D> pointsList = new CellDesignerLineTransformation().getLinePointsFromPoints(baseA, baseB, points);
    return new PolylineData(pointsList);
  }

  /**
   * Creates {@link PolylineData} object from two CellDesigner base points and
   * list of points (in CellDesigner format).
   * 
   * @param baseA
   *          first base point
   * @param baseB
   *          second base point
   * @param points
   *          structure with intermediate points in CellDesigner format
   * @return {@link PolylineData} object representing input data
   */
  public static PolylineData createPolylineDataFromEditPoints(Point2D baseA, Point2D baseB, EditPoints points) {
    if (points != null) {
      return createPolylineDataFromEditPoints(baseA, baseB, points.getPoints());
    } else {
      return createPolylineDataFromEditPoints(baseA, baseB, new ArrayList<Point2D>());
    }
  }

  /**
   * Creates {@link PolylineData} object from the parameter that doesn't contain
   * collinear points.
   * 
   * @param ld
   *          original line
   * @return line without collinear points
   */
  public static PolylineData removeCollinearPoints(PolylineData ld) {
    LineTransformation lt = new LineTransformation();
    PolylineData result = new PolylineData(ld);
    int pointNumber = 1;
    while (pointNumber < result.getPoints().size() - 1) {
      Point2D previousPoint = result.getPoints().get(pointNumber - 1);
      Point2D nextPoint = result.getPoints().get(pointNumber + 1);
      Point2D currentPoint = result.getPoints().get(pointNumber);
      Point2D closestPointOnLine = lt.closestPointOnSegmentLineToPoint(previousPoint, nextPoint, currentPoint);
      if (closestPointOnLine.distance(currentPoint) <= Configuration.EPSILON) {
        result.getPoints().remove(pointNumber);
      } else {
        pointNumber++;
      }
    }
    return result;
  }
}
