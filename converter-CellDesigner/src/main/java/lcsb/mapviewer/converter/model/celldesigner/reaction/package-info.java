/**
 * This package contains parsers used for parsing CellDesiger xml for
 * {@link lcsb.mapviewer.db.model.map.reaction.Reaction Reaction}.
 */
package lcsb.mapviewer.converter.model.celldesigner.reaction;
