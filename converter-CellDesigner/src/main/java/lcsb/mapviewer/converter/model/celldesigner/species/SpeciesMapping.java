package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.model.celldesigner.structure.*;
import lcsb.mapviewer.model.map.species.*;

/**
 * This enum contains information about mapping between CellDesigner xml nodes
 * and classes that extend {@link CellDesignerSpecies}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum SpeciesMapping {
  /**
   * {@link Protein}.
   */
  PROTEIN(CellDesignerProtein.class, Protein.class, "PROTEIN"),

  /**
   * {@link Gene}.
   */
  GENE(CellDesignerGene.class, Gene.class, "GENE"),

  /**
   * {@link ComplexSpecies}.
   */
  COMPLEX(CellDesignerComplexSpecies.class, Complex.class, "COMPLEX"),

  /**
   * {@link SimpleMolecule}.
   */
  SIMPLE_MOLECULE(CellDesignerSimpleMolecule.class, SimpleMolecule.class, "SIMPLE_MOLECULE"),

  /**
   * {@link Ion}.
   */
  ION(CellDesignerIon.class, Ion.class, "ION"),

  /**
   * {@link Phenotype}.
   */
  PHENOTYPE(CellDesignerPhenotype.class, Phenotype.class, "PHENOTYPE"),

  /**
   * {@link Drug}.
   */
  DRUG(CellDesignerDrug.class, Drug.class, "DRUG"),

  /**
   * {@link Rna}.
   */
  RNA(CellDesignerRna.class, Rna.class, "RNA"),

  /**
   * {@link AntisenseRna}.
   */
  ANTISENSE_RNA(CellDesignerAntisenseRna.class, AntisenseRna.class, "ANTISENSE_RNA"),

  /**
   * {@link Unknown}.
   */
  UNKNOWN(CellDesignerUnknown.class, Unknown.class, "UNKNOWN"),

  /**
   * {@link Degraded}.
   */
  DEGRADED(CellDesignerDegraded.class, Degraded.class, "DEGRADED");

  /**
   * CellDesigner xml node type.
   */
  private String cellDesignerString;

  /**
   * Class that should be used to represent cell designer node.
   */
  private Class<? extends CellDesignerSpecies<?>> cellDesignerClazz;

  /**
   * Model class that represent the species.
   */
  private Class<? extends Element> modelClazz;

  /**
   * Default constructor.
   * 
   * @param cellDesignerClazz
   *          {@link #clazz}
   * @param modelClazz
   *          {@link #clazz class} in model corresponding to CellDesigner object
   * @param cellDesignerString
   *          {@link #cellDesignerString}
   */
  SpeciesMapping(Class<?> cellDesignerClazz, Class<? extends Element> modelClazz, String cellDesignerString) {
    this.cellDesignerString = cellDesignerString;
    this.setCellDesignerClazz(cellDesignerClazz);
    this.modelClazz = modelClazz;
  }

  /**
   * Returns {@link SpeciesMapping} for given CellDesigner xml node name.
   *
   * @param string
   *          CellDesigner xml node name
   * @return {@link SpeciesMapping} for given CellDesigner xml node name
   */
  public static SpeciesMapping getMappingByString(String string) {
    for (SpeciesMapping mapping : SpeciesMapping.values()) {
      if (mapping.getCellDesignerString().equalsIgnoreCase(string)) {
        return mapping;
      }
    }
    return null;
  }

  /**
   * Returns {@link SpeciesMapping} for given {@link CellDesignerSpecies} class.
   *
   * @param searchClazz
   *          {@link CellDesignerSpecies} class
   * @return {@link SpeciesMapping} for given {@link CellDesignerSpecies} class
   */
  public static SpeciesMapping getMappingByModelClass(Class<? extends Element> searchClazz) {
    for (SpeciesMapping mapping : SpeciesMapping.values()) {
      if (mapping.getModelClazz().isAssignableFrom(searchClazz)) {
        return mapping;
      }
    }
    return null;
  }

  /**
   * @return the cellDesignerString
   * @see #cellDesignerString
   */
  public String getCellDesignerString() {
    return cellDesignerString;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<? extends CellDesignerSpecies<?>> getCellDesignerClazz() {
    return cellDesignerClazz;
  }

  /**
   * Sets new cellDesignerClazz value.
   *
   * @param cellDesignerClazz2
   *          new cellDesignerClazz value
   */
  @SuppressWarnings("unchecked")
  private void setCellDesignerClazz(Class<?> cellDesignerClazz2) {
    cellDesignerClazz = (Class<? extends CellDesignerSpecies<?>>) cellDesignerClazz2;
  }

  /**
   * Returns model class that represent the species.
   *
   * @return model class that represent the species
   */
  public Class<? extends Element> getModelClazz() {
    return modelClazz;
  }

  /**
   * Creates instance of {@link CellDesignerSpecies} specific for this
   * {@link SpeciesMapping}.
   *
   * @param result
   *          parameter that should be passed to the constructor
   * @return new instance of {@link CellDesignerSpecies} specific for this
   *         {@link SpeciesMapping}.
   */
  public CellDesignerSpecies<?> createSpecies(CellDesignerSpecies<?> result) {
    try {
      return cellDesignerClazz.getConstructor(result.getClass()).newInstance(result);
    } catch (Exception e) {
      throw new InvalidStateException(e);
    }
  }
}
