package lcsb.mapviewer.converter.model.celldesigner.parameter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;

public class ParameterXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ParameterXmlParser.class);

  private Model model;

  public ParameterXmlParser(Model model) {
    this.model = model;
  }

  public SbmlParameter parseParameter(Node unitNode) throws InvalidXmlSchemaException {
    String parameterId = XmlParser.getNodeAttr("id", unitNode);

    SbmlParameter result = new SbmlParameter(parameterId);
    result.setName(XmlParser.getNodeAttr("name", unitNode));
    result.setValue(Double.parseDouble(XmlParser.getNodeAttr("value", unitNode)));
    result.setUnits(model.getUnitsByUnitId(XmlParser.getNodeAttr("units", unitNode)));
    return result;
  }

  public String toXml(SbmlParameter sbmlFunction) {
    StringBuilder result = new StringBuilder();
    result.append("<parameter ");
    result.append("id=\"" + sbmlFunction.getElementId() + "\" ");
    result.append("name=\"" + sbmlFunction.getName() + "\" ");
    result.append("value=\"" + sbmlFunction.getValue() + "\" ");
    if (sbmlFunction.getUnits() != null) {
      result.append("units=\"" + sbmlFunction.getUnits().getUnitId() + "\" ");
    }
    result.append("/>");

    return result.toString();
  }
}
