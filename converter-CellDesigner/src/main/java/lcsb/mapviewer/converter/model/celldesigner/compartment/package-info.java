/**
 * This package contains converters for CellDesigner xml nodes representing
 * compartments.
 */
package lcsb.mapviewer.converter.model.celldesigner.compartment;
