package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.*;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.*;

/**
 * Parser of CellDesigner xml used for parsing SpeciesAlias.
 * 
 * @author Piotr Gawron
 * 
 * @see Complex
 */
public class SpeciesAliasXmlParser extends AbstractAliasXmlParser<Species> {
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(SpeciesAliasXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Model for which we parse elements.
   */
  private Model model;

  /**
   * Default constructor with model object for which we parse data.
   * 
   * @param model
   *          model for which we parse elements
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public SpeciesAliasXmlParser(CellDesignerElementCollection elements, Model model) {
    this.elements = elements;
    this.model = model;
  }

  @Override
  Species parseXmlAlias(Node aliasNode) throws InvalidXmlSchemaException {

    String speciesId = XmlParser.getNodeAttr("species", aliasNode);
    String aliasId = XmlParser.getNodeAttr("id", aliasNode);

    String errorPrefix = "[" + speciesId + "," + aliasId + "]\t";
    CellDesignerSpecies<?> species = elements.getElementByElementId(speciesId);
    if (species == null) {
      throw new InvalidXmlSchemaException("Unknown species for alias (speciesId: " + speciesId + ")");
    }
    if (species instanceof CellDesignerComplexSpecies) {
      logger.warn(
          errorPrefix + "Species is defined as a complex, but alias is not a complex. Changing alias to complex.");
    }

    elements.addElement(species, aliasId);
    try {
      Species result = species.createModelElement(aliasId);

      String state = "usual";
      NodeList nodes = aliasNode.getChildNodes();
      View usualView = null;
      View briefView = null;
      for (int x = 0; x < nodes.getLength(); x++) {
        Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
            result.setActivity(XmlParser.getNodeValue(node).equalsIgnoreCase("active"));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
            result.setX(XmlParser.getNodeAttr("X", node));
            result.setY(XmlParser.getNodeAttr("Y", node));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
            result.setFontSize(XmlParser.getNodeAttr("size", node));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
            state = XmlParser.getNodeAttr("state", node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
            usualView = getCommonParser().getView(node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
            briefView = getCommonParser().getView(node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
            processAliasState(node, result);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
            if (species instanceof CellDesignerProtein) {
              ((CellDesignerProtein<?>) species)
                  .setStructuralStateAngle(Double.parseDouble(XmlParser.getNodeAttr("angle", node)));
            }
          } else {
            throw new InvalidXmlSchemaException(
                errorPrefix + "Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
          }
        }
      }

      View view = null;
      if (state.equalsIgnoreCase("usual")) {
        view = usualView;
      } else if (state.equalsIgnoreCase("brief")) {
        view = briefView;
      }

      if (view != null) {
        // inner position defines the position in compartment
        // result.moveBy(view.innerPosition);
        result.setWidth(view.getBoxSize().width);
        result.setHeight(view.getBoxSize().height);
        result.setLineWidth(view.getSingleLine().getWidth());
        result.setFillColor(view.getColor());
      } else {
        throw new InvalidXmlSchemaException(errorPrefix + "No view in Alias");
      }
      result.setState(state);
      String compartmentAliasId = XmlParser.getNodeAttr("compartmentAlias", aliasNode);
      if (!compartmentAliasId.isEmpty()) {
        Compartment compartment = model.getElementByElementId(compartmentAliasId);
        if (compartment == null) {
          throw new InvalidXmlSchemaException(errorPrefix + "CompartmentAlias does not exist: " + compartmentAliasId);
        } else {
          result.setCompartment(compartment);
          compartment.addElement(result);
        }
      }
      String complexAliasId = XmlParser.getNodeAttr("complexSpeciesAlias", aliasNode);
      if (!complexAliasId.isEmpty()) {
        Complex complex = model.getElementByElementId(complexAliasId);
        if (complex == null) {
          throw new InvalidXmlSchemaException(
              errorPrefix + "ComplexAlias does not exist: " + complexAliasId + ", current: " + result.getElementId());
        } else {
          result.setComplex(complex);
          complex.addSpecies(result);
        }
      }

      species.updateModelElementAfterLayoutAdded(result);
      return result;
    } catch (InvalidArgumentException e) {
      throw new InvalidXmlSchemaException(errorPrefix + e.getMessage(), e);
    } catch (NotImplementedException e) {
      throw new InvalidXmlSchemaException(errorPrefix + "Problem with creating species", e);
    }
  }

  @Override
  public String toXml(Species species) {
    Compartment ca = null;
    // artificial compartment aliases should be excluded
    if (species.getCompartment() != null && !(species.getCompartment() instanceof PathwayCompartment)) {
      ca = (Compartment) species.getCompartment();
    } else if (species.getComplex() == null) {
      ModelData model = species.getModelData();
      if (model != null) {
        for (Compartment cAlias : model.getModel().getCompartments()) {
          if (!(cAlias instanceof PathwayCompartment) && cAlias.cross(species)) {
            if (ca == null) {
              ca = cAlias;
            } else if (ca.getSize() > cAlias.getSize()) {
              ca = cAlias;
            }
          }
        }
      }
    }

    Complex complex = species.getComplex();

    String compartmentAliasId = null;
    if (ca != null) {
      compartmentAliasId = ca.getElementId();
    }
    StringBuilder sb = new StringBuilder("");
    sb.append("<celldesigner:speciesAlias ");
    sb.append("id=\"" + species.getElementId() + "\" ");
    sb.append("species=\"" + elements.getElementId(species) + "\" ");

    if (complex != null) {
      sb.append("complexSpeciesAlias=\"" + complex.getElementId() + "\" ");
    } else if (compartmentAliasId != null) {
      // you cannot have both if there are both then CellDesigner behaves improperly
      sb.append("compartmentAlias=\"" + compartmentAliasId + "\" ");
    }
    sb.append(">\n");

    if (species.getActivity() != null) {
      if (species.getActivity()) {
        sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
      } else {
        sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
      }
    }

    sb.append("<celldesigner:bounds ");
    sb.append("x=\"" + species.getX() + "\" ");
    sb.append("y=\"" + species.getY() + "\" ");
    sb.append("w=\"" + species.getWidth() + "\" ");
    sb.append("h=\"" + species.getHeight() + "\" ");
    sb.append("/>\n");

    sb.append(createFontTag(species));

    // TODO to be improved
    sb.append("<celldesigner:view state=\"usual\"/>\n");
    sb.append("<celldesigner:usualView>");
    sb.append("<celldesigner:innerPosition x=\"" + species.getX() + "\" y=\"" + species.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + XmlParser.colorToString(species.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:usualView>\n");
    sb.append("<celldesigner:briefView>");
    sb.append("<celldesigner:innerPosition x=\"" + species.getX() + "\" y=\"" + species.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + XmlParser.colorToString(species.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:briefView>\n");
    if (species.getStateLabel() != null || species.getStatePrefix() != null) {
      sb.append("<celldesigner:info state=\"open\" prefix=\"" + species.getStatePrefix() + "\" label=\""
          + species.getStateLabel() + "\"/>\n");
    }
    sb.append(createStructuralStateTag(species));
    sb.append("</celldesigner:speciesAlias>\n");
    return sb.toString();
  }

  /**
   * Process node with information about alias state and puts data into alias.
   *
   * @param node
   *          node where information about alias state is stored
   * @param alias
   *          alias object to be modified if necessary
   */
  private void processAliasState(Node node, Species alias) {
    String state = XmlParser.getNodeAttr("state", node);
    if ("open".equalsIgnoreCase(state)) {
      String prefix = XmlParser.getNodeAttr("prefix", node);
      String label = XmlParser.getNodeAttr("label", node);
      alias.setStatePrefix(prefix);
      alias.setStateLabel(label);
    } else if ("empty".equalsIgnoreCase(state)) {
      return;
    } else if (state == null || state.isEmpty()) {
      return;
    } else {
      throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unkown alias state: " + state);
    }

  }
}
