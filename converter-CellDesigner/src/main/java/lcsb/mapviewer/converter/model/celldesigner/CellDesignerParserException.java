package lcsb.mapviewer.converter.model.celldesigner;

/**
 * Exception that should be thrown when the parser encounter problem with
 * CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class CellDesignerParserException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public CellDesignerParserException() {
    super(); // call superclass constructor
  }

  /**
   * Constructor receives some kind of message.
   * 
   * @param err
   *          message associated with exception
   */

  public CellDesignerParserException(final String err) {
    super(err);
  }

  /**
   * Constructor receives some kind of message and parent exception.
   * 
   * @param err
   *          message associated with exception
   * @param throwable
   *          parent exception
   */
  public CellDesignerParserException(final String err, final Throwable throwable) {
    super(err, throwable);
  }

  /**
   * Public constructor with parent exception that was caught.
   * 
   * @param e
   *          parent exception
   */

  public CellDesignerParserException(final Throwable e) {
    super(e);
  }

  public String getMessageWithContext() {
    String message;
    if (this.getCause() != null) {
      message = this.getCause().getMessage();
    } else {
      message = this.getMessage();
    }
    return getMessageContext() + " " + message;
  }

  public abstract String getMessageContext();
}
