package lcsb.mapviewer.converter.model.celldesigner.annotation;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class NoteFieldTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testVallues() {
    for (NoteField field : NoteField.values()) {
      assertNotNull(NoteField.valueOf(field.toString()));
      assertNotNull(field.getClazz());
    }
  }

}
