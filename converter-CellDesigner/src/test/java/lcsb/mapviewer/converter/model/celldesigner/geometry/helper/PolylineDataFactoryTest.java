package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.PolylineData;

public class PolylineDataFactoryTest {

  @Test
  public void testPrivateCotnstructor() throws Exception {
    try {
      Constructor<?> constr = PolylineDataFactory.class.getDeclaredConstructor(new Class<?>[] {});
      constr.setAccessible(true);
      assertNotNull(constr.newInstance(new Object[] {}));
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testRemoveCollinearPoints2() throws Exception {
    try {
      PolylineData line = new PolylineData();
      line.addPoint(new Point2D.Double(10, 20));
      line.addPoint(new Point2D.Double(11, 30));
      line.addPoint(new Point2D.Double(12, 40));
      PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
      assertEquals(2, modifiedLine.getPoints().size());
      assertEquals(0, modifiedLine.getBeginPoint().distance(new Point2D.Double(10, 20)), Configuration.EPSILON);
      assertEquals(0, modifiedLine.getEndPoint().distance(new Point2D.Double(12, 40)), Configuration.EPSILON);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testRemoveCollinearPoints() throws Exception {
    try {
      PolylineData line = new PolylineData();
      line.addPoint(new Point2D.Double(10, 20));
      line.addPoint(new Point2D.Double(10, 30));
      line.addPoint(new Point2D.Double(10, 35));
      line.addPoint(new Point2D.Double(10, 40));
      PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
      assertEquals(2, modifiedLine.getPoints().size());
      assertEquals(0, modifiedLine.getBeginPoint().distance(new Point2D.Double(10, 20)), Configuration.EPSILON);
      assertEquals(0, modifiedLine.getEndPoint().distance(new Point2D.Double(10, 40)), Configuration.EPSILON);
    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testRemoveCollinearPointsWithoutCollinearPoints() throws Exception {
    try {
      PolylineData line = new PolylineData();
      line.addPoint(new Point2D.Double(10, 20));
      line.addPoint(new Point2D.Double(10, 30));
      line.addPoint(new Point2D.Double(20, 40));
      line.addPoint(new Point2D.Double(10, 40));
      PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
      assertEquals(4, modifiedLine.getPoints().size());
    } catch (Exception e) {
      throw e;
    }
  }

}
