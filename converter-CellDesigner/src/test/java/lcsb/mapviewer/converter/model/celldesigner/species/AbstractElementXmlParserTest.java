package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;

public class AbstractElementXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger();

  AbstractElementXmlParser<?, ?> parser = new ProteinXmlParser(null);
  
  @Test
  public void testEncode() {
    String encoded = parser.encodeName("\n");
    assertTrue(encoded.contains("_br_"));
  }

}
