package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class RnaTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(RnaTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerRna());
  }

  @Test
  public void testConstructor1() {
    CellDesignerRna rna = new CellDesignerRna(new CellDesignerSpecies<Rna>());
    assertNotNull(rna);
  }

  @Test
  public void testGetters() {
    CellDesignerRna rna = new CellDesignerRna(new CellDesignerSpecies<Rna>());
    List<CellDesignerModificationResidue> regions = new ArrayList<>();

    rna.setRegions(regions);

    assertEquals(regions, rna.getRegions());
  }

  @Test
  public void testCopy() {
    CellDesignerRna rna = new CellDesignerRna().copy();
    assertNotNull(rna);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerRna rna = Mockito.spy(CellDesignerRna.class);
    rna.copy();
  }

  @Test
  public void testAddRnaRegion() {
    CellDesignerRna original = new CellDesignerRna();
    CellDesignerModificationResidue region = new CellDesignerModificationResidue("id1");
    original.addRegion(region);

    CellDesignerModificationResidue region2 = new CellDesignerModificationResidue("id1");
    region2.setName("nam");
    original.addRegion(region2);

    assertEquals(1, original.getRegions().size());

    assertEquals("nam", original.getRegions().get(0).getName());

    CellDesignerModificationResidue region3 = new CellDesignerModificationResidue("id2");
    region3.setName("nam");
    original.addRegion(region3);

    assertEquals(2, original.getRegions().size());
  }

  @Test
  public void testUpdate() {
    CellDesignerRna original = new CellDesignerRna();
    CellDesignerModificationResidue region2 = new CellDesignerModificationResidue("id1");
    region2.setName("nam");
    original.addRegion(region2);
    CellDesignerModificationResidue region3 = new CellDesignerModificationResidue("id2");
    region3.setName("nam");
    original.addRegion(region3);

    CellDesignerRna copy = new CellDesignerRna(original);
    copy.addRegion(new CellDesignerModificationResidue());
    copy.getRegions().get(0).setState(ModificationState.ACETYLATED);

    original.update(copy);

    boolean acetylatedFound = false;
    for (CellDesignerModificationResidue region : copy.getRegions()) {
      if (ModificationState.ACETYLATED.equals(region.getState())) {
        acetylatedFound = true;
      }
    }
    assertTrue(acetylatedFound);
    assertEquals(3, original.getRegions().size());
    assertEquals(3, copy.getRegions().size());

    original.update(new CellDesignerGenericProtein());
  }

}
