package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class ProteinXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(ProteinXmlParserTest.class.getName());

  ProteinXmlParser proteinParser;
  String testProteinFile = "testFiles/xmlNodeTestExamples/protein.xml";

  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    proteinParser = new ProteinXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    String xmlString = readFile(testProteinFile);
    Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
    CellDesignerProtein<?> protein = result.getRight();
    assertEquals("pr23", result.getLeft());
    assertEquals("SDHA", protein.getName());
    assertTrue(protein instanceof CellDesignerGenericProtein);
    assertEquals(1, protein.getModificationResidues().size());
    assertEquals("rs1", protein.getModificationResidues().get(0).getIdModificationResidue());
    assertEquals("S176 bla bla", protein.getModificationResidues().get(0).getName());
    assertEquals("Difference to big", 3.141592653589793, protein.getModificationResidues().get(0).getAngle(), 1e-6);
    assertEquals("none", protein.getModificationResidues().get(0).getSide());
    assertTrue(protein.getNotes().contains("UniProtKB	P31040	SDHA		GO:0005749	GO_REF:0000024"));
  }

  @Test
  public void testToXml() throws Exception {
    String xmlString = readFile(testProteinFile);

    Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
    CellDesignerProtein<?> protein = result.getRight();
    String transformedXml = proteinParser.toXml(protein.createModelElement("id"));
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("celldesigner:protein", root.item(0).getNodeName());

    elements = new CellDesignerElementCollection();
    proteinParser = new ProteinXmlParser(elements);
    Protein proteinWithLayout = protein.createModelElement("id");
    proteinWithLayout.setWidth(10);
    proteinWithLayout.setHeight(10);
    protein.updateModelElementAfterLayoutAdded(proteinWithLayout);
    Pair<String, CellDesignerProtein<?>> result2 = proteinParser
        .parseXmlElement(proteinParser.toXml(proteinWithLayout));

    CellDesignerProtein<?> protein2 = result2.getRight();
    assertEquals(protein.getName(), protein2.getName());
    assertEquals(protein.getClass(), protein2.getClass());
    assertEquals(protein.getModificationResidues().size(), protein2.getModificationResidues().size());
    assertEquals(protein.getModificationResidues().get(0).getIdModificationResidue(),
        protein2.getModificationResidues().get(0).getIdModificationResidue());
    assertEquals(protein.getModificationResidues().get(0).getName(),
        protein2.getModificationResidues().get(0).getName());
    assertEquals("Difference to big", protein.getModificationResidues().get(0).getAngle(),
        protein2.getModificationResidues().get(0).getAngle(), 1e-6);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml() throws Exception {
    String xmlString = readFile("testFiles/invalid/protein.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml2() throws Exception {
    String xmlString = readFile("testFiles/invalid/protein2.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml3() throws Exception {
    String xmlString = readFile("testFiles/invalid/protein3.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml4() throws Exception {
    String xmlString = readFile("testFiles/invalid/protein4.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidProteinToXml() throws Exception {
    proteinParser.toXml(Mockito.mock(Protein.class));
  }

}
