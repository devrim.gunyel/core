package lcsb.mapviewer.converter.model.celldesigner.parameter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class ParameterXmlParserTest extends CellDesignerTestFunctions {

  @Test
  public void testParseBasicData() throws InvalidXmlSchemaException, IOException {
    SbmlUnit volume = new SbmlUnit("volume");
    Model model = new ModelFullIndexed(null);
    model.addUnit(volume);
    ParameterXmlParser parser = new ParameterXmlParser(model);
    SbmlParameter unit = parser
        .parseParameter(super.getXmlDocumentFromFile("testFiles/parameter/simple.xml").getFirstChild());

    assertNotNull(unit);
    assertEquals(unit.getParameterId(), "param_id");
    assertEquals(unit.getName(), "Parameter name");
    assertEquals(unit.getValue(), 12.0, Configuration.EPSILON);
    assertEquals(unit.getUnits(), volume);
  }

}
