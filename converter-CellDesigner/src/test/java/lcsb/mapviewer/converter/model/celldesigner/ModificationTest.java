package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;

public class ModificationTest extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(ModificationTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testBindingRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/protein_with_binding_region.xml");
    Protein protein = model.getElementByElementId("sa1");
    assertEquals(1, protein.getModificationResidues().size());
    ModificationResidue residue = protein.getModificationResidues().get(0);

    assertTrue(residue instanceof BindingRegion);
    BindingRegion bindingRegion = (BindingRegion) residue;
    assertEquals(bindingRegion.getPosition().getX(), protein.getX(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() < bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_regulatory_region.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof RegulatoryRegion);
    RegulatoryRegion bindingRegion = (RegulatoryRegion) residue;
    assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testGeneCodingRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_coding_region.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof CodingRegion);
    CodingRegion bindingRegion = (CodingRegion) residue;
    assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testGeneModificationSite() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_modification_site.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof ModificationSite);
    ModificationSite bindingRegion = (ModificationSite) residue;
    assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryTranscriptionSiteRight() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_right.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());
    ModificationResidue residue = gene.getModificationResidues().get(0);

    assertTrue(residue instanceof TranscriptionSite);
    TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
    assertEquals(transcriptionSite.getPosition().getY(), gene.getY(), Configuration.EPSILON);
    assertEquals("RIGHT", transcriptionSite.getDirection());
    assertTrue(transcriptionSite.getActive());

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryTranscriptionSiteLeft() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_left.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());
    ModificationResidue residue = gene.getModificationResidues().get(0);

    assertTrue(residue instanceof TranscriptionSite);
    TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
    assertEquals(transcriptionSite.getPosition().getY(), gene.getY(), Configuration.EPSILON);
    assertEquals("LEFT", transcriptionSite.getDirection());
    assertFalse(transcriptionSite.getActive());

    testXmlSerialization(model);
  }

  @Test
  public void testProtinWithResidues() throws Exception {
    Model model = getModelForFile("testFiles/modifications/protein_with_residues.xml");
    Protein protein = (Protein) model.getElementByElementId("sa1");
    assertEquals(14, protein.getModificationResidues().size());

    ModificationResidue residue = protein.getModificationResidues().get(0);

    assertTrue(residue instanceof Residue);

    testXmlSerialization(model);
  }

  @Test
  public void testRnaWithRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/rna_with_region.xml");
    Rna rna = model.getElementByElementId("sa1");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof CodingRegion);

    rna = model.getElementByElementId("sa2");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof ProteinBindingDomain);

    rna = model.getElementByElementId("sa3");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof ModificationSite);
  }

  @Test
  public void testAntisenseRnaWithRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/antisense_rna_with_region.xml");
    AntisenseRna rna = model.getElementByElementId("sa1");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof CodingRegion);

    rna = model.getElementByElementId("sa2");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof ProteinBindingDomain);

    rna = model.getElementByElementId("sa3");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof ModificationSite);

    rna = model.getElementByElementId("sa4");
    assertEquals(1, rna.getRegions().size());
    assertTrue(rna.getRegions().get(0) instanceof ModificationSite);
    ModificationSite modificationSite = (ModificationSite) rna.getRegions().get(0);
    assertEquals(ModificationState.PHOSPHORYLATED, modificationSite.getState());
  }

  @Test
  public void testPhosporylatedProteinToXml() throws Exception {
    Model model = getModelForFile("testFiles/problematic/phosphorylated_protein.xml");
    testXmlSerialization(model);
  }

}
