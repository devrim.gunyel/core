package lcsb.mapviewer.converter.model.celldesigner.types;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ModifierTypeTest.class,
    ModifierTypeUtilsTest.class,
    OperatorTypeTest.class,
    OperatorTypeUtilsTest.class,
})
public class AllTypesTests {

}
