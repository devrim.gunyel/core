package lcsb.mapviewer.converter.model.celldesigner.geometry;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.AllHelperTests;

@RunWith(Suite.class)
@SuiteClasses({ AbstractCellDesignerAliasConverterTest.class,
    AllHelperTests.class,
    AntisenseRnaCellDesignerAliasConverterTest.class,
    CellDesignerAliasConverterTest.class,
    ComplexConverterTest.class,
    DegradedCellDesignerAliasConverterTest.class,
    GeneCellDesignerAliasConverterTest.class,
    IonCellDesignerAliasConverterTest.class,
    ProteinConverterTest.class,
    ProteinCellDesignerAliasConverterTest.class,
    ReactionCellDesignerConverterTest.class,
    RnaCellDesignerAliasConverterTest.class,
    SimpleMoleculeCellDesignerAliasConverterTest.class,
    UnknownCellDesignerAliasConverterTest.class,
})
public class AllGeometryTests {

}
