package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.geom.Point2D;
import java.lang.reflect.Field;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ModifierTypeUtilsTest extends CellDesignerTestFunctions {

  ModifierTypeUtils utils = new ModifierTypeUtils();

  Modifier invalidModifier = Mockito.mock(Modifier.class, Mockito.CALLS_REAL_METHODS);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetInvalidModifierTypeForClazz() {
    ModifierType type = utils.getModifierTypeForClazz(invalidModifier.getClass());
    assertNull(type);
  }

  @Test
  public void testGetInvalidStringTypeByModifier() {
    String type = utils.getStringTypeByModifier(invalidModifier);
    assertNull(type);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier() {
    utils.getTargetLineIndexByModifier(invalidModifier);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier2() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Reactant());
    utils.getTargetLineIndexByModifier(operator);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier3() {
    NodeOperator operator = new AndOperator();
    operator.addInput(invalidModifier);
    utils.getTargetLineIndexByModifier(operator);
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType() {
    Reaction reaction = createDummyReaction();
    Point2D point = utils.getAnchorPointOnReactionRect(reaction, "0,0");
    assertNotNull(point);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetAnchorPointOnReactionRectByInvalidType2() {
    Reaction reaction = createDummyReaction();
    utils.getAnchorPointOnReactionRect(reaction, "0");
  }

  private Reaction createDummyReaction() {
    Reaction reaction = new Reaction();
    Reactant reactant = new Reactant();
    Product product = new Product();
    reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)));
    product.setLine(new PolylineData(new Point2D.Double(12, 0), new Point2D.Double(13, 0)));
    reaction.addReactant(reactant);
    reaction.addProduct(product);
    reaction.setLine(new PolylineData(new Point2D.Double(1, 0), new Point2D.Double(12, 0)));
    return reaction;
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetAnchorPointOnReactionRectByInvalidType3() {
    Reaction reaction = createDummyReaction();
    utils.getAnchorPointOnReactionRect(reaction, "10,10");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier() {
    utils.updateLineEndPoint(invalidModifier);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier2() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Product());
    operator.addInput(invalidModifier);
    utils.updateLineEndPoint(operator);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier3() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Product());
    operator.addInput(new Product());
    utils.updateLineEndPoint(operator);
  }

  @Test(expected = InvalidStateException.class)
  public void testCreateInvalidModifierForStringType2() throws Exception {
    // artificial implementation of Modifier that is invalid
    class InvalidModifier extends Modifier {
      private static final long serialVersionUID = 1L;

      @SuppressWarnings("unused")
      public InvalidModifier() {
        throw new NotImplementedException();
      }

      @SuppressWarnings("unused")
      public InvalidModifier(Species alias, CellDesignerElement<?> element) {
        throw new NotImplementedException();
      }
    }

    // mopdify one of the elements of OperatorType so it will have invalid
    // implementation
    ModifierType typeToModify = ModifierType.CATALYSIS;
    Class<? extends Modifier> clazz = typeToModify.getClazz();

    try {
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, InvalidModifier.class);

      // and check if we catch properly information about problematic
      // implementation
      utils.createModifierForStringType(typeToModify.getStringName(), new GenericProtein("id"));
    } finally {
      // restore correct values for the modified type (if not then other test
      // might fail...)
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, clazz);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateModifierForStringType2() throws Exception {
    utils.createModifierForStringType("unjkType", null);
  }

}
