package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class CellDesignerAnchorTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    for (CellDesignerAnchor value : CellDesignerAnchor.values()) {
      assertNotNull(value.getName());
    }
  }

}
