package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class CompartmentAliasXmlParserTest extends CellDesignerTestFunctions {
  static Logger logger = LogManager.getLogger(CompartmentAliasXmlParser.class);

  CompartmentAliasXmlParser parser;
  Model model;
  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);
    elements = new CellDesignerElementCollection();
    parser = new CompartmentAliasXmlParser(elements, model);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testToXml() throws Exception {
    Compartment compartment = createCompartment();
    String xmlString = parser.toXml(compartment);
    assertNotNull(xmlString);

    elements.addElement(new CellDesignerCompartment(elements.getElementId(compartment)));

    Compartment alias2 = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());

    assertEquals(compartment.getElementId(), alias2.getElementId());
    assertNotNull(compartment.getName());
    assertEquals(compartment.getFontSize(), alias2.getFontSize(), 1e-6);
    assertEquals(compartment.getHeight(), alias2.getHeight(), 1e-6);
    assertEquals(compartment.getWidth(), alias2.getWidth(), 1e-6);
    assertEquals(compartment.getX(), alias2.getX(), 1e-6);
    assertEquals(compartment.getY(), alias2.getY(), 1e-6);
    assertEquals(compartment.getNamePoint().getX(), alias2.getNamePoint().getX(), 1e-6);
    assertEquals(compartment.getNamePoint().getY(), alias2.getNamePoint().getY(), 1e-6);
  }

  private Compartment createCompartment() {
    Compartment compartment = new SquareCompartment("comp_id");
    compartment.setName("name");
    compartment.setX(13);
    compartment.setY(14);
    compartment.setWidth(100);
    compartment.setHeight(120);
    compartment.setNamePoint(new Point2D.Double(0, 1));

    return compartment;
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias() throws Exception {
    Model model = new ModelFullIndexed(null);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias2() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias3() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias2.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias4() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias3.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test
  public void testParseXmlAliasNode() throws Exception {
    elements.addElement(new CellDesignerCompartment("c1"));

    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
    Compartment alias = (Compartment) parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());
    assertNotNull(alias);
    assertEquals("ca1", alias.getElementId());
    assertNotNull(alias.getName());
    assertTrue(alias instanceof SquareCompartment);
    assertEquals(139.0, alias.getX(), 1e-6);
    assertEquals(55.0, alias.getY(), 1e-6);
    assertEquals(522.0, alias.getWidth(), 1e-6);
    assertEquals(392.5, alias.getNamePoint().getX(), 1e-6);
    assertEquals(196.5, alias.getNamePoint().getY(), 1e-6);
    assertEquals(0xffcccc00, alias.getFillColor().getRGB());
  }

  @Test
  public void testGetters() {
    parser.setCommonParser(null);

    assertNull(parser.getCommonParser());
  }

  @Test
  public void testLeftToXml() throws Exception {
    Compartment alias = new LeftSquareCompartment("id");
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testRightToXml() throws Exception {
    Compartment alias = new RightSquareCompartment("id");
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testTopToXml() throws Exception {
    Compartment alias = new TopSquareCompartment("id");
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testBotttomToXml() throws Exception {
    Compartment alias = new BottomSquareCompartment("id");
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test(expected = NotImplementedException.class)
  public void testUnknownToXml() throws Exception {
    Compartment alias = Mockito.mock(BottomSquareCompartment.class);
    Mockito.when(alias.getElementId()).thenReturn("some id");
    parser.toXml(alias);
  }

}
