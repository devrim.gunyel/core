package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;

public class CompartmentParserTests extends CellDesignerTestFunctions {
  Logger logger = LogManager.getLogger(CompartmentParserTests.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseCompartmensBottom() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_compartment.xml");
    Compartment c = model.getElementByElementId("ca4");
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);
    assertEquals(0, c.getX(), Configuration.EPSILON);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensTop() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_compartment.xml");

    Compartment c = model.getElementByElementId("ca3");
    assertEquals(0, c.getX(), Configuration.EPSILON);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() < model.getHeight());
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/right_compartment.xml");
    Compartment c = model.getElementByElementId("ca2");
    assertTrue(c.getX() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(0, c.getX(), Configuration.EPSILON);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertTrue(c.getWidth() < model.getWidth());
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentBottomRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_right_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertTrue(c.getX() > 0);
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentBottomLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(c.getX(), 0, Configuration.EPSILON);
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentTopLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(c.getX(), 0, Configuration.EPSILON);
    assertEquals(c.getY(), 0, Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentTopRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_right_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertTrue(c.getX() > 0);
    assertEquals(c.getY(), 0, Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testOldCellDesignerCompartment() throws Exception {
    Model model = getModelForFile("testFiles/compartment/old_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");

    assertEquals(1, model.getElements().size());
  }

}
