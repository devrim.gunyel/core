package lcsb.mapviewer.converter.model.celldesigner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.model.celldesigner.alias.AllAliasTests;
import lcsb.mapviewer.converter.model.celldesigner.annotation.AllAnnotationTests;
import lcsb.mapviewer.converter.model.celldesigner.compartment.AllCompartmentTests;
import lcsb.mapviewer.converter.model.celldesigner.function.AllFunctionTests;
import lcsb.mapviewer.converter.model.celldesigner.geometry.AllGeometryTests;
import lcsb.mapviewer.converter.model.celldesigner.parameter.ParameterXmlParserTest;
import lcsb.mapviewer.converter.model.celldesigner.reaction.AllReactionTests;
import lcsb.mapviewer.converter.model.celldesigner.species.AllSpeciesTests;
import lcsb.mapviewer.converter.model.celldesigner.structure.AllStructureTests;
import lcsb.mapviewer.converter.model.celldesigner.types.AllTypesTests;
import lcsb.mapviewer.converter.model.celldesigner.unit.UnitXmlParserTest;

@RunWith(Suite.class)
@SuiteClasses({
    AllAnnotationTests.class,
    AllAliasTests.class,
    AllCompartmentTests.class,
    AllFunctionTests.class,
    AllGeometryTests.class,
    AllReactionTests.class,
    AllSpeciesTests.class,
    AllStructureTests.class,
    AllTypesTests.class,
    CellDesignerElementCollectionTest.class,
    CellDesignerXmlParserTest.class,
    ComplexParserTests.class,
    InvalidGroupExceptionTest.class,
    LayerXmlParserTest.class,
    NestedComplexParsingTests.class,
    ModificationTest.class,
    ParameterXmlParserTest.class,
    ReconDataInCellDesignerXmlParserTest.class,
    SbmlValidationTests.class,
    UnitXmlParserTest.class,
})
public class AllCellDesignerTests {

}
