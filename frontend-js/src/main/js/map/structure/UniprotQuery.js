var $ = require('jquery');

function ajaxQuery(url, type) {
  if (type === undefined) type = "GET";

  return $.ajax({
    type: type,
    url: url
  });
}

/**
 *
 * @param accession
 * @return {Promise}
 */
var getUniprotSequence = function (accession) {
  return ajaxQuery('https://www.uniprot.org/uniprot/?query=accession%3A' + accession + '&columns=sequence&format=tab').then(function (data) {
    return data.split(/\r?\n/)[1];
  })
};

module.exports = getUniprotSequence;