function MolArtVariant() {
    
    this.posFrom       = undefined;
    this.posTo          = undefined;
    this.aaFrom           = undefined;
    this.aaTo             = undefined;
    this.varType        = undefined;
    this.description    = undefined;

}

module.exports = MolArtVariant;