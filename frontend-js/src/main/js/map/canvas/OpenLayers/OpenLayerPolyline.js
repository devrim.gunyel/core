"use strict";

var Polyline = require('../Polyline');
var Bounds = require('../Bounds');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');
var Functions = require('../../../Functions');

function OpenLayerPolyline(options) {
  Polyline.call(this, options);

  var self = this;

  self.registerListenerType("onHide");

  if (options.strokeColor === undefined) {
    options.strokeColor = "#000000";
  }
  if (options.strokeWeight === undefined) {
    options.strokeWeight = 1;
  }
  this._options = options;

  var style = self.createStyle(options);

  var line = this.createLine(options.path);

  var feature = new ol.Feature({
    geometry: line,
    id: options.id
  });

  this.setOpenLayersPolyline(feature);
  feature.setStyle(new ol.style.Style({}));
  options.source.addFeature(feature);

  feature.__openLayerPolyline = this;

  this._style = style;
}

OpenLayerPolyline.prototype = Object.create(Polyline.prototype);
OpenLayerPolyline.prototype.constructor = OpenLayerPolyline;

OpenLayerPolyline.prototype.createStyle = function (options) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: Functions.colorToRgbaString(options.strokeColor, options.strokeOpacity),
      width: options.strokeWeight
    })
  });
};

OpenLayerPolyline.prototype.createLine = function (points) {
  var self = this;

  var result = [];
  for (var i = 0; i < points.length; i++) {
    result.push(self.getMap().fromPointToProjection(points[i]));
  }

  return new ol.geom.Polygon([result]);
};

OpenLayerPolyline.prototype.setOpenLayersPolyline = function (polyline) {
  this._polyline = polyline;
};

OpenLayerPolyline.prototype.getOpenLayersPolyline = function () {
  return this._polyline;
};

OpenLayerPolyline.prototype.show = function () {
  return this.getOpenLayersPolyline().setStyle(this._style);
};

OpenLayerPolyline.prototype.hide = function () {
  this.getOpenLayersPolyline().setStyle(new ol.style.Style({}));
  return this.callListeners("onHide");
};

OpenLayerPolyline.prototype.isShown = function () {
  return this.getOpenLayersPolyline().getStyle().getStroke() !== null;
};

OpenLayerPolyline.prototype.getBounds = function () {
  var self = this;
  var extent = self.getOpenLayersPolyline().getGeometry().getExtent();

  var projection1 = [extent[0], extent[1]];
  var p1 = self.getMap().fromProjectionToPoint(projection1);
  var projection2 = [extent[2], extent[3]];
  var p2 = self.getMap().fromProjectionToPoint(projection2);
  return new Bounds(p1, p2);
};

/**
 *
 * @param options
 */
OpenLayerPolyline.prototype.setOptions = function (options) {
  var self = this;
  self._options = Object.assign(self._options, options);
  var style = self.createStyle(self._options);
  if (self.isShown()) {
    self.getOpenLayersPolyline().setStyle(style);
  }
  self._style = style;
};


module.exports = OpenLayerPolyline;
