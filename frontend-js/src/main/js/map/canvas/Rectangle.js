"use strict";

var MapCanvas = require('./MapCanvas');
var Point = require('./Point');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * @param {MapCanvas} options.map
 *
 * @constructor
 * @augments {ObjectWithListeners}
 */

function Rectangle(options) {
  ObjectWithListeners.call(this);
  this.registerListenerType("click");
  this.setMap(options.map);
}

Rectangle.prototype = Object.create(ObjectWithListeners.prototype);
Rectangle.prototype.constructor = ObjectWithListeners;

Rectangle.prototype.show = function () {
  throw new Error("Not implemented");
};

Rectangle.prototype.hide = function () {
  throw new Error("Not implemented");
};

/**
 * @returns {Bounds}
 */
Rectangle.prototype.getBounds = function () {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Bounds} bounds
 */
Rectangle.prototype.setBounds = function (bounds) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 */
Rectangle.prototype.setOptions = function (options) {
  throw new Error("Not implemented");
};

/**
 *
 * @param {MapCanvas} map
 */
Rectangle.prototype.setMap = function (map) {
  if (!(map instanceof MapCanvas) && map !== null) {
    throw new Error("Map must be of MapCanvas class");
  }
  this._map = map;
};

/**
 *
 * @returns {MapCanvas}
 */
Rectangle.prototype.getMap = function () {
  return this._map;
};

/**
 *
 * @param {Point} point
 * @param {Bounds} oldBounds
 * @param {Bounds} newBounds
 *
 * @returns {Point}
 * @protected
 */
Rectangle.prototype._transformCoordinates = function (point, oldBounds, newBounds) {
  var x = newBounds.getTopLeft().x + (point.x - oldBounds.getTopLeft().x) / (oldBounds.getRightBottom().x - oldBounds.getTopLeft().x) * (newBounds.getRightBottom().x - newBounds.getTopLeft().x);
  var y = newBounds.getTopLeft().y + (point.y - oldBounds.getTopLeft().y) / (oldBounds.getRightBottom().y - oldBounds.getTopLeft().y) * (newBounds.getRightBottom().y - newBounds.getTopLeft().y);
  return new Point(x, y);
};

module.exports = Rectangle;
