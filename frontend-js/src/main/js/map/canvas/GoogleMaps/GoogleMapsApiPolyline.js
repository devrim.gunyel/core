"use strict";

var Polyline = require('../Polyline');
var Bounds = require('../Bounds');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

/**
 * @param {MapCanvas} options.map
 * @param {Point[]} options.path
 * @param {string} options.strokeColor
 * @param {number} options.strokeWeight
 * @param {number} options.strokeOpacity
 *
 * @constructor
 * @augments {Polyline}
 */
function GoogleMapsApiPolyline(options) {
  Polyline.call(this, options);

  var path = [];
  for (var i = 0; i < options.path.length; i++) {
    path.push(this.getMap().fromPointToLatLng(options.path[i]));
  }

  this.setGooglePolyline(new google.maps.Polyline({
    strokeColor: options.strokeColor,
    strokeOpacity: options.strokeOpacity,
    strokeWeight: options.strokeWeight,
    path: path
  }));
  google.maps.event.addListener(this.getGooglePolyline(), "click", function(){
    return self.callListeners("click");
  });
}

GoogleMapsApiPolyline.prototype = Object.create(Polyline.prototype);
GoogleMapsApiPolyline.prototype.constructor = GoogleMapsApiPolyline;

/**
 *
 * @param {google.maps.Polyline} polyline
 */
GoogleMapsApiPolyline.prototype.setGooglePolyline = function (polyline) {
  this._polyline = polyline;
};

/**
 *
 * @returns {google.maps.Polyline}
 */
GoogleMapsApiPolyline.prototype.getGooglePolyline = function () {
  return this._polyline;
};

GoogleMapsApiPolyline.prototype.show = function () {
  var googlePolyline = this.getGooglePolyline();
  if (googlePolyline.getMap() !== undefined && googlePolyline.getMap() !== null) {
    logger.warn("Polyline is already shown");
  }
  else {
    googlePolyline.setMap(this.getMap().getGoogleMap());
  }
};

GoogleMapsApiPolyline.prototype.hide = function () {
  if (!this.isShown()) {
    logger.warn("Polyline is already invisible");
  } else {
    this.getGooglePolyline().setMap(null);
  }
};

/**
 *
 * @returns {boolean}
 */
GoogleMapsApiPolyline.prototype.isShown = function () {
  var googlePolyline = this.getGooglePolyline();
  return googlePolyline.getMap() !== null && googlePolyline.getMap() !== undefined;
};

/**
 *
 * @returns {Bounds}
 */
GoogleMapsApiPolyline.prototype.getBounds = function () {
  var result = new Bounds();
  for (var i = 0; i < this.getGooglePolyline().getPath().getLength(); i++) {
    var latLng = this.getGooglePolyline().getPath().getAt(i);
    var point = this.getMap().fromLatLngToPoint(latLng);
    result.extend(point);
  }
  return result;
};

/**
 *
 * @param {Object} options
 */
GoogleMapsApiPolyline.prototype.setOptions = function (options) {
  this.getGooglePolyline().setOptions(options);
};

module.exports = GoogleMapsApiPolyline;
