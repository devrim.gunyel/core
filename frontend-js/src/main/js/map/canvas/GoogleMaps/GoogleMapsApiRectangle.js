"use strict";

var Bounds = require('../Bounds');
var Point = require('../Point');
var Rectangle = require('../Rectangle');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

/**
 *
 * @param {GoogleMapsApiCanvas} options.map
 * @param {Bounds} options.bounds
 * @param {string} [options.fillColor]
 * @param {{color: string, amount: number}[]} [options.fillGradient]
 * @param {number} options.fillOpacity
 * @param {string} options.id
 * @param {string} options.strokeColor
 * @param {number} options.strokeOpacity
 * @param {number} options.strokeWeight
 *
 * @constructor
 * @extends Rectangle
 */
function GoogleMapsApiRectangle(options) {
  Rectangle.call(this, options);

  var self = this;
  self._rectangles = [];


  var i;
  var bounds;
  if (options.fillGradient !== undefined) {
    var totalAmount = 0;
    for (i = 0; i < options.fillGradient.length; i++) {
      totalAmount += options.fillGradient[i].amount;
    }
    var y = options.bounds.getTopLeft().y;
    for (i = 0; i < options.fillGradient.length; i++) {
      var x1 = options.bounds.getTopLeft().x;
      var x2 = options.bounds.getRightBottom().x;
      var y1 = y;
      var ratio = options.fillGradient[i].amount / totalAmount;

      var y2 = y1 + ratio * (options.bounds.getRightBottom().y - options.bounds.getTopLeft().y);
      y = y2;
      bounds = new google.maps.LatLngBounds();
      bounds.extend(self.getMap().fromPointToLatLng(new Point(x1, y1)));
      bounds.extend(self.getMap().fromPointToLatLng(new Point(x2, y2)));
      self.addGoogleRectangle(new google.maps.Rectangle({
        bounds: bounds,
        fillOpacity: options.fillOpacity,
        strokeWeight: 0.0,
        fillColor: options.fillGradient[i].color
      }));
    }
    bounds = new google.maps.LatLngBounds();
    bounds.extend(self.getMap().fromPointToLatLng(options.bounds.getTopLeft()));
    bounds.extend(self.getMap().fromPointToLatLng(options.bounds.getRightBottom()));
    self.addGoogleRectangle(new google.maps.Rectangle({
      bounds: bounds,
      fillOpacity: 0.0,
      id: options.id,
      strokeWeight: options.strokeWeight,
      strokeColor: options.strokeColor,
      strokeOpacity: options.strokeOpacity
    }));
  } else {
    bounds = new google.maps.LatLngBounds();
    bounds.extend(self.getMap().fromPointToLatLng(options.bounds.getTopLeft()));
    bounds.extend(self.getMap().fromPointToLatLng(options.bounds.getRightBottom()));
    self.addGoogleRectangle(new google.maps.Rectangle({
      bounds: bounds,
      fillOpacity: options.fillOpacity,
      id: options.id,
      strokeWeight: options.strokeWeight,
      fillColor: options.fillColor,
      strokeColor: options.strokeColor,
      strokeOpacity: options.strokeOpacity
    }));
  }

  var rectangles = self.getGoogleRectangles();
  for (i = 0; i < rectangles.length; i++) {
    google.maps.event.addListener(rectangles[i], "click", function () {
      return self.callListeners("click");
    });
  }
}

GoogleMapsApiRectangle.prototype = Object.create(Rectangle.prototype);
GoogleMapsApiRectangle.prototype.constructor = GoogleMapsApiRectangle;

/**
 *
 * @param {google.maps.Rectangle} rectangle
 */
GoogleMapsApiRectangle.prototype.addGoogleRectangle = function (rectangle) {
  this._rectangles.push(rectangle);
};

/**
 *
 * @returns {google.maps.Rectangle[]}
 */
GoogleMapsApiRectangle.prototype.getGoogleRectangles = function () {
  return this._rectangles;
};

GoogleMapsApiRectangle.prototype.show = function () {
  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    rectangle.setMap(this.getMap().getGoogleMap());
  }
};

GoogleMapsApiRectangle.prototype.hide = function () {
  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    rectangle.setMap(null);
  }
};

/**
 *
 * @returns {boolean}
 */
GoogleMapsApiRectangle.prototype.isShown = function () {
  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    if (rectangle.getMap() !== null && rectangle.getMap() !== undefined) {
      return true;
    }
  }
  return false;
};

/**
 *
 * @param newBounds {Bounds}
 */
GoogleMapsApiRectangle.prototype.setBounds = function (newBounds) {
  var oldBounds = this.getBounds();

  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    var rectangleLatLngBounds = rectangle.getBounds();
    var currentBounds = new Bounds();
    currentBounds.extend(this.getMap().fromLatLngToPoint(rectangleLatLngBounds.getSouthWest()));
    currentBounds.extend(this.getMap().fromLatLngToPoint(rectangleLatLngBounds.getNorthEast()));

    var latLngBounds = new google.maps.LatLngBounds();

    var topLeft = this._transformCoordinates(currentBounds.getTopLeft(), oldBounds, newBounds);
    var rightBottom = this._transformCoordinates(currentBounds.getRightBottom(), oldBounds, newBounds);

    latLngBounds.extend(this.getMap().fromPointToLatLng(topLeft));
    latLngBounds.extend(this.getMap().fromPointToLatLng(rightBottom));
    rectangle.setBounds(latLngBounds);
  }
};

/**
 *
 * @returns {Bounds}
 */
GoogleMapsApiRectangle.prototype.getBounds = function () {
  var result = new Bounds();

  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    var latLngBounds = rectangle.getBounds();
    result.extend(this.getMap().fromLatLngToPoint(latLngBounds.getSouthWest()));
    result.extend(this.getMap().fromLatLngToPoint(latLngBounds.getNorthEast()));
  }
  return result;
};

/**
 *
 * @param {Object} options
 */
GoogleMapsApiRectangle.prototype.setOptions = function (options) {
  var rectangles = this.getGoogleRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    rectangle.setOptions(options);
  }
};


module.exports = GoogleMapsApiRectangle;
