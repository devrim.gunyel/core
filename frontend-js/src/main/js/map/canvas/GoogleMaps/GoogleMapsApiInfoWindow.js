"use strict";

var InfoWindow = require('../InfoWindow');
var GoogleMapsApiMarker = require('./GoogleMapsApiMarker');

var logger = require('../../../logger');

/**
 *
 * @param {Point} options.position
 * @param {GoogleMapsApiMarker} [options.marker]
 * @param {string} options.id
 * @param {GoogleMapsApiCanvas} options.map
 *
 * @constructor
 * @extends InfoWindow
 */
function GoogleMapsApiInfoWindow(options) {
  InfoWindow.call(this, options);

  if (options.marker !== undefined) {
    if (options.marker instanceof GoogleMapsApiMarker) {
      this.setMarker(options.marker);
    } else {
      throw new Error("Invalid argument: " + options.marker);
    }
  }
  this.setGoogleInfoWindow(new google.maps.InfoWindow({
    position: this.getMap().fromPointToLatLng(options.position),
    id: options.id
  }));
}

GoogleMapsApiInfoWindow.prototype = Object.create(InfoWindow.prototype);
GoogleMapsApiInfoWindow.prototype.constructor = GoogleMapsApiInfoWindow;

GoogleMapsApiInfoWindow.prototype.open = function () {
  var googleMarker = this.getGoogleInfoWindow();
  if (googleMarker.getMap() !== undefined && googleMarker.getMap() !== null) {
    logger.warn("InfoWindow is already opened");
  }
  else {
    googleMarker.open(this.getMap().getGoogleMap(), this._marker);
  }
};

GoogleMapsApiInfoWindow.prototype.hide = function () {
  if (!this.isOpened()) {
    logger.warn("InfoWindow is already invisible");
  } else {
    this.getGoogleInfoWindow().setMap(null);
  }
};
GoogleMapsApiInfoWindow.prototype.isOpened = function () {
  var googleMarker = this.getGoogleInfoWindow();
  return googleMarker.getMap() !== null && googleMarker.getMap() !== undefined;
};

/**
 *
 * @param {google.maps.InfoWindow} infoWindow
 */
GoogleMapsApiInfoWindow.prototype.setGoogleInfoWindow = function (infoWindow) {
  this._infoWindow = infoWindow;
};

GoogleMapsApiInfoWindow.prototype.getGoogleInfoWindow = function () {
  return this._infoWindow;
};

/**
 *
 * @param {string|HTMLElement} content
 */
GoogleMapsApiInfoWindow.prototype.setContent = function (content) {
  this.getGoogleInfoWindow().setContent(content);
};

/**
 *
 * @param {GoogleMapsApiMarker} marker
 */
GoogleMapsApiInfoWindow.prototype.setMarker = function (marker) {
  this._marker = marker.getGoogleMarker();
};

GoogleMapsApiInfoWindow.prototype.destroy = function () {
  //do nothing
};


module.exports = GoogleMapsApiInfoWindow;
