"use strict";

var TargettingStructure = require("./TargettingStructure");

/**
 * @typedef {TargettingStructureOptions} MiRnaOptions
 */

/**
 *
 * @param {MiRnaOptions|MiRna} javaObject
 * @constructor
 * @extends TargettingStructure
 */
function MiRna(javaObject) {
  TargettingStructure.call(this, javaObject);
}

MiRna.prototype = Object.create(TargettingStructure.prototype);
MiRna.prototype.constructor = MiRna;

module.exports = MiRna;
