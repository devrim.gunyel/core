"use strict";

/**
 * @typedef {Object} ModifierOptions
 * @property {number} aliasId
 * @property {string} stoichiometry
 */

/**
 *
 * @param {ModifierOptions} javaObject
 * @constructor
 */
function Modifier(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

/**
 *
 * @returns {number}
 */
Modifier.prototype.getAlias = function () {
  return this._alias;
};

/**
 *
 * @param {number} alias
 */
Modifier.prototype.setAlias = function (alias) {
  this._alias = alias;
};

/**
 *
 * @returns {string}
 */
Modifier.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};

/**
 *
 * @param {string} stoichiometry
 */
Modifier.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};

module.exports = Modifier;
