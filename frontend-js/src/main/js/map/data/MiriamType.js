"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');
/**
 * @typedef {Object} MiriamTypeOptions
 * @property {string[]} uris
 * @property {string} homepage
 * @property {string} commonName
 * @property {string} registryIdentifier
 */


/**
 *
 * @param {MiriamTypeOptions} data
 * @param {string} name
 * @constructor
 */
function MiriamType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setUris(data.uris);
  self.setHomepage(data.homepage);
  self.setName(name);
  self.setCommonName(data.commonName);
  self.setRegistryIdentifier(data.registryIdentifier);
}

MiriamType.prototype = Object.create(ObjectWithListeners.prototype);
MiriamType.prototype.constructor = MiriamType;

/**
 *
 * @param {string[]} uris
 */
MiriamType.prototype.setUris = function(uris) {
  this._uris = uris;
};

/**
 *
 * @returns {string[]}
 */
MiriamType.prototype.getUris = function() {
  return this._uris;
};

/**
 *
 * @param {string} homepage
 */
MiriamType.prototype.setHomepage = function(homepage) {
  this._homepage = homepage;
};

/**
 *
 * @returns {string}
 */
MiriamType.prototype.getHomepage = function() {
  return this._homepage;
};

/**
 *
 * @param {string} name
 */
MiriamType.prototype.setName = function(name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
MiriamType.prototype.getName = function() {
  return this._name;
};

/**
 *
 * @param {string} commonName
 */
MiriamType.prototype.setCommonName = function(commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
MiriamType.prototype.getCommonName = function() {
  return this._commonName;
};

/**
 *
 * @param {string} registryIdentifier
 */
MiriamType.prototype.setRegistryIdentifier = function(registryIdentifier) {
  this._registryIdentifier = registryIdentifier;
};

/**
 *
 * @returns {string}
 */
MiriamType.prototype.getRegistryIdentifier = function() {
  return this._registryIdentifier;
};

module.exports = MiriamType;
