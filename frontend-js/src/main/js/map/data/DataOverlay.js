"use strict";

var Promise = require("bluebird");

var IdentifiedElement = require('./IdentifiedElement');
var LayoutAlias = require('./LayoutAlias');
var LayoutReaction = require('./LayoutReaction');
var logger = require('../../logger');

/**
 * Class representing data in a specific overlay.
 *
 * @param {number|Object} overlayId
 * @param {string} [name]
 * @constructor
 */
function DataOverlay(overlayId, name) {
  this.setInitialized(false);

  if (name === undefined) {
    // from json structure
    var object = overlayId;
    this.setId(object.idObject);
    this.setOrder(object.order);
    this.setName(object.name);
    this.setImagesDirectory(object.images);
    this.setDescription(object.description);
    this.setCreator(object.creator);
    this.setDeprecatedColumns(object.deprecatedColumns);
    this.setContent(object.content);
    this.setFilename(object.filename);
    this.setPublicOverlay(object.publicOverlay);
    this.setDefaultOverlay(object.defaultOverlay);
    this.setGoogleLicenseConsent(object.googleLicenseConsent);
    this.setInputDataAvailable(object.inputDataAvailable);
    this.setType(object.type);
    if (!this.getInputDataAvailable()) {
      this.setInitialized(true);
    }
  } else {
    // default two param call
    this.setId(overlayId);
    this.setName(name);
  }
  this.aliases = [];
  this.aliasById = [];
  this.reactions = [];
}

/**
 * Adds alias to the {@link DataOverlay}
 *
 * @param {LayoutAlias} overlayAlias
 *          information about alias in the overlay
 */
DataOverlay.prototype.addAlias = function (overlayAlias) {
  this.aliases.push(overlayAlias);
  this.aliasById[overlayAlias.getId()] = overlayAlias;
};

/**
 * Adds reaction to the {@link DataOverlay}
 *
 * @param {LayoutReaction} overlayReaction
 *          information about reaction in the overlay
 */
DataOverlay.prototype.addReaction = function (overlayReaction) {
  this.reactions.push(overlayReaction);
};

/**
 *
 * @returns {number}
 */
DataOverlay.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param {number|string} id
 */
DataOverlay.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
DataOverlay.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getCreator = function () {
  return this._creator;
};

/**
 *
 * @param {string} creator
 */
DataOverlay.prototype.setCreator = function (creator) {
  this._creator = creator;
};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.getInputDataAvailable = function () {
  return this._inputDataAvailable;
};

/**
 *
 * @param {string|boolean} inputDataAvailable
 */
DataOverlay.prototype.setInputDataAvailable = function (inputDataAvailable) {
  var value = inputDataAvailable;
  if (inputDataAvailable === undefined) {
    value = false;
  } else if (inputDataAvailable === "true") {
    value = true;
  } else if (inputDataAvailable === "false") {
    value = false;
  } else if (typeof (inputDataAvailable) !== "boolean") {
    logger.warn("inputDataAvailable should be boolean");
    value = false;
  }
  this._inputDataAvailable = value;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getName = function () {
  return this.name;
};

/**
 *
 * @param {string} name
 */
DataOverlay.prototype.setName = function (name) {
  this.name = name;
};

/**
 *
 * @param {number} modelId
 * @returns {string|null}
 */
DataOverlay.prototype.getImagesDirectory = function (modelId) {
  for (var i = 0; i < this._imagesDirectory.length; i++) {
    if (parseInt(this._imagesDirectory[i].modelId) === modelId) {
      return this._imagesDirectory[i].path;
    }
  }
  return null;
};

/**
 *
 * @param {Object.<number,string>} imagesDirectory
 */
DataOverlay.prototype.setImagesDirectory = function (imagesDirectory) {
  this._imagesDirectory = imagesDirectory;
};

/**
 *
 * @param {LayoutAlias} overlayAlias
 */
DataOverlay.prototype.updateAlias = function (overlayAlias) {
  if (this.aliasById[overlayAlias.getId()] === undefined) {
    logger.warn("Cannot update alias, it doesn't exist. Alias: " + overlayAlias.getId());
  } else {
    this.aliasById[overlayAlias.getId()].update(overlayAlias);
  }
};

/**
 *
 * @param {number} id
 * @returns {LayoutAlias}
 */
DataOverlay.prototype.getAliasById = function (id) {
  return this.aliasById[id];
};

/**
 *
 * @param {number} id
 * @returns {Promise<LayoutAlias>}
 */
DataOverlay.prototype.getFullAliasById = function (id) {
  var self = this;
  var alias = self.getAliasById(id);
  if (alias !== undefined) {
    if (alias.getType() === LayoutAlias.LIGHT) {
      return ServerConnector.getFullOverlayElement({
        element: new IdentifiedElement(alias),
        overlay: self
      }).then(function (data) {
        self.updateAlias(data);
        return alias;
      });
    }
  }
  return Promise.resolve(alias);
};

/**
 *
 * @param {boolean} value
 */
DataOverlay.prototype.setInitialized = function (value) {
  this._initialized = value;
};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.isInitialized = function () {
  return this._initialized;
};

/**
 *
 * @returns {LayoutAlias[]}
 */
DataOverlay.prototype.getAliases = function () {
  return this.aliases;
};

/**
 *
 * @returns {LayoutReaction[]}
 */
DataOverlay.prototype.getReactions = function () {
  return this.reactions;
};

/**
 *
 * @returns {Promise}
 */
DataOverlay.prototype.init = function () {
  var self = this;
  if (this.isInitialized()) {
    return Promise.resolve();
  }
  return ServerConnector.getOverlayElements(self.getId()).then(function (data) {
    for (var i = 0; i < data.length; i++) {
      if (data[i] instanceof LayoutAlias) {
        self.addAlias(data[i]);
      } else if (data[i] instanceof LayoutReaction) {
        self.addReaction(data[i]);
      } else {
        return Promise.reject("Unknown element type: " + typeof (data[i]));
      }

    }
    self.setInitialized(true);
  });

};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.getPublicOverlay = function () {
  return this._publicOverlay;
};

/**
 *
 * @param {boolean} publicOverlay
 */
DataOverlay.prototype.setPublicOverlay = function (publicOverlay) {
  this._publicOverlay = publicOverlay;
};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.isDefaultOverlay = function () {
  return this._defaultOverlay;
};

/**
 *
 * @param {boolean} defaultOverlay
 */
DataOverlay.prototype.setDefaultOverlay = function (defaultOverlay) {
  this._defaultOverlay = defaultOverlay;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getContent = function () {
  return this._content;
};

/**
 *
 * @param {string} content
 */
DataOverlay.prototype.setContent = function (content) {
  this._content = content;
};

/**
 *
 * @returns {string[]}
 */
DataOverlay.prototype.getDeprecatedColumns = function () {
  return this._deprecatedColumns;
};

/**
 *
 * @param {string[]} deprecatedColumns
 */
DataOverlay.prototype.setDeprecatedColumns = function (deprecatedColumns) {
  this._deprecatedColumns = deprecatedColumns;
};

/**
 *
 * @returns {number}
 */
DataOverlay.prototype.getOrder = function () {
  return this._order;
};

/**
 *
 * @param {number} order
 */
DataOverlay.prototype.setOrder = function (order) {
  this._order = order;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getFilename = function () {
  return this._filename;
};

/**
 *
 * @param {string} filename
 */
DataOverlay.prototype.setFilename = function (filename) {
  this._filename = filename;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} type
 */
DataOverlay.prototype.setType = function (type) {
  this._type = type;
};

/**
 *
 * @param {boolean} value
 */
DataOverlay.prototype.setGoogleLicenseConsent = function (value) {
  this._googleLicenseConsent = value;
};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.isGoogleLicenseConsent = function () {
  return this._googleLicenseConsent;
};

/**
 *
 * @param {DataOverlay} overlay
 */
DataOverlay.prototype.update = function (overlay) {
  this.setPublicOverlay(overlay.getPublicOverlay());
  this.setOrder(overlay.getOrder());
  this.setCreator(overlay.getCreator());
  this.setName(overlay.getName());
  this.setDescription(overlay.getDescription());
};


module.exports = DataOverlay;
