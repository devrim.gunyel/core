"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Annotator = require('./Annotator');

/**
 *
 * @param [javaObject]
 * @constructor
 */
function UserPreferences(javaObject) {
  if (javaObject !== undefined) {
    this.setProjectUpload(javaObject["project-upload"]);
    this.setElementAnnotators(javaObject["element-annotators"]);
    this.setElementRequiredAnnotations(javaObject["element-required-annotations"]);
    this.setElementValidAnnotations(javaObject["element-valid-annotations"]);
    this.setGuiPreferences(javaObject["gui-preferences"]);
  } else {
    this._projectUpload = {};
    /**
     * @private
     */
    this._elementAnnotators = {};
    this._elementRequiredAnnotations = {};
    this._elementValidAnnotations = {};
    this._guiPreferences = {};
  }
}

/**
 *
 * @param {UserPreferences} userPreferences
 */
UserPreferences.prototype.update = function (userPreferences) {
  var updateDict = function (originalDict, newDict) {
    for (var key in newDict) {
      if (newDict.hasOwnProperty(key)) {
        originalDict[key] = newDict[key];
      }
    }
  };

  updateDict(this._projectUpload, userPreferences.getProjectUpload());
  updateDict(this._elementAnnotators, userPreferences._elementAnnotators);
  updateDict(this._elementValidAnnotations, userPreferences._elementValidAnnotations);
  updateDict(this._elementRequiredAnnotations, userPreferences._elementRequiredAnnotations);
  updateDict(this._guiPreferences, userPreferences.getGuiPreferences());
};

/**
 *
 * @param {Object} projectUpload
 * @param {boolean} projectUpload.auto-resize
 * @param {boolean} projectUpload.validate-miriam
 * @param {boolean} projectUpload.validate-miriam
 * @param {boolean} projectUpload.cache-data
 * @param {boolean} projectUpload.sbgn
 */
UserPreferences.prototype.setProjectUpload = function (projectUpload) {
  this._projectUpload = {
    autoResize: projectUpload["auto-resize"],
    validateMiriam: projectUpload["validate-miriam"],
    annotateModel: projectUpload["annotate-model"],
    cacheData: projectUpload["cache-data"],
    semanticZoomingContainsMultipleOverlays: projectUpload["semantic-zooming-contains-multiple-overlays"],
    sbgn: projectUpload["sbgn"]
  };
};

UserPreferences.prototype.getGuiPreferences = function () {
  return this._guiPreferences;
};
UserPreferences.prototype.setGuiPreference = function (key, value) {
  this._guiPreferences[key] = value;
};
UserPreferences.prototype.getGuiPreference = function (key, defaultValue) {
  var result = this._guiPreferences[key];
  if (result === undefined) {
    result = defaultValue;
  }
  return result;
};
UserPreferences.prototype.setGuiPreferences = function (guiPreferences) {
  this._guiPreferences = guiPreferences;
};
UserPreferences.prototype.getProjectUpload = function () {
  return this._projectUpload;
};

/**
 *
 * @param {Object<string, Annotator>} elementAnnotators
 */
UserPreferences.prototype.setElementAnnotators = function (elementAnnotators) {
  this._elementAnnotators = {};
  for (var key in elementAnnotators) {
    if (elementAnnotators.hasOwnProperty(key)) {
      var annotators = elementAnnotators[key];
      this._elementAnnotators[key] = [];
      for (var i = 0; i < annotators.length; i++) {
        this._elementAnnotators[key].push(new Annotator(annotators[i]));
      }
    }
  }
};

/**
 *
 * @param {string} className
 * @returns {Annotator[]}
 */
UserPreferences.prototype.getElementAnnotators = function (className) {
  var result = this._elementAnnotators[className];
  if (result === undefined) {
    this._elementAnnotators[className] = [];
    result = this._elementAnnotators[className];
  }
  return result;
};

/**
 *
 * @param {Object<string, Object>} elementRequiredAnnotations
 */
UserPreferences.prototype.setElementRequiredAnnotations = function (elementRequiredAnnotations) {
  this._elementRequiredAnnotations = {};
  for (var key in elementRequiredAnnotations) {
    if (elementRequiredAnnotations.hasOwnProperty(key)) {
      var data = elementRequiredAnnotations[key];
      this._elementRequiredAnnotations[key] = {
        requiredAtLeastOnce: data["require-at-least-one"],
        list: data["annotation-list"]
      };
    }
  }
};

/**
 *
 * @param {string} className
 * @returns {{list:string[], requiredAtLeastOnce:boolean}}
 */
UserPreferences.prototype.getElementRequiredAnnotations = function (className) {
  var result = this._elementRequiredAnnotations[className];
  if (result === undefined) {
    result = {list: [], requiredAtLeastOnce: false};
  }
  return result;
};

/**
 *
 * @param {Object<string,string[]>}elementValidAnnotations
 */
UserPreferences.prototype.setElementValidAnnotations = function (elementValidAnnotations) {
  this._elementValidAnnotations = elementValidAnnotations;
};

/**
 *
 * @param {string} className
 * @returns {string[]}
 */
UserPreferences.prototype.getElementValidAnnotations = function (className) {
  var result = this._elementValidAnnotations[className];
  if (result === undefined) {
    result = [];
  }
  return result;
};

UserPreferences.prototype.toExport = function () {
  var requiredAnnotations = {};
  for (var key in this._elementRequiredAnnotations) {
    if (this._elementRequiredAnnotations.hasOwnProperty(key)) {
      var data = this._elementRequiredAnnotations[key];
      requiredAnnotations[key] = {
        "require-at-least-one": data.requiredAtLeastOnce,
        "annotation-list": data.list
      };
    }
  }

  var elementAnnotators = {};

  for (key in this._elementAnnotators) {
    if (this._elementAnnotators.hasOwnProperty(key)) {
      elementAnnotators[key] = [];
      for (var i = 0; i < this._elementAnnotators[key].length; i++) {
        elementAnnotators[key].push(this._elementAnnotators[key][i].toExport());
      }
    }
  }

  return {
    "project-upload": {
      "auto-resize": this._projectUpload.autoResize,
      "validate-miriam": this._projectUpload.validateMiriam,
      "annotate-model": this._projectUpload.annotateModel,
      "cache-data": this._projectUpload.cacheData,
      "sbgn": this._projectUpload.sbgn,
      "semantic-zooming-contains-multiple-overlays": this._projectUpload.semanticZoomingContainsMultipleOverlays
    },
    "element-annotators": elementAnnotators,
    "element-valid-annotations": this._elementValidAnnotations,
    "element-required-annotations": requiredAnnotations,
    "gui-preferences": this.getGuiPreferences()
  };
};

module.exports = UserPreferences;
