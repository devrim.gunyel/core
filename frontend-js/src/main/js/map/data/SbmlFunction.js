"use strict";

/**
 *
 * @param {Object} jsonObject
 * @param {string} jsonObject.functionId
 * @param {boolean} jsonObject.global
 * @param {string} jsonObject.name
 * @param {number} jsonObject.id
 * @param {string[]} jsonObject.arguments
 * @param {string} jsonObject.definition
 * @param {string} jsonObject.mathMlPresentation
 * @constructor
 */
function SbmlFunction(jsonObject) {
  var self = this;
  self.setFunctionId(jsonObject.functionId);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setArguments(jsonObject["arguments"]);
  self.setDefinition(jsonObject.definition);
  self.setMathMlPresentation(jsonObject.mathMlPresentation);
}

/**
 *
 * @param {string} functionId
 */
SbmlFunction.prototype.setFunctionId = function (functionId) {
  this._functionId = functionId;
};

/**
 *
 * @returns {string}
 */
SbmlFunction.prototype.getFunctionId = function () {
  return this._functionId;
};

/**
 *
 * @param {string} definition
 */
SbmlFunction.prototype.setDefinition = function (definition) {
  this._definition = definition;
};

/**
 *
 * @returns {string}
 */
SbmlFunction.prototype.getDefinition = function () {
  return this._definition;
};

/**
 *
 * @param {string} mathMlPresentation
 */
SbmlFunction.prototype.setMathMlPresentation = function (mathMlPresentation) {
  this._mathMlPresentation = mathMlPresentation;
};

/**
 *
 * @returns {string}
 */
SbmlFunction.prototype.getMathMlPresentation = function () {
  return this._mathMlPresentation;
};

/**
 *
 * @param {string} name
 */
SbmlFunction.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
SbmlFunction.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string[]} args
 */
SbmlFunction.prototype.setArguments = function (args) {
  this._arguments = args;
};

/**
 *
 * @returns {string[]}
 */
SbmlFunction.prototype.getArguments = function () {
  return this._arguments;
};

/**
 *
 * @param {number} id
 */
SbmlFunction.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
SbmlFunction.prototype.getId = function () {
  return this._id;
};

module.exports = SbmlFunction;
