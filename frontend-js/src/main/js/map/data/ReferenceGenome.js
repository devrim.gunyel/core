"use strict";

var Annotation = require('./Annotation');
var ReferenceGenomeGeneMapping = require('./ReferenceGenomeGeneMapping');

/**
 * @typedef {Object} ReferenceGenomeOptions
 * @property {number} idObject
 * @property {string} type
 * @property {string} version
 * @property {string} localUrl
 * @property {string} sourceUrl
 * @property {AnnotationOptions} organism
 * @property {number} downloadProgress
 * @property {GeneMappingOptions[]} geneMapping
 */

/**
 *
 * @param {ReferenceGenomeOptions} [javaObject]
 * @constructor
 */
function ReferenceGenome(javaObject) {
  if (javaObject !== undefined && javaObject !== null) {
    this.setId(javaObject.idObject);
    this.setType(javaObject.type);
    this.setVersion(javaObject.version);
    this.setLocalUrl(javaObject.localUrl);
    this.setSourceUrl(javaObject.sourceUrl);
    if (javaObject.localUrl !== undefined) {
      this.setUrl(javaObject.localUrl);
    } else {
      this.setUrl(javaObject.sourceUrl);
    }
    this.setOrganism(new Annotation(javaObject.organism));
    this.setDownloadProgress(javaObject.downloadProgress);
    this._geneMapping = [];
    if (javaObject.geneMapping !== undefined) {
      for (var i = 0; i < javaObject.geneMapping.length; i++) {
        this._geneMapping.push(new ReferenceGenomeGeneMapping(javaObject.geneMapping[i]));
      }
    }
  } else {
    this._geneMapping = [];
  }
}

/**
 *
 * @param {string} type
 */
ReferenceGenome.prototype.setType = function (type) {
  this._type = type;
};

/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {number} id
 */
ReferenceGenome.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
ReferenceGenome.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @returns {number}
 */
ReferenceGenome.prototype.getDownloadProgress = function () {
  return this._downloadProgress;
};

/**
 *
 * @param {number} downloadProgress
 */
ReferenceGenome.prototype.setDownloadProgress = function (downloadProgress) {
  this._downloadProgress = downloadProgress;
};


/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getDownloadProgressStatus = function () {
  var progress = this.getDownloadProgress();
  if (progress === 100) {
    if (this.getLocalUrl() !== undefined) {
      return "READY";
    } else {
      return "ERROR";
    }
  } else {
    if (progress === undefined) {
      return "N/A";
    } else {
      return progress.toFixed(2) + ' %';
    }
  }
};


/**
 *
 * @param {Annotation} organism
 */
ReferenceGenome.prototype.setOrganism = function (organism) {
  this._organism = organism;
};

/**
 *
 * @returns {Annotation}
 */
ReferenceGenome.prototype.getOrganism = function () {
  return this._organism;
};

/**
 *
 * @param {string} url
 */
ReferenceGenome.prototype.setUrl = function (url) {
  this._url = url;
};

/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getUrl = function () {
  return this._url;
};

/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getSourceUrl = function () {
  return this._sourceUrl;
};

/**
 *
 * @param {string} sourceUrl
 */
ReferenceGenome.prototype.setSourceUrl = function (sourceUrl) {
  this._sourceUrl = sourceUrl;
};

/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getLocalUrl = function () {
  return this._localUrl;
};

/**
 *
 * @param {string} localUrl
 */
ReferenceGenome.prototype.setLocalUrl = function (localUrl) {
  if (localUrl === null) {
    this._localUrl = undefined;
  } else {
    this._localUrl = localUrl;
  }
};

/**
 *
 * @param {string} version
 */
ReferenceGenome.prototype.setVersion = function (version) {
  this._version = version;
};

/**
 *
 * @returns {string}
 */
ReferenceGenome.prototype.getVersion = function () {
  return this._version;
};

/**
 *
 * @returns {ReferenceGenomeGeneMapping[]}
 */
ReferenceGenome.prototype.getGeneMappings = function () {
  return this._geneMapping;
};

module.exports = ReferenceGenome;
