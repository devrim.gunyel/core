"use strict";

var GeneVariant = require('./GeneVariant');

/**
 * Class representing alias visualized in a overlay.
 *
 * @param {number} javaObject.idObject
 * @param {number} [javaObject.value]
 * @param {{rgb:number}} [javaObject.color]
 * @param {number} javaObject.modelId
 * @param {string} [javaObject.description]
 * @param {string} [javaObject.type=LayoutAlias.LIGHT]
 * @param {Array} [javaObject.geneVariations]
 */
function LayoutAlias(javaObject) {
  this.setId(javaObject.idObject);
  this.setValue(javaObject.value);
  this.setColor(javaObject.color);
  this.setModelId(javaObject.modelId);
  this.setDescription(javaObject.description);
  if (javaObject.type === undefined) {
    this.setType(LayoutAlias.LIGHT);
  } else if (javaObject.type === LayoutAlias.GENETIC_VARIANT) {
    this.setType(LayoutAlias.GENETIC_VARIANT);
  } else if (javaObject.type === LayoutAlias.GENERIC) {
    this.setType(LayoutAlias.GENERIC);
  } else {
    throw new Error("Unknown type: " + javaObject.type);
  }

  this.setGeneVariants([]);
  if (javaObject.geneVariations !== undefined) {
    for (var i = 0; i < javaObject.geneVariations.length; i++) {
      this.addGeneVariant(new GeneVariant(javaObject.geneVariations[i]));
    }
  }
}

LayoutAlias.LIGHT = "LIGHT";
LayoutAlias.GENETIC_VARIANT = "GENETIC_VARIANT";
LayoutAlias.GENERIC = "GENERIC";

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param id
 */
LayoutAlias.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getModelId = function () {
  return this._modelId;
};

/**
 *
 * @param modelId
 */
LayoutAlias.prototype.setModelId = function (modelId) {
  this._modelId = parseInt(modelId);
};

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getValue = function () {
  return this.value;
};

/**
 *
 * @returns {{rgb:number}}
 */
LayoutAlias.prototype.getColor = function () {
  return this.color;
};

/**
 *
 * @returns {string}
 */
LayoutAlias.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @returns {GeneVariant[]}
 */
LayoutAlias.prototype.getGeneVariants = function () {
  return this._geneVariants;
};

/**
 *
 * @param {number} newValue
 */
LayoutAlias.prototype.setValue = function (newValue) {
  this.value = newValue;
};

/**
 *
 * @param {{rgb:number}} newColor
 */
LayoutAlias.prototype.setColor = function (newColor) {
  this.color = newColor;
};

/**
 *
 * @param {string} newType
 */
LayoutAlias.prototype.setType = function (newType) {
  this._type = newType;
};

/**
 *
 * @param {GeneVariant[]} newGeneVariants
 */
LayoutAlias.prototype.setGeneVariants = function (newGeneVariants) {
  this._geneVariants = newGeneVariants;
};

/**
 *
 * @param {LayoutAlias} alias
 */
LayoutAlias.prototype.update = function (alias) {
  if (!(alias instanceof LayoutAlias)) {
    throw new Error("Unknown parameter type: " + alias);
  }

  this.setValue(alias.getValue());
  this.setColor(alias.getColor());
  this.setGeneVariants(alias.getGeneVariants());
  this.setType(alias.getType());
  this.setDescription(alias.getDescription());
};

/**
 *
 * @param {GeneVariant} geneVariant
 */
LayoutAlias.prototype.addGeneVariant = function (geneVariant) {
  this._geneVariants.push(geneVariant);
};

/**
 *
 * @returns {string}
 */
LayoutAlias.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
LayoutAlias.prototype.setDescription = function (description) {
  this._description = description;
};

module.exports = LayoutAlias;
