"use strict";


/**
 * @typedef {Object} GeneMappingOptions
 * @property {number} idObject
 * @property {number} downloadProgress
 * @property {string} name
 * @property {string} localUrl
 * @property {string} sourceUrl
 */

/**
 *
 * @param {GeneMappingOptions} javaObject
 * @constructor
 */
function ReferenceGenomeGeneMapping(javaObject) {
  this.setName(javaObject.name);
  if (javaObject.localUrl !== undefined) {
    this.setUrl(javaObject.localUrl);
  } else {
    this.setUrl(javaObject.sourceUrl);
  }
  this.setLocalUrl(javaObject.localUrl);
  this.setSourceUrl(javaObject.sourceUrl);
  this.setProgress(javaObject.downloadProgress);
  this.setId(javaObject.idObject);
}

/**
 *
 * @param {string} name
 */
ReferenceGenomeGeneMapping.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
ReferenceGenomeGeneMapping.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} url
 */
ReferenceGenomeGeneMapping.prototype.setUrl = function (url) {
  this._url = url;
};

/**
 *
 * @returns {string}
 */
ReferenceGenomeGeneMapping.prototype.getUrl = function () {
  return this._url;
};

/**
 *
 * @param {string} url
 */
ReferenceGenomeGeneMapping.prototype.setLocalUrl = function (url) {
  this._localUrl = url;
};

/**
 *
 * @returns {string}
 */
ReferenceGenomeGeneMapping.prototype.getLocalUrl = function () {
  return this._localUrl;
};

/**
 *
 * @returns {string}
 */
ReferenceGenomeGeneMapping.prototype.getSourceUrl = function () {
  return this._sourceUrl;
};

/**
 *
 * @returns {string}
 */
ReferenceGenomeGeneMapping.prototype.getProgressStatus = function () {
  var progress = this.getProgress();
  if (progress === 100) {
    if (this.getLocalUrl() !== undefined) {
      return "READY";
    } else {
      return "ERROR";
    }
  } else {
    if (progress === undefined) {
      return "N/A";
    } else {
      return progress.toFixed(2) + ' %';
    }
  }
};

/**
 *
 * @param {string} url
 */
ReferenceGenomeGeneMapping.prototype.setSourceUrl = function (url) {
  this._sourceUrl = url;
};

/**
 *
 * @param {number} progress
 */
ReferenceGenomeGeneMapping.prototype.setProgress = function (progress) {
  this._progress = progress;
};

/**
 *
 * @returns {number}
 */
ReferenceGenomeGeneMapping.prototype.getProgress = function () {
  return this._progress;
};

/**
 *
 * @param {number} id
 */
ReferenceGenomeGeneMapping.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
ReferenceGenomeGeneMapping.prototype.getId = function () {
  return this._id;
};

module.exports = ReferenceGenomeGeneMapping;
