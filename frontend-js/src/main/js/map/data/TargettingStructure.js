"use strict";

/* exported logger */

var Target = require("./Target");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 * @typedef {Object} TargettingStructureOptions
 * @property {string} name
 * @property {number} id
 * @property {TargetOptions[]} targets
 */

/**
 *
 * @param {TargettingStructureOptions|TargettingStructure} javaObject
 * @constructor
 */
function TargettingStructure(javaObject) {
  if (javaObject !== undefined) {
    if (javaObject instanceof TargettingStructure) {
      this.setName(javaObject.getName());
      this.setId(javaObject.getId());
      this.setTargets(javaObject.getTargets());
    } else {
      this.setName(javaObject.name);
      this.setId(javaObject.id);
      this.setTargets(javaObject.targets);
    }
  }
}

/**
 *
 * @param {string} name
 */
TargettingStructure.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
TargettingStructure.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {number} id
 */
TargettingStructure.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
TargettingStructure.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {TargetOptions[]|Target[]} targets
 */
TargettingStructure.prototype.setTargets = function (targets) {
  this._targets = [];
  for (var i = 0; i < targets.length; i++) {
    this._targets.push(new Target(targets[i]));
  }
};

/**
 *
 * @returns {Target[]}
 */
TargettingStructure.prototype.getTargets = function () {
  return this._targets;
};

/**
 *
 * @param {IdentifiedElement[]} targetIdentifiedElement
 * @returns {Target[]}
 */
TargettingStructure.prototype.getTargetsForIdentifiedElement = function (targetIdentifiedElement) {
  var result = [];
  this.getTargets().forEach(function (target) {
    var targetOk = false;
    target.getTargetElements().forEach(function (identifiedElement) {
      if (targetIdentifiedElement.equals(identifiedElement)) {
        targetOk = true;
      }
    });
    if (targetOk) {
      result.push(target);
    }
  });
  return result;
};

module.exports = TargettingStructure;
