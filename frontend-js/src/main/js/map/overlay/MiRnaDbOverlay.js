"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractTargettingDbOverlay = require('./AbstractTargettingDbOverlay');

var ServerConnector = require('../../ServerConnector');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractTargettingDbOverlay
 */
function MiRnaDbOverlay(params) {
  params.iconType= "target";
  params.iconColorStart = 2;
  // call super constructor
  AbstractTargettingDbOverlay.call(this, params);
  
}

MiRnaDbOverlay.prototype = Object.create(AbstractTargettingDbOverlay.prototype);
MiRnaDbOverlay.prototype.constructor = MiRnaDbOverlay;

/**
 *
 * @param {IdentifiedElement} param
 * @returns {Promise<string[]>}
 */
MiRnaDbOverlay.prototype.getNamesByTargetFromServer = function(param) {
  return ServerConnector.getMiRnaNamesByTarget(param);
};

/**
 *
 * @param {string} param.query
 * @returns {Promise<Drug[]>}
 */
MiRnaDbOverlay.prototype.getElementsByQueryFromServer = function(param) {
  return ServerConnector.getMiRnasByQuery(param);
};

module.exports = MiRnaDbOverlay;
