"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

/**
 *
 * @param {string} message
 * @constructor
 * @extends Error
 */
function ObjectExistsError(message) {
  this.message = message;
  this.stack = (new Error(message)).stack;
}

ObjectExistsError.prototype = Object.create(Error.prototype);
ObjectExistsError.prototype.constructor = ObjectExistsError;

module.exports = ObjectExistsError;
