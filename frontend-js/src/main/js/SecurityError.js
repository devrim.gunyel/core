"use strict";

/* exported logger */

var logger = require('./logger');

/**
 *
 * @param {string} message
 * @constructor
 * @extends Error
 */
function SecurityError(message) {
  this.message = message;
  this.stack = (new Error(message)).stack;
}

SecurityError.prototype = Object.create(Error.prototype);
SecurityError.prototype.constructor = SecurityError;

module.exports = SecurityError;
