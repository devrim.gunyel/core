"use strict";

/* exported logger */

var logger = require('./logger');

var SecurityError = require("./SecurityError");

/**
 *
 * @param {string} message
 * @constructor
 * @extends SecurityError
 */
function InvalidCredentialsError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

InvalidCredentialsError.prototype = Object.create(SecurityError.prototype);
InvalidCredentialsError.prototype.constructor = InvalidCredentialsError;

module.exports = InvalidCredentialsError;
