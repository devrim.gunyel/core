"use strict";

/* exported logger */

var logger = require('./logger');

/**
 *
 * @param {string} message
 * @constructor
 * @extends Error
 */
function ValidationError(message) {
  this.message = message;
  this.stack = (new Error(message)).stack;
}

ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

module.exports = ValidationError;
