"use strict";

/* exported logger */
var $ = require('jquery');
require('datatables.net')();
require('datatables.net-rowreorder')();

var AddOverlayDialog = require('../AddOverlayDialog');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');
var ValidationError = require('../../ValidationError');

var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

var Promise = require('bluebird');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} [params.configuration]
 * @param {Project} [params.project]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function OverlayPanel(params) {
  params["panelName"] = "overlays";
  params["scrollable"] = true;
  params["helpTip"] = "<p>Overlays tab allows to display or generate custom coloring of elements and interactions in the map.</p>"
    + "<p>General overlays are overlays accessible for every user viewing the content.</p>"
    + "<p>Custom overlays are user-provided overlays, this menu becomes available upon login (see below).</p>";
  if (params.project === undefined) {
    params.project = params.customMap.getProject();
  }
  Panel.call(this, params);

  //overflow is defined in minerva-overlay-panel, so remove the one that is already there
  $(params.element).css("overflow", "");
  $(params.element).addClass("minerva-overlay-panel");

  var self = this;

  self._createOverlayPanelGui();

  var addButton = $("[name='addOverlay']", self.getElement());
  var refreshButton = $("[name='refreshOverlays']", self.getElement());

  addButton.on("click", function () {
    return self.openAddOverlayDialog();
  });

  refreshButton.on("click", function () {
    return self.refresh();
  });

  var titleElement = this.getControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE);
  self.setCustomOverlaysMessage(titleElement.innerHTML);

  $(self.getElement()).on("click", "[name='overlayToggle']", function () {
    var thisCheckbox = this;
    var overlayId = $(thisCheckbox).attr("data");
    var toggleOverlayPromise;
    if (thisCheckbox.checked) {
      toggleOverlayPromise = self.getMap().openDataOverlay(overlayId);
    } else {
      toggleOverlayPromise = self.getMap().hideDataOverlay(overlayId);
    }
    $(thisCheckbox).prop("disabled", true);
    return toggleOverlayPromise.catch(GuiConnector.alert).finally(function () {
      $(thisCheckbox).prop("disabled", false);
    });
  });
  $(self.getElement()).on("click", "[name='overlayLink']", function () {
    var overlayId = $(this).attr("data");
    return self.getMap().openDataOverlay(overlayId);
  });
  self.getMap().addListener("onBackgroundOverlayChange", function (e) {
    var overlayId = e.arg.toString();
    var buttons = $("[name='overlayLink']", self.getElement());
    for (var i = 0; i < buttons.length; i++) {
      var button = buttons[i];
      if ($(button).attr("data").toString() === overlayId) {
        $(button.parentNode.parentNode).addClass('active').siblings().removeClass('active');
      }
    }
  });
  $(self.getElement()).on("click", "[name='download-overlay']", function () {
    var overlayId = $(this).attr("data");
    return self.getServerConnector().getOverlaySourceDownloadUrl({
      overlayId: overlayId
    }).then(function (url) {
      return self.downloadFile(url);
    }).then(null, GuiConnector.alert);
  });
  $(self.getElement()).on("click", "[name='editButton']", function () {
    var overlayId = $(this).attr("data");
    return self.getProject().getDataOverlayById(overlayId).then(function (overlay) {
      return self.openEditOverlayDialog(overlay);
    });
  });
}

OverlayPanel.prototype = Object.create(Panel.prototype);
OverlayPanel.prototype.constructor = OverlayPanel;

/**
 *
 * @private
 */
OverlayPanel.prototype._createOverlayPanelGui = function () {
  var generalOverlaysDiv = Functions.createElement({
    type: "div",
    name: "generalOverlays",
    className: "searchPanel"
  });
  this.getElement().appendChild(generalOverlaysDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_DIV, generalOverlaysDiv);

  var generalOverlaysTitle = Functions.createElement({
    type: "h5",
    content: "GENERAL OVERLAYS:"
  });
  generalOverlaysDiv.appendChild(generalOverlaysTitle);

  var generalOverlaysTableDiv = Functions.createElement({
    type: "table",
    name: "generalOverlaysTab",
    className: "table table-bordered",
    style: "width:100%"
  });
  generalOverlaysDiv.appendChild(generalOverlaysTableDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_TABLE, generalOverlaysTableDiv);

  var customOverlaysDiv = Functions.createElement({
    type: "div",
    name: "customOverlays",
    className: "searchPanel"
  });
  this.getElement().appendChild(customOverlaysDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_DIV, customOverlaysDiv);

  var customOverlaysTitle = Functions.createElement({
    type: "h5",
    name: "customOverlaysTitle",
    content: "USER-PROVIDED OVERLAYS:"
  });
  customOverlaysDiv.appendChild(customOverlaysTitle);
  this.setControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE, customOverlaysTitle);

  var customOverlaysTableDiv = Functions.createElement({
    type: "table",
    name: "customOverlaysTab",
    className: "table table-bordered",
    style: "width:100%"
  });
  customOverlaysDiv.appendChild(customOverlaysTableDiv);

  var centerTag = Functions.createElement({
    type: "div",
    style: "text-align: center;"
  });
  customOverlaysDiv.appendChild(centerTag);

  var addOverlayButton = Functions.createElement({
    type: "button",
    name: "addOverlay",
    content: "Add overlay",
    style: "margin: 4px;"
  });
  centerTag.appendChild(addOverlayButton);

  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshOverlays",
    content: "<span class=\"ui-icon ui-icon-refresh\"></span>&nbsp;Refresh",
    xss: false
  });
  centerTag.appendChild(refreshButton);
};

/**
 *
 */
OverlayPanel.prototype.clear = function () {
  var table = this.getControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_TABLE);
  while (table.firstChild) {
    table.removeChild(table.firstChild);
  }

  table = $("[name='customOverlaysTab']", this.getElement()).DataTable();
  table.clear().draw();
};

/**
 *
 * @param {boolean} [edit=false]
 * @returns {HTMLElement}
 */
OverlayPanel.prototype.createTableHeader = function (edit) {
  if (edit === undefined) {
    edit = false;
  }
  var result = document.createElement("thead");

  var row = document.createElement("tr");

  var nameTd = document.createElement("th");
  nameTd.innerHTML = "Name";
  row.appendChild(nameTd);

  var viewTd = document.createElement("th");
  viewTd.innerHTML = "View";
  row.appendChild(viewTd);

  var dataTd = document.createElement("th");
  dataTd.innerHTML = "Data";
  row.appendChild(dataTd);

  if (edit) {
    var editTd = document.createElement("th");
    editTd.innerHTML = "Edit";
    row.appendChild(editTd);
  }

  result.appendChild(row);
  return result;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} [checked=false]
 * @param {boolean} [disabled=false]
 * @returns {HTMLElement}
 */
OverlayPanel.prototype.createOverlayRow = function (overlay, checked, disabled) {
  var result = document.createElement("tr");

  if (checked && !overlay.getInputDataAvailable()) {
    result.className = "active";
  }

  var nameTd = Functions.createElement({type: "td", content: overlay.getName(), className: "word_wrap", xss: true});
  result.appendChild(nameTd);

  var viewTd = document.createElement("td");
  if (overlay.getInputDataAvailable()) {
    var checkbox = Functions.createElement({
      type: "input",
      inputType: "checkbox",
      name: "overlayToggle",
      data: overlay.getId()
    });
    checkbox.checked = checked;
    $(checkbox).prop("disabled", disabled);
    if (disabled) {
      var warningDiv = Functions.createElement({
        type: "div",
        content: "<i class='fa fa-exclamation-triangle' style='font-size:18px; padding-right:10px;color:orange'></i>",
        xss: false
      });
      warningDiv.title = "You did not consent to terms of the license of Google Maps Platform. Click the \"Edit\" button to do so.";
      viewTd.appendChild(warningDiv)
    } else {
      viewTd.appendChild(checkbox);
    }
  } else {
    var link = Functions.createElement({
      type: "a",
      href: "#",
      className: "minerva-search-link",
      name: "overlayLink",
      data: overlay.getId(),
      content: "<i class='minerva-search-button'></i>",
      xss: false
    });
    viewTd.appendChild(link);
  }
  result.appendChild(viewTd);

  var dataTd = document.createElement("td");
  if (overlay.getInputDataAvailable()) {
    var button = Functions.createElement({
      type: "button",
      name: "download-overlay",
      data: overlay.getId(),
      content: "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>",
      xss: false
    });
    dataTd.appendChild(button);
  }

  result.appendChild(dataTd);

  if (overlay.getCreator() !== "" && overlay.getCreator() !== undefined) {
    var editTd = document.createElement("td");
    button = Functions.createElement({
      type: "button",
      name: "editButton",
      data: overlay.getId(),
      content: "<span class='ui-icon ui-icon-document'></span>",
      xss: false
    });
    editTd.appendChild(button);
    result.appendChild(editTd);
  }
  result.title = overlay.getDescription();
  return result;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} checked
 * @param {boolean} disabled
 * @returns {Array}
 */
OverlayPanel.prototype.overlayToDataRow = function (overlay, checked, disabled) {
  var result = [];
  result[0] = overlay.getOrder();
  if (overlay.getDeprecatedColumns() !== undefined && overlay.getDeprecatedColumns() !== null && overlay.getDeprecatedColumns().length > 0) {

    result[1] = "<div title='This data overlay uses old deprecated column names: ";
    for (var j = 0; j < overlay.getDeprecatedColumns().length; j++) {
      result[1] += overlay.getDeprecatedColumns()[j] + ", ";
    }
    result[1] += " This format will be not supported in minerva 16 and higher. Please re-upload data set using new format.'>" +
      "<i class='fa fa-exclamation-triangle' style='font-size:18px; padding-right:10px;color:orange'></i>" + overlay.getName() + "</div>"
  } else {
    result[1] = overlay.getName();
  }

  if (overlay.getInputDataAvailable()) {
    if (disabled) {
      result[2] = "<div title='You did not consent to terms of the license of Google Maps Platform. Click the \"Edit\" button to do so.'>" +
        "<i class='fa fa-exclamation-triangle' style='font-size:18px; padding-right:10px;color:orange'></i></div>";
    } else {
      var checkedString = "";
      if (checked) {
        checkedString = " checked ";
      }
      result[2] = "<input type='checkbox' " + checkedString + " data='" + overlay.getId() + "' name='overlayToggle'/>";
    }

    result[3] = "<button data='" + overlay.getId() + "' name='download-overlay'><span class='ui-icon ui-icon-arrowthickstop-1-s'></span></button>";
  } else {
    result[2] = "<a href='#' class='minerva-search-link' data='" + overlay.getId() + "' name='overlayLink'><i class='minerva-search-button'></i></a>";
    result[3] = "";
  }

  if (overlay.getCreator() !== "" && overlay.getCreator() !== undefined) {
    result[4] = "<button data='" + overlay.getId() + "' name='editButton'><span class='ui-icon ui-icon-document'></span></button>";
  } else {
    result[4] = "";
  }
  if (overlay.getDescription() !== "") {
    for (var i = 1; i < result.length; i++) {

      result[i] = "<div class=\"minerva-tooltip\">" + result[i] +
        "<span class=\"minerva-tooltip-text\">" + overlay.getDescription() + "</span>" +
        "</div>";
    }
  }
  return result;
};

/**
 *
 * @param {DataOverlay} overlay
 */
OverlayPanel.prototype.openEditOverlayDialog = function (overlay) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var content = document.createElement("fieldset");
  var nameInput = guiUtils.createInputText(overlay.getName());
  var row = guiUtils.createTableRow([guiUtils.createLabel("Name: "), nameInput]);
  content.appendChild(row);

  var descriptionInput = guiUtils.createTextArea(overlay.getDescription());
  row = guiUtils.createTableRow([guiUtils.createLabel("Description: "), descriptionInput]);
  content.appendChild(row);

  var consentCheckbox = Functions.createElement({
    type: "input",
    name: "overlay-google-consent",
    inputType: "checkbox"
  });

  if (self.getProject().getMapCanvasType() === "GOOGLE_MAPS_API") {
    row = guiUtils.createTableRow([guiUtils.createLabel("I am aware that if this map is displayed using Google Maps API, " +
      "it falls under their license <a href='https://cloud.google.com/maps-platform/terms/' target='_blank'>" +
      "https://cloud.google.com/maps-platform/terms/</a>, to which I agree. I warrant that this dataset contains no " +
      "Protected Health Information (as defined in and subject to HIPAA)."), consentCheckbox]);
    content.appendChild(row);
  }

  var buttons = [{
    text: "SAVE",
    click: function () {
      var windowSelf = this;
      overlay.setName(nameInput.value);
      overlay.setDescription(descriptionInput.value);
      overlay.setGoogleLicenseConsent(consentCheckbox.checked);

      return self.getServerConnector().updateOverlay(overlay).then(function () {
        return self.refresh();
      }).then(function () {
        $(windowSelf).dialog("close");
      }).catch(GuiConnector.alert);
    }
  }, {
    text: "REMOVE",
    click: function () {
      var windowSelf = this;
      return GuiConnector.showConfirmationDialog({
        message: "Do you want to delete overlay: " + overlay.getName() + "?"
      }).then(function (confirmation) {
        if (confirmation) {
          return self.removeOverlay(overlay).then(function () {
            $(windowSelf).dialog("close");
          }).catch(GuiConnector.alert);
        }
      });
    }
  }, {
    text: "CANCEL",
    click: function () {
      $(this).dialog("close");
    }
  }];
  self.openDialog(content, {
    width: "600px",
    id: overlay.getId(),
    buttons: buttons,
    title: "Data overlay: " + overlay.getName(),
    className: "minerva-overlay-dialog"
  });
};

/**
 *
 * @param {boolean} [showDefault=false]
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.refresh = function (showDefault) {
  if (showDefault === undefined) {
    showDefault = false;
  }
  var self = this;
  var user = null;

  var overlayTypes = [];
  var selectedOverlay = [];
  var overlaysFromServer = [];

  return self.getServerConnector().getOverlayTypes().then(function (types) {
    overlayTypes = types;
    return self.getServerConnector().getLoggedUser();
  }).then(function (loggedUser) {
    user = loggedUser;
    return self.getMap().getVisibleDataOverlays();
  }).then(function (visibleDataOverlays) {
    for (var j = 0; j < visibleDataOverlays.length; j++) {
      selectedOverlay[visibleDataOverlays[j].getId()] = true;
    }

    return self.getServerConnector().getOverlays({creator: user.getLogin()});
  }).then(function (userOverlays) {
    overlaysFromServer = userOverlays;
    return self.getServerConnector().getOverlays({publicOverlay: true});
  }).then(function (publicOverlays) {
    overlaysFromServer = overlaysFromServer.concat((publicOverlays));

    self.getProject().addOrUpdateDataOverlays(overlaysFromServer);

    var overlays = self.getProject().getDataOverlays();
    for (var index = 0; index < overlays.length; index++) {
      var remove = true;
      for (var index2 = 0; index2 < overlaysFromServer.length; index2++) {
        if (overlaysFromServer[index2].getId() === overlays[index].getId()) {
          remove = false;
        }
      }
      if (remove) {
        self.getProject().removeDataOverlay(overlays[index]);
      }
    }

    if (!showDefault) {
      if (overlaysFromServer.length === 0) {
        return Promise.reject(new ValidationError("Project doesn't have a background defined. Please re-upload map in admin panel."));
      }
      var id = self.getMap().getBackgroundDataOverlay().getId();
      selectedOverlay[id] = true;
    }

    self.clear();

    var generalOverlays = [];
    var overlay;

    overlays = self.getProject().getDataOverlays();
    var customOverlays = [];
    var defaultOverlay = null;
    for (var i = 0; i < overlays.length; i++) {
      overlay = overlays[i];
      if (overlay.getCreator() === undefined || overlay.getCreator() === "") {
        generalOverlays.push(overlay);
        if (showDefault && overlay.isDefaultOverlay()) {
          defaultOverlay = overlay;
        }
      } else {
        customOverlays.push(overlay);
      }
    }
    if (defaultOverlay !== null) {
      selectedOverlay[defaultOverlay.getId()] = true;
    }

    var table = self.getControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_TABLE);
    table.appendChild(self.createTableHeader());

    var body = document.createElement("tbody");
    table.appendChild(body);

    generalOverlays.sort(function (o1, o2) {
      var val1 = o1.getId();
      var val2 = o2.getId();
      if (o1.getOrder() !== o2.getOrder()) {
        val1 = o1.getOrder();
        val2 = o2.getOrder();
      }
      if (val1 < val2) {
        return -1;
      }
      if (val1 > val2) {
        return 1;
      }
      return 0;
    });
    for (i = 0; i < generalOverlays.length; i++) {
      overlay = generalOverlays[i];
      body.appendChild(self.createOverlayRow(overlay, selectedOverlay[overlay.getId()], false));
    }

    var title = self.getControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE);
    var addButton = $("[name='addOverlay']", self.getElement());
    var refreshButton = $("[name='refreshOverlays']", self.getElement());
    var tableElement = $("[name='customOverlaysTab']", self.getElement());
    if (user.getLogin() === "anonymous") {
      title.innerHTML = 'YOU ARE NOT LOGGED IN. PLEASE, <a href="#">LOG IN</a> '
        + 'TO UPLOAD AND VIEW CUSTOM OVERLAYS<br/><center><button>LOGIN</button></center>';
      var openLoginDialog = function () {
        return self.getParent().getLoginDialog().open();
      };
      $(title).find("a")[0].onclick = openLoginDialog;
      $(title).find("button")[0].onclick = openLoginDialog;
      addButton.hide();
      refreshButton.hide();
      tableElement.hide();
    } else {
      tableElement.show();
      title.innerHTML = self.getCustomOverlaysMessage();
      addButton.show();
      refreshButton.show();

      table = tableElement.on('order.dt', function () {
        if (tableElement.dataTable().fnSettings().aaSorting[0][0] === 0) {
          table.rowReorder.enable();
        } else {
          table.rowReorder.disable();
        }
      }).DataTable();
      var data = [];
      self.setGoogleLicenseConsentRequired(false);
      for (i = 0; i < customOverlays.length; i++) {
        overlay = customOverlays[i];
        if (showDefault && overlay.isDefaultOverlay()) {
          selectedOverlay[overlay.getId()] = true;
        }
        var disabled = !overlay.isGoogleLicenseConsent() && self.getMap().getProject().getMapCanvasType() === "GOOGLE_MAPS_API";
        data.push(self.overlayToDataRow(overlay, selectedOverlay[overlay.getId()], disabled));
        if (disabled) {
          self.setGoogleLicenseConsentRequired(true);
        }
      }
      table.clear().rows.add(data).draw();

    }

    self.onresize();

    var promises = [];
    if (showDefault) {
      for (var key in selectedOverlay) {
        if (selectedOverlay.hasOwnProperty(key) && selectedOverlay[key]) {
          promises.push(self.getMap().openDataOverlay(key));
        }
      }
    }
    return Promise.all(promises);
  }).then(function () {
    return self.getMap().redrawSelectedDataOverlays();
  });
};

/**
 *
 * @param {string} customOverlaysMessage
 */
OverlayPanel.prototype.setCustomOverlaysMessage = function (customOverlaysMessage) {
  this._customOverlaysMessage = customOverlaysMessage;
};

/**
 *
 * @returns {string}
 */
OverlayPanel.prototype.getCustomOverlaysMessage = function () {
  return this._customOverlaysMessage;
};


/**
 *
 * @returns {Promise}
 */
OverlayPanel.prototype.openAddOverlayDialog = function () {
  var self = this;
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
  }
  self._addOverlayDialog = new AddOverlayDialog({
    project: self.getProject(),
    customMap: self.getMap(),
    element: document.createElement("div"),
    configuration: self.getConfiguration()
  });
  self._addOverlayDialog.addListener("onAddOverlay", function (e) {
    self.getProject().addDataOverlay(e.arg);
    return self.refresh();
  });
  return self._addOverlayDialog.init().then(function () {
    return self._addOverlayDialog.open();
  });
};

/**
 *
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.init = function () {
  var self = this;
  var backgroundOverlay = self.getServerConnector().getSessionData().getSelectedBackgroundOverlay();
  var showDefault = (backgroundOverlay === undefined || backgroundOverlay === "undefined");
  var table = $("[name='customOverlaysTab']", self.getElement()).DataTable({
    columns: [{
      title: 'No',
      type: 'num',
      className: "no_padding"
    }, {
      title: 'Name',
      className: "word_wrap"
    }, {
      title: 'View',
      orderable: false,
      className: "no_padding"
    }, {
      title: 'Data',
      orderable: false,
      className: "no_padding"
    }, {
      title: 'Edit',
      orderable: false,
      className: "no_padding"
    }],
    paging: false,
    searching: false,
    info: false,
    rowReorder: true
  });
  table.on('row-reorder', function (e, diff) {
    var promises = [];

    for (var i = 0, ien = diff.length; i < ien; i++) {
      var rowData = table.row(diff[i].node).data();
      var checkbox = $("[type='checkbox']", rowData[2])[0];
      if (checkbox === undefined) {
        checkbox = rowData[2];
      }
      var overlayId = parseInt($(checkbox).attr("data"));
      var order;
      if (Functions.isInt(diff[i].newData)) {
        order = diff[i].newData;
      } else {
        order = parseInt($.parseHTML(diff[i].newData)[0].innerHTML);
      }
      promises.push(self.updateOverlayOrder(overlayId, order));
    }
    promises.push(self.getMap().redrawSelectedDataOverlays());
    return Promise.all(promises);
  });

  return this.refresh(showDefault);
};

/**
 *
 * @param {number} overlayId
 * @param {number} order
 * @returns {Promise}
 */
OverlayPanel.prototype.updateOverlayOrder = function (overlayId, order) {
  var self = this;
  return self.getProject().getDataOverlayById(overlayId).then(function (overlay) {
    overlay.setOrder(order);
    return self.getServerConnector().updateOverlay(overlay);
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @returns {PromiseLike<any>}
 */
OverlayPanel.prototype.removeOverlay = function (overlay) {
  var self = this;
  return self.getMap().hideDataOverlay(overlay.getId()).then(function () {
    return self.getServerConnector().removeOverlay({overlayId: overlay.getId()});
  }).then(function () {
    self.getProject().removeDataOverlay(overlay);
    return self.refresh();
  })
};

/**
 *
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.destroy = function () {
  var self = this;
  Panel.prototype.destroy.call(this);
  var customOverlayDataTable = $("[name='customOverlaysTab']", self.getElement());
  if ($.fn.DataTable.isDataTable(customOverlayDataTable)) {
    $(customOverlayDataTable).DataTable().destroy();
  }

  if (self._addOverlayDialog !== undefined) {
    return self._addOverlayDialog.destroy();
  } else {
    return Promise.resolve();
  }
};

/**
 *
 * @param {boolean} value
 */
OverlayPanel.prototype.setGoogleLicenseConsentRequired = function (value) {
  var leftPanel = this.getParent();
  if (leftPanel !== undefined) {
    leftPanel.setGoogleLicenseConsentRequired(value);
  }
};
module.exports = OverlayPanel;
