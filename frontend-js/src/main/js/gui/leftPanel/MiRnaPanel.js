"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */
var logger = require('../../logger');

var AbstractDbPanel = require('./AbstractDbPanel');
var PanelControlElementType = require('../PanelControlElementType');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbPanel
 */
function MiRnaPanel(params) {
  params.panelName = "mirna";
  params.helpTip = '<p>source: <a target="_mirtarbase" href="http://mirtarbase.mbc.nctu.edu.tw/">miRTarBase</a></p>'
      + '<p>use only mature sequence IDs according to <a target="_mirbase" href="http://www.mirbase.org">www.mirbase.org</a> (e.g., hsa-miR-125a-3p)</p>'
      + '<p>only targets with strong evidence as defined by miRTarBase are displayed<p>separate multiple search by semicolon</p>';
  params.placeholder = "mature seq. ID (miRTarBase)";
  AbstractDbPanel.call(this, params);
  $(params.element).addClass("minerva-mirna-panel");
  this.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH FOR TARGETS OF:";
}
MiRnaPanel.prototype = Object.create(AbstractDbPanel.prototype);
MiRnaPanel.prototype.constructor = MiRnaPanel;

/**
 *
 * @param {MiRna} [miRna]
 * @returns {HTMLDivElement}
 */
MiRnaPanel.prototype.createPreamble = function(miRna) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("div");
  if (miRna === undefined || miRna.getName() === undefined) {
    result.appendChild(guiUtils.createLabel("NOT FOUND"));
  } else {
    var line = document.createElement("div");
    line.appendChild(guiUtils.createLabel("MiRNA: "));
    line.appendChild(guiUtils.createLink("http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc=" + miRna.getName(), miRna
        .getName()));
    line.appendChild(guiUtils.createNewLine());

    result.appendChild(line);
    result.appendChild(guiUtils.createNewLine());
  }

  return result;
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
MiRnaPanel.prototype.createTableElement = function(target, icon) {
  return this.createTargetRow(target, icon);
};

/**
 *
 * @returns {Promise}
 */
MiRnaPanel.prototype.searchByQuery = function() {
  var self = this;
  var query = self.getControlElement(PanelControlElementType.SEARCH_INPUT).value;
  return self.getOverlayDb().searchByQuery(query);
};

/**
 *
 * @returns {Promise}
 */
MiRnaPanel.prototype.init = function() {
  var self = this;
  return AbstractDbPanel.prototype.init.call(this).then(function () {
    var query = ServerConnector.getSessionData().getMiRnaQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
MiRnaPanel.prototype.destroy = function () {
    return Promise.resolve();
};

/**
 *
 * @param {string} query
 * @returns {string[]}
 */
MiRnaPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

/**
 *
 * @returns {Promise}
 */
MiRnaPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getMiRnaSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

module.exports = MiRnaPanel;
