"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */
var logger = require('../../logger');

var AbstractDbPanel = require('./AbstractDbPanel');
var PanelControlElementType = require('../PanelControlElementType');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbPanel
 */
function DrugPanel(params) {
  params.panelName = "drug";
  params.helpTip = '<p>source: <a target="_drugbank" href="http://www.drugbank.ca/">DrugBank</a> and '
    + '<a target="_drugbank" href="https://www.ebi.ac.uk/chembl/">ChEMBL</a></p>'
    + "<p>use of drug names, synonyms and brand names is supported<p>separate multiple search by semicolon</p>";
  params.placeholder = "drug, synonym, brand name";

  AbstractDbPanel.call(this, params);
  $(params.element).addClass("minerva-drug-panel");
  var self = this;
  self.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH FOR TARGETS OF:";
}

DrugPanel.prototype = Object.create(AbstractDbPanel.prototype);
DrugPanel.prototype.constructor = DrugPanel;

/**
 *
 * @param {Drug} [drug]
 * @returns {HTMLDivElement}
 */
DrugPanel.prototype.createPreamble = function (drug) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("div");
  if (drug === undefined || drug.getName() === undefined) {
    result.appendChild(guiUtils.createLabel("NOT FOUND"));
  } else {
    result.appendChild(guiUtils.createParamLine({label: "Drug: ", value: drug.getName()}));
    result.appendChild(guiUtils.createParamLine({label: "Description: ", value: drug.getDescription()}));
    result.appendChild(guiUtils.createArrayParamLine({label: "Synonyms: ", value: drug.getSynonyms()}));
    result.appendChild(guiUtils.createArrayParamLine({label: "Brand names: ", value: drug.getBrandNames()}));
    result.appendChild(guiUtils.createParamLine({label: "Blood brain barrier: ", value: drug.getBloodBrainBarrier()}));
    result.appendChild(guiUtils.createAnnotations({
      label: "Sources: ",
      annotations: drug.getReferences(),
      groupAnnotations: false
    }));
    result.appendChild(guiUtils.createNewLine());
  }

  return result;
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
DrugPanel.prototype.createTableElement = function (target, icon) {
  return this.createTargetRow(target, icon);
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = self.getControlElement(PanelControlElementType.SEARCH_INPUT).value;
  return self.getOverlayDb().searchByQuery(query);
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.init = function () {
  var self = this;
  return AbstractDbPanel.prototype.init.call(this).then(function () {
    var query = ServerConnector.getSessionData().getDrugQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.destroy = function () {
  return Promise.resolve();
};

/**
 *
 * @param {string} query
 * @returns {string[]}
 */
DrugPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getDrugSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

module.exports = DrugPanel;
