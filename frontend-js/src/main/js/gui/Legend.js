"use strict";

var $ = require('jquery');
require('popper.js');
require('bootstrap');

var AbstractGuiElement = require('./AbstractGuiElement');
var ConfigurationType = require('../ConfigurationType');
var Functions = require('../Functions');
var PanelControlElementType = require('./PanelControlElementType');

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var Promise = require('bluebird');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function Legend(params) {
  AbstractGuiElement.call(this, params);

  this._initializeGui();
}

Legend.prototype = Object.create(AbstractGuiElement.prototype);
Legend.prototype.constructor = Legend;

/**
 *
 * @private
 */
Legend.prototype._initializeGui = function () {
  var self = this;

  var legendDiv = Functions.createElement({
    type: "div",
    id: "legend-div",
    className: "carousel"
  });
  legendDiv.setAttribute("data-ride", "carousel");

  self.getElement().appendChild(legendDiv);

  var indicators = Functions.createElement({
    type: "ul",
    name: "indicators",
    className: "carousel-indicators"
  });
  legendDiv.appendChild(indicators);
  self.setControlElement(PanelControlElementType.LEGEND_INDICATORS_OL, indicators);

  var slidesDiv = Functions.createElement({
    type: "div",
    name: "slides",
    className: "carousel-inner",
    role: "listbox"
  });
  legendDiv.appendChild(slidesDiv);
  self.setControlElement(PanelControlElementType.LEGEND_SLIDES_DIV, slidesDiv);

  var leftButton = Functions
    .createElement({
      type: "a",
      className: "carousel-control-prev",
      role: "button",
      href: "#legend-div",
      content: '<span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span>',
      xss: false
    });
  leftButton.setAttribute("data-slide", "prev");
  legendDiv.appendChild(leftButton);

  var rightButton = Functions
    .createElement({
      type: "a",
      className: "carousel-control-next",
      role: "button",
      href: "#legend-div",
      content: '<span class="carousel-control-next-icon"></span><span class="sr-only">Next</span>',
      xss: false
    });
  rightButton.setAttribute("data-slide", "next");
  legendDiv.appendChild(rightButton);
};

/**
 *
 */
Legend.prototype.hide = function () {
  this.getElement().style.display = "none";
};

/**
 *
 */
Legend.prototype.show = function () {
  var maxZIndex = Math.max.apply(null,
    $.map($('body *'), function (e) {
      if ($(e).css('position') !== 'static') {
        return parseInt($(e).css('z-index')) || 1;
      }
    }));

  this.getElement().style.display = "block";
  $(this.getElement()).css('z-index', maxZIndex + 1);
};

/**
 *
 * @param {string} file
 * @param {number} index
 * @returns {HTMLLIElement}
 */
function createLegendIndicator(file, index) {
  var result = document.createElement("li");
  result.setAttribute("data-target", "#legend");
  result.setAttribute("data-slide-to", "" + index);
  if (index === 0) {
    result.className = "active";
  }
  return result;
}

/**
 *
 * @param {ConfigurationOption} file
 * @param {number} index
 * @returns {HTMLDivElement}
 */
function createLegendSlide(file, index) {
  var result = document.createElement("div");
  if (index === 0) {
    result.className = "carousel-item active";
  } else {
    result.className = "carousel-item";
  }
  var img = document.createElement("img");
  // img.className = "d-block w-100";
  img.src = file.getValue();
  result.appendChild(img);
  return result;
}

/**
 *
 * @param {ConfigurationOption[]}legendFiles
 * @returns {Promise<ConfigurationOption[]>}
 */
function getValidLegendFiles(legendFiles) {
  var result = [];

  var promises = [];
  legendFiles.forEach(function (file) {
    result.push(file);
    var url = file.getValue();
    if (url.indexOf("http") !== 0 && "" !== url) {
      url = ServerConnector.getServerBaseUrl() + url;
    }
    promises.push(ServerConnector.sendGetRequest(url).catch(function () {
      var index = result.indexOf(file);
      if (index > -1) {
        result.splice(index, 1);
      }
    }));
  });

  return Promise.all(promises).then(function () {
    return result;
  });
}

/**
 *
 * @returns {Promise}
 */
Legend.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var menu = self.getControlElement(PanelControlElementType.LEGEND_INDICATORS_OL);
  var slides = self.getControlElement(PanelControlElementType.LEGEND_SLIDES_DIV);
  return ServerConnector.getConfigurationParam(ConfigurationType.LEGEND_FILES).then(function (legendFiles) {
    return getValidLegendFiles(legendFiles);
  }).then(function (legendFiles) {
    for (var i = 0; i < legendFiles.length; i++) {
      var legendFile = legendFiles[i];
      menu.appendChild(createLegendIndicator(legendFile, i));
      slides.appendChild(createLegendSlide(legendFile, i));
    }
    $(element).carousel();
  });

};

module.exports = Legend;
