"use strict";

/* exported logger */

var ContextMenu = require('./ContextMenu');

var logger = require('../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends ContextMenu
 */
function SelectionContextMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createSelectionContextMenuGui();
}

SelectionContextMenu.prototype = Object.create(ContextMenu.prototype);
SelectionContextMenu.prototype.constructor = SelectionContextMenu;

/**
 *
 * @returns {Promise}
 */
SelectionContextMenu.prototype.init = function () {
  var self = this;
  return self.createExportAsImageSubmenu().then(function (submenu) {
    self.addOption({submenu: submenu});
    return self.createExportAsModelSubmenu();
  }).then(function (submenu) {
    self.addOption({submenu: submenu});
  });
};

/**
 *
 * @private
 */
SelectionContextMenu.prototype._createSelectionContextMenuGui = function () {
  var self = this;
  self.addOption({
    name: "Remove Selection", handler: function () {
      self.getMap().removeSelection();
      if (self.getMap().isDrawingOn()) {
        self.getMap().toggleDrawing();
      }
    }
  });
};

module.exports = SelectionContextMenu;
