"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var InvalidCredentialsError = require('../InvalidCredentialsError');
var PanelControlElementType = require('./PanelControlElementType');
var ConfigurationType = require("../ConfigurationType");

var Functions = require('../Functions');
var logger = require('../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function LoginDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self._createLoginTab();

  var loginButton = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_BUTTON);
  loginButton.onclick = function () {
    var login = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_INPUT_TEXT).value;
    var password = self.getControlElement(PanelControlElementType.USER_TAB_PASSWORD_INPUT_TEXT).value;

    return ServerConnector.login(login, password).then(function () {
      window.location.reload(false);
    }).then(null, function (error) {
      if (error instanceof InvalidCredentialsError) {
        GuiConnector.alert("invalid credentials");
      } else {
        GuiConnector.alert(error);
      }
    });
  };

}

LoginDialog.prototype = Object.create(AbstractGuiElement.prototype);
LoginDialog.prototype.constructor = LoginDialog;

/**
 *
 * @param {HTMLElement[]} elements
 * @returns {HTMLElement}
 */
LoginDialog.prototype.createTableRow = function (elements) {
  var row = Functions.createElement({
    type: "div",
    style: "display: table-row;"
  });

  for (var i = 0; i < elements.length; i++) {
    var cell = Functions.createElement({
      type: "div",
      style: "display: table-cell;"
    });
    cell.appendChild(elements[i]);
    row.appendChild(cell);
  }
  return row;
};

/**
 *
 * @private
 */
LoginDialog.prototype._createLoginTab = function () {
  var self = this;

  var authorizationFormTab = Functions.createElement({
    type: "div",
    style: "width:100%;display: table;border-spacing: 10px;"
  });
  this.getElement().appendChild(authorizationFormTab);

  var loginLabel = Functions.createElement({
    type: "div",
    content: "LOGIN:"
  });
  var loginInput = Functions.createElement({
    type: "input",
    inputType: "text",
    style: "width:100%",
    name: "loginText"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_LOGIN_INPUT_TEXT, loginInput);

  authorizationFormTab.appendChild(self.createTableRow([loginLabel, loginInput]));

  var passwordLabel = Functions.createElement({
    type: "div",
    content: "PASSWORD:"
  });
  var passwordInput = Functions.createElement({
    type: "input",
    inputType: "password",
    style: "width:100%",
    name: "passwordText"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_PASSWORD_INPUT_TEXT, passwordInput);

  authorizationFormTab.appendChild(self.createTableRow([passwordLabel, passwordInput]));

  var centerTag = Functions.createElement({
    type: "center"
  });
  this.getElement().appendChild(centerTag);

  var loginButton = Functions.createElement({
    type: "button",
    name: "loginButton",
    content: "LOGIN"
  });
  centerTag.appendChild(loginButton);
  this.setControlElement(PanelControlElementType.USER_TAB_LOGIN_BUTTON, loginButton);
  this.getElement().appendChild(Functions.createElement({type: "br"}));

  this.getElement().appendChild(Functions.createElement({
    type: "a",
    name: "requestAccount",
    content: "Request an account",
    href: "#"
  }));

};

/**
 *
 * @returns {Promise}
 */
LoginDialog.prototype.init = function () {
  var self = this;
  return ServerConnector.getConfigurationParam(ConfigurationType.REQUEST_ACCOUNT_EMAIL).then(function (value) {
    var link = $("[name=requestAccount]", self.getElement());
    link.attr("href", "mailto:" + value + "?subject=MINERVA account request");
  });
};

/**
 *
 */
LoginDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-login-dialog',
      autoOpen: false,
      resizable: false
    });
  }

  $(div).dialog('option', 'title', 'AUTHORIZATION FORM');
  $(div).dialog("open");
};

/**
 *
 */
LoginDialog.prototype.destroy = function () {
  var self = this;
  if ($(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog("destroy");
  }
};

module.exports = LoginDialog;
