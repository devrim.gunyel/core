"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractExportPanel = require('./AbstractExportPanel');
var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');

var logger = require('../../logger');
var xss = require('xss');

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends AbstractExportPanel
 */
function GraphicsExportPanel(params) {
  params.panelName = "graphicsExport";
  AbstractExportPanel.call(this, params);
}

GraphicsExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
GraphicsExportPanel.prototype.constructor = GraphicsExportPanel;

/**
 *
 * @returns {Promise}
 */
GraphicsExportPanel.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var configuration;
  element.appendChild(self._createSelectProjectDiv());
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    element.appendChild(self._createSelectGraphicsFormatDiv(configuration.getImageConverters()));
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');
  });
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createSelectProjectDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "modelSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: "(Sub)map:"
  }));

  var choicesContainer = Functions.createElement({
    type: "ul"
  });
  typeDiv.appendChild(choicesContainer);

  var models = self.getModels();

  for (var i = 0; i < models.length; i++) {
    var model = models[i];
    var checkedString = "";
    if (i === 0) {
      checkedString = ' checked="checked" ';
    }
    var modelName = xss(model.getName());
    var row = Functions.createElement({
      type: "li",
      content: '<div><label> <input type="radio" name="model" value="' + model.getId() + '"' + checkedString + '/>'
      + modelName + '</label></div>',
      xss: false
    });
    choicesContainer.appendChild(row);
  }

  return typeDiv;
};

/**
 *
 * @param {ImageConverter[]} formats
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createSelectGraphicsFormatDiv = function (formats) {
  var typeDiv = Functions.createElement({
    type: "div",
    name: "formatSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: "Format:"
  }));

  var choicesContainer = Functions.createElement({
    type: "ul"
  });
  typeDiv.appendChild(choicesContainer);

  for (var i = 0; i < formats.length; i++) {
    var format = formats[i];
    var checkedString = "";
    if (i === 0) {
      checkedString = ' checked="checked" ';
    }
    var row = Functions.createElement({
      type: "li",
      content: '<div><label> <input type="radio" name="format" value="' + format.handler + '"' + checkedString + '/>'
      + format.name + '</label></div>',
      xss: false

    });
    choicesContainer.appendChild(row);
  }

  return typeDiv;
};

/**
 *
 * @returns {string}
 */
GraphicsExportPanel.prototype.getSubmapId = function () {
  var self = this;
  var div = $("div[name='modelSelectDiv']", $(self.getElement()))[0];
  var id = null;
  $(":checked", $(div)).each(function (index, element) {
    id = element.value;
  });
  return id;
};

/**
 *
 * @returns {string}
 */
GraphicsExportPanel.prototype.getFormatHandler = function () {
  var self = this;
  var div = $("div[name='formatSelectDiv']", $(self.getElement()))[0];
  var format = null;
  $(":checked", $(div)).each(function (index, element) {
    format = element.value;
  });
  return format;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createDownloadButton = function () {
  var self = this;
  var downloadDiv = Functions.createElement({
    type: "div",
    name: "downloadDiv",
    style: "clear:both; padding: 10px;"
  });
  var button = Functions.createElement({
    type: "button",
    name: "downloadButton",
    content: " Download",
    onclick: function () {
      var identifier = null;
      var defaultOverlayName = "Network";
      for (var i = 0; i < self.getProject().getDataOverlays().length; i++) {
        var overlay = self.getProject().getDataOverlays()[i];
        if (identifier === null || overlay.getName() === defaultOverlayName) {
          identifier = overlay.getId();
        }
      }

      return ServerConnector.getImageDownloadUrl({
        modelId: self.getSubmapId(),
        backgroundOverlayId: identifier,
        handlerClass: self.getFormatHandler()
      }).then(function (url) {
        return self.downloadFile(url);
      }).then(null, GuiConnector.alert);
    }
  });
  downloadDiv.appendChild(button);

  return downloadDiv;
};

module.exports = GraphicsExportPanel;
