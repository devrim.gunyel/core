"use strict";

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function AbstractAnnotatorsDialog(params) {
  AbstractGuiElement.call(this, params);
}

AbstractAnnotatorsDialog.prototype = Object.create(AbstractGuiElement.prototype);
AbstractAnnotatorsDialog.prototype.constructor = AbstractAnnotatorsDialog;

/**
 *
 * @param {BioEntityType} elementType
 * @param {boolean} [includeChildren=false]
 *
 * @returns {BioEntityType[]}
 */
AbstractAnnotatorsDialog.prototype.getAllChildrenTypesIfNeeded = function (elementType, includeChildren) {
  var result = [elementType];
  if (includeChildren) {
    var queue = [elementType];
    var elementTypes = this.getConfiguration().getElementTypes();
    while (queue.length > 0) {
      var type = queue.shift();
      for (var i = 0; i < elementTypes.length; i++) {
        if (type.className === elementTypes[i].parentClass) {
          queue.push(elementTypes[i]);
          result.push(elementTypes[i]);
        }
      }
    }
  }
  return result;
};

/**
 *
 * @return {BioEntityTypeTreeNode[]}
 */
AbstractAnnotatorsDialog.prototype.getTypeNodeList = function () {
  var types = [];

  var treeData = this.getConfiguration().getElementTypeTree();

  var queue = [treeData];

  while (queue.length > 0) {
    var node = queue.shift();
    if (node.children !== undefined && node.children.length > 0) {
      for (var i = 0; i < node.children.length; i++) {
        queue.push(node.children[i]);
      }
    } else {
      types.push(node);
    }
  }
  types.sort(function (a, b) {
    return a.text.localeCompare(b.text);
  });
  return types;
};


module.exports = AbstractAnnotatorsDialog;
