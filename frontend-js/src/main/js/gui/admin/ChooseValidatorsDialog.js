"use strict";

/* exported logger */
var $ = require('jquery');
require('jquery-ui/dialog');
require('jstree');

var AbstractAnnotatorsDialog = require('./AbstractAnnotatorsDialog');
var GuiConnector = require("../../GuiConnector");
var UserPreferences = require("../../map/data/UserPreferences");
var MultiCheckboxList = require("multi-checkbox-list");

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 /**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function ChooseValidatorsDialog(params) {
  AbstractAnnotatorsDialog.call(this, params);
  var self = this;
  self.createGui();
}

ChooseValidatorsDialog.prototype = Object.create(AbstractAnnotatorsDialog.prototype);
ChooseValidatorsDialog.prototype.constructor = ChooseValidatorsDialog;

/**
 *
 */
ChooseValidatorsDialog.prototype.createGui = function () {
  var self = this;
  var content = Functions.createElement({
    type: "div",
    style: "display:table;height:100%;width:100%"
  });
  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%;",
    content: "<div name='elementTree' style='height:100%;overflow-y:scroll;'/>",
    xss: false
  }));

  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%;width:100%;",
    content: "<div name='annotatorListBox' style='height:100%;width:100%;overflow-y:auto;'><span> </span></div>",
    xss: false
  }));

  self.getElement().appendChild(content);
};

/**
 *
 * @param {BioEntityType} elementType
 * @returns {Promise}
 */
ChooseValidatorsDialog.prototype.setElementType = function (elementType) {
  var self = this;

  var configuration;

  return self.getServerConnector().getConfiguration().then(function (result) {
    configuration = result;
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var element = $("[name='annotatorListBox']", self.getElement())[0];
    Functions.removeChildren(element);

    var validAnnotationSelect = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    element.appendChild(validAnnotationSelect);
    self.createValidAnnotationsDualListBox(user, configuration, elementType, validAnnotationSelect);

    var verifyAnnotationSelect = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    element.appendChild(verifyAnnotationSelect);
    self.createVerifyAnnotationsDualListBox(user, configuration, elementType, verifyAnnotationSelect);

    element.appendChild(Functions.createElement({type: "br"}));
    var includeChildrenCheckbox = Functions.createElement({type: "input", inputType: "checkbox"});
    var copyFromButton = Functions.createElement({
      type: "button", content: "Copy from", name: "copy-from-validator", onclick: function () {
        var typeClassName = copyFromSelect.value;
        var requiredAnnotations, validAnnotators;
        for (var i = 0; i < configuration.getElementTypes().length; i++) {
          var type = configuration.getElementTypes()[i];
          if (typeClassName === type.className) {
            requiredAnnotations = user.getPreferences().getElementRequiredAnnotations(typeClassName);
            validAnnotators = user.getPreferences().getElementValidAnnotations(typeClassName);
          }
        }
        if (requiredAnnotations === undefined) {
          return GuiConnector.alert("Invalid element type: " + copyFromSelect.value);
        } else {
          var includeChildren = includeChildrenCheckbox.checked;
          return self.saveAnnotationsInfo({
            elementTypes: self.getAllChildrenTypesIfNeeded(elementType, includeChildren),
            requiredAnnotators: requiredAnnotations,
            validAnnotators: validAnnotators
          }).then(function () {
            return self.setElementType(elementType);
          });
        }
      }
    });
    element.appendChild(copyFromButton);
    var copyFromSelect = Functions.createElement({type: "select", style: "margin:5px"});
    element.appendChild(copyFromSelect);
    var options = [], i;
    var nodeList = self.getTypeNodeList();
    for (i = 0; i < nodeList.length; i++) {
      var type = nodeList[i];
      options.push(Functions.createElement({
        type: "option",
        value: type.data.className,
        content: type.text
      }));
    }
    options.sort(function (a, b) {
      return a.text === b.text ? 0 : a.text < b.text ? -1 : 1
    });
    for (i = 0; i < options.length; i++) {
      copyFromSelect.appendChild(options[i]);
    }

    element.appendChild(includeChildrenCheckbox);
    element.appendChild(Functions.createElement({type: "span", content: "Apply to all in subtree"}));

  });

};

/**
 *
 * @param {BioEntityType[]} params.elementTypes
 * @param {{list:string[], requiredAtLeastOnce:boolean}} params.requiredAnnotators
 * @param {string[]} params.validAnnotators
 *
 * @returns {Promise}
 */
ChooseValidatorsDialog.prototype.saveAnnotationsInfo = function (params) {
  var elementTypes = params.elementTypes;
  var requiredAnnotators = params.requiredAnnotators;
  var validAnnotators = params.validAnnotators;
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().getLoggedUser().then(function (user) {

    var data = new UserPreferences();

    var elementRequiredAnnotators = {};
    var elementValidAnnotators = {};
    for (var i = 0; i < elementTypes.length; i++) {
      elementRequiredAnnotators[elementTypes[i].className] = {
        "require-at-least-one": requiredAnnotators.requiredAtLeastOnce,
        "annotation-list": requiredAnnotators.list.slice()
      };
      elementValidAnnotators[elementTypes[i].className] = validAnnotators.slice();
    }
    data.setElementRequiredAnnotations(elementRequiredAnnotators);
    data.setElementValidAnnotations(elementValidAnnotators);
    return self.getServerConnector().updateUserPreferences({
      user: user,
      preferences: data
    });
  }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
};


/**
 *
 * @param {User} user
 * @param {Configuration} configuration
 * @param {BioEntityType} elementType
 * @param {HTMLElement} validAnnotationSelect
 */
ChooseValidatorsDialog.prototype.createValidAnnotationsDualListBox = function (user, configuration, elementType, validAnnotationSelect) {
  var self = this;

  var miriamTypes = configuration.getMiriamTypes();

  var validAnnotations = user.getPreferences().getElementValidAnnotations(elementType.className);

  var entries = [];


  for (var i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    var entry = {name: miriamType.getCommonName(), value: miriamType.getName(), selected: false};

    for (var j = 0; j < validAnnotations.length; j++) {
      if (miriamType.getName() === validAnnotations[j]) {
        entry.selected = true;
      }
    }
    entries.push(entry);
  }

  var checkboxList = new MultiCheckboxList(validAnnotationSelect, {
    entries: entries,
    listTitle: "Available",
    selectedTitle: "Selected",
    selectedList: true
  });

  var changeSelection = function (elementId, selected) {
    var miriamTypes = configuration.getMiriamTypes();
    var miriamType;
    for (var i = 0; i < miriamTypes.length; i++) {
      if (elementId === miriamTypes[i].getName()) {
        miriamType = miriamTypes[i];
      }
    }
    if (selected) {
      validAnnotations.push(miriamType.getName());
    } else {
      var index = validAnnotations.indexOf(miriamType.getName());
      if (index > -1) {
        validAnnotations.splice(index, 1);
      }
    }

    var data = new UserPreferences();

    var elementAnnotators = {};
    elementAnnotators[elementType.className] = validAnnotations;
    data.setElementValidAnnotations(elementAnnotators);
    GuiConnector.showProcessing();
    return self.getServerConnector().updateUserPreferences({
      user: user,
      preferences: data
    }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
  };

  checkboxList.addListener("select", function (element) {
    return changeSelection(element.value, true);
  });
  checkboxList.addListener("deselect", function (element) {
    return changeSelection(element.value, false);
  });

};

/**
 *
 * @param {User} user
 * @param {Configuration} configuration
 * @param {BioEntityType} elementType
 * @param {HTMLElement} verifyAnnotationSelect
 */

ChooseValidatorsDialog.prototype.createVerifyAnnotationsDualListBox = function (user, configuration, elementType, verifyAnnotationSelect) {
  var self = this;
  var requiredAnnotationsData = user.getPreferences().getElementRequiredAnnotations(elementType.className);

  var verifyCheckboxDiv = Functions.createElement({type: "div"});
  var checkbox = Functions.createElement({
    type: "input",
    inputType: "checkbox",
    name: "require-at-least-one-checkbox",
    onclick: function () {
      var data = new UserPreferences();

      var elementRequiredAnnotations = {};
      elementRequiredAnnotations[elementType.className] = {
        "require-at-least-one": checkbox.checked,
        "annotation-list": requiredAnnotationsData.list
      };
      data.setElementRequiredAnnotations(elementRequiredAnnotations);
      GuiConnector.showProcessing();
      return self.getServerConnector().updateUserPreferences({
        user: user,
        preferences: data
      }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
    }
  });
  checkbox.checked = requiredAnnotationsData.requiredAtLeastOnce;
  verifyCheckboxDiv.appendChild(Functions.createElement({type: "br"}));
  verifyCheckboxDiv.appendChild(Functions.createElement({type: "br"}));
  verifyCheckboxDiv.appendChild(checkbox);
  verifyCheckboxDiv.appendChild(Functions.createElement({
    type: "span",
    content: "One of these miriam should be required"
  }));
  verifyAnnotationSelect.parentNode.insertBefore(verifyCheckboxDiv, verifyAnnotationSelect);

  var miriamTypes = configuration.getMiriamTypes();

  var entries = [];

  for (var i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    var entry = {name: miriamType.getCommonName(), value: miriamType.getName(), selected: false};
    for (var j = 0; j < requiredAnnotationsData.list.length; j++) {
      if (miriamType.getName() === requiredAnnotationsData.list[j]) {
        entry.selected = true;
      }
    }
    entries.push(entry);
  }

  var checkboxList = new MultiCheckboxList(verifyAnnotationSelect, {
    entries: entries,
    listTitle: "Available",
    selectedTitle: "Selected",
    selectedList: true
  });

  var changeSelection = function (elementId, selected) {

    var miriamTypes = configuration.getMiriamTypes();
    var miriamType;
    for (var i = 0; i < miriamTypes.length; i++) {
      if (elementId === miriamTypes[i].getName()) {
        miriamType = miriamTypes[i];
      }
    }
    if (selected) {
      requiredAnnotationsData.list.push(miriamType.getName());
    } else {
      var index = requiredAnnotationsData.list.indexOf(miriamType.getName());
      if (index > -1) {
        requiredAnnotationsData.list.splice(index, 1);
      }
    }

    var data = new UserPreferences();

    var elementRequiredAnnotations = {};
    elementRequiredAnnotations[elementType.className] = {
      "require-at-least-one": requiredAnnotationsData.requiredAtLeastOnce,
      "annotation-list": requiredAnnotationsData.list
    };
    data.setElementRequiredAnnotations(elementRequiredAnnotations);
    GuiConnector.showProcessing();
    return self.getServerConnector().updateUserPreferences({
      user: user,
      preferences: data
    }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
  };

  checkboxList.addListener("select", function (element) {
    return changeSelection(element.value, true);
  });
  checkboxList.addListener("deselect", function (element) {
    return changeSelection(element.value, false);
  });

};

/**
 *
 * @returns {Promise}
 */
ChooseValidatorsDialog.prototype.init = function () {
  var self = this;
  return self.getServerConnector().getConfiguration().then(function (configuration) {

    var treeData = configuration.getElementTypeTree();

    var element = $('[name="elementTree"]', self.getElement());
    element.jstree({
      core: {
        data: treeData
      }
    }).on('ready.jstree', function () {
      element.jstree("open_all");
    }).on("select_node.jstree",
      function (evt, data) {
        return self.setElementType(data.node.data);
      }
    );
  });
};

/**
 *
 */
ChooseValidatorsDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

/**
 *
 */
ChooseValidatorsDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-select-annotations-dialog',
      title: "Select valid annotations",
      modal: true,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2

    });
  }

  $(div).dialog("open");
};

module.exports = ChooseValidatorsDialog;
