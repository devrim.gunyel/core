"use strict";

var $ = require('jquery');
require('spectrum-colorpicker');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ConfigurationType = require('../../ConfigurationType');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var NetworkError = require('../../NetworkError');
var ValidationError = require('../../ValidationError');

var logger = require('../../logger');
var HttpStatus = require('http-status-codes');

var Promise = require("bluebird");
var xss = require("xss");

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {string} params.panelName
 * @param params.parent
 *
 * @constructor
 * @extends AbstractAdminPanel
 */
function ConfigurationAdminPanel(params) {
  AbstractAdminPanel.call(this, params);

  var self = this;
  $(self.getElement()).removeClass("pre-scrollable");
  $(self.getElement()).addClass("minerva-configuration-tab");

  self._createGui();
}

ConfigurationAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
ConfigurationAdminPanel.prototype.constructor = ConfigurationAdminPanel;

/**
 *
 * @private
 */
ConfigurationAdminPanel.prototype._createGui = function () {
  var self = this;
  self.getGuiUtils().initTabContent(self);


  $(self.getElement()).on("input", ".minerva-color-input", function () {
    var input = this;
    var value = $(input).val();
    var type = $(input).attr("data");
    if (value.length !== 6) {
      value = "FFFFFF";
    }
    $("[name='edit-color-" + type + "']", self.getElement()).css("background-color", "#" + value);
  });

  // $("[name='configurationTable']", self.getElement()).on("change", "input", function () {
  $(self.getElement()).on("change", "input, textarea", function () {
    var input = this;
    var type = $(input).attr("data");
    var name = $(input).attr("name");
    if (name !== undefined && name.indexOf("edit") === 0) {
      return self.saveOption(type);
    }
  });

  $(self.getElement()).on("click", ".minerva-color-button", function () {
    var button = this;
    var value = $(button).css("background-color");
    var type = $(button).attr("data");
    // noinspection JSUnusedGlobalSymbols
    var colorPicker = $(button).parent().spectrum({
      color: value,
      move: function (color) {
        var value = color.toHexString().replace("#", '');
        $("[name='edit-" + type + "']", self.getElement()).val(value);
        $("[name='edit-color-" + type + "']", self.getElement()).css("background-color", "#" + value);
      },
      hide: function () {
        colorPicker.spectrum("destroy");
        return self.saveOption(type);
      }
    });
    return new Promise.delay(1).then(function () {
      colorPicker.show();
      colorPicker.spectrum("show");
    });
  });
};

/**
 *
 * @param {ConfigurationOption[]} options
 * @param {string} type
 */
ConfigurationAdminPanel.prototype.createOptionsTable = function (options, type) {
  var self = this;

  var configurationDiv = Functions.createElement({
    type: "div",
    className: "pre-scrollable"
  });
  var configurationTable = Functions.createElement({
    type: "table",
    name: "configurationTable",
    className: "display",
    style: "width:100%"
  });
  configurationDiv.appendChild(configurationTable);

  // noinspection JSUnusedGlobalSymbols
  var dataTable = $(configurationTable).DataTable({
    columns: [{
      title: 'Name'
    }, {
      title: 'Value'
    }],
    columnDefs: [
      {"orderable": false, "targets": 1}
    ],
    order: [[0, "asc"]]
  });

  var data = [];

  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    if (option.getGroup() === type) {
      var rowData = self.optionToTableRow(option);
      data.push(rowData);
    }
  }

  dataTable.clear().rows.add(data).draw();

  self.getGuiUtils().addTab(self, {name: type, content: configurationDiv});

  return Promise.all([self.bindDataTablePageLengthToUserPreference({
    element: configurationTable,
    preferenceName: 'admin-configuration-datatable-length'
  }), self.bindDataTableOrderToUserPreference({
    element: configurationTable,
    preferenceName: 'admin-configuration-' + type + '-datatable-order'
  })]);
};

/**
 *
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var configuration = self.getConfiguration();
    var privilege = configuration.getPrivilegeType(PrivilegeType.IS_ADMIN);
    if (user.hasPrivilege(privilege)) {
      return self.setOptions(configuration.getOptions());
    } else {
      self.disablePanel("You have no privilege to manage configuration");
    }
  });
};

/**
 *
 * @param {ConfigurationOption[]} options
 *
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.setOptions = function (options) {
  var self = this;

  var categories = {"": true};
  var promises = [];
  for (var i = 0; i < options.length; i++) {
    var option = options[i];

    var group = option.getGroup();
    if (categories[group] === undefined && group !== undefined) {
      categories[group] = true;
      promises.push(self.createOptionsTable(options, group));
    }
  }
  return Promise.all(promises);
};

/**
 *
 * @param {ConfigurationOption} option
 * @returns {Array}
 */
ConfigurationAdminPanel.prototype.optionToTableRow = function (option) {
  var value = option.getValue();
  if (value === undefined) {
    value = "";
  }
  var row = [];
  var editOption;

  if (option.getValueType() === "STRING" ||
    option.getValueType() === "INTEGER" ||
    option.getValueType() === "DOUBLE" ||
    option.getValueType() === "EMAIL" ||
    option.getValueType() === "URL") {
    editOption = "<input name='edit-" + option.getType() + "' data='" + option.getType() + "' value='" + value + "'/>";
  } else if (option.getValueType() === "PASSWORD") {
    editOption = "<input type='password' name='edit-" + option.getType() + "' autocomplete='new-password' data='" + option.getType() + "' value='" + value + "'/>";
  } else if (option.getValueType() === "TEXT") {
    editOption = "<textarea name='edit-" + option.getType() + "' data='" + option.getType() + "' >" + xss(value) + "</textarea>";
  } else if (option.getValueType() === "BOOLEAN") {
    var checked = "";
    if (value.toLowerCase() === "true") {
      checked = " checked ";
    }
    editOption = "<input type='checkbox' name='edit-" + option.getType() + "' " + checked + " data='" + option.getType() + "' />";
  } else if (option.getValueType() === "COLOR") {
    editOption = "<div>" +
      "<input class='minerva-color-input' name='edit-" + option.getType() + "' data='" + option.getType() + "' value='" + value + "'/>" +
      "<button class='minerva-color-button' name='edit-color-" + option.getType() + "' data='" + option.getType() + "' style='background-color: #" + value + "'>&nbsp</button>" +
      "</div>";
  } else {
    logger.warn("Don't know how to handle: " + option.getValueType());
    editOption = "<input name='edit-" + option.getType() + "' value='" + value + "' readonly data='" + option.getType() + "' />";
  }
  row[0] = option.getCommonName();
  row[1] = editOption;
  return row;
};

/**
 *
 * @param {string} type
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.saveOption = function (type) {
  var self = this;
  var option;

  var value;
  var element = $("[name='edit-" + type + "']", self.getElement());
  if (element.is(':checkbox')) {
    if (element.is(':checked')) {
      value = "true";
    } else {
      value = "false";
    }
  } else {
    value = element.val();
  }

  var oldVal;
  GuiConnector.showProcessing();
  return self.getServerConnector().getConfiguration().then(function (configuration) {
    option = configuration.getOption(type);
    oldVal = option.getValue();
    if (option === undefined) {
      return Promise.reject(new ValidationError("Unknown configuration type: " + type));
    }
    if (option.getValueType() === 'PASSWORD' && value === '') {
      return new Promise(function (resolve) {
        var html = '<form><span>Do you want to reset this password?</span><br></form>';
        $(html).dialog({
          modal: true,
          title: 'Reset password',
          close: function () {
            $(this).dialog('destroy').remove();
            resolve(false);
          },
          buttons: {
            'YES': function () {
              $(this).dialog('destroy').remove();
              resolve(true);
            },
            'NO': function () {
              $(this).dialog('destroy').remove();
              resolve(false);
            }
          }
        });
      });
    } else {
      return true;
    }
  }).then(function (performUpdate) {
    if (performUpdate) {
      option.setValue(value);
      return self.getServerConnector().updateConfigurationOption(option);
    }
  }).then(function () {
    if (type === ConfigurationType.TERMS_OF_USE) {
      return new Promise(function (resolve) {
        var html = '<form><span>Do you want to reset user the status of ToS acceptance for all users of the platform?</span><br></form>';
        $(html).dialog({
          modal: true,
          title: 'Status of users ToS acceptance',
          close: function () {
            $(this).dialog('destroy').remove();
            resolve(false);
          },
          buttons: {
            'YES': function () {
              $(this).dialog('destroy').remove();
              resolve(true);
            },
            'NO': function () {
              $(this).dialog('destroy').remove();
              resolve(false);
            }
          }
        });
      }).then(function (status) {
        if (status) {
          return self.getServerConnector().resetUserTos();
        }
      });
    }
  }).catch(function (e) {
    if (e instanceof NetworkError && e.statusCode === HttpStatus.BAD_REQUEST) {
      var content = e.content;
      GuiConnector.alert(new ValidationError(content.reason));
      option.setValue(oldVal);
      element.val(oldVal);
    } else {
      GuiConnector.alert(e);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 */
ConfigurationAdminPanel.prototype.destroy = function () {
  var self = this;
  var tables = $("[name='configurationTable']", self.getElement());
  for (var i = 0; i < tables.length; i++) {
    if ($.fn.DataTable.isDataTable(tables[i])) {
      $(tables[i]).DataTable().destroy();
    }
  }

};

module.exports = ConfigurationAdminPanel;
