"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');


/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {string} params.projectId
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function LogListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createLogListDialogGui();

  /**
   * @type {string}
   * @private
   */
  self._projectId = params.projectId;
}

LogListDialog.prototype = Object.create(AbstractGuiElement.prototype);
LogListDialog.prototype.constructor = LogListDialog;

/**
 *
 */
LogListDialog.prototype.createLogListDialogGui = function () {
  var self = this;
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" +
      "<th>ID</th>" +
      "<th>Type</th>" +
      "<th>Severity</th>" +
      "<th>Object ID</th>" +
      "<th>Object Class</th>" +
      "<th>Map name</th>" +
      "<th>Content</th>" +
      "<th>Source</th>" +
      "</tr>"
  });
  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-logs-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  self.getElement().appendChild(menuRow);

  var downloadButton = Functions.createElement({
    type: "button",
    name: "saveUser",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;DOWNLOAD',
    onclick: function () {
      GuiConnector.showProcessing();
      return self.downloadAll().then(function (result) {
        var blob = new Blob([result], {
          type: "text/plain;charset=utf-8"
        });
        var FileSaver = require("file-saver");
        return FileSaver.saveAs(blob, self._projectId + "-logs.txt");
      }).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
      });
    },
    xss: false
  });
  menuRow.appendChild(downloadButton);

};

function removeNull(object) {
  if (object === null || object === undefined) {
    return "";
  }
  return object;
}

function extractTypeName(type) {
  if (type === undefined || type === null) {
    return "";
  }
  var result = type.toLowerCase().replace("_", " ");
  return result[0].toUpperCase() + result.slice(1);
}

function entryToRow(entry) {
  var row = [];
  row[0] = removeNull(entry.id);
  row[1] = removeNull(extractTypeName(entry.type));
  row[2] = removeNull(entry.level);
  row[3] = removeNull(entry.objectIdentifier);
  row[4] = removeNull(entry.objectClass);
  row[5] = removeNull(entry.mapName);
  row[6] = removeNull(entry.content);
  row[7] = removeNull(entry.source);
  return row;
}

/**
 *
 * @param {Object} data
 * @param {number} data.start
 * @param {number} data.length
 * @param {Object} data.search
 * @param {Object} data.draw
 * @param {Array} data.order
 * @param {function} callback
 * @returns {Promise}
 * @private
 */
LogListDialog.prototype._dataTableAjaxCall = function (data, callback) {
  var self = this;
  return self.getServerConnector().getProjectLogs({
    start: data.start,
    length: data.length,
    sortColumn: self.getColumnsDefinition()[data.order[0].column].name,
    sortOrder: data.order[0].dir,
    search: data.search.value,
    projectId: self._projectId
  }).then(function (logEntries) {
    var out = [];

    for (var i = 0; i < logEntries.data.length; i++) {
      var entry = logEntries.data[i];

      var row = entryToRow(entry);
      out.push(row);
    }
    callback({
      draw: data.draw,
      recordsTotal: logEntries.totalSize,
      recordsFiltered: logEntries.filteredSize,
      data: out
    });
  });
};

/**
 *
 * @returns {Promise}
 */
LogListDialog.prototype.downloadAll = function () {
  var self = this;
  return self.getServerConnector().getProjectLogs({
    length: 2147483646,
    projectId: self._projectId
  }).then(function (logEntries) {
    var tmp = [];
    for (var i = 0; i < logEntries.data.length; i++) {
      tmp.push(entryToRow(logEntries.data[i]).join("\t"));
    }
    return tmp.join("\n");
  });
};

LogListDialog.prototype.open = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      dialogClass: 'minerva-logs-dialog',
      title: "Log list",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  if (!$.fn.DataTable.isDataTable(self.tableElement)) {
    return new Promise(function (resolve) {
      $(self.tableElement).dataTable({
        serverSide: true,
        ordering: true,
        searching: true,
        ajax: function (data, callback) {
          resolve(self._dataTableAjaxCall(data, callback));
        },
        columns: self.getColumnsDefinition()
      });
    });
  } else {
    return Promise.resolve();
  }

};

/**
 *
 * @returns {Array}
 */
LogListDialog.prototype.getColumnsDefinition = function () {
  return [{
    name: "id"
  }, {
    name: "type"
  }, {
    name: "level"
  }, {
    name: "objectIdentifier"
  }, {
    name: "objectClass"
  }, {
    name: "mapName"
  }, {
    name: "content"
  }, {
    name: "source"
  }];
};

/**
 *
 * @returns {Promise}
 */
LogListDialog.prototype.init = function () {
  return Promise.resolve();
};

/**
 *
 */
LogListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

module.exports = LogListDialog;
