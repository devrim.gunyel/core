"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var AddProjectDialog = require('./AddProjectDialog');
var EditProjectDialog = require('./EditProjectDialog');
var LogListDialog = require('./LogListDialog');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ConfigurationType = require('../../ConfigurationType');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var Promise = require("bluebird");

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function MapsAdminPanel(params) {
  var self = this;
  AbstractAdminPanel.call(self, params);
  self._createGui();

  $(self.getElement()).addClass("minerva-projects-tab");

}

MapsAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
MapsAdminPanel.prototype.constructor = MapsAdminPanel;

MapsAdminPanel.AUTO_REFRESH_TIME = 5000;

/**
 *
 * @private
 */
MapsAdminPanel.prototype._createGui = function () {
  var self = this;
  var projectsDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(projectsDiv);

  var dataDiv = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  projectsDiv.appendChild(dataDiv);

  dataDiv.appendChild(self._createMenuRow());
  dataDiv.appendChild(self._createProjectTableRow());
  dataDiv.appendChild(self._createMenuRow());

};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
MapsAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addProjectButton = Functions.createElement({
    type: "button",
    name: "addProject",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD PROJECT',
    onclick: function () {
      return self.onAddClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshProject",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addProjectButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
MapsAdminPanel.prototype._createProjectTableRow = function () {
  var self = this;
  var projectsRow = Functions.createElement({
    type: "div",
    style: "display:table-row; width:100%"
  });

  var projectsTable = Functions.createElement({
    type: "table",
    name: "projectsTable",
    className: "display",
    style: "width:100%"
  });
  projectsRow.appendChild(projectsTable);

  $(projectsTable).DataTable({
    columns: [{
      title: 'ProjectId'
    }, {
      title: 'Created'
    }, {
      title: 'Created by'
    }, {
      title: 'Name'
    }, {
      title: 'Disease'
    }, {
      title: 'Organism'
    }, {
      title: 'Status'
    }, {
      title: 'Edit'
    }, {
      title: 'Remove'
    }],
    order: [[0, "asc"]],
    columnDefs: [
      {"orderable": false, "targets": [7, 8]}
    ]
  });
  self.bindDataTablePageLengthToUserPreference({
    element: projectsTable,
    preferenceName: 'admin-projects-datatable-length'
  });

  $(projectsTable).on("click", "[name='removeProject']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove '" + $(button).attr("data") + "' map?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.removeProject($(button).attr("data"))
      }
    }).catch(GuiConnector.alert);
  });

  $(projectsTable).on("click", "[name='showEditDialog']", function () {
    var button = this;
    return self.showEditDialog($(button).attr("data")).catch(GuiConnector.alert);
  });
  $(projectsTable).on("click", "[name='showLogEntries']", function () {
    var button = this;
    return self.showLogs($(button).attr("data")).catch(GuiConnector.alert);
  });

  return projectsRow;
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(self).then(function () {
    return self.getServerConnector().getProjects();
  }).then(function (projects) {
    return self.setProjects(projects);
  }).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var configuration = self.getConfiguration();
    var canAddProject = user.hasPrivilege(configuration.getPrivilegeType(PrivilegeType.IS_CURATOR)) ||
      user.hasPrivilege(configuration.getPrivilegeType(PrivilegeType.IS_ADMIN));
    $("[name='addProject']", self.getElement()).attr("disabled", !canAddProject);

    var projectsTable = $("[name='projectsTable']", self.getElement())[0];

    return self.bindDataTableOrderToUserPreference({
      element: projectsTable,
      preferenceName: 'admin-projects-datatable-order'
    });
  });
};

/**
 *
 * @param {Project} project
 * @param {Array} [row]
 * @param {User} user
 * @returns {Array}
 */
MapsAdminPanel.prototype.projectToTableRow = function (project, row, user) {
  var self = this;
  var disease = self.getHtmlStringLink(project.getDisease());
  var organism = self.getHtmlStringLink(project.getOrganism());

  if (row === undefined) {
    row = [];
  }
  var projectId = project.getProjectId();
  var formattedProjectId;
  if (project.getStatus().toLowerCase() === "ok") {
    formattedProjectId = "<a href='" + "index.xhtml?id=" + projectId + "' target='" + projectId + "'>" + projectId + "</a>";
  } else {
    formattedProjectId = projectId;
  }
  var status = project.getStatus();
  if (project.getStatus().toLowerCase() !== "ok" && project.getStatus().toLowerCase() !== "failure") {
    status += ' (' + project.getProgress().toFixed(2) + ' %)';
  }

  var isAdmin = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR));

  var icon;
  if (project.hasLogEntries()) {
    icon = "<i class='fa fa-exclamation-triangle' style='font-size:18px; padding-right:10px;color:black'></i>";
    if (isAdmin) {
      status += "<a name='showLogEntries' href='#' data='" + project.getProjectId() + "'>" + icon + "</a>";
    } else {
      status += icon;
    }
  }

  row[0] = formattedProjectId;
  var date = project.getCreationDate();
  if (date === undefined) {
    date = "N/A";
  } else {
    date = date.split(" ")[0];
  }
  row[1] = date;
  row[2] = project.getOwner();
  if (row[2] === undefined) {
    row[2] = 'N/A';
  }
  row[3] = project.getName();
  row[4] = disease;
  row[5] = organism;
  row[6] = status;

  var disabledEdit = " disabled ";
  var disabledRemove = " disabled ";
  if ((user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), project.getProjectId()))
    && (status.indexOf("Ok") === 0 || status.indexOf("Failure") === 0)) {
    disabledEdit = "";
  }

  if ((user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), project.getProjectId())))
    && (status.indexOf("Ok") === 0 || status.indexOf("Failure") === 0)) {
    disabledRemove = "";
  }

  row[7] = "<button name='showEditDialog' data='" + project.getProjectId() + "'" + disabledEdit + "><i class='fa fa-edit'></i></button>";

  if (self.getConfiguration().getOption(ConfigurationType.DEFAULT_MAP).getValue() === projectId) {
    disabledRemove = " disabled ";
  }
  row[8] = "<button name='removeProject' data='" + project.getProjectId() + "'" + disabledRemove + "><i class='fa fa-trash-alt'></button>";

  return row;
};

/**
 *
 * @param {Annotation} annotation
 * @returns {string}
 */
MapsAdminPanel.prototype.getHtmlStringLink = function (annotation) {
  var self = this;
  if (annotation !== undefined && annotation !== null) {
    var link = self.getGuiUtils().createAnnotationLink(annotation, true);
    var tmp = document.createElement("div");
    tmp.appendChild(link);
    return tmp.innerHTML;
  } else {
    return "N/A";
  }

};

/**
 *
 * @param {Project[]} projects
 * @returns {Promise}
 */
MapsAdminPanel.prototype.setProjects = function (projects) {
  var self = this;

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var requireUpdate = false;
    var dataTable = $("[name='projectsTable']", self.getElement()).DataTable();
    var data = [];
    var page = dataTable.page();

    for (var i = 0; i < projects.length; i++) {
      var project = projects[i];
      var rowData = self.projectToTableRow(project, undefined, user);
      self.addUpdateListener(project);
      data.push(rowData);
      if (project.getStatus().toLowerCase() !== "ok" && project.getStatus().toLowerCase() !== "failure") {
        requireUpdate = true;
      }
    }
    //it should be simplified, but I couldn't make it work
    dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);

    if (requireUpdate) {
      setTimeout(function () {
        logger.debug("Projects auto refresh");
        return self.onRefreshClicked();
      }, MapsAdminPanel.AUTO_REFRESH_TIME);
    }
  });
};

/**
 *
 * @param {Project} project
 */
MapsAdminPanel.prototype.addUpdateListener = function (project) {
  var self = this;

  var listenerName = "PROJECT_LIST_LISTENER";
  var listeners = project.getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      project.removeListener("onreload", listeners[i]);
    }
  }
  var listener = function () {
    return self.getServerConnector().getLoggedUser().then(function (user) {
      var dataTable = $($("[name='projectsTable']", self.getElement())[0]).DataTable();

      var length = dataTable.data().length;
      for (var i = 0; i < length; i++) {
        var row = dataTable.row(i);
        var data = row.data();
        if (data[0].indexOf(">" + project.getProjectId() + "<") >= 0) {
          self.projectToTableRow(project, data, user);
          var page = dataTable.page();
          row.data(data).draw();
          dataTable.page(page).draw(false);
        }
      }
    });
  };
  listener.listenerName = listenerName;
  project.addListener("onreload", listener);
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  var dialog = self._addDialog;
  if (dialog === undefined) {
    dialog = new AddProjectDialog({
      element: Functions.createElement({
        type: "div"
      }),
      customMap: null,
      configuration: self.getConfiguration(),
      serverConnector: self.getServerConnector()
    });
    self._addDialog = dialog;
    dialog.addListener("onProjectAdd", function () {
      return self.onRefreshClicked()
    });
    return dialog.init().then(function () {
      return dialog.open();
    });
  } else {
    return dialog.clear().then(function () {
      return dialog.open();
    })
  }
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.destroy = function () {
  var promises = [];
  var self = this;
  var dialog = self._addDialog;
  if (dialog !== undefined) {
    promises.push(dialog.destroy());
  }
  var table = $("[name='projectsTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    promises.push($(table).DataTable().destroy());
  }

  var key;
  for (key in self._logDialogs) {
    if (self._logDialogs.hasOwnProperty(key)) {
      promises.push(self._logDialogs[key].destroy());
    }
  }
  for (key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      promises.push(self._dialogs[key].destroy());
    }
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return ServerConnector.getProjects(true).then(function (projects) {
    return self.setProjects(projects);
  }).then(function () {
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    var curatorPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var adminPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(curatorPrivilege) || user.hasPrivilege(adminPrivilege)) {
      return ServerConnector.getUsers(true);
    } else {
      return Promise.resolve();
    }
  });
};

/**
 *
 * @param {Project} project
 * @returns {*}
 */
MapsAdminPanel.prototype.getDialog = function (project) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var dialog = self._dialogs[project.getProjectId()];
  if (dialog === undefined) {
    dialog = new EditProjectDialog({
      element: Functions.createElement({
        type: "div"
      }),
      project: project,
      configuration: self.getConfiguration(),
      customMap: null,
      serverConnector: self.getServerConnector()
    });
    self._dialogs[project.getProjectId()] = dialog;
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return dialog.refresh().then(function () {
      return Promise.resolve(dialog);
    });
  }
};

/**
 *
 * @param {string} projectId
 * @returns {Promise<LogListDialog>}
 */
MapsAdminPanel.prototype.getLogDialog = function (projectId) {
  var self = this;
  if (self._logDialogs === undefined) {
    self._logDialogs = [];
  }
  var dialog = self._logDialogs[projectId];
  if (dialog === undefined) {
    dialog = new LogListDialog({
      element: Functions.createElement({
        type: "div"
      }),
      configuration: self.getConfiguration(),
      projectId: projectId,
      serverConnector: self.getServerConnector(),
      customMap: null
    });
    self._logDialogs[projectId] = dialog;
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise}
 */
MapsAdminPanel.prototype.showEditDialog = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return ServerConnector.getProject(id).then(function (project) {
    return self.getDialog(project);
  }).then(function (dialog) {
    dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise<LogListDialog>}
 */
MapsAdminPanel.prototype.showLogs = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getLogDialog(id).then(function (dialog) {
    return dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise}
 */
MapsAdminPanel.prototype.removeProject = function (id) {
  var self = this;
  return ServerConnector.removeProject(id).then(function () {
    return self.onRefreshClicked();
  });
};

module.exports = MapsAdminPanel;
