"use strict";

var types = ["IMAGE", "OVERLAY", "MAP", "GLYPH"];

/**
 *
 * @param {Object} params
 * @param {string} params.type
 * @param {string} params.filename
 * @param {Object} params.data
 * @constructor
 */
function ZipEntry(params) {
  var self = this;
  self.setType(params.type);
  self.setFilename(params.filename);
  self.setData(params.data);
}

/**
 *
 * @param {string} type
 */
ZipEntry.prototype.setType = function (type) {
  if (types.indexOf(type) === -1) {
    throw new Error("Unknown ZipEntryType: " + type + ".")
  }
  this._type = type;
};

/**
 *
 * @returns {string}
 */
ZipEntry.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} filename
 */
ZipEntry.prototype.setFilename = function (filename) {
  this._filename = filename;
};

/**
 *
 * @returns {string}
 */
ZipEntry.prototype.getFilename = function () {
  return this._filename;
};

/**
 *
 * @param {Object} data
 */
ZipEntry.prototype.setData = function (data) {
  this._data = data;
};

/**
 *
 * @returns {{type: MapType, root:boolean, mapping: boolean}}
 */
ZipEntry.prototype.getData = function () {
  return this._data;
};

module.exports = ZipEntry;
