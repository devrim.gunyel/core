"use strict";

/* exported logger */
var Promise = require("bluebird");
var $ = require('jquery');

var AbstractGuiElement = require('../AbstractGuiElement');
var AddOverlayDialog = require('../AddOverlayDialog');
var Annotation = require('../../map/data/Annotation');
var CommentsTab = require('./CommentsAdminPanel');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ValidationError = require("../../ValidationError");
var HttpStatus = require('http-status-codes');
var NetworkError = require('../../NetworkError');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();
var xss = require('xss');

/**
 *
 * @param params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends {AbstractGuiElement}
 */
function EditProjectDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  /**
   *
   * @type {string}
   * @private
   */
  self._title = self.getProject().getProjectId();

  guiUtils.setConfiguration(params.configuration);
  self.setConfiguration(params.configuration);
  $(self.getElement()).addClass("minerva-edit-project-dialog");
  $(self.getElement()).css({overflow: "hidden"});
  self.createGui();

  /**
   @name EditProjectDialog#_overlayById
   @type DataOverlay[]
   @private
   */
  self._overlayById = [];

  /**
   @name EditProjectDialog#_mapsById
   @type MapModel[]
   @private
   */
  self._mapsById = [];

  /**
   @name EditProjectDialog#_userByLogin
   @type User[]
   @private
   */
  self._userByLogin = [];

}

EditProjectDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditProjectDialog.prototype.constructor = EditProjectDialog;

/**
 *
 */
EditProjectDialog.prototype.createGui = function () {
  var self = this;

  guiUtils.initTabContent(self);

  guiUtils.addTab(self, {
    name: "GENERAL",
    content: self.createGeneralTabContent()
  });

  guiUtils.addTab(self, {
    name: "OVERLAYS",
    content: self.createOverlaysTabContent()
  });
  guiUtils.addTab(self, {
    name: "MAPS",
    content: self.createMapsTabContent()
  });
  guiUtils.addTab(self, {
    name: "USERS",
    content: self.createUsersTabContent()
  });

  guiUtils.addTab(self, {
    name: "COMMENTS",
    panelClass: CommentsTab
  });
  self.setCommentsTab(self._panels[0]);

};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createGeneralTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-general-tab"
  });

  var table = Functions.createElement({
    type: "div",
    style: "display:table"
  });
  result.appendChild(table);

  var projectIdRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(projectIdRow);
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "ProjectId"
  }));
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    name: "projectId"
  }));

  var mapCanvasTypeRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(mapCanvasTypeRow);
  mapCanvasTypeRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Map canvas type: "
  }));
  mapCanvasTypeRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: Functions.createElement({
      type: "select",
      name: "project-map-canvas-type",
      onchange: function () {
        var select = this;
        var licenceCheckbox = $("[name='project-google-maps-license']", self.getElement())[0];
        var licenceDiv = licenceCheckbox.parentElement.parentElement;

        var update = true;
        var mapType;
        $("option:selected", select).each(function () {
          mapType = $(this).val();
          if (mapType === "GOOGLE_MAPS_API") {
            update = false;
            $(licenceDiv).css("display", "table-row");
          } else {
            $(licenceDiv).css("display", "none");
          }
          self.setLicenseAccepted(false);
        });
        if (update) {
          var project = self.getProject();
          project.setMapCanvasType(self.getMapCanvasTypeId());
          return self.updateProject(project);
        }
      }
    })
  }));

  var googleLicenseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(googleLicenseRow);
  googleLicenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "I agree to <a target='_license' href='https://developers.google.com/maps/terms'>Google Maps APIs Terms of Service</a>: ",
    xss: false
  }));
  googleLicenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: Functions.createElement({
      type: "input",
      inputType: "checkbox",
      name: "project-google-maps-license",
      onchange: function () {
        var select = $("[name='project-map-canvas-type']", self.getElement())[0];
        var licenceCheckbox = $("[name='project-google-maps-license']", self.getElement())[0];

        var update = true;
        var mapType;
        $("option:selected", select).each(function () {
          mapType = $(this).val();
          if (mapType === "GOOGLE_MAPS_API") {
            update = self.isLicenseAccepted();
          }
        });
        if (self.isLicenseAccepted()) {
          $(licenceCheckbox).prop("disabled", true);
        }
        if (update) {
          var project = self.getProject();
          project.setMapCanvasType(self.getMapCanvasTypeId());
          return self.updateProject(project);
        }
      }
    })
  }));

  var nameRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(nameRow);
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Name"
  }));
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectName'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setName($("[name='projectName']", this).val());
      return self.updateProject(project);
    }
  }));

  var versionRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(versionRow);
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Version"
  }));
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectVersion'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setVersion($("[name='projectVersion']", this).val());
      return self.updateProject(project);
    }
  }));

  var diseaseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(diseaseRow);
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Disease"
  }));
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectDisease'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setDisease(prepareMiriamData("MESH_2012", $("[name='projectDisease']", this).val()));
      return self.updateProject(project);
    }
  }));

  var organismRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(organismRow);
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Organism"
  }));
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectOrganism'/>",
    xss: false,
    onchange: function () {
      var id = $("[name='projectOrganism']", this).val();
      var project = self.getProject();
      GuiConnector.showProcessing();
      return self.getServerConnector().getTaxonomy({id: id}).catch(function () {
        if (id !== "") {
          var newId = "";
          if (project.getOrganism() !== undefined) {
            newId = xss(project.getOrganism().getResource());
          }
          $("[name='projectOrganism']", this).val(newId);
          $("[name='projectOrganism']", this).focus();
          throw new ValidationError("Invalid taxonomy id: " + id);
        }
      }).then(function () {
        project.setOrganism(prepareMiriamData("TAXONOMY", id));
        return self.updateProject(project);
      }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
    }
  }));

  var emailRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(emailRow);
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Notify email"
  }));
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectNotifyEmail'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setNotifyEmail($("[name='projectNotifyEmail']", this).val());
      return self.updateProject(project);
    }
  }));

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createMapsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-maps-tab"
  });
  result.appendChild(self._createMapsTable());
  return result;
};

EditProjectDialog.prototype._createMapsTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var mapsTable = Functions.createElement({
    type: "table",
    name: "mapsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(mapsTable);

  $(mapsTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "map-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Default center x',
      orderable: false
    }, {
      title: 'Default center y',
      orderable: false
    }, {
      title: 'Default zoom level',
      orderable: false
    }]
  });

  $(mapsTable).on("change", "[name='defaultCenterX']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultCenterX(parseInt($(this).val()));
    return self.updateMap(map);
  });

  $(mapsTable).on("change", "[name='defaultCenterY']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultCenterY(parseInt($(this).val()));
    return self.updateMap(map);
  });

  $(mapsTable).on("change", "[name='defaultZoomLevel']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultZoomLevel(parseInt($(this).val()));
    return self.updateMap(map);
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createOverlaysTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-overlays-tab"
  });
  result.appendChild(self._createOverlayTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
EditProjectDialog.prototype._createOverlayTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var overlaysTable = Functions.createElement({
    type: "table",
    name: "overlaysTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(overlaysTable);

  // noinspection JSUnusedGlobalSymbols
  $(overlaysTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "overlay-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }, {
      title: 'Public',
      orderable: false
    }, {
      title: 'Default',
      orderable: false
    }, {
      title: 'Owner',
      orderable: false
    }, {
      title: 'Data',
      orderable: false
    }, {
      title: 'Remove',
      orderable: false
    }],
    columnDefs: [
      {
        orderDataType: "dom-input",
        type: "string",
        targets: [1, 2]
      }
    ],
    dom: '<"minerva-datatable-toolbar">frtip',
    initComplete: function () {
      $("div.minerva-datatable-toolbar", $(result)).html('<button name="addOverlay">Add overlay</button>');
    }

  });

  $(overlaysTable).on("click", "[name='removeOverlay']", function () {
    var button = this;
    var overlayId = parseInt($(button).attr("data"));
    var overlay = self._overlayById[overlayId];
    var confirmationMessage = "Do you want to delete overlay: " + overlay.getName() + "?";
    if (!overlay.getInputDataAvailable()) {
      confirmationMessage += "<br/><span style='color:red;font-weight:bold'>Warning: This contains one of the map's " +
        "diagrams and is required<br/>" +
        "for map display. Removing it cannot be undone.</span>"
    }
    return GuiConnector.showConfirmationDialog({
      message: confirmationMessage
    }).then(function (confirmation) {
      if (confirmation) {
        $(button).attr("disabled", true);
        return self.removeOverlay(overlayId).catch(GuiConnector.alert);
      }
    });
  });

  $(overlaysTable).on("change", "[name='overlayName']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setName($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='overlayDescription']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setDescription($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='publicOverlay']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setPublicOverlay($(this).prop('checked'));
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='defaultOverlay']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setDefaultOverlay($(this).prop('checked'));
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='creator']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    var creator = $(this).val();
    if (creator === "") {
      creator = undefined;
    }
    //put it on the bottom of ordered list of data overlays for given user
    var order = 1;
    for (var key in self._overlayById) {
      if (self._overlayById.hasOwnProperty(key)) {
        var existingOverlay = self._overlayById[key];
        if (existingOverlay.getCreator() === creator) {
          if (existingOverlay.getId() !== overlayId) {
            order = Math.max(order, self._overlayById[key].getOrder() + 1);
          } else {
            order = Math.max(order, self._overlayById[key].getOrder());
          }
        }
      }
    }
    overlay.setOrder(order);

    overlay.setCreator($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("click", "[name='downloadSource']", function () {
    var button = this;
    return self.getServerConnector().getOverlaySourceDownloadUrl({
      overlayId: $(button).attr("data"),
      projectId: self.getProject().getProjectId()
    }).then(function (url) {
      return self.downloadFile(url);
    }).then(null, GuiConnector.alert);
  });

  $(result).on("click", "[name='addOverlay']", function () {
    return self.openAddOverlayDialog();
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createUsersTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-users-tab",
    style: "margin-top:10px;"
  });

  var usersTable = Functions.createElement({
    type: "table",
    name: "usersTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(usersTable);

  $(usersTable).on("change", "[name='privilege']", function () {
    var type = $(this).attr("data").split(",")[0];
    var login = $(this).attr("data").split(",")[1];
    if ($(this).prop("checked")) {
      if (type === PrivilegeType.WRITE_PROJECT) {
        var readCheckbox = $("[data='" + PrivilegeType.READ_PROJECT + "," + login + "']", usersTable);
        if (!readCheckbox.is(":checked")) {
          readCheckbox.click();
        }
      }
      return self.grantPrivilege(self._userByLogin[login], type, self.getProject().getProjectId());
    } else {
      if (type === PrivilegeType.READ_PROJECT) {
        var writeCheckbox = $("[data='" + PrivilegeType.WRITE_PROJECT + "," + login + "']", usersTable);
        if (writeCheckbox.is(":checked")) {
          writeCheckbox.click();
        }
      }
      return self.revokePrivilege(self._userByLogin[login], type, self.getProject().getProjectId());
    }
  });

  return result;
};

/**
 *
 * @returns {{title: string, privilegeType: PrivilegeType|undefined}[]}
 */
EditProjectDialog.prototype.createUserPrivilegeColumns = function () {
  var self = this;

  if (self._userPrivilegeColumns !== undefined) {
    return self._userPrivilegeColumns;
  }

  var configuration = self.getConfiguration();
  self._userPrivilegeColumns = [{
    title: "Name",
    orderable: true
  }];
  var privilegeTypes = configuration.getPrivilegeTypes();
  for (var i = 0; i < privilegeTypes.length; i++) {
    var type = privilegeTypes[i];
    if (type.getObjectType() === "Project") {
      self._userPrivilegeColumns.push({
        "title": type.getCommonName(),
        privilegeType: type,
        orderable: false
      });
    }
  }
  return self._userPrivilegeColumns;
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.init = function () {
  var self = this;
  self.getProject().addListener("onreload", function () {
    self.projectDataUpdated(self.getProject());
  });
  return self.initUsersTab().then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (loggedUser) {
    var readOnlyAccess = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      !loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());
    if (readOnlyAccess) {
      self._title += " (View only)";
    }
    return self.refresh();
  }).then(function () {
    $(window).trigger('resize');
  });
};

/**
 *
 * @param {Project} project
 */

EditProjectDialog.prototype.projectDataUpdated = function (project) {
  var element = this.getElement();
  $("[name='projectName']", element).val(xss(project.getName()));
  $("[name='projectId']", element).html(xss(project.getProjectId()));
  $("[name='projectVersion']", element).val(xss(project.getVersion()));

  var disease = "";
  if (project.getDisease() !== undefined) {
    disease = xss(project.getDisease().getResource());
  }
  $("[name='projectDisease']", element).val(disease);
  var organism = "";
  if (project.getOrganism() !== undefined) {
    organism = xss(project.getOrganism().getResource());
  }
  $("[name='projectOrganism']", element).val(organism);

  var email = "";
  if (project.getNotifyEmail() !== undefined) {
    email = xss(project.getNotifyEmail());
  }
  $("[name='projectNotifyEmail']", element).val(email);
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refresh = function () {
  var self = this;
  var project = self.getProject();

  self.projectDataUpdated(project);


  return self.refreshUsers().then(function () {
    return self.refreshMaps();
  }).then(function () {
    return self.refreshOverlays();
  }).then(function () {
    return self.refreshComments();
  }).then(function () {
    var configuration = self.getConfiguration();
    var mapCanvasTypes = configuration.getMapCanvasTypes();
    var select = $("[name='project-map-canvas-type']", self.getElement())[0];
    $(select).empty();
    var licenceCheckbox = $("[name='project-google-maps-license']", self.getElement())[0];
    var licenceDiv = licenceCheckbox.parentElement.parentElement;

    for (var i = 0; i < mapCanvasTypes.length; i++) {
      var mapCanvasType = mapCanvasTypes[i];
      var option = Functions.createElement({
        type: "option",
        content: mapCanvasType.name,
        value: mapCanvasType.id
      });
      if (mapCanvasType.id === self.getProject().getMapCanvasType()) {
        option.selected = true;
        if (mapCanvasType.id === "GOOGLE_MAPS_API") {
          $(licenceDiv).css("display", "table-row");
          self.setLicenseAccepted(true);
        } else {
          $(licenceDiv).css("display", "none");
          self.setLicenseAccepted(false);
        }
      }
      select.appendChild(option);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.initUsersTab = function () {
  var self = this;

  var usersTable = $("[name=usersTable]", self.getElement())[0];

  var columns = self.createUserPrivilegeColumns();
  $(usersTable).DataTable({
    columns: columns,
    ordering: true
  });
  return Promise.resolve();
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshOverlays = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    var curatorPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var adminPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(curatorPrivilege) || user.hasPrivilege(adminPrivilege)) {
      return self.getServerConnector().getOverlays({
        projectId: self.getProject().getProjectId()
      }).then(function (overlays) {
        return self.setOverlays(overlays);
      });
    } else {
      guiUtils.disableTab($(".minerva-project-overlays-tab", self.getElement())[0], "You have no privileges to manage users data");
    }
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshMaps = function () {
  var self = this;
  return self.getServerConnector().getModels(self.getProject().getProjectId()).then(function (maps) {
    return self.setMaps(maps);
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshUsers = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (loggedUser) {
    var isAdmin = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
    var isCurator = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());

    //we need to refresh users as well because of privileges
    if (isAdmin || isCurator) {
      return self.getServerConnector().getUsers(true).then(function (users) {
        return self.setUsers(users);
      });
    } else {
      guiUtils.disableTab($(".minerva-project-users-tab", self.getElement())[0], "You have no privileges to manage users data");
    }
  });
};

/**
 *
 * @param {DataOverlay[]} overlays
 * @returns {Promise}
 */
EditProjectDialog.prototype.setOverlays = function (overlays) {
  var self = this;
  self._overlayById = [];
  return ServerConnector.getUsers().then(function (users) {
    var dataTable = $($("[name='overlaysTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < overlays.length; i++) {
      var overlay = overlays[i];
      self._overlayById[overlay.getId()] = overlay;
      var rowData = self.overlayToTableRow(overlay, users);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {MapModel[]} maps
 * @return {Promise<T>}
 */
EditProjectDialog.prototype.setMaps = function (maps) {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    self._mapsById = [];
    var dataTable = $($("[name='mapsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < maps.length; i++) {
      var map = maps[i];
      self._mapsById[map.getId()] = map;
      var rowData = self.mapToTableRow(map, user);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {User[]} users
 */
EditProjectDialog.prototype.setUsers = function (users) {
  var self = this;
  self._userByLogin = [];
  var columns = self.createUserPrivilegeColumns();
  var dataTable = $($("[name='usersTable']", self.getElement())[0]).DataTable();
  var data = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    self._userByLogin[user.getLogin()] = user;
    var rowData = self.userToTableRow(user, columns);
    data.push(rowData);
  }
  dataTable.clear().rows.add(data).draw();
};

/**
 *
 * @param {User} user
 * @param {Array} columns
 * @returns {Array}
 */
EditProjectDialog.prototype.userToTableRow = function (user, columns) {
  var self = this;
  var row = [];
  var login = user.getLogin();

  var isAdmin = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
  var disabled = "";
  if (isAdmin) {
    disabled = " disabled "
  }

  row[0] = user.getName() + " " + user.getSurname() + " (" + login + ")";
  for (var i = 1; i < columns.length; i++) {
    var column = columns[i];
    if (column.privilegeType !== undefined) {
      if (column.privilegeType.getValueType() === "boolean") {
        var checked = '';
        if (user.hasPrivilege(column.privilegeType, self.getProject().getProjectId())) {
          checked = 'checked';
        }
        row[i] = "<input type='checkbox' name='privilege" + "' data='" + column.privilegeType.getName() + "," + login + "' "
          + checked + disabled + "/>";
        if (isAdmin) {
          row[i] += " ADMIN";
        }
      } else {
        throw new Error("Unsupported type: " + column.privilegeType.getValueType());
      }
    }
  }
  return row;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {User[]} users
 * @returns {Array}
 */
EditProjectDialog.prototype.overlayToTableRow = function (overlay, users) {
  var self = this;

  var loggedUser = null, i;
  for (i = 0; i < users.length; i++) {

    if (users[i].getLogin() === self.getServerConnector().getSessionData().getLogin()) {
      loggedUser = users[i];
    }
  }

  var disabled = " disabled ";
  var isAdmin = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
  var isCurator = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
    loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());
  if (isAdmin || isCurator) {
    disabled = "";
  }

  var row = [];
  var id = overlay.getId();
  var creatorSelect;

  creatorSelect = "<select data='" + id + "' name='creator'" + disabled + ">";

  var selected = "";
  if (overlay.getCreator() === "") {
    selected = "selected";
  }
  creatorSelect += "<option value='' " + selected + ">---</option>";
  for (i = 0; i < users.length; i++) {
    selected = "";
    var user = users[i];
    if (overlay.getCreator() === user.getLogin()) {
      selected = "selected";
    }

    creatorSelect += "<option value='" + user.getLogin() + "' " + selected + ">" + user.getLogin() + "("
      + user.getName() + " " + user.getSurname() + ")</option>";

    if (user.getLogin() === self.getServerConnector().getSessionData().getLogin()) {
      loggedUser = user;
    }
  }
  creatorSelect += "</select>";

  var checked = '';
  if (overlay.getPublicOverlay()) {
    checked = "checked";
  }
  var publicOverlayCheckbox = "<input type='checkbox' data='" + id + "' name='publicOverlay' " + checked + disabled + "/>";

  checked = '';
  if (overlay.isDefaultOverlay()) {
    checked = "checked";
  }
  var defaultOverlayCheckbox = "<input type='checkbox' data='" + id + "' name='defaultOverlay' " + checked + disabled + "/>";

  var downloadSourceButton;
  if (overlay.getInputDataAvailable()) {
    downloadSourceButton = "<button name='downloadSource' data='" + id + "'" + disabled + ">"
      + "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>" + "</button>";
  } else {
    downloadSourceButton = "N/A";
  }

  row[0] = id;
  row[1] = "<input data='" + id + "' name='overlayName' value='" + overlay.getName() + "'" + disabled + "/>";
  row[2] = "<input data='" + id + "' name='overlayDescription' value='" + overlay.getDescription() + "'" + disabled + "/>";
  row[3] = publicOverlayCheckbox;
  row[4] = defaultOverlayCheckbox;
  row[5] = creatorSelect;
  row[6] = downloadSourceButton;


  row[7] = "<button name='removeOverlay' data='" + id + "'" + disabled + "><i class='fa fa-trash-alt'></button>";

  return row;
};

/**
 *
 * @param value
 * @returns {*}
 */
function getValueOrEmpty(value) {
  if (value !== undefined && value !== null) {
    return value;
  } else {
    return "";
  }
}

/**
 *
 * @param {MapModel} map
 * @param {User} user
 * @returns {Array}
 */
EditProjectDialog.prototype.mapToTableRow = function (map, user) {
  var self = this;

  var canWrite = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());

  var disabled = " disabled ";

  if (canWrite) {
    disabled = '';
  }

  var row = [];
  var id = map.getId();
  var centerX = getValueOrEmpty(map.getDefaultCenterX());
  var centerY = getValueOrEmpty(map.getDefaultCenterY());
  var zoomLevel = getValueOrEmpty(map.getDefaultZoomLevel());
  row[0] = id;
  row[1] = map.getName();
  row[2] = "<input name='defaultCenterX' data='" + id + "' value='" + centerX + "'" + disabled + "/>";
  row[3] = "<input name='defaultCenterY' data='" + id + "' value='" + centerY + "'" + disabled + "/>";

  var selected = "";
  if ('' === zoomLevel) {
    selected = " selected ";
  }

  var zoomLevelSelect = "<select name='defaultZoomLevel' data='" + id + "' " + disabled + ">" +
    "<option value=''" + selected + ">---</option>";
  for (var i = map.getMinZoom(); i <= map.getMaxZoom(); i++) {
    selected = "";
    if (i === zoomLevel) {
      selected = " selected ";
    }
    zoomLevelSelect += "<option value='" + i + "' " + selected + ">" + (i - map.getMinZoom()) + "</option>";
  }
  zoomLevelSelect += "</select>";
  row[4] = zoomLevelSelect;

  return row;
};

/**
 *
 */
EditProjectDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-edit-project-dialog',
      title: self._title,
      width: window.innerWidth * 0.75,
      height: window.innerHeight / 2
    });
    $(".ui-dialog-titlebar-close", $(div).dialog().siblings('.ui-dialog-titlebar')).on("mousedown", function () {
      //we need to close dialog on mouse down, because processing modal dialogs prevents the click to finish happening
      $(div).dialog("close");
    });
  }
  $(div).dialog("open");
};

/**
 *
 * @param {string} type
 * @param {?null|string} resource
 * @returns {Annotation|null}
 */
var prepareMiriamData = function (type, resource) {
  if (resource === "" || resource === undefined || resource === null) {
    return null;
  } else {
    return new Annotation({
      type: type,
      resource: resource
    });
  }
};

/**
 *
 * @param {Project} project
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateProject = function (project) {
  var self = this;

  GuiConnector.showProcessing();
  return self.getServerConnector().updateProject(project).catch(function (error) {
    if ((error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST)) {
      GuiConnector.alert(error.content.reason);
    } else {
      GuiConnector.alert(error);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";

  var mapCanvasTypeId = self.getMapCanvasTypeId();
  if (mapCanvasTypeId === "GOOGLE_MAPS_API") {
    if (!self.isLicenseAccepted()) {
      isValid = false;
      error += "<li>Please accept Google Maps License when choosing Google Maps API engine</li>";
    }
  }

  if (isValid) {
    return Promise.resolve(true);
  } else {
    return Promise.reject(new ValidationError(error));
  }
};

/**
 *
 */
EditProjectDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 * @param {DataOverlay} overlay
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateOverlay = function (overlay) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateOverlay(overlay, self.getProject()).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {MapModel} map
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateMap = function (map) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateModel({
    projectId: self.getProject().getProjectId(),
    model: map
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {User} user
 * @param {string} privilegeType
 * @param {string} projectId
 * @returns {Promise}
 */
EditProjectDialog.prototype.grantPrivilege = function (user, privilegeType, projectId) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().grantProjectPrivilege({
    projectId: projectId,
    user: user,
    privilegeType: privilegeType
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {User} user
 * @param {string} privilegeType
 * @param {string} projectId
 * @returns {Promise}
 */
EditProjectDialog.prototype.revokePrivilege = function (user, privilegeType, projectId) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().revokeProjectPrivilege({
    projectId: projectId,
    user: user,
    privilegeType: privilegeType
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};


/**
 *
 * @param {number} overlayId
 * @returns {Promise}
 */
EditProjectDialog.prototype.removeOverlay = function (overlayId) {
  var self = this;
  return self.getServerConnector().removeOverlay({
    overlayId: overlayId,
    projectId: self.getProject().getProjectId()
  }).then(function () {
    return self.refreshOverlays();
  });
};

/**
 *
 * @returns {Promise<AddOverlayDialog>}
 */
EditProjectDialog.prototype.openAddOverlayDialog = function () {
  var self = this;
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
  }
  self._addOverlayDialog = new AddOverlayDialog({
    project: self.getProject(),
    customMap: null,
    element: document.createElement("div"),
    configuration: self.getConfiguration(),
    serverConnector: self.getServerConnector()
  });
  self._addOverlayDialog.addListener("onAddOverlay", function () {
    return self.refreshOverlays();
  });
  return self._addOverlayDialog.init().then(function () {
    return self._addOverlayDialog.open();
  }).then(function () {
    return self._addOverlayDialog;
  });
};

/**
 *
 * @param {CommentsTab} commentsTab
 */
EditProjectDialog.prototype.setCommentsTab = function (commentsTab) {
  this._commentsTab = commentsTab;
};

/**
 *
 * @returns {CommentsTab}
 */
EditProjectDialog.prototype.getCommentsTab = function () {
  return this._commentsTab;
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshComments = function () {
  return this.getCommentsTab().refreshComments();
};


/**
 *
 */
EditProjectDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  var usersTable = $("[name=usersTable]", self.getElement())[0];
  var overlaysTable = $("[name=overlaysTable]", self.getElement())[0];
  var mapsTable = $("[name=mapsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(usersTable)) {
    $(usersTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(overlaysTable)) {
    $(overlaysTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(mapsTable)) {
    $(mapsTable).DataTable().destroy();
  }

  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
    delete self._addOverlayDialog;
  }
  if (this.getCommentsTab() !== undefined) {
    this.getCommentsTab().destroy();
  }
};

/**
 *
 * @returns {string}
 */
EditProjectDialog.prototype.getMapCanvasTypeId = function () {
  return $("[name='project-map-canvas-type']", this.getElement()).val();
};

/**
 *
 * @param {boolean} isLicenseAccepted
 */
EditProjectDialog.prototype.setLicenseAccepted = function (isLicenseAccepted) {
  var licenceCheckbox = $("[name='project-google-maps-license']", this.getElement());
  licenceCheckbox.prop("checked", isLicenseAccepted);
  licenceCheckbox.prop("disabled", isLicenseAccepted);
};

/**
 *
 * @returns {boolean}
 */
EditProjectDialog.prototype.isLicenseAccepted = function () {
  return $("[name='project-google-maps-license']", this.getElement()).is(":checked");
};

module.exports = EditProjectDialog;
