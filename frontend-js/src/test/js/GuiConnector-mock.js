"use strict";

/* exported logger */

var logger = require('./logger');

var OriginalGuiConnector = require('../../main/js/GuiConnector');

var GuiConnectorMock = OriginalGuiConnector;

GuiConnectorMock.alert = function (error) {
  if (error instanceof Error) {
    throw error;
  } else {
    throw new Error(error);
  }
};

GuiConnectorMock.showProcessing = function (message) {
  if (message === undefined) {
    message = "";
  }
  logger.debug("[PROCESSING STARTED] " + message);
};

GuiConnectorMock.hideProcessing = function () {
  logger.debug("[PROCESSING STOPPED]");
};

module.exports = GuiConnectorMock;
