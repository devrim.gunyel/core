"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var CustomMap = require('../../../../main/js/map/CustomMap');
var InvalidArgumentError = require('../../../../main/js/InvalidArgumentError');
var UserDbOverlay = require('../../../../main/js/map/overlay/UserDbOverlay');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;

describe('UserDbOverlay', function () {
  it("constructor 1", function () {
    var map = helper.createCustomMap();
    var oc = new UserDbOverlay({
      map: map,
      name: 'user'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'user');

    assert.equal(logger.getWarnings.length, 0);
  });

  it("clear", function () {
    var overlay;
    return ServerConnector.getProject().then(function (project) {
      var options = helper.createCustomMapOptions(project);
      var map = new CustomMap(options);
      overlay = new UserDbOverlay({
        map: map,
        name: 'user'
      });
      return overlay.addMarker({
        element: {
          id: 329171,
          modelId: 15781,
          type: "ALIAS"
        },
        options: {
          type: "SURFACE"
        }
      });
    }).then(function () {
      return overlay.getIdentifiedElements();
    }).then(function (elements) {
      assert.equal(elements.length, 1);
      return overlay.clear();
    }).then(function () {
      return overlay.getIdentifiedElements();
    }).then(function (elements) {
      assert.equal(elements.length, 0);
    });
  });

  describe("addMarker", function () {
    it("add non existing object", function () {
      var overlay;
      return ServerConnector.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);
        var map = new CustomMap(options);
        overlay = new UserDbOverlay({
          map: map,
          name: 'user'
        });
        return overlay.addMarker({
          element: {
            id: -1,
            modelId: 15781,
            type: "ALIAS"
          },
          options: {
            type: "SURFACE"
          }
        });
      }).then(function () {
        assert.ok(false, "Exception expected");
      }, function (error) {
        assert.ok(error instanceof InvalidArgumentError);
      });
    });

    it("add to non existing map", function () {
      var overlay;
      return ServerConnector.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);
        var map = new CustomMap(options);
        overlay = new UserDbOverlay({
          map: map,
          name: 'user'
        });
        return overlay.addMarker({
          element: {
            id: 329171,
            modelId: -1,
            type: "ALIAS"
          },
          options: {
            type: "SURFACE"
          }
        });
      }).then(function () {
        assert.ok(false, "Exception expected");
      }, function (error) {
        assert.ok(error instanceof InvalidArgumentError);
      });
    });
  });

});
