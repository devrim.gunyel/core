"use strict";

require("../../mocha-config");
var ReactionSurface = require('../../../../main/js/map/surface/ReactionSurface');
// noinspection JSUnusedLocalSymbols
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('ReactionSurface', function () {
  describe("init", function () {
    it("default", function () {
      var map = helper.createAbstractCustomMap();
      var reaction = helper.createReaction(map);

      var reactionOverlay = new ReactionSurface({
        reaction: reaction,
        map: map,
        customized: false
      });
      return reactionOverlay.init().then(function () {
        assert.ok(reactionOverlay.getMapCanvasObjects().length > 0);
        assert.ok(reactionOverlay.getColor());
        assert.ok(reactionOverlay.getWidth());
        assert.ok(reactionOverlay.getBounds());
        assert.ok(reactionOverlay.getBioEntity());
        assert.ok(reactionOverlay.getCustomMap());
        assert.ok(reactionOverlay.getId());
        assert.ok(typeof reactionOverlay.getColor() === "string");
      });

    });
    it("with data overlay", function () {
      var map = helper.createAbstractCustomMap();
      var reaction = helper.createReaction(map);
      var layoutReaction = helper.createLayoutReaction(reaction);

      var reactionOverlay = new ReactionSurface({
        layoutReaction: layoutReaction,
        reaction: reaction,
        map: map,
        customized: false
      });
      return reactionOverlay.init().then(function () {
        assert.ok(reactionOverlay.getMapCanvasObjects().length > 0);
        assert.ok(reactionOverlay.getColor());
        assert.ok(reactionOverlay.getWidth());
        assert.ok(reactionOverlay.getBounds());
        assert.ok(reactionOverlay.getBioEntity());
        assert.ok(reactionOverlay.getCustomMap());
        assert.ok(reactionOverlay.getId());
        assert.ok(typeof reactionOverlay.getColor() === "string");
      });
    });
  });

  it("show", function () {
    var map = helper.createCustomMap();
    var reaction = helper.createReaction(map);
    var layoutReaction = helper.createLayoutReaction(reaction);

    var reactionOverlay = new ReactionSurface({
      overlayData: layoutReaction,
      reaction: reaction,
      map: map,
      customized: false
    });
    return reactionOverlay.init().then(function () {
      return reactionOverlay.show();
    }).then(function () {
      assert.equal(reactionOverlay.isShown(), true);
      return reactionOverlay.hide();
    }).then(function () {
      assert.equal(reactionOverlay.isShown(), false);

      return reactionOverlay.show();
    }).then(function () {
      assert.ok(reactionOverlay.isShown());

      assert.equal(logger.getWarnings().length, 0);

      return reactionOverlay.show();
    }).then(function () {
      assert.ok(reactionOverlay.isShown());
      assert.equal(logger.getWarnings().length, 1);
    });

  });

  it("changedToCustomized", function () {
    var map = helper.createCustomMap();
    var reaction = helper.createReaction(map);
    var layoutReaction = helper.createLayoutReaction(reaction);

    var reactionOverlay = new ReactionSurface({
      overlayData: layoutReaction,
      reaction: reaction,
      map: map,
      customized: false
    });

    reactionOverlay.changedToCustomized();
    assert.ok(reactionOverlay.isCustomized());

  });

  it("createLine", function () {
    var map = helper.createAbstractCustomMap();

    var line = ReactionSurface.createLine({
      start: {
        x: 0,
        y: 10
      },
      end: {
        x: 100,
        y: 100
      }
    }, "#000000", 3, map);

    assert.ok(line);

  });
});
