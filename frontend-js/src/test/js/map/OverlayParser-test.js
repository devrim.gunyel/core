"use strict";

require("../mocha-config.js");

var OverlayParser = require('../../../main/js/map/OverlayParser');
var ServerConnector = require('../ServerConnector-mock');
var chai = require('chai');
var assert = chai.assert;

var TextEncoder = require('text-encoding').TextEncoder;

describe('OverlayParser', function () {
  describe('parse', function () {
    it('simple', function () {
      var parser = new OverlayParser();
      var fileContent = "#NAME=some Name\n#DESCRIPTION=xxx\nname\tvalue\ns1\t1";

      var overlay = parser.parse(fileContent);
      assert.ok(overlay);
      assert.equal(overlay.getDescription(), "xxx");
      assert.equal(overlay.getName(), "some Name");
      assert.ok(overlay.getContent());
    });
    it('Uint8Array', function () {
      var parser = new OverlayParser();
      var fileContent = new TextEncoder("UTF8").encode("#NAME=some Name\n#DESCRIPTION=xxx\nname\tvalue\ns1\t1");

      var overlay = parser.parse(fileContent);
      assert.ok(overlay);
      assert.equal(overlay.getDescription(), "xxx");
      assert.equal(overlay.getName(), "some Name");
      assert.ok(overlay.getContent());
    });
    it('with type', function () {

      return ServerConnector.sendGetRequest("testFiles/overlay/good.txt").then(function (fileContent) {
        var parser = new OverlayParser();
        var overlay = parser.parse(fileContent);
        assert.equal(overlay.getName(), "example name");
        assert.equal(overlay.getDescription(), "layout description");
        assert.equal(overlay.getType(), "GENERIC");
      });
    });
  });
  describe('containsMixedNewLineCharacters', function () {
    it('normal file', function () {
      return ServerConnector.sendGetRequest("testFiles/overlay/good.txt").then(function (fileContent) {
        var parser = new OverlayParser();
        assert.notOk(parser.containsMixedNewLineCharacters(fileContent));
      });
    });
    it('windows/linux mix', function () {
      var content = "line1\n\rline2\n";
      var parser = new OverlayParser();
      assert.ok(parser.containsMixedNewLineCharacters(content));
    });

    it('windows empty lines', function () {
      var content = "line1\n\rline2\n\r\n\rline4";
      var parser = new OverlayParser();
      assert.notOk(parser.containsMixedNewLineCharacters(content));
    });

    it('windows/mac mix', function () {
      var content = "line1\n\rline2\r";
      var parser = new OverlayParser();
      assert.ok(parser.containsMixedNewLineCharacters(content));
    });

    it('mac empty lines', function () {
      var content = "line1\rline2\r\rline4";
      var parser = new OverlayParser();
      assert.notOk(parser.containsMixedNewLineCharacters(content));
    });

    it('windows/mac mix 2', function () {
      var content = "line1\n\r\rline2\n\rline3";
      var parser = new OverlayParser();
      assert.ok(parser.containsMixedNewLineCharacters(content));
    });

    it('linux empty lines', function () {
      var content = "line1\nline2\n\nline4";
      var parser = new OverlayParser();
      assert.notOk(parser.containsMixedNewLineCharacters(content));
    });


  });

});
