"use strict";
require("../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Bounds = require('../../../../main/js/map/canvas/Bounds');
var Point = require('../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('Bounds', function () {
  describe("constructor", function () {
    it("default", function () {
      var bounds = new Bounds();
      assert.ok(bounds);
    });
    it("with single point", function () {
      var p1 = new Point(0, 0);
      var bounds = new Bounds(p1);
      assert.ok(bounds);
    });
    it("with two points", function () {
      var p1 = new Point(0, 0);
      var p2 = new Point(10, 10);
      var bounds = new Bounds(p1, p2);
      assert.ok(bounds);
    });
  });
  it("getTopLeft", function () {
    var p1 = new Point(2, 5);
    var bounds = new Bounds(p1);
    assert.ok(bounds.getTopLeft().x === p1.x);
    assert.ok(bounds.getTopLeft().y === p1.y);
  });

  it("getRightBottom", function () {
    var p1 = new Point(2, 5);
    var bounds = new Bounds(p1);
    assert.ok(bounds.getRightBottom().x === p1.x);
    assert.ok(bounds.getRightBottom().y === p1.y);
  });

  describe("contains", function () {
    it("point inside", function () {
      var p1 = new Point(0, 0);
      var p2 = new Point(10, 10);
      var bounds = new Bounds(p1, p2);
      var inside = new Point(5, 8);

      assert.ok(bounds.contains(inside));
    });
    it("point on border", function () {
      var p1 = new Point(0, 0);
      var p2 = new Point(10, 10);
      var bounds = new Bounds(p1, p2);

      assert.ok(bounds.contains(p1));
      assert.ok(bounds.contains(p2));
    });
    it("point on outside", function () {
      var p1 = new Point(0, 0);
      var p2 = new Point(10, 10);
      var bounds = new Bounds(p1, p2);

      assert.notOk(bounds.contains(new Point(-1, 5)));
      assert.notOk(bounds.contains(new Point(11, 5)));
      assert.notOk(bounds.contains(new Point(5, -1)));
      assert.notOk(bounds.contains(new Point(5, 11)));
    });
  });

});
