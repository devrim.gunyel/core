"use strict";
require("../../mocha-config");

var logger = require('../../logger');

var Point = require('../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('Point', function () {
  describe("constructor", function () {
    it("from coordinates", function () {
      var point = new Point(12, 3);
      assert.equal(12, point.x);
      assert.equal(3, point.y);
    });
    it("from point", function () {
      var point = new Point(12, 3);
      var point2 = new Point(point);
      assert.equal(point.x, point2.x);
      assert.equal(point.y, point2.y);
    });
    it("from dictionary", function () {
      var point = {x: 12, y: 3};
      var point2 = new Point(point);
      assert.equal(point.x, point2.x);
      assert.equal(point.y, point2.y);
    });
  });
  describe("distanceTo", function () {
    it("the same", function () {
      var point = new Point(12, 3);
      assert.closeTo(point.distanceTo(point), 0, helper.EPSILON);
    });
    it("different", function () {
      var point = new Point(12, 3);
      var point2 = new Point(15, 7);
      assert.closeTo(point.distanceTo(point2), 5, helper.EPSILON);
      assert.closeTo(point.distanceTo(new Point(15, 3)), 3, helper.EPSILON);
      assert.closeTo(point.distanceTo(new Point(12, -1)), 4, helper.EPSILON);
    });
  });

  it("toString", function () {
    var point = new Point(12, 3);
    assert.ok(point.toString().indexOf("12")>=0);
    assert.ok(point.toString().indexOf("3")>=0);
  });

});
