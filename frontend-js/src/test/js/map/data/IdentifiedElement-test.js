"use strict";

require("../../mocha-config");

var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var Alias = require('../../../../main/js/map/data/Alias');
var Reaction = require('../../../../main/js/map/data/Reaction');
var Point = require('../../../../main/js/map/canvas/Point');
var PointData = require('../../../../main/js/map/data/PointData');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

describe('IdentifiedElement', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  describe("constructor", function () {
    it("simple", function () {
      var javaObj = {
        objectId: "31165",
        modelId: 269,
        type: "alias",
        icon: "marker/marker/marker_red_1.png"
      };
      var ie = new IdentifiedElement(javaObj);
      assert.ok(ie);
      assert.equal(31165, ie.getId());
      assert.equal(269, ie.getModelId());
      assert.equal("ALIAS", ie.getType());
      assert.equal("marker/marker/marker_red_1.png", ie.getIcon());
    });

    it("from point", function () {
      var javaObj = {
        objectId: "Point2D.Double[117.685546875, 204.6923828125001]",
        modelId: 269,
        type: "POINT",
        icon: "icons/comment.png"
      };
      var ie = new IdentifiedElement(javaObj);
      assert.ok(ie);
      assert.ok(ie.getPoint());
    });

    it("from point 2", function () {
      var javaObj = {
        objectId: "(117.685546875, 204.6923828125001)",
        modelId: 269,
        type: "POINT",
        icon: "empty.png"
      };
      var ie = new IdentifiedElement(javaObj);
      assert.ok(ie);
      assert.ok(ie.getPoint());
    });
    it("from alias", function () {
      var jsonString = '{"bounds":{"x":190,"y":44,"width":80,"height":40},"modelId":57,"idObject":18554}';
      var javaObject = JSON.parse(jsonString);
      var alias = new Alias(javaObject);
      var ie = new IdentifiedElement(alias);
      assert.ok(ie);
      assert.equal(ie.getType(), "ALIAS");
    });

    it("from Reaction", function () {
      var javaObject = {
        lines: [{
          start: Object,
          end: Object,
          type: "START"
        }, {
          start: Object,
          end: Object,
          type: "END"
        }, {
          start: Object,
          end: Object,
          type: "MIDDLE"
        }],
        modelId: 319,
        idObject: "13178",
        centerPoint: {}
      };
      var reaction = new Reaction(javaObject);

      var ie = new IdentifiedElement(reaction);
      assert.ok(ie);
      assert.equal(ie.getType(), "REACTION");
    });

    it("from invalid object", function () {
      var javaObject = {
        modelId: 2
      };
      var code = function () {
        // noinspection JSCheckFunctionSignatures
        new IdentifiedElement(javaObject);
      };
      assert.throws(code, new RegExp("Type not defined"));
    });

    it("from PointData", function () {
      var modelId = 3;
      var point = new Point(2, 3.45);
      var pointData = new PointData(point, modelId);

      var ie = new IdentifiedElement(pointData);
      assert.ok(ie);
      assert.equal(ie.getType(), "POINT");
    });

    it("from invalid object 2", function () {
      var javaObject = {
        modelId: 2,
        type: "unk_type"
      };
      var code = function () {
        new IdentifiedElement(javaObject);
      };
      assert.throws(code, new RegExp("Unknown type"));
    });

    it("from invalid object 3", function () {
      var javaObject = {
        type: "alias",
        objectId: "el_id"
      };
      var code = function () {
        // noinspection JSCheckFunctionSignatures
        new IdentifiedElement(javaObject);
      };
      assert.throws(code, new RegExp("ModelId is invalid"));
    });

    it("from invalid object 4", function () {
      var javaObject = {
        type: "alias",
        modelId: 4
      };
      var code = function () {
        new IdentifiedElement(javaObject);
      };
      assert.throws(code, new RegExp("Id not defined"));
    });

    it("from artificial obj", function () {
      var javaObject = {
        type: "alias",
        objectId: "el_id",
        modelId: 123
      };
      var ie = new IdentifiedElement(javaObject);
      var point = ie.getPoint();
      expect(point).to.equal(null);
      assert.equal(logger.getWarnings().length, 1);
    });

  });

  it("equals", function () {
    var javaObj = {
      objectId: "31165",
      modelId: 269,
      type: "alias"
    };
    var ie = new IdentifiedElement(javaObj);
    var ie2 = new IdentifiedElement(javaObj);

    var ie3 = helper.createIdentifiedElement();

    assert.ok(ie.equals(ie));
    assert.ok(ie.equals(ie2));
    assert.notOk(ie.equals(ie3));

  });

});
