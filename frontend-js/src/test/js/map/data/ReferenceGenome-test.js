"use strict";

var ReferenceGenome = require('../../../../main/js/map/data/ReferenceGenome');
var chai = require('chai');
var assert = chai.assert;

describe('ReferenceGenome', function () {
  describe("constructor", function () {
    it("full", function () {
      var data = {
        type: "TYP",
        version: "v3",
        localUrl: "http://google.pl/",
        sourceUrl: "http://google.pl/",
        organism: {
          "annotatorClassName": "",
          "descriptionByType": "",
          "descriptionByTypeRelation": "",
          "id": 517528,
          "link": "http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=8822",
          "resource": "8822",
          "type": "TAXONOMY"
        }
      };
      var genome = new ReferenceGenome(data);
      assert.equal(genome.getType(), data.type);
      assert.equal(genome.getVersion(), data.version);
      assert.equal(genome.getUrl(), data.localUrl);
      assert.equal(genome.getGeneMappings().length, 0);
    });
    it("simple", function () {
      var data = {
        sourceUrl: "http://google.pl/",
        geneMapping: [{}],
        organism: {
          "annotatorClassName": "",
          "descriptionByType": "",
          "descriptionByTypeRelation": "",
          "id": 517528,
          "link": "http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=8822",
          "resource": "8822",
          "type": "TAXONOMY"
        }
      };
      var genome = new ReferenceGenome(data);
      assert.equal(genome.getUrl(), data.sourceUrl);
      assert.equal(genome.getGeneMappings().length, 1);
    });

    it("empty argument", function () {
      var genome = new ReferenceGenome();
      assert.ok(genome);
    });

    it("null argument", function () {
      var genome = new ReferenceGenome(null);
      assert.ok(genome);
    });
  });
});
