"use strict";

var KineticLaw = require('../../../../main/js/map/data/KineticLaw');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('KineticLaw', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  it("constructor", function () {
    var kineticLaw = new KineticLaw({
      "parameterIds": [],
      "definition": '<math xmlns="http://www.w3.org/1998/Math/MathML">' +
      '<apply xmlns="http://www.w3.org/1998/Math/MathML">' +
      '<divide/><apply><times/><ci>ca1</ci><apply><plus/><ci>sa1</ci><ci>sa2</ci></apply></apply><cn type=\"integer\"> 2 </cn>' +
      '</apply></math>',
      "functionIds": []
    });
    assert.ok(kineticLaw);
    assert.ok(kineticLaw.getDefinition().indexOf("apply") >= 0);
  });
});
