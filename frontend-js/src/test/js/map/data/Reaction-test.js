"use strict";

require("../../mocha-config.js");

var Reaction = require('../../../../main/js/map/data/Reaction');
var KineticLaw = require('../../../../main/js/map/data/KineticLaw');
var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('Reaction', function () {


  describe("constructor", function () {
    it("invalid line type", function () {
      var javaObject = {
        lines: [{
          start: Object,
          end: Object,
          type: "UNK"
        }]
      };
      try {
        new Reaction(javaObject);
      } catch (exception) {
        assert.ok(exception.message.indexOf("Unknown line type") >= 0);
      }
    });
    it("invalid center point", function () {
      var javaObject = {
        lines: []
      };
      try {
        new Reaction(javaObject);
      } catch (exception) {
        assert.ok(exception.message.indexOf("undefined center") >= 0);
      }
    });


    it("from Reaction", function () {
      var reaction = helper.createReaction();
      var reaction2 = new Reaction(reaction);
      assert.equal(reaction2.getId(), reaction.getId());
      assert.deepEqual(reaction2.getCenter(), reaction.getCenter());
      assert.equal(reaction2.getModelId(), reaction.getModelId());
      assert.deepEqual(reaction2.getLines(), reaction.getLines());
      assert.equal(logger.getWarnings().length, 0);
    });

    it("with empty Kinetics", function () {
      var javaObject = {
        reactionId: "re1",
        references: [],
        type: "",
        lines: [],
        centerPoint: {x: 0, y: 0},
        kineticLaw: {}
      };
      var reaction = new Reaction(javaObject);
      assert.ok(reaction.getKineticLaw() instanceof KineticLaw);
    });

  });
});
