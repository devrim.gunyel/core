"use strict";

require("../mocha-config.js");
var ServerConnector = require('../ServerConnector-mock');

var CustomMap = require('../../../main/js/map/CustomMap');
var PluginData = require('../../../main/js/map/data/PluginData');
var PluginDialog = require('../../../main/js/gui/PluginDialog');
var PluginManager= require('../../../main/js/plugin/PluginManager');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('PluginDialog', function () {

  it('init', function () {
    var pluginDialog;
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);

      pluginDialog = new PluginDialog({
        element: testDiv,
        customMap: map,
        pluginManager: new PluginManager({
          project: project,
          customMap: map,
          element: testDiv,
          configuration: helper.getConfiguration()
        }),
        configuration: helper.getConfiguration()
      });
      return pluginDialog.init();
    }).then(function(){
      return pluginDialog.destroy();
    });
  });

  it('open', function () {
    var pluginDialog;
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);

      pluginDialog = new PluginDialog({
        element: testDiv,
        customMap: map,
        pluginManager: new PluginManager({
          project: project,
          customMap: map,
          element: testDiv,
          configuration: helper.getConfiguration()
        }),
        configuration: helper.getConfiguration()
      });
      return pluginDialog.init();
    }).then(function(){
      pluginDialog._knownPlugins.push(new PluginData({
        urls: [],
        name: "x",
        hash: "8f2112859d40de86dacc1994a224ea3d",
        version: "0.0.1",
        isPublic:"true"
      }));
      return pluginDialog.open();
    }).then(function(){
      return pluginDialog.destroy();
    });
  });

});
