"use strict";

require("../../mocha-config");
var $ = require('jquery');

var ConfigurationAdminPanel = require('../../../../main/js/gui/admin/ConfigurationAdminPanel');
var ConfigurationType = require('../../../../main/js/ConfigurationType');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

/**
 *
 * @returns {ConfigurationAdminPanel}
 */
function createConfigurationTab() {
  return new ConfigurationAdminPanel({
    element: testDiv,
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('ConfigurationAdminPanel', function () {

  describe('init', function () {
    it('admin', function () {
      helper.loginAsAdmin();
      var mapTab = createConfigurationTab();
      return mapTab.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok($("[name^='edit-']", testDiv).length > 0);
        assert.notOk($("[name^='edit-']", testDiv).prop('disabled'));
        return mapTab.destroy();
      });
    });
    it('no access', function () {
      helper.loginWithoutAccess();
      var mapTab = createConfigurationTab();
      return mapTab.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.equal($("[name^='edit']", testDiv).length, 0);
        return mapTab.destroy();
      });
    });
  });
  it('saveOption', function () {
    helper.loginAsAdmin();
    var mapTab = createConfigurationTab();
    return mapTab.init().then(function () {
      return mapTab.saveOption(ConfigurationType.DEFAULT_MAP);
    }).then(function () {
      return mapTab.destroy();
    });
  });

});
