"use strict";

require("../../mocha-config");
var $ = require('jquery');

var ChooseValidatorsDialog = require('../../../../main/js/gui/admin/ChooseValidatorsDialog');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');
var Promise = require('bluebird');

var chai = require('chai');
var assert = chai.assert;

function createDialog() {
  return new ChooseValidatorsDialog({
    element: testDiv,
    customMap: null,
    serverConnector: ServerConnector,
    configuration: helper.getConfiguration()
  });
}

describe('ChooseValidatorsDialog', function () {
  it('init', function () {
    var dialog = createDialog();
    assert.equal(0, logger.getWarnings().length);
    return dialog.init();
  });

  it('setElementType', function () {
    var dialog = createDialog();
    return ServerConnector.getConfiguration().then(function (configuration) {
      return dialog.setElementType(configuration.getReactionTypes()[0]);
    })
  });

  it('open', function () {
    var dialog = createDialog();
    return dialog.init().then(function () {
      return dialog.open();
    }).then(function () {
      return dialog.destroy();
    }).then(function () {
      assert.equal(logger.getWarnings().length, 0);
    });
  });

  it('copy from', function () {
    helper.loginAsAdmin();
    var originalFunction = ServerConnector.updateUserPreferences;
    var functionCalled = false;
    var dialog = createDialog();
    return dialog.init().then(function () {
      return dialog.open();
    }).then(function () {
      return dialog.setElementType(helper.getConfiguration().getReactionTypes()[0]);
    }).then(function () {
      ServerConnector.updateUserPreferences = function (params) {
        functionCalled = true;
        return Promise.resolve(params.user);
      };
      return $("[name='copy-from-validator']")[0].onclick();
    }).then(function () {
      assert.ok(functionCalled);
    }).finally(function () {
      ServerConnector.updateUserPreferences = originalFunction;
      return dialog.destroy();
    });
  });

});
