"use strict";

require("../../mocha-config");
var $ = require('jquery');

var EditGenomeDialog = require('../../../../main/js/gui/admin/EditGenomeDialog');
var ReferenceGenome = require('../../../../main/js/map/data/ReferenceGenome');
var ValidationError = require('../../../../main/js/ValidationError');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');
var Promise = require('bluebird');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

/**
 *
 * @param {ReferenceGenome} genome
 * @returns {EditGenomeDialog}
 */
function createDialog(genome) {
  return new EditGenomeDialog({
    element: testDiv,
    referenceGenome: genome,
    customMap: null,
    serverConnector: ServerConnector
  });
}

describe('EditGenomeDialog', function () {

  describe('init', function () {
    it('new genome', function () {
      var genome = new ReferenceGenome();
      var dialog = createDialog(genome);
      return dialog.init().then(function () {
        return dialog.destroy();
      });
    });
    it('existing', function () {
      helper.loginAsAdmin();
      var dialog;
      return ServerConnector.getReferenceGenome({genomeId: 154}).then(function (genome) {
        dialog = createDialog(genome);
        return dialog.init();
      }).then(function () {
        return dialog.destroy();
      });
    });
  });

  it('removeMapping', function () {
    helper.loginAsAdmin();
    var dialog;
    return ServerConnector.getReferenceGenome({genomeId: 154}).then(function (genome) {
      dialog = createDialog(genome);
      return dialog.init();
    }).then(function () {
      dialog.askConfirmRemoval = function () {
        return Promise.resolve({status: true});
      };
      assert.equal(0, logger.getWarnings().length);

      var element = $("[name='removeMapping']")[0];
      return helper.triggerJqueryEvent(element, "click");
    }).then(function () {
      return dialog.destroy();
    });
  });


  it('on organism change', function () {
    var genome = new ReferenceGenome();
    var dialog = createDialog(genome);
    return dialog.init().then(function () {
      var element = $("[name='genomeOrganismSelect']")[0];
      return helper.triggerJqueryEvent(element, "change");
    }).then(function () {
      return dialog.destroy();
    });
  });

  it('on type change', function () {
    var genome = new ReferenceGenome();
    var dialog = createDialog(genome);
    return dialog.init().then(function () {
      var element = $("[name='genomeTypeSelect']")[0];
      return helper.triggerJqueryEvent(element, "change");
    }).then(function () {
      return dialog.destroy();
    });
  });

  it('on version change', function () {
    var genome = new ReferenceGenome();
    var dialog = createDialog(genome);
    return dialog.init().then(function () {
      var element = $("[name='genomeVersionSelect']")[0];
      return helper.triggerJqueryEvent(element, "change");
    }).then(function () {
      return dialog.destroy();
    });
  });

  describe('click cancel', function () {
    it('new genome', function () {
      var genome = new ReferenceGenome();
      var dialog = createDialog(genome);
      return dialog.init().then(function () {
        return dialog.open();
      }).then(function () {
        return $("[name=cancelGenome]", testDiv)[0].onclick();
      }).then(function () {
        dialog.destroy();
      });
    });
  });

  describe('click save', function () {
    it('existing genome', function () {
      var dialog;
      return ServerConnector.getReferenceGenome({
        organism: "9606",
        type: "UCSC",
        version: "hg38"
      }).then(function (genome) {
        dialog = createDialog(genome);
        return dialog.init();
      }).then(function () {
        return dialog.open();
      }).then(function () {
        return $("[name=saveGenome]", testDiv)[0].onclick();
      }).then(function () {
        dialog.destroy();
      });
    });

    it('new genome', function () {
      helper.loginAsAdmin();
      var genome = new ReferenceGenome();
      var dialog = createDialog(genome);
      return dialog.init().then(function () {
        return dialog.open();
      }).then(function () {
        return $("[name=saveGenome]", testDiv)[0].onclick();
      }).then(function () {
        dialog.destroy();
      });
    });
  });

});
