"use strict";

require("../../mocha-config");
var $ = require('jquery');

var CommentsAdminPanel = require('../../../../main/js/gui/admin/CommentsAdminPanel');
var ServerConnector = require('../../ServerConnector-mock');


var logger = require('../../logger');

var fs = require("fs");
var chai = require('chai');
var assert = chai.assert;

describe('CommentsAdminPanel', function () {
  function createDialog() {
    return ServerConnector.getProject().then(function (project) {
      return new CommentsAdminPanel({
        element: testDiv,
        configuration: helper.getConfiguration(),
        customMap: null,
        project: project,
        serverConnector: ServerConnector
      });
    });
  }

  describe('askConfirmRemoval', function () {
    it('check question', function () {
      helper.loginAsAdmin();
      return createDialog().then(function (dialog) {
        var title = "Question?";
        dialog.askConfirmRemoval({title: title});
        assert.ok($(".ui-dialog-title").html().indexOf(title) >= 0);
        $("button:contains(Cancel)").click();

        return dialog.destroy();
      });
    });
    it('check ok status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $("button:contains(OK)").click();
        return promise;
      }).then(function (result) {
        assert.ok(result.status);
        return dialog.destroy();
      });
    });
    it('check cancel status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $("button:contains(Cancel)").click();
        return promise;
      }).then(function (result) {
        assert.notOk(result.status);
        return dialog.destroy();
      });
    });
    it('check close confirmation status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $(".ui-dialog-titlebar-close").click();
        return promise;
      }).then(function (result) {
        assert.notOk(result.status);
        return dialog.destroy();
      });
    });
  });

  describe('remove comment', function () {
    it('admin can remove', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        return dialog.init();
      }).then(function () {
        assert.ok($("[name='removeComment']").is(':enabled'));
        return dialog.destroy();
      });
    });
    it('anonymous cannot remove', function () {
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        return dialog.init();
      }).then(function () {
        assert.ok($("[name='removeComment']").is(':disabled'));
        return dialog.destroy();
      });
    });
  });

});
