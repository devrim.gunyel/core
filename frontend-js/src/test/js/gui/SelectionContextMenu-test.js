"use strict";

require("../mocha-config.js");
var $ = require('jquery');

var Point = require('../../../main/js/map/canvas/Point');
var SelectionContextMenu = require('../../../main/js/gui/SelectionContextMenu');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('SelectionContextMenu', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    new SelectionContextMenu({
      element: testDiv,
      customMap: map
    });

    assert.equal(logger.getWarnings().length, 0);
  });

  it('download image', function () {
    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      var contextMenu = new SelectionContextMenu({
        element: testDiv,
        customMap: map
      });
      return contextMenu.init();
    }).then(function () {
      map.setActiveSubmapId(map.getProject().getModels()[0].getId());
      map.setActiveSubmapClickCoordinates(new Point(2, 12));

      var handler = $($("a:contains('PNG')", $(testDiv))[0]).data("handler");
      return handler();
    }).then(function () {
      map.destroy();
    });
  });

  it('download cell designer file', function () {
    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      var contextMenu = new SelectionContextMenu({
        element: testDiv,
        customMap: map
      });
      return contextMenu.init();
    }).then(function () {
      map.setActiveSubmapId(map.getProject().getModels()[0].getId());
      map.setActiveSubmapClickCoordinates(new Point(2, 12));
      var handler = $($("a:contains('CellDesigner')", $(testDiv))[0]).data("handler");
      return handler();
    }).then(function () {
      map.destroy();
    });
  });
});
