"use strict";

/* exported logger */

require('../mocha-config.js');

var Panel = require('../../../main/js/gui/Panel');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('Panel', function () {

  it('openDialog', function () {
    var map = helper.createCustomMap();

    var panel = new Panel({
      element: testDiv,
      customMap: map
    });

    var content = document.createElement("div");
    content.innerHTML = "some content";

    var id = 1;
    panel.openDialog(content, {
      id: id
    });

    assert.ok(panel.getDialogDiv(id).innerHTML.indexOf("some content") >= 0);
    panel.destroy();
    assert.equal(0, logger.getWarnings().length);
  });

});
