"use strict";

require('../../mocha-config.js');

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var MiRna = require('../../../../main/js/map/data/MiRna');
var MiRnaPanel = require('../../../../main/js/gui/leftPanel/MiRnaPanel');
var PanelControlElementType = require('../../../../main/js/gui/PanelControlElementType');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('MiRnaPanel', function () {
  function createPanel() {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createMiRnaDbOverlay(map);

    return new MiRnaPanel({
      element: testDiv,
      customMap: map
    });

  }

  it('constructor', function () {
    var map = helper.createCustomMap();
    helper.createMiRnaDbOverlay(map);

    new MiRnaPanel({
      element: testDiv,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
  });

  describe('init', function () {
    it('with mirna query in session', function () {
      var panel;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        helper.createMiRnaDbOverlay(map);

        panel = new MiRnaPanel({
          element: testDiv,
          customMap: map
        });
        var query = panel.getOverlayDb().encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, "hsa-miR-125a-3p");
        ServerConnector.getSessionData(project).setQuery({
          type: "mirna",
          query: query
        });

        return panel.init();
      }).then(function () {
        panel.destroy();
      });
    });
  });

  it('create MiRnaPanel for empty', function () {
    var map = helper.createCustomMap();
    helper.createMiRnaDbOverlay(map);

    var panel = new MiRnaPanel({
      element: testDiv,
      customMap: map
    });

    assert.ok(panel.createPreamble().innerHTML.indexOf("NOT FOUND") > 0);
  });

  it('create MiRnaPanel for empty MiRna', function () {
    var map = helper.createCustomMap();
    helper.createMiRnaDbOverlay(map);

    var panel = new MiRnaPanel({
      element: testDiv,
      customMap: map
    });

    assert.ok(panel.createPreamble(new MiRna()).innerHTML.indexOf("NOT FOUND") > 0);
  });

  it('on searchResults changed', function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    var miRnaDbOverlay = helper.createMiRnaDbOverlay(map);

    new MiRnaPanel({
      element: testDiv,
      customMap: map
    });

    return miRnaDbOverlay.searchByQuery("hsa-miR-125a-3p").then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("hsa-miR-125a-3p") >= 0);
    });
  });

  it('searchByQuery', function () {
    var panel = createPanel();

    panel.getControlElement(PanelControlElementType.SEARCH_INPUT).value = "hsa-miR-125a-3p";

    return panel.searchByQuery().then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("http://www.mirbase.org/cgi-bin/mirna_entry.pl?acc") >= 0);
    });
  });

  it("refreshSearchAutocomplete", function () {
    var panel = createPanel();

    return panel.refreshSearchAutocomplete().then(function (data) {
      assert.ok(data);
    });
  });

});
