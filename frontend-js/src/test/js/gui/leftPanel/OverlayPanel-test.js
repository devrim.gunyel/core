"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var OverlayPanel = require('../../../../main/js/gui/leftPanel/OverlayPanel');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

/**
 *
 * @param {CustomMap} map
 * @returns {OverlayPanel}
 */
function createOverlayPanel(map) {
  return new OverlayPanel({
    element: testDiv,
    customMap: map,
    configuration: helper.getConfiguration()
  });
}

describe('OverlayPanel', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    var overlay = new OverlayPanel({
      element: testDiv,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
    return overlay.destroy();
  });

  describe('refresh', function () {
    it('anonymous', function () {
      var panel;
      return ServerConnector.getProject("sample").then(function (project) {
        project.addDataOverlay(helper.createBackgroundOverlay());
        var map = helper.createCustomMap(project);

        panel = new OverlayPanel({
          element: testDiv,
          customMap: map
        });
        return panel.init();
      }).then(function () {
        return panel.refresh();
      }).then(function () {
        assert.ok(panel.getElement().innerHTML.indexOf("testLayout") < 0);
        assert.ok(panel.getElement().innerHTML.indexOf("YOU ARE NOT LOGGED") >= 0);
        return panel.destroy();
      });
    });
    it('admin', function () {
      helper.loginAsAdmin();
      var panel;
      return ServerConnector.getProject("sample").then(function (project) {
        project.addDataOverlay(helper.createBackgroundOverlay());
        var map = helper.createCustomMap(project);

        panel = new OverlayPanel({
          element: testDiv,
          customMap: map
        });

        return panel.init();
      }).then(function () {

        return panel.refresh();
      }).then(function () {
        assert.ok(panel.getElement().innerHTML.indexOf("testLayout") < 0);
        assert.ok(panel.getElement().innerHTML.indexOf("YOU ARE NOT LOGGED") < 0);
        return panel.destroy();
      });
    });
  });

  it('createRow', function () {
    var map = helper.createCustomMap();

    var overlay = helper.createOverlay();
    overlay.setInputDataAvailable(true);
    overlay.setCreator("me");
    map.getProject().addDataOverlay(overlay);

    var panel = new OverlayPanel({
      element: testDiv,
      customMap: map
    });

    var row = panel.createOverlayRow(overlay);
    assert.ok(row);

    var buttons = row.getElementsByTagName("button");
    var openButton = null;
    for (var i = 0; i < buttons.length; i++) {
      var name = buttons[i].getAttribute("name");
      if (name !== undefined && name.indexOf("editButton") >= 0) {
        openButton = buttons[i];
      }
    }
    assert.ok(openButton);
    return helper.triggerJqueryEvent(openButton, "click").then(function () {
      return panel.destroy();
    });
  });
  it('select background', function () {
    var panel;
    return ServerConnector.getProject().then(function (project) {
      panel = new OverlayPanel({
        element: testDiv,
        customMap: helper.createCustomMap(project)
      });
      return panel.init();
    }).then(function () {
      var backgroundLink = $("[name='overlayLink']", panel.getElement())[0];
      assert.ok($(backgroundLink).attr("data"));
      return helper.triggerJqueryEvent(backgroundLink, "click");
    }).then(function () {
      return panel.destroy();
    });
  });
  it('download', function () {
    var panel;
    return ServerConnector.getProject("sample").then(function (project) {
      var map = helper.createCustomMap(project);

      panel = new OverlayPanel({
        element: testDiv,
        customMap: map
      });

      return panel.init();
    }).then(function () {
      return panel.refresh();
    }).then(function () {
      var buttons = panel.getElement().getElementsByTagName("button");
      var downloadButton;
      for (var i = 0; i < buttons.length; i++) {
        var name = buttons[i].getAttribute("name");
        if (name !== undefined && name !== null && name.indexOf("download-overlay") >= 0) {
          downloadButton = buttons[i];
        }
      }
      assert.ok(downloadButton);
      assert.notOk(panel.getLastDownloadUrl());
      return helper.triggerJqueryEvent(downloadButton, "click");
    }).then(function () {
      assert.ok(panel.getLastDownloadUrl());
      return panel.destroy();
    });
  });

  describe('openAddOverlayDialog', function () {
    it('open', function () {
      it('open', function () {
        var map = helper.createCustomMap();

        var panel = createOverlayPanel(map);

        return panel.openAddOverlayDialog().then(function () {
          return panel.destroy();
        });
      });

      it('trigger onAddOverlay event handler', function () {
        var map = helper.createCustomMap();
        var panel = createOverlayPanel(map);

        return panel.init().then(function () {
          return panel.openAddOverlayDialog();
        }).then(function () {
          var overlay = helper.createOverlay(panel.getProject().getModels()[0]);
          return panel._addOverlayDialog.callListeners("onAddOverlay", overlay);
        }).then(function () {
          return panel.destroy();
        });
      });
    });
  });

  describe('openEditOverlayDialog', function () {
    it('open for map with Google Maps Api', function () {
      helper.loginAsAdmin();
      var panel;
      return ServerConnector.getProject().then(function (project) {
        project.setMapCanvasType("GOOGLE_MAPS_API");
        panel = new OverlayPanel({
          element: testDiv,
          customMap: helper.createCustomMap(project)
        });
        return panel.init();
      }).then(function () {
        var backgroundLink = $("[name='editButton']", panel.getElement())[0];
        assert.ok($(backgroundLink).attr("data"));
        return helper.triggerJqueryEvent(backgroundLink, "click");
      }).then(function () {
        assert.equal(1, $("[name='overlay-google-consent']").length);
        return panel.destroy();
      });
    });

    it('open for map without Google Maps Api', function () {
      helper.loginAsAdmin();
      var panel;
      return ServerConnector.getProject().then(function (project) {
        project.setMapCanvasType("OPEN_LAYERS");
        panel = new OverlayPanel({
          element: testDiv,
          customMap: helper.createCustomMap(project)
        });
        return panel.init();
      }).then(function () {
        var backgroundLink = $("[name='editButton']", panel.getElement())[0];
        assert.ok($(backgroundLink).attr("data"));
        return helper.triggerJqueryEvent(backgroundLink, "click");
      }).then(function () {
        assert.equal(0, $("[name='overlay-google-consent']").length);
        return panel.destroy();
      });
    });

  });
});
