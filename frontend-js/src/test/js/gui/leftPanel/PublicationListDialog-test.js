"use strict";

require('../../mocha-config.js');

var PublicationListDialog = require('../../../../main/js/gui/leftPanel/PublicationListDialog');
var ServerConnector = require('../../ServerConnector-mock');
var Functions = require('../../../../main/js/Functions');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('PublicationListDialog', function () {

  /**
   *
   * @param {CustomMap} map
   * @returns {PublicationListDialog}
   */
  function createPublicationListDialog(map) {
    return new PublicationListDialog({
      element: testDiv,
      customMap: map,
      configuration: helper.getConfiguration()
    });
  }

  it('constructor', function () {
    var map = helper.createCustomMap();

    var dialog = createPublicationListDialog(map);
    assert.equal(logger.getWarnings().length, 0);

    dialog.destroy();
  });

  it('_dataTableAjaxCall', function () {
    var dialog;
    var callbackCalled = false;
    return ServerConnector.getProject().then(function (project) {
      dialog = createPublicationListDialog(helper.createCustomMap(project));
      // noinspection JSAccessibilityCheck
      return dialog._dataTableAjaxCall({
        start: 0,
        length: 10,
        order: [{column: 0, dir: 'asc'}],
        search: {value: '', regex: false}
      }, function () {
        callbackCalled = true;
      });
    }).then(function () {
      assert.ok(callbackCalled);
    }).finally(function () {
      dialog.destroy();
    });
  });

  it('publicationListAsCsvString', function () {

    var dialog;
    return ServerConnector.getProject().then(function (project) {
      dialog = createPublicationListDialog(helper.createCustomMap(project));
      // noinspection JSAccessibilityCheck
      return ServerConnector.getPublications({
        start: 0,
        length: 10
      });
    }).then(function (publicationList) {
      return dialog.publicationListAsCsvString(publicationList);
    }).then(function (result) {
      assert.ok(Functions.isString(result));
      assert.ok(result.indexOf("re21") >= 0);
    }).finally(function () {
      dialog.destroy();
    });
  });

  describe('publicationListToArray', function () {
    it('default', function () {
      var dialog;
      return ServerConnector.getProject().then(function (project) {
        dialog = createPublicationListDialog(helper.createCustomMap(project));
        // noinspection JSAccessibilityCheck
        return ServerConnector.getPublications({
          start: 0,
          length: 10
        });
      }).then(function (publicationList) {
        return dialog.publicationListToArray(publicationList);
      }).then(function (result) {
        assert.equal(10, result.length);
      }).finally(function () {
        dialog.destroy();
      });
    });
    it('invalid publication response', function () {
      var dialog;
      return ServerConnector.getProject().then(function (project) {
        dialog = createPublicationListDialog(helper.createCustomMap(project));
        // noinspection JSAccessibilityCheck
        return ServerConnector.getPublications({
          start: 0,
          length: 10
        });
      }).then(function (publicationList) {
        publicationList.data[0].publication.article = null;
        return dialog.publicationListToArray(publicationList);
      }).then(function (result) {
        assert.equal(10, result.length);
      }).finally(function () {
        dialog.destroy();
      });
    });
  });

});
