"use strict";

require("../mocha-config");
var $ = require('jquery');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var Plugin = require('../../../main/js/plugin/Plugin');
var Point = require('../../../main/js/map/canvas/Point');
var ServerConnector = require('../ServerConnector-mock');

var logger = require('../logger');
var chai = require('chai');
var assert = chai.assert;

var fs = require('fs');

/**
 *
 * @param {string} file
 * @param {CustomMap} [map]
 * @returns {Plugin}
 */
function createPlugin(file, map) {
  if (map === undefined) {
    map = helper.createCustomMap();
  }
  return new Plugin({
    url: file,
    map: map,
    element: testDiv,
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('Plugin', function () {
  it('constructor', function () {
    var plugin = new Plugin({
      url: "./testFiles/plugin/empty.js",
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector,
      element: testDiv,
      map: helper.createCustomMap()
    });
    assert.ok(plugin);
  });

  it('test plugins', function () {
    var promises = [];

    global.$ = $;
    fs.readdirSync('./testFiles/plugin/').forEach(function (file) {
      var element = document.createElement("div");
      testDiv.appendChild(element);
      var plugin = createPlugin("./testFiles/plugin/" + file);
      promises.push(plugin.load());
    });
    return Promise.all(promises).finally(function () {
      global.$ = undefined;
    });
  });

  describe('load', function () {
    it('default', function () {
      var plugin = createPlugin("./testFiles/plugin/empty.js");
      return plugin.load().then(function () {
        assert.equal("test plugin", plugin.getName());
        assert.equal("0.0.1", plugin.getVersion());
        assert.equal(0, logger.getWarnings().length);
      });
    });

    it('invalid javascript code', function () {
      var plugin = createPlugin("./testFiles/plugin-invalid/invalid_javascript.js");
      return plugin.load().then(function () {
        assert.notOk("expected error");
      }, function (error) {
        assert.ok(error.message.indexOf("Unexpected identifier") >= 0, "Wrong message: " + error.message);
      });
    });

    it('plugin register crash', function () {
      var plugin = createPlugin("./testFiles/plugin-invalid/invalid_register.js");
      return plugin.load().then(function () {
        assert.false("expected error");
      }, function (error) {
        assert.ok(error.message.indexOf("Let's crash") >= 0, "Wrong message: " + error.message);
      });
    });
    it('plugin register promise reject', function () {
      var plugin = createPlugin("./testFiles/plugin-invalid/result-promise-crash.js");
      return plugin.load().then(function () {
        assert.false("expected error");
      }).catch(function (error) {
        assert.ok(error.message.indexOf("Let's reject") >= 0, "Wrong message: " + error.message);
      });
    });
  });
  describe('unload', function () {
    it('warning about cleaning', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var plugin = createPlugin("./testFiles/plugin-invalid/unclean-unregister.js", map);
      return plugin.load().then(function () {
        assert.equal(0, logger.getWarnings().length);
        return plugin.unload();
      }).then(function () {
        assert.equal(1, logger.getWarnings().length);
      });
    });
    it('plugin crashed during unload', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var plugin = createPlugin("./testFiles/plugin-invalid/crash-unregister.js", map);
      return plugin.load().then(function () {
        assert.equal(0, logger.getWarnings().length);
        return plugin.unload();
      }).then(function () {
        assert.equal(2, logger.getWarnings().length);
      });
    });
  });
  it('plugin listener crash', function () {
    var map = helper.createCustomMap();

    var plugin = createPlugin("./testFiles/plugin/listener-crash.js", map);
    return plugin.load().then(function () {
      return map.callListeners("onCenterChanged", new Point(0, 0));
    }).then(function () {
      assert.equal(2, logger.getWarnings().length);
    });
  });
});
