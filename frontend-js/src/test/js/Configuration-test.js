"use strict";

require("./mocha-config");

var Configuration = require('../../main/js/Configuration');
var ConfigurationType = require('../../main/js/ConfigurationType');
var ServerConnector = require('./ServerConnector-mock');
var logger = require('./logger');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

describe('Configuration', function () {
  describe('constructor', function () {
    it('default', function () {
      var configuration = new Configuration({
        options: {},
        overlayTypes: {},
        imageFormats: {},
        modelFormats: {},
        elementTypes: {},
        reactionTypes: {},
        miriamTypes: {},
        mapTypes: {},
        modificationStateTypes: {},
        privilegeTypes: {},
        annotators: {},
        bioEntityFields: []
      });
      assert.ok(configuration);
      assert.equal(0, logger.getWarnings().length);
    });
    it('copy', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var copy = new Configuration(configuration);

        assert.equal(configuration.getElementTypes().length, copy.getElementTypes().length)
      });
    });
  });

  describe('getParentType', function () {
    it('for element type', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var elementType = configuration.getElementTypes()[0];
        if (elementType.parentClass.indexOf("BioEntity") > 0) {
          elementType = configuration.getElementTypes()[1];
        }
        var parent = configuration.getParentType(elementType);
        assert.ok(parent);
      });
    });
    it('for reaction type', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var elementType = configuration.getReactionTypes()[0];
        if (elementType.parentClass.indexOf("BioEntity") > 0) {
          elementType = configuration.getReactionTypes()[1];
        }
        var parent = configuration.getParentType(elementType);
        assert.ok(parent);
      });
    });
  });

  describe('getElementAnnotators', function () {
    it('get all', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var annotators = configuration.getElementAnnotators();
        assert.ok(annotators);
        assert.ok(annotators.length > 0);
      });
    });
    it('get for element', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var found = false;
        var types = configuration.getElementTypes();
        for (var i = 0; i < types.length; i++) {
          var annotators = configuration.getElementAnnotators(types[i]);
          if (annotators.length > 0) {
            found = true;
          }
        }
        assert.ok(found);
      });
    });
  });

  describe('getOption', function () {
    it('DEFAULT_MAP', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var option = configuration.getOption(ConfigurationType.DEFAULT_MAP);
        assert.ok(option);
        assert.ok(option.getType());
        assert.ok(option.getValue());
        assert.ok(option.getCommonName());
      });
    });
    it('get for element', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var found = false;
        var types = configuration.getElementTypes();
        for (var i = 0; i < types.length; i++) {
          var annotators = configuration.getElementAnnotators(types[i]);
          if (annotators.length > 0) {
            found = true;
          }
        }
        assert.ok(found);
      });
    });
  });

  describe('getBooleanValue', function () {
    var conf = new Configuration({
      options: [{
        "commonName": "OK value",
        "group": "Email notification details",
        "type": "BOOLEAN_OPTION_OK",
        "value": "true",
        "valueType": "BOOLEAN"
      }, {
        "commonName": "FALSE value",
        "group": "Email notification details",
        "type": "BOOLEAN_OPTION_NOT_OK",
        "value": "false",
        "valueType": "BOOLEAN"
      }, {
        "commonName": "FALSE value",
        "group": "Email notification details",
        "type": "STRING_OPTION",
        "value": "false",
        "valueType": "STRING"
      }],
      overlayTypes: {},
      imageFormats: {},
      modelFormats: {},
      elementTypes: {},
      reactionTypes: {},
      miriamTypes: {},
      mapTypes: {},
      modificationStateTypes: {},
      privilegeTypes: {},
      annotators: {},
      bioEntityFields: []
    });
    it('for valid value', function () {
      assert.ok(conf.getBooleanValue("BOOLEAN_OPTION_OK"));
      assert.notOk(conf.getBooleanValue("BOOLEAN_OPTION_NOT_OK"));
    });
    it('for string value', function () {
      expect(function () {
        conf.getBooleanValue("STRING_OPTION");
      }).to.throw();
    });
  });

  describe('getElementTypeTree', function () {
    it('default', function () {

      return ServerConnector.getConfiguration().then(function (configuration) {
        var treeData = configuration.getElementTypeTree();

        assert.ok(treeData);
        assert.ok(treeData.text);
        assert.ok(treeData.children);
        assert.equal(2, treeData.children.length);
      })
    });
    it('check sorting', function () {
      /**
       *
       * @param {BioEntityTypeTreeNode[]} children
       */
      function checkChildrenSorted(children) {
        var i;
        for (i = 1; i < children.length; i++) {
          assert.ok(children[i - 1].text < children[i].text, "children are not sorted: " + children[i - 1].text + " ; " + children[i].text);
        }
        for (i = 0; i < children.length; i++) {
          if (children[i].children != null) {
            checkChildrenSorted(children[i].children);
          }
        }
      }

      return ServerConnector.getConfiguration().then(function (configuration) {
        var treeData = configuration.getElementTypeTree();
        checkChildrenSorted(treeData.children);
      })
    });
  });


});
