"use strict";

require("./mocha-config");

var GuiConnector = require('../../main/js/GuiConnector');
var SecurityError = require('../../main/js/SecurityError');
var ServerConnector = require('./ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('./logger');

describe('logger', function () {
  beforeEach(function(){
    logger.flushBuffer();
  });
  it('warn', function () {
    logger.warn("test warning");
    assert.equal(1, logger.getWarnings().length);
    assert.equal(1, logger.getEvents().length);
  });

  it('debug', function () {
    logger.debug("test debug");
    assert.equal(1, logger.getEvents().length);
  });

  it('error', function () {
    logger.error("test error");
    assert.equal(1, logger.getErrors().length);
  });

});