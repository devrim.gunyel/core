#TYPE=GENETIC_VARIANT						
#GENOME_TYPE=UCSC						
#GENOME_VERSION=hg38						
position	original_dna	alternative_dna	name	description	color	contig	allele_frequency	variant_identifier	amino_acid_change
40758652	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon49:c.T7190C:p.M2397T
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon1:c.T45C:p.T15T
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.T15T
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.T15K,p.T17K
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon1:c.151_152insCCTCCAAGTTATTTCAAGGCAAAAATATCCATGTGCCTCT:p.A51fs
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.A52insfs
40619084	G	GCCTCCAAGTTATTTCAAGGCAAAAATATCCATGTGCCTCT	LRRK2	upstream;downstream	#ff0001	chr1	0.5	identifier_2	LRRK2:NM_198578:exon1:c.151_152insCCTCCAAGTTATTTCAAGGCAAAAATATCCATGTGCCTCT:p.A51insfs
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon27:c.3777_3778insATTCCTCCT:p.E1259delinsEIPP
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.I1260delinsIPP,p.P1261delinsKT
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon17:c.2069_2070del:p.Q690fs
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.Q691delfs
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	LRRK2:NM_198578:exon9:c.1099_1101del:p.367_367del
40618978	T	C	LRRK2	upstream	#ff0000	chr1	0.8	identifier_1	p.368_373del
