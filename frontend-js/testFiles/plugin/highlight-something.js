minervaDefine(function () {
  return {
    register: function (minervaObject) {
      minervaObject.element.innerHTML = "<div><button id='show'>highlight something</button></div>";
      $("#show", minervaObject.element).on("click", function () {
        console.log("button clicked");
        console.log(minervaObject);
        return minervaObject.project.data.getAllBioEntities().then(function (bioEntities) {
          console.log("found " + bioEntities.length + " elements");
          console.log("SHOW: ", bioEntities[0]);
          return minervaObject.project.map.showBioEntity({element: bioEntities[0], type: "ICON"});
        });
      });
    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "high test";
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});