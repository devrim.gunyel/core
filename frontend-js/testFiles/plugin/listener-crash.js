minervaDefine(function () {
  return {
    register: function (minervaObject) {
      var options = {
        object: "map",
        type: "onCenterChanged",
        callback: function () {
          throw new Error("Let's crash");
        }
      };
      minervaObject.project.map.addListener(options);
    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "test plugin";
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});