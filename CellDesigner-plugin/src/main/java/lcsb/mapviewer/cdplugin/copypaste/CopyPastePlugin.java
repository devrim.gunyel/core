package lcsb.mapviewer.cdplugin.copypaste;

import java.awt.*;
import java.awt.event.KeyEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.MainWindow;
import jp.sbi.celldesigner.plugin.CellDesignerPlugin;
import jp.sbi.celldesigner.plugin.PluginSBase;

/**
 * This class represent a plugin to cell designer with copy past functionality
 * that allows to copy and paste notes and annotations of species.
 * 
 * It overrides:
 * <ul>
 * <li>ALT + CTRL + C shortcut - copy annotations from selected element into
 * system clipboard, if many elements are selected then copy from the one with
 * the lowest id,</li>
 * <li>ALT + CTRL + V shortcut - paste annotations from clipboard into selected
 * element(s), if many elements are selected then all elements will be modified,
 * </li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
public class CopyPastePlugin extends CellDesignerPlugin {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(CopyPastePlugin.class.getName());
  /**
   * Main window of cell designer.
   */
  private MainWindow win;

  /**
   * Paste action.
   */
  private PastePluginAction pastePluginAction;

  /**
   * Paste action.
   */
  private CopyPluginAction copyPluginAction;

  /**
   * Default constructor. Creates menu and InfoFrame.
   */
  public CopyPastePlugin() {
    try {
      // PropertyConfigurator.configure("D:/log4j.properties");
      logger.debug("Loading copy-paste plugin...");

      win = MainWindow.getLastInstance();

      pastePluginAction = new PastePluginAction(this, win);
      copyPluginAction = new CopyPluginAction(this, win);

      // create keyboard listener for shortcuts
      KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(createKeyEventDispatcher());

    } catch (Exception exception) {
      logger.error(exception, exception);
    }
  }

  protected KeyEventDispatcher createKeyEventDispatcher() {
    return new KeyEventDispatcher() {
      public boolean dispatchKeyEvent(KeyEvent e) {
        switch (e.getID()) {
        case KeyEvent.KEY_PRESSED:
          if (e.getKeyCode() == java.awt.event.KeyEvent.VK_V
              && e.isControlDown()
              && e.isAltDown()) {
            getPastePluginAction().myActionPerformed(null);
            return true;
          } else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_C
              && e.isControlDown()
              && e.isAltDown()) {
            getCopyPluginAction().myActionPerformed(null);
            return true;
          } else {
            return false;
          }
        default:
          return false;
        }
      }
    };
  }

  @Override
  public void addPluginMenu() {
  }

  // CHECKSTYLE:OFF
  @Override
  public void SBaseAdded(PluginSBase arg0) {

  }

  @Override
  public void SBaseChanged(PluginSBase arg0) {

  }

  // CHECKSTYLE:ON

  @Override
  public void SBaseDeleted(PluginSBase arg0) {
  }

  @Override
  public void modelOpened(PluginSBase arg0) {

  }

  @Override
  public void modelSelectChanged(PluginSBase arg0) {

  }

  @Override
  public void modelClosed(PluginSBase arg0) {

  }

  protected CopyPluginAction getCopyPluginAction() {
    return copyPluginAction;
  }

  protected PastePluginAction getPastePluginAction() {
    return pastePluginAction;
  }
}