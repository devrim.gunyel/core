package lcsb.mapviewer.cdplugin.copypaste;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.plugin.*;
import lcsb.mapviewer.common.SystemClipboard;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

/**
 * CD Plugin action responsible for translating annotation stored in notes to
 * xml format stored in annotations.
 * 
 * @author Piotr Gawron
 * 
 */
public class CopyAction extends CopyPasteAbstractAction {
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger(CopyAction.class.getName());

  /**
   * Method that perform annotation of a CellDesigner species listed in
   * speciesList.
   * 
   * @param plug
   *          - a plugin object
   * @param speciesList
   *          - list of species to annotate
   */
  public void performAnnotation(CopyPastePlugin plug, PluginListOf speciesList) {
    SystemClipboard sc = new SystemClipboard();

    int size = speciesList.size();

    PluginSBase chosenElement = null;
    String id = "";
    for (int i = 0; i < size; i++) {
      PluginSBase element = speciesList.get(i);
      if (element instanceof PluginSpeciesAlias) {
        PluginSpeciesAlias alias = (PluginSpeciesAlias) element;
        PluginSpecies species = alias.getSpecies();

        if (chosenElement == null) {
          chosenElement = species;
          id = species.getId();
        } else if (id.compareTo(species.getId()) < 0) {
          chosenElement = species;
          id = species.getId();
        }
      } else if (element instanceof PluginReaction) {
        PluginReaction reaction = (PluginReaction) element;
        if (chosenElement == null) {
          chosenElement = reaction;
          id = reaction.getId();
        } else if (id.compareTo(reaction.getId()) < 0) {
          chosenElement = reaction;
          id = reaction.getId();
        }
      } else {
        logger.warn("Unknown class type :" + element.getClass());
      }
    }

    if (chosenElement != null) {
      try {
        sc.setClipboardContents(getCopyString(chosenElement.getAnnotationString(), chosenElement.getNotesString()));
      } catch (InvalidXmlSchemaException e) {
        logger.warn("Problem with creating copy-paste String", e);
      }
    }
  }

}
