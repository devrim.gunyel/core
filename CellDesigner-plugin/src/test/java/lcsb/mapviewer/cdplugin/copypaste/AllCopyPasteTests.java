package lcsb.mapviewer.cdplugin.copypaste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CopyActionTest.class,
    CopyPasteAbstractActionTest.class,
    CopyPastePluginTest.class,
    PasteActionTest.class,
})
public class AllCopyPasteTests {

}
