package lcsb.mapviewer.cdplugin.copypaste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;

import jp.sbi.celldesigner.plugin.PluginListOf;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;
import lcsb.mapviewer.common.SystemClipboard;

public class CopyActionTest extends CdPluginFunctions {
  Logger logger = LogManager.getLogger(CopyActionTest.class);

  SystemClipboard cp = new SystemClipboard();

  @Test
  public void testCopyFromSpecies() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithSpecies(2);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertNotEquals("Data in clipboard didn't change", data, newData);
  }

  @Test
  public void testCopyFromReaction() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithReaction(2);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertNotEquals("Data in clipboard didn't change", data, newData);
  }

  @Test
  public void testCopyFromEmptyList() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithSpecies(0);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertEquals("Data in clipboard didn't change", data, newData);
  }

}
