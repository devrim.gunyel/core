package lcsb.mapviewer.cdplugin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.cdplugin.copypaste.AllCopyPasteTests;
import lcsb.mapviewer.cdplugin.info.AllInfoTests;

@RunWith(Suite.class)
@SuiteClasses({ AllCopyPasteTests.class,
    AllInfoTests.class })
public class AllCdPluginTests {

}
