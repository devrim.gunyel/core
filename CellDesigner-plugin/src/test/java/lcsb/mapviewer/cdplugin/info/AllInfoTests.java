package lcsb.mapviewer.cdplugin.info;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ InfoFrameTest.class,
    InfoPluginTest.class })
public class AllInfoTests {

}
