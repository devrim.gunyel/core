package lcsb.mapviewer.cdplugin.info;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class InfoFrameTest extends CdPluginFunctions {
  Logger logger = LogManager.getLogger(InfoFrameTest.class);

  @Test
  public void testSetEmptySpeciesList() {
    InfoFrame frame = InfoFrame.getInstance();
    List<PluginSpeciesAlias> list = new ArrayList<>();
    frame.setSpecies(list);
    assertEquals(list, frame.getSpecies());
  }

  @Test
  public void testSetNotEmptySpeciesList() {
    InfoFrame frame = InfoFrame.getInstance();
    frame.setSpecies(Arrays.asList(super.createSpeciesAlias("id"), super.createSpeciesAlias("id2")));
    assertEquals(2, frame.getSpecies().size());
  }

}
