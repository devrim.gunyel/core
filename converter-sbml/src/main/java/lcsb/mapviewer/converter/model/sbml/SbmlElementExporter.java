package lcsb.mapviewer.converter.model.sbml;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.LocalStyle;

import lcsb.mapviewer.model.map.species.Element;

public abstract class SbmlElementExporter<T extends Element, S extends org.sbml.jsbml.Symbol>
    extends SbmlBioEntityExporter<T, S> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlElementExporter.class);

  public SbmlElementExporter(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel,
      Collection<SbmlExtension> sbmlExtensions) {
    super(sbmlModel, minervaModel, sbmlExtensions);
  }

  protected void assignLayoutToGlyph(T element, AbstractReferenceGlyph speciesGlyph) {
    BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(element.getX(), element.getY(), element.getZ()));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(element.getWidth());
    dimensions.setHeight(element.getHeight());
    boundingBox.setDimensions(dimensions);
    speciesGlyph.setBoundingBox(boundingBox);

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      LocalStyle style = createStyle(element);

      assignStyleToGlyph(speciesGlyph, style);
    }

  }

  @Override
  protected LocalStyle createStyle(T element) {
    LocalStyle result = super.createStyle(element);
    ColorDefinition color = getColorDefinition(element.getFillColor());
    result.getGroup().setFill(color.getId());
    result.getGroup().setFontSize(element.getFontSize().shortValue());

    return result;
  }

}
