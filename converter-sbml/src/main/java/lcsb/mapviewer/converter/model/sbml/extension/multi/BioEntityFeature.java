package lcsb.mapviewer.converter.model.sbml.extension.multi;

import lcsb.mapviewer.model.map.species.*;

public enum BioEntityFeature {
  STRUCTURAL_STATE("Structural state", "", new Class<?>[] { Protein.class, Complex.class },
      "minerva_structural_state_"),

  POSITION_TO_COMPARTMENT("Position to compartment", MultiPackageNamingUtils.NULL_REPRESENTATION,
      new Class<?>[] { Element.class },
      "minerva_position_to_compartment_"),

  SYNONYM("Synonym", null, new Class<?>[] { Element.class },
      "minerva_synonym_"),

  FORMER_SYMBOL("Former symbol", null, new Class<?>[] { Element.class },
      "minerva_former_symbol_"),

  FULL_NAME("Full name", null, new Class<?>[] { Element.class },
      "minerva_full_name_"),

  FORMULA("Formula", null, new Class<?>[] { Element.class },
      "minerva_formula_"),

  HYPOTHETICAL("Hypothetical", null, new Class<?>[] { Element.class },
      "minerva_hypothetical_"),

  ACTIVITY("Activity", null, new Class<?>[] { Element.class },
      "minerva_activity_"),

  DIMER("Dimer", null, new Class<?>[] { Element.class },
      "minerva_dimer_"),

  CHARGE("Charge", null, new Class<?>[] { Element.class },
      "minerva_charge_"),

  ABBREVIATION("Abbreviation", null, new Class<?>[] { Element.class },
      "minerva_abbreviation_"),

  SYMBOL("Symbol", null, new Class<?>[] { Element.class },
      "minerva_symbol_");

  private String featureName;
  private String defaultValue;
  private Class<?>[] properClasses;
  private String idPrefix;

  private BioEntityFeature(String featureName, String defaultValue, Class<?>[] properClasses, String idPrefix) {
    this.featureName = featureName;
    this.defaultValue = defaultValue;
    this.properClasses = properClasses;
    this.idPrefix = idPrefix;
  }

  public String getFeatureName() {
    return featureName;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public Class<?>[] getProperClasses() {
    return properClasses;
  }

  public String getIdPrefix() {
    return idPrefix;
  }
}
