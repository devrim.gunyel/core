package lcsb.mapviewer.converter.model.sbml.reaction;

import java.awt.BasicStroke;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.*;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.render.*;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class SbmlReactionExporter extends SbmlBioEntityExporter<Reaction, org.sbml.jsbml.Reaction> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(SbmlReactionExporter.class);

  private SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter;

  private SbmlBioEntityExporter<Compartment, org.sbml.jsbml.Compartment> compartmentExporter;

  private Map<ReactionNode, SimpleSpeciesReference> speciesReferenceByReactionNode = new HashMap<>();

  public SbmlReactionExporter(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel,
      SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter,
      Collection<SbmlExtension> sbmlExtensions,
      SbmlBioEntityExporter<lcsb.mapviewer.model.map.compartment.Compartment, org.sbml.jsbml.Compartment> compartmentExporter) {
    super(sbmlModel, minervaModel, sbmlExtensions);
    this.speciesExporter = speciesExporter;
    this.compartmentExporter = compartmentExporter;
  }

  private KineticLaw createKineticLaw(Reaction reaction) throws InconsistentModelException {
    SbmlKinetics kinetics = reaction.getKinetics();
    KineticLaw result = new KineticLaw();
    for (SbmlParameter minervaParameter : kinetics.getParameters()) {
      if (getMinervaModel().getParameterById(minervaParameter.getElementId()) == null) {
        LocalParameter parameter = new LocalParameter();
        parameter.setId(minervaParameter.getElementId());
        parameter.setName(minervaParameter.getName());
        if (minervaParameter.getValue() != null) {
          parameter.setValue(minervaParameter.getValue());
        }
        if (minervaParameter.getUnits() != null) {
          parameter.setUnits(minervaParameter.getUnits().getUnitId());
        }
        result.addLocalParameter(parameter);
      }
    }

    try {
      Node node = XmlParser.getXmlDocumentFromString(kinetics.getDefinition());
      for (Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", node)) {
        String id = XmlParser.getNodeValue(ciNode).trim();
        Element element = getMinervaModel().getElementByElementId(id);
        if (element != null) {
          String sbmlId = null;
          Species species = speciesExporter.getSbmlElementByElementId(id);
          if (species != null) {
            sbmlId = species.getId();
          } else {
            sbmlId = compartmentExporter.getSbmlElementByElementId(id).getId();
          }
          ciNode.setTextContent(sbmlId);
        }
      }
      String definition = XmlParser.nodeToString(node);
      definition = definition.replace(" xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "");
      definition = definition.replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
      result.setMath(ASTNode.parseMathML(definition));
      return result;
    } catch (InvalidXmlSchemaException e) {
      throw new InconsistentModelException(e);
    }

  }

  private String getReactionId(Reaction reaction) {
    String elementId = getElementId(reaction);
    int separatorIndex = elementId.indexOf("__");
    if (separatorIndex > 0) {
      return elementId.substring(0, separatorIndex);
    }
    return elementId;
  }

  @Override
  protected Collection<Reaction> getElementList() {
    return getMinervaModel().getReactions();
  }

  @Override
  public org.sbml.jsbml.Reaction createSbmlElement(Reaction reaction) throws InconsistentModelException {
    String reactionId = getReactionId(reaction);
    org.sbml.jsbml.Reaction result = super.getSbmlElementByElementId(reactionId);
    if (result != null) {
      return result;
    }
    result = getSbmlModel().createReaction(reactionId);
    result.setReversible(reaction.isReversible());
    String sboTerm = SBOTermReactionType.getTermByType(reaction.getClass());
    if (sboTerm != null) {
      result.setSBOTerm(sboTerm);
    }
    for (Product product : reaction.getProducts()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(product.getElement()));
      SpeciesReference speciesReference = result.createProduct(sbmlSymbol);
      speciesReference.setConstant(false);
      speciesReferenceByReactionNode.put(product, speciesReference);
    }
    for (Reactant reactant : reaction.getReactants()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(reactant.getElement()));
      SpeciesReference speciesReference = result.createReactant(sbmlSymbol);
      speciesReference.setConstant(false);
      speciesReferenceByReactionNode.put(reactant, speciesReference);
    }
    for (Modifier modifier : reaction.getModifiers()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(modifier.getElement()));
      SimpleSpeciesReference speciesReference = result.createModifier(sbmlSymbol);
      speciesReferenceByReactionNode.put(modifier, speciesReference);
      String term = SBOTermModifierType.getTermByType(modifier.getClass());
      if (term != null) {
        speciesReference.setSBOTerm(term);
      }
    }
    if (reaction.getKinetics() != null) {
      result.setKineticLaw(createKineticLaw(reaction));
    }
    return result;
  }

  @Override
  protected String getSbmlIdKey(Reaction reaction) {
    return reaction.getClass().getSimpleName() + "\n" + getElementId(reaction);
  }

  @Override
  protected void assignLayoutToGlyph(Reaction reaction, AbstractReferenceGlyph glyph) {
    ReactionGlyph reactionGlyph = (ReactionGlyph) glyph;
    boolean firstReactant = true;
    reactionGlyph.setCurve(new Curve());

    for (Reactant reactant : reaction.getReactants()) {
      SpeciesReferenceGlyph reactantGlyph = createNodeGlyph(reactionGlyph, reactant);
      if (firstReactant) {
        reactantGlyph.setRole(SpeciesReferenceRole.SUBSTRATE);
        for (NodeOperator operator : reaction.getOperators()) {
          if (operator.isReactantOperator()) {
            addOperatorLineToGlyph(reactionGlyph, operator, false);
            break;
          }
        }
      } else {
        reactantGlyph.setRole(SpeciesReferenceRole.SIDESUBSTRATE);
      }
      firstReactant = false;
      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        LocalStyle style = createStyle(reactant.getLine());
        style.getGroup().setEndHead(reactant.getLine().getBeginAtd().getArrowType().name());

        assignStyleToGlyph(reactantGlyph, style);
      }
    }
    Curve curve = createCurve(reaction.getLine(), false);
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      reactionGlyph.getCurve().addCurveSegment(new LineSegment(curve.getCurveSegment(i)));
    }

    boolean firstProduct = true;
    for (Product product : reaction.getProducts()) {
      SpeciesReferenceGlyph productGlyph = createNodeGlyph(reactionGlyph, product);
      if (firstProduct) {
        productGlyph.setRole(SpeciesReferenceRole.PRODUCT);
        for (NodeOperator operator : reaction.getOperators()) {
          if (operator.isProductOperator()) {
            addOperatorLineToGlyph(reactionGlyph, operator, true);
            break;
          }
        }
      } else {
        productGlyph.setRole(SpeciesReferenceRole.SIDEPRODUCT);
      }
      firstProduct = false;

      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        LocalStyle style = createStyle(product.getLine());
        style.getGroup().setEndHead(product.getLine().getEndAtd().getArrowType().name());

        assignStyleToGlyph(productGlyph, style);
      }

    }

    for (Modifier modifier : reaction.getModifiers()) {
      SpeciesReferenceGlyph modifierGlyph = createNodeGlyph(reactionGlyph, modifier);
      if (modifier instanceof Inhibition) {
        modifierGlyph.setRole(SpeciesReferenceRole.INHIBITOR);
      } else if (modifier instanceof Trigger) {
        modifierGlyph.setRole(SpeciesReferenceRole.ACTIVATOR);
      } else {
        modifierGlyph.setRole(SpeciesReferenceRole.MODIFIER);
      }
      LocalStyle style = createStyle(modifier.getLine());
      style.getGroup().setEndHead(modifier.getLine().getEndAtd().getArrowType().name());

      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        assignStyleToGlyph(modifierGlyph, style);
      }
    }

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      assignStyleToGlyph(reactionGlyph, createStyle(reaction));
    }
    if (reactionGlyph.getBoundingBox() == null) {
      reactionGlyph.setBoundingBox(new BoundingBox());
    }
    if (reactionGlyph.getBoundingBox().getPosition() == null) {
      reactionGlyph.getBoundingBox().setPosition(curveToPoint(reactionGlyph.getCurve()));
      reactionGlyph.getBoundingBox().setDimensions(curveToDimension(reactionGlyph.getCurve()));
    }
    reactionGlyph.getBoundingBox().getPosition().setZ(reaction.getZ());

    removeColinearPoints(reactionGlyph);
  }

  private Dimensions curveToDimension(Curve curve) {
    Point p = curveToPoint(curve);
    double x = Double.MIN_VALUE;
    double y = Double.MIN_VALUE;
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      LineSegment segment = (LineSegment) curve.getCurveSegment(i);
      x = Math.max(x, segment.getStart().getX());
      x = Math.max(x, segment.getEnd().getX());

      y = Math.max(y, segment.getStart().getY());
      y = Math.max(y, segment.getEnd().getY());

    }
    return new Dimensions(x - p.getX(), y - p.getY(), 0, getSbmlModel().getLevel(), getSbmlModel().getVersion());
  }

  private Point curveToPoint(Curve curve) {
    double x = Double.MAX_VALUE;
    double y = Double.MAX_VALUE;
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      LineSegment segment = (LineSegment) curve.getCurveSegment(i);
      x = Math.min(x, segment.getStart().getX());
      x = Math.min(x, segment.getEnd().getX());

      y = Math.min(y, segment.getStart().getY());
      y = Math.min(y, segment.getEnd().getY());

    }
    return new Point(x, y);
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlElementId, String glyphId) {
    int separatorIndex = glyphId.indexOf("__");
    if (separatorIndex > 0) {
      glyphId = glyphId.substring(separatorIndex + 2);
    }
    ReactionGlyph reactionGlyph;
    try {
      // handle case when input data cannot doesn't come from SBML parser and contains
      // "__" that mess up identifier uniqueness
      reactionGlyph = getLayout().createReactionGlyph(glyphId, sbmlElementId);
    } catch (IllegalArgumentException e) {
      glyphId += "_" + getNextId();
      reactionGlyph = getLayout().createReactionGlyph(glyphId, sbmlElementId);
    }

    return reactionGlyph;
  }

  @Override
  protected LocalStyle createStyle(Reaction reaction) {
    return createStyle(reaction.getLine());
  }

  private void removeColinearPoints(ReactionGlyph glyph) {
    PolylineData line = createLine(glyph.getCurve());
    line = PolylineDataFactory.removeCollinearPoints(line);
    Curve curve = createCurve(line, false);
    glyph.setCurve(curve);
  }

  private PolylineData createLine(Curve curve) {
    PolylineData result = new PolylineData();
    if (curve.getCurveSegmentCount() > 0) {
      CurveSegment segment = curve.getCurveSegment(0);
      result.addPoint(new Point2D.Double(segment.getStart().getX(), segment.getStart().getY()));
    }
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      CurveSegment segment = curve.getCurveSegment(i);
      result.addPoint(new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY()));

    }
    return result;
  }

  private LocalStyle createStyle(PolylineData line) {
    LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
    LocalStyle style = new LocalStyle();
    style.setGroup(new RenderGroup());
    renderInformation.addLocalStyle(style);
    ColorDefinition color = getColorDefinition(line.getColor());
    style.getGroup().setStroke(color.getId());
    style.getGroup().setFill(color.getId());
    style.getGroup().setStrokeWidth(line.getWidth());
    style.getGroup().setStrokeDashArray(strokeToArray(line.getType().getStroke()));

    return style;
  }

  private List<Short> strokeToArray(Stroke stroke) {
    if (stroke instanceof BasicStroke) {
      BasicStroke basicStroke = (BasicStroke) stroke;
      if (basicStroke.getDashArray() != null) {
        List<Short> result = new ArrayList<>();

        for (float entry : basicStroke.getDashArray()) {
          result.add((short) entry);
        }
        return result;
      }
    }
    return null;
  }

  private void addOperatorLineToGlyph(ReactionGlyph reactantGlyph, NodeOperator operator, boolean reverse) {
    Curve curve = reactantGlyph.getCurve();
    List<Line2D> lines = operator.getLine().getLines();
    if (reverse) {
      lines = operator.getLine().reverse().getLines();
    }
    for (Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1()));
        segment.setEnd(new Point(line.getX2(), line.getY2()));
        curve.addCurveSegment(segment);
      }
    }
  }

  private SpeciesReferenceGlyph createNodeGlyph(ReactionGlyph reactionGlyph, ReactionNode node) {
    SpeciesReferenceGlyph reactantGlyph = reactionGlyph.createSpeciesReferenceGlyph("node_" + getNextId());
    reactantGlyph.setSpeciesGlyph(
        speciesExporter.getSbmlGlyphByElementId(speciesExporter.getElementId(node.getElement())).getId());
    Curve curve = createCurve(node.getLine(), node instanceof Reactant);
    if (curve.getCurveSegmentCount() == 0) {
      logger.warn(new ElementUtils().getElementTag(node) + " Problematic line");
    }
    reactantGlyph.setCurve(curve);
    reactantGlyph.setSpeciesReference(speciesReferenceByReactionNode.get(node));
    return reactantGlyph;
  }

  private Curve createCurve(PolylineData polyline, boolean reverse) {
    Curve curve = new Curve();
    List<Line2D> lines = polyline.getLines();
    if (reverse) {
      lines = polyline.reverse().getLines();
    }
    for (Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1()));
        segment.setEnd(new Point(line.getX2(), line.getY2()));
        curve.addCurveSegment(segment);
      }
    }
    return curve;
  }

}
