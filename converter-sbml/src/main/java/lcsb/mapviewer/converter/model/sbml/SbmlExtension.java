package lcsb.mapviewer.converter.model.sbml;

public enum SbmlExtension {
  LAYOUT,
  RENDER,
  MULTI;
}
