package lcsb.mapviewer.converter.model.sbml.units;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.*;

import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityParser;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;

public class SbmlUnitsParser extends SbmlBioEntityParser {
  Logger logger = LogManager.getLogger(SbmlUnitsParser.class);

  public SbmlUnitsParser(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  protected SbmlUnit parse(org.sbml.jsbml.UnitDefinition unitDefinition, Model sbmlModel) {
    SbmlUnit result = new SbmlUnit(unitDefinition.getId());
    result.setName(unitDefinition.getName());
    for (int i = 0; i < unitDefinition.getUnitCount(); i++) {
      Unit sbmlUnit = unitDefinition.getUnit(i);
      SbmlUnitTypeFactor factor = new SbmlUnitTypeFactor(UnitMapping.kindToUnitType(sbmlUnit.getKind()),
          (int) sbmlUnit.getExponent(), sbmlUnit.getScale(), sbmlUnit.getMultiplier());
      result.addUnitTypeFactor(factor);
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.UnitDefinition> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfUnitDefinitions();
  }

  public Collection<SbmlUnit> parseList(Model sbmlModel) {
    List<SbmlUnit> result = new ArrayList<>();
    for (org.sbml.jsbml.UnitDefinition unitDefinition : getSbmlElementList(sbmlModel)) {
      result.add(parse(unitDefinition, sbmlModel));
    }
    return result;
  }
}
