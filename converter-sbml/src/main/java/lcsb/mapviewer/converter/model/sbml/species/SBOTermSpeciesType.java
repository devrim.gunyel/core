package lcsb.mapviewer.converter.model.sbml.species;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.species.*;

public enum SBOTermSpeciesType {
  ANTISENSE_RNA(AntisenseRna.class, new String[] { "SBO:0000334" }),
  COMPLEX(Complex.class, new String[] { "SBO:0000297" }),
  DEGRADED(Degraded.class, new String[] { "SBO:0000291" }),
  DRUG(Drug.class, new String[] { "SBO:0000298" }),
  GENE(Gene.class, new String[] { "SBO:0000243" }),
  GENERIC_PROTEIN(GenericProtein.class, new String[] { "SBO:0000252" }),
  TRUNCATED_PROTEIN(TruncatedProtein.class, new String[] { "SBO:0000421" }),
  ION(Ion.class, new String[] { "SBO:0000327" }),
  ION_CHANNEL(IonChannelProtein.class, new String[] { "SBO:0000284" }),
  PHENOTYPE(Phenotype.class, new String[] { "SBO:0000358" }),
  RECEPTOR(ReceptorProtein.class, new String[] { "SBO:0000244" }),
  RNA(Rna.class, new String[] { "SBO:0000278" }),
  SIMPLE_MOLECULE(SimpleMolecule.class, new String[] { "SBO:0000247", "SBO:0000299" }),
  UNKNOWN(Unknown.class, new String[] { "SBO:0000285" }),
  ;

  private static Logger logger = LogManager.getLogger(SBOTermSpeciesType.class);
  Class<? extends Species> clazz;
  private List<String> sboTerms = new ArrayList<>();

  private SBOTermSpeciesType(Class<? extends Species> clazz, String[] inputSboTerms) {
    this.clazz = clazz;
    for (String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends Species> getTypeSBOTerm(String sboTerm) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return SimpleMolecule.class;
    }
    Class<? extends Species> result = null;
    for (SBOTermSpeciesType term : values()) {
      for (String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    if (result == null) {
      logger.warn("Don't know how to handle SBOTerm " + sboTerm + " for species");
      result = SimpleMolecule.class;
    }
    return result;
  }

  public static String getTermByType(Class<? extends Species> clazz) {
    for (SBOTermSpeciesType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  private String getSBO() {
    if (sboTerms.size() != 0) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
