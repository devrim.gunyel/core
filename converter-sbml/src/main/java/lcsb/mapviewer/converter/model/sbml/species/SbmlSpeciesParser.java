package lcsb.mapviewer.converter.model.sbml.species;

import java.awt.geom.Point2D;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.multi.*;
import org.sbml.jsbml.ext.render.LocalStyle;

import com.google.common.base.Objects;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlElementParser;
import lcsb.mapviewer.converter.model.sbml.extension.multi.BioEntityFeature;
import lcsb.mapviewer.converter.model.sbml.extension.multi.MultiPackageNamingUtils;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class SbmlSpeciesParser extends SbmlElementParser<org.sbml.jsbml.Species> {
  private static String ARTIFITIAL_SINK_ID = "sbml_artifitial_sink";
  private static String ARTIFITIAL_SOURCE_ID = "sbml_artifitial_source";
  Logger logger = LogManager.getLogger(SbmlSpeciesParser.class);

  Map<String, String> structuralStateIdMap = new HashMap<>();

  public SbmlSpeciesParser(Model model, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(model, minervaModel);
  }

  private void assignMultiData(org.sbml.jsbml.Species sbmlSpecies, Species minervaElement) {
    String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    MultiModelPlugin multiPlugin = getMultiPlugin();
    MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    if (multiExtension != null) {
      MultiSpeciesType speciesType = multiPlugin.getListOfSpeciesTypes().get(multiExtension.getSpeciesType());
      if (speciesType == null) {
        logger.warn(warnPrefix + "Species type not defined in multi extension");
      } else {
        for (SpeciesFeature feature : multiExtension.getListOfSpeciesFeatures()) {
          assignMultiFeatureData(minervaElement, speciesType, feature);
        }
        for (OutwardBindingSite site : multiExtension.getListOfOutwardBindingSites()) {
          logger.warn(
              warnPrefix + "OutwardBindingSite not supported: " + site.getComponent() + "; " + site.getBindingStatus());
        }
        for (SubListOfSpeciesFeature site : multiExtension.getListOfSubListOfSpeciesFeatures()) {
          logger.warn(
              warnPrefix + "SubListOfSpeciesFeature not supported: " + site.getComponent());
        }
      }
    }
  }

  private void assignMultiFeatureData(Species minervaElement, MultiSpeciesType speciesType,
      SpeciesFeature feature) {
    String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    String featureTypeString = feature.getSpeciesFeatureType();
    List<String> featureValues = getFeatureValues(speciesType, feature);
    if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.STRUCTURAL_STATE)) {
      String structuralStateId = null;
      for (SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
        if (featureValue.getId() != null) {
          structuralStateId = featureValue.getId();
        }
      }
      if (minervaElement instanceof Protein) {
        minervaElement.setStructuralState(createStructuralState(featureValues));
      } else if (minervaElement instanceof Complex) {
        minervaElement.setStructuralState(createStructuralState(featureValues));
      } else {
        logger.warn(warnPrefix + "Structural state not supported");
      }
      structuralStateIdMap.put(minervaElement.getElementId(), structuralStateId);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.POSITION_TO_COMPARTMENT)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Position to compartment must have exactly one value");
      } else {
        minervaElement.setPositionToCompartment(PositionToCompartment.getByString(featureValues.get(0)));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.SYNONYM)) {
      minervaElement.setSynonyms(featureValues);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FORMER_SYMBOL)) {
      minervaElement.setFormerSymbols(featureValues);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.DIMER)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Dimer must have exactly one value");
      } else {
        try {
          minervaElement.setHomodimer(Integer.parseInt(featureValues.get(0)));
        } catch (NumberFormatException e) {
          logger.warn(warnPrefix + "Dimer must have integer value, instead found: " + featureValues.get(0));
        }
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.CHARGE)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Charge must have exactly one value");
      } else {
        try {
          minervaElement.setCharge(Integer.parseInt(featureValues.get(0)));
        } catch (NumberFormatException e) {
          logger.warn(warnPrefix + "Charge must have integer value, instead found: " + featureValues.get(0));
        }
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.SYMBOL)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Symbol must have exactly one value");
      } else {
        minervaElement.setSymbol(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FULL_NAME)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Full name must have exactly one value");
      } else {
        minervaElement.setFullName(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FORMULA)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Formula must have exactly one value");
      } else {
        minervaElement.setFormula(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.ABBREVIATION)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Abbreviation must have exactly one value");
      } else {
        minervaElement.setAbbreviation(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.HYPOTHETICAL)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Hypothetical must have exactly one value");
      } else {
        Boolean value = null;
        if (featureValues.get(0).equalsIgnoreCase("true")) {
          value = true;
        } else if (featureValues.get(0).equalsIgnoreCase("false")) {
          value = false;
        } else {
          logger.warn(warnPrefix + "Hypothetical must be true/false value. Found: " + featureValues.get(0));
        }
        minervaElement.setHypothetical(value);
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.ACTIVITY)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Activity must have exactly one value");
      } else {
        Boolean value = null;
        if (featureValues.get(0).equalsIgnoreCase("true")) {
          value = true;
        } else if (featureValues.get(0).equalsIgnoreCase("false")) {
          value = false;
        } else {
          logger.warn(warnPrefix + "Activity must be true/false value. Found: " + featureValues.get(0));
        }
        minervaElement.setActivity(value);
      }
    } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString)) {
      createModificationResidues(minervaElement, speciesType, feature);
    } else {
      logger.warn(warnPrefix + "Feature not supported: " + featureTypeString);
    }
  }

  private StructuralState createStructuralState(List<String> featureValues) {
    StructuralState structuralState = new StructuralState();
    structuralState.setValue(String.join("; ", featureValues));
    return structuralState;
  }

  private List<String> getFeatureValues(MultiSpeciesType speciesType, SpeciesFeature feature) {
    SpeciesFeatureType featureType = speciesType.getListOfSpeciesFeatureTypes().get(feature.getSpeciesFeatureType());

    List<String> result = new ArrayList<>();
    if (featureType != null) {
      for (SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
        PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = featureType.getListOfPossibleSpeciesFeatureValues()
            .get(featureValue.getValue());
        if (possibleSpeciesFeatureValue.getName().equals(MultiPackageNamingUtils.NULL_REPRESENTATION)) {
          result.add(null);
        } else {
          result.add(possibleSpeciesFeatureValue.getName());
        }
      }
    } else {
      for (SpeciesTypeInstance speciesTypeInstance : speciesType.getListOfSpeciesTypeInstances()) {
        speciesType = getMultiPlugin().getSpeciesType(speciesTypeInstance.getSpeciesType());
        result = getFeatureValues(speciesType, feature);
        if (result.size() > 0) {
          return result;
        }
      }

    }
    return result;
  }

  private void createModificationResidues(Species minervaElement, MultiSpeciesType speciesType,
      SpeciesFeature feature) {
    String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    ModificationResidue mr = null;

    String featureTypeString = feature.getSpeciesFeatureType();
    SpeciesFeatureType featureType = speciesType.getListOfSpeciesFeatureTypes().get(featureTypeString);
    for (SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, BindingRegion.class)) {
        mr = new BindingRegion();
        if (minervaElement instanceof SpeciesWithBindingRegion) {
          ((SpeciesWithBindingRegion) minervaElement).addBindingRegion((BindingRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, CodingRegion.class)) {
        mr = new CodingRegion();
        if (minervaElement instanceof SpeciesWithCodingRegion) {
          ((SpeciesWithCodingRegion) minervaElement).addCodingRegion((CodingRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, ProteinBindingDomain.class)) {
        mr = new ProteinBindingDomain();
        if (minervaElement instanceof SpeciesWithProteinBindingDomain) {
          ((SpeciesWithProteinBindingDomain) minervaElement).addProteinBindingDomain((ProteinBindingDomain) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, RegulatoryRegion.class)) {
        mr = new RegulatoryRegion();
        if (minervaElement instanceof SpeciesWithRegulatoryRegion) {
          ((SpeciesWithRegulatoryRegion) minervaElement).addRegulatoryRegion((RegulatoryRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, TranscriptionSite.class)) {
        mr = new TranscriptionSite();
        if (minervaElement instanceof SpeciesWithTranscriptionSite) {
          TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
          transcriptionSite.setActive(
              MultiPackageNamingUtils.getTranscriptionFactorActiveStateFromFeatureTypeName(featureTypeString));
          transcriptionSite.setDirection(
              MultiPackageNamingUtils.getTranscriptionFactorDirectionStateFromFeatureTypeName(featureTypeString));
          ((SpeciesWithTranscriptionSite) minervaElement).addTranscriptionSite(transcriptionSite);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, ModificationSite.class)) {
        mr = new ModificationSite();
        ((ModificationSite) mr)
            .setState(MultiPackageNamingUtils.getModificationStateFromFeatureTypeName(featureTypeString));
        if (minervaElement instanceof SpeciesWithModificationSite) {
          ((SpeciesWithModificationSite) minervaElement).addModificationSite((ModificationSite) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, Residue.class)) {
        mr = new Residue();
        ((Residue) mr)
            .setState(MultiPackageNamingUtils.getModificationStateFromFeatureTypeName(featureTypeString));
        if (minervaElement instanceof SpeciesWithResidue) {
          ((SpeciesWithResidue) minervaElement).addResidue((Residue) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else {
        logger.warn(warnPrefix + "Unsupported modification type: " + featureTypeString);
      }
      PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = featureType.getListOfPossibleSpeciesFeatureValues()
          .get(featureValue.getValue());
      if (possibleSpeciesFeatureValue != null) {
        mr.setName(possibleSpeciesFeatureValue.getName());
        mr.setIdModificationResidue(featureValue.getId());
      } else {
        logger.warn(warnPrefix + "Invalid feature value: " + featureValue.getValue());
      }
    }

  }

  private String extractSBOTermFromSpecies(org.sbml.jsbml.Species species) {
    String sboTerm = species.getSBOTermID();
    MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) species.getExtension("multi");
    if (multiExtension != null) {
      MultiSpeciesType speciesType = getMultiPlugin().getListOfSpeciesTypes().get(multiExtension.getSpeciesType());
      if (speciesType != null) {
        String sboTerm2 = speciesType.getSBOTermID();
        if (sboTerm != null && !sboTerm.isEmpty() && !sboTerm2.equals(sboTerm)) {
          logger.warn("Different SBO terms defining species and speciesType: " + species.getId() + ". " + sboTerm + ";"
              + sboTerm2);
        } else {
          sboTerm = sboTerm2;
        }
      }
    }
    return sboTerm;
  }

  private void assignModificationResiduesLayout(Element element) {
    if (element instanceof SpeciesWithModificationResidue) {
      for (ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
        GeneralGlyph residueGlyph = getResidueGlyphForModification(mr);
        if (residueGlyph != null) {
          if (residueGlyph.getBoundingBox() == null || residueGlyph.getBoundingBox().getDimensions() == null) {
            logger.warn(new ElementUtils().getElementTag(mr) + "Layout doesn't contain coordinates");
          } else {
            double width = residueGlyph.getBoundingBox().getDimensions().getWidth();
            double height = residueGlyph.getBoundingBox().getDimensions().getHeight();
            double x = residueGlyph.getBoundingBox().getPosition().getX();
            double y = residueGlyph.getBoundingBox().getPosition().getY();
            mr.setPosition(new Point2D.Double(x + width / 2, y + height / 2));
            if (mr instanceof AbstractRegionModification) {
              ((AbstractRegionModification) mr).setWidth(width);
              ((AbstractRegionModification) mr).setHeight(height);
            }
          }
        }
      }
    }

  }

  private GeneralGlyph getResidueGlyphForModification(ModificationResidue mr) {
    GeneralGlyph residueGlyph = null;
    for (GraphicalObject graphicalObject : getLayout().getListOfAdditionalGraphicalObjects()) {
      if (graphicalObject instanceof GeneralGlyph) {
        if (((GeneralGlyph) graphicalObject).getReference().equals(mr.getIdModificationResidue())) {
          if (((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().size() > 0) {
            // find a reference to the alias in layout, so we know it's the proper value
            ReferenceGlyph referenceGlyph = ((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().get(0);
            if (Objects.equal(referenceGlyph.getGlyph(), mr.getSpecies().getElementId())) {
              // if (referenceGlyph.getGlyph().equals(mr.getSpecies().getElementId())) {
              residueGlyph = (GeneralGlyph) graphicalObject;
            }
          } else {
            residueGlyph = (GeneralGlyph) graphicalObject;
          }
        }
      }
    }
    return residueGlyph;
  }

  private GeneralGlyph getResidueGlyphForStructuralState(StructuralState mr) {
    GeneralGlyph residueGlyph = null;
    String structurlaStateId = structuralStateIdMap.get(mr.getSpecies().getElementId());
    for (GraphicalObject graphicalObject : getLayout().getListOfAdditionalGraphicalObjects()) {
      if (graphicalObject instanceof GeneralGlyph) {
        if (Objects.equal(((GeneralGlyph) graphicalObject).getReference(), structurlaStateId)) {
          if (((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().size() > 0) {
            // find a reference to the alias in layout, so we know it's the proper value
            ReferenceGlyph referenceGlyph = ((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().get(0);
            if (Objects.equal(referenceGlyph.getGlyph(), mr.getSpecies().getElementId())) {
              // if (referenceGlyph.getGlyph().equals(mr.getSpecies().getElementId())) {
              residueGlyph = (GeneralGlyph) graphicalObject;
            }
          } else {
            residueGlyph = (GeneralGlyph) graphicalObject;
          }
        }
      }
    }
    return residueGlyph;
  }

  private void assignCompartment(Element element, String compartmentId) {
    Compartment compartment = getMinervaModel().getElementByElementId(compartmentId);
    if (compartment == null && getLayout() != null) {
      List<Compartment> compartments = new ArrayList<>();
      for (CompartmentGlyph glyph : getLayout().getListOfCompartmentGlyphs()) {
        if (glyph.getCompartment().equals(compartmentId) && !glyph.getCompartment().equals("default")) {
          compartments.add(getMinervaModel().getElementByElementId(glyph.getId()));
        }
      }
      for (Compartment compartment2 : compartments) {
        if (compartment2.contains(element)) {
          compartment = compartment2;
        }
      }

    }
    if (compartment != null) {
      compartment.addElement(element);
    }

  }

  @Override
  protected ListOf<org.sbml.jsbml.Species> getSbmlElementList() {
    return getSbmlModel().getListOfSpecies();
  }

  @Override
  public List<Element> mergeLayout(List<? extends Element> elements)
      throws InvalidInputDataExecption {
    List<Element> result = super.mergeLayout(elements);

    for (Element element : result) {
      String compartmentId = null;
      String speciesId = element.getElementId();
      if (getLayout().getSpeciesGlyph(element.getElementId()) != null) {
        compartmentId = ((org.sbml.jsbml.Species) getLayout().getSpeciesGlyph(element.getElementId())
            .getSpeciesInstance()).getCompartment();
        speciesId = ((org.sbml.jsbml.Species) getLayout().getSpeciesGlyph(element.getElementId()).getSpeciesInstance())
            .getId();
      } else {
        if (!element.getElementId().equals(ARTIFITIAL_SINK_ID)
            && !element.getElementId().equals(ARTIFITIAL_SOURCE_ID)) {
          compartmentId = getSbmlModel().getSpecies(element.getElementId()).getCompartment();
        }
      }
      structuralStateIdMap.put(element.getElementId(), structuralStateIdMap.get(speciesId));
      assignCompartment(element, compartmentId);
      assignModificationResiduesLayout(element);
      assignStructuralStateLayout(element);
    }
    return result;
  }

  private void assignStructuralStateLayout(Element element) {
    if (element instanceof Protein || element instanceof Complex) {
      StructuralState state = ((Species) element).getStructuralState();
      if (state != null) {
        state.setFontSize(10);
        state.setHeight(20);
        state.setWidth(element.getWidth());
        state.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
        GeneralGlyph residueGlyph = getResidueGlyphForStructuralState(state);
        if (residueGlyph != null) {
          if (residueGlyph.getBoundingBox() == null || residueGlyph.getBoundingBox().getDimensions() == null) {
            logger.warn(
                new ElementUtils().getElementTag(element) + " Layout doesn't contain coordinates for structural state");
          } else {
            double width = residueGlyph.getBoundingBox().getDimensions().getWidth();
            double height = residueGlyph.getBoundingBox().getDimensions().getHeight();
            double x = residueGlyph.getBoundingBox().getPosition().getX();
            double y = residueGlyph.getBoundingBox().getPosition().getY();
            state.setPosition(new Point2D.Double(x, y));
            state.setWidth(width);
            state.setHeight(height);
          }
        }

      }
    }
  }

  @Override
  protected void applyStyleToElement(Element elementWithLayout, LocalStyle style) {
    super.applyStyleToElement(elementWithLayout, style);
    Species specisWithLayout = (Species) elementWithLayout;
    if (style.getGroup().isSetStrokeWidth()) {
      specisWithLayout.setLineWidth(style.getGroup().getStrokeWidth());
    }
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs() {
    List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (SpeciesGlyph glyph : getLayout().getListOfSpeciesGlyphs()) {
      result.add(new Pair<String, AbstractReferenceGlyph>(glyph.getSpecies(), glyph));
    }
    return result;
  }

  @SuppressWarnings("deprecation")
  @Override
  protected Species parse(org.sbml.jsbml.Species species) throws InvalidInputDataExecption {
    String sboTerm = extractSBOTermFromSpecies(species);
    Class<? extends Species> clazz = SBOTermSpeciesType.getTypeSBOTerm(sboTerm);
    try {
      Species result = clazz.getConstructor(String.class).newInstance(species.getId());
      if (!Double.isNaN(species.getInitialAmount())) {
        result.setInitialAmount(species.getInitialAmount());
      }
      if (!Double.isNaN(species.getInitialConcentration())) {
        result.setInitialConcentration(species.getInitialConcentration());
      }
      if (species.isSetHasOnlySubstanceUnits()) {
        result.setOnlySubstanceUnits(species.hasOnlySubstanceUnits());
      }
      if (species.isSetBoundaryCondition()) {
        result.setBoundaryCondition(species.getBoundaryCondition());
      }
      if (species.isSetConstant()) {
        result.setConstant(species.getConstant());
      }
      if (species.isSetCharge()) {
        result.setCharge(species.getCharge());
      }
      assignBioEntityData(species, result);
      if (getLayout() == null) {
        assignCompartment(result, species.getCompartment());
      }
      if (getMultiPlugin() != null) {
        assignMultiData(species, result);
      }
      return result;
    } catch (SecurityException | NoSuchMethodException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException e) {
      throw new InvalidStateException(e);
    }
  }

  public Species getArtifitialInput() {
    Species result = getMinervaModel().getElementByElementId(ARTIFITIAL_SOURCE_ID);
    if (result == null) {
      result = new Unknown(ARTIFITIAL_SOURCE_ID);
      result.setName("Artifitial source");
      getMinervaModel().addElement(result);
    }
    return result;
  }

  public Species getArtifitialOutput() {
    Species result = getMinervaModel().getElementByElementId(ARTIFITIAL_SINK_ID);
    if (result == null) {
      result = new Degraded(ARTIFITIAL_SINK_ID);
      result.setName("Artifitial sink");
      getMinervaModel().addElement(result);
    }
    return result;
  }

}
