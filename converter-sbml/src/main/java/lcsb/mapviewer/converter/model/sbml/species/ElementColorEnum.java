package lcsb.mapviewer.converter.model.sbml.species;

import java.awt.*;

import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.*;

public enum ElementColorEnum {
  ANTISENSE_RNA(AntisenseRna.class, "#ff6666"),
  COMPLEX(Complex.class, "#f7f7f7"),
  DEGRADED(Degraded.class, "#ffcccc"),
  DRUG(Drug.class, "#ff00ff"),
  ELEMENT(Element.class, "#000000"),
  GENE(Gene.class, "#ffff66"),
  GENERIC_PROTEIN(GenericProtein.class, "#ccffcc"),
  TRUNCATED_PROTEIN(TruncatedProtein.class, "#ffcccc"),
  ION(Ion.class, "#9999ff"),
  ION_CHANNEL(IonChannelProtein.class, "#ccffff"),
  OVAL_COMPARTMENT(OvalCompartment.class, "#cccc00"),
  PHENOTYPE(Phenotype.class, "#cc99ff"),
  RECEPTOR(ReceptorProtein.class, "#ffffcc"),
  RNA(Rna.class, "#66ff66"),
  SIMPLE_MOLECULE(SimpleMolecule.class, "#ccff66"),
  SQUARE_COMPARTMENT(SquareCompartment.class, "#cccc00"),
  UNKNOWN(Unknown.class, "#cccccc"),
  ;

  ColorParser colorParser = new ColorParser();
  private Class<? extends Element> clazz;
  private Color color;

  private ElementColorEnum(Class<? extends Element> clazz, String color) {
    this.clazz = clazz;
    if (color != null) {
      this.color = colorParser.parse(color);
    }
  }

  public static Color getColorByClass(Class<? extends Element> clazz) {
    Color result = null;
    for (ElementColorEnum type : ElementColorEnum.values()) {
      if (type.getClazz().equals(clazz)) {
        result = type.getColor();
      }
    }
    if (result == null) {
      for (ElementColorEnum type : ElementColorEnum.values()) {
        if (type.getClazz().isAssignableFrom(clazz)) {
          result = type.getColor();
        }
      }
    }
    return result;
  }

  public Class<? extends Element> getClazz() {
    return clazz;
  }

  public Color getColor() {
    return color;
  }

}
