package lcsb.mapviewer.converter.model.sbml;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.text.StringEscapeUtils;
import org.sbml.jsbml.AbstractNamedSBase;

import lcsb.mapviewer.converter.InvalidInputDataExecption;

/**
 * This utility class parses notes from SBML node and prepares escaped string
 * ready to use in SBML.
 * 
 * @author Piotr Gawron
 *
 */
public class NotesUtility {

  /**
   * Extract notes from SBML node
   * 
   * @param sbmlElement
   *          SBML node
   * @return notes
   * @throws InvalidInputDataExecption
   *           thrown when there is problem with extracting notes
   */
  public static String extractNotes(AbstractNamedSBase sbmlElement) throws InvalidInputDataExecption {
    String notes = "";
    try {
      notes = sbmlElement.getNotesString();
    } catch (XMLStreamException e) {
      throw new InvalidInputDataExecption(sbmlElement.getId() + " Invalid notes", e);
    }
    if (sbmlElement.getNotes() != null) {
      if (sbmlElement.getNotes().getChildCount() > 1) {
        if (sbmlElement.getNotes().getChild(1).getChildCount() > 1) {
          if (sbmlElement.getNotes().getChild(1).getChild(1).getChildCount() > 0) {
            notes = sbmlElement.getNotes().getChild(1).getChild(1).getChild(0).getCharacters();
          } else {
            notes = sbmlElement.getNotes().getChild(1).getChild(1).getCharacters();
          }
        }
      }
    }
    return notes;
  }

  /**
   * Prepares escaped xml string with notes.
   * 
   * @param notes
   *          notes to be processed
   * @return escaped xml string with notes
   */
  public static String prepareEscapedXmlNotes(String notes) {
    if (notes == null) {
      return "";
    }
    return StringEscapeUtils.escapeXml10(notes);
  }

}
