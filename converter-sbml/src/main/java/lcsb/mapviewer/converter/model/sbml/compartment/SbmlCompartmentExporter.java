package lcsb.mapviewer.converter.model.sbml.compartment;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.*;
import org.sbml.jsbml.ext.multi.MultiCompartmentPlugin;
import org.sbml.jsbml.ext.render.*;

import lcsb.mapviewer.converter.model.sbml.SbmlElementExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;

public class SbmlCompartmentExporter extends SbmlElementExporter<Compartment, org.sbml.jsbml.Compartment> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlCompartmentExporter.class);

  public SbmlCompartmentExporter(Model sbmlModel, lcsb.mapviewer.model.map.model.Model minervaModel,
      Collection<SbmlExtension> sbmlExtensions) {
    super(sbmlModel, minervaModel, sbmlExtensions);
  }

  @Override
  protected List<Compartment> getElementList() {
    List<Compartment> result = new ArrayList<>();
    result.addAll(getMinervaModel().getCompartments());
    boolean defaultFound = false;
    for (Compartment compartment : result) {
      if (getElementId(compartment).equals("default")) {
        defaultFound = true;
      }
    }
    if (!defaultFound) {
      Compartment defaultCompartment = new Compartment("default");
      result.add(defaultCompartment);
    }
    return result;
  }

  @Override
  public org.sbml.jsbml.Compartment createSbmlElement(Compartment element) throws InconsistentModelException {
    org.sbml.jsbml.Compartment result;
    if (element == null || getElementId(element).equals("default")) {
      result = getSbmlModel().createCompartment("default");
    } else {
      result = getSbmlModel().createCompartment("comp_" + (getNextId()));
    }
    // for now we don't have this information - needed for validation
    result.setConstant(false);
    result.setValue(1.0);
    result.setSpatialDimensions(3);
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      MultiCompartmentPlugin multiExtension = new MultiCompartmentPlugin(result);
      multiExtension.setIsType(false);
      result.addExtension("multi", multiExtension);
    }
    return result;
  }

  @Override
  public org.sbml.jsbml.Compartment getSbmlElement(Compartment element) throws InconsistentModelException {
    String mapKey = getSbmlIdKey(element);
    if (element == null && sbmlElementByElementNameAndCompartmentName.get(mapKey) == null) {
      org.sbml.jsbml.Compartment sbmlElement = createSbmlElement(element);
      sbmlElementByElementNameAndCompartmentName.put(mapKey, sbmlElement);
    }
    return super.getSbmlElement(element);
  }

  @Override
  protected String getSbmlIdKey(Compartment compartment) {
    if (compartment == null || getElementId(compartment).equals("default")) {
      return "default";
    }
    return compartment.getName();
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlCompartmentId, String glyphId) {
    if (sbmlCompartmentId.equals("default")) {
      glyphId = "default_compartment";
    }
    CompartmentGlyph result = getLayout().createCompartmentGlyph(glyphId, sbmlCompartmentId);
    return result;
  }

  @Override
  protected LocalStyle createStyle(Compartment element) {
    LocalStyle style = super.createStyle(element);
    style.getGroup().setStrokeWidth(element.getThickness());
    style.getGroup().setFill(null);
    style.getGroup().setStroke(getColorDefinition(element.getFillColor()).getId());
    if (element instanceof OvalCompartment) {
      Ellipse ellipse = new Ellipse();
      ellipse.setAbsoluteRx(false);
      ellipse.setAbsoluteRy(false);
      ellipse.setAbsoluteCx(false);
      ellipse.setAbsoluteCy(false);
      ellipse.setCx(50);
      ellipse.setCy(50.0);
      ellipse.setRx(50.0);
      ellipse.setRy(50.0);
      style.getGroup().addElement(ellipse);
    } else {
      Rectangle rectangle = new Rectangle();
      rectangle.setAbsoluteRx(false);
      rectangle.setAbsoluteRy(false);
      rectangle.setAbsoluteX(false);
      rectangle.setAbsoluteY(false);
      rectangle.setAbsoluteHeight(false);
      rectangle.setAbsoluteWidth(false);
      rectangle.setRx(0);
      rectangle.setRy(0);
      rectangle.setX(0);
      rectangle.setY(0);
      rectangle.setWidth(100);
      rectangle.setHeight(100);
      style.getGroup().addElement(rectangle);
    }

    return style;
  }

  @Override
  protected void assignLayoutToGlyph(Compartment element, AbstractReferenceGlyph speciesGlyph) {
    if (getElementId(element).equals("default")) {
      BoundingBox boundingBox = new BoundingBox();

      boundingBox.setPosition(new Point(element.getX(), element.getY(), 0));
      Dimensions dimensions = new Dimensions();

      if (getMinervaModel().getWidth() > 0) {
        dimensions.setWidth(getMinervaModel().getWidth());
        dimensions.setHeight(getMinervaModel().getHeight());
      }
      boundingBox.setDimensions(dimensions);
      speciesGlyph.setBoundingBox(boundingBox);
    } else {
      super.assignLayoutToGlyph(element, speciesGlyph);
      TextGlyph textGlyph = getLayout().createTextGlyph("text_" + getElementId(element), element.getName());
      Point2D point = element.getNamePoint();
      double width = element.getWidth() - (point.getX() - element.getX());
      double height = element.getHeight() - (point.getY() - element.getY());
      textGlyph.setBoundingBox(createBoundingBox(point.getX(), point.getY(), element.getZ() + 1, width, height));
      textGlyph.setReference(speciesGlyph.getId());

      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
        LocalStyle style = new LocalStyle();
        style.getIDList().add(textGlyph.getId());
        style.setGroup(new RenderGroup());
        style.getGroup().setTextAnchor(HTextAnchor.START);
        style.getGroup().setVTextAnchor(VTextAnchor.TOP);
        renderInformation.addLocalStyle(style);
      }
    }
  }

}
