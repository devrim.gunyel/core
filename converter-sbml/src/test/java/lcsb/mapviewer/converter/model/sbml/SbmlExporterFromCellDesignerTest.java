package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class SbmlExporterFromCellDesignerTest extends SbmlTestFunctions {
  Logger logger = LogManager.getLogger(SbmlExporterFromCellDesignerTest.class);

  CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();

  ModelComparator comparator = new ModelComparator();

  @Test
  public void testExportHeterodimerAssociation() throws Exception {
    Model originalModel = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/heterodimer_association.xml"));
    Model model = getModelAfterSerializing(originalModel);
    Reaction reaction1 = originalModel.getReactions().iterator().next();
    Reaction reaction2 = model.getReactions().iterator().next();
    // change reaction id - due to some issues it cannot be persisted properly in
    // sbml format
    reaction2.setIdReaction(reaction1.getIdReaction());

    model.setName(originalModel.getName());

    String cellDesignerXml = cellDesignerXmlParser.model2String(model);
    ByteArrayInputStream stream = new ByteArrayInputStream(cellDesignerXml.getBytes("UTF-8"));

    assertEquals(0, comparator.compare(model, originalModel));
    Model cdModel = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(stream));

    cdModel.setName(originalModel.getName());

    assertEquals(0, comparator.compare(model, cdModel));
  }

}
