package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;

public class SbmlSpeciesParserTest {
  Logger logger = LogManager.getLogger(SbmlSpeciesParserTest.class);
  SbmlParser parser = new SbmlParser();

  ColorParser colorParser = new ColorParser();

  @Test
  public void testParseAntisenseRna() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/antisense_rna.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof AntisenseRna);
    assertEquals(ElementColorEnum.ANTISENSE_RNA.getColor(), element.getFillColor());
  }

  @Test
  public void testParseComplex() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/complex.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Complex);
    assertEquals(ElementColorEnum.COMPLEX.getColor(), element.getFillColor());
  }

  @Test
  public void testParseDegraded() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/degraded.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Degraded);
    assertEquals(ElementColorEnum.DEGRADED.getColor(), element.getFillColor());
  }

  @Test
  public void testParseDrug() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/drug.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Drug);
    assertEquals(ElementColorEnum.DRUG.getColor(), element.getFillColor());
  }

  @Test
  public void testParseGene() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/gene.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Gene);
    assertEquals(ElementColorEnum.GENE.getColor(), element.getFillColor());
  }

  @Test
  public void testParseGenericProtein() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/generic_protein.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof GenericProtein);
    assertEquals(ElementColorEnum.GENERIC_PROTEIN.getColor(), element.getFillColor());
  }

  @Test
  public void testParseIonChannel() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion_channel.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof IonChannelProtein);
    assertEquals(ElementColorEnum.ION_CHANNEL.getColor(), element.getFillColor());
  }

  @Test
  public void testParseIon() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Ion);
    assertEquals(ElementColorEnum.ION.getColor(), element.getFillColor());
  }

  @Test
  public void testParsePhenotype() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/phenotype.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Phenotype);
    assertEquals(ElementColorEnum.PHENOTYPE.getColor(), element.getFillColor());
  }

  @Test
  public void testParseReceptor() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/receptor.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof ReceptorProtein);
    assertEquals(ElementColorEnum.RECEPTOR.getColor(), element.getFillColor());
  }

  @Test
  public void testParseRna() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/rna.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Rna);
    assertEquals(ElementColorEnum.RNA.getColor(), element.getFillColor());
  }

  @Test
  public void testParseSimpleMolecule() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
    assertEquals(ElementColorEnum.SIMPLE_MOLECULE.getColor(), element.getFillColor());
  }

  @Test
  public void testParseSimpleMoleculeWithAlternativeSBOTerm() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule2.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
  }

  @Test
  public void testParseUnknown() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/unknown_species.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Unknown);
    assertEquals(ElementColorEnum.UNKNOWN.getColor(), element.getFillColor());
  }

  @Test
  public void testParseInitialAmount() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/initial_amount.xml"));
    Species element = model.getElementByElementId("s1");
    assertEquals((Double) 1.0, element.getInitialAmount());
  }

  @Test
  public void testParseHasOnlySubstanceUnits() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/has_only_substance_units.xml"));
    Species element = model.getElementByElementId("s1");
    assertTrue(element.hasOnlySubstanceUnits());
  }
}
