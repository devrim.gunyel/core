package lcsb.mapviewer.converter.model.sbml.reaction;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SbmlReactionExportArrowType.class,
    SbmlReactionExporterTest.class,
    SbmlReactionParserTest.class,
    SbmlReactionParserExtractCurveTest.class
})
public class AllSbmlReactionParserTests {

}
