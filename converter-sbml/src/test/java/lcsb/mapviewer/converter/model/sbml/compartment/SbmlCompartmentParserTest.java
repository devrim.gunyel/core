package lcsb.mapviewer.converter.model.sbml.compartment;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.FileNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlCompartmentParserTest {
  Logger logger = LogManager.getLogger(SbmlCompartmentParserTest.class);

  SbmlParser parser = new SbmlParser();

  @Test
  public void testCompartmentNAmePosition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/compartment_with_text_position.xml"));
    Compartment element = model.getElementByElementId("ca1");
    assertEquals("Label position is too far from expected", 0.0,
        element.getNamePoint().distance(new Point2D.Double(202.5, 178.5)), Configuration.EPSILON);
  }

}
