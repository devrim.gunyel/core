package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class GeneratedSbmlValidationTests extends SbmlTestFunctions {
  Logger logger = LogManager.getLogger();

  Model model ;

  public GeneratedSbmlValidationTests(Model model, String name) {
    this.model= model;
  }

  @Parameters(name = "{index}: {1}")
  public static Collection<Object[]> data() throws Exception {
    List<Object[]> result = new ArrayList<>();
    result.add(createRow( createModelWithSingleSpecies() ));
    return result;
  }

  private static Object[] createRow(Model model) {
    new ZIndexPopulator().populateZIndex(model);
    return new Object[] {model, model.getName()};
  }

  private static Model createModelWithSingleSpecies() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setName("Single species");
    Species species = new GenericProtein("id");
    species.setBoundaryCondition(null);
    species.setInitialConcentration(null);
    species.setInitialAmount(null);
    species.setConstant(null);
    model.addElement(species);
    new ApplySimpleLayoutModelCommand(model).execute();
    return model;
  }
  
  @Test
  public void testIsValidSbml() throws Exception {
    SbmlParser parser = new SbmlParser();
    String xml = parser.model2String(model);

    validateSBML(xml, "xxx");
  }

}
