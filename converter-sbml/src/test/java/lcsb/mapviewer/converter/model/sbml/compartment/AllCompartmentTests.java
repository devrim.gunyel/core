package lcsb.mapviewer.converter.model.sbml.compartment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SbmlCompartmentParserTest.class })
public class AllCompartmentTests {

}
