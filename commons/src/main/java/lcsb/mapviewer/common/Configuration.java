package lcsb.mapviewer.common;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Basic configuration parameters of the system (these values cannot be modified
 * by the user, user modified values are kept in the db:
 * {@link lcsb.mapviewer.db.model.user.Configuration Configuration} ).
 * 
 * @author Piotr Gawron
 * 
 */
public final class Configuration {

  /**
   * What is the minimal zoom level in the Google Maps API. It cannot be set to 0
   * because Google Maps API was designed to visualize map of Earth which is based
   * on torus. Therefore, if we use too small zoom level we will see next to the
   * right edge elements from the left part. When we increase minimal zoom level
   * there will be a gap (huge enough) between the overlapping parts and user will
   * be unaware of that fact.
   */
  public static final int MIN_ZOOM_LEVEL = 2;
  /**
   * This constant describes minimum size (in square pixels) of object visible
   * during nesting (it is a soft limit, can be override by depth of the depending
   * tree).
   */
  public static final double MIN_VISIBLE_OBJECT_SIZE = 55000;
  /**
   * This constant describes maximum size (in square pixels) of object visible
   * during nesting.
   */
  public static final double MAX_VISIBLE_OBJECT_SIZE = 80000;
  /**
   * Where the main web page is located.
   */
  public static final String MAIN_PAGE = "/index.xhtml";
  /**
   * Name of the cookie for authentication token.
   */
  public static final String AUTH_TOKEN = "MINERVA_AUTH_TOKEN";
  /**
   * Guest account.
   */
  public static final String ANONYMOUS_LOGIN = "anonymous";
  /**
   * Epsilon used for different types of comparisons.
   */
  public static final double EPSILON = 1e-6;
  /**
   * Default value for {@link #memorySaturationRatioTriggerClean}. It defines at
   * what memory usage level application should release cached objects (to prevent
   * unnecessary out of memory exceptions).
   *
   */
  private static final double DEFAULT_MEMORY_SATURATION_TRIGGER_CLEAN = 0.9;
  /**
   * How many elements should be visible in auto-complete lists.
   */
  private static final int DEFAULT_AUTOCOMPLETE_SIZE = 5;
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(Configuration.class);
  /**
   * Max session length in seconds.
   */
  private static int sessionLength = 60 * 120;
  /**
   * Should the application cache be turned on.
   */
  private static boolean applicationCacheOn = true;
  /**
   * This constant defines at what memory usage level application should release
   * cached objects (to prevent unnecessary out of memory exceptions).
   */
  private static Double memorySaturationRatioTriggerClean = DEFAULT_MEMORY_SATURATION_TRIGGER_CLEAN;
  /**
   * What is the size of auto-complete elements.
   */
  private static int autocompleteSize = DEFAULT_AUTOCOMPLETE_SIZE;
  /**
   * Git version from which framework was built.
   */
  private static String systemBuildVersion = null;

  /**
   * Date when the framework was built.
   */
  private static String systemBuildTime = null;

  /**
   * Version of the system (used by debian package).
   */
  private static String systemVersion = null;

  /**
   * Address that should be allowed to use x-frame.
   */
  private static List<String> xFrametDomain = new ArrayList<>();

  /**
   * Should CORS be disabled.
   */
  private static boolean disableCors = false;

  /**
   * Directory where tomcat webapp folder is located. Default value is "." because
   * it should be set to proper value when tomcat application is deployed and run.
   */
  private static String webAppDir = "./";

  /**
   * Default constructor which prevents instantiation.
   */
  private Configuration() {

  }

  /**
   * @return the applicationCacheOn
   */
  public static boolean isApplicationCacheOn() {
    return applicationCacheOn;
  }

  /**
   * @param applicationCacheOn
   *          the applicationCacheOn to set
   */
  public static void setApplicationCacheOn(boolean applicationCacheOn) {
    Configuration.applicationCacheOn = applicationCacheOn;
  }

  /**
   * @return the autocompleteSize
   */
  public static int getAutocompleteSize() {
    return autocompleteSize;
  }

  /**
   * @param autocompleteSize
   *          the autocompleteSize to set
   */
  public static void setAutocompleteSize(int autocompleteSize) {
    Configuration.autocompleteSize = autocompleteSize;
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemBuildVersion(String baseDir) {
    if (systemBuildVersion == null) {
      loadSystemVersion(baseDir);
    }
    return systemBuildVersion;
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @return {@link #systemVersion}
   */
  public static String getSystemVersion(String baseDir) {
    if (systemVersion == null) {
      loadSystemVersion(baseDir);
    }
    return systemVersion;
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @param forceReload
   *          if true then forces to reload data from text file
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemBuildVersion(String baseDir, boolean forceReload) {
    if (forceReload) {
      systemBuildVersion = null;
    }
    return getSystemBuildVersion(baseDir);
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @param forceReload
   *          if true then forces to reload data from text file
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemVersion(String baseDir, boolean forceReload) {
    if (forceReload) {
      systemVersion = null;
    }
    return getSystemVersion(baseDir);
  }

  /**
   * Loads system version (git version, build date) from the file.
   * 
   * @param baseDir
   *          directory where the system is placed
   */
  protected static void loadSystemVersion(String baseDir) {
    systemBuildVersion = "Unknown";
    systemBuildTime = "Unknown";
    systemVersion = "Unknown";
    File buildVersionFile = null;
    File changelogFile = null;
    if (baseDir == null) {
      buildVersionFile = new File("version.txt");
      changelogFile = new File("CHANGELOG");
    } else {
      buildVersionFile = new File(baseDir + "version.txt");
      changelogFile = new File(baseDir + "CHANGELOG");
    }
    if (buildVersionFile.exists()) {
      loadSystemBuildVersion(buildVersionFile);
    } else {
      logger.error(buildVersionFile.getAbsoluteFile() + " doesn't exist.");
    }

    if (changelogFile.exists()) {
      loadSystemVersion(changelogFile);
    } else {
      logger.error(changelogFile.getAbsoluteFile() + " doesn't exist.");
    }
  }

  /**
   * Loads system version (git version, build date) from the file.
   * 
   * @param file
   *          file from which data is loaded
   */
  protected static void loadSystemBuildVersion(File file) {
    try {
      BufferedReader reader = new BufferedReader(new FileReader(file));
      systemBuildVersion = reader.readLine().trim();
      systemBuildTime = reader.readLine().trim();
      reader.close();
    } catch (IOException e) {
      logger.error(e);
    }
  }

  /**
   * Loads system version from debian changelog file.
   * 
   * @param file
   *          debian changelog file
   */
  protected static void loadSystemVersion(File file) {
    try {
      BufferedReader reader = new BufferedReader(new FileReader(file));
      String line = reader.readLine();

      int startIndex = line.indexOf("(");
      int endIndex = line.indexOf(")");

      if (startIndex >= 0 && endIndex >= 0) {
        systemVersion = line.substring(startIndex + 1, endIndex);
      } else {
        logger.error("Invalid CHANGELOG file. Cannot find system version in line: " + line);
      }
      reader.close();
    } catch (IOException e) {
      logger.error(e);
    }
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @return the systemBuild
   * @see #systemBuildTime
   */
  public static String getSystemBuild(String baseDir) {
    if (systemBuildTime == null) {
      loadSystemVersion(baseDir);
    }
    return systemBuildTime;
  }

  /**
   * @param baseDir
   *          directory where the system is placed
   * @param forceReload
   *          if true then forces to reload data from text file
   * @return the systemBuild
   * @see #systemBuildTime
   */
  public static String getSystemBuild(String baseDir, boolean forceReload) {
    if (forceReload) {
      systemBuildTime = null;
    }
    return getSystemBuild(baseDir);
  }

  /**
   * @return the xFrametDomain
   * @see #xFrametDomain
   */
  public static List<String> getxFrameDomain() {
    return xFrametDomain;
  }

  /**
   * @param xFrametDomains
   *          the xFrametDomain to set
   * @see #xFrametDomain
   */
  public static void setxFrameDomain(List<String> xFrametDomains) {
    Configuration.xFrametDomain = xFrametDomains;
  }

  /**
   * @return the {@link #webAppDir}
   */
  public static String getWebAppDir() {
    return Configuration.webAppDir;
  }

  /**
   * @param path
   *          the path to webapps to set
   * @see #webAppDir
   */
  public static void setWebAppDir(String path) {
    Configuration.webAppDir = path;
  }

  /**
   * @return the memorySaturationRatioTriggerClean
   * @see #memorySaturationRatioTriggerClean
   */
  public static Double getMemorySaturationRatioTriggerClean() {
    return memorySaturationRatioTriggerClean;
  }

  /**
   * @param memorySaturationRatioTriggerClean
   *          the memorySaturationRatioTriggerClean to set
   * @see #memorySaturationRatioTriggerClean
   */
  public static void setMemorySaturationRatioTriggerClean(Double memorySaturationRatioTriggerClean) {
    Configuration.memorySaturationRatioTriggerClean = memorySaturationRatioTriggerClean;
  }

  /**
   * Returns information about version of the framework.
   * 
   * @param baseDir
   *          directory where project was deployed (or information about version
   *          is stored)
   * @return information about version of the framework
   */
  public static FrameworkVersion getFrameworkVersion(String baseDir) {
    FrameworkVersion result = new FrameworkVersion();
    loadSystemVersion(baseDir);
    result.setGitVersion(getSystemBuildVersion(baseDir));
    result.setTime(getSystemBuild(baseDir));
    result.setVersion(getSystemVersion(baseDir));
    return result;
  }

  public static int getSessionLength() {
    return sessionLength;
  }

  public static void setSessionLength(int sessionLength) {
    Configuration.sessionLength = sessionLength;
  }

  public static boolean isDisableCors() {
    return disableCors;
  }

  public static void setDisableCors(boolean disableCors) {
    Configuration.disableCors = disableCors;
  }

}
