package lcsb.mapviewer.common;

import java.awt.*;
import java.awt.datatransfer.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class allowing access to system clipboard.
 * 
 * @author Piotr Gawron
 * 
 */
public class SystemClipboard implements ClipboardOwner {
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger(SystemClipboard.class);

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
  }

  /**
   * Get the String residing on the clipboard.
   *
   * @return any text found on the Clipboard; if none found, return an empty
   *         String.
   */
  public String getClipboardContents() {
    String result = null;
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    // odd: the Object param of getContents is not currently used
    Transferable contents = clipboard.getContents(null);
    boolean hasTransferableText = contents.isDataFlavorSupported(DataFlavor.stringFlavor);
    if (hasTransferableText) {
      try {
        result = (String) contents.getTransferData(DataFlavor.stringFlavor);
      } catch (Exception ex) {
        logger.error(ex, ex);
      }
    }
    return result;
  }

  /**
   * Place a String on the clipboard, and make this class the owner of the
   * Clipboard's contents.
   *
   * @param aString
   *          what we want to put into clipboard
   */
  public void setClipboardContents(String aString) {
    StringSelection stringSelection = new StringSelection(aString);
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(stringSelection, this);
  }

}
