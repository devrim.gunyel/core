package lcsb.mapviewer.common.exception;

/**
 * Exception that should be thrown when application/class/method entered to
 * invalid state.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidStateException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public InvalidStateException() {
    super();
  }

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidStateException(String string) {
    super(string);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */
  public InvalidStateException(Exception e) {
    super(e);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param string
   *          message of this exception
   * @param e
   *          parent exception
   */
  public InvalidStateException(String string, Exception e) {
    super(string, e);
  }

}
