package lcsb.mapviewer.common.exception;

/**
 * Exception that shold be thrown when the xml schema is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidXmlSchemaException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public InvalidXmlSchemaException() {
    super(); // call superclass constructor
  }

  /**
   * Constructor receives some kind of message.
   * 
   * @param err
   *          message associated with exception
   */

  public InvalidXmlSchemaException(final String err) {
    super(err);
  }

  /**
   * Constructor receives some kind of message and parent exception.
   * 
   * @param err
   *          message associated with exception
   * @param throwable
   *          parent exception
   */
  public InvalidXmlSchemaException(final String err, final Throwable throwable) {
    super(err, throwable);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */

  public InvalidXmlSchemaException(final Exception e) {
    super(e);
  }
}
