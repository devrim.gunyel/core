package lcsb.mapviewer.common.geometry;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for basic point transformations.
 * 
 * @author Piotr Gawron
 * 
 */
public class PointTransformation {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(PointTransformation.class.getName());

  /**
   * Rotates point around center using the angle.
   * 
   * @param point
   *          object that we want to rotate
   * @param angle
   *          angle by which we want to rotate
   * @param center
   *          central point around which we rotate the object
   * @return the same object rotated by the appropriate angle
   */
  public Point2D rotatePoint(final Point2D point, final double angle, final Point2D center) {
    double s = Math.sin(angle);
    double c = Math.cos(angle);

    // translate point back to origin:

    point.setLocation(point.getX() - center.getX(), point.getY() - center.getY());

    // rotate point
    double xnew = point.getX() * c - point.getY() * s;
    double ynew = point.getX() * s + point.getY() * c;

    // translate point back:
    point.setLocation(xnew + center.getX(), ynew + center.getY());
    return point;
  }

  /**
   * Checks if a point given in the parameter is valid (can be used for drawing).
   * The point is considered as valid if coordinates are finite (NaN and Infinity
   * are invalid - they cannot be drawn).
   * 
   * @param point
   *          point to check
   * @return <code>true</code> if coordinates are normal real numbers,
   *         <code>false</code> otherwise (NaN, infinity)
   */
  public boolean isValidPoint(Point2D point) {
    return Double.isFinite(point.getX()) && Double.isFinite(point.getY());
  }

  /**
   * Creates a copy of the point.
   * 
   * @param point
   *          object to be copied
   * @return copy of the object
   */
  public Point2D copyPoint(Point2D point) {
    return new Point2D.Double(point.getX(), point.getY());
  }

  /**
   * Returns a point on line.
   * 
   * @param start
   * @param end
   * @param coef
   * @return {@link Point2D} on line defined by input points
   */
  public Point2D getPointOnLine(Point2D start, Point2D end, double coef) {
    double x = start.getX() + (end.getX() - start.getX()) * coef;
    double y = start.getY() + (end.getY() - start.getY()) * coef;
    return new Point2D.Double(x, y);
  }

}
