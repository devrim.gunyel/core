package lcsb.mapviewer.common.geometry;

/**
 * Enum defining type of alignment to be used.
 * 
 * @author Piotr Gawron
 *
 */
public enum TextAlignment {

  /**
   * Text should be aligned to left.
   */
  LEFT,

  /**
   * Text should be aligned to right.
   */
  RIGHT,

  /**
   * Text should be centered.
   */
  CENTER;
}
