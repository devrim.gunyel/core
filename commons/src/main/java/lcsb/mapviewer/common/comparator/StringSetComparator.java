package lcsb.mapviewer.common.comparator;

import java.util.Comparator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for comparing sets of strings.
 * 
 * @author Piotr Gawron
 * 
 */
public class StringSetComparator implements Comparator<Set<String>> {
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(StringSetComparator.class);

  @Override
  public int compare(Set<String> arg0, Set<String> arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    for (String string : arg1) {
      if (!arg0.contains(string)) {
        logger.debug(string + " couldn't be found in " + arg0);
        return 1;
      }
    }

    for (String string : arg0) {
      if (!arg1.contains(string)) {
        logger.debug(string + " couldn't be found in " + arg1);
        return -1;
      }
    }
    return 0;
  }

}
