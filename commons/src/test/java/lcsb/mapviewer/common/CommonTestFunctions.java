package lcsb.mapviewer.common;

import java.util.List;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;

public class CommonTestFunctions {

  @Rule
  public UnitTestFailedWatcher ruleExample = new UnitTestFailedWatcher();

  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrors() {
    return appender.getErrors();
  }

}
