package lcsb.mapviewer.common.exception;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ InvalidArgumentExceptionTest.class, InvalidClassExceptionTest.class, InvalidStateExceptionTest.class,
    InvalidXmlSchemaExceptionTest.class,
    NotImplementedExceptionTest.class })
public class AllExceptionTests {

}
