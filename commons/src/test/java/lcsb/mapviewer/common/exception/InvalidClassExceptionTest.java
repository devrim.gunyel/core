package lcsb.mapviewer.common.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class InvalidClassExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    assertNotNull(new InvalidClassException("str"));
  }

  @Test
  public void testConstructor2() {
    assertNotNull(new InvalidClassException("dsr", new Exception()));
  }

}
