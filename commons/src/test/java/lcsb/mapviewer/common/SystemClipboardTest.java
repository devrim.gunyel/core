package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

public class SystemClipboardTest extends CommonTestFunctions {
  Logger logger = LogManager.getLogger(SystemClipboardTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testClipboard() throws Exception {
    SystemClipboard sc = new SystemClipboard();
    sc.setClipboardContents("TEST");

    SystemClipboard sc2 = new SystemClipboard();
    assertEquals("TEST", sc2.getClipboardContents());
  }

  @Test
  public void testInvalidClipboard() throws Exception {
    SystemClipboard sc2 = new SystemClipboard();

    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(new Transferable() {

      @Override
      public DataFlavor[] getTransferDataFlavors() {
        return null;
      }

      @Override
      public boolean isDataFlavorSupported(DataFlavor flavor) {
        return true;
      }

      @Override
      public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        throw new UnsupportedFlavorException(flavor);
      }
    }, sc2);

    assertEquals(null, sc2.getClipboardContents());
  }

  @Test
  public void testLostOwnership() throws Exception {
    SystemClipboard sc = new SystemClipboard();
    sc.lostOwnership(null, null);
  }

  @Test
  public void testEmpty() throws Exception {
    SystemClipboard sc2 = new SystemClipboard();
    sc2.setClipboardContents(null);
    assertNull(sc2.getClipboardContents());
  }

}
