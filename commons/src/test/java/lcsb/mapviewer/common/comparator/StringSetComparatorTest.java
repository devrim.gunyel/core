package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.*;

public class StringSetComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparisonEqualLength() {
    Set<String> list1 = new HashSet<>();
    Set<String> list2 = new HashSet<>();
    Set<String> list3 = new HashSet<>();
    list1.add("str1");
    list1.add("x");
    list2.add("str2");
    list2.add("x");
    list3.add("x");
    list3.add("str1");
    StringSetComparator comp = new StringSetComparator();
    assertTrue(comp.compare(list1, list3) == 0);
    assertFalse(comp.compare(list1, list2) == 0);
    assertFalse(comp.compare(list2, list3) == 0);
  }

  @Test
  public void testNotNullComparisonDiffLength() {
    Set<String> list1 = new HashSet<>();
    Set<String> list2 = new HashSet<>();
    list1.add("str1");
    list2.add("str2");
    list2.add("str1");
    StringSetComparator comp = new StringSetComparator();
    assertFalse(comp.compare(list1, list2) == 0);
    assertFalse(comp.compare(list2, list1) == 0);
  }

  @Test
  public void testNullComparison() {
    StringSetComparator comp = new StringSetComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new HashSet<>(), null) == 0);
    assertFalse(comp.compare(null, new HashSet<>()) == 0);
  }

}
