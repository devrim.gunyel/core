package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class StringListComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparisonEqualLength() {
    List<String> list1 = new ArrayList<>();
    List<String> list2 = new ArrayList<>();
    List<String> list3 = new ArrayList<>();
    list1.add("str1");
    list2.add("str2");
    list3.add("str1");
    StringListComparator comp = new StringListComparator();
    assertTrue(comp.compare(list1, list3) == 0);
    assertFalse(comp.compare(list1, list2) == 0);
    assertFalse(comp.compare(list2, list3) == 0);
  }

  @Test
  public void testNotNullComparisonDiffLength() {
    List<String> list1 = new ArrayList<>();
    List<String> list2 = new ArrayList<>();
    list1.add("str1");
    list2.add("str2");
    list2.add("str1");
    StringListComparator comp = new StringListComparator();
    assertFalse(comp.compare(list1, list2) == 0);
  }

  @Test
  public void testNullComparison() {
    StringListComparator comp = new StringListComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new ArrayList<>(), null) == 0);
    assertFalse(comp.compare(null, new ArrayList<>()) == 0);
  }

}
