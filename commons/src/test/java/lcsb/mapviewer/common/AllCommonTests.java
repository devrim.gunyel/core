package lcsb.mapviewer.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.common.comparator.AllComparatorTests;
import lcsb.mapviewer.common.exception.AllExceptionTests;
import lcsb.mapviewer.common.geometry.AllGeometryTests;

@RunWith(Suite.class)
@SuiteClasses({ AllComparatorTests.class,
    AllExceptionTests.class,
    AllGeometryTests.class,
    ConfigurationTest.class,
    GlobalLoggerAppenderTest.class,
    MimeTypeTest.class,
    ObjectUtilsTest.class,
    PairTest.class,
    SystemClipboardTest.class,
    TextFileUtilsTest.class,
    XmlParserTest.class,
})
public class AllCommonTests {

}
