package lcsb.mapviewer.common.geometry;

import java.awt.*;
import java.awt.geom.Line2D;

import org.junit.*;

public class CompositeStrokeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    CompositeStroke cs = new CompositeStroke(new BasicStroke(10f), new BasicStroke(0.5f));
    cs.createStrokedShape(new Line2D.Double(0.1, 0.1, 0.1, 0.1));
  }

}
