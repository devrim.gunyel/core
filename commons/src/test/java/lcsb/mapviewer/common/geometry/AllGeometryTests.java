package lcsb.mapviewer.common.geometry;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ColorParserTest.class,
    CompositeStrokeTest.class,
    EllipseTransformationTest.class,
    LineTransformationTest.class,
    PointTransformationTest.class,
    TextAlignmentTest.class,
})
public class AllGeometryTests {

}
