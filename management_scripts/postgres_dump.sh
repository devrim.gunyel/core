#! /bin/bash
echo "Dump of postgres DB..."
TIMESTAMP=$(date +"%F")
BACKUP_DIR="$BACKUP_DIR/$TIMESTAMP"

mkdir -p "$BACKUP_DIR/postgres"
pg_dump -U map_viewer map_viewer | gzip > "$BACKUP_DIR/postgres/map_viewer.gz"
cp "$BACKUP_DIR/postgres/map_viewer.gz" $RESTORE_DIR
echo "Done"
