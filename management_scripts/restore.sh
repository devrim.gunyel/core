#!/bin/bash

function print_help {
    echo "Usage: ";
    echo " $0 [params]";
    echo "";
    echo "If no params are set then script restores minerva framework from the data put manually in the \$RESTORE_DIR directory. To run script properly following environment variables must be set: ";
    echo "  \$CATALINA_HOME  Apache Tomcat installation directory";
    echo "  \$RESTORE_DIR    directory with restore point (files that should override current configuration)";
    echo "  \$BACKUP_DIR     backup directory - place where all backup files are located";
    echo "";
    echo "";
    echo "Available params: ";
    echo "  --help                 prints this help";
    echo "  --list-points          prints list of available restore time points";
    echo "  --restore-point=POINT  defines point from which system should be restored (if POINT=LAST then latest restore point is used)";

    echo "";
    echo "Current configuration:"
    echo "  CATALINA_HOME: $CATALINA_HOME"
    echo "  RESTORE_DIR:   $RESTORE_DIR"
    echo "  BACKUP_DIR:    $BACKUP_DIR"

}

function print_list {
    ls -c1 $BACKUP_DIR
}
function restore {
  point=`echo $1| cut -f2 -d"="`
  if [[ $point == LAST ]]
  then
    point=`ls -c1 $BACKUP_DIR|head -1`
  fi
  cp $BACKUP_DIR/$point/map_viewer/MapViewer.war $RESTORE_DIR
  cp $BACKUP_DIR/$point/postgres/map_viewer.gz $RESTORE_DIR
}


if [ -z "$CATALINA_HOME" ]
then 
  echo "\$CATALINA_HOME is not set"
  exit
fi

if [ -z "$RESTORE_DIR" ]
then 
  echo "\$RESTORE_DIR is not set"
  exit
fi

if [ -z "$BACKUP_DIR" ]
then 
  echo "\$BACKUP_DIR is not set"
  exit
fi

for var in "$@"
do
  if [ "$var" == "--list-points" ]
  then
    print_list
    exit
  fi
  if [ "$var" == "--help" ]
  then
    print_help
    exit
  fi
  if [[ $var == "--restore-point"* ]]
  then
    restore $var
  fi
done

echo "Shutdown tomcat... "

while [ `ps aux |grep apache-tomcat |wc -l` -ne "1" ]
do
        $CATALINA_HOME/bin/shutdown.sh
        echo "waiting for tomcat to shut down"
        sleep 1
        ps aux |grep apache-tomcat |wc -l
done
if [ -f $RESTORE_DIR/map_viewer.gz ]
then
    echo "Gzipped db found"
    cd $RESTORE_DIR
    gzip -d map_viewer.gz
    mv map_viewer map_viewer.sql
fi
if [ -f $RESTORE_DIR/map_images.tar.gz ]
then
    echo "Images archive found..."
    mv $RESTORE_DIR/map_images.tar.gz $CATALINA_HOME/webapps/
    cd $CATALINA_HOME/webapps/
    tar --checkpoint=1024 -zxf map_images.tar.gz
    rm map_images.tar.gz
fi

if [ -f $RESTORE_DIR/img.tar.gz ]
then
    echo "Images archive found..."
    mv $RESTORE_DIR/img.tar.gz $CATALINA_HOME/webapps/map_images/
    cd $CATALINA_HOME/webapps/map_images/
    tar --checkpoint=1024 -zxf img.tar.gz
    rm img.tar.gz
fi


if [ -f $RESTORE_DIR/map_viewer.sql ]
then
        echo "Drop db..."
        dropdb -U map_viewer -h localhost map_viewer
        echo "Create db..."
        createdb -U map_viewer -h localhost map_viewer
        psql -U map_viewer -h localhost -f $RESTORE_DIR/map_viewer.sql map_viewer
        rm $RESTORE_DIR/map_viewer.sql
fi
if [ -f $RESTORE_DIR/MapViewer.war ]
then
        echo "New MapViewer.war found"
        rm -rf $CATALINA_HOME/webapps/MapViewer
	rm -rf $CATALINA_HOME/work/Catalina
        mv $RESTORE_DIR/MapViewer.war $CATALINA_HOME/webapps/
fi
echo "Turn on tomcat..."
$CATALINA_HOME/bin/startup.sh
