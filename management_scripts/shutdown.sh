#!/bin/bash

if [ -z "$CATALINA_HOME" ]
then 
  echo "\$CATALINA_HOME is not set"
  exit
fi

if [ -z "$RESTORE_DIR" ]
then 
  echo "\$RESTORE_DIR is not set"
  exit
fi

if [ -z "$BACKUP_DIR" ]
then 
  echo "\$BACKUP_DIR is not set"
  exit
fi
echo "Shutdown tomcat... "

while [ `ps aux |grep apache-tomcat |wc -l` -ne "1" ]
do
        $CATALINA_HOME/bin/shutdown.sh
        echo "waiting for tomcat to shut down"
        sleep 1
        ps aux |grep apache-tomcat |wc -l
done
