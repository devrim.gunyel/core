package lcsb.mapviewer.api.convert;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.SBMLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.map.InconsistentModelException;

@RestController
@RequestMapping(value = "/convert", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConvertController extends BaseController {
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ConvertController.class);

  private ConvertRestImpl convertController;

  @Autowired
  public ConvertController(ConvertRestImpl convertController) {
    this.convertController = convertController;
  }

  @PostMapping(value = "/{fromFormat}:{toFormat}", produces = MediaType.APPLICATION_XML_VALUE)
  public String convertInput(@PathVariable(value = "fromFormat") String fromFormat,
      @PathVariable(value = "toFormat") String toFormat,
      @RequestBody String body)
      throws IOException, QueryException, SBMLException,
      InvalidInputDataExecption, InconsistentModelException, ConverterException {
    return convertController.convert(fromFormat, toFormat, body);
  }

  @PostMapping(value = "/image/{fromFormat}:{toFormat}")
  public @ResponseBody ResponseEntity<byte[]> convertInputToImage(
      @PathVariable(value = "fromFormat") String fromFormat,
      @PathVariable(value = "toFormat") String toFormat,
      @RequestBody String body)
      throws IOException, QueryException, SBMLException, InvalidInputDataExecption, ConverterException,
      DrawingException {
    ByteArrayOutputStream os = convertController.converToImage(fromFormat, toFormat, body);
    return ResponseEntity.ok().contentLength(os.size())
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=model" + toFormat)
        .body(os.toByteArray());
  }

  @RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST })
  public Map<String, Object> getInformation() {
    return convertController.getInformation();
  }

  @RequestMapping(value = "/image/", method = { RequestMethod.GET, RequestMethod.POST })
  public Map<String, Object> getInformationImage() {
    return convertController.getInformationImage();
  }

}