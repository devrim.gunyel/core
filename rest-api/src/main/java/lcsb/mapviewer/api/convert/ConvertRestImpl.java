package lcsb.mapviewer.api.convert;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.SBMLException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.*;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;

@Transactional
@Service
public class ConvertRestImpl extends BaseRestImpl {

  private Logger logger = LogManager.getLogger(ConvertRestImpl.class);

  public String convert(String fromFormat, String toFormat, String input)
      throws InvalidInputDataExecption, SBMLException, InconsistentModelException, IOException, ConverterException,
      QueryException {
    ConverterParams params = createConvertParams(input);
    Model model = getModelParserByNameOrClass(fromFormat).createModel(params);

    Converter exporter = getModelParserByNameOrClass(toFormat);
    return IOUtils.toString(exporter.model2InputStream(model), StandardCharsets.UTF_8);
  }

  public ByteArrayOutputStream converToImage(String fromFormat, String toFormat, String input,
      Double targetWidth, Double targetHeight)
      throws InvalidInputDataExecption, SBMLException, IOException, ConverterException, DrawingException,
      QueryException {
    Model model = getModelParserByNameOrClass(fromFormat).createModel(createConvertParams(input));

    AbstractImageGenerator generator = getImageGenerator(toFormat, createImageParams(model, targetWidth, targetHeight));
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    generator.saveToOutputStream(os);
    return os;
  }

  public ByteArrayOutputStream converToImage(String fromFormat, String toFormat, String input)
      throws IOException, DrawingException, QueryException {
    try {
      return converToImage(fromFormat, toFormat, input, 0.0, 0.0);
    } catch (InvalidInputDataExecption | ConverterException e) {
      throw new QueryException("Input file is invalid", e);
    }
  }

  public Map<String, Object> getInformation() {
    Map<String, Object> info = new LinkedHashMap<>();

    List<Object> converters = new ArrayList<>();
    for (Converter converter : getModelConverters()) {

      List<String> names = new ArrayList<>();
      names.add(removeWhiteSpaces(converter.getCommonName()));
      names.add(converter.getClass().getCanonicalName());

      Map<String, Object> names_item = new LinkedHashMap<>();
      names_item.put("available_names", names);
      converters.add(names_item);
    }

    info.put("inputs", converters);
    info.put("outputs", converters);
    return info;
  }

  public Map<String, Object> getInformationImage() {
    Map<String, Object> info = getInformation();

    List<Object> generators = new ArrayList<>();
    ImageGenerators igs = new ImageGenerators();
    for (Pair<String, Class<? extends AbstractImageGenerator>> generator : igs.getAvailableImageGenerators()) {

      List<String> names = new ArrayList<>();
      names.add(igs.getExtension(generator.getRight()));
      names.add(generator.getRight().getCanonicalName());

      Map<String, Object> names_item = new LinkedHashMap<>();
      names_item.put("available_names", names);
      generators.add(names_item);
    }

    info.remove("outputs");
    info.put("outputs", generators);
    return info;
  }

  private Converter getModelParserByNameOrClass(String id) throws QueryException {
    try {
      return getModelParserByName(id);
    } catch (QueryException e) {
      return getModelParser(id);
    }
  }

  private String removeWhiteSpaces(String str) {
    return str.replace(' ', '_');
  }

  private Converter getModelParserByName(String name) throws QueryException {
    for (Converter converter : getModelConverters()) {
      if (removeWhiteSpaces(converter.getCommonName()).equals(name)) {
        return converter;
      }
    }
    throw new QueryException("Unknown parser name: " + name);
  }

  private AbstractImageGenerator getImageGenerator(String extOrClass, AbstractImageGenerator.Params params)
      throws QueryException {
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : new ImageGenerators()
        .getAvailableImageGenerators()) {

      try {
        Class<? extends AbstractImageGenerator> clazz = element.getRight();
        Constructor<?> ctor = clazz.getConstructor(AbstractImageGenerator.Params.class);
        AbstractImageGenerator generator = (AbstractImageGenerator) ctor.newInstance(params);
        if (extOrClass.equals(clazz.getCanonicalName()) || extOrClass.equals(generator.getFileExtension())) {
          return generator;
        }

      } catch (NoSuchMethodException | java.lang.SecurityException | InstantiationException
          | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
        logger.error("Creation of image generator class for '" + element.getLeft() + "' failed.");
        throw new QueryException("Issue with obtaining image generator for extension " + extOrClass + ".");
      }

    }

    throw new QueryException("Image generator for extension " + extOrClass + " not available.");
  }

  private AbstractImageGenerator.Params createImageParams(Model model, Double targetWidth, Double targetHeight) {
    Double padding = 5.0;
    Double maxDim = 10000.0;

    Double w = model.getWidth();
    Double h = model.getHeight();

    if (targetHeight == 0)
      targetHeight = Math.min(h, maxDim);
    if (targetWidth == 0)
      targetWidth = Math.min(w, maxDim);

    Double scale = targetWidth / w;
    if (h * scale > targetHeight) {
      scale = targetHeight / h;
    }

    Double wScaled = w * scale;
    Double hScaled = h * scale;

    return new AbstractImageGenerator.Params().model(model).width(wScaled + padding).height(hScaled + padding)
        .scale(1 / scale)
        .x(0);

  }

  private ConverterParams createConvertParams(String input) {
    ConverterParams params = new ConverterParams();
    InputStream is = new ByteArrayInputStream(input.getBytes());

    params.inputStream(is);

    return params;
  }
}
