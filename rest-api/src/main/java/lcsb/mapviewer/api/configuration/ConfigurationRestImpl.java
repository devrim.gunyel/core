package lcsb.mapviewer.api.configuration;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.layout.ColorSchemaType;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@Transactional
@Service
public class ConfigurationRestImpl extends BaseRestImpl {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ConfigurationRestImpl.class);

  private IConfigurationService configurationService;

  private ModelAnnotator modelAnnotator;

  @Autowired
  public ConfigurationRestImpl(IConfigurationService configurationService, ModelAnnotator modelAnnotator) {
    this.configurationService = configurationService;
    this.modelAnnotator = modelAnnotator;
  }

  public List<Map<String, Object>> getAllValues() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ConfigurationOption option : configurationService.getAllValues()) {
      result.add(optionToMap(option));
    }
    return result;
  }

  private Map<String, Object> optionToMap(ConfigurationOption option) {
    Map<String, Object> result = new TreeMap<>();
    result.put("idObject", option.getId());
    result.put("type", option.getType());
    result.put("valueType", option.getType().getEditType());
    result.put("commonName", option.getType().getCommonName());
    result.put("isServerSide", option.getType().isServerSide());
    // don't send password over API
    if (!option.getType().getEditType().equals(ConfigurationElementEditType.PASSWORD)) {
      result.put("value", option.getValue());
    }
    if (option.getType().getGroup() != null) {
      result.put("group", option.getType().getGroup().getCommonName());
    }
    return result;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  public List<Map<String, Object>> getImageFormats() {
    List<Map<String, Object>> result = new ArrayList<>();
    ImageGenerators imageGenerators = new ImageGenerators();
    List<Pair<String, Class<? extends AbstractImageGenerator>>> imageGeneratorList = imageGenerators
        .getAvailableImageGenerators();

    for (Pair<String, Class<? extends AbstractImageGenerator>> element : imageGeneratorList) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", element.getLeft());
      row.put("handler", element.getRight().getCanonicalName());
      row.put("extension", imageGenerators.getExtension(element.getRight()));
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getModelFormats() {
    List<Converter> converters = getModelConverters();

    List<Map<String, Object>> result = new ArrayList<>();

    for (Converter converter : converters) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", converter.getCommonName());
      row.put("handler", converter.getClass().getCanonicalName());
      row.put("extension", converter.getFileExtension());
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getOverlayTypes() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ColorSchemaType type : ColorSchemaType.values()) {
      Map<String, Object> map = new TreeMap<>();
      map.put("name", type.name());
      result.add(map);
    }
    return result;
  }

  public Set<Map<String, String>> getElementTypes() {
    return getClassStringTypesList(Element.class);
  }

  private Set<Map<String, String>> getClassStringTypesList(Class<?> elementClass) {
    Set<Map<String, String>> result = new LinkedHashSet<>();
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<>();
    queue.add(top);
    while (!queue.isEmpty()) {
      ClassTreeNode clazz = queue.poll();
      queue.addAll(clazz.getChildren());
      if (elementClass.isAssignableFrom(clazz.getClazz())) {
        Map<String, String> row = new TreeMap<>();
        row.put("className", clazz.getClazz().getName());
        row.put("name", clazz.getCommonName());
        if (clazz.getParent() == null) {
          row.put("parentClass", null);
        } else {
          row.put("parentClass", clazz.getParent().getClazz().getName());
        }
        result.add(row);
      }
    }
    return result;
  }

  public Set<Map<String, String>> getReactionTypes() {
    return getClassStringTypesList(Reaction.class);
  }

  public Map<String, Object> getMiriamTypes() {
    Map<String, Object> result = new TreeMap<>();
    for (MiriamType type : MiriamType.values()) {
      result.put(type.name(), createMiriamTypeResponse(type));
    }
    return result;
  }

  private Map<String, Object> createMiriamTypeResponse(MiriamType type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getCommonName());
    result.put("homepage", type.getDbHomepage());
    result.put("registryIdentifier", type.getRegistryIdentifier());
    result.put("uris", type.getUris());

    return result;
  }

  public Object getModificationStateTypes() {
    Map<String, Object> result = new TreeMap<>();
    for (ModificationState type : ModificationState.values()) {
      result.put(type.name(), createModificationStateResponse(type));
    }
    return result;
  }

  private Map<String, Object> createModificationStateResponse(ModificationState type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getFullName());
    result.put("abbreviation", type.getAbbreviation());
    return result;
  }

  public Map<String, Object> getPrivilegeTypes() {
    Map<String, Object> result = new TreeMap<>();
    for (PrivilegeType type : PrivilegeType.values()) {
      result.put(type.name(), createPrivilegeTypeResponse(type));
    }
    return result;
  }

  private Map<String, Object> createPrivilegeTypeResponse(PrivilegeType type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getDescription());
    if (type.getPrivilegeObjectType() != null) {
      result.put("objectType", type.getPrivilegeObjectType().getSimpleName());
    } else {
      result.put("objectType", null);
    }
    result.put("valueType", "boolean");
    return result;
  }

  public List<Map<String, Object>> getAnnotators() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ElementAnnotator annotator : modelAnnotator.getAvailableAnnotators()) {
      result.add(prepareAnnotator(annotator));
    }
    return result;
  }

  private Map<String, Object> prepareAnnotator(ElementAnnotator annotator) {
    Map<String, Object> result = new TreeMap<>();
    result.put("className", annotator.getClass().getName());
    result.put("name", annotator.getCommonName());
    result.put("description", annotator.getDescription());
    result.put("url", annotator.getUrl());
    result.put("elementClassNames", annotator.getValidClasses());
    result.put("parameters", prepareAnnotatorsParams(annotator.createAnnotatorData().getAnnotatorParams()));
    return result;
  }

  public List<Map<String, Object>> getMapTypes() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (SubmodelType type : SubmodelType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getMapCanvasTypes() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (MapCanvasType type : MapCanvasType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

  public Map<String, Object> updateOption(String option, Map<String, Object> data) throws QueryException {
    ConfigurationElementType type = ConfigurationElementType.valueOf(option);
    String value = (String) data.get("value");
    try {
      configurationService.setConfigurationValue(type, value);
    } catch (InvalidArgumentException e) {
      throw new QueryException(e.getMessage(), e);
    }
    return optionToMap(configurationService.getValue(type));
  }

  public List<Map<String, Object>> getUnitTypes() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (SbmlUnitType type : SbmlUnitType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getBioEntityFields() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (BioEntityField field : BioEntityField.values()) {
      Map<String, Object> entry = new TreeMap<>();
      entry.put("name", field.name());
      entry.put("commonName", field.getCommonName());
      result.add(entry);
    }
    return result;
  }

}
