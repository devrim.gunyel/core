package lcsb.mapviewer.api.configuration;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RestController
@RequestMapping(value = "/configuration", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConfigurationController extends BaseController {

  Logger logger = LogManager.getLogger();

  private ConfigurationRestImpl configurationController;
  private IConfigurationService configurationService;
  private ServletContext context;

  @Autowired
  public ConfigurationController(ConfigurationRestImpl configurationController,
      IConfigurationService configurationService,
      ServletContext context) {
    this.configurationController = configurationController;
    this.configurationService = configurationService;
    this.context = context;
  }

  @GetMapping(value = "/")
  public Map<String, Object> getConfiguration(Authentication authentication) {
    Map<String, Object> result = new TreeMap<>();
    result.put("options", getOptions(authentication));
    result.put("imageFormats", configurationController.getImageFormats());
    result.put("modelFormats", configurationController.getModelFormats());
    result.put("overlayTypes", configurationController.getOverlayTypes());
    result.put("elementTypes", configurationController.getElementTypes());
    result.put("reactionTypes", configurationController.getReactionTypes());
    result.put("miriamTypes", configurationController.getMiriamTypes());
    result.put("mapTypes", configurationController.getMapTypes());
    result.put("mapCanvasTypes", configurationController.getMapCanvasTypes());
    result.put("unitTypes", configurationController.getUnitTypes());
    result.put("modificationStateTypes", configurationController.getModificationStateTypes());
    result.put("privilegeTypes", configurationController.getPrivilegeTypes());
    result.put("version", configurationService.getSystemSvnVersion(context.getRealPath("/")));
    result.put("buildDate", configurationService.getSystemBuild(context.getRealPath("/")));
    result.put("gitHash", configurationService.getSystemGitVersion(context.getRealPath("/")));
    result.put("annotators", configurationController.getAnnotators());
    result.put("bioEntityFields", configurationController.getBioEntityFields());
    return result;
  }

  @GetMapping(value = "/options/")
  public List<Map<String, Object>> getOptions(Authentication authentication) {
    boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));
    return configurationController.getAllValues().stream()
        .filter(option -> !((Boolean) option.get("isServerSide")) || isAdmin)
        .collect(Collectors.toList());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/options/{option}")
  public Map<String, Object> updateOption(@RequestBody String body, @PathVariable(value = "option") String option)
      throws IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "option");
    return configurationController.updateOption(option, data);
  }
}