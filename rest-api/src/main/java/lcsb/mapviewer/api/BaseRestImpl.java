package lcsb.mapviewer.api;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.*;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.annotator.*;
import lcsb.mapviewer.services.interfaces.*;
import lcsb.mapviewer.services.search.ElementMatcher;

@Transactional
public abstract class BaseRestImpl {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(BaseRestImpl.class);

  @Autowired
  private IModelService modelService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private MiriamConnector miriamConnector;

  @Autowired
  private PubmedParser pubmedParser;

  @Autowired
  private List<Converter> modelConverters;

  private ElementMatcher elementMatcher = new ElementMatcher();

  private Transformer mathMlTransformer;

  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  protected Map<String, Object> okStatus() {
    return new TreeMap<>();
  }

  protected Map<String, Object> createMinifiedSearchResult(BioEntity object) {
    Map<String, Object> result = new TreeMap<>();
    if (object instanceof Element) {
      result.put("type", ElementIdentifierType.ALIAS);
      Element element = (Element) object;
      result.put("id", element.getId());
      result.put("modelId", element.getModel().getId());
    } else if (object instanceof Reaction) {
      result.put("type", ElementIdentifierType.REACTION);
      Reaction element = (Reaction) object;
      result.put("id", element.getId());
      result.put("modelId", element.getModel().getId());

    } else {
      throw new InvalidStateException("Unknown type of result: " + object.getClass());
    }
    return result;
  };

  protected Map<String, Object> createAnnotation(MiriamData annotation) {
    if (annotation != null && annotation.getDataType() != null) {
      Map<String, Object> result = new TreeMap<>();
      if (annotation.getDataType().getUris().size() > 0) {
        try {
          result.put("link", miriamConnector.getUrlString(annotation));
        } catch (Exception e) {
          logger.error("Problem with miriam: " + annotation, e);
        }
      }
      if (MiriamType.PUBMED.equals(annotation.getDataType())) {
        if (NumberUtils.isDigits(annotation.getResource())) {
          try {
            Article article = pubmedParser.getPubmedArticleById(annotation.getResource());
            result.put("article", article);
          } catch (PubmedSearchException e) {
            logger.error("Problem with accessing info about pubmed", e);
          }
        } else {
          logger.error("Invalid pubmed identifier: " + annotation.getResource());
        }
      }
      result.put("type", annotation.getDataType().name());
      result.put("resource", annotation.getResource());
      result.put("id", annotation.getId());

      if (annotation.getAnnotator() != null) {
        result.put("annotatorClassName", annotation.getAnnotator().getName());
      } else {
        result.put("annotatorClassName", "");
      }

      return result;
    } else {
      throw new InvalidArgumentException("invalid miriam data: " + annotation);
    }
  };

  protected Map<String, Object> createAnnotation(Article article) {
    Map<String, Object> result = new TreeMap<>();
    if (article != null) {
      MiriamType type = MiriamType.PUBMED;
      result.put("link", miriamConnector.getUrlString(new MiriamData(MiriamType.PUBMED, article.getId())));
      result.put("article", article);
      result.put("type", type);
      result.put("resource", article.getId());
    }
    return result;
  }

  protected List<Map<String, Object>> createAnnotations(Collection<?> references) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (Object miriamData : references) {
      if (miriamData instanceof MiriamData) {
        result.add(createAnnotation((MiriamData) miriamData));
      } else if (miriamData instanceof Article) {
        result.add(createAnnotation((Article) miriamData));
      } else {
        throw new InvalidArgumentException();
      }
    }
    return result;
  }

  /**
   * Returns list of models for given project limited by modelId.
   * 
   * @param projectId
   *          project identifier
   * @param modelId
   *          list of model identifiers separated by "," or '*' when all models
   *          should be returned
   * @throws QueryException
   */
  protected List<Model> getModels(String projectId, String modelId) throws QueryException {
    Model model = modelService.getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    List<Model> models = new ArrayList<>();

    if (!modelId.equals("*")) {
      for (String str : modelId.split(",")) {
        if (!StringUtils.isNumeric(str)) {
          throw new QueryException("Invalid modelId: " + modelId);
        }

        Model submodel = model.getSubmodelById(Integer.valueOf(str));
        if (submodel == null) {
          throw new ObjectNotFoundException("Model with given id doesn't exist");
        }
        models.add(submodel);
      }
    } else {
      models.addAll(model.getSubmodels());
      models.add(model);
    }
    models.sort(Model.ID_COMPARATOR);
    return models;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService
   *          the modelService to set
   * @see #modelService
   */
  public void setModelService(IModelService modelService) {
    this.modelService = modelService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  protected List<Map<String, Object>> prepareTargets(Collection<Target> targets, List<Model> models) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (Target target : targets) {
      result.add(prepareTarget(target, models));
    }
    result.sort(new Comparator<Map<String, Object>>() {

      @Override
      public int compare(Map<String, Object> o1, Map<String, Object> o2) {
        List<?> targetedObjects1 = (List<?>) o1.get("targetElements");
        List<?> targetedObjects2 = (List<?>) o2.get("targetElements");
        Integer size1 = 0;
        Integer size2 = 0;
        if (targetedObjects1 != null) {
          size1 = targetedObjects1.size();
        }
        if (targetedObjects2 != null) {
          size2 = targetedObjects2.size();
        }
        if (size1 == size2) {
          String name1 = (String) o1.get("name");
          String name2 = (String) o2.get("name");
          return new StringComparator().compare(name1, name2);
        }
        return -size1.compareTo(size2);
      }
    });
    return result;
  }

  protected Map<String, Object> prepareTarget(Target target, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    result.put("name", target.getName());
    result.put("references", createAnnotations(target.getReferences()));
    result.put("targetParticipants", createAnnotations(target.getGenes()));

    List<Map<String, Object>> targetedObjects = new ArrayList<>();
    List<BioEntity> bioEntities = new ArrayList<>();
    for (Model model : models) {
      bioEntities.addAll(model.getBioEntities());
    }
    bioEntities.sort(BioEntity.ID_COMPARATOR);
    for (BioEntity bioEntity : bioEntities) {
      if (elementMatcher.elementMatch(target, bioEntity)) {
        Map<String, Object> elementMapping = new TreeMap<>();
        elementMapping.put("id", bioEntity.getId());
        elementMapping.put("type", getType(bioEntity));
        elementMapping.put("modelId", bioEntity.getModel().getId());
        targetedObjects.add(elementMapping);
      }
    }

    result.put("targetElements", targetedObjects);

    return result;
  }

  private String getType(BioEntity object) {
    if (object instanceof Reaction) {
      return ElementIdentifierType.REACTION.getJsName();
    } else if (object instanceof Element) {
      return ElementIdentifierType.ALIAS.getJsName();
    } else {
      throw new InvalidArgumentException("Unknown type of element " + object.getClass());
    }
  }

  /**
   * @return the projectService
   * @see #projectService
   */
  public IProjectService getProjectService() {
    return projectService;
  }

  /**
   * @param projectService
   *          the projectService to set
   * @see #projectService
   */
  public void setProjectService(IProjectService projectService) {
    this.projectService = projectService;
  }

  protected Converter getModelParser(String handlerClass) throws QueryException {
    for (Converter converter : getModelConverters()) {
      if (converter.getClass().getCanonicalName().equals(handlerClass)) {
        try {
          return (Converter) Class.forName(handlerClass).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
          throw new QueryException("Problem with handler:" + handlerClass);
        }
      }
    }
    throw new QueryException("Unknown handlerClass: " + handlerClass);
  }

  protected List<Converter> getModelConverters() {
    return modelConverters;
  }

  protected String getFirstValue(List<Object> list) {
    if (list == null) {
      return null;
    }
    if (list.size() > 0) {
      return (String) list.get(0);
    }
    return null;
  }

  protected String mathMLToPresentationML(String xmlContent)
      throws IOException, InvalidXmlSchemaException, TransformerException {
    Transformer transformer = getMathMLTransformer();

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    transformer.transform(new StreamSource(new ByteArrayInputStream(xmlContent.getBytes())), new StreamResult(baos));
    String result = baos.toString("UTF-8");
    if (result.startsWith("<?xml")) {
      result = result.substring(result.indexOf(">") + 1);
    }
    return result;
  }

  private Transformer getMathMLTransformer()
      throws TransformerFactoryConfigurationError, IOException, TransformerConfigurationException {
    if (this.mathMlTransformer == null) {
      TransformerFactory factory = TransformerFactory.newInstance();

      Resource resource = new ClassPathResource("mathmlc2p.xsl");
      InputStream styleInputStream = resource.getInputStream();

      StreamSource stylesource = new StreamSource(styleInputStream);
      this.mathMlTransformer = factory.newTransformer(stylesource);
    }
    return this.mathMlTransformer;
  }

  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  public MiriamConnector getMiriamConnector() {
    return miriamConnector;
  }

  public void setMiriamConnector(MiriamConnector miriamConnector) {
    this.miriamConnector = miriamConnector;
  }

  public PubmedParser getPubmedParser() {
    return pubmedParser;
  }

  public void setPubmedParser(PubmedParser pubmedParser) {
    this.pubmedParser = pubmedParser;
  }

  /**
   * Prepares annotator parameters in the form of a map having annotators class
   * names as keys and map of name:value pairs of given annotator as values.
   *
   * @param annotatorsParams
   * @return
   */
  protected List<Map<String, Object>> prepareAnnotatorsParams(List<AnnotatorParameter> annotatorsParams) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (AnnotatorParameter param : annotatorsParams) {
      Map<String, Object> annotatorParams;
      if (param instanceof AnnotatorConfigParameter) {
        annotatorParams = prepareConfigParameter((AnnotatorConfigParameter) param);
      } else if (param instanceof AnnotatorInputParameter) {
        annotatorParams = prepareInputParameter((AnnotatorInputParameter) param);
      } else if (param instanceof AnnotatorOutputParameter) {
        annotatorParams = prepareOutputParameter((AnnotatorOutputParameter) param);
      } else {
        throw new NotImplementedException();
      }
      result.add(annotatorParams);
    }
    return result;
  }

  public Map<String, Object> prepareConfigParameter(AnnotatorConfigParameter configParameter) {
    Map<String, Object> annotatorParams = new TreeMap<>();
    annotatorParams.put("name", configParameter.getType().name());
    annotatorParams.put("commonName", configParameter.getType().getName());
    annotatorParams.put("inputType", configParameter.getType().getType());
    annotatorParams.put("description", configParameter.getType().getDescription());
    annotatorParams.put("value", configParameter.getValue());
    annotatorParams.put("order", configParameter.getOrderPosition());
    annotatorParams.put("type", "CONFIG");
    return annotatorParams;
  }

  public Map<String, Object> prepareInputParameter(AnnotatorInputParameter configParameter) {
    Map<String, Object> annotatorParams = new TreeMap<>();
    annotatorParams.put("field", configParameter.getField());
    annotatorParams.put("annotation_type", configParameter.getIdentifierType());
    annotatorParams.put("order", configParameter.getOrderPosition());
    annotatorParams.put("type", "INPUT");
    return annotatorParams;
  }

  public Map<String, Object> prepareOutputParameter(AnnotatorOutputParameter configParameter) {
    Map<String, Object> annotatorParams = new TreeMap<>();
    annotatorParams.put("field", configParameter.getField());
    annotatorParams.put("annotation_type", configParameter.getIdentifierType());
    annotatorParams.put("order", configParameter.getOrderPosition());
    annotatorParams.put("type", "OUTPUT");
    return annotatorParams;
  }

  public String prepareDate(Calendar creationDate) {
    if (creationDate == null) {
      return null;
    }
    return dateFormat.format(creationDate.getTime());
  }

  public List<String> prepareDates(List<Calendar> modificationDates) {
    List<String> result = new ArrayList<>();
    for (Calendar calendar : modificationDates) {
      result.add(prepareDate(calendar));
    }
    return result;
  }

  protected Integer parseInteger(Object value, String fieldName) throws QueryException {
    if (value instanceof Integer) {
      return (Integer) value;
    } else if (value instanceof Double) {
      logger.warn("Double will be trunceted to int: " + value);
      return ((Double) value).intValue();
    } else if (value == null) {
      return null;
    } else if (value instanceof String) {
      if (((String) value).equalsIgnoreCase("")) {
        return null;
      } else {
        try {
          return Integer.parseInt((String) value);
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid " + fieldName + " value: " + value);
        }
      }
    } else {
      throw new QueryException("Invalid " + fieldName + " value: " + value);
    }
  }

  protected Integer parseInteger(Object value) throws QueryException {
    return parseInteger(value, "integer");
  }

}
