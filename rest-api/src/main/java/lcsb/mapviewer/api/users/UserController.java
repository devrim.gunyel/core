package lcsb.mapviewer.api.users;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController extends BaseController {

  Logger logger = LogManager.getLogger();

  private IUserService userService;
  private UserRestImpl userRest;

  @Autowired
  public UserController(IUserService userService, UserRestImpl userRest) {
    this.userService = userService;
    this.userRest = userRest;
  }

  /**
   * This API action is protected by spring security and will fail if the user is
   * not authenticated. Therefore simply calling this action can be used to detect
   * whether the current session is still valid.
   */
  @GetMapping(value = "/isSessionValid")
  @PreAuthorize("isAuthenticated() and authentication.name != '" + Configuration.ANONYMOUS_LOGIN + "'")
  public Object isSessionValid(Authentication authentication) {
    Map<String, Object> result = new TreeMap<>();
    result.put("login", authentication.getName());
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR') or #login == authentication.name")
  @GetMapping(value = "/{login:.+}")
  public Map<String, Object> getUser(
      @PathVariable(value = "login") String login,
      @RequestParam(value = "columns", defaultValue = "") String columns) throws ObjectNotFoundException {
    return userRest.getUser(login, columns);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/{login}:updatePrivileges")
  public Map<String, Object> updatePrivileges(
      @RequestBody String body,
      @PathVariable(value = "login") String login) throws IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "privileges");
    return userRest.updatePrivileges(login, data);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') or #login == authentication.name")
  @PatchMapping(value = "/{login}:updatePreferences")
  public Map<String, Object> updatePreferences(
      @RequestBody String body,
      @PathVariable(value = "login") String login) throws IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "preferences");
    return userRest.updatePreferences(login, data);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getUsers(@RequestParam(value = "columns", defaultValue = "") String columns) {
    return userRest.getUsers(columns).stream()
        .sorted(Comparator.comparing(user -> (String) user.get("login"), Comparator.reverseOrder()))
        .collect(Collectors.toList());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') or #login == authentication.name")
  @PatchMapping(value = "/{login:.+}")
  public Map<String, Object> updateUser(
      @RequestBody String body,
      @PathVariable(value = "login") String login,
      Authentication authentication) throws QueryException, IOException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "user");
    return userRest.updateUser(login, data, authentication.getAuthorities());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/{login:.+}")
  public Map<String, Object> addUser(
      @RequestBody MultiValueMap<String, Object> formData,
      @PathVariable(value = "login") String login) throws QueryException {
    Map<String, Object> userMap = userRest.addUser(login, formData);
    return userMap;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{login:.+}")
  public Map<String, Object> removeUser(@PathVariable(value = "login") String login) throws QueryException {
    return userRest.removeUser(login);
  }

  public IUserService getUserService() {
    return userService;
  }

  public void setUserService(IUserService userService) {
    this.userService = userService;
  }
}