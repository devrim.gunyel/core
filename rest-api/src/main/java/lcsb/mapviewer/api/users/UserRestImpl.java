package lcsb.mapviewer.api.users;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.model.user.annotator.*;

@Transactional
@Service
public class UserRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(UserRestImpl.class);
  private PasswordEncoder passwordEncoder;

  @Autowired
  public UserRestImpl(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  public Map<String, Object> getUser(String login, String columns)
      throws ObjectNotFoundException {
    Set<String> columnSet = createUserColumnSet(columns);
    User user = getUserService().getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("User doesn't exist");
    }
    Boolean ldapAvailable = false;
    if (columnSet.contains("ldapAccountAvailable")) {
      List<User> userList = new ArrayList<>();
      userList.add(user);
      ldapAvailable = getUserService().ldapAccountExistsForLogin(userList).get(login);
    }
    return prepareUse(user, columnSet, ldapAvailable);
  }

  private Set<String> createUserColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("id");
      columnsSet.add("login");
      columnsSet.add("name");
      columnsSet.add("surname");
      columnsSet.add("email");
      columnsSet.add("minColor");
      columnsSet.add("maxColor");
      columnsSet.add("neutralColor");
      columnsSet.add("simpleColor");
      columnsSet.add("removed");
      columnsSet.add("privileges");
      columnsSet.add("termsOfUseConsent");
      columnsSet.add("connectedToLdap");
      columnsSet.add("ldapAccountAvailable");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

  private Map<String, Object> prepareUse(User user, Set<String> columnsSet, Boolean ldapAvailable) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value;
      switch (column) {
      case "id":
      case "idobject":
        value = user.getId();
        break;
      case "name":
        value = user.getName();
        break;
      case "surname":
        value = user.getSurname();
        break;
      case "login":
        value = user.getLogin();
        break;
      case "email":
        value = user.getEmail();
        break;
      case "mincolor":
        value = user.getMinColor();
        break;
      case "maxcolor":
        value = user.getMaxColor();
        break;
      case "neutralcolor":
        value = user.getNeutralColor();
        break;
      case "simplecolor":
        value = user.getSimpleColor();
        break;
      case "removed":
        value = user.isRemoved();
        break;
      case "termsofuseconsent":
        value = user.isTermsOfUseConsent();
        break;
      case "connectedtoldap":
        value = user.isConnectedToLdap();
        break;
      case "ldapaccountavailable":
        if (ldapAvailable == null) {
          ldapAvailable = false;
        }
        value = ldapAvailable;
        break;
      case "privileges":
        value = preparePrivileges(user);
        break;
      case "preferences":
        value = preparePreferences(user);
        break;
      default:
        value = "Unknown column";
        break;
      }
      result.put(string, value);
    }
    return result;
  }

  Map<String, Object> preparePreferences(User user) {
    Map<String, Object> result = new TreeMap<>();
    UserAnnotationSchema schema = getProjectService().prepareUserAnnotationSchema(user);
    result.put("project-upload", prepareProjectUploadPreferences(schema));
    result.put("element-annotators", prepareElementAnnotators(schema.getClassAnnotators()));
    result.put("element-required-annotations", prepareRequiredAnnotations(schema.getClassRequiredAnnotators()));
    result.put("element-valid-annotations", prepareValidAnnotations(schema.getClassValidAnnotators()));
    result.put("gui-preferences", prepareGuiPreferences(schema.getGuiPreferences()));
    return result;
  }

  Map<String, Object> prepareGuiPreferences(Set<UserGuiPreference> guiPreferences) {
    Map<String, Object> result = new TreeMap<>();
    for (UserGuiPreference userGuiPreference : guiPreferences) {
      result.put(userGuiPreference.getKey(), userGuiPreference.getValue());
    }
    return result;
  }

  private Map<String, Object> prepareValidAnnotations(List<UserClassValidAnnotations> classValidAnnotators) {
    Map<String, Object> result = new TreeMap<>();
    for (UserClassValidAnnotations userClassAnnotators : classValidAnnotators) {
      result.put(userClassAnnotators.getClassName(), new ArrayList<>(userClassAnnotators.getValidMiriamTypes()));
    }
    return result;
  }

  private void updateValidAnnotations(UserAnnotationSchema schema, Map<String, Object> data) {
    for (String key : data.keySet()) {
      UserClassValidAnnotations annotator = null;
      for (UserClassValidAnnotations userClassAnnotators : schema.getClassValidAnnotators()) {
        if (userClassAnnotators.getClassName().equals(key)) {
          annotator = userClassAnnotators;
        }
      }
      if (annotator == null) {
        annotator = new UserClassValidAnnotations();
        annotator.setClassName(key);
        schema.addClassValidAnnotations(annotator);
      }
      annotator.getValidMiriamTypes().clear();
      for (Object row : (List<?>) data.get(key)) {
        annotator.getValidMiriamTypes().add(MiriamType.valueOf((String) row));
      }
    }
  }

  private void updateGuiPreferences(UserAnnotationSchema schema, Map<String, Object> data) {
    for (String key : data.keySet()) {
      Object object = data.get(key);
      String value = "";
      if (object instanceof Integer) {
        value = object + "";
      } else if (object instanceof String) {
        value = (String) data.get(key);
      } else {
        throw new InvalidArgumentException("Invalid value class: " + value);
      }
      schema.setGuiPreference(key, value);
    }
  }

  private Map<String, Object> prepareRequiredAnnotations(List<UserClassRequiredAnnotations> classRequiredAnnotators) {
    Map<String, Object> result = new TreeMap<>();
    for (UserClassRequiredAnnotations requiredAnnotations : classRequiredAnnotators) {
      Map<String, Object> row = new TreeMap<>();
      row.put("require-at-least-one", requiredAnnotations.getRequireAtLeastOneAnnotation());
      List<String> miriamTypes = new ArrayList<>();

      for (MiriamType mt : requiredAnnotations.getRequiredMiriamTypes()) {
        miriamTypes.add(mt.name());
      }
      row.put("annotation-list", miriamTypes);
      result.put(requiredAnnotations.getClassName(), row);
    }
    return result;
  }

  private void updateRequiredAnnotations(UserAnnotationSchema schema, Map<String, Object> data) throws QueryException {
    for (String key : data.keySet()) {
      UserClassRequiredAnnotations annotator = null;
      for (UserClassRequiredAnnotations userClassAnnotators : schema.getClassRequiredAnnotators()) {
        if (userClassAnnotators.getClassName().equals(key)) {
          annotator = userClassAnnotators;
        }
      }
      if (annotator == null) {
        annotator = new UserClassRequiredAnnotations();
        annotator.setClassName(key);
        schema.addClassRequiredAnnotations(annotator);
      }
      updateAnnotator(annotator, (Map<String, Object>) data.get(key));
    }
  }

  private void updateAnnotator(UserClassRequiredAnnotations annotator, Map<String, Object> data) throws QueryException {
    for (String key : data.keySet()) {
      if (key.equals("require-at-least-one")) {
        annotator.setRequireAtLeastOneAnnotation((Boolean) data.get("require-at-least-one"));
      } else if (key.equals("annotation-list")) {
        annotator.getRequiredMiriamTypes().clear();
        for (Object row : (List<?>) data.get(key)) {
          annotator.getRequiredMiriamTypes().add(MiriamType.valueOf((String) row));
        }
      } else {
        throw new QueryException("Unknown field: " + key);
      }
    }

  }

  private Map<String, Object> prepareElementAnnotators(List<UserClassAnnotators> classAnnotators) {
    Map<String, Object> result = new TreeMap<>();
    for (UserClassAnnotators userClassAnnotators : classAnnotators) {
      result.put(userClassAnnotators.getClassName(), prepareAnnotators(userClassAnnotators.getAnnotators()));
    }
    return result;
  }

  private List<Map<String, Object>> prepareAnnotators(List<AnnotatorData> annotators) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (AnnotatorData annotatorData : annotators) {
      result.add(prepareAnnotator(annotatorData));
    }
    return result;
  }

  private Map<String, Object> prepareAnnotator(AnnotatorData annotatorData) {
    Map<String, Object> result = new HashMap<>();
    result.put("annotatorClass", annotatorData.getAnnotatorClassName().getName());
    result.put("id", annotatorData.getId());
    result.put("order", annotatorData.getOrderIndex());
    result.put("parameters", prepareAnnotatorsParams(annotatorData.getAnnotatorParams()));
    return result;
  }

  private AnnotatorData parseAnnotator(Map<String, Object> map) throws QueryException {
    try {
      if (map.get("annotatorClass") == null) {
        throw new QueryException("annotatorClass for annotator must be defined");
      }
      AnnotatorData result = new AnnotatorData((String) map.get("annotatorClass"));
      for (String key : map.keySet()) {
        Object value = map.get(key);
        if (key.equalsIgnoreCase("annotatorClass") || key.equalsIgnoreCase("id")) {
          continue;
        } else if (key.equalsIgnoreCase("order")) {
          if (value instanceof Integer) {
            result.setOrderIndex((Integer) value);
          } else {
            result.setOrderIndex(Integer.valueOf((String) map.get("order")));
          }
        } else if (key.equalsIgnoreCase("parameters")) {
          result.addAnnotatorParameters(parseAnnotatorParameters((List<Map<String, Object>>) value));
        } else {
          throw new InvalidArgumentException("Unknown annotator property: " + key);
        }
      }
      return result;
    } catch (ClassNotFoundException e) {
      throw new QueryException(e);
    }
  }

  private List<AnnotatorParameter> parseAnnotatorParameters(List<Map<String, Object>> data) {
    List<AnnotatorParameter> result = new ArrayList<>();
    for (Map<String, Object> entry : data) {
      if (entry.get("type").equals("INPUT")) {
        result.add(parseInputParameter(entry));
      } else if (entry.get("type").equals("OUTPUT")) {
        result.add(parseOutputParameter(entry));
      } else if (entry.get("type").equals("CONFIG")) {
        result.add(parseConfigParameter(entry));
      } else {
        throw new InvalidArgumentException("Unknown parameter type: " + entry.get("type"));
      }
    }
    Collections.sort(result);
    return result;
  }

  AnnotatorConfigParameter parseConfigParameter(Map<String, Object> entry) {
    AnnotatorParamDefinition type = null;
    String parameterValue = null;
    int order = 0;

    for (String key : entry.keySet()) {
      Object value = entry.get(key);
      if (key.equalsIgnoreCase("name")) {
        type = AnnotatorParamDefinition.valueOf((String) value);
      } else if (key.equalsIgnoreCase("value")) {
        parameterValue = (String) value;
      } else if (key.equalsIgnoreCase("type")) {
        continue;
      } else if (key.equalsIgnoreCase("commonName")) {
        continue;
      } else if (key.equalsIgnoreCase("inputType")) {
        continue;
      } else if (key.equalsIgnoreCase("description")) {
        continue;
      } else if (key.equalsIgnoreCase("order")) {
        if (value instanceof Integer) {
          order = (Integer) value;
        } else {
          order = Integer.valueOf((String) value);
        }
      } else {
        throw new InvalidArgumentException("Unknown annotator property: " + key);
      }
    }

    AnnotatorConfigParameter result = new AnnotatorConfigParameter(type, parameterValue);
    result.setOrderPosition(order);

    return result;
  }

  AnnotatorParameter parseOutputParameter(Map<String, Object> entry) {
    BioEntityField field = null;
    MiriamType annotationType = null;
    AnnotatorParameter result;
    int order = 0;

    for (String key : entry.keySet()) {
      Object value = entry.get(key);
      if (key.equalsIgnoreCase("field")) {
        field = parseBioEntityField(value);
      } else if (key.equalsIgnoreCase("annotation_type")) {
        annotationType = extractMiriamType(value);
      } else if (key.equalsIgnoreCase("type")) {
        continue;
      } else if (key.equalsIgnoreCase("order")) {
        if (value instanceof Integer) {
          order = (Integer) value;
        } else {
          order = Integer.valueOf((String) value);
        }
      } else {
        throw new InvalidArgumentException("Unknown annotator property: " + key);
      }
    }
    if (field != null && annotationType != null) {
      throw new InvalidArgumentException("Either field or annotation_type must be defined (both found)");
    } else if (field == null && annotationType == null) {
      throw new InvalidArgumentException("Either field or annotation_type must be defined (none found)");
    } else if (field != null) {
      result = new AnnotatorOutputParameter(field);
    } else {
      result = new AnnotatorOutputParameter(annotationType);
    }
    result.setOrderPosition(order);
    return result;
  }

  private MiriamType extractMiriamType(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof MiriamType) {
      return (MiriamType) value;
    }
    return MiriamType.valueOf((String) value);
  }

  private BioEntityField parseBioEntityField(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof BioEntityField) {
      return (BioEntityField) value;
    }
    return BioEntityField.valueOf((String) value);
  }

  AnnotatorInputParameter parseInputParameter(Map<String, Object> entry) {
    BioEntityField field = null;
    MiriamType annotationType = null;
    int order = 0;

    for (String key : entry.keySet()) {
      Object value = entry.get(key);
      if (key.equalsIgnoreCase("field")) {
        field = parseBioEntityField(value);
      } else if (key.equalsIgnoreCase("annotation_type")) {
        annotationType = extractMiriamType(value);
      } else if (key.equalsIgnoreCase("order")) {
        if (value instanceof Integer) {
          order = (Integer) value;
        } else if (value != null) {
          order = Integer.valueOf((String) value);
        }
      } else if (key.equalsIgnoreCase("type")) {
        continue;
      } else {
        throw new InvalidArgumentException("Unknown annotator property: " + key);
      }
    }
    AnnotatorInputParameter result = new AnnotatorInputParameter(field, annotationType);
    result.setOrderPosition(order);
    return result;
  }

  private void updateElementAnnotators(UserAnnotationSchema schema, Map<String, Object> data) throws QueryException {
    for (String key : data.keySet()) {
      UserClassAnnotators annotator = null;
      for (UserClassAnnotators userClassAnnotators : schema.getClassAnnotators()) {
        if (userClassAnnotators.getClassName().equals(key)) {
          annotator = userClassAnnotators;
        }
      }
      if (annotator == null) {
        annotator = new UserClassAnnotators();
        annotator.setClassName(key);
        schema.addClassAnnotator(annotator);
      }
      List<AnnotatorData> annotators = new ArrayList<>();

      if (!(data.get(key) instanceof List)) {
        throw new QueryException("Expected list of annotators but got: " + data.get(key));
      }
      List<?> serialializedAnnotatorsData = (List<?>) data.get(key);
      for (int i = 0; i < serialializedAnnotatorsData.size(); i++) {
        if (serialializedAnnotatorsData.get(i) instanceof Map) {
          annotators.add(parseAnnotator((Map<String, Object>) serialializedAnnotatorsData.get(i)));
        } else {
          throw new QueryException(
              "Expected dictionary describing annotator but got: " + serialializedAnnotatorsData.get(i));
        }
      }
      annotator.setAnnotators(annotators);
    }
  }

  private Map<String, Object> prepareProjectUploadPreferences(UserAnnotationSchema schema) {
    Map<String, Object> result = new TreeMap<>();
    result.put("validate-miriam", schema.getValidateMiriamTypes());
    result.put("annotate-model", schema.getAnnotateModel());
    result.put("cache-data", schema.getCacheData());
    result.put("auto-resize", schema.getAutoResizeMap());
    result.put("semantic-zooming", schema.getSemanticZooming());
    result.put("semantic-zooming-contains-multiple-overlays", schema.getSemanticZoomContainsMultipleOverlays());
    result.put("sbgn", schema.getSbgnFormat());
    return result;
  }

  private List<Map<String, Object>> preparePrivileges(User user) {
    List<Map<String, Object>> result = new ArrayList<>();
    List<Privilege> privileges = new ArrayList<>(user.getPrivileges());
    for (Privilege privilege : privileges) {
      result.add(preparePrivilege(privilege));
    }
    return result;
  }

  private Map<String, Object> preparePrivilege(Privilege privilege) {
    Map<String, Object> result = new TreeMap<>();
    if (privilege.getClass().equals(Privilege.class)) {
      result.put("privilegeType", privilege.getType());
      result.put("objectId", privilege.getObjectId());
      return result;
    } else {
      throw new InvalidArgumentException("Don't know how to handle class: " + privilege.getClass());
    }
  }

  public List<Map<String, Object>> getUsers(String columns) {
    Set<String> columnSet = createUserColumnSet(columns);
    List<Map<String, Object>> result = new ArrayList<>();
    List<User> users = getUserService().getUsers();
    Map<String, Boolean> ldapAvailability = getUserService().ldapAccountExistsForLogin(users);
    for (User user : users) {
      result.add(prepareUse(user, columnSet, ldapAvailability.get(user.getLogin())));
    }
    return result;
  }

  public Map<String, Object> updatePrivileges(String login, Map<String, Object> privilegesData) throws QueryException {
    if (privilegesData == null) {
      throw new QueryException("Privileges not defined");
    }

    User user = getUserService().getUserByLogin(login);
    if (user == null) {
      throw new QueryException("User does not exist.");
    }

    for (String privilege : privilegesData.keySet()) {
      String[] split = privilege.split(":");

      if (split.length > 2) {
        throw new QueryException("Invalid privilege format.");
      }

      PrivilegeType type;
      try {
        type = PrivilegeType.valueOf(split[0]);
      } catch (IllegalArgumentException e) {
        throw new QueryException("Privilege does not exist.", e);
      }

      boolean grant;
      try {
        grant = (boolean) privilegesData.get(privilege);
      } catch (ClassCastException e) {
        throw new QueryException("Privilege can only be set to true (grant) or false (revoke).", e);
      }
      boolean isObjectPrivilege = split.length > 1;

      if (isObjectPrivilege) {
        if (grant) {
          getUserService().grantUserPrivilege(user, type, split[1]);
        } else {
          getUserService().revokeUserPrivilege(user, type, split[1]);
        }
      } else {
        if (grant) {
          getUserService().grantUserPrivilege(user, type);
        } else {
          getUserService().revokeUserPrivilege(user, type);
        }
      }
    }
    return getUser(login, "");
  }

  public Map<String, Object> updatePreferences(String login, Map<String, Object> preferencesData)
      throws QueryException {
    if (preferencesData == null) {
      throw new QueryException("Preferences not defined");
    }
    try {
      User modifiedUser = getUserService().getUserByLogin(login);
      if (modifiedUser == null) {
        throw new ObjectNotFoundException("User doesn't exist");
      }

      UserAnnotationSchema schema = getProjectService().prepareUserAnnotationSchema(modifiedUser);

      for (String key : preferencesData.keySet()) {
        Map<String, Object> value = (Map<String, Object>) preferencesData.get(key);

        if (key.equals("project-upload")) {
          updateUploadPreferences(schema, value);
        } else if (key.equals("element-annotators")) {
          updateElementAnnotators(schema, value);
        } else if (key.equals("element-required-annotations")) {
          updateRequiredAnnotations(schema, value);
        } else if (key.equals("element-valid-annotations")) {
          updateValidAnnotations(schema, value);
        } else if (key.equals("gui-preferences")) {
          updateGuiPreferences(schema, value);
        } else {
          throw new QueryException("Unknown preferences field: " + key);
        }
      }
      modifiedUser.setAnnotationSchema(schema);
      getUserService().updateUser(modifiedUser);
      return getUser(login, "preferences");
    } catch (IllegalArgumentException e) {
      throw new QueryException("Invalid input", e);
    }
  }

  private void updateUploadPreferences(UserAnnotationSchema schema, Map<String, Object> data) throws QueryException {
    for (String key : data.keySet()) {
      Boolean value = (Boolean) data.get(key);
      if (value != null) {
        if (key.equals("validate-miriam")) {
          schema.setValidateMiriamTypes(value);
        } else if (key.equals("annotate-model")) {
          schema.setAnnotateModel(value);
        } else if (key.equals("cache-data")) {
          schema.setCacheData(value);
        } else if (key.equals("auto-resize")) {
          schema.setAutoResizeMap(value);
        } else if (key.equals("semantic-zooming")) {
          schema.setSemanticZooming(value);
        } else if (key.equals("semantic-zooming-contains-multiple-overlays")) {
          schema.setSemanticZoomContainsMultipleOverlays(value);
        } else if (key.equals("sbgn")) {
          schema.setSbgnFormat(value);
        } else {
          throw new QueryException("Unknown upload preference field: " + key);
        }
      }
    }

  }

  public Map<String, Object> updateUser(String login, Map<String, Object> userData,
      Collection<? extends GrantedAuthority> authorities)
      throws QueryException {
    boolean isAdmin = authorities.contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));

    if (userData == null) {
      throw new QueryException("user field cannot be undefined");
    }
    User user = getUserService().getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exist");
    }
    for (String key : userData.keySet()) {
      Object value = userData.get(key);
      String stringValue = null;
      if (value instanceof String) {
        stringValue = (String) value;
      }
      if (key.equalsIgnoreCase("name")) {
        user.setName(stringValue);
      } else if (key.equalsIgnoreCase("surname")) {
        user.setSurname(stringValue);
      } else if (key.equalsIgnoreCase("email")) {
        user.setEmail(stringValue);
      } else if (key.equalsIgnoreCase("termsofuseconsent")) {
        user.setTermsOfUseConsent((Boolean) value);
      } else if (key.equalsIgnoreCase("connectedtoldap")) {
        if (isAdmin) {
          user.setConnectedToLdap((Boolean) value);
        } else {
          throw new AccessDeniedException("connectedtoldap can be updated by admin");
        }
      } else if (key.equalsIgnoreCase("password")) {
        if (stringValue != null && !stringValue.trim().isEmpty()) {
          user.setCryptedPassword(passwordEncoder.encode(stringValue));
        }
      } else if (key.equalsIgnoreCase("login")) {
        if (!user.getLogin().equals((String) value)) {
          throw new QueryException("login cannot be modified");
        }
      } else {
        throw new QueryException("Unknown parameter: " + key);
      }
    }
    getUserService().updateUser(user);
    return getUser(login, "");
  }

  public Map<String, Object> addUser(String login, MultiValueMap<String, Object> userData) throws QueryException {
    User user = getUserService().getUserByLogin(login);
    if (user != null) {
      throw new ObjectExistsException("user exists");
    }
    user = new User();
    user.setLogin(login);
    boolean defaultPrivileges = false;
    for (String key : userData.keySet()) {
      String stringValue = getFirstValue(userData.get(key));
      if (key.equalsIgnoreCase("name")) {
        user.setName(stringValue);
      } else if (key.equalsIgnoreCase("surname")) {
        user.setSurname(stringValue);
      } else if (key.equalsIgnoreCase("email")) {
        user.setEmail(stringValue);
      } else if (key.equalsIgnoreCase("password")) {
        if (stringValue != null && !stringValue.trim().isEmpty()) {
          user.setCryptedPassword(passwordEncoder.encode(stringValue));
        }
      } else if (key.equalsIgnoreCase("login")) {
        if (!user.getLogin().equals(stringValue)) {
          throw new QueryException("login must match url");
        }
      } else if (key.equalsIgnoreCase("defaultPrivileges")) {
        if (stringValue != null) {
          defaultPrivileges = "true".equalsIgnoreCase(stringValue);
        }
      } else {
        throw new QueryException("Unknown parameter: " + key);
      }
    }
    if (user.getCryptedPassword() == null) {
      throw new QueryException("password cannot be null");
    }
    getUserService().addUser(user);
    if (defaultPrivileges) {
      getUserService().grantDefaultPrivileges(user);
    }
    return getUser(login, "");
  }

  public Map<String, Object> removeUser(String login) throws QueryException {
    User user = getUserService().getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exists");
    } else if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      throw new OperationNotAllowedException("guest account cannot be removed");
    }
    getUserService().deleteUser(user);
    return okStatus();
  }

}
