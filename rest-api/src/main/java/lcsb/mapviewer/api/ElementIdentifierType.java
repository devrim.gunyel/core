package lcsb.mapviewer.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Enum identifying type of {@link ElementIdentifier}.
 * 
 * @author Piotr Gawron
 *
 */
public enum ElementIdentifierType {

  /**
   * Type representing alias object on the map.
   */
  ALIAS("ALIAS"),

  /**
   * Type representing any point on the map.
   */
  POINT("POINT"),

  /**
   * Type representing reaction object on the map.
   */
  REACTION("REACTION");

  private static Logger logger = LogManager.getLogger(ElementIdentifierType.class);
  /**
   * String used in JavaScript code to identify type.
   */
  private String jsName;

  /**
   * Constructor initializing enum type.
   * 
   * @param jsName
   *          {@link #jsName}
   */
  ElementIdentifierType(String jsName) {
    this.jsName = jsName;
  }

  /**
   * Returns {@link ElementIdentifierType} by the JavaScript name.
   *
   * @param jsName
   *          JavaScript name of the type
   * @return {@link ElementIdentifierType} by the JavaScript name
   */
  public static ElementIdentifierType getTypeByJsName(String jsName) {
    for (ElementIdentifierType value : values()) {
      if (value.getJsName().equalsIgnoreCase(jsName)) {
        return value;
      }
    }
    logger.warn("Cannot find ElementIdentifierType for jsName: " + jsName);
    return null;
  }

  /**
   * @return the jsName
   * @see #jsName
   */
  public String getJsName() {
    return jsName;
  }

}