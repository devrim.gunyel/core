package lcsb.mapviewer.api.projects.models;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/projects/{projectId:.+}/models", produces = MediaType.APPLICATION_JSON_VALUE)
public class ModelController extends BaseController {

  Logger logger = LogManager.getLogger();

  private ModelRestImpl modelController;
  private IUserService userService;

  @Autowired
  public ModelController(ModelRestImpl modelController, IUserService userService) {
    this.modelController = modelController;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getModels(@PathVariable(value = "projectId") String projectId)
      throws QueryException {
    return modelController.getModels(projectId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modelId:.+}")
  public Object getModel(
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "projectId") String projectId) throws QueryException {
    if (modelId.equals("*")) {
      return modelController.getModels(projectId);
    } else {
      return modelController.getModel(projectId, modelId);
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PatchMapping(value = "/{modelId:.+}")
  public Object updateModel(
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "projectId") String projectId,
      @RequestBody String body) throws IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "model");
    return modelController.updateModel(projectId, modelId, data);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modelId:.+}:downloadImage")
  public ResponseEntity<byte[]> getModelAsImage(
      Authentication authentication,
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "handlerClass") String handlerClass,
      @RequestParam(value = "backgroundOverlayId", defaultValue = "") String backgroundOverlayId,
      @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds,
      @RequestParam(value = "zoomLevel", defaultValue = "") String zoomLevel,
      @RequestParam(value = "polygonString", defaultValue = "") String polygonString)
      throws QueryException, IOException, InvalidColorSchemaException, CommandExecutionException, DrawingException {
    User user = userService.getUserByLogin(authentication.getName());
    FileEntry file = modelController.getModelAsImage(
        projectId, modelId, handlerClass, backgroundOverlayId, overlayIds, zoomLevel, polygonString, user);
    return ResponseEntity.ok().contentLength(file.getFileContent().length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/{modelId:.+}:downloadModel", method = { RequestMethod.GET,
      RequestMethod.POST }, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE, "application/zip" })
  public ResponseEntity<byte[]> getModelAsModelFile(
      Authentication authentication,
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "handlerClass") String handlerClass,
      @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds,
      @RequestParam(value = "backgroundOverlayId", defaultValue = "") String backgroundOverlayId,
      @RequestParam(value = "polygonString", defaultValue = "") String polygonString,
      @RequestParam(value = "elementIds", defaultValue = "") String elementIds,
      @RequestParam(value = "reactionIds", defaultValue = "") String reactionIds,
      @RequestHeader(value = "Accept", required = false) String accept)
      throws Exception {

    User user = userService.getUserByLogin(authentication.getName());
    FileEntry file = modelController.getModelAsModelFile(
        projectId, modelId, handlerClass, overlayIds, polygonString, elementIds, reactionIds, user,
        backgroundOverlayId);
    if (isMimeTypeIncluded(accept, MimeType.ZIP.getTextRepresentation())) {
      return sendAsZip(file);
    }
    if (file.getFileContent().length >= 1024 * 1024) {
      return sendAsZip(file);
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  private boolean isMimeTypeIncluded(String accept, String mimeType) {
    List<MediaType> mediaTypes = MediaType.parseMediaTypes(accept);
    for (MediaType mediaType : mediaTypes) {
      if (Objects.equals(mediaType.getType() + "/" + mediaType.getSubtype(), mimeType)) {
        return true;
      }
    }
    return false;
  }
}