package lcsb.mapviewer.api.projects.drugs;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.api.*;

@RestController
@RequestMapping(value = "/projects/{projectId}/", produces = MediaType.APPLICATION_JSON_VALUE)
public class DrugController extends BaseController {

  private DrugRestImpl drugController;

  @Autowired
  public DrugController(DrugRestImpl drugController) {
    this.drugController = drugController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "drugs:search")
  public List<Map<String, Object>> getDrugsByQuery(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "query", defaultValue = "") String query,
      @RequestParam(value = "target", defaultValue = "") String target) throws QueryException, ObjectNotFoundException {
    if (!query.equals("")) {
      return drugController.getDrugsByQuery(projectId, columns, query);
    } else if (target.contains(":")) {
      String targetType = target.split(":", -1)[0];
      String targetId = target.split(":", -1)[1];
      return drugController.getDrugsByTarget(projectId, targetType, targetId, columns);
    } else {
      return new ArrayList<>();
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "drugs/suggestedQueryList")
  public List<String> getSuggestedQueryList(@PathVariable(value = "projectId") String projectId)
      throws DrugSearchException, ObjectNotFoundException {
    return drugController.getSuggestedQueryList(projectId);
  }

}