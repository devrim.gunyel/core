package lcsb.mapviewer.api.projects.models.units;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/units", produces = MediaType.APPLICATION_JSON_VALUE)
public class UnitsController extends BaseController {

  private UnitsRestImpl unitController;

  @Autowired
  public UnitsController(UnitsRestImpl unitController) {
    this.unitController = unitController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{unitId}")
  public Map<String, Object> getUnit(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "unitId") String unitId) throws QueryException {
    return unitController.getUnit(projectId, modelId, unitId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getUnits(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId) throws QueryException {
    return unitController.getUnits(projectId, modelId);
  }

}