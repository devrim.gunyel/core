package lcsb.mapviewer.api.projects.mirnas;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.mirna.IMiRNAService;

@Transactional
@Service
public class MiRnaRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(MiRnaRestImpl.class);

  private IMiRNAService miRnaService;

  private MiRNAParser miRNAParser;

  @Autowired
  public MiRnaRestImpl(IMiRNAService miRnaService, MiRNAParser miRNAParser) {
    this.miRnaService = miRnaService;
    this.miRNAParser = miRNAParser;
  }

  public List<Map<String, Object>> getMiRnasByQuery(String projectId, String columns, String query)
      throws QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);

    Set<String> columnSet = createMiRnaColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    MiRNA miRna = miRnaService.getByName(query,
        new DbSearchCriteria().project(project).organisms(organism).colorSet(0));
    if (miRna != null) {
      List<Model> models = getModels(projectId, "*");
      result.add(prepareMiRna(miRna, columnSet, models));
    }

    return result;
  }

  private Map<String, Object> prepareMiRna(MiRNA miRna, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value;
      switch (column) {
      case "id":
      case "idobject":
      case "name":
        value = miRna.getName();
        break;
      case "targets":
        value = prepareTargets(miRna.getTargets(), models);
        break;
      default:
        value = "Unknown column";
        break;
      }
      result.put(string, value);
    }
    return result;
  }

  private Set<String> createMiRnaColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("id");
      columnsSet.add("targets");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

  public List<Map<String, Object>> getMiRnasByTarget(String projectId, String targetType, String targetId,
      String columns) throws QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);

    List<Model> models = getModels(projectId, "*");

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      Element element = null;
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createMiRnaColumnSet(columns);

    List<MiRNA> miRnas = miRnaService.getForTargets(targets,
        new DbSearchCriteria().project(project).organisms(organism));

    List<Map<String, Object>> result = new ArrayList<>();

    for (MiRNA miRna : miRnas) {
      result.add(prepareMiRna(miRna, columnSet, models));
    }

    return result;
  }

  public List<String> getSuggestedQueryList(String projectId) throws MiRNASearchException, ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return miRNAParser.getSuggestedQueryList(project);
  }

}
