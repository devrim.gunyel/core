package lcsb.mapviewer.api.projects.models.parameters;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/parameters", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParametersController extends BaseController {

  private ParametersRestImpl parameterController;

  @Autowired
  public ParametersController(ParametersRestImpl parameterController) {
    this.parameterController = parameterController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{parameterId}")
  public Map<String, Object> getParameter(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "parameterId") String parameterId) throws QueryException {
    return parameterController.getParameter(projectId, modelId, parameterId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getParameters(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId) throws QueryException {
    return parameterController.getParameters(projectId, modelId);
  }

}