package lcsb.mapviewer.api.projects.models.parameters;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;

@Transactional
@Service
public class ParametersRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ParametersRestImpl.class);

  public Map<String, Object> getParameter(String projectId, String modelId, String parameterId)
      throws QueryException {
    Set<SbmlParameter> globalParameters = getGlobalParametersFromProject(projectId, modelId);
    Set<SbmlParameter> parameters = getParametersFromProject(projectId, modelId);
    int id = Integer.valueOf(parameterId);
    for (SbmlParameter parameter : parameters) {
      if (parameter.getId() == id) {
        return parameterToMap(parameter, globalParameters.contains(parameter));
      }
    }
    throw new ObjectNotFoundException("Parameter with given id doesn't exist");
  }

  private Set<SbmlParameter> getParametersFromProject(String projectId, String modelId)
      throws QueryException {
    List<Model> models = getModels(projectId, modelId);
    Set<SbmlParameter> parameters = new LinkedHashSet<>();

    for (Model model : models) {
      parameters.addAll(model.getParameters());
      for (Reaction reaction : model.getReactions()) {
        if (reaction.getKinetics() != null) {
          parameters.addAll(reaction.getKinetics().getParameters());
        }
      }
    }
    return parameters;
  }

  private Set<SbmlParameter> getGlobalParametersFromProject(String projectId, String modelId)
      throws QueryException {
    List<Model> models = getModels(projectId, modelId);
    Set<SbmlParameter> parameters = new LinkedHashSet<>();

    for (Model model : models) {
      parameters.addAll(model.getParameters());
    }
    return parameters;
  }

  private Map<String, Object> parameterToMap(SbmlParameter parameter, boolean global) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", parameter.getId());
    result.put("parameterId", parameter.getParameterId());
    result.put("name", parameter.getName());
    result.put("value", parameter.getValue());
    result.put("global", global);
    if (parameter.getUnits() != null) {
      result.put("unitsId", parameter.getUnits().getId());
    } else {
      result.put("unitsId", null);
    }
    return result;
  }

  public List<Map<String, Object>> getParameters(String projectId, String modelId)
      throws QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    Set<SbmlParameter> globalParameters = getGlobalParametersFromProject(projectId, modelId);
    Set<SbmlParameter> parameters = getParametersFromProject(projectId, modelId);
    for (SbmlParameter parameter : parameters) {
      result.add(parameterToMap(parameter, globalParameters.contains(parameter)));
    }
    return result;
  }
}
