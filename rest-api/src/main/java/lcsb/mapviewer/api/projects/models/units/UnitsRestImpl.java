package lcsb.mapviewer.api.projects.models.units;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.model.Model;

@Transactional
@Service
public class UnitsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(UnitsRestImpl.class);

  public Map<String, Object> getUnit(String projectId, String modelId, String unitId)
      throws QueryException {
    List<Model> models = getModels(projectId, modelId);
    int id = Integer.valueOf(unitId);
    for (Model model : models) {
      for (SbmlUnit unit : model.getUnits()) {
        if (unit.getId() == id) {
          return unitToMap(unit);
        }
      }
    }
    throw new ObjectNotFoundException("Unit with given id doesn't exist");
  }

  private Map<String, Object> unitToMap(SbmlUnit unit) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", unit.getId());
    result.put("unitId", unit.getUnitId());
    result.put("name", unit.getName());
    List<Map<String, Object>> factors = new ArrayList<>();
    for (SbmlUnitTypeFactor factor : unit.getUnitTypeFactors()) {
      factors.add(factorToMap(factor));
    }
    result.put("unitTypeFactors", factors);
    return result;
  }

  private Map<String, Object> factorToMap(SbmlUnitTypeFactor factor) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", factor.getId());
    result.put("exponent", factor.getExponent());
    result.put("multiplier", factor.getMultiplier());
    result.put("scale", factor.getScale());
    result.put("unitType", factor.getUnitType());
    return result;
  }

  public List<Map<String, Object>> getUnits(String projectId, String modelId) throws QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    List<Model> models = getModels(projectId, modelId);
    for (Model model : models) {
      for (SbmlUnit unit : model.getUnits()) {
        result.add(unitToMap(unit));
      }
    }
    return result;
  }
}
