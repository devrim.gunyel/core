package lcsb.mapviewer.api.projects.models.bioEntities.reactions;

import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping("/projects/{projectId}/models/{modelId}/bioEntities/reactions")
public class ReactionsController extends BaseController {

  private ReactionsRestImpl reactionController;

  public ReactionsController(ReactionsRestImpl reactionController) {
    this.reactionController = reactionController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getReactions(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "id", defaultValue = "") String id,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "participantId", defaultValue = "") String participantId) throws QueryException {
    return reactionController.getReactions(projectId, id, columns, modelId, participantId);
  }

}