package lcsb.mapviewer.api.projects.overlays;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/projects/{projectId}/overlays", produces = MediaType.APPLICATION_JSON_VALUE)
public class OverlayController extends BaseController {

  private OverlayRestImpl overlayRestImp;
  private IUserService userService;

  public OverlayController(OverlayRestImpl overlayRestImp,
      IUserService userService) {
    this.overlayRestImp = overlayRestImp;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostFilter("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and (filterObject['creator'] == authentication.name or filterObject['publicOverlay'])")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getOverlayList(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "creator", defaultValue = "") String creator,
      @RequestParam(value = "publicOverlay", defaultValue = "false") boolean publicOverlay)
      throws lcsb.mapviewer.api.ObjectNotFoundException {
    return overlayRestImp.getOverlayList(projectId).stream()
        .filter(overlay -> !publicOverlay || (Boolean) overlay.get("publicOverlay"))
        .filter(
            overlay -> creator.isEmpty() || (overlay.get("creator") != null && overlay.get("creator").equals(creator)))
        .collect(Collectors.toList());
  }

  @PostAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and (returnObject['creator'] == authentication.name or returnObject['publicOverlay'])")
  @GetMapping(value = "/{overlayId}/")
  public Map<String, Object> getOverlayById(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "overlayId") Integer overlayId) throws QueryException {
    return overlayRestImp.getOverlayById(projectId, overlayId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and " +
      "   (@layoutService.getOverlayCreator(#overlayId)?.login == authentication.name or @layoutService.getLayoutById(#overlayId)?.publicLayout == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/")
  public List<Map<String, Object>> getOverlayElements(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "overlayId") Integer overlayId,
      @RequestParam(value = "columns", defaultValue = "") String columns) throws QueryException {
    return overlayRestImp.getOverlayElements(projectId, Integer.valueOf(overlayId), columns);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and " +
      "   (@layoutService.getOverlayCreator(#overlayId)?.login == authentication.name or @layoutService.getLayoutById(#overlayId)?.publicLayout == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/reactions/{reactionId}/")
  public Map<String, Object> getFullReaction(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "overlayId") Integer overlayId,
      @PathVariable(value = "reactionId") String reactionId,
      @RequestParam(value = "columns", defaultValue = "") String columns)
      throws QueryException, NumberFormatException, ObjectNotFoundException {
    return overlayRestImp.getOverlayElement(projectId, Integer.valueOf(modelId), Integer.valueOf(overlayId),
        Integer.valueOf(reactionId), "REACTION", columns);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and " +
      "   (@layoutService.getOverlayCreator(#overlayId)?.login == authentication.name or @layoutService.getLayoutById(#overlayId)?.publicLayout == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/elements/{elementId}/")
  public Map<String, Object> getFullSpecies(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "overlayId") Integer overlayId,
      @PathVariable(value = "elementId") String reactionId,
      @RequestParam(value = "columns", defaultValue = "") String columns)
      throws QueryException, NumberFormatException, ObjectNotFoundException {
    return overlayRestImp.getOverlayElement(projectId, Integer.valueOf(modelId), Integer.valueOf(overlayId),
        Integer.valueOf(reactionId), "ALIAS", columns);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))" +
      " or (hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId))" +
      " or (hasAuthority('READ_PROJECT:' + #projectId) and hasAuthority('CAN_CREATE_OVERLAYS'))")
  @PostMapping(value = "/")
  public Map<String, Object> addOverlay(
      Authentication authentication,
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "description") String description,
      @RequestParam(value = "content", defaultValue = "") String content,
      @RequestParam(value = "fileId", defaultValue = "") String fileId,
      @RequestParam(value = "filename") String filename,
      @RequestParam(value = "googleLicenseConsent") String googleLicenseConsent,
      @RequestParam(value = "type", defaultValue = "") String type)
      throws QueryException, IOException, SecurityException {
    User user = userService.getUserByLogin(authentication.getName());
    return overlayRestImp.addOverlay(
        projectId, name, description, content, fileId, filename, type, googleLicenseConsent, user);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      "or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)" +
      "or hasAuthority('READ_PROJECT:' + #projectId) and @layoutService.getOverlayCreator(#overlayId)?.login == authentication.name")
  @DeleteMapping(value = "/{overlayId}")
  public Map<String, Object> removeOverlay(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "overlayId") Integer overlayId) throws QueryException, IOException {
    return overlayRestImp.removeOverlay(projectId, overlayId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and @layoutService.getOverlayCreator(#overlayId)?.login == authentication.name")
  @PatchMapping(value = "/{overlayId}")
  public Map<String, Object> updateOverlay(
      @RequestBody String body,
      @PathVariable(value = "overlayId") Integer overlayId,
      @PathVariable(value = "projectId") String projectId)
      throws QueryException, IOException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "overlay");
    return overlayRestImp.updateOverlay(projectId, overlayId, data);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or hasAuthority('READ_PROJECT:' + #projectId) and " +
      "   (@layoutService.getOverlayCreator(#overlayId)?.login == authentication.name or @layoutService.getLayoutById(#overlayId)?.publicLayout == true)")
  @GetMapping(value = "/{overlayId}:downloadSource")
  public ResponseEntity<byte[]> getOverlaySource(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "overlayId") Integer overlayId)
      throws QueryException {

    FileEntry file = overlayRestImp.getOverlaySource(projectId, overlayId);
    MediaType type = MediaType.TEXT_PLAIN;
    String filename = file.getOriginalFileName();
    if (filename != null) {
      if (file.getOriginalFileName().endsWith("xml")) {
        type = MediaType.APPLICATION_XML;
      }
    } else {
      filename = overlayId + ".txt";
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + filename).body(file.getFileContent());
  }
}