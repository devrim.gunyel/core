package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;

@Transactional
@Service
public class ElementsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ElementsRestImpl.class);

  public List<Map<String, Object>> getElements(String projectId, String id, String columns, String modelId,
      String type, String includedCompartmentIds, String excludedCompartmentIds)
      throws QueryException {
    Set<Integer> ids = new LinkedHashSet<>();
    if (!id.equals("")) {
      for (String str : id.split(",")) {
        ids.add(Integer.valueOf(str));
      }
    }
    Set<String> types = new LinkedHashSet<>();
    if (!type.isEmpty()) {
      for (String str : type.split(",")) {
        types.add(str.toLowerCase());
      }
    }
    List<Model> models = getModels(projectId, modelId);

    Set<Compartment> includedCompartments = getCompartments(includedCompartmentIds, models);
    Set<Compartment> excludedCompartments = getCompartments(excludedCompartmentIds, models);
    Set<String> columnsSet = createElementColumnSet(columns);

    List<Element> elements = new ArrayList<>();

    for (Model model2 : models) {
      for (Element element : model2.getElements()) {
        if (ids.size() == 0 || ids.contains(element.getId())) {
          if (types.size() == 0 || types.contains(element.getStringType().toLowerCase())) {
            if (matchIncludedExcludedCompartments(element, includedCompartments, excludedCompartments)) {
              elements.add(element);
            }
          }
        }
      }
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Element element : elements) {
      result.add(preparedElement(element, columnsSet));

    }
    return result;
  }

  private boolean matchIncludedExcludedCompartments(Element element, Set<Compartment> includedCompartments,
      Set<Compartment> excludedCompartments) {
    boolean matchIncluded = true;
    boolean matchExcluded = false;

    if (includedCompartments.size() > 0) {
      matchIncluded = matchCompartments(element, includedCompartments);
    }
    if (excludedCompartments.size() > 0) {
      matchExcluded = matchCompartments(element, excludedCompartments);
    }

    return matchIncluded && !matchExcluded;
  }

  private boolean matchCompartments(Element element, Set<Compartment> compartmentList) {
    boolean isInside = false;
    if (element != null) {
      for (Compartment compartment : compartmentList) {
        if (compartment.contains(element) && element.getModel().getId().equals(compartment.getModel().getId())) {
          isInside = true;
        }
      }
    }
    return isInside;
  }

  private Set<Compartment> getCompartments(String includedCompartmentIds, List<Model> models) {
    Set<Compartment> includedCompartments = new LinkedHashSet<>();
    int count = 0;
    if (!includedCompartmentIds.isEmpty()) {
      for (String compartmentId : includedCompartmentIds.split(",")) {
        Integer integerId = Integer.valueOf(compartmentId);
        for (Model model : models) {
          Compartment compartment = model.getElementByDbId(integerId);
          if (compartment != null) {
            includedCompartments.add(compartment);
          }
        }
        count++;
      }
      if (count > 0 && includedCompartments.size() == 0) {
        includedCompartments.add(new Compartment("empty_comp_to_reject_all_elements"));
      }
    }
    return includedCompartments;
  }

  private Map<String, Object> preparedElement(Element element, Set<String> columnsSet) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
      case "id":
      case "idobject":
        value = element.getId();
        break;
      case "modelid":
        value = element.getModelData().getId();
        break;
      case "elementid":
        value = element.getElementId();
        break;
      case "name":
        value = element.getName();
        break;
      case "type":
        value = element.getStringType();
        break;
      case "symbol":
        value = element.getSymbol();
        break;
      case "fullname":
        value = element.getFullName();
        break;
      case "abbreviation":
        value = element.getAbbreviation();
        break;
      case "compartmentid":
        if (element.getCompartment() != null) {
          value = element.getCompartment().getId();
        }
        break;
      case "complexid":
        if (element instanceof Species) {
          if (((Species) element).getComplex() != null) {
            value = ((Species) element).getComplex().getId();
          }
        }
        break;
      case "initialconcentration":
        if (element instanceof Species) {
          value = ((Species) element).getInitialConcentration();
        }
        break;
      case "initialamount":
        if (element instanceof Species) {
          value = ((Species) element).getInitialAmount();
        }
        break;
      case "boundarycondition":
        if (element instanceof Species) {
          value = ((Species) element).isBoundaryCondition();
        }
        break;
      case "constant":
        if (element instanceof Species) {
          value = ((Species) element).isConstant();
        }
        break;
      case "references":
        value = createAnnotations(element.getMiriamData());
        break;
      case "synonyms":
        value = element.getSynonyms();
        break;
      case "formula":
        value = element.getFormula();
        break;
      case "notes":
        value = element.getNotes();
        break;
      case "other":
        value = getOthersForElement(element);
        break;
      case "formersymbols":
        value = element.getFormerSymbols();
        break;
      case "hierarchyvisibilitylevel":
        value = element.getVisibilityLevel();
        break;
      case "transparencylevel":
        value = element.getTransparencyLevel();
        break;
      case "linkedsubmodel":
        if (element.getSubmodel() != null) {
          value = element.getSubmodel().getSubmodel().getId();
        }
        break;
      case "bounds":
        value = createBounds(element.getX(), element.getY(), element.getWidth(), element.getHeight());
        break;
      default:
        value = "Unknown column";
        break;
      }
      result.put(string, value);
    }
    return result;
  }

  protected Map<String, Object> getOthersForElement(Element element) {
    Map<String, Object> result = new TreeMap<>();
    List<Map<String, Object>> modifications = new ArrayList<>();
    StructuralState structuralState = null;
    Map<String, Object> structures = new TreeMap<>();
    if (element instanceof Protein) {
      Protein protein = ((Protein) element);
      modifications = getModifications(protein.getModificationResidues());
      structuralState = protein.getStructuralState();
    } else if (element instanceof Rna) {
      Rna rna = ((Rna) element);
      modifications = getModifications(rna.getRegions());
    } else if (element instanceof AntisenseRna) {
      AntisenseRna antisenseRna = ((AntisenseRna) element);
      modifications = getModifications(antisenseRna.getRegions());
    } else if (element instanceof Gene) {
      Gene gene = ((Gene) element);
      modifications = getModifications(gene.getModificationResidues());
    }
    if (element instanceof Species) {
      structures = getStructures(((Species) element).getUniprots());
    }
    result.put("modifications", modifications);
    if (structuralState != null) {
      result.put("structuralState", structuralState.getValue());
    } else {
      result.put("structuralState", null);
    }
    result.put("structures", structures);

    return result;
  }

  private List<Map<String, Object>> getModifications(List<? extends ModificationResidue> elements) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ModificationResidue region : elements) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", region.getName());
      row.put("modificationId", region.getIdModificationResidue());
      if (region instanceof AbstractSiteModification) {
        AbstractSiteModification siteModification = ((AbstractSiteModification) region);
        if (siteModification.getState() != null) {
          row.put("state", siteModification.getState().name());
        }
      }
      String type;
      if (region instanceof Residue) {
        type = ModificationType.RESIDUE.name();
      } else if (region instanceof BindingRegion) {
        type = ModificationType.BINDING_REGION.name();
      } else if (region instanceof CodingRegion) {
        type = ModificationType.CODING_REGION.name();
      } else if (region instanceof ProteinBindingDomain) {
        type = ModificationType.PROTEIN_BINDING_DOMAIN.name();
      } else if (region instanceof RegulatoryRegion) {
        type = ModificationType.REGULATORY_REGION.name();
      } else if (region instanceof TranscriptionSite) {
        if (((TranscriptionSite) region).getDirection().equals("LEFT")) {
          type = ModificationType.TRANSCRIPTION_SITE_LEFT.name();
        } else {
          type = ModificationType.TRANSCRIPTION_SITE_RIGHT.name();
        }
      } else if (region instanceof ModificationSite) {
        type = ModificationType.MODIFICATION_SITE.name();
      } else {
        throw new InvalidArgumentException("Unknown class: " + region.getClass());
      }
      row.put("type", type);
      result.add(row);
    }
    return result;
  }

  private Map<String, Object> getStructures(Set<UniprotRecord> uniprots) {
    Map<String, Object> result = new TreeMap<>();
    for (UniprotRecord uniprotRec : uniprots) {
      Set<Object> structs = new LinkedHashSet<>();
      for (Structure struct : uniprotRec.getStructures()) {
        structs.add(struct.toMap());
      }
      result.put(uniprotRec.getUniprotId(), structs);
    }
    return result;
  }

  private Map<String, Object> createBounds(Double x, Double y, Double width, Double height) {
    Map<String, Object> result = new TreeMap<>();
    result.put("x", x);
    result.put("y", y);
    result.put("width", width);
    result.put("height", height);
    return result;
  }

  private Set<String> createElementColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("id");
      columnsSet.add("elementId");
      columnsSet.add("modelId");
      columnsSet.add("name");
      columnsSet.add("type");
      columnsSet.add("notes");
      columnsSet.add("symbol");
      columnsSet.add("complexId");
      columnsSet.add("compartmentId");
      columnsSet.add("fullName");
      columnsSet.add("abbreviation");
      columnsSet.add("formula");
      columnsSet.add("synonyms");
      columnsSet.add("formerSymbols");
      columnsSet.add("references");
      columnsSet.add("bounds");
      columnsSet.add("hierarchyVisibilityLevel");
      columnsSet.add("transparencyLevel");
      columnsSet.add("linkedSubmodel");
      columnsSet.add("other");
      columnsSet.add("initialConcentration");
      columnsSet.add("boundaryCondition");
      columnsSet.add("constant");
      columnsSet.add("initialAmount");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

}
