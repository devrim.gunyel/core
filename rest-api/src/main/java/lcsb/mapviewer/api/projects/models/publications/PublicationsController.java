package lcsb.mapviewer.api.projects.models.publications;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/publications", produces = MediaType.APPLICATION_JSON_VALUE)
public class PublicationsController extends BaseController {

  private PublicationsRestImpl projectController;

  @Autowired
  public PublicationsController(PublicationsRestImpl projectController) {
    this.projectController = projectController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public Map<String, Object> getPublications(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "start", defaultValue = "0") String start,
      @RequestParam(value = "length", defaultValue = "10") Integer length,
      @RequestParam(value = "sortColumn", defaultValue = "pubmedId") String sortColumn,
      @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
      @RequestParam(value = "search", defaultValue = "") String search) throws QueryException {
    return projectController.getPublications(projectId, modelId, start, length, sortColumn, sortOrder, search);
  }

}