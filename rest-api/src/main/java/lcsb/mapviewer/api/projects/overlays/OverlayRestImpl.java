package lcsb.mapviewer.api.projects.overlays;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.ILayoutService.CreateLayoutParams;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

@Transactional
@Service
public class OverlayRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(OverlayRestImpl.class);

  private ILayoutService layoutService;

  private UploadedFileEntryDao uploadedFileEntryDao;

  private LayoutDao layoutDao;

  private ColorParser colorParser = new ColorParser();

  @Autowired
  public OverlayRestImpl(ILayoutService layoutService, UploadedFileEntryDao uploadedFileEntryDao, LayoutDao layoutDao) {
    this.layoutService = layoutService;
    this.uploadedFileEntryDao = uploadedFileEntryDao;
    this.layoutDao = layoutDao;
  }

  public List<Map<String, Object>> getOverlayList(String projectId) throws ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return overlaysToMap(layoutService.getLayoutsByProject(project));
  }

  private List<Map<String, Object>> overlaysToMap(List<Layout> overlays) {
    overlays.sort(Layout.ID_COMPARATOR);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Layout overlay : overlays) {
      result.add(overlayToMap(overlay));
    }
    return result;
  }

  private Map<String, Object> overlayToMap(Layout overlay) {

    Map<String, Object> result = new TreeMap<>();
    result.put("idObject", overlay.getId());
    result.put("name", overlay.getTitle());
    result.put("order", overlay.getOrderIndex());
    result.put("description", overlay.getDescription());
    result.put("publicOverlay", overlay.isPublicLayout());
    result.put("defaultOverlay", overlay.isDefaultOverlay());

    result.put("deprecatedColumns", getDeprecatedColumns(overlay));
    result.put("googleLicenseConsent", overlay.isGoogleLicenseConsent());
    List<Map<String, Object>> images = new ArrayList<>();
    List<DataOverlayImageLayer> imageList = new ArrayList<>(overlay.getDataOverlayImageLayers());
    imageList.sort(DataOverlayImageLayer.ID_COMPARATOR);

    for (DataOverlayImageLayer child : imageList) {
      Map<String, Object> image = new TreeMap<>();
      image.put("path", child.getDirectory());
      image.put("modelId", child.getModel().getId());
      images.add(image);
    }
    result.put("images", images);
    if (overlay.getCreator() != null) {
      result.put("creator", overlay.getCreator().getLogin());
    }
    result.put("inputDataAvailable", overlay.getInputData() != null);
    return result;
  }

  private List<String> getDeprecatedColumns(Layout overlay) {
    try {
      List<String> result = new ArrayList<>();
      for (Pair<ColorSchemaColumn, String> entry : new ColorSchemaReader().getDeprecatedColumns(overlay)) {
        result.add(entry.getRight());
      }
      return result;
    } catch (IOException | InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    }
  }

  public List<Map<String, Object>> getOverlayElements(String projectId, int overlayId, String columns)
      throws QueryException {

    String[] columnSet;
    if (columns != null && !columns.trim().isEmpty()) {
      columnSet = columns.split(",");
    } else {
      columnSet = new String[] { "modelId", "idObject", "value", "color", "uniqueId", "width" };
    }
    List<Map<String, Object>> result = new ArrayList<>();

    getOverlay(projectId, overlayId);

    Model model = getModelService().getLastModelByProjectId(projectId);
    List<Pair<Element, ColorSchema>> speciesList = layoutService.getAliasesForLayout(model, overlayId);
    for (Pair<Element, ColorSchema> elementDataOverlay : speciesList) {
      Map<String, Object> element = new TreeMap<>();
      element.put("type", ElementIdentifierType.ALIAS);
      element.put("overlayContent", overlayContentToMap(elementDataOverlay, columnSet));
      result.add(element);
    }

    List<Pair<Reaction, ColorSchema>> reactions = layoutService.getReactionsForLayout(model, overlayId);
    for (Pair<Reaction, ColorSchema> reactionDataOverlay : reactions) {
      Map<String, Object> element = new TreeMap<>();
      element.put("type", ElementIdentifierType.REACTION);
      element.put("overlayContent", overlayContentToMap(reactionDataOverlay, columnSet));
      result.add(element);
    }
    return result;
  }

  public Map<String, Object> getOverlayById(String projectId, Integer overlayId)
      throws QueryException {
    Layout overlay = getOverlay(projectId, overlayId);
    return overlayToMap(overlay);
  }

  private Layout getOverlay(String projectId, Integer overlayId) throws ObjectNotFoundException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Layout overlay = layoutService.getLayoutById(overlayId);
    if (overlay == null) {
      throw new ObjectNotFoundException("Overlay with given id doesn't exist");
    }
    if (overlay.getProject().getId() != model.getProject().getId()) {
      throw new ObjectNotFoundException("Overlay with given id doesn't exist");
    }
    return overlay;
  }

  public FileEntry getOverlaySource(String projectId, Integer overlayId)
      throws QueryException {
    Layout overlay = getOverlay(projectId, overlayId);
    // lazy initialization issue
    overlay.getInputData().getFileContent();
    return overlay.getInputData();
  }

  public Map<String, Object> updateOverlay(String projectId, Integer overlayId, Map<String, Object> overlayData)
      throws QueryException {
    if (overlayData == null) {
      throw new QueryException("overlay field cannot be undefined");
    }
    List<User> reorderUsers = new ArrayList<>();
    try {
      Layout layout = getOverlay(projectId, overlayId);
      for (String key : overlayData.keySet()) {
        Object value = overlayData.get(key);
        if (key.equalsIgnoreCase("description")) {
          layout.setDescription((String) value);
        } else if (key.equalsIgnoreCase("name")) {
          if (value == null || ((String) value).trim().isEmpty()) {
            throw new QueryException("name cannot be empty");
          }
          layout.setTitle((String) value);
        } else if (key.equalsIgnoreCase("order")) {
          layout.setOrderIndex(parseInteger(value));
        } else if (key.equalsIgnoreCase("publicOverlay")) {
          layout.setPublicLayout(parseBoolean(value));
        } else if (key.equalsIgnoreCase("defaultOverlay")) {
          layout.setDefaultOverlay(parseBoolean(value));
        } else if (key.equalsIgnoreCase("googleLicenseConsent")) {
          layout.setGoogleLicenseConsent((Boolean) overlayData.get("googleLicenseConsent"));
        } else if (key.equalsIgnoreCase("creator")) {
          if ("".equals(value)) {
            if (layout.getCreator() != null) {
              reorderUsers.add(layout.getCreator());
            }
            layout.setCreator(null);
          } else {
            if (layout.getCreator() == null) {
              reorderUsers.add(getUserService().getUserByLogin((String) value));
            } else if (!layout.getCreator().getLogin().equals(value)) {
              reorderUsers.add(getUserService().getUserByLogin((String) value));
              reorderUsers.add(layout.getCreator());
            }
            layout.setCreator(getUserService().getUserByLogin((String) value));
          }
        } else {
          throw new QueryException("Unknown parameter: " + key);
        }
      }
      layoutDao.update(layout);
      for (User user : reorderUsers) {
        reorderOverlays(user, layout.getProject());
      }
      return getOverlayById(layout.getProject().getProjectId(), overlayId);
    } catch (NumberFormatException e) {
      throw new QueryException("invliad overlay id", e);
    }
  }

  private boolean parseBoolean(Object value) {
    if (value instanceof Boolean) {
      return (Boolean) value;
    } else {
      return "true".equalsIgnoreCase((String) value);
    }
  }

  public Map<String, Object> removeOverlay(String projectId, Integer overlayId)
      throws QueryException, IOException {
    Layout layout = getOverlay(projectId, overlayId);
    User owner = layout.getCreator();
    Project project = layout.getProject();
    layoutService.removeLayout(layout, null);

    reorderOverlays(owner, project);
    return okStatus();
  }

  private void reorderOverlays(User owner, Project project) {
    List<Layout> overlays = layoutService.getLayoutsByProject(project).stream()
        .filter(lay -> lay.getCreator() == owner).collect(Collectors.toList());

    overlays.sort(Layout.ORDER_COMPARATOR);
    for (int i = 0; i < overlays.size(); i++) {
      Layout overlay = overlays.get(i);
      if (overlay.getOrderIndex() != i + 1) {
        overlay.setOrderIndex(i + 1);
        layoutService.updateLayout(overlay);
      }
    }
  }

  public Map<String, Object> addOverlay(String projectId, String name, String description, String content,
      String fileId, String filename, String type, String googleLicenseConsent, User user)
      throws QueryException, IOException, SecurityException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    ColorSchemaType colorSchemaType = ColorSchemaType.GENERIC;
    if (type != null && !type.equals("")) {
      try {
        colorSchemaType = ColorSchemaType.valueOf(type);
      } catch (IllegalArgumentException e) {
        throw new QueryException("Invalid type of overlay: " + type, e);
      }
    }
    if (content.isEmpty() && fileId.isEmpty()) {
      throw new QueryException("Either content or fileId must be provided");
    }

    if (name == null || name.trim().isEmpty()) {
      throw new QueryException("Name cannot be empty");
    }
    try {
      InputStream stream;
      if (!content.isEmpty()) {
        stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
      } else {
        try {
          int id = Integer.valueOf(fileId);
          UploadedFileEntry file = uploadedFileEntryDao.getById(id);
          if (file == null) {
            throw new QueryException("Invalid file id: " + fileId);
          }
          if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
            throw new SecurityException("Access denied to source file");
          }
          stream = new ByteArrayInputStream(file.getFileContent());
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid fileId: " + fileId);
        }
      }

      Layout layout = layoutService
          .createLayout(new CreateLayoutParams()
              .async(false)
              .colorInputStream(stream)
              .description(description)
              .layoutFileName(filename)
              .name(name).user(user)
              .colorSchemaType(colorSchemaType)
              .directory(".")
              .project(model.getProject())
              .googleLicenseConsent(googleLicenseConsent.equalsIgnoreCase("true")));

      int count = (int) layoutService.getLayoutsByProject(model.getProject()).stream()
          .filter(lay -> lay.getCreator() != null && lay.getCreator().getLogin().equals(user.getLogin()))
          .count();
      layout.setOrderIndex(count);
      layoutService.updateLayout(layout);

      return getOverlayById(projectId, layout.getId());
    } catch (InvalidColorSchemaException e) {
      throw new QueryException(e.getMessage(), e);
    }

  }

  public Map<String, Object> getOverlayElement(String projectId, Integer modelId, Integer overlayId,
      Integer elementId, String elementType, String columns) throws QueryException {
    getOverlay(projectId, overlayId);

    Model topModel = getModelService().getLastModelByProjectId(projectId);
    Model model = topModel.getSubmodelById(modelId);

    String[] columnSet;
    if (columns != null && !columns.trim().isEmpty()) {
      columnSet = columns.split(",");
    } else {
      columnSet = new String[] { "modelId", "idObject", "value", "color", "description", "type", "geneVariations",
          "uniqueId" };
    }
    Map<String, Object> result = new TreeMap<>();
    if (ElementIdentifierType.ALIAS.getJsName().equals(elementType)) {
      Pair<? extends BioEntity, ColorSchema> elementDataOverlay = layoutService.getFullAliasForLayout(model, elementId,
          overlayId);
      if (elementDataOverlay == null) {
        throw new ObjectNotFoundException("Element data cannot be found");
      }
      result.put("type", ElementIdentifierType.ALIAS);
      result.put("overlayContent", overlayContentToMap(elementDataOverlay, columnSet));
      return result;
    } else if (ElementIdentifierType.REACTION.getJsName().equals(elementType)) {
      Pair<? extends BioEntity, ColorSchema> reactionDataOverlay = layoutService.getFullReactionForLayout(model,
          elementId, overlayId);
      if (reactionDataOverlay == null) {
        throw new ObjectNotFoundException("Reaction data cannot be found");
      }
      result.put("type", ElementIdentifierType.REACTION);
      result.put("overlayContent", overlayContentToMap(reactionDataOverlay, columnSet));
      return result;
    } else {
      throw new QueryException("Unknown element type: " + elementType);
    }
  }

  private Map<String, Object> overlayContentToMap(Pair<? extends BioEntity, ColorSchema> bioEntityDataOverlay,
      String[] columns) {
    Map<String, Object> result = new LinkedHashMap<>();
    BioEntity bioEntity = bioEntityDataOverlay.getLeft();
    ColorSchema colorSchema = bioEntityDataOverlay.getRight();
    for (String string : columns) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
      case "id":
      case "idobject":
      case "uniqueid":
        // casting to string is only to provide the same results as before refactoring
        value = bioEntity.getId() + "";
        break;
      case "modelid":
        value = bioEntity.getModel().getId();
        break;
      case "value":
        value = colorSchema.getValue();
        break;
      case "color":
        if (colorSchema.getColor() != null) {
          value = colorParser.colorToMap(colorSchema.getColor());
        }
        break;
      case "description":
        value = colorSchema.getDescription();
        break;
      case "width":
        if (bioEntity instanceof Reaction) {
          value = colorSchema.getLineWidth();
        } else {
          continue;
        }
        break;
      case "type":
        if (colorSchema instanceof GeneVariationColorSchema) {
          value = ColorSchemaType.GENETIC_VARIANT;
        } else {
          value = ColorSchemaType.GENERIC;
        }
        break;
      case "genevariations":
        if (colorSchema instanceof GeneVariationColorSchema) {
          value = ((GeneVariationColorSchema) colorSchema).getGeneVariations();
        } else {
          value = new Object[] {};
        }
        break;
      default:
        value = "Unknown column";
        break;
      }
      result.put(string, value);
    }
    return result;
  }

}
