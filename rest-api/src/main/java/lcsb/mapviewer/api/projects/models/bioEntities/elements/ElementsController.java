package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/bioEntities/elements", produces = MediaType.APPLICATION_JSON_VALUE)
public class ElementsController extends BaseController {

  private ElementsRestImpl projectController;

  @Autowired
  public ElementsController(ElementsRestImpl projectController) {
    this.projectController = projectController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST })
  public List<Map<String, Object>> getElements(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "id", defaultValue = "") String id,
      @RequestParam(value = "type", defaultValue = "") String type,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "includedCompartmentIds", defaultValue = "") String includedCompartmentIds,
      @RequestParam(value = "excludedCompartmentIds", defaultValue = "") String excludedCompartmentIds)
      throws QueryException {
    return projectController.getElements(projectId, id, columns, modelId, type, includedCompartmentIds,
        excludedCompartmentIds);
  }
}