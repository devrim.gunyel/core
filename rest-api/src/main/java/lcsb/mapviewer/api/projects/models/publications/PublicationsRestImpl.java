package lcsb.mapviewer.api.projects.models.publications;

import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;

@Transactional
@Service
public class PublicationsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(PublicationsRestImpl.class);

  private PubmedParser pubmedParser;

  @Autowired
  public PublicationsRestImpl(PubmedParser pubmedParser) {
    this.pubmedParser = pubmedParser;
  }

  public SortedMap<MiriamData, List<BioEntity>> getPublications(Collection<Model> models) {
    SortedMap<MiriamData, List<BioEntity>> publications = new TreeMap<>();

    List<BioEntity> bioEntities = new ArrayList<>();
    for (Model modelData : models) {
      bioEntities.addAll(modelData.getBioEntities());
    }
    bioEntities.sort(BioEntity.ID_COMPARATOR);
    for (BioEntity bioEntity : bioEntities) {
      for (MiriamData md : bioEntity.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.PUBMED)) {
          List<BioEntity> list = publications.computeIfAbsent(md, k -> new ArrayList<>());
          list.add(bioEntity);
        }
      }

    }
    return publications;
  }

  public Map<String, Object> getPublications(
      String projectId, String modelId, String startString, Integer length,
      String sortColumn, String sortOrder, String search) throws QueryException {
    List<Model> models = getModels(projectId, modelId);

    SortColumn sortColumnEnum = getSortOrderColumn(sortColumn);
    Comparator<Map.Entry<MiriamData, List<BioEntity>>> comparator = getComparatorForColumn(sortColumnEnum, sortOrder);

    Integer start = Math.max(0, Integer.valueOf(startString));
    List<Map<String, Object>> resultList = new ArrayList<>();

    SortedMap<MiriamData, List<BioEntity>> publications = getPublications(models);
    List<Map.Entry<MiriamData, List<BioEntity>>> filteredList = new ArrayList<>();

    for (Map.Entry<MiriamData, List<BioEntity>> entry : publications.entrySet()) {
      Set<Model> publicationModels = new LinkedHashSet<>();
      for (BioEntity bioEntity : entry.getValue()) {
        publicationModels.add(bioEntity.getModel());
      }
      if (isSearchResult(entry.getKey(), search, publicationModels)) {
        filteredList.add(entry);
      }
    }
    if (comparator != null) {
      filteredList.sort(comparator);
    }

    int index = 0;
    for (Map.Entry<MiriamData, List<BioEntity>> entry : filteredList) {
      if (index >= start && index < start + length) {
        List<Object> elements = new ArrayList<>();
        for (BioEntity object : entry.getValue()) {
          elements.add(createMinifiedSearchResult(object));
        }

        Map<String, Object> row = new TreeMap<>();
        row.put("elements", elements);
        row.put("publication", createAnnotation(entry.getKey()));
        resultList.add(row);
      }
      index++;
    }

    Map<String, Object> result = new TreeMap<>();
    result.put("data", resultList);
    result.put("totalSize", publications.size());
    result.put("filteredSize", filteredList.size());
    result.put("start", start);
    result.put("length", resultList.size());
    return result;
  }

  Comparator<Entry<MiriamData, List<BioEntity>>> getComparatorForColumn(SortColumn sortColumnEnum,
      String sortOrder) {
    final int orderFactor;
    if (sortOrder.toLowerCase().equals("desc")) {
      orderFactor = -1;
    } else {
      orderFactor = 1;
    }
    if (sortColumnEnum == null) {
      return null;
    } else if (sortColumnEnum.equals(SortColumn.PUBMED_ID)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        IntegerComparator integerComparator = new IntegerComparator();

        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          Integer id1 = extractPubmedId(o1.getKey());
          Integer id2 = extractPubmedId(o2.getKey());
          return integerComparator.compare(id1, id2) * orderFactor;

        }

      };
    } else if (sortColumnEnum.equals(SortColumn.YEAR)) {
      return (o1, o2) -> {
        try {
          Article article1 = getArticle(o1.getKey().getResource());
          Article article2 = getArticle(o2.getKey().getResource());
          return article1.getYear().compareTo(article2.getYear()) * orderFactor;
        } catch (Exception e) {
          logger.error("Problem with accessing article data ", e);
          return 0;
        }
      };
    } else if (sortColumnEnum.equals(SortColumn.JOURNAL)) {
      return (o1, o2) -> {
        try {
          Article article1 = getArticle(o1.getKey().getResource());
          Article article2 = getArticle(o2.getKey().getResource());
          return article1.getJournal().compareTo(article2.getJournal()) * orderFactor;
        } catch (Exception e) {
          logger.error("Problem with accessing article data ", e);
          return 0;
        }
      };
    } else if (sortColumnEnum.equals(SortColumn.TITLE)) {
      return (o1, o2) -> {
        try {
          Article article1 = getArticle(o1.getKey().getResource());
          Article article2 = getArticle(o2.getKey().getResource());
          return article1.getTitle().compareTo(article2.getTitle()) * orderFactor;
        } catch (Exception e) {
          logger.error("Problem with accessing article data ", e);
          return 0;
        }

      };
    } else if (sortColumnEnum.equals(SortColumn.AUTHORS)) {
      return (o1, o2) -> {
        try {
          Article article1 = getArticle(o1.getKey().getResource());
          Article article2 = getArticle(o2.getKey().getResource());
          return article1.getStringAuthors().compareTo(article2.getStringAuthors()) * orderFactor;
        } catch (Exception e) {
          logger.error("Problem with accessing article data ", e);
          return 0;
        }
      };
    } else {
      throw new InvalidArgumentException("Unknown column type: " + sortColumnEnum);
    }
  }

  private SortColumn getSortOrderColumn(String sortColumn) throws QueryException {
    if (!sortColumn.isEmpty()) {
      for (SortColumn type : SortColumn.values()) {
        if (type.commonName.toLowerCase().equals(sortColumn.toLowerCase())) {
          return type;
        }
      }
      throw new QueryException("Unknown sortColumn: " + sortColumn);
    }
    return null;
  }

  private boolean isSearchResult(MiriamData key, String search, Set<Model> models) {
    String lowerCaseSearch = search.toLowerCase();
    if (search.isEmpty()) {
      return true;
    }
    Article article = null;
    if (MiriamType.PUBMED.equals(key.getDataType())) {
      try {
        article = pubmedParser.getPubmedArticleById(Integer.valueOf(key.getResource()));
      } catch (PubmedSearchException e) {
        logger.error("Problem with accessing info about pubmed", e);
      }
    }
    if (article != null) {
      if (article.getId().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getJournal().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getStringAuthors().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getTitle().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getYear().toString().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else {
        for (Model model : models) {
          if (model.getName().toLowerCase().contains(lowerCaseSearch)) {
            return true;

          }
        }
      }
    }
    return false;
  }

  /**
   * @return the pubmedParser
   * @see #pubmedParser
   */
  public PubmedParser getPubmedParser() {
    return pubmedParser;
  }

  /**
   * @param pubmedParser
   *          the pubmedParser to set
   * @see #pubmedParser
   */
  public void setPubmedParser(PubmedParser pubmedParser) {
    this.pubmedParser = pubmedParser;
  }

  private Article getArticle(String resource) throws NumberFormatException, PubmedSearchException {
    return pubmedParser.getPubmedArticleById(Integer.valueOf(resource));
  }

  private Integer extractPubmedId(MiriamData md) {
    if (NumberUtils.isDigits(md.getResource())) {
      return Integer.valueOf(md.getResource());
    } else {
      return null;
    }

  }

  enum SortColumn {
    PUBMED_ID("pubmedId"),
    YEAR("year"),
    JOURNAL("journal"),
    TITLE("title"),
    AUTHORS("authors");

    private String commonName;

    SortColumn(String commonName) {
      this.commonName = commonName;
    }
  }

}
