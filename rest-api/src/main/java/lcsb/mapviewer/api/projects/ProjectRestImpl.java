package lcsb.mapviewer.api.projects;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.*;
import lcsb.mapviewer.api.projects.models.publications.PublicationsRestImpl;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.zip.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.CreateProjectParams;

@Transactional
@Service
public class ProjectRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(ProjectRestImpl.class);

  private PublicationsRestImpl publicationsRestImpl;

  private IProjectService projectService;
  private IUserService userService;

  private MeSHParser meshParser;

  private ProjectDao projectDao;

  private UploadedFileEntryDao uploadedFileEntryDao;

  public ProjectRestImpl(PublicationsRestImpl publicationsRestImpl,
      IProjectService projectService,
      ProjectDao projectDao,
      UploadedFileEntryDao uploadedFileEntryDao,
      IUserService userService,
      MeSHParser meshParser) {
    this.publicationsRestImpl = publicationsRestImpl;
    this.projectService = projectService;
    this.projectDao = projectDao;
    this.meshParser = meshParser;
    this.userService = userService;
    this.uploadedFileEntryDao = uploadedFileEntryDao;
  }

  public Map<String, Object> getProject(String projectId) throws ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return createData(project);
  }

  private Map<String, Object> createData(Project project) {
    Map<String, Object> result = new LinkedHashMap<>();

    result.put("version", project.getVersion());
    if (project.getDisease() != null) {
      result.put("disease", createAnnotation(project.getDisease()));
    } else {
      result.put("disease", null);
    }
    if (project.getOrganism() != null) {
      result.put("organism", createAnnotation(project.getOrganism()));
    } else {
      result.put("organism", null);
    }
    result.put("idObject", project.getId());
    if (project.getStatus() != null) {
      result.put("status", project.getStatus().toString());
    }
    result.put("directory", project.getDirectory());
    result.put("progress", project.getProgress());
    result.put("notifyEmail", project.getNotifyEmail());
    result.put("logEntries", project.getLogEntries().size() > 0);
    result.put("name", project.getName());
    result.put("owner", project.getOwner().getLogin());
    result.put("projectId", project.getProjectId());
    result.put("creationDate", super.prepareDate(project.getCreationDate()));
    result.put("mapCanvasType", project.getMapCanvasType());

    List<Map<String, Object>> images = new ArrayList<>();
    for (OverviewImage image : project.getOverviewImages()) {
      images.add(imageToMap(image));
    }
    result.put("overviewImageViews", images);

    Set<OverviewImage> set = new LinkedHashSet<>();
    set.addAll(project.getOverviewImages());
    for (OverviewImage image : project.getOverviewImages()) {
      for (OverviewLink ol : image.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          set.remove(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    if (set.size() > 0) {
      result.put("topOverviewImage", imageToMap(set.iterator().next()));
    } else if (project.getOverviewImages().size() > 0) {
      logger.warn(
          "Cannot determine top level image. Taking first one. " + project.getOverviewImages().get(0).getFilename());
      result.put("topOverviewImage", imageToMap(project.getOverviewImages().get(0)));
    } else {
      result.put("topOverviewImage", null);
    }

    return result;
  }

  private Map<String, Object> imageToMap(OverviewImage image) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("idObject", image.getId());
    result.put("filename", image.getProject().getDirectory() + "/" + image.getFilename());
    result.put("width", image.getWidth());
    result.put("height", image.getHeight());
    List<Map<String, Object>> links = new ArrayList<>();
    for (OverviewLink link : image.getLinks()) {
      links.add(overviewLinkToMap(link));
    }
    result.put("links", links);
    return result;
  }

  private Map<String, Object> overviewLinkToMap(OverviewLink link) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("idObject", link.getId());
    result.put("polygon", polygonToMap(link.getPolygon()));
    if (link instanceof OverviewModelLink) {
      OverviewModelLink modelLink = (OverviewModelLink) link;
      result.put("zoomLevel", modelLink.getZoomLevel());
      result.put("modelPoint", new Point2D.Double(modelLink.getxCoord(), modelLink.getyCoord()));
      result.put("modelLinkId", modelLink.getLinkedModel().getId());

    } else if (link instanceof OverviewImageLink) {
      result.put("imageLinkId", ((OverviewImageLink) link).getLinkedOverviewImage().getId());
    } else if (link instanceof OverviewSearchLink) {
      result.put("query", ((OverviewSearchLink) link).getQuery());
    } else {
      throw new NotImplementedException("Not implemented behaviour for class: " + link.getClass());
    }
    result.put("type", link.getClass().getSimpleName());
    return result;
  }

  private List<Map<String, Double>> polygonToMap(String polygon) {
    List<Map<String, Double>> result = new ArrayList<>();
    for (String string : polygon.split(" ")) {
      String tmp[] = string.split(",");
      Map<String, Double> point = new TreeMap<>();
      point.put("x", Double.parseDouble(tmp[0]));
      point.put("y", Double.parseDouble(tmp[1]));
      result.add(point);
    }
    return result;
  }

  public FileEntry getSource(String projectId) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (project.getInputData() != null) {
      // fetch the data from db
      project.getInputData().getFileContent();
    }
    return project.getInputData();
  }

  public Map<String, Object> getStatistics(String projectId) throws QueryException {
    Map<String, Object> result = new TreeMap<>();

    Map<MiriamType, Integer> elementAnnotations = new TreeMap<>();
    for (MiriamType mt : MiriamType.values()) {
      elementAnnotations.put(mt, 0);
    }
    List<Model> models = super.getModels(projectId, "*");

    for (Model model2 : models) {
      for (Element alias : model2.getElements()) {
        for (MiriamData md : alias.getMiriamData()) {
          Integer amount = elementAnnotations.get(md.getDataType());
          amount += 1;
          elementAnnotations.put(md.getDataType(), amount);
        }
      }
    }
    result.put("elementAnnotations", elementAnnotations);

    Map<MiriamType, Integer> reactionAnnotations = new TreeMap<>();
    for (MiriamType mt : MiriamType.values()) {
      reactionAnnotations.put(mt, 0);
    }
    for (Model model2 : models) {
      for (Reaction reaction : model2.getReactions()) {
        for (MiriamData md : reaction.getMiriamData()) {
          Integer amount = reactionAnnotations.get(md.getDataType());
          amount += 1;
          reactionAnnotations.put(md.getDataType(), amount);
        }
      }
    }

    result.put("reactionAnnotations", reactionAnnotations);

    result.put("publications", publicationsRestImpl.getPublications(models).size());

    return result;
  }

  public List<Map<String, Object>> getProjects() {
    List<Project> projects = getProjectService().getAllProjects();
    List<Map<String, Object>> result = new ArrayList<>();
    for (Project project : projects) {
      result.add(createData(project));
    }
    return result;
  }

  public Map<String, Object> updateProject(String projectId, Map<String, Object> data)
      throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Set<String> fields = data.keySet();
    for (String fieldName : fields) {
      Object value = data.get(fieldName);
      String stringValue = null;
      if (value instanceof String) {
        stringValue = (String) value;
      }
      if (fieldName.equalsIgnoreCase("version")) {
        if (value != null && ((String) value).length() > 20) {
          throw new QueryException("version is too long (>20 characters)");
        }
        project.setVersion((String) value);
      } else if (fieldName.equalsIgnoreCase("id")) {
        int id = parseInteger(stringValue, "id");
        if (id != project.getId()) {
          throw new QueryException("Invalid id: " + stringValue);
        }
      } else if (fieldName.equalsIgnoreCase("projectId")) {
        if (!project.getProjectId().equalsIgnoreCase(stringValue)) {
          throw new QueryException("You cannot modify projectId");
        }
      } else if (fieldName.equalsIgnoreCase("name")) {
        if (value != null && ((String) value).length() > 255) {
          throw new QueryException("name is too long (>255 characters)");
        }
        project.setName((String) value);
      } else if (fieldName.equalsIgnoreCase("notifyEmail")) {
        project.setNotifyEmail(stringValue);
      } else if (fieldName.equalsIgnoreCase("organism")) {
        MiriamData organism = updateMiriamData(project.getOrganism(), value);
        project.setOrganism(organism);
      } else if (fieldName.equalsIgnoreCase("disease")) {
        try {
          MiriamData sourceData = updateMiriamData(null, value);
          if (meshParser.isValidMeshId(sourceData)) {
            MiriamData disease = updateMiriamData(project.getDisease(), value);
            project.setDisease(disease);
          } else if (sourceData == null || sourceData.getResource().isEmpty()) {
            project.setDisease(null);
          } else {
            throw new QueryException("invalid mesh identifier: " + value);
          }
        } catch (AnnotatorException e) {
          throw new QueryException("invalid miriamdData: " + value, e);
        }
      } else if (fieldName.equalsIgnoreCase("mapCanvasType")) {
        MapCanvasType mapCanvasType;
        try {
          mapCanvasType = MapCanvasType.valueOf(stringValue);
        } catch (Exception e) {
          throw new QueryException("Invalid mapCanvasType value");
        }
        project.setMapCanvasType(mapCanvasType);
      } else {
        throw new QueryException("Unknown field: " + fieldName);
      }
    }
    getProjectService().updateProject(project);
    return getProject(projectId);
  }

  private MiriamData updateMiriamData(MiriamData organism, Object res) {
    if (res == null || res.equals("")) {
      return null;
    }
    if (organism == null) {
      organism = new MiriamData();
    }

    if (res instanceof Map) {
      @SuppressWarnings("unchecked")
      Map<String, Object> map = (Map<String, Object>) res;
      organism.setDataType(MiriamType.valueOf((String) map.get("type")));
      organism.setResource((String) map.get("resource"));
      return organism;
    } else {
      throw new InvalidArgumentException("invalid miriamdData: " + res);
    }
  }

  public Map<String, Object> addProject(String projectId, MultiValueMap<String, Object> data, String path, User user)
      throws QueryException, IOException, SecurityException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project != null) {
      throw new ObjectExistsException("Project with given id already exists");
    }
    if (projectId.length() > 255) {
      throw new QueryException("projectId is too long");
    }
    CreateProjectParams params = new CreateProjectParams();
    String directory = computePathForProject(projectId, path);
    String fileId = getFirstValue(data.get("file-id"));
    if (fileId == null) {
      throw new QueryException("file-id is obligatory");
    }
    UploadedFileEntry file = uploadedFileEntryDao.getById(super.parseInteger(fileId, "fileId"));
    if (file == null) {
      throw new QueryException("Invalid file id: " + fileId);
    }
    if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
      throw new SecurityException("Access denied to source file");
    }
    String parserClass = getFirstValue(data.get("parser"));
    if (parserClass == null) {
      throw new QueryException("parser is obligatory");
    }
    Converter parser = getModelParser(parserClass);

    List<ZipEntryFile> zipEntries = extractZipEntries(data);
    params.complex(zipEntries.size() > 0);
    for (ZipEntryFile entry : zipEntries) {
      params.addZipEntry(entry);
    }

    params.async(true);
    params.parser(parser);
    params.autoResize(getFirstValue(data.get("auto-resize")));
    params.cacheModel(getFirstValue(data.get("cache")));
    params.description(getFirstValue(data.get("description")));
    params.images(true);
    params.notifyEmail(getFirstValue(data.get("notify-email")));
    params.projectDir(directory);
    params.projectDisease(getFirstValue(data.get("disease")));
    params.projectFile(file);
    params.projectId(projectId);
    params.projectName(getFirstValue(data.get("name")));
    if (params.getProjectName() != null && params.getProjectName().length() > 255) {
      throw new QueryException("name is too long");
    }
    params.projectOrganism(getFirstValue(data.get("organism")));
    params.sbgnFormat(getFirstValue(data.get("sbgn")));
    params.semanticZoomContainsMultipleLayouts(getFirstValue(data.get("semantic-zoom-contains-multiple-layouts")));
    params.version(getFirstValue(data.get("version")));
    if (params.getVersion() != null && params.getVersion().length() > 20) {
      throw new QueryException("version is too long (>20 characters)");
    }
    params.annotations(getFirstValue(data.get("annotate")));
    params.setUser(user);
    MapCanvasType mapCanvasType;
    try {
      mapCanvasType = MapCanvasType.valueOf(getFirstValue(data.get("mapCanvasType")));
    } catch (Exception e) {
      throw new QueryException("Invalid mapCanvasType value");
    }
    params.mapCanvasType(mapCanvasType);

    if (params.isUpdateAnnotations()) {
      params.annotatorsMap(projectService.createClassAnnotatorTree(user));
    }
    params.analyzeAnnotations(getFirstValue(data.get("verify-annotations")));
    if (params.isAnalyzeAnnotations()) {
      params.requiredAnnotations(projectService.createClassAnnotatorTree(user));
      params.validAnnotations(projectService.createClassAnnotatorTree(user));
    }

    getProjectService().createProject(params);
    return getProject(projectId);
  }

  protected String computePathForProject(String projectId, String path) {
    long id = projectDao.getNextId();
    return path + "/../map_images/" + md5(projectId + "-" + id) + "/";
  }

  protected List<ZipEntryFile> extractZipEntries(MultiValueMap<String, Object> data) throws QueryException {
    int fileIndex = 0;
    List<ZipEntryFile> result = new ArrayList<>();
    while (data.get("zip-entries[" + fileIndex + "][_filename]") != null) {
      ZipEntryFile entry = null;
      String entryType = (String) data.get("zip-entries[" + fileIndex + "][_type]").get(0);
      String filename = (String) data.get("zip-entries[" + fileIndex + "][_filename]").get(0);
      if ("MAP".equalsIgnoreCase(entryType)) {
        String submodelTypeKey = "zip-entries[" + fileIndex + "][_data][type][id]";
        String rootKey = "zip-entries[" + fileIndex + "][_data][root]";
        String mappingKey = "zip-entries[" + fileIndex + "][_data][mapping]";

        String mapTypeString = getStringValue(data.get(submodelTypeKey), SubmodelType.UNKNOWN.name());
        SubmodelType mapType = SubmodelType.valueOf(mapTypeString);

        String name = (String) data.get("zip-entries[" + fileIndex + "][_data][name]").get(0);
        Boolean root = getBoolValue(data.get(rootKey), false);
        Boolean mapping = getBoolValue(data.get(mappingKey), false);

        entry = new ModelZipEntryFile(filename, name, root, mapping, mapType);
      } else if ("OVERLAY".equalsIgnoreCase(entryType)) {
        String name = (String) data.get("zip-entries[" + fileIndex + "][_data][name]").get(0);
        String description = (String) data.get("zip-entries[" + fileIndex + "][_data][description]").get(0);
        entry = new LayoutZipEntryFile(filename, name, description);
      } else if ("IMAGE".equalsIgnoreCase(entryType)) {
        entry = new ImageZipEntryFile(filename);
      } else if ("GLYPH".equalsIgnoreCase(entryType)) {
        entry = new GlyphZipEntryFile(filename);
      } else {
        throw new QueryException("Unknown entry type: " + entryType);
      }
      fileIndex++;
      result.add(entry);

    }
    return result;
  }

  private Boolean getBoolValue(List<Object> list, boolean defaultValue) {
    if (list == null) {
      return defaultValue;
    }
    Object obj = list.get(0);
    if (obj instanceof Boolean) {
      return (Boolean) list.get(0);
    } else {
      return "true".equalsIgnoreCase((String) obj);
    }
  }

  private String getStringValue(List<Object> list, String defaultValue) {
    if (list == null) {
      return defaultValue;
    }
    if (list.size() == 0) {
      return defaultValue;
    }
    Object obj = list.get(0);
    if (obj instanceof String) {
      return (String) list.get(0);
    } else {
      return obj.toString();
    }
  }

  /**
   * Method that computes md5 hash for a given {@link String}.
   * 
   * @param data
   *          input string
   * @return md5 hash for input string
   */
  private String md5(String data) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] mdbytes = md.digest(data.getBytes());
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < mdbytes.length; i++) {
        // CHECKSTYLE:OFF
        // this magic formula transforms integer into hex value
        sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        // CHECKSTYLE:ON
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      logger.fatal("Problem with instance of MD5 encoder", e);
    }

    return null;
  }

  public Map<String, Object> removeProject(String projectId, String path) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (getConfigurationService().getConfigurationValue(ConfigurationElementType.DEFAULT_MAP)
        .equals(project.getProjectId())) {
      throw new OperationNotAllowedException("You cannot remove default map");
    }
    getProjectService().removeProject(project, path, true);
    return getProject(projectId);
  }

  public Map<String, Object> getLogs(String projectId, String level, String startString, Integer length,
      String sortColumn, String sortOrder, String search) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    LogSortColumn sortColumnEnum = getSortOrderColumn(sortColumn);
    Comparator<LogEntry> comparator = getComparatorForColumn(sortColumnEnum, sortOrder);

    Integer start = Math.max(0, Integer.valueOf(startString));
    List<LogEntry> resultList = new ArrayList<>();

    List<LogEntry> logEntries = getEntries(project, level);

    List<LogEntry> filteredList = new ArrayList<>();

    search = search.toLowerCase();
    for (LogEntry entry : logEntries) {
      if (isSearchResult(entry, search)) {
        filteredList.add(entry);
      }
    }
    if (comparator != null) {
      filteredList.sort(comparator);
    }

    int index = 0;
    for (LogEntry entry : filteredList) {
      if (index >= start && index < start + length) {
        resultList.add(entry);
      }
      index++;
    }

    Map<String, Object> result = new TreeMap<>();
    result.put("data", resultList);
    result.put("totalSize", logEntries.size());
    result.put("filteredSize", filteredList.size());
    result.put("start", start);
    result.put("length", resultList.size());
    return result;

  }

  private boolean isSearchResult(LogEntry entry, String search) {
    if (search == null || search.isEmpty()) {
      return true;
    }
    if (entry.content.toLowerCase().contains(search) || entry.id.toString().contains(search)) {
      return true;
    }

    return false;

  }

  private List<LogEntry> getEntries(Project project, String level) {
    List<LogEntry> result = new ArrayList<>();
    for (ProjectLogEntry s : project.getLogEntries()) {
      if (level == null || level.equals("*") || level.equalsIgnoreCase(s.getSeverity())) {
        result.add(new LogEntry(s));
      }
    }
    return result;
  }

  private Comparator<LogEntry> getComparatorForColumn(LogSortColumn sortColumnEnum, String sortOrder)
      throws QueryException {
    final int orderFactor;
    if (sortOrder.toLowerCase().equals("desc")) {
      orderFactor = -1;
    } else {
      orderFactor = 1;
    }
    StringComparator stringComparator = new StringComparator();
    if (sortColumnEnum == null) {
      return null;
    } else if (sortColumnEnum.equals(LogSortColumn.ID)) {
      return (o1, o2) -> o1.id.compareTo(o2.id) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.CONTENT)) {
      return (o1, o2) -> o1.content.compareTo(o2.content) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.LEVEL)) {
      return (o1, o2) -> o1.level.compareTo(o2.level) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.MAP_NAME)) {
      return (o1, o2) -> stringComparator.compare(o1.mapName, o2.mapName) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.OBJECT_CLASS)) {
      return (o1, o2) -> stringComparator.compare(o1.objectClass, o2.objectClass) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.OBJECT_IDENTIFIER)) {
      return (o1, o2) -> stringComparator.compare(o1.objectIdentifier, o2.objectIdentifier) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.SOURCE)) {
      return (o1, o2) -> stringComparator.compare(o1.source, o2.source) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.TYPE)) {
      return (o1, o2) -> stringComparator.compare(o1.type, o2.type) * orderFactor;
    } else {
      throw new QueryException("Sort order not implemented for: " + sortColumnEnum);
    }
  }

  private LogSortColumn getSortOrderColumn(String sortColumn) throws QueryException {
    if (!sortColumn.isEmpty()) {
      for (LogSortColumn type : LogSortColumn.values()) {
        if (type.commonName.toLowerCase().equals(sortColumn.toLowerCase())) {
          return type;
        }
      }
      throw new QueryException("Unknown sortColumn: " + sortColumn);
    }
    return null;
  }

  public List<Map<String, Object>> getSubmapConnections(String projectId) throws QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    List<Model> models = getModels(projectId, "*");
    List<Element> elements = new ArrayList<>();
    for (Model model : models) {
      elements.addAll(model.getElements());
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    for (Element element : elements) {
      if (element.getSubmodel() != null) {
        result.add(submodelConnectionToMap(element));
      }
    }
    return result;
  }

  private Map<String, Object> submodelConnectionToMap(Element element) {
    Map<String, Object> result = new TreeMap<>();
    result.put("from", super.createMinifiedSearchResult(element));
    Map<String, Object> to = new TreeMap<>();
    to.put("modelId", element.getSubmodel().getSubmodel().getId());
    result.put("to", to);
    return result;
  }

  public Map<String, Object> grantPrivilegesProject(String projectId, Map[] data) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    for (Map m : data) {
      PrivilegeType privilege = getPrivilegeType(m);
      User user = getUser(m);
      userService.grantUserPrivilege(user, privilege, projectId);
    }
    return null;
  }

  private User getUser(Map m) throws QueryException {
    if (!(m.get("login") instanceof String)) {
      throw new QueryException("Invalid login: " + m.get("login"));
    }
    String login = (String) m.get("login");
    User result = userService.getUserByLogin(login);
    if (result == null) {
      throw new ObjectNotFoundException("User doesn't exist: " + login);
    }
    return result;
  }

  public Map<String, Object> revokePrivilegesProject(String projectId, Map[] data) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    for (Map m : data) {
      PrivilegeType privilege = getPrivilegeType(m);
      User user = getUser(m);
      userService.revokeUserPrivilege(user, privilege, projectId);
    }
    return null;
  }

  private PrivilegeType getPrivilegeType(Map m) throws QueryException {
    if (!(m.get("privilegeType") instanceof String)) {
      throw new QueryException("Invalid privilegeType: " + m.get("privilegeType"));
    }
    String type = (String) m.get("privilegeType");
    try {
      PrivilegeType result = PrivilegeType.valueOf(type);
      return result;
    } catch (Exception e) {
      throw new QueryException("Invalid privilegeType: " + type, e);
    }
  }

  private enum LogSortColumn {
    ID("id"),
    LEVEL("level"),
    TYPE("type"),
    OBJECT_IDENTIFIER("objectIdentifier"),
    OBJECT_CLASS("objectClass"),
    MAP_NAME("mapName"),
    SOURCE("source"),
    CONTENT("content");

    private String commonName;

    LogSortColumn(String commonName) {
      this.commonName = commonName;
    }
  }

  private class LogEntry implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public Integer id;
    public String content;
    public String level;
    public String type;
    public String objectIdentifier;
    public String objectClass;
    public String mapName;
    public String source;

    public LogEntry(ProjectLogEntry s) {
      this.id = s.getId();
      this.content = s.getContent();
      this.level = s.getSeverity();
      this.type = s.getType().toString();
      this.objectIdentifier = s.getObjectIdentifier();
      this.objectClass = s.getObjectClass();
      this.mapName = s.getMapName();
      this.source = s.getSource();
    }
  }

}
