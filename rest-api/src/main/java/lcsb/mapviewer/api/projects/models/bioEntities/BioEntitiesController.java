package lcsb.mapviewer.api.projects.models.bioEntities;

import java.awt.geom.Point2D;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/", produces = MediaType.APPLICATION_JSON_VALUE)
public class BioEntitiesController extends BaseController {

  private BioEntitiesRestImpl bioEntitiesRestImpl;

  @Autowired
  public BioEntitiesController(BioEntitiesRestImpl bioEntitiesRestImpl) {
    this.bioEntitiesRestImpl = bioEntitiesRestImpl;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "bioEntities:search")
  public List<Map<String, Object>> getClosestElementsByCoordinates(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @RequestParam(value = "coordinates", defaultValue = "") String coordinates,
      @RequestParam(value = "query", defaultValue = "") String query,
      @RequestParam(value = "count", defaultValue = "") String count,
      @RequestParam(value = "type", defaultValue = "") String type,
      @RequestParam(value = "perfectMatch", defaultValue = "false") String perfectMatch) throws QueryException {
    if (!coordinates.trim().isEmpty()) {
      if (modelId.equals("*")) {
        throw new QueryException("modelId must be defined when searching by coordinates");
      }
      if (count.trim().isEmpty()) {
        count = "5";
      }
      String[] tmp = coordinates.split(",");
      if (tmp.length != 2) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
      }
      Double x = null;
      Double y = null;
      try {
        x = Double.valueOf(tmp[0]);
        y = Double.valueOf(tmp[1]);
      } catch (NumberFormatException e) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
      }
      Point2D pointCoordinates = new Point2D.Double(x, y);
      return bioEntitiesRestImpl.getClosestElementsByCoordinates(projectId, modelId, pointCoordinates,
          Integer.valueOf(count), perfectMatch, type);
    } else if (!query.trim().isEmpty()) {
      if (count.trim().isEmpty()) {
        count = "100";
      }
      return bioEntitiesRestImpl.getElementsByQuery(projectId, query, modelId,  Integer.valueOf(count),
          perfectMatch);
    } else {
      return new ArrayList<>();
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "bioEntities/suggestedQueryList", method = { RequestMethod.GET, RequestMethod.POST })
  public String[] getSuggestedQueryList(@PathVariable(value = "projectId") String projectId) throws ObjectNotFoundException {
    return bioEntitiesRestImpl.getSuggestedQueryList(projectId);
  }

}