package lcsb.mapviewer.api.projects.chemicals;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.chemical.IChemicalService;

@Transactional
@Service
public class ChemicalRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(ChemicalRestImpl.class);

  private IChemicalService chemicalService;

  private ChemicalParser chemicalParser;

  private MeSHParser meSHParser;

  @Autowired
  public ChemicalRestImpl(IChemicalService chemicalService, ChemicalParser chemicalParser, MeSHParser meSHParser) {
    this.chemicalService = chemicalService;
    this.chemicalParser = chemicalParser;
    this.meSHParser = meSHParser;
  }

  public List<Map<String, Object>> getChemicalsByQuery(String projectId, String columns, String query)
      throws QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    Set<String> columnSet = createChemicalColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    Chemical chemical = chemicalService.getByName(query,
        new DbSearchCriteria().project(project).organisms(organism).colorSet(0).disease(project.getDisease()));
    if (chemical != null) {
      List<Model> models = getModels(projectId, "*");
      result.add(prepareChemical(chemical, columnSet, models));
    }

    return result;
  }

  protected Map<String, Object> prepareChemical(Chemical chemical, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();

    String description = "Mesh term not available";
    List<String> synonyms = new ArrayList<>();

    try {
      MeSH mesh = meSHParser.getMeSH(chemical.getChemicalId());
      if (mesh != null) {
        description = mesh.getDescription();
        synonyms = mesh.getSynonyms();
      } else {
        logger.warn("Mesh used by chemical is invalid: " + chemical.getChemicalId());
      }
    } catch (AnnotatorException e) {
      logger.error("Problem with accessing mesh database", e);
    }

    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
      case "id":
      case "idobject":
        value = chemical.getChemicalId();
        break;
      case "name":
        value = chemical.getChemicalName();
        break;
      case "references":
        List<Map<String, Object>> references = new ArrayList<>();
        references.add(createAnnotation(chemical.getChemicalId()));
        if (chemical.getCasID() != null) {
          references.add(createAnnotation(chemical.getCasID()));
        }
        value = references;
        break;
      case "directevidencereferences":
        value = createAnnotations(chemical.getDirectEvidencePublication());
        break;
      case "description":
        value = description;
        break;
      case "directevidence":
        if (chemical.getDirectEvidence() != null) {
          value = chemical.getDirectEvidence().getValue();
        }
        break;
      case "synonyms":
        value = synonyms;
        break;
      case "targets":
        value = prepareTargets(chemical.getInferenceNetwork(), models);
        break;
      default:
        value = "Unknown column";
        break;
      }
      result.put(string, value);
    }
    return result;
  }

  protected Set<String> createChemicalColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("references");
      columnsSet.add("description");
      columnsSet.add("synonyms");
      columnsSet.add("id");
      columnsSet.add("directEvidenceReferences");
      columnsSet.add("directEvidence");
      columnsSet.add("targets");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

  public List<Map<String, Object>> getChemicalsByTarget(String projectId, String targetType,
      String targetId, String columns) throws QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);

    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    List<Model> models = getModels(projectId, "*");
    Element element = null;

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createChemicalColumnSet(columns);

    List<Chemical> chemicals = chemicalService.getForTargets(targets,
        new DbSearchCriteria().project(project).organisms(organism).disease(project.getDisease()));

    List<Map<String, Object>> result = new ArrayList<>();

    for (Chemical chemical : chemicals) {
      result.add(prepareChemical(chemical, columnSet, models));
    }

    return result;
  }

  public List<String> getSuggestedQueryList(String projectId) throws ChemicalSearchException, ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist: " + projectId);
    }
    return chemicalParser.getSuggestedQueryList(project, project.getDisease());
  }
}
