package lcsb.mapviewer.api.projects.models;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.commands.*;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.data.BuildInLayout;

@Transactional
@Service
public class ModelRestImpl extends BaseRestImpl {

  /**
   * Constant defining size of the array returned by
   * {@link PathIterator#currentSegment(double[])} method. More information can be
   * found <a href=
   * "http://docs.oracle.com/javase/7/docs/api/java/awt/geom/PathIterator.html#currentSegment(double[])"
   * >here</a>
   */
  private static final int PATH_ITERATOR_SEGMENT_SIZE = 6;

  private ILayoutService layoutService;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ModelRestImpl.class);

  @Autowired
  public ModelRestImpl(ILayoutService layoutService) {
    this.layoutService = layoutService;
  }

  public List<Map<String, Object>> getModels(String projectId) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return createData(project);
  }

  public Map<String, Object> getModel(String projectId, String modelId) throws QueryException {
    if (!StringUtils.isNumeric(modelId)) {
      throw new QueryException("Invalid modelId: " + modelId);
    }
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Model submodel = model.getSubmodelById(modelId);
    if (submodel == null) {
      return null;
    } else {
      Map<String, Object> result = new LinkedHashMap<>();
      result.put("version", null);
      result.put("name", submodel.getName());
      result.put("idObject", submodel.getId());
      result.put("tileSize", submodel.getTileSize());
      result.put("width", (int) (double) submodel.getWidth());
      result.put("height", (int) (double) submodel.getHeight());
      result.put("defaultCenterX", submodel.getDefaultCenterX());
      result.put("defaultCenterY", submodel.getDefaultCenterY());
      result.put("defaultZoomLevel", submodel.getDefaultZoomLevel());
      result.put("minZoom", Configuration.MIN_ZOOM_LEVEL);
      result.put("maxZoom", Configuration.MIN_ZOOM_LEVEL + submodel.getZoomLevels());
      result.put("submodelType", SubmodelType.UNKNOWN);
      result.put("references", createAnnotations(submodel.getMiriamData()));
      result.put("authors", submodel.getAuthors());
      result.put("creationDate", super.prepareDate(submodel.getCreationDate()));
      result.put("modificationDates", super.prepareDates(submodel.getModificationDates()));
      result.put("description", submodel.getNotes());
      return result;
    }
  }

  private List<Map<String, Object>> createData(Project project) throws QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    Model model = getModelService().getLastModelByProjectId(project.getProjectId());
    if (model != null) {

      List<Model> models = new ArrayList<>();
      models.add(model);
      models.addAll(model.getSubmodels());
      models.sort(Model.ID_COMPARATOR);
      for (Model model2 : models) {
        result.add(getModel(project.getProjectId(), model2.getId() + ""));
      }
    }
    return result;
  }

  public Map<String, Object> updateModel(String projectId, String modelId, Map<String, Object> data)
      throws QueryException {
    if (modelId.equals("*")) {
      throw new QueryException("Invalid model id: " + modelId);
    }
    List<Model> models = super.getModels(projectId, modelId);
    if (models.size() != 1) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    ModelData model = models.get(0).getModelData();
    Set<String> fields = data.keySet();
    for (String fieldName : fields) {
      Object value = data.get(fieldName);
      if (fieldName.equalsIgnoreCase("defaultCenterX")) {
        model.setDefaultCenterX(parseDouble(value));
      } else if (fieldName.equalsIgnoreCase("defaultCenterY")) {
        model.setDefaultCenterY(parseDouble(value));
      } else if (fieldName.equalsIgnoreCase("defaultZoomLevel")) {
        model.setDefaultZoomLevel(parseInteger(value));
      } else if (fieldName.equalsIgnoreCase("id")) {
        if (!model.getId().equals(parseInteger(value))) {
          throw new QueryException("Id doesn't match: " + value + ", " + model.getId());
        }
      } else {
        throw new QueryException("Unknown field: " + fieldName);
      }
    }
    getModelService().updateModel(model);
    return getModel(projectId, modelId);
  }

  private Double parseDouble(Object value) throws QueryException {
    if (value instanceof Double) {
      return (Double) value;
    } else if (value instanceof Integer) {
      return ((Integer) value).doubleValue();
    } else if (value instanceof String) {
      if (((String) value).equalsIgnoreCase("")) {
        return null;
      } else {
        try {
          return Double.parseDouble((String) value);
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid double value: " + value);
        }
      }
    } else if (value == null) {
      return null;
    } else {
      throw new QueryException("Don't know how to change " + value.getClass() + " into Double");
    }
  }

  public FileEntry getModelAsModelFile(String projectId, String modelId, String handlerClass,
      String overlayIds, String polygonString, String elementIds,
      String reactionIds, User user, String backgroundOverlayId)
      throws QueryException, IOException, InvalidColorSchemaException, CommandExecutionException,
      ConverterException, InconsistentModelException {

    Project project = getProjectService().getProjectByProjectId(projectId);

    Model originalModel = getModelByModelId(projectId, modelId);

    Path2D polygon = stringToPolygon(polygonString, originalModel);

    Set<Integer> elementIdsList = stringListToIntegerSet(elementIds);
    Set<Integer> reactionIdsList = stringListToIntegerSet(reactionIds);

    // create model bounded by the polygon
    SubModelCommand subModelCommand = new SubModelCommand(originalModel, polygon, elementIdsList, reactionIdsList);
    Model part = subModelCommand.execute();

    // Get list of overlay ids
    String[] overlayIdsList = stringListToArray(overlayIds);
    // Remove all colors
    if (overlayIdsList.length > 0) {
      new ClearColorModelCommand(part).execute();
    }

    if (!backgroundOverlayId.equals("")) {
      Layout overlay = project.getLayoutByIdentifier(Integer.valueOf(backgroundOverlayId));

      if (overlay == null) {
        throw new ObjectNotFoundException("Unknown overlay in model. Layout.id=" + backgroundOverlayId);
      }
      if (overlay.getTitle().equals(BuildInLayout.CLEAN.getTitle())) {
        // this might not return true if we change CLEAN.title in future...

        // if it's clean then remove coloring
        new ClearColorModelCommand(part).execute();
      }
    }

    // Color with overlays
    for (String overlayId : overlayIdsList) {
      Layout overlay = layoutService.getLayoutById(Integer.parseInt(overlayId.trim()));

      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas = reader.readColorSchema(overlay.getInputData().getFileContent(),
          overlay.getColorSchemaType());

      new ColorModelCommand(part, schemas, getUserService().getColorExtractorForUser(user)).execute();
    }

    Converter parser = getModelParser(handlerClass);
    InputStream is = parser.model2InputStream(part);

    String fileExtension = parser.getFileExtension();

    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("model." + fileExtension);
    entry.setFileContent(IOUtils.toByteArray(is));
    entry.setLength(entry.getFileContent().length);
    return entry;

  }

  protected Set<Integer> stringListToIntegerSet(String elementIds) {
    Set<Integer> result = new LinkedHashSet<>();
    if (elementIds != null) {
      String[] stringIds = elementIds.split(",");
      for (String string : stringIds) {
        if (!string.trim().isEmpty()) {
          result.add(Integer.valueOf(string));
        }
      }
    }
    return result;
  }

  private Model getModelByModelId(String projectId, String modelId) throws ObjectNotFoundException {
    Model topModel = getModelService().getLastModelByProjectId(projectId);
    if (topModel == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Model originalModel = topModel.getSubmodelById(modelId);

    if (originalModel == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }
    return originalModel;
  }

  private String[] stringListToArray(String overlayIds) {
    String[] overlayIdsList;
    if (overlayIds == null || overlayIds.trim().isEmpty()) {
      overlayIdsList = new String[0];
    } else {
      overlayIdsList = overlayIds.split(",");
    }
    return overlayIdsList;
  }

  public FileEntry getModelAsImage(String projectId, String modelId, String handlerClass,
      String backgroundOverlayId, String overlayIds, String zoomLevel, String polygonString, User user)
      throws QueryException, IOException, InvalidColorSchemaException, CommandExecutionException, DrawingException {

    Project project = getProjectService().getProjectByProjectId(projectId);
    Model topModel = getModelService().getLastModelByProjectId(projectId);
    if (topModel == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Model originalModel = topModel.getSubmodelById(modelId);

    if (originalModel == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    Layout overlay = null;
    if (!backgroundOverlayId.equals("")) {
      overlay = project.getLayoutByIdentifier(Integer.valueOf(backgroundOverlayId));

      if (overlay == null) {
        throw new ObjectNotFoundException("Unknown overlay in model. Layout.id=" + backgroundOverlayId);
      }
    } else {
      if (project.getLayouts().size() > 0) {
        overlay = project.getLayouts().get(0);
      }
    }

    Model colorModel = new CopyCommand(originalModel).execute();
    if (overlay != null) {
      if (overlay.getInputData() != null) {
        ColorSchemaReader reader = new ColorSchemaReader();
        Collection<ColorSchema> schemas = reader.readColorSchema(overlay.getInputData().getFileContent(),
            overlay.getColorSchemaType());

        new ColorModelCommand(colorModel, schemas, getUserService().getColorExtractorForUser(user)).execute();
      } else if (overlay.getTitle().equals(BuildInLayout.CLEAN.getTitle())) {
        // this might not return true if we change CLEAN.title in future...

        // if it's clean then remove coloring
        new ClearColorModelCommand(colorModel).execute();
      } else if (overlay.isHierarchicalView()) {
        new SetFixedHierarchyLevelCommand(colorModel, overlay.getHierarchyViewLevel()).execute();
      }
    }

    int level = Configuration.MIN_ZOOM_LEVEL;
    if (!zoomLevel.equals("")) {
      level = Integer.parseInt(zoomLevel);
    }

    Path2D polygon = stringToPolygon(polygonString, colorModel);

    Double minX = originalModel.getWidth();
    Double minY = originalModel.getHeight();
    Double maxX = 0.0;
    Double maxY = 0.0;

    PathIterator pathIter = polygon.getPathIterator(null);
    while (!pathIter.isDone()) {
      final double[] segment = new double[PATH_ITERATOR_SEGMENT_SIZE];
      if (pathIter.currentSegment(segment) != PathIterator.SEG_CLOSE) {
        minX = Math.min(minX, segment[0]);
        maxX = Math.max(maxX, segment[0]);
        minY = Math.min(minY, segment[1]);
        maxY = Math.max(maxY, segment[1]);
      }
      pathIter.next();
    }

    maxX = Math.min(originalModel.getWidth(), maxX);
    maxY = Math.min(originalModel.getHeight(), maxY);
    minX = Math.max(0.0, minX);
    minY = Math.max(0.0, minY);

    double scale = Math.max(originalModel.getHeight(), originalModel.getWidth()) / (originalModel.getTileSize());

    for (int i = level; i > Configuration.MIN_ZOOM_LEVEL; i--) {
      scale /= 2;
    }

    ColorExtractor colorExtractor = getUserService().getColorExtractorForUser(user);

    Params params = new Params().x(minX).y(minY).height((maxY - minY) / scale).width((maxX - minX) / scale)
        .level(level - Configuration.MIN_ZOOM_LEVEL).nested(false).// automatically set nested view as invalid
        scale(scale).colorExtractor(colorExtractor).sbgn(topModel.getProject().isSbgnFormat()).model(colorModel);
    if (overlay != null) {
      params.nested(overlay.isHierarchicalView());
    }
    List<Integer> visibleLayoutIds = deserializeIdList(overlayIds);
    for (Integer integer : visibleLayoutIds) {
      Map<Object, ColorSchema> map = layoutService.getElementsForLayout(colorModel, integer);
      params.addVisibleLayout(map);
    }

    ImageGenerators imageGenerator = new ImageGenerators();
    if (!imageGenerator.isValidClassName(handlerClass)) {
      throw new QueryException("Invalid handlerClass");
    }
    String extension = imageGenerator.getExtension(handlerClass);
    File file = File.createTempFile("map", "." + extension);

    imageGenerator.generate(handlerClass, params, file.getAbsolutePath());

    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("map." + extension);
    entry.setFileContent(IOUtils.toByteArray(new FileInputStream(file)));
    entry.setLength(entry.getFileContent().length);
    file.delete();
    return entry;

  }

  private Path2D stringToPolygon(String polygonString, Model colorModel) {
    String[] stringPointArray = polygonString.split(";");

    List<Point2D> points = new ArrayList<>();
    for (String string : stringPointArray) {
      if (!string.trim().equals("")) {
        double x = Double.valueOf(string.split(",")[0]);
        double y = Double.valueOf(string.split(",")[1]);
        points.add(new Point2D.Double(x, y));
      }
    }

    if (points.size() <= 2) {
      points.clear();
      points.add(new Point2D.Double(0, 0));
      points.add(new Point2D.Double(colorModel.getWidth(), 0));
      points.add(new Point2D.Double(colorModel.getWidth(), colorModel.getHeight()));
      points.add(new Point2D.Double(0, colorModel.getHeight()));
    }

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
      Point2D point = points.get(i);
      polygon.lineTo(point.getX(), point.getY());
    }
    polygon.closePath();
    return polygon;
  }

  private List<Integer> deserializeIdList(String overlayIds) {
    List<Integer> result = new ArrayList<>();
    String[] tmp = overlayIds.split(",");
    for (String string : tmp) {
      if (!string.equals("")) {
        result.add(Integer.valueOf(string));
      }
    }
    return result;
  }

}
