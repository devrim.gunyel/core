package lcsb.mapviewer.api.projects.models.bioEntities;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.ISearchService;

@Transactional
@Service
public class BioEntitiesRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(BioEntitiesRestImpl.class);

  private ISearchService searchService;

  @Autowired
  public BioEntitiesRestImpl(ISearchService searchService) {
    this.searchService = searchService;
  }

  public List<Map<String, Object>> getClosestElementsByCoordinates(
      String projectId, String modelId, Point2D coordinates, Integer count, String perfectMatch, String type)
      throws ObjectNotFoundException {
    List<Map<String, Object>> resultMap = new ArrayList<>();

    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Model submodel = model.getSubmodelById(modelId);

    if (submodel == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    Set<String> types = new LinkedHashSet<>();
    if (!type.isEmpty()) {
      for (String str : type.split(",")) {
        types.add(str.toLowerCase());
      }
    }

    List<BioEntity> elements = searchService.getClosestElements(submodel, coordinates, count,
        perfectMatch.equalsIgnoreCase("true"), types);
    for (BioEntity object : elements) {
      Map<String, Object> result = createMinifiedSearchResult(object);
      resultMap.add(result);
    }
    return resultMap;
  }

  public List<Map<String, Object>> getElementsByQuery(String projectId, String query, String modelId,
      Integer maxElements, String perfectMatch) throws ObjectNotFoundException {
    List<Map<String, Object>> resultMap = new ArrayList<>();

    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    if (!modelId.equals("*")) {
      model = model.getSubmodelById(modelId);
    }
    if (model == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    boolean match = perfectMatch.equals("true");
    List<BioEntity> elements = searchService.searchByQuery(model, query, maxElements, match);
    for (BioEntity object : elements) {
      Map<String, Object> result = createMinifiedSearchResult(object);
      resultMap.add(result);
    }
    return resultMap;
  }

  public String[] getSuggestedQueryList(String projectId) throws ObjectNotFoundException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model==null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return searchService.getSuggestedQueryList(model);
  }

}
