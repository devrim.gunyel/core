package lcsb.mapviewer.api.projects.drugs;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.drug.IDrugService;

@Transactional
@Service
public class DrugRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(DrugRestImpl.class);

  private IDrugService drugService;

  private IUserService userService;

  @Autowired
  public DrugRestImpl(IDrugService drugService, IUserService userService) {
    this.drugService = drugService;
    this.userService = userService;
  }

  public List<Map<String, Object>> getDrugsByQuery(String projectId, String columns, String query)
      throws QueryException, ObjectNotFoundException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);

    Set<String> columnSet = createDrugColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    Drug drug = drugService.getByName(query, new DbSearchCriteria().project(project).organisms(organism).colorSet(0));
    if (drug != null) {
      List<Model> models = getModels(projectId, "*");
      result.add(prepareDrug(drug, columnSet, models));
    }
    return result;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  private Map<String, Object> prepareDrug(Drug drug, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = drug.getName();
      } else if (column.equals("name")) {
        value = drug.getName();
      } else if (column.equals("references")) {
        value = createAnnotations(drug.getSources());
      } else if (column.equals("description")) {
        value = drug.getDescription();
      } else if (column.equals("bloodbrainbarrier")) {
        value = drug.getBloodBrainBarrier();
      } else if (column.equals("brandnames")) {
        value = drug.getBrandNames();
      } else if (column.equals("synonyms")) {
        value = drug.getSynonyms();
      } else if (column.equals("targets")) {
        value = prepareTargets(drug.getTargets(), models);
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private Set<String> createDrugColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("references");
      columnsSet.add("description");
      columnsSet.add("bloodBrainBarrier");
      columnsSet.add("brandNames");
      columnsSet.add("synonyms");
      columnsSet.add("id");
      columnsSet.add("targets");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

  public List<Map<String, Object>> getDrugsByTarget(String projectId, String targetType, String targetId,
      String columns) throws QueryException, ObjectNotFoundException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Project project = getProjectService().getProjectByProjectId(projectId);

    List<Model> models = getModels(projectId, "*");

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = getTargets(targetType, models, dbId);
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createDrugColumnSet(columns);

    List<Drug> drugs = drugService.getForTargets(targets, new DbSearchCriteria().project(project).organisms(organism));

    List<Map<String, Object>> result = new ArrayList<>();

    for (Drug drug : drugs) {
      result.add(prepareDrug(drug, columnSet, models));
    }

    return result;
  }

  private List<Element> getTargets(String targetType, List<Model> models, Integer dbId) throws QueryException {
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      Element element = null;
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    return targets;
  }

  public List<String> getSuggestedQueryList(String projectId) throws DrugSearchException, ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return drugService.getSuggestedQueryList(project, project.getOrganism());
  }

}
