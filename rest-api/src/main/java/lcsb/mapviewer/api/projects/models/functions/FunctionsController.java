package lcsb.mapviewer.api.projects.models.functions;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;

@RestController
@RequestMapping(value = "/projects/{projectId}/models/{modelId}/functions", produces = MediaType.APPLICATION_JSON_VALUE)
public class FunctionsController extends BaseController {

  private FunctionsRestImpl functionController;

  @Autowired
  public FunctionsController(FunctionsRestImpl functionController) {
    this.functionController = functionController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{functionId}")
  public Map<String, Object> getFunction(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId,
      @PathVariable(value = "functionId") String functionId) throws QueryException {
    return functionController.getFunction(projectId, modelId, functionId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getFunctions(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "modelId") String modelId) throws QueryException {
    return functionController.getFunctions(projectId, modelId);
  }

}