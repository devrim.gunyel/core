package lcsb.mapviewer.api.projects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController extends BaseController {

  private ServletContext context;
  private ProjectRestImpl projectController;
  private IUserService userService;

  @Autowired
  public ProjectController(ServletContext context, ProjectRestImpl projectController, IUserService userService) {
    this.context = context;
    this.projectController = projectController;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId:.+}")
  public Map<String, Object> getProject(@PathVariable(value = "projectId") String projectId)
      throws ObjectNotFoundException {
    return projectController.getProject(projectId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PatchMapping(value = "/{projectId:.+}")
  public Map<String, Object> updateProject(
      @RequestBody String body,
      @PathVariable(value = "projectId") String projectId) throws IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "project");
    return projectController.updateProject(projectId, data);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PatchMapping(value = "/{projectId:.+}:grantPrivileges")
  public Map<String, Object> grantPrivileges(
      @RequestBody String body,
      @PathVariable(value = "projectId") String projectId) throws IOException, QueryException {
    Map[] data = extractListBody(body);
    return projectController.grantPrivilegesProject(projectId, data);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PatchMapping(value = "/{projectId:.+}:revokePrivileges")
  public Map<String, Object> revokePrivileges(
      @RequestBody String body,
      @PathVariable(value = "projectId") String projectId) throws IOException, QueryException {
    Map[] data = extractListBody(body);
    return projectController.revokePrivilegesProject(projectId, data);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @PostMapping(value = "/{projectId:.+}")
  public Map<String, Object> addProject(
      Authentication authentication,
      @RequestBody MultiValueMap<String, Object> formData,
      @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, SecurityException {
    if (projectId.equals("*")) {
      throw new QueryException("No.");
    }
    User user = userService.getUserByLogin(authentication.getName());
    Map<String, Object> project = projectController.addProject(projectId, formData, context.getRealPath("/"), user);

    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, projectId);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, projectId);
    userService.grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType.READ_PROJECT, projectId);
    userService.grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType.WRITE_PROJECT, projectId);

    return project;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{projectId:.+}")
  public Map<String, Object> removeProject(@PathVariable(value = "projectId") String projectId)
      throws QueryException {
    Map<String, Object> response = projectController.removeProject(projectId, context.getRealPath("/"));

    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.WRITE_PROJECT, projectId);
    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.READ_PROJECT, projectId);

    return response;
  }

  @PostFilter("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + filterObject['projectId'])")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getProjects() {
    return projectController.getProjects();
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/statistics")
  public Object getStatistics(@PathVariable(value = "projectId") String projectId) throws QueryException {
    return projectController.getStatistics(projectId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}:downloadSource")
  public ResponseEntity<byte[]> getProjectSource(@PathVariable(value = "projectId") String projectId)
      throws QueryException {
    FileEntry file = projectController.getSource(projectId);
    if (file == null) {
      throw new ObjectNotFoundException("File doesn't exist");
    }
    MediaType type = MediaType.TEXT_PLAIN;
    if (file.getOriginalFileName().endsWith("xml")) {
      type = MediaType.APPLICATION_XML;
    } else if (file.getOriginalFileName().endsWith("zip")) {
      type = MediaType.APPLICATION_OCTET_STREAM;
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/logs/")
  public Map<String, Object> getLogs(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "level", defaultValue = "*") String level,
      @RequestParam(value = "start", defaultValue = "0") String start,
      @RequestParam(value = "length", defaultValue = "10") Integer length,
      @RequestParam(value = "sortColumn", defaultValue = "id") String sortColumn,
      @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
      @RequestParam(value = "search", defaultValue = "") String search) throws QueryException {
    return projectController.getLogs(projectId, level, start, length, sortColumn, sortOrder, search);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/submapConnections")
  public List<Map<String, Object>> getSubmapConnections(@PathVariable(value = "projectId") String projectId)
      throws QueryException {
    return projectController.getSubmapConnections(projectId);
  }

  public ServletContext getContext() {
    return context;
  }

  public void setContext(ServletContext context) {
    this.context = context;
  }
}