package lcsb.mapviewer.api.projects.comments;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/projects/{projectId}/comments", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController extends BaseController {

  Logger logger = LogManager.getLogger();

  private CommentRestImpl commentController;

  private IUserService userService;

  @Autowired
  public CommentController(CommentRestImpl commentController, IUserService userService) {
    this.commentController = commentController;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostFilter("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or filterObject['owner'] == authentication.name" +
      " or filterObject['isPinned']")
  @GetMapping(value = "/models/{modelId}/")
  public List<Map<String, Object>> getComments(
      Authentication authentication,
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "removed", defaultValue = "") String removed) throws QueryException {
    boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.name()));
    boolean isProjectCurator = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_CURATOR.name()))
        && authentication.getAuthorities()
            .contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT.name() + ":" + projectId));

    List<Map<String, Object>> comments = commentController.getCommentList(
        projectId, columns, "", "", removed);
    if (!isAdmin && !isProjectCurator) {
      comments.forEach(comment -> {
        comment.remove("author");
        comment.remove("email");
        comment.remove("removeReason");
      });
    }
    return comments;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)" +
      " or @commentService.getOwnerByCommentId(#commentId)?.login == authentication.name")
  @DeleteMapping(value = "/{commentId}/")
  public Map<String, Object> removeComment(
      @RequestBody(required = false) String body,
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "commentId") String commentId) throws QueryException, IOException {
    Map<String, Object> node = parseBody(body);
    String reason = (String) node.get("reason");
    return commentController.removeComment(projectId, commentId, reason);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostFilter("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or filterObject['owner'] == authentication.name" +
      " or filterObject['isPinned']")
  @GetMapping(value = "/models/{modelId}/bioEntities/reactions/{reactionId}")
  public List<Map<String, Object>> getCommentsByReaction(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @PathVariable(value = "reactionId") String reactionId,
      @RequestParam(value = "removed", defaultValue = "") String removed) throws QueryException {
    return commentController.getCommentList(projectId, columns, reactionId,
        ElementIdentifierType.REACTION.getJsName(), removed);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostFilter("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or filterObject['owner'] == authentication.name" +
      " or filterObject['isPinned']")
  @GetMapping(value = "/models/{modelId}/bioEntities/elements/{elementId}")
  public List<Map<String, Object>> getCommentsByElement(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @PathVariable(value = "elementId") String elementId,
      @RequestParam(value = "removed", defaultValue = "") String removed) throws QueryException {
    return commentController.getCommentList(
        projectId, columns, elementId, ElementIdentifierType.ALIAS.getJsName(), removed);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostFilter("hasAuthority('IS_ADMIN')" +
      " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)" +
      " or filterObject['owner'] == authentication.name" +
      " or filterObject['isPinned']")
  @GetMapping(value = "/models/{modelId}/points/{coordinates:.+}")
  public List<Map<String, Object>> getCommentsByPoint(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @PathVariable(value = "coordinates") String coordinates,
      @RequestParam(value = "removed", defaultValue = "") String removed) throws QueryException {
    return commentController.getCommentList(projectId, columns, coordinates,
        ElementIdentifierType.POINT.getJsName(), removed);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/bioEntities/elements/{elementId}")
  public Map<String, Object> addCommentForElement(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "elementId") String elementId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "content") String content,
      @RequestParam(value = "pinned", defaultValue = "true") String pinned,
      @RequestParam(value = "coordinates") String coordinates,
      @PathVariable(value = "modelId") String modelId, Authentication authentication) throws QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);

    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    return commentController.addComment(projectId, ElementIdentifierType.ALIAS.getJsName(), elementId, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId, user);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/bioEntities/reactions/{reactionId}")
  public Map<String, Object> addCommentForReaction(
      @PathVariable(value = "projectId") String projectId,
      @PathVariable(value = "reactionId") String reactionId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "content") String content,
      @RequestParam(value = "pinned", defaultValue = "true") String pinned,
      @RequestParam(value = "coordinates") String coordinates,
      @PathVariable(value = "modelId") String modelId, Authentication authentication) throws QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);
    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    return commentController.addComment(projectId, ElementIdentifierType.REACTION.getJsName(), reactionId, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId, user);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/points/{coordinates}")
  public Map<String, Object> addCommentForPoint(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "email") String email,
      @RequestParam(value = "content") String content,
      @RequestParam(value = "pinned", defaultValue = "true") String pinned,
      @PathVariable(value = "coordinates") String coordinates,
      @PathVariable(value = "modelId") String modelId, Authentication authentication) throws QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);
    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    return commentController.addComment(projectId, ElementIdentifierType.POINT.getJsName(), coordinates, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId, user);
  }

  private Point2D parseCoordinates(String coordinates) throws QueryException {
    String[] tmp = coordinates.split(",");
    if (tmp.length != 2) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
    }
    Double x = null;
    Double y = null;
    try {
      x = Double.valueOf(tmp[0]);
      y = Double.valueOf(tmp[1]);
    } catch (NumberFormatException e) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
    }
    Point2D pointCoordinates = new Point2D.Double(x, y);
    return pointCoordinates;
  }
}