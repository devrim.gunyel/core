package lcsb.mapviewer.api.projects.comments;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.ReactionDao;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.services.interfaces.ICommentService;

@Transactional
@Service
public class CommentRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(CommentRestImpl.class);

  private ICommentService commentService;

  private ReactionDao reactionDao;

  private ElementDao elementDao;

  private PointTransformation pt = new PointTransformation();

  @Autowired
  public CommentRestImpl(ICommentService commentService, ReactionDao reactionDao, ElementDao elementDao) {
    this.commentService = commentService;
    this.reactionDao = reactionDao;
    this.elementDao = elementDao;
  }

  public List<Map<String, Object>> getCommentList(
      String projectId, String columns, String elementId,
      String elementType, String removed) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Set<String> columnsSet = createCommentColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    List<Comment> comments = commentService.getCommentsByProject(project);
    for (Comment comment : comments) {
      boolean reject = false;
      if (!"".equals(elementType)) {
        if (elementType.equals(ElementIdentifierType.POINT.getJsName())) {
          reject = comment.getTableName() != null;
        } else if (elementType.equals(ElementIdentifierType.REACTION.getJsName())) {
          reject = comment.getTableName() == null || !comment.getTableName().getName().contains("Reaction");
        } else if (elementType.equals(ElementIdentifierType.ALIAS.getJsName())) {
          reject = comment.getTableName() == null || comment.getTableName().getName().contains("Reaction");
        } else {
          throw new QueryException("Unknown element type: " + elementType);
        }
      }
      if (!"".equals(elementId)) {
        Object id = getId(comment).toString();
        reject |= (!elementId.equals(id));
      }
      if (!"".equals(removed)) {
        boolean expectedRemoved = removed.equalsIgnoreCase("true");
        reject |= (comment.isDeleted() != expectedRemoved);
      }
      if (!reject) {
        result.add(preparedComment(comment, columnsSet));
      }
    }

    return result;
  }

  private Map<String, Object> preparedComment(Comment comment, Set<String> columnsSet) {
    Map<String, Object> result = new TreeMap<>();
    result.put("isPinned", comment.isPinned());
    if (comment.getUser() != null) {
      result.put("owner", comment.getUser().getLogin());
    } else {
      result.put("owner", null);
    }
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value;
      switch (column) {
      case "id":
      case "idobject":
        value = comment.getId();
        break;
      case "elementid":
        value = getId(comment);
        break;
      case "modelid":
        value = comment.getModelData().getId();
        break;
      case "title":
        value = getTitle(comment);
        break;
      case "pinned":
        value = comment.isPinned();
        break;
      case "content":
        value = comment.getContent();
        break;
      case "removed":
        value = comment.isDeleted();
        break;
      case "coord":
        value = getCoordinates(comment);
        break;
      case "removereason":
        value = comment.getRemoveReason();
        break;
      case "type":
        value = getType(comment);
        break;
      case "icon":
        value = "icons/comment.png?v=" + Configuration.getSystemBuildVersion(null);
        break;
      case "author":
        value = comment.getName();
        break;
      case "email":
        value = comment.getEmail();
        break;
      default:
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private Object getId(Comment comment) {
    if (comment.getTableName() == null) {
      return String.format("%.2f", comment.getCoordinates().getX()) + ","
          + String.format("%.2f", comment.getCoordinates().getY());
    } else {
      return comment.getTableId();
    }
  }

  private Object getType(Comment comment) {
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        return ElementIdentifierType.REACTION;
      } else {
        return ElementIdentifierType.ALIAS;
      }
    } else {
      return ElementIdentifierType.POINT;
    }
  }

  private String getTitle(Comment comment) {
    String title = "";
    if (comment.getCoordinates() != null) {
      title = "Comment (coord: " + String.format("%.2f", comment.getCoordinates().getX()) + ", "
          + String.format("%.2f", comment.getCoordinates().getY()) + ")";
    }
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        Reaction reaction = reactionDao.getById(comment.getTableId());
        if (reaction != null) {
          title = "Reaction " + reaction.getIdReaction();
        } else {
          logger.warn("Invalid reaction dbID: " + comment.getTableId());
        }

      } else {
        Element alias = elementDao.getById(comment.getTableId());
        if (alias != null) {
          title = alias.getName();
        } else {
          logger.warn("Invalid alias dbID: " + comment.getTableId());
        }
      }
    }
    return title;
  }

  private Point2D getCoordinates(Comment comment) {
    Point2D coordinates = comment.getCoordinates();
    if (comment.getCoordinates() != null) {
      coordinates = comment.getCoordinates();
    }
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        Reaction reaction = reactionDao.getById(comment.getTableId());
        if (reaction != null) {
          Line2D centerLine = reaction.getLine().getLines().get(reaction.getLine().getLines().size() / 2);
          coordinates = pt.getPointOnLine(centerLine.getP1(), centerLine.getP2(), 0.5);
        } else {
          logger.warn("Invalid reaction dbID: " + comment.getTableId());
        }

      } else {
        Element alias = elementDao.getById(comment.getTableId());
        if (alias != null) {
          coordinates = alias.getCenter();
        } else {
          logger.warn("Invalid alias dbID: " + comment.getTableId());
        }
      }
    }
    return coordinates;
  }

  private Set<String> createCommentColumnSet(String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("title");
      columnsSet.add("icon");
      columnsSet.add("type");
      columnsSet.add("content");
      columnsSet.add("removed");
      columnsSet.add("coord");
      columnsSet.add("modelId");
      columnsSet.add("elementId");
      columnsSet.add("id");
      columnsSet.add("pinned");
      columnsSet.add("author");
      columnsSet.add("email");
      columnsSet.add("removeReason");
    } else {
      Collections.addAll(columnsSet, columns.split(","));
    }
    return columnsSet;
  }

  public Map<String, Object> addComment(String projectId, String elementType, String elementId,
      String name, String email, String content, boolean pinned, Point2D pointCoordinates, String submodelId, User owner)
      throws QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Integer modelId;
    try {
      modelId = Integer.valueOf(submodelId);
    } catch (NumberFormatException e) {
      throw new QueryException("Invalid model identifier for given project", e);
    }
    Model submodel = model.getSubmodelById(modelId);
    if (submodel == null) {
      throw new QueryException("Invalid model identifier for given project");
    }
    Object commentedObject;
    if (elementType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      commentedObject = submodel.getElementByDbId(Integer.valueOf(elementId));
      if (commentedObject == null) {
        throw new QueryException("Invalid commented element identifier for given project");
      }
    } else if (elementType.equals(ElementIdentifierType.REACTION.getJsName())) {
      commentedObject = submodel.getReactionByDbId(Integer.valueOf(elementId));
      if (commentedObject == null) {
        throw new QueryException("Invalid commented element identifier for given project");
      }
    } else if (elementType.equals(ElementIdentifierType.POINT.getJsName())) {
      commentedObject = null;
    } else {
      throw new QueryException("Unknown type of commented object: " + elementType);
    }

    Comment comment = commentService.addComment(name, email, content, pointCoordinates, commentedObject, pinned,
        submodel, owner);

    return preparedComment(comment, createCommentColumnSet(""));
  }

  public Map<String, Object> removeComment(String projectId, String commentId, String reason) throws QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Comment comment = commentService.getCommentById(commentId);
    if (comment == null || comment.getModelData().getProject().getId() != project.getId()) {
      throw new ObjectNotFoundException("Comment with given id doesn't exist");
    }

    commentService.deleteComment(comment, reason);
    return okStatus();
  }

}
