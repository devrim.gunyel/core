package lcsb.mapviewer.api.projects.mirnas;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.MiRNASearchException;
import lcsb.mapviewer.api.*;

@RestController
@RequestMapping(value = "/projects/{projectId}/", produces = MediaType.APPLICATION_JSON_VALUE)
public class MiRnaController extends BaseController {

  private MiRnaRestImpl miRnaController;

  @Autowired
  public MiRnaController(MiRnaRestImpl miRnaController) {
    this.miRnaController = miRnaController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "miRnas:search")
  public List<Map<String, Object>> getMiRnasByQuery(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "query", defaultValue = "") String query,
      @RequestParam(value = "target", defaultValue = "") String target) throws QueryException {
    if (!query.equals("")) {
      return miRnaController.getMiRnasByQuery(projectId, columns, query);
    } else if (target.contains(":")) {
      String targetType = target.split(":", -1)[0];
      String targetId = target.split(":", -1)[1];
      return miRnaController.getMiRnasByTarget(projectId, targetType, targetId, columns);
    } else {
      return new ArrayList<>();
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "miRnas/suggestedQueryList")
  public List<String> getSuggestedQueryList(@PathVariable(value = "projectId") String projectId)
      throws MiRNASearchException, ObjectNotFoundException {
    return miRnaController.getSuggestedQueryList(projectId);
  }

}