package lcsb.mapviewer.api.projects.chemicals;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.ChemicalSearchException;
import lcsb.mapviewer.api.*;

@RestController
@RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChemicalController extends BaseController {

  private ChemicalRestImpl chemicalController;

  @Autowired
  public ChemicalController(ChemicalRestImpl chemicalController) {
    this.chemicalController = chemicalController;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/chemicals:search")
  public List<Map<String, Object>> getChemicalsByQuery(
      @PathVariable(value = "projectId") String projectId,
      @RequestParam(value = "columns", defaultValue = "") String columns,
      @RequestParam(value = "query", defaultValue = "") String query,
      @RequestParam(value = "target", defaultValue = "") String target) throws QueryException {
    if (!query.equals("")) {
      return chemicalController.getChemicalsByQuery(projectId, columns, query);
    } else if (target.contains(":")) {
      String targetType = target.split(":", -1)[0];
      String targetId = target.split(":", -1)[1];
      return chemicalController.getChemicalsByTarget(projectId, targetType, targetId, columns);
    } else {
      return new ArrayList<>();
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/chemicals/suggestedQueryList")
  public List<String> getSuggestedQueryList(@PathVariable(value = "projectId") String projectId)
      throws ChemicalSearchException, ObjectNotFoundException {
    return chemicalController.getSuggestedQueryList(projectId);
  }
}