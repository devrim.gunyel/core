package lcsb.mapviewer.api.minervanet;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RestController
@RequestMapping("/minervanet")
public class MinervaNetController extends BaseController {

  private Logger logger = LogManager.getLogger(MinervaNetController.class);

  private ServletContext context;
  private IConfigurationService configurationService;

  @Autowired
  public MinervaNetController(ServletContext context, IConfigurationService configurationService) {
    this.context = context;
    this.configurationService = configurationService;
  }

  @PostMapping(value = "/submitError")
  public void submitError(@RequestBody ErrorReport report) throws IOException {
    String version = configurationService.getSystemSvnVersion(context.getRealPath("/"));
    report.setVersion(version);

    ObjectMapper mapper = new ObjectMapper();
    String jsonReport = mapper.writeValueAsString(report);

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();

    try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
      StringEntity requestEntity = new StringEntity(jsonReport, ContentType.APPLICATION_JSON);
      HttpPost post = new HttpPost(server);
      post.setEntity(requestEntity);
      try (CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200 || !responseBodyValid(responseBody)) {
          String error = "Could not submit report to MinervaNet. Reason: " + responseBody;
          logger.error(error);
          throw new ReportSubmissionException(error);
        }
      }
    }
  }

  private boolean responseBodyValid(String body) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
    try {
      mapper.readTree(body);
    } catch (IOException e) {
      return false;
    }
    return true;
  }

}
