package lcsb.mapviewer.api.minervanet;

public class ReportSubmissionException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ReportSubmissionException() {
    super();
  }

  public ReportSubmissionException(String message) {
    super(message);
  }

  public ReportSubmissionException(String message, Throwable cause) {
    super(message, cause);
  }

  public ReportSubmissionException(Throwable cause) {
    super(cause);
  }

  protected ReportSubmissionException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
