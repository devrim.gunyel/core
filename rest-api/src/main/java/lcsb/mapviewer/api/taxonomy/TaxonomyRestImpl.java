package lcsb.mapviewer.api.taxonomy;

import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@Transactional
@Service
public class TaxonomyRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(TaxonomyRestImpl.class);

  private TaxonomyBackend taxonomyParser;

  @Autowired
  public TaxonomyRestImpl(TaxonomyBackend taxonomyParser) {
    this.taxonomyParser = taxonomyParser;
  }

  public Map<String, Object> getTaxonomy(String id) throws TaxonomySearchException, ObjectNotFoundException {
    String name = taxonomyParser.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, id));
    if (name == null) {
      throw new ObjectNotFoundException("Object not found: " + id);
    }
    Map<String, Object> result = new TreeMap<>();
    result.put("name", name);
    result.put("id", id);
    return result;
  }
}
