package lcsb.mapviewer.api.taxonomy;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/taxonomy", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaxonomyController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(TaxonomyController.class);

  private TaxonomyRestImpl taxonomyRest;

  @Autowired
  public TaxonomyController(TaxonomyRestImpl taxonomyRest) {
    this.taxonomyRest = taxonomyRest;
  }

  @GetMapping(value = "/{id:.+}")
  public Map<String, Object> getTaxonomy(@PathVariable(value = "id") String id)
      throws TaxonomySearchException, ObjectNotFoundException {
    return taxonomyRest.getTaxonomy(id);
  }
}