package lcsb.mapviewer.api.plugins;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/plugins", produces = MediaType.APPLICATION_JSON_VALUE)
public class PluginController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(PluginController.class);

  private PluginRestImpl pluginRest;
  private IUserService userService;

  @Autowired
  public PluginController(PluginRestImpl pluginRest, IUserService userService) {
    this.pluginRest = pluginRest;
    this.userService = userService;
  }

  @PreAuthorize("(not #isPublic and #isDefault!=true) or hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/")
  public Map<String, Object> createPlugin(
      @RequestParam(value = "hash") String hash,
      @RequestParam(value = "name") String name,
      @RequestParam(value = "version") String version,
      @RequestParam(value = "isPublic", defaultValue = "false") boolean isPublic,
      @RequestParam(value = "isDefault", required = false) Boolean isDefault,
      @RequestParam(value = "url", defaultValue = "") String url) {
    return pluginRest.createPlugin(hash, name, version, url, isPublic, isDefault);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/{hash}")
  public Map<String, Object> updatePlugin(
      @PathVariable(value = "hash") String hash,
      @RequestBody String body) throws QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "plugin");
    return pluginRest.updatePlugin(hash, data);
  }

  @GetMapping(value = "/")
  public List<Map<String, Object>> getPlugins(
      @RequestParam(value = "onlyPublic", defaultValue = "false") String onlyPublic) {
    return pluginRest.getPlugins(onlyPublic.equalsIgnoreCase("true"));
  }

  @GetMapping(value = "/{hash}")
  public Map<String, Object> getPlugin(@PathVariable(value = "hash") String hash) throws ObjectNotFoundException {
    return pluginRest.getPlugin(hash);
  }

  // FIXME: check if this takes priority over filter chain
  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{hash}")
  public Map<String, Object> removePlugin(@PathVariable(value = "hash") String hash) throws ObjectNotFoundException {
    return pluginRest.removePlugin(hash);
  }

  @PostMapping(value = "/{hash}/data/users/{key}")
  public Map<String, Object> createPluginDataEntry(
      Authentication authentication,
      @PathVariable(value = "hash") String hash,
      @PathVariable(value = "key") String key,
      @RequestParam(value = "value", defaultValue = "") String value) throws ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());
    return pluginRest.createPluginDataEntry(hash, user, key, value);
  }

  @PostMapping(value = "/{hash}/data/global/{key}")
  public Map<String, Object> createPluginDataEntry(
      @PathVariable(value = "hash") String hash,
      @PathVariable(value = "key") String key,
      @RequestParam(value = "value", defaultValue = "") String value) throws ObjectNotFoundException {
    return pluginRest.createPluginDataEntry(hash, null, key, value);
  }

  @GetMapping(value = "/{hash}/data/users/{key}")
  public Map<String, Object> getPluginDataEntry(
      Authentication authentication,
      @PathVariable(value = "hash") String hash,
      @PathVariable(value = "key") String key) throws ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());
    return pluginRest.getPluginDataEntry(hash, user, key);
  }

  @GetMapping(value = "/{hash}/data/global/{key}")
  public Map<String, Object> getPluginDataEntry(
      @PathVariable(value = "hash") String hash,
      @PathVariable(value = "key") String key) throws ObjectNotFoundException {
    return pluginRest.getPluginDataEntry(hash, null, key);
  }

}