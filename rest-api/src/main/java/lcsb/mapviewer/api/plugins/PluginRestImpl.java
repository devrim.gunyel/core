package lcsb.mapviewer.api.plugins;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.plugin.PluginDao;
import lcsb.mapviewer.persist.dao.plugin.PluginDataEntryDao;

@Transactional
@Service
public class PluginRestImpl extends BaseRestImpl {

  Logger logger = LogManager.getLogger();

  private PluginDao pluginDao;

  private PluginDataEntryDao pluginDataEntryDao;

  @Autowired
  public PluginRestImpl(PluginDao pluginDao, PluginDataEntryDao pluginDataEntryDao) {
    this.pluginDao = pluginDao;
    this.pluginDataEntryDao = pluginDataEntryDao;
  }

  public Map<String, Object> createPlugin(String hash, String name, String version, String url, boolean isPublic,
      Boolean isDefault) {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin != null) {
      plugin.getUrls().add(url);
      plugin.setPublic(plugin.isPublic() || isPublic);
      if (isDefault != null) {
        plugin.setDefault(isDefault);
      }
      pluginDao.update(plugin);
    } else {
      plugin = new Plugin();
      plugin.setHash(hash);
      plugin.setName(name);
      plugin.setVersion(version);
      plugin.setPublic(isPublic);
      if (isDefault != null) {
        plugin.setDefault(isDefault);
      }
      if (!url.isEmpty()) {
        plugin.getUrls().add(url);
      }
      pluginDao.add(plugin);
    }
    return pluginToMap(plugin);
  }

  private Map<String, Object> pluginToMap(Plugin plugin) {
    Map<String, Object> result = new TreeMap<>();
    result.put("hash", plugin.getHash());
    result.put("name", plugin.getName());
    result.put("version", plugin.getVersion());
    result.put("isPublic", plugin.isPublic());
    result.put("isDefault", plugin.isDefault());
    List<String> urls = new ArrayList<>(plugin.getUrls());
    Collections.sort(urls);
    result.put("urls", urls);
    return result;
  }

  public Map<String, Object> getPlugin(String hash) throws ObjectNotFoundException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    return pluginToMap(plugin);
  }

  public Map<String, Object> createPluginDataEntry(String hash, User user, String key, String value)
      throws ObjectNotFoundException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    PluginDataEntry entry = pluginDataEntryDao.getByKey(plugin, key, user);
    if (entry == null) {
      entry = new PluginDataEntry();
      entry.setKey(key);
      entry.setPlugin(plugin);
      entry.setUser(user);
      entry.setValue(value);
      pluginDataEntryDao.add(entry);
    } else {
      entry.setValue(value);
      pluginDataEntryDao.update(entry);
    }

    return pluginEntryToMap(entry);
  }

  private Map<String, Object> pluginEntryToMap(PluginDataEntry entry) {
    Map<String, Object> result = new TreeMap<>();
    result.put("key", entry.getKey());
    result.put("value", entry.getValue());
    if (entry.getUser() != null) {
      result.put("user", entry.getUser().getLogin());
    }
    return result;
  }

  public Map<String, Object> getPluginDataEntry(String hash, User user, String key) throws ObjectNotFoundException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    PluginDataEntry entry = pluginDataEntryDao.getByKey(plugin, key, user);
    if (entry == null) {
      throw new ObjectNotFoundException("Entry doesn't exist");
    }

    return pluginEntryToMap(entry);
  }

  public List<Map<String, Object>> getPlugins(boolean onlyPublic) {
    List<Plugin> plugins = pluginDao.getAll();
    plugins.sort(Plugin.ID_COMPARATOR);

    List<Map<String, Object>> result = new ArrayList<>();
    for (Plugin plugin : plugins) {
      if (!onlyPublic || plugin.isPublic()) {
        result.add(pluginToMap(plugin));
      }
    }
    return result;
  }

  public Map<String, Object> removePlugin(String hash) throws ObjectNotFoundException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin != null) {
      plugin.setPublic(false);
      plugin.setDefault(false);
      pluginDao.update(plugin);
    } else {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    return getPlugin(hash);
  }

  public Map<String, Object> updatePlugin(String hash, Map<String, Object> data) throws QueryException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    if (data == null) {
      throw new QueryException("plugin data is not defined");
    }

    Set<String> fields = data.keySet();
    for (String fieldName : fields) {
      Object value = data.get(fieldName);
      String stringValue = null;
      Integer intValue = null;
      Boolean boolValue = null;
      if (value instanceof String) {
        stringValue = (String) value;
      }
      if (value instanceof Integer) {
        intValue = (Integer) value;
      }
      if (value instanceof Boolean) {
        boolValue = (Boolean) value;
      }
      if (fieldName.equalsIgnoreCase("isPublic")) {
        plugin.setPublic(boolValue);
      } else if (fieldName.equalsIgnoreCase("isDefault")) {
        plugin.setDefault(boolValue);
      } else if (fieldName.equalsIgnoreCase("name")) {
        plugin.setName(stringValue);
      } else if (fieldName.equalsIgnoreCase("version")) {
        plugin.setVersion(stringValue);
      } else if (fieldName.equalsIgnoreCase("hash")) {
        if (!stringValue.equals(hash)) {
          throw new QueryException("plugin hash cannot be changed");
        }
      } else if (fieldName.equalsIgnoreCase("id")) {
        if (intValue != 0 && intValue != plugin.getId()) {
          throw new QueryException("plugin id cannot be changed");
        }
      } else if (fieldName.equalsIgnoreCase("urls")) {
        if (value instanceof List) {
          if (((List) value).size() > 0) {
            plugin.getUrls().clear();
            for (Object string : (List) value) {
              plugin.getUrls().add(string.toString());
            }
          }
        }
      } else {
        throw new QueryException("Unknown field: " + fieldName);
      }
    }

    pluginDao.update(plugin);
    return getPlugin(hash);
  }
}
