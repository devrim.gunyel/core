package lcsb.mapviewer.api;

/**
 * Thrown when object cannot be found via API.
 * 
 * @author Piotr Gawron
 *
 */
public class ObjectNotFoundException extends QueryException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public ObjectNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor with error message and parent exception.
   * 
   * @param message
   *          error message
   * @param reason
   *          parent exception that caused this one
   */
  public ObjectNotFoundException(String message, Exception reason) {
    super(message, reason);
  }

}
