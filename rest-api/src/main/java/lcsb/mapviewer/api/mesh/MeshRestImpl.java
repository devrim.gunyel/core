package lcsb.mapviewer.api.mesh;

import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@Transactional
@Service
public class MeshRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(MeshRestImpl.class);

  private MeSHParser meSHParser;

  @Autowired
  public MeshRestImpl(MeSHParser meSHParser) {
    this.meSHParser = meSHParser;
  }

  public Map<String, Object> getMesh(String id) throws AnnotatorException, ObjectNotFoundException {

    MeSH mesh = meSHParser.getMeSH(new MiriamData(MiriamType.MESH_2012, id));
    if (mesh == null) {
      throw new ObjectNotFoundException("Object not found: " + id);
    }
    Map<String, Object> result = new TreeMap<>();
    result.put("name", mesh.getName());
    result.put("id", mesh.getMeSHId());
    result.put("description", mesh.getDescription());
    result.put("synonyms", mesh.getSynonyms());
    return result;
  }
}
