package lcsb.mapviewer.api.mesh;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/mesh", produces = MediaType.APPLICATION_JSON_VALUE)
public class MeshController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(MeshController.class);

  private MeshRestImpl meshRest;

  @Autowired
  public MeshController(MeshRestImpl meshRest) {
    this.meshRest = meshRest;
  }

  @GetMapping(value = "/{id:.+}")
  public Map<String, Object> getMesh(@PathVariable(value = "id") String id)
      throws AnnotatorException, ObjectNotFoundException {
    return meshRest.getMesh(id);
  }
}