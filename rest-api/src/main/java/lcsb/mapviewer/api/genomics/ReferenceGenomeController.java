package lcsb.mapviewer.api.genomics;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.api.*;

@RestController
@RequestMapping(value = "/genomics", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReferenceGenomeController extends BaseController {

  private ReferenceGenomeRestImpl referenceGenomeController;

  @Autowired
  public ReferenceGenomeController(ReferenceGenomeRestImpl referenceGenomeController) {
    this.referenceGenomeController = referenceGenomeController;
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}/")
  public Map<String, Object> getGenomesByQuery(
      @PathVariable(value = "organismId") String organism,
      @PathVariable(value = "type") String type,
      @PathVariable(value = "version") String version) throws QueryException {
    return referenceGenomeController.getReferenceGenome(organism, type, version);
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}:getAvailableRemoteUrls")
  public List<Map<String, Object>> getRemoteUrls(
      @PathVariable(value = "organismId") String organism,
      @PathVariable(value = "type") String type,
      @PathVariable(value = "version") String version) throws QueryException {
    return referenceGenomeController.getRemoteUrls(organism, type, version);
  }

  @GetMapping(value = "/taxonomies/")
  public List<Map<String, Object>> getGenomeTaxonomies() throws QueryException {
    return referenceGenomeController.getReferenceGenomeTaxonomies();
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/")
  public Map<String, Object> addGenome(@RequestBody MultiValueMap<String, Object> formData)
      throws QueryException, IOException, ReferenceGenomeConnectorException {
    return referenceGenomeController.addReferenceGenome(formData);
  }

  @GetMapping(value = "/")
  public List<Map<String, Object>> getDownloaded() {
    return referenceGenomeController.getReferenceGenomes();
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/")
  public List<Map<String, Object>> getGenomeTaxonomyTypes(
      @PathVariable(value = "organismId") String organism) throws QueryException {
    return referenceGenomeController.getReferenceGenomeTypes(organism);
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/")
  public List<Map<String, Object>> getGenomeVersion(
      @PathVariable(value = "organismId") String organism,
      @PathVariable(value = "type") String type) throws QueryException {
    return referenceGenomeController.getReferenceGenomeVersions(organism, type);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{genomeId}/")
  public Map<String, Object> removeGenome(@PathVariable(value = "genomeId") String genomeId) throws IOException {
    return referenceGenomeController.removeGenome(genomeId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{genomeId}/geneMapping/{mappingId}/")
  public Map<String, Object> removeGeneMapping(
      @PathVariable(value = "genomeId") String genomeId,
      @PathVariable(value = "mappingId") String mappingId) throws IOException, ObjectNotFoundException {
    return referenceGenomeController.removeGeneMapping(genomeId, mappingId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/{genomeId}/geneMapping/")
  public Map<String, Object> addGeneMapping(
      @PathVariable(value = "genomeId") String genomeId,
      @RequestBody MultiValueMap<String, Object> formData)
      throws QueryException, IOException, ReferenceGenomeConnectorException {
    return referenceGenomeController.addGeneMapping(formData, genomeId);
  }

}