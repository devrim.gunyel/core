package lcsb.mapviewer.api.genomics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.api.*;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import lcsb.mapviewer.services.utils.ReferenceGenomeExistsException;

@Transactional
@Service
public class ReferenceGenomeRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ReferenceGenomeRestImpl.class);

  /**
   * Service that manages reference genomes.
   */
  private IReferenceGenomeService referenceGenomeService;

  private BigFileCache bigFileCache;

  @Autowired
  public ReferenceGenomeRestImpl(IReferenceGenomeService referenceGenomeService, BigFileCache bigFileCache) {
    this.referenceGenomeService = referenceGenomeService;
    this.bigFileCache = bigFileCache;
  }

  public Map<String, Object> getReferenceGenome(String organismId, String type, String version) throws QueryException {
    MiriamData organism;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    try {
      ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
      version = version.replaceAll("\\*", "");
      ReferenceGenome genome = referenceGenomeService.getReferenceGenomeViewByParams(organism, genomeType, version);
      if (genome == null) {
        throw new ObjectNotFoundException("Cannot find requested reference genome");
      }
      return genomeToMap(genome);
    } catch (IllegalArgumentException e) {
      throw new QueryException("Cannot find type: " + type);
    }
  }

  public List<Map<String, Object>> getRemoteUrls(String organismId, String type, String version) throws QueryException {
    MiriamData organism;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    try {
      ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
      String url = referenceGenomeService.getUrlForGenomeVersion(genomeType, organism, version);

      List<Map<String, Object>> result = new ArrayList<>();
      if (url != null) {
        Map<String, Object> row = new TreeMap<>();
        row.put("url", url);
        result.add(row);
      }
      return result;
    } catch (IllegalArgumentException e) {
      throw new QueryException("Cannot find type: " + type);
    }
  }

  private Map<String, Object> genomeToMap(ReferenceGenome genome) {
    Map<String, Object> result = new TreeMap<>();
    result.put("organism", super.createAnnotation(genome.getOrganism()));
    result.put("version", genome.getVersion());
    result.put("type", genome.getType());
    result.put("downloadProgress", genome.getDownloadProgress());
    result.put("sourceUrl", genome.getSourceUrl());
    result.put("localUrl", getLocalUrl(genome.getSourceUrl()));
    result.put("idObject", genome.getId());
    result.put("geneMapping", geneMappingToMaps(genome.getGeneMapping()));

    return result;
  }

  private String getLocalUrl(String sourceUrl) {
    String url = null;
    try {
      url = "../" + bigFileCache.getLocalPathForFile(sourceUrl);
    } catch (FileNotFoundException e) {
      logger.warn("Cannot find local file", e);
    }
    return url;
  }

  private List<Map<String, Object>> geneMappingToMaps(List<ReferenceGenomeGeneMapping> geneMapping) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ReferenceGenomeGeneMapping referenceGenomeGeneMapping : geneMapping) {
      result.add(geneMappingToMap(referenceGenomeGeneMapping));
    }

    return result;
  }

  private Map<String, Object> geneMappingToMap(ReferenceGenomeGeneMapping mapping) {
    Map<String, Object> result = new TreeMap<>();
    result.put("downloadProgress", mapping.getDownloadProgress());
    result.put("localUrl", getLocalUrl(mapping.getSourceUrl()));
    result.put("sourceUrl", mapping.getSourceUrl());
    result.put("name", mapping.getName());
    result.put("idObject", mapping.getId());
    return result;
  }

  public List<Map<String, Object>> getReferenceGenomeTaxonomies() throws QueryException {
    try {
      Set<MiriamData> organisms = new LinkedHashSet<>();
      for (ReferenceGenomeType type : ReferenceGenomeType.values()) {
        organisms.addAll(referenceGenomeService.getOrganismsByReferenceGenomeType(type));
      }
      List<Map<String, Object>> result = new ArrayList<>();
      for (MiriamData miriamData : organisms) {
        result.add(createAnnotation(miriamData));
      }
      return result;
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  public List<Map<String, Object>> getReferenceGenomeTypes(String organismId) throws QueryException {
    try {
      Set<MiriamData> organisms = new LinkedHashSet<>();
      for (ReferenceGenomeType type : ReferenceGenomeType.values()) {
        organisms.addAll(referenceGenomeService.getOrganismsByReferenceGenomeType(type));
      }
      List<Map<String, Object>> result = new ArrayList<>();
      for (MiriamData miriamData : organisms) {
        if (miriamData.getResource().equals(organismId)) {
          Map<String, Object> entry = new HashMap<>();
          entry.put("type", ReferenceGenomeType.UCSC.toString());
          result.add(entry);
        }
      }
      return result;
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  public List<Map<String, Object>> getReferenceGenomeVersions(String organismId, String type)
      throws QueryException {
    ReferenceGenomeType genomeType = null;
    try {
      genomeType = ReferenceGenomeType.valueOf(type);
    } catch (IllegalArgumentException e) {
      throw new QueryException("Invalid type: " + type);
    }
    MiriamData organism;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    List<String> versions;
    try {
      versions = referenceGenomeService.getAvailableGenomeVersions(genomeType, organism);
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining version list", e);
    }

    List<Map<String, Object>> result = new ArrayList<>();
    for (String string : versions) {
      Map<String, Object> entry = new HashMap<>();
      entry.put("version", string);
      result.add(entry);
    }
    return result;
  }

  public List<Map<String, Object>> getReferenceGenomes() {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ReferenceGenome genome : referenceGenomeService.getDownloadedGenomes()) {
      result.add(genomeToMap(genome));
    }
    return result;
  }

  public Map<String, Object> removeGenome(String genomeId) throws IOException {
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(Integer.parseInt(genomeId));
    referenceGenomeService.removeGenome(genome);
    return okStatus();
  }

  public Map<String, Object> addReferenceGenome(MultiValueMap<String, Object> formData)
      throws QueryException, IOException, ReferenceGenomeConnectorException {
    String organismId = getFirstValue(formData.get("organismId"));
    String type = getFirstValue(formData.get("type"));
    String version = getFirstValue(formData.get("version"));
    String url = getFirstValue(formData.get("sourceUrl"));
    MiriamData organism;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
    try {
      referenceGenomeService.addReferenceGenome(genomeType, organism, version, url);
      return okStatus();
    } catch (ReferenceGenomeExistsException e) {
      throw new ObjectExistsException(e);
    } catch (URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

  public Map<String, Object> addGeneMapping(MultiValueMap<String, Object> formData, String genomeId)
      throws QueryException, IOException, ReferenceGenomeConnectorException {
    int id = parseInteger(genomeId, "genomeId");
    try {
      ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(id);
      String name = getFirstValue(formData.get("name"));
      String url = getFirstValue(formData.get("url"));
      if (!url.toLowerCase().endsWith("bb")) {
        throw new QueryException("Only big bed format files are supported but found: \"" + url + "\".");
      }

      referenceGenomeService.addReferenceGenomeGeneMapping(genome, name, url);
      return okStatus();
    } catch (URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

  public Map<String, Object> removeGeneMapping(String genomeId, String mappingId)
      throws IOException, ObjectNotFoundException {
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(Integer.parseInt(genomeId));
    if (genome == null) {
      throw new ObjectNotFoundException("Genome doesn't exist");
    }
    int id = Integer.parseInt(mappingId);
    ReferenceGenomeGeneMapping geneMapping = null;
    for (ReferenceGenomeGeneMapping mapping : genome.getGeneMapping()) {
      if (mapping.getId() == id) {
        geneMapping = mapping;
      }
    }
    if (geneMapping == null) {
      throw new ObjectNotFoundException("Gene mapping doesn't exist");
    }
    referenceGenomeService.removeReferenceGenomeGeneMapping(geneMapping);
    return okStatus();
  }

}
