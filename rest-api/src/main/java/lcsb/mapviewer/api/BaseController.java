package lcsb.mapviewer.api;

import java.io.*;
import java.net.URLDecoder;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.FileEntry;

public abstract class BaseController {
  private Logger logger = LogManager.getLogger(BaseController.class);

  private ObjectMapper mapper = new ObjectMapper();

  @ExceptionHandler({ Exception.class })
  public ResponseEntity<Object> handleException(Exception e) {
    if (e instanceof AccessDeniedException) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else if (e instanceof ObjectNotFoundException) {
      return createErrorResponse("Object not found.", e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    } else if (e instanceof ObjectExistsException) {
      return createErrorResponse("Object already exists.", e.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
    } else if (e instanceof OperationNotAllowedException) {
      return createErrorResponse("Operation not allowed.", e.getMessage(), new HttpHeaders(),
          HttpStatus.METHOD_NOT_ALLOWED);
    } else if (e instanceof QueryException
        || e instanceof HttpMessageNotReadableException
        || e instanceof MissingServletRequestParameterException
        || e instanceof HttpMediaTypeNotSupportedException
        || e instanceof MethodArgumentTypeMismatchException) {
      logger.error(e, e);
      return createErrorResponse("Query server error.", e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof ServletRequestBindingException && e.getMessage().contains(Configuration.AUTH_TOKEN)) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else {
      logger.error(e, e);
      return createErrorResponse("Internal server error.", e.getMessage(), new HttpHeaders(),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ResponseEntity<Object> createErrorResponse(String errorMessage, String error, HttpHeaders httpHeaders,
      HttpStatus status) {

    Map<String, String> response = new HashMap<>();
    response.put("error", errorMessage);
    response.put("reason", error);
    return new ResponseEntity<Object>(new Gson().toJson(response), httpHeaders, status);
  }

  public Map parseBody(String body) throws QueryException {
    try {
      if (body == null || body.isEmpty()) {
        return new TreeMap<>();
      }
      ObjectNode result = mapper.readValue(body, ObjectNode.class);
      return mapper.convertValue(result, Map.class);
    } catch (IOException e) {
      throw new QueryException("Cannot parse body", e);
    }
  }

  public Map[] extractListBody(String body) throws QueryException {
    try {
      if (body == null || body.isEmpty()) {
        return new Map[0];
      }
      return mapper.readValue(body, Map[].class);
    } catch (IOException e) {
      throw new QueryException("Cannot parse body", e);
    }
  }

  protected Map getData(Map<String, Object> node, String objectName) {
    return (Map) node.get(objectName);
  }

  protected Map<String, String> parsePostBody(String body) {
    Map<String, String> result = new TreeMap<>();
    String[] parameters = body.split("&");
    for (String string : parameters) {
      int position = string.indexOf("=");
      if (position < 0) {
        position = string.length();
      }
      String key = string.substring(0, position);
      String value = null;
      if (position < string.length()) {
        value = string.substring(position + 1);
        try {
          value = URLDecoder.decode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
          throw new InvalidStateException("Cannot decode input", e);
        }
      }
      result.put(key, value);
    }
    return result;
  }

  protected ResponseEntity<byte[]> sendAsZip(FileEntry originalFile) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(baos);

    ZipEntry entry = new ZipEntry(originalFile.getOriginalFileName());

    zos.putNextEntry(entry);
    zos.write(originalFile.getFileContent());
    zos.closeEntry();
    zos.close();


    return ResponseEntity.ok().contentLength(baos.size())
        .contentType(MediaType.parseMediaType(MimeType.ZIP.getTextRepresentation()))
        .header("Content-Disposition", "attachment; filename=" + originalFile.getOriginalFileName() + ".zip")
        .body(baos.toByteArray());
  }

}
