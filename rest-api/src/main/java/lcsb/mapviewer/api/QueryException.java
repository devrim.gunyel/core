package lcsb.mapviewer.api;

/**
 * Generic exception thrown when there is a problem with API query.
 * 
 * @author Piotr Gawron
 *
 */
public class QueryException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public QueryException(String message) {
    super(message);
  }

  public QueryException(Exception e) {
    super(e);
  }

  /**
   * Constructor with error message and parent exception.
   * 
   * @param message
   *          error message
   * @param reason
   *          parent exception that caused this one
   */
  public QueryException(String message, Exception reason) {
    super(message, reason);
  }

}
