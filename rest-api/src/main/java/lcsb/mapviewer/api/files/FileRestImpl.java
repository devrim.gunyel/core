package lcsb.mapviewer.api.files;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IFileService;

@Transactional
@Service
public class FileRestImpl extends BaseRestImpl {

  private UploadedFileEntryDao uploadedFileEntryDao;

  private IFileService fileService;

  @Autowired
  public FileRestImpl(UploadedFileEntryDao uploadedFileEntryDao, IFileService fileService) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
    this.fileService = fileService;
  }

  public Map<String, Object> createFile(String filename, String length, User user) {
    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName(filename);
    entry.setFileContent(new byte[] {});
    entry.setLength(Long.valueOf(length));
    entry.setOwner(user);
    uploadedFileEntryDao.add(entry);
    try {
      return getFile(entry.getId());
    } catch (ObjectNotFoundException e) {
      throw new InvalidStateException(e);
    }
  }

  public Map<String, Object> getFile(Integer id) throws ObjectNotFoundException {
    UploadedFileEntry fileEntry = fileService.getById(id);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    return serializeEntry(fileEntry);
  }

  private Map<String, Object> serializeEntry(UploadedFileEntry fileEntry) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", fileEntry.getId());
    result.put("filename", fileEntry.getOriginalFileName());
    result.put("length", fileEntry.getLength());
    result.put("owner", fileEntry.getOwner() == null ? null : fileEntry.getOwner().getLogin());
    result.put("uploadedDataLength", fileEntry.getFileContent().length);
    return result;
  }

  public Map<String, Object> uploadContent(Integer id, byte[] data) throws QueryException {
    int fileId = Integer.valueOf(id);
    UploadedFileEntry fileEntry = uploadedFileEntryDao.getById(fileId);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    long missingByteLength = fileEntry.getLength() - fileEntry.getFileContent().length;
    if (data.length > missingByteLength) {
      throw new QueryException(
          "Too many bytes sent. There are " + missingByteLength + " missing bytes, but " + data.length + " sent.");
    }
    byte[] newConent = ArrayUtils.addAll(fileEntry.getFileContent(), data);
    fileEntry.setFileContent(newConent);
    uploadedFileEntryDao.update(fileEntry);
    return serializeEntry(fileEntry);
  }

  public UploadedFileEntryDao getUploadedFileEntryDao() {
    return uploadedFileEntryDao;
  }

  public void setUploadedFileEntryDao(UploadedFileEntryDao uploadedFileEntryDao) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
  }

}
