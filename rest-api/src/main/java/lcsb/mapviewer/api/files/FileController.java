package lcsb.mapviewer.api.files;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(value = "/files", produces = MediaType.APPLICATION_JSON_VALUE)
public class FileController extends BaseController {

  private FileRestImpl fileRest;
  private IUserService userService;

  @Autowired
  public FileController(FileRestImpl fileRest, IUserService userService) {
    this.fileRest = fileRest;
    this.userService = userService;
  }

  @PostMapping(value = "/")
  public Map<String, Object> createFile(
      Authentication authentication,
      @RequestParam(value = "filename") String filename,
      @RequestParam(value = "length") String length) {
    return fileRest.createFile(filename, length, userService.getUserByLogin(authentication.getName()));
  }

  @PostAuthorize("hasAuthority('IS_ADMIN') or returnObject['owner'] == authentication.name")
  @GetMapping(value = "/{id}")
  public Map<String, Object> getFile(@PathVariable(value = "id") Integer id) throws ObjectNotFoundException {
    return fileRest.getFile(id);
  }

  @PreAuthorize("@fileService.getOwnerByFileId(#id)?.login == authentication.name")
  @PostMapping(value = "/{id}:uploadContent")
  public Map<String, Object> uploadContent(@PathVariable(value = "id") Integer id, @RequestBody byte[] data)
      throws QueryException {
    return fileRest.uploadContent(id, data);
  }

}