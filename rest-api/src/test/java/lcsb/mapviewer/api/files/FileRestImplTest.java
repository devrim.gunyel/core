package lcsb.mapviewer.api.files;

import static org.junit.Assert.*;

import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IUserService;

public class FileRestImplTest extends RestTestFunctions {

  @Autowired
  UploadedFileEntryDao uploadedFileEntryDao;

  @Autowired
  FileRestImpl fileRestImpl;

  @Autowired
  IUserService userService;

  @Test
  public void testCreateFile() throws Exception {
    User user = userService.getUserByLogin("admin");
    Map<String, Object> result = fileRestImpl.createFile("test.txt", "100", user);
    assertNotNull(result);
    int id = (Integer) result.get("id");
    UploadedFileEntry file = uploadedFileEntryDao.getById(id);
    assertNotNull(file);
    assertEquals(100, file.getLength());
    assertEquals(0, file.getFileContent().length);
    uploadedFileEntryDao.delete(file);
  }

  @Test
  public void testUploadContent() throws Exception {
    User user = userService.getUserByLogin("admin");
    byte[] dataChunk = new byte[] { 105, 103 };
    byte[] dataChunk2 = new byte[] { 32, 73 };
    byte[] dataChunkMerged = ArrayUtils.addAll(dataChunk, dataChunk2);
    Map<String, Object> result = fileRestImpl.createFile("test.txt", "100", user);
    int id = (Integer) result.get("id");
    fileRestImpl.uploadContent(id, dataChunk);

    UploadedFileEntry file = uploadedFileEntryDao.getById(id);
    assertEquals(100, file.getLength());
    assertEquals(2, file.getFileContent().length);
    assertArrayEquals(dataChunk, file.getFileContent());

    fileRestImpl.uploadContent(id, dataChunk2);

    assertEquals(100, file.getLength());
    assertEquals(4, file.getFileContent().length);
    assertArrayEquals(dataChunkMerged, file.getFileContent());

    uploadedFileEntryDao.delete(file);
  }

  @Test
  public void testUploadInvalidContent() throws Exception {
    User user = userService.getUserByLogin("admin");
    byte[] dataChunk = new byte[100];
    Map<String, Object> result = fileRestImpl.createFile("test.txt", "100", user);
    int id = (Integer) result.get("id");

    try {
      fileRestImpl.uploadContent(id, dataChunk);
    } finally {
      uploadedFileEntryDao.getById(id);
      UploadedFileEntry file = uploadedFileEntryDao.getById(id);
      uploadedFileEntryDao.delete(file);
    }
  }

}
