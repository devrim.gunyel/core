package lcsb.mapviewer.api.genomics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReferenceGenomeControllerTest.class })
public class AllGenomicsTests {

}
