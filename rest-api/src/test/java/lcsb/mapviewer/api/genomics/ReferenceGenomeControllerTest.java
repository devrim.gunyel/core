package lcsb.mapviewer.api.genomics;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;

public class ReferenceGenomeControllerTest extends RestTestFunctions {
  @Autowired
  public ReferenceGenomeRestImpl referenceGenomeRestImpl;
  Logger logger = LogManager.getLogger(ReferenceGenomeControllerTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = QueryException.class)
  public void testGetForNonExistingType() throws Exception {
    referenceGenomeRestImpl.getReferenceGenome("9606", "type", "ver");
  }

  @Test(expected = QueryException.class)
  public void testGetForNonExistingOrganism() throws Exception {
    referenceGenomeRestImpl.getReferenceGenome("960000", "UCSC", "ver");
  }

  @Test
  public void testGetReferenceGenomeTaxonomies() throws Exception {
    List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeTaxonomies();
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetReferenceGenomeTypes() throws Exception {
    List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeTypes("9606");
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetReferenceGenomeVersions() throws Exception {
    List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeVersions("9606", "UCSC");
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetRemoteUrls() throws Exception {
    List<Map<String, Object>> result = referenceGenomeRestImpl.getRemoteUrls("9606", "UCSC", "hg18");
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

}
