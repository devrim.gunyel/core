package lcsb.mapviewer.api;

import static org.junit.Assert.*;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.api.projects.ProjectRestImpl;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;

public class BaseRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger();

  @Autowired
  MiriamConnector mc;

  @Autowired
  ProjectRestImpl restImpl;

  @Autowired
  PubmedParser pubmedParser;

  @Autowired
  List<ElementAnnotator> annotators;

  @Test
  public void testMathMLToPresentationML() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    assertNotNull(presentationXml);
    assertTrue("Presentation math ml doesn't contain expected mrow tag", presentationXml.indexOf("mrow") >= 0);
  }

  @Test
  public void testMathMLToPresentationMLCalledTwice() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    String presentationXml2 = controller.mathMLToPresentationML(content);
    assertEquals(presentationXml, presentationXml2);
  }

  @Test
  public void testMathMLToPresentationMLCalledTwiceShouldntHaveXmlNode() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    assertFalse(presentationXml.startsWith("<?xml"));
  }

  @Test
  public void testCreateAnnotationWithWhitespace() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    controller.setMiriamConnector(mc);
    controller.setPubmedParser(pubmedParser);
    Map<String, Object> response = controller.createAnnotation(new MiriamData(MiriamType.PUBMED, "28255955 "));
    assertNotNull(response);
  }

  @Test
  public void testPrepareAnnotatorsParams() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    for (ElementAnnotator annotator : annotators) {
      AnnotatorData annotatorData = annotator.createAnnotatorData();
      List<Map<String, Object>> data = controller.prepareAnnotatorsParams(annotatorData.getAnnotatorParams());
      assertEquals(annotatorData.getAnnotatorParams().size(), data.size());
    }
  }

  @Test
  public void testCreateAnnotationWithNonNumericValues() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    controller.setMiriamConnector(mc);
    controller.setPubmedParser(pubmedParser);
    Map<String, Object> response = controller.createAnnotation(new MiriamData(MiriamType.PUBMED, "28255955PG"));
    assertNotNull(response);
  }

  @Test
  public void testCreateAnnotationWithEmptyValue() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    controller.setMiriamConnector(mc);
    controller.setPubmedParser(pubmedParser);
    Map<String, Object> response = controller.createAnnotation(new MiriamData(MiriamType.PUBMED, ""));
    assertNotNull(response);
  }

  @Test
  public void testParseIntegerFromDouble() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    Integer x = controller.parseInteger(12.3);
    assertEquals((Integer) 12, x);
  }

  @Test
  public void testParseIntegerFromNull() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    Integer x = controller.parseInteger(null);
    assertNull(x);
  }

  @Test
  public void testGetConverters() throws Exception {
    List<Converter> list = restImpl.getModelConverters();
    assertTrue(list.size() > 0);
  }

}
