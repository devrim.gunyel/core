package lcsb.mapviewer.api.convert;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConvertRestImplTest.class })
public class AllConvertTests {

}
