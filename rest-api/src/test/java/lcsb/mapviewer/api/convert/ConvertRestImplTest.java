package lcsb.mapviewer.api.convert;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.sbml.jsbml.SBMLException;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.map.InconsistentModelException;

public class ConvertRestImplTest extends RestTestFunctions {

  @Autowired
  public ConvertRestImpl convertRestImpl;
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ConvertRestImplTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetInformation() throws Exception {
    Map<String, Object> result = convertRestImpl.getInformationImage();
    assertTrue(result.keySet().size() == 2);
    assertNotNull(result.get("inputs"));
    assertNotNull(result.get("outputs"));

    List<Object> inputs = (List<Object>) result.get("inputs");
    List<Object> outputs = (List<Object>) result.get("outputs");

    assertTrue(inputs.size() > 0);
    assertTrue(outputs.size() > 0);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetInformationImage() throws Exception {
    Map<String, Object> result = convertRestImpl.getInformationImage();
    assertTrue(result.keySet().size() == 2);
    assertNotNull(result.get("inputs"));
    assertNotNull(result.get("outputs"));

    List<Object> inputs = (List<Object>) result.get("inputs");
    List<Object> outputs = (List<Object>) result.get("outputs");

    assertTrue(inputs.size() > 0);
    assertTrue(outputs.size() > 0);
  }

  @Test
  public void testCelDesigner2Sbml1() throws Exception {
    String content = readFile("testFiles/convert/sample-cd.xml");
    String result = convertRestImpl.convert(
        "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser",
        "lcsb.mapviewer.converter.model.sbml.SbmlParser", content);
    assertTrue(result.length() > 0);
    assertTrue(result.contains("</layout:speciesReferenceGlyph>"));
  }

  @Test
  public void testCelDesigner2Svg() throws Exception {
    String content = readFile("testFiles/convert/sample-cd.xml");
    String result = convertRestImpl.converToImage("CellDesigner_SBML", "svg", content).toString();

    assertTrue(result.contains("<rect"));
  }

  @Test
  public void testCellDesigner2Sbml2Svg() throws Exception {
    String content = readFile("testFiles/convert/neuron_cd.xml");
    String resultSbml = convertRestImpl.convert("CellDesigner_SBML", "SBML", content).toString();
    String resultSvg = convertRestImpl.converToImage("SBML", "svg", resultSbml).toString();

    assertTrue(resultSvg.contains("<rect"));
  }

  @Test
  public void testCellDesigner2Sbml2CellDesignerSvg() throws Exception {
    String content = readFile("testFiles/convert/neuron_cd.xml");
    String resultSbml = convertRestImpl.convert("CellDesigner_SBML", "SBML", content).toString();
    String resultCellDesignerSbml = convertRestImpl.convert("SBML", "CellDesigner_SBML", resultSbml)
        .toString();
    String resultSvg = convertRestImpl.converToImage("CellDesigner_SBML", "svg", resultCellDesignerSbml)
        .toString();

    assertTrue(resultSvg.contains("<rect"));
  }

  @Test
  public void testScaling() throws Exception {
    String content = readFile("testFiles/convert/sample-cd.xml");
    String result1 = convertRestImpl.converToImage("CellDesigner_SBML", "svg", content, 50.0, 50.0).toString();
    String result2 = convertRestImpl.converToImage("CellDesigner_SBML", "svg", content, 100.0, 100.0)
        .toString();

    Pattern pattern = Pattern.compile(".*scale\\(([0-9]+\\.[0-9]+),([0-9]+\\.[0-9]+)\\).*");
    Matcher matcher1 = pattern.matcher(result1);
    Matcher matcher2 = pattern.matcher(result2);

    assertTrue(matcher1.find());
    assertTrue(matcher2.find());

    assertFalse(matcher1.group(1).toString().equals(matcher2.group(1).toString()));
  }

  @Test
  public void test1() throws Exception {
    String content = readFile("testFiles/convert/glycolysis2.xml");

    String result = convertRestImpl.convert("CellDesigner_SBML", "SBML", content);
    ByteArrayOutputStream result1 = convertRestImpl.converToImage("SBML", "svg", result);

    File output = new File("testFiles/convert/glycolysis2.svg");

    FileOutputStream outputStream = new FileOutputStream(output);
    outputStream.write(result1.toByteArray());
    outputStream.close();
    assertTrue(output.exists());
    output.delete();
  }

  @Test
  public void test2() throws Exception {
    String content = readFile("testFiles/convert/glycolysis2.xml");

    String result = convertRestImpl.convert("CellDesigner_SBML", "SBML", content);
    String result1 = convertRestImpl.convert("SBML", "CellDesigner_SBML", result);

    assertNotNull(result);
    assertNotNull(result1);
  }

  @Test
  public void testCelDesigner2Sbgn() throws Exception {
    String content = readFile("testFiles/convert/sample-cd.xml");
    String result = convertRestImpl.convert("CellDesigner_SBML", "SBGN-ML", content).toString();
    assertTrue(result.contains("glyph class=\"complex\""));
  }

  @SuppressWarnings("unchecked")
  private List<String> extractIdsFromOutputs(Map<String, Object> infoObject) {
    List<String> ids = new ArrayList<>();
    for (Map<String, List<String>> o : (List<Map<String, List<String>>>) infoObject.get("outputs")) {
      ids.add(o.get("available_names").get(0));
    }
    return ids;
  }

  private void test2All(String content, String converter)
      throws SBMLException, SecurityException, InvalidInputDataExecption, InconsistentModelException,
      IOException, ConverterException, XMLStreamException, DrawingException, QueryException {

    Map<String, Object> info = convertRestImpl.getInformation();
    Map<String, Object> infoImage = convertRestImpl.getInformationImage();

    List<String> targets = new ArrayList<>();

    targets.addAll(extractIdsFromOutputs(info));
    for (String target : targets) {
      String result = convertRestImpl.convert(converter, target, content).toString();
      assertTrue(result.length() > 0);
    }

    targets.clear();
    targets.addAll(extractIdsFromOutputs(infoImage));
    for (String target : targets) {
      String result = convertRestImpl.converToImage(converter, target, content).toString();
      assertTrue(result.length() > 0);
    }
  }

  @Test
  public void testCelDesigner2All() throws Exception {
    String content = readFile("testFiles/convert/sample-cd.xml");
    test2All(content, "CellDesigner_SBML");
  }

  @Test
  public void testSbml2All() throws Exception {
    String content = readFile("testFiles/convert/sample-sbml.xml");
    test2All(content, "SBML");
  }

  @Test
  public void testSbgn2All() throws Exception {
    String content = readFile("testFiles/convert/sample.sbgn");
    test2All(content, "SBGN-ML");
  }

  @Test
  public void testNewtSbgn2All() throws Exception {
    String content = readFile("testFiles/convert/newt.sbgn");
    test2All(content, "SBGN-ML");
  }

}
