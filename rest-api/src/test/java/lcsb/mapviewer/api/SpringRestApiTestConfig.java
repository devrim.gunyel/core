package lcsb.mapviewer.api;

import org.springframework.context.annotation.*;
import org.springframework.security.core.session.SessionRegistryImpl;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.persist.SpringPersistConfig;
import lcsb.mapviewer.services.SpringServiceConfig;

@Configuration
@Import({
    SpringRestApiConfig.class,
    SpringPersistConfig.class,
    SpringServiceConfig.class,
    SpringAnnotationConfig.class
})
public class SpringRestApiTestConfig {

  @Bean
  public SessionRegistryImpl sessionRegistry() {
    return new SessionRegistryImpl();
  }

}
