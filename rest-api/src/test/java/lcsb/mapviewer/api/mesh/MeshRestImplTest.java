package lcsb.mapviewer.api.mesh;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.RestTestFunctions;

public class MeshRestImplTest extends RestTestFunctions {

  Logger logger = LogManager.getLogger(MeshRestImplTest.class);
  @Autowired
  MeshRestImpl meshRestImpl;

  @Test
  public void test() throws ObjectNotFoundException, AnnotatorException {
    Map<String, Object> result = meshRestImpl.getMesh("D010300");
    assertNotNull(result);
    String name = (String) result.get("name");
    assertTrue(name.toLowerCase().contains("parkinson"));
  }

}
