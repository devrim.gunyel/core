package lcsb.mapviewer.api.projects.models.bioEntities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.api.projects.models.bioEntities.elements.AllElementsTests;
import lcsb.mapviewer.api.projects.models.bioEntities.reactions.AllReactionsTests;

@RunWith(Suite.class)
@SuiteClasses({
    AllElementsTests.class,
    AllReactionsTests.class,
    BioEntitiesControllerTest.class
})
public class AllBioeEntitiesTests {

}
