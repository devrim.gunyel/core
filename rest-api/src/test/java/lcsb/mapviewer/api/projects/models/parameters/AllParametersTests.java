package lcsb.mapviewer.api.projects.models.parameters;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ParametersRestImplTest.class })
public class AllParametersTests {

}
