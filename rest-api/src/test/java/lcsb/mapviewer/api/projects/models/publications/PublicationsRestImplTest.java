package lcsb.mapviewer.api.projects.models.publications;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.*;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.api.projects.models.publications.PublicationsRestImpl.SortColumn;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class PublicationsRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(PublicationsRestImplTest.class);

  @Autowired
  PublicationsRestImpl _projectRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPublications() throws Exception {
    PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getPublications("sample", "*", "0", 20, "", "", "");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertEquals(1, ((List<?>) result.get("data")).size());

    result = projectRest.getPublications("sample", "*", "1", 20, "", "", "");
    assertEquals(1, result.get("totalSize"));
    assertEquals(1, result.get("start"));
    assertEquals(0, ((List<?>) result.get("data")).size());

    result = projectRest.getPublications("sample", "*", "0", 00, "", "", "");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertEquals(0, ((List<?>) result.get("data")).size());
  }

  @Test
  public void testPublicationsFiltering() throws Exception {
    PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getPublications("sample", "*", "0", 20, "pubmedId", "asc", "123");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertEquals(1, ((List<?>) result.get("data")).size());

    result = projectRest.getPublications("sample", "*", "0", 20, "pubmedId", "asc", "1234");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertEquals(0, ((List<?>) result.get("data")).size());
  }

  @Test
  public void testPublicationsFilteringByModelName() throws Exception {
    PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getPublications("sample", "*", "0", 20, "pubmedId", "asc",
        "sample");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertTrue(((List<?>) result.get("data")).size() > 0);

    result = projectRest.getPublications("sample", "*", "0", 20, "pubmedId", "asc", "sampleX");
    assertEquals(1, result.get("totalSize"));
    assertEquals(0, result.get("start"));
    assertEquals(0, ((List<?>) result.get("data")).size());
  }

  @Test(expected = QueryException.class)
  public void testPublicationsInvalidSortingFiltering() throws Exception {
    PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    projectRest.getPublications("sample", "*", "0", 20, "xxx", "asc", "123");
  }

  private PublicationsRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    _projectRestImpl.setModelService(mockModelService);
    return _projectRestImpl;
  }

  @Test
  public void testComparatorForColumnWithInvalidData() {
    Entry<MiriamData, List<BioEntity>> valid = new Entry<MiriamData, List<BioEntity>>() {

      @Override
      public MiriamData getKey() {
        return new MiriamData(MiriamType.PUBMED, "12345");
      }

      @Override
      public List<BioEntity> getValue() {
        return new ArrayList<>();
      }

      @Override
      public List<BioEntity> setValue(List<BioEntity> value) {
        return null;
      }
    };
    Entry<MiriamData, List<BioEntity>> invalid = new Entry<MiriamData, List<BioEntity>>() {

      @Override
      public MiriamData getKey() {
        return new MiriamData(MiriamType.PUBMED, "");
      }

      @Override
      public List<BioEntity> getValue() {
        return new ArrayList<>();
      }

      @Override
      public List<BioEntity> setValue(List<BioEntity> value) {
        return null;
      }
    };
    for (SortColumn sortColumn : SortColumn.values()) {
      Comparator<Entry<MiriamData, List<BioEntity>>> comparator = _projectRestImpl.getComparatorForColumn(sortColumn,
          "desc");
      assertNotNull(comparator.compare(valid, invalid));

    }
  }

}
