package lcsb.mapviewer.api.projects.overlays;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ColorSchemaType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class OverlayRestImplTest extends RestTestFunctions {

  @Autowired
  OverlayRestImpl overlayRest;

  @Autowired
  ProjectDao projectDao;

  @Autowired
  UserDao userDao;

  @Autowired
  IProjectService projectService;

  String projectId = "test_id";

  @Test
  public void testAddDataOverlay() throws Exception {
    try {
      User admin = userService.getUserByLogin("admin");
      createProject("testFiles/model/sample.xml", projectId);

      Map<String, Object> result = overlayRest.addOverlay(projectId, "x", "desc", "s1", null, null,
          ColorSchemaType.GENERIC.name(), "true", admin);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
    } finally {
      projectService.removeProject(projectDao.getProjectByProjectId(projectId), null, false);
    }
  }

  @Test(expected = QueryException.class)
  public void testAddDataOverlayWithInvalidName() throws Exception {
    try {
      createProject("testFiles/model/sample.xml", projectId);

      overlayRest.addOverlay(projectId, " ", "desc", "s1", null, null, ColorSchemaType.GENERIC.name(), "true", null);
    } finally {
      projectService.removeProject(projectDao.getProjectByProjectId(projectId), null, false);
    }
  }

  @Test
  public void testUpdateDataOverlay() throws Exception {
    try {
      createProject("testFiles/model/sample.xml", projectId);

      Map<String, Object> result = overlayRest.addOverlay(projectId, "x", "desc", "s1", null, null,
          ColorSchemaType.GENERIC.name(), "true", null);

      Integer id = Integer.valueOf(result.get("idObject").toString());
      Map<String, Object> data = new HashMap<>();
      data.put("name", "xyz");
      result = overlayRest.updateOverlay(projectId, id, data);
      assertNotNull(result);
    } finally {
      projectService.removeProject(projectDao.getProjectByProjectId(projectId), null, false);
    }
  }

  @Test(expected = QueryException.class)
  public void testUpdateDataOverlayWithInvalidName() throws Exception {
    try {
      createProject("testFiles/model/sample.xml", projectId);

      Map<String, Object> result = overlayRest.addOverlay(projectId, "x", "desc", "s1", null, null,
          ColorSchemaType.GENERIC.name(), "true", null);

      Integer id = Integer.valueOf(result.get("idObject").toString());
      Map<String, Object> data = new HashMap<>();
      data.put("name", "");
      result = overlayRest.updateOverlay(projectId, id, data);
      assertNotNull(result);
    } finally {
      projectService.removeProject(projectDao.getProjectByProjectId(projectId), null, false);
    }
  }

  private Project createProject(String string, String projectId) throws Exception {
    Project project = new Project(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    Model model = super.getModelForFile(string, false);
    project.addModel(model);
    projectDao.add(project);
    return project;
  }

}
