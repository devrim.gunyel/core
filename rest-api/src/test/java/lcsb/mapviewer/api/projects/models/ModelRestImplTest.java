package lcsb.mapviewer.api.projects.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.ArgumentMatchers.refEq;

import java.util.*;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.PdfImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class ModelRestImplTest extends RestTestFunctions {

  @Autowired
  ModelRestImpl _modelRestImpl;

  @Autowired
  IModelService modelService;

  @Autowired
  IProjectService projectService;

  @Before
  public void setUp() throws Exception {
    _modelRestImpl.setModelService(modelService);
    _modelRestImpl.setProjectService(projectService);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testModels() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    List<Map<String, Object>> result = modelRest.getModels("sample");
    assertEquals(1, result.size());
  }

  @Test
  public void testGetEmptyModels() throws Exception {
    ModelRestImpl modelRest = createEmptyProjectRest("test");
    List<Map<String, Object>> result = modelRest.getModels("test");
    assertEquals(0, result.size());
  }

  private ModelRestImpl createEmptyProjectRest(String string) throws Exception {
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(refEq(string))).thenReturn(null);

    IProjectService mockProjectService = Mockito.mock(IProjectService.class);
    Mockito.when(mockProjectService.getProjectByProjectId(refEq(string))).thenReturn(new Project());

    _modelRestImpl.setModelService(mockModelService);
    _modelRestImpl.setProjectService(mockProjectService);
    return _modelRestImpl;
  }

  private ModelRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    Project project = new Project();
    project.addModel(model);

    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(nullable(String.class))).thenReturn(model);
    _modelRestImpl.setModelService(mockModelService);

    IProjectService mockProjectService = Mockito.mock(IProjectService.class);
    Mockito.when(mockProjectService.getProjectByProjectId(nullable(String.class))).thenReturn(project);
    _modelRestImpl.setProjectService(mockProjectService);

    return _modelRestImpl;
  }

  @Test(expected = QueryException.class)
  public void testGetModelAsImageForInvalidConverter() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    modelRest.getModelAsImage("sample", "0", "", "", "", "", "", null);
  }

  @Test
  public void testGetModelAsImage() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    FileEntry result = modelRest.getModelAsImage("sample", "0", PdfImageGenerator.class.getCanonicalName(), "",
        "", "", "", null);
    assertNotNull(result);
  }

  @Test
  public void testGetModelAsImage2() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    modelRest.getModelAsImage("sample", "0", PdfImageGenerator.class.getCanonicalName(), "", "", "", "", null);
  }

  @Test(expected = QueryException.class)
  public void testGetModelAsFileModel() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    modelRest.getModelAsModelFile("sample", "0", "", "", "", null, null, null, "");
  }

  @Test
  public void testGetModelAsFileModel2() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    modelRest.getModelAsModelFile("sample", "0", CellDesignerXmlParser.class.getCanonicalName(), "",
        "0,0;90,0;90,90;90,0", null, null, null, "");
  }

  @Test(expected = QueryException.class)
  public void testGetModelAsFileModel3() throws Exception {
    ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
    modelRest.getModelAsModelFile("sample", "0", "", "", "0,0;90,0;90,90;90,0", null, null, null, "");
  }

  @Test
  public void testEmptyStringListToIntegerList() throws Exception {
    Set<Integer> ids = _modelRestImpl.stringListToIntegerSet("");
    assertNotNull(ids);
    assertEquals(0, ids.size());
  }

  @Test
  public void testStringListToIntegerArray() throws Exception {
    Set<Integer> ids = _modelRestImpl.stringListToIntegerSet("12,3");
    assertNotNull(ids);
    assertEquals(2, ids.size());
  }
}
