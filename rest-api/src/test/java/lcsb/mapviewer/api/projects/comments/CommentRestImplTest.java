package lcsb.mapviewer.api.projects.comments;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class CommentRestImplTest extends RestTestFunctions {

  @Autowired
  public CommentRestImpl _commentRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = QueryException.class)
  public void testGetForNonExisting() throws Exception {
    _commentRestImpl.getCommentList("unk", "", "", "", "");
  }

  @Test
  public void testGetCommentsDependencies() throws Exception {
    CommentRestImpl commentRestImpl = createMockProjectRest("testFiles/model/sample.xml");
    commentRestImpl.getCommentList("sample", "", "", "", "");
    Mockito.verify(commentRestImpl.getModelService(), times(0)).getLastModelByProjectId(anyString());
  }

  private CommentRestImpl createMockProjectRest(String string) throws Exception {
    Model model = null;
    Project project = null;
    String modelName = "";
    if (string != null) {
      project = new Project();
      model = super.getModelForFile(string, true);
      project.addModel(model);
      modelName = model.getName();
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(eq(modelName))).thenReturn(model);
    _commentRestImpl.setModelService(mockModelService);

    IProjectService projectServiceMock = Mockito.mock(IProjectService.class);
    Mockito.when(projectServiceMock.getProjectByProjectId(eq(modelName))).thenReturn(project);
    List<Project> projects = new ArrayList<>();
    projects.add(project);
    Mockito.when(projectServiceMock.getAllProjects()).thenReturn(projects);
    _commentRestImpl.setProjectService(projectServiceMock);

    return _commentRestImpl;
  }

}
