package lcsb.mapviewer.api.projects.models.parameters;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class ParametersRestImplTest extends RestTestFunctions {

  @Autowired
  ParametersRestImpl parametersRestImpl;

  @Autowired
  IConfigurationService configurationService;

  @Autowired
  IProjectService projectService;

  @Test(expected = ObjectNotFoundException.class)
  public void testGetParameterForNonExistingProject() throws Exception {
    parametersRestImpl.getParameter("non-existing", "-1", "-1");
  }

  @Test(expected = ObjectNotFoundException.class)
  public void testGetParameterForNonExistingModel() throws Exception {
    parametersRestImpl.getParameter(configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP),
        "0", "0");
  }

  @Test(expected = ObjectNotFoundException.class)
  public void testGetNonExistingParameter() throws Exception {
    Project project = projectService.getProjectByProjectId(
        configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP));
    parametersRestImpl.getParameter(project.getProjectId(),
        "" + project.getModels().iterator().next().getId(), "-1");
  }

}
