package lcsb.mapviewer.api.projects.drugs;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;

public class DrugRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(DrugRestImplTest.class);

  @Autowired

  private DrugRestImpl _drugRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetDrugsByQuery() throws Exception {
    DrugRestImpl drugRestImpl = createMockProjectRest("testFiles/model/drug_target.xml");
    Map<String, Object> result = drugRestImpl.getDrugsByQuery("drug_target", "", "Selisistat").get(0);
    List<Map<String, Object>> references = (List<Map<String, Object>>) result.get("references");
    Map<String, Object> reference = references.get(0);
    assertNotNull(reference.get("type"));
  }

  @Test
  public void testTargetWithEmptyMechanism() throws Exception {
    DrugRestImpl drugRestImpl = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = drugRestImpl.getDrugsByQuery("sample", "", "Picato").get(0);
    assertNotNull(result);
  }

  private DrugRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    _drugRestImpl.setModelService(createModelMockServiceForModel(model));
    _drugRestImpl.setProjectService(createProjectMockServiceForModel(model));
    return _drugRestImpl;
  }

}
