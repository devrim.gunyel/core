package lcsb.mapviewer.api.projects.models;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.api.projects.models.bioEntities.AllBioeEntitiesTests;
import lcsb.mapviewer.api.projects.models.functions.AllFunctionsTests;
import lcsb.mapviewer.api.projects.models.parameters.AllParametersTests;
import lcsb.mapviewer.api.projects.models.publications.AllPublicationsTests;

@RunWith(Suite.class)
@SuiteClasses({
    AllBioeEntitiesTests.class,
    AllFunctionsTests.class,
    AllParametersTests.class,
    AllPublicationsTests.class,
    ModelRestImplTest.class,
})
public class AllModelsTests {

}
