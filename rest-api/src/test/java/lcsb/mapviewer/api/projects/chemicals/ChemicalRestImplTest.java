package lcsb.mapviewer.api.projects.chemicals;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class ChemicalRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(ChemicalRestImplTest.class);

  @Autowired
  private ChemicalRestImpl _drugRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPrepareChemical() throws Exception {
    Chemical chemical = new Chemical();
    chemical.setChemicalId(new MiriamData(MiriamType.MESH_2012, "D010300"));
    Map<String, Object> result = _drugRestImpl.prepareChemical(chemical, _drugRestImpl.createChemicalColumnSet(""),
        new ArrayList<>());
    assertNotNull(result);
  }

  @Test
  public void testPrepareChemicalWithProblematicMesh() throws Exception {
    Chemical chemical = new Chemical();
    chemical.setChemicalId(new MiriamData(MiriamType.MESH_2012, "invalidID"));

    Map<String, Object> result = _drugRestImpl.prepareChemical(chemical, _drugRestImpl.createChemicalColumnSet(""),
        new ArrayList<>());
    assertNotNull(result);
  }

  protected ChemicalRestImpl createMockChemicalRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    _drugRestImpl.setModelService(mockModelService);
    return _drugRestImpl;
  }

}
