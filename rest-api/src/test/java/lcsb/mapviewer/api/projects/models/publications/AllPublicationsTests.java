package lcsb.mapviewer.api.projects.models.publications;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PublicationsRestImplTest.class })
public class AllPublicationsTests {

}
