package lcsb.mapviewer.api.projects.models.bioEntities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class BioEntitiesControllerTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(BioEntitiesControllerTest.class);

  @Autowired
  BioEntitiesRestImpl _bioEntitiesRestImpl;

  ObjectMapper mapper = new ObjectMapper();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetClosestElementsByCoordinates() throws Exception {
    BioEntitiesRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    int count = 3;
    List<Map<String, Object>> result = projectRest.getClosestElementsByCoordinates("sample", "0",
        new Point2D.Double(1, 2), count, "false", "");
    assertEquals(count, result.size());

    String json = mapper.writeValueAsString(result);
    assertNotNull(json);
  }

  private BioEntitiesRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    _bioEntitiesRestImpl.setModelService(mockModelService);
    return _bioEntitiesRestImpl;
  }

}
