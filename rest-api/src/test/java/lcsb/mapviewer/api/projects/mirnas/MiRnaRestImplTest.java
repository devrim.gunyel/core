package lcsb.mapviewer.api.projects.mirnas;

import static org.junit.Assert.assertFalse;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;

public class MiRnaRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(MiRnaRestImplTest.class);

  @Autowired
  MiRnaRestImpl _miRnaRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetMiRnasByTarget() throws Exception {
    MiRnaRestImpl miRnaRestImpl = createMockProjectRest("testFiles/model/mi_rna_target.xml");
    List<Map<String, Object>> result = miRnaRestImpl.getMiRnasByTarget("mi_rna_target", "ALIAS", "0", "");
    Set<String> names = new HashSet<>();
    for (Map<String, Object> miRna : result) {
      assertFalse("Duplicate miRna found: " + miRna.get("name"), names.contains(miRna.get("name")));
      names.add((String) miRna.get("name"));
    }
  }

  private MiRnaRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    _miRnaRestImpl.setModelService(createModelMockServiceForModel(model));
    _miRnaRestImpl.setProjectService(createProjectMockServiceForModel(model));
    return _miRnaRestImpl;
  }

}
