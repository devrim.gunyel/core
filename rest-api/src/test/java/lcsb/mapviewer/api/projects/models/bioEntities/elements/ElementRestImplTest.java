package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.annotation.services.annotators.PdbAnnotator;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;
import lcsb.mapviewer.services.interfaces.IModelService;

public class ElementRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(RestTestFunctions.class);

  @Autowired
  ElementsRestImpl _elementsRestImpl;

  @Autowired
  PdbAnnotator pdbAnnotator;

  ObjectMapper mapper = new ObjectMapper();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetElementsProcessAllColumns() throws Exception {
    ElementsRestImpl projectRest = createMockElementRest("testFiles/model/sample.xml", true);
    List<Map<String, Object>> result = projectRest.getElements("sample", "", "", "*", "", "", "");
    for (Map<String, Object> element : result) {
      for (String paramName : element.keySet()) {
        Object val = element.get(paramName);
        String paramValue = "";
        if (val != null) {
          paramValue = val.toString();
        }
        assertFalse("Improper handler for column name: " + paramName, paramValue.contains("Unknown column"));
      }
    }
    String json = mapper.writeValueAsString(result);
    assertNotNull(json);
  }

  @Test
  public void testGetElementsByType() throws Exception {
    String proteinType = new GenericProtein("1").getStringType();
    ElementsRestImpl projectRest = createMockElementRest("testFiles/model/sample.xml", false);
    List<Map<String, Object>> result = projectRest.getElements("sample", "", "", "*", proteinType, "", "");
    assertEquals(12, result.size());
  }

  @Test
  public void testGetElementsVisibility() throws Exception {
    ElementsRestImpl elementRest = createMockElementRest("testFiles/model/sample.xml");
    List<Map<String, Object>> result = elementRest.getElements("sample", "", "", "*", "", "", "");
    for (Map<String, Object> map : result) {
      assertTrue(map.get("hierarchyVisibilityLevel") instanceof String);
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForProtein() throws Exception {
    StructuralState state = new StructuralState();
    state.setValue("asd");
    GenericProtein protein = new GenericProtein("s1");
    Residue mr = new Residue();
    mr.setState(ModificationState.ACETYLATED);
    mr.setName("S250");
    protein.addResidue(mr);
    protein.setStructuralState(state);
    Map<String, Object> result = _elementsRestImpl.getOthersForElement(protein);
    assertNotNull(result.get("modifications"));
    assertEquals(state.getValue(), result.get("structuralState"));
    List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
    assertEquals(1, modifications.size());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForRna() throws Exception {
    Rna rna = new Rna("s1");
    ModificationSite mr = new ModificationSite();
    mr.setState(ModificationState.ACETYLATED);
    mr.setName("S250");
    rna.addModificationSite(mr);
    Map<String, Object> result = _elementsRestImpl.getOthersForElement(rna);
    assertNotNull(result.get("modifications"));
    assertEquals(null, result.get("structuralState"));
    List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
    assertEquals(1, modifications.size());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForAntisenseRna() throws Exception {
    AntisenseRna antisenseRna = new AntisenseRna("s1");
    ModificationSite mr = new ModificationSite();
    mr.setName("S250");
    antisenseRna.addModificationSite(mr);
    Map<String, Object> result = _elementsRestImpl.getOthersForElement(antisenseRna);
    assertNotNull(result.get("modifications"));
    assertEquals(null, result.get("structuralState"));
    List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
    assertEquals(1, modifications.size());
  }

  private ElementsRestImpl createMockElementRest(String string) throws Exception {
    return createMockElementRest(string, false);
  }

  private ElementsRestImpl createMockElementRest(String string, Boolean annotate) throws Exception {
    Model model = super.getModelForFile(string, !annotate);
    if (annotate) {
      try {
        Protein protein = new GenericProtein("SNCA");
        protein.setElementId("SNCA");
        pdbAnnotator.annotateElement(protein);
        model.addElement(protein);
      } catch (Exception e) {
      }
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    _elementsRestImpl.setModelService(mockModelService);
    return _elementsRestImpl;
  }
}
