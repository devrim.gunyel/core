package lcsb.mapviewer.api.projects;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;

import lcsb.mapviewer.api.*;
import lcsb.mapviewer.converter.zip.*;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class ProjectRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(ProjectRestImplTest.class);

  @Autowired
  ProjectRestImpl _projectRestImpl;

  @Autowired
  IModelService modelService;

  @Autowired
  IProjectService projectService;

  @Autowired
  ProjectDao projectDao;

  @Autowired
  UserDao userDao;

  @Before
  public void before() {
    _projectRestImpl.setModelService(modelService);
    _projectRestImpl.setProjectService(projectService);

  }

  @Test
  public void testGetModelDataDependencies() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getProject("sample");
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));
    Mockito.verify(projectRest.getModelService(), times(0)).getLastModelByProjectId(anyString());
  }

  @Test(expected = ObjectNotFoundException.class)
  public void testRemoveNonExistentProject() throws Exception {
    _projectRestImpl.removeProject("blabla", null);
  }

  @Test
  public void testRemoveProject() throws Exception {
    String projectId = "test";
    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    _projectRestImpl.removeProject(projectId, null);
  }

  @Test(expected = ObjectNotFoundException.class)
  public void testGetInvalidMetaData() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest(null);
    projectRest.getProject("unknown_model_id");
  }

  @Test
  public void testGetProject() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getProject("sample");
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));
  }

  @Test
  public void testUpdateProject() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, String> disease = new HashMap<>();
    disease.put("type", MiriamType.MESH_2012.name());
    disease.put("resource", "D010300");
    Map<String, Object> data = new HashMap<>();
    data.put("version", "1");
    data.put("name", "test");
    data.put("organism", null);
    data.put("disease", disease);
    data.put("projectId", "sample");
    data.put("id", "0");
    projectRest.updateProject("sample", data);
  }

  @Test
  public void testComputePathForProject() throws Exception {
    String projectId = "Some_id";
    String directory1 = _projectRestImpl.computePathForProject(projectId, ".");
    Project project = new Project(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectDao.delete(project);
    String directory2 = _projectRestImpl.computePathForProject(projectId, ".");

    assertFalse(directory1.equals(directory2));
  }

  @Test
  public void testGetProjects() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    List<Map<String, Object>> result = projectRest.getProjects();
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));
  }

  @Test
  public void testGetStatistics() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getStatistics("sample");
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));

    Map<?, ?> elementAnnotations = (Map<?, ?>) result.get("elementAnnotations");
    assertEquals(elementAnnotations.get(MiriamType.CAS), 0);
    assertTrue((Integer) elementAnnotations.get(MiriamType.ENTREZ) > 0);

    Map<?, ?> reactionAnnotations = (Map<?, ?>) result.get("reactionAnnotations");
    assertEquals(reactionAnnotations.get(MiriamType.ENTREZ), 0);
    assertEquals(reactionAnnotations.get(MiriamType.PUBMED), 1);
  }

  @Test
  public void testGetMetaDataForComplexWithImages() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/complex_model_with_submaps.zip");
    Map<String, Object> result = projectRest.getProject("sample");
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));
  }

  @Test
  public void testExtractZipEntries() throws Exception {
    MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
    data.put("zip-entries[0][_type]", createLinkedList("MAP"));
    data.put("zip-entries[0][_filename]", createLinkedList("main.xml"));
    data.put("zip-entries[0][_data][root]", createLinkedList("true"));
    data.put("zip-entries[0][_data][name]", createLinkedList("main"));
    data.put("zip-entries[0][_data][type][id]", createLinkedList("UNKNOWN"));
    data.put("zip-entries[0][_data][type][name]", createLinkedList("Unknown"));
    data.put("zip-entries[1][_type]", createLinkedList("OVERLAY"));
    data.put("zip-entries[1][_filename]", createLinkedList("layouts/goodschema.txt"));
    data.put("zip-entries[1][_data][name]", createLinkedList("example name"));
    data.put("zip-entries[1][_data][description]", createLinkedList("layout description"));
    data.put("zip-entries[2][_type]", createLinkedList("IMAGE"));
    data.put("zip-entries[2][_filename]", createLinkedList("images/test.png"));
    List<ZipEntryFile> result = _projectRestImpl.extractZipEntries(data);
    assertNotNull(result);
    assertEquals(3, result.size());
    assertTrue(result.get(0) instanceof ModelZipEntryFile);
    assertTrue(result.get(1) instanceof LayoutZipEntryFile);
    assertTrue(result.get(2) instanceof ImageZipEntryFile);

  }

  @Test(expected = QueryException.class)
  public void testExtractZipEntriesWithInvalidType() throws Exception {
    MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
    data.put("zip-entries[0][_type]", createLinkedList("MAPX"));
    data.put("zip-entries[0][_filename]", createLinkedList("main.xml"));
    _projectRestImpl.extractZipEntries(data);
  }

  @Test
  public void testExtractZipEntriesWithNoMapType() throws Exception {
    MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
    data.put("zip-entries[0][_type]", createLinkedList("MAP"));
    data.put("zip-entries[0][_filename]", createLinkedList("main.xml"));
    data.put("zip-entries[0][_data][root]", createLinkedList("true"));
    data.put("zip-entries[0][_data][name]", createLinkedList("main"));
    List<ZipEntryFile> result = _projectRestImpl.extractZipEntries(data);
    assertNotNull(result);
    assertEquals(1, result.size());
    assertTrue(result.get(0) instanceof ModelZipEntryFile);
  }

  private LinkedList<Object> createLinkedList(Object string) {
    LinkedList<Object> result = new LinkedList<>();
    result.add(string);
    return result;
  }

  private ProjectRestImpl createMockProjectRest(String string) throws Exception {
    Model model = null;
    Project project = null;
    if (string != null) {
      project = new Project();
      model = super.getModelForFile(string, true);
      project.addModel(model);
      project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
      project.setProjectId(model.getName());
      project.addLogEntry(new ProjectLogEntry());
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    _projectRestImpl.setModelService(mockModelService);

    IProjectService projectServiceMock = Mockito.mock(IProjectService.class);
    Mockito.when(projectServiceMock.getProjectByProjectId(anyString())).thenReturn(project);
    List<Project> projects = new ArrayList<>();
    projects.add(project);
    Mockito.when(projectServiceMock.getAllProjects()).thenReturn(projects);
    _projectRestImpl.setProjectService(projectServiceMock);

    return _projectRestImpl;
  }

  @Test
  public void testGetLogs() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
    Map<String, Object> result = projectRest.getLogs("sample", "", "0", 10, "id", "asc", "");
    assertNotNull(result);
    Gson gson = new Gson();
    assertNotNull(gson.toJson(result));
  }

}
