package lcsb.mapviewer.api.projects;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.api.projects.chemicals.AllChemicalTests;
import lcsb.mapviewer.api.projects.comments.AllCommentTests;
import lcsb.mapviewer.api.projects.drugs.AllDrugTests;
import lcsb.mapviewer.api.projects.mirnas.AllMiRnaTests;
import lcsb.mapviewer.api.projects.models.AllModelsTests;
import lcsb.mapviewer.api.projects.overlays.OverlayRestImplTest;

@RunWith(Suite.class)
@SuiteClasses({ AllChemicalTests.class,
    AllCommentTests.class,
    AllDrugTests.class,
    AllMiRnaTests.class,
    AllModelsTests.class,
    OverlayRestImplTest.class,
    ProjectRestImplTest.class })
public class AllProjectTests {

}
