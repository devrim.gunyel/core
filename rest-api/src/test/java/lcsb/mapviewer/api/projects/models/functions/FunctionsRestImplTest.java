package lcsb.mapviewer.api.projects.models.functions;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;

public class FunctionsRestImplTest extends RestTestFunctions {
  Logger logger = LogManager.getLogger(FunctionsRestImplTest.class);

  @Autowired
  FunctionsRestImpl functionRestImpl;

  @Test
  public void testExtractLambda() throws Exception {
    String lambda = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n<lambda>\n<bvar>\n<ci> x </ci>\n</bvar>\n<bvar>\n<ci> y </ci>\n</bvar>\n<apply>\n<plus/>\n<ci> x </ci>\n<ci> y </ci>\n<cn type=\"integer\"> 2 </cn>\n</apply>\n</lambda>\n\n</math>";
    String definition = functionRestImpl.extractLambda(lambda);
    assertNotNull(definition);
    assertFalse(definition.contains("lambda"));
    assertFalse(definition.contains("bvar"));
    assertTrue(definition.contains("math"));
    assertTrue(definition.contains("apply"));
  }

}
