package lcsb.mapviewer.api.projects.models.bioEntities.reactions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReactionsRestImplTest.class })
public class AllReactionsTests {

}
