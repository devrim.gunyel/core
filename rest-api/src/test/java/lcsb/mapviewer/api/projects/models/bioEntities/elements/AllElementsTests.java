package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ElementRestImplTest.class })
public class AllElementsTests {

}
