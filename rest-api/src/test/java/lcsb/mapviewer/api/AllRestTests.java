package lcsb.mapviewer.api;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.api.configuration.AllConfigurationTests;
import lcsb.mapviewer.api.convert.AllConvertTests;
import lcsb.mapviewer.api.files.AllFileTests;
import lcsb.mapviewer.api.genomics.AllGenomicsTests;
import lcsb.mapviewer.api.mesh.AllMeshTests;
import lcsb.mapviewer.api.projects.AllProjectTests;
import lcsb.mapviewer.api.users.AllUserTests;

@RunWith(Suite.class)
@SuiteClasses({ AllConfigurationTests.class,
    AllConvertTests.class,
    AllFileTests.class,
    AllGenomicsTests.class,
    AllMeshTests.class,
    AllProjectTests.class,
    AllUserTests.class,
})
public class AllRestTests {

}
