package lcsb.mapviewer.api.configuration;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.annotator.BioEntityField;

public class ConfigurationRestImplTest extends RestTestFunctions {

  @Autowired
  public ConfigurationRestImpl configurationRestImpl;
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ConfigurationRestImplTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAllParams() throws Exception {
    List<Map<String, Object>> list = configurationRestImpl.getAllValues();
    assertTrue(list.size() > 0);
  }

  @Test(expected = QueryException.class)
  public void testSetSmtpPortToInvalid() throws Exception {
    Map<String, Object> data = new HashMap<>();
    data.put("value", "not a number");
    configurationRestImpl.updateOption(ConfigurationElementType.EMAIL_SMTP_PORT.name(), data);
  }

  @Test
  public void testSetSmtpPortToValid() throws Exception {
    Map<String, Object> data = new HashMap<>();
    data.put("value", "255");
    Map<String, Object> result = configurationRestImpl.updateOption(
        ConfigurationElementType.EMAIL_SMTP_PORT.name(), data);
    assertNotNull(result);
  }

  @Test
  public void testImageFormats() throws Exception {
    List<Map<String, Object>> list = configurationRestImpl.getImageFormats();
    assertTrue(list.size() > 0);
  }

  @Test
  public void testModelFormats() throws Exception {
    List<Map<String, Object>> list = configurationRestImpl.getModelFormats();
    assertTrue(list.size() > 0);
  }

  @Test
  public void testGetElementTypes() throws Exception {
    Set<Map<String, String>> list = configurationRestImpl.getElementTypes();
    GenericProtein protein = new GenericProtein("id");
    boolean contains = false;
    for (Map<String, String> object : list) {
      contains |= (object.get("name").equals(protein.getStringType()));
    }
    assertTrue(contains);
  }

  @Test
  public void testGetReactionTypes() throws Exception {
    Set<Map<String, String>> list = configurationRestImpl.getReactionTypes();
    StateTransitionReaction reaction = new StateTransitionReaction();
    boolean contains = false;
    for (Map<String, String> object : list) {
      contains |= (object.get("name").equals(reaction.getStringType()));
    }
    assertTrue(contains);
  }

  @Test
  public void testGetMiriamTypes() throws Exception {
    Map<?, ?> list = configurationRestImpl.getMiriamTypes();
    assertNotNull(list.get(MiriamType.PUBMED.name()));
  }

  @Test
  public void testGetBioEntityFields() throws Exception {
    List<?> list = configurationRestImpl.getBioEntityFields();
    assertEquals(BioEntityField.values().length, list.size());
  }

}
