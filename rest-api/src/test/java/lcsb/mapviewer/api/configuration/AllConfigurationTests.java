package lcsb.mapviewer.api.configuration;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    ConfigurationRestImplTest.class })
public class AllConfigurationTests {

}
