package lcsb.mapviewer.api.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.*;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

import lcsb.mapviewer.api.ObjectExistsException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.model.user.annotator.*;
import lcsb.mapviewer.services.interfaces.IUserService;

public class UserRestImplTest extends RestTestFunctions {

  private final static String TEST_LOGIN = "test_user";

  @Autowired
  UserRestImpl userRestImpl;

  @Autowired
  IUserService userService;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = ObjectExistsException.class)
  public void testAddExistingUser() throws Exception {
    User user = new User();
    user.setLogin(TEST_LOGIN);
    user.setCryptedPassword("");
    userService.addUser(user);
    userRestImpl.addUser(TEST_LOGIN, new LinkedMultiValueMap<>());
  }

  @Test
  public void testUpdatePreferences() throws Exception {
    try {
      User user = new User();
      user.setCryptedPassword("");
      user.setLogin(TEST_LOGIN);
      userService.addUser(user);

      dbUtils.setAutoFlush(true);
      Map<String, Object> data = deserialize(userRestImpl.getUser(TEST_LOGIN, "preferences"));
      userRestImpl.updatePreferences(TEST_LOGIN, (Map<String, Object>) data.get("preferences"));
    } finally {
      dbUtils.setAutoFlush(false);
    }
  }

  private Map<String, Object> deserialize(Map<String, Object> data)
      throws JsonParseException, JsonMappingException, IOException {
    String body = new Gson().toJson(data);
    ObjectMapper mapper = new ObjectMapper();
    ObjectNode result = mapper.readValue(body, ObjectNode.class);
    return mapper.convertValue(result, Map.class);
  }

  @Test
  public void testGetUsers() throws Exception {
    Object response = userRestImpl.getUsers("");
    assertNotNull(response);
  }

  @Test
  public void testParseInputField() throws Exception {
    AnnotatorInputParameter object = userRestImpl.parseInputParameter(
        userRestImpl.prepareInputParameter(new AnnotatorInputParameter(BioEntityField.ABBREVIATION)));
    assertNotNull(object);
  }

  @Test
  public void testParseInputAnnotation() throws Exception {
    AnnotatorInputParameter object = userRestImpl.parseInputParameter(
        userRestImpl.prepareInputParameter(new AnnotatorInputParameter(MiriamType.BiGG_COMPARTMENT)));
    assertNotNull(object);
  }

  @Test
  public void testParseOutputField() throws Exception {
    AnnotatorParameter object = userRestImpl.parseOutputParameter(
        userRestImpl.prepareOutputParameter(new AnnotatorOutputParameter(BioEntityField.ABBREVIATION)));
    assertNotNull(object);
  }

  @Test
  public void testParseOutputAnnotation() throws Exception {
    AnnotatorParameter object = userRestImpl.parseOutputParameter(
        userRestImpl.prepareOutputParameter(new AnnotatorOutputParameter(MiriamType.BiGG_COMPARTMENT)));
    assertNotNull(object);
  }

  @Test
  public void testParseConfigAnnotation() throws Exception {
    AnnotatorParameter object = userRestImpl.parseConfigParameter(
        userRestImpl.prepareConfigParameter(
            new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, null)));
    assertNotNull(object);
  }

  @Test
  public void testPrepareGuiPreferencesSimple() throws Exception {
    Map<String, Object> response = userRestImpl.prepareGuiPreferences(new HashSet<>());
    assertNotNull(response);
  }

  @Test
  public void testPrepareGuiPreferencesWithOption() throws Exception {
    Set<UserGuiPreference> guiPreferences = new HashSet<>();
    UserGuiPreference option = new UserGuiPreference();
    option.setKey("TestKey");
    option.setValue("TestValue");
    guiPreferences.add(option);

    Map<String, Object> response = userRestImpl.prepareGuiPreferences(guiPreferences);
    assertNotNull(response);
    assertEquals("TestValue", response.get(option.getKey()));
  }

}
