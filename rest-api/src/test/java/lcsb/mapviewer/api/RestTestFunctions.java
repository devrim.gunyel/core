package lcsb.mapviewer.api;

import static org.mockito.ArgumentMatchers.anyString;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.services.interfaces.*;

@Transactional
@Rollback(true)
@ContextConfiguration(classes = SpringRestApiTestConfig.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class RestTestFunctions {

  protected static String ADMIN_BUILT_IN_LOGIN = "admin";

  private static Map<String, Model> models = new HashMap<>();
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  public double EPSILON = 1e-6;

  @Autowired
  protected PasswordEncoder passwordEncoder;

  @Autowired
  protected IUserService userService;

  @Autowired
  protected DbUtils dbUtils;

  protected String username;

  protected String adminToken;
  private Logger logger = LogManager.getLogger(RestTestFunctions.class);

  @Before
  public void generalSetUp() {
    dbUtils.setAutoFlush(false);
  }

  @After
  public void generatTearDown() throws IOException {
    File f = new File("map_images");
    if (f.exists()) {
      logger.info("Removing output test directory: " + f.getAbsolutePath());
      FileUtils.deleteDirectory(f);
    }

  }

  protected String readFile(String file) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader reader = new BufferedReader(new FileReader(file));
    try {
      String line = null;
      String ls = System.getProperty("line.separator");

      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
        stringBuilder.append(ls);
      }
    } finally {
      reader.close();
    }

    return stringBuilder.toString();
  }

  protected Node getNodeFromXmlString(String text) throws InvalidXmlSchemaException {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(text));
    return getXmlDocumentFromInputSource(is).getChildNodes().item(0);
  }

  protected Document getXmlDocumentFromFile(String fileName) throws InvalidXmlSchemaException, IOException {
    File file = new File(fileName);
    InputStream inputStream = new FileInputStream(file);
    Reader reader = null;
    try {
      reader = new InputStreamReader(inputStream, "UTF-8");
      InputSource is = new InputSource(reader);

      Document result = getXmlDocumentFromInputSource(is);
      inputStream.close();
      return result;
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  protected Document getXmlDocumentFromInputSource(InputSource stream) throws InvalidXmlSchemaException {
    DocumentBuilder db;
    try {
      db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser");
    }
    Document doc = null;
    try {
      doc = db.parse(stream);
    } catch (SAXException e) {
      logger.error(e);
    } catch (IOException e) {
      logger.error(e);
    }
    return doc;
  }

  protected Model getModelForFile(String fileName, boolean fromCache) throws Exception {
    Model result = null;
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      result = getModelForFile(fileName);
    } else {
      result = RestTestFunctions.models.get(fileName);
      if (result == null) {
        logger.debug("File to cache: " + fileName);

        result = getModelForFile(fileName);
        RestTestFunctions.models.put(fileName, result);
      }
    }
    return result;
  }

  private Model getModelForFile(String fileName) throws InvalidInputDataExecption, IOException, ConverterException {
    if (fileName.endsWith("zip")) {
      ComplexZipConverter parser = new ComplexZipConverter(CellDesignerXmlParser.class);
      ComplexZipConverterParams complexParams;
      complexParams = new ComplexZipConverterParams().zipFile(fileName);
      ZipEntryFile entry1 = new ModelZipEntryFile("main.xml", "main", true, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry2 = new ModelZipEntryFile("submaps/s1.xml", "s1", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry3 = new ModelZipEntryFile("submaps/s2.xml", "s2", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry4 = new ModelZipEntryFile("submaps/s3.xml", "s3", false, false, SubmodelType.UNKNOWN);
      ZipEntryFile entry5 = new ModelZipEntryFile("submaps/mapping.xml", "mapping", false, true, SubmodelType.UNKNOWN);
      complexParams.entry(entry1);
      complexParams.entry(entry2);
      complexParams.entry(entry3);
      complexParams.entry(entry4);
      complexParams.entry(entry5);

      Model model = parser.createModel(complexParams);
      model.setTileSize(256);
      for (ModelSubmodelConnection connection : model.getSubmodelConnections()) {
        connection.getSubmodel().setTileSize(256);
      }
      model.setProject(new Project());
      return model;

    } else {
      Model model = new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
      model.setTileSize(256);
      model.setProject(new Project());
      return model;
    }
  }

  protected String createTmpFileName() {
    try {
      File f = File.createTempFile("prefix", ".txt");
      String filename = f.getName();
      f.delete();
      return filename;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  protected String nodeToString(Node node) {
    return nodeToString(node, false);
  }

  protected String nodeToString(Node node, boolean includeHeadNode) {
    if (node == null)
      return null;
    StringWriter sw = new StringWriter();
    try {
      Transformer t = TransformerFactory.newInstance().newTransformer();
      t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      t.setOutputProperty(OutputKeys.INDENT, "yes");
      t.setOutputProperty(OutputKeys.METHOD, "xml");

      NodeList list = node.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        Node element = list.item(i);
        t.transform(new DOMSource(element), new StreamResult(sw));
      }
    } catch (TransformerException te) {
      logger.debug("nodeToString Transformer Exception");
    }
    if (includeHeadNode) {
      return "<" + node.getNodeName() + ">" + sw.toString() + "</" + node.getNodeName() + ">";
    }
    return sw.toString();
  }

  protected boolean equalFiles(String fileA, String fileB) throws IOException {
    int BLOCK_SIZE = 65536;
    FileInputStream inputStreamA = new FileInputStream(fileA);
    FileInputStream inputStreamB = new FileInputStream(fileB);
    // vary BLOCK_SIZE to suit yourself.
    // it should probably a factor or multiple of the size of a disk
    // sector/cluster.
    // Note that your max heap size may need to be adjused
    // if you have a very big block size or lots of these comparators.

    // assume inputStreamA and inputStreamB are streams from your two files.
    byte[] streamABlock = new byte[BLOCK_SIZE];
    byte[] streamBBlock = new byte[BLOCK_SIZE];
    boolean match = true;
    int bytesReadA = 0;
    int bytesReadB = 0;
    do {
      bytesReadA = inputStreamA.read(streamABlock);
      bytesReadB = inputStreamB.read(streamBBlock);
      match = ((bytesReadA == bytesReadB) && Arrays.equals(streamABlock, streamBBlock));
    } while (match && (bytesReadA > -1));
    inputStreamA.close();
    inputStreamB.close();
    return match;
  }

  public File createTempDirectory() throws IOException {
    final File temp;

    temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

    if (!(temp.delete())) {
      throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
    }

    if (!(temp.mkdir())) {
      throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
    }

    return (temp);
  }

  protected String getWebpage(String accessUrl) throws IOException {
    String inputLine;
    StringBuilder tmp = new StringBuilder();
    URL url = new URL(accessUrl);
    URLConnection urlConn = url.openConnection();
    BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

    while ((inputLine = in.readLine()) != null) {
      tmp.append(inputLine);
    }
    in.close();
    return tmp.toString();
  }

  protected IProjectService createProjectMockServiceForModel(Model model) {
    IProjectService mockModelService = Mockito.mock(IProjectService.class);
    Mockito.when(mockModelService.getProjectByProjectId(anyString())).thenReturn(model.getProject());
    return mockModelService;
  }

  protected IModelService createModelMockServiceForModel(Model model) {
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString())).thenReturn(model);
    return mockModelService;
  }

}
