package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.w3c.dom.Element;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.DataNode;

public class DataNodeParserTest extends WikipathwaysTestFunctions {
  Logger logger = LogManager.getLogger(DataNodeParserTest.class);
  DataNodeParser parser ;

  @Before
  public void setUp() throws Exception {
    parser = new DataNodeParser("mapName");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseGene() throws Exception {
    Element node = fileToNode("testFiles/elements/gene.xml");
    DataNode dataNode = parser.parse(node);

    assertNotNull(dataNode);
    assertEquals("GeneProduct", dataNode.getTextLabel());
    assertEquals("ab30b", dataNode.getGraphId());
    assertEquals(1, dataNode.getBiopaxReference().size());
    assertEquals("aea", dataNode.getBiopaxReference().get(0));
    assertEquals(1, dataNode.getReferences().size());
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"), dataNode.getReferences().get(0));

    assertEquals((Integer) 32768, dataNode.getzOrder());
    assertEquals((Double) 10.0, dataNode.getFontSize());
    assertEquals("Middle", dataNode.getvAlign());
    assertEquals("GeneProduct", dataNode.getType());
    assertTrue(dataNode.getComments().contains("comment 1"));
    assertTrue(dataNode.getComments().contains("Type your comment here"));

    assertEquals(0, getWarnings().size());
  }

}
