package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.*;
import org.w3c.dom.Element;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Label;

public class LabelParserTest extends WikipathwaysTestFunctions {
  LabelParser parser = new LabelParser("mapName");

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseLabel() throws Exception {
    Element node = fileToNode("testFiles/elements/label.xml");
    Label label = parser.parse(node);
    assertNotNull(label);

    assertEquals("Label 2", label.getTextLabel());
    assertEquals("edf06", label.getGraphId());
    assertEquals(2, label.getBiopaxReference().size());
    assertEquals("b38", label.getBiopaxReference().get(1));

    assertEquals((Integer) 28672, label.getzOrder());
    assertEquals((Double) 10.0, label.getFontSize());
    assertEquals("Middle", label.getvAlign());
    assertTrue(label.getComments().contains("comment 2"));
    assertEquals(Color.WHITE, label.getFillColor());
    assertEquals(Color.BLACK, label.getColor());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseLabeHrefl() throws Exception {
    Element node = fileToNode("testFiles/elements/label_href.xml");
    Label label = parser.parse(node);
    assertNotNull(label);

    assertEquals(1, label.getReferences().size());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseGraphics() throws Exception {
    Element node = fileToNode("testFiles/elements/label_graphics.xml");

    Label label = new Label("test", "mapName");
    parser.parseGraphics(node, label);
    assertNotNull(label);

    assertEquals(0, getWarnings().size());
  }

}
