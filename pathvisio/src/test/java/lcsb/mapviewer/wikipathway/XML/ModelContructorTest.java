package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.*;

import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ModelContructorTest extends WikipathwaysTestFunctions {
  Logger logger = LogManager.getLogger(ModelContructorTest.class);

  ModelComparator comparator = new ModelComparator();
  ModelContructor mc ;

  private int elementCounter = 0;

  @Before
  public void setUp() throws Exception {
    mc = new ModelContructor("mapName");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testZIndexForElement() throws Exception {
    Model model = createEmptyModel();
    model.addElement(createProtein());
    ModelToGPML parser = new ModelToGPML(model.getName());
    String xml = parser.getGPML(model);
    assertNotNull(xml);

    Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
    assertEquals(0, comparator.compare(model2, model));
  }

  private Model createEmptyModel() {
    Model result = new ModelFullIndexed(null);
    result.setWidth(640);
    result.setHeight(480);
    return result;
  }

  private GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + (elementCounter++));
    result.setX(100);
    result.setY(20);
    result.setWidth(100);
    result.setHeight(30);
    result.setZ(7890);
    return result;
  }

  @Test
  public void testShouldRotate90degreesWithNoAsAnswer() {
    assertFalse(mc.shouldRotate90degrees(0.0));
    assertFalse(mc.shouldRotate90degrees(Math.PI));
    assertFalse(mc.shouldRotate90degrees(15 * Math.PI));
    assertFalse(mc.shouldRotate90degrees(-Math.PI));
    assertFalse(mc.shouldRotate90degrees((Double)null));
  }

  @Test
  public void testShouldRotate90degrees() {
    assertTrue(mc.shouldRotate90degrees(Math.PI / 2));
    assertTrue(mc.shouldRotate90degrees(15 * Math.PI / 2));
    assertTrue(mc.shouldRotate90degrees(-Math.PI / 2));
  }

  @Test
  public void testShouldRotate90degreesWithWarnings() {
    assertFalse(mc.shouldRotate90degrees(0.0 + 0.5));
    assertEquals(1, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(Math.PI + 0.5));
    assertEquals(2, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(15 * Math.PI + 0.5));
    assertEquals(3, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(-Math.PI + 0.5));
    assertEquals(4, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(Math.PI / 2 + 0.5));
    assertEquals(5, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(15 * Math.PI / 2 + 0.5));
    assertEquals(6, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(-Math.PI / 2 + 0.5));
    assertEquals(7, super.getWarnings().size());
  }

  @Test
  public void testRotate90degrees() {
    Rectangle2D result = mc.rotate90degrees(new Rectangle2D.Double(10, 20, 30, 40));
    assertEquals(5, result.getX(), Configuration.EPSILON);
    assertEquals(25, result.getY(), Configuration.EPSILON);
    assertEquals(40, result.getWidth(), Configuration.EPSILON);
    assertEquals(30, result.getHeight(), Configuration.EPSILON);
  }

}
