package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ReferenceParserTest extends WikipathwaysTestFunctions {
  Logger logger = LogManager.getLogger(ReferenceParserTest.class);

  ReferenceParser mc = new ReferenceParser("mapName");

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCreateHmdbMiriam() {
    MiriamData md = mc.createMiriamData("HMDB03791", "HMDB");
    assertNotNull(md);
  }

  @Test
  public void testCreateWormBaseMiriam() {
    MiriamData md = mc.createMiriamData("WormBase", "WormBase");
    assertNotNull(md);
  }

  @Test
  public void testSgdMiriam() {
    MiriamData md = mc.createMiriamData("S000028457", "SGD");
    assertNotNull(md);
  }

  @Test
  public void testWikidataMiriam() {
    MiriamData md = mc.createMiriamData("Q15623825", "Wikidata");
    assertNotNull(md);
  }

  @Test
  public void testCreateEnsemblPlantMiriam() {
    MiriamData md = mc.createMiriamData("AT3G18550", "Ensembl Plants");
    assertNotNull(md);
  }

  @Test
  public void testCreateCasMiriam() {
    MiriamData md = mc.createMiriamData("657-24-9", "CAS");
    assertNotNull(md);
  }

  @Test
  public void testCreateGenBankMiriam() {
    MiriamData md = mc.createMiriamData("123", "GenBank");
    assertNotNull(md);
  }

  @Test
  public void testCreatePubchemMiriam() {
    MiriamData md = mc.createMiriamData("39484", "pubchem.compound");
    assertNotNull(md);
  }

  @Test
  public void testCreateWikipediaMiriam() {
    MiriamData md = mc.createMiriamData("Reactive_oxygen_species", "Wikipedia");
    assertNotNull(md);
  }

  @Test
  public void testCreateKeggOrthologyMiriam() throws Exception {
    MiriamData md = mc.createMiriamData("K00114", "Kegg ortholog");
    assertNotNull(md);
  }

  @Test
  public void testChemSpiderMiriam() throws Exception {
    MiriamData md = mc.createMiriamData("110354", "Chemspider");
    assertNotNull(md);
  }

  @Test
  public void testPfamMiriam() throws Exception {
    MiriamData md = mc.createMiriamData("PF00071", "Pfam");
    assertNotNull(md);
  }

  @Test
  public void testMiRBaseMiriam() throws Exception {
    MiriamData md = mc.createMiriamData("MI0000750", "miRBase");
    assertNotNull(md);
  }

  @Test
  public void testParse() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/elements/xref.xml");
    MiriamData md = mc.parse((Element) node.getFirstChild());
    assertNotNull(md);
    assertEquals(md, new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testToXml() throws Exception {
    MiriamData md = new MiriamData(MiriamType.CHEBI, "CHEBI:123");
    String xml = mc.toXml(md);

    Node node = super.getNodeFromXmlString(xml);
    MiriamData md2 = mc.parse((Element) node);
    assertNotNull(md);
    assertEquals(md, md2);
    assertEquals(0, getWarnings().size());
  }
}
