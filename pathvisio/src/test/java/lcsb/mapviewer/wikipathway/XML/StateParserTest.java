package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Test;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.State;

public class StateParserTest extends WikipathwaysTestFunctions {
  StateParser parser = new StateParser("mapName");

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseState() throws Exception {
    Element node = fileToNode("testFiles/elements/state.xml");
    State state = parser.parse(node);
    assertNotNull(state);

    assertEquals(ModificationState.PHOSPHORYLATED, state.getType());
    assertEquals("e7637", state.getGraphId());
    assertEquals(1.0, state.getRelX(), Configuration.EPSILON);
    assertEquals(-0.9978969505783312, state.getRelY(), Configuration.EPSILON);
    assertEquals(15.0, state.getWidth(), Configuration.EPSILON);
    assertEquals(15.0, state.getHeight(), Configuration.EPSILON);
    assertEquals("Oval", state.getShape());
    assertEquals(0, getWarnings().size());
  }

}
