package lcsb.mapviewer.wikipathway;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.wikipathway.XML.AllXmlTests;
import lcsb.mapviewer.wikipathway.model.AllModelTests;

@RunWith(Suite.class)
@SuiteClasses({ AllModelTests.class,
    AllXmlTests.class,
    ComplexReactionToModelTest.class,
    GPMLToModelTest.class,
    ReactionElbowsTest.class,
    ReactionGpmlInputToModelTest.class,
    ReactionGpmlOutputToModelTest.class,
    ReactionGpmlToModelTest.class,
})
public class AllWikipathwaysTests {

}
