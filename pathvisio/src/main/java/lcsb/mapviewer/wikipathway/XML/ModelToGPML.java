package lcsb.mapviewer.wikipathway.XML;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.wikipathway.utils.Geo;

/**
 * Class contains methods for ModelToGPML conversion.
 * 
 * @author Jan Badura
 * 
 */
public class ModelToGPML {

  /**
   * Maximum distance between point and line. ???
   */
  private static final double DIS_FOR_LINE = 3.0;
  /**
   * This value define margin of the group border. Rectangle border will be
   * resized by this value in every direction.
   */
  private static final int GROUP_RECTANGLE_BORDER_MARGIN = 8;
  /**
   * First id value used for generating identifiers during conversion.
   */
  private static final int INITIAL_ID_VALUE = 10000;
  /**
   * Maximum distance between point and rectangle. ???
   */
  private static final double DIS_FOR_REP = 9.0;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ModelToGPML.class);
  /**
   * Counter used for generating identifiers if identifiers aren't provided by the
   * GPML model.
   */
  private int idCounter = INITIAL_ID_VALUE;
  /**
   * Parser used to convert from/to xml {@link MiriamData} annotations.
   */
  private ReferenceParser referenceParser;

  /**
   * Parser used to convert from/to xml (in BioPAX format) {@link MiriamData}
   * annotations.
   */
  private BiopaxParser biopaxParser;

  private ColorParser colorParser = new ColorParser();

  public ModelToGPML(String mapName) {
    referenceParser = new ReferenceParser(mapName);
  }

  /**
   * Returns new unique identifier for the model.
   * 
   * @return new unique identifier for the model
   */
  private String getNewId() {
    idCounter++;
    return "id" + idCounter;
  }

  /**
   * This function returns gpml Species type based on class. If it can not
   * recognize one of (Rna,GeneProduct,Protein,Metabolite) it will throw
   * {@link InvalidArgumentException}.
   * 
   * @param species
   *          species for which gpml type will be returned
   * @return String - type
   */
  private String getType(Species species) {
    String res = "";
    if (species instanceof AntisenseRna) {
      res = "Rna";
    } else if (species instanceof Rna) {
      res = "Rna";
    } else if (species instanceof Gene) {
      res = "GeneProduct";
    } else if (species instanceof Protein) {
      res = "Protein";
    } else if (species instanceof SimpleMolecule) {
      res = "Metabolite";
    } else if (species instanceof Ion) {
      res = "Metabolite";
    } else if (species instanceof Drug) {
      res = "Metabolite";
    } else if (species instanceof Phenotype) {
      res = "Pathway";
    } else if (species instanceof Unknown) {
      res = "Unknown";
    } else if (species instanceof Degraded) {
      res = "Unknown";
    } else {
      throw new InvalidArgumentException("Unknown class type: " + species.getClass());
    }

    return res;
  }

  /**
   * Since in pathvisio it is impossible to set the size of group rectangle this
   * function calculates new rectangle for complex based on contents of this
   * complex.
   * 
   * @param alias
   *          alias for which rectangle border is calculated
   * @return rectangle border for alias
   */
  private Rectangle2D getRectangle(Element alias) {
    Rectangle2D res = null;

    if (alias instanceof Complex) {
      Complex ca = (Complex) alias;
      if (ca.getElements().size() == 0) {
        res = (Rectangle2D) ca.getBorder().clone();
      } else {
        res = getRectangle(ca.getElements().get(0));
        for (Element al : ca.getElements()) {
          Rectangle2D.union(res, getRectangle(al), res);
        }
        res = new Rectangle2D.Double(
            res.getX() - GROUP_RECTANGLE_BORDER_MARGIN, res.getY() - GROUP_RECTANGLE_BORDER_MARGIN,
            res.getWidth() + 2 * GROUP_RECTANGLE_BORDER_MARGIN,
            res.getHeight() + 2 * GROUP_RECTANGLE_BORDER_MARGIN);
      }
    } else if (alias instanceof Species) {
      res = (Rectangle2D) alias.getBorder().clone();
    }
    return res;
  }

  /**
   * This function calculates {@link PolylineData} based on start {@link Reactant}
   * and end {@link Product}. If end or start points are to far away from their
   * rectangles new points are added.
   * 
   * @param start
   *          reactant from which polyline starts
   * @param end
   *          product where polyline ends
   * @return {@link PolylineData} between start and end
   */
  private PolylineData getPolyline(Reactant start, Product end) {
    PolylineData res = new PolylineData();

    NodeOperator and = start.getNodeOperatorForInput();
    // NodeOperator split = end.getNodeOperatorForOutput();

    PolylineData startLine = start.getLine();
    Point2D ps = startLine.getBeginPoint();
    Rectangle2D rec1 = getRectangle(start.getElement());
    double dis1 = Geo.distance(ps, rec1);

    if (dis1 > DIS_FOR_REP) {
      res.addPoint(Geo.closestPointOnRectangle(rec1, ps));
    }
    for (Point2D p2d : startLine.getPoints()) {
      res.addPoint(p2d);
    }
    if (and != null) {
      for (Point2D p2d : and.getLine().getPoints()) {
        res.addPoint(p2d);
      }
    }

    PolylineData endLine = end.getLine();
    Point2D pe = endLine.getEndPoint();
    Rectangle2D rec2 = getRectangle(end.getElement());
    double dis2 = Geo.distance(pe, rec2);

    for (Point2D p2d : endLine.getPoints()) {
      res.addPoint(p2d);
    }
    if (dis2 > DIS_FOR_REP) {
      res.addPoint(Geo.closestPointOnRectangle(rec2, pe));
    }
    return Geo.removeRedundantPoints(res);
  }

  /**
   * This function calculates PolylineData based on ReactionNode.
   * 
   * @param rn
   *          reaction node for which polyline is calculated
   * @param mainLine
   *          main line to which polyline is attached
   * @return polyline for {@link ReactionNode}
   */
  private PolylineData getPolyline(ReactionNode rn, PolylineData mainLine) {
    PolylineData res = new PolylineData();

    PolylineData line = rn.getLine();
    Point2D ps = line.getBeginPoint();
    Point2D pe = line.getEndPoint();
    Rectangle2D rec = getRectangle(rn.getElement());

    if (rn instanceof Reactant || rn instanceof Modifier) {
      double dis = Geo.distance(ps, rec);
      double dis2 = Geo.distanceFromPolyline(pe, mainLine);
      if (dis > DIS_FOR_REP) {
        res.addPoint(Geo.closestPointOnRectangle(rec, ps));
      }
      for (Point2D p2d : line.getPoints()) {
        res.addPoint(p2d);
      }
      if (dis2 > DIS_FOR_LINE) {
        Point2D tmpPoint = Geo.closestPointOnPolyline(mainLine, pe);
        if (tmpPoint != null) {
          res.addPoint(tmpPoint);
        }
      }
    } else if (rn instanceof Product) {
      double dis = Geo.distance(pe, rec);
      double dis2 = Geo.distanceFromPolyline(ps, mainLine);
      if (dis2 > DIS_FOR_LINE) {
        Point2D tmpPoint = Geo.closestPointOnPolyline(mainLine, ps);
        if (tmpPoint != null) {
          res.addPoint(tmpPoint);
        }
      }
      for (Point2D p2d : line.getPoints()) {
        res.addPoint(p2d);
      }
      if (dis > DIS_FOR_REP) {
        res.addPoint(Geo.closestPointOnRectangle(rec, pe));
      }
    }
    return Geo.removeRedundantPoints(res);
  }

  /**
   * This function returns all species names that are in given Compartment.
   * 
   * @param ca
   *          compartment alias
   * @return string with list of species names in a compartment
   */
  private String getAllNames(Compartment ca) {
    StringBuilder res = new StringBuilder("");
    for (Element a : ca.getAllSubElements()) {
      if (a instanceof Species) {
        Species sp = (Species) a;
        res.append(sp.getName() + ";");
      }
    }
    return res.toString();
  }

  /**
   * This function transform Compartments into Shapes (Oval or Rectangle) from
   * PathVisio.
   * 
   * @param model
   *          model where compartments are placed
   * @return String that encodes all compartments in GPML format
   */
  private String getComparments(Model model) {
    StringBuilder comparments = new StringBuilder("");
    for (Compartment ca : model.getCompartments()) {

      double x = ca.getCenterX(), y = ca.getCenterY(), h = ca.getHeight(), w = ca.getWidth();
      String shape;
      if (ca instanceof OvalCompartment) {
        shape = "Oval";
      } else {
        shape = "Rectangle";
      }

      comparments.append("  <Shape TextLabel=\"" + ca.getName() + "\" GraphId=\"" + ca.getElementId() + "\">\n");
      comparments.append("    <Comment>" + getAllNames(ca) + "</Comment>\n");
      comparments.append("    <Attribute Key=\"org.pathvisio.CellularComponentProperty\" Value=\"Cell\" />\n");
      comparments.append("    <Attribute Key=\"org.pathvisio.DoubleLineProperty\" Value=\"Double\" />\n");
      comparments.append(
          "    <Graphics CenterX=\"" + x + "\" "
              + "CenterY=\"" + y + "\" "
              + "Width=\"" + w + "\" "
              + "Height=\"" + h + "\" "
              + "ZOrder=\"" + ca.getZ() + "\" "
              + "FontSize=\"" + ca.getFontSize() + "\" "
              + "Valign=\"Bottom\" "
              + "ShapeType=\"" + shape + "\" "
              + "LineThickness=\"3.0\" "
              + "Color=\"" + colorToString(ca.getFillColor()) + "\" "
              + "Rotation=\"0.0\" />\n");
      comparments.append("  </Shape>\n");
    }
    return comparments.toString();
  }

  /**
   * This function creates Interaction for other product, reactant or modifier
   * from main reaction. This is support function for getInteractions(Model
   * model).
   * 
   * @param rn
   *          object representing reactant/product/modifier
   * @param anchId
   *          identifier of the anchor where it will be connected
   * @param anchors
   *          string builder where anchors are stored (it will be modified to add
   *          anchor for this element)
   * @param mainLine
   *          line used for connecting this element
   * @return string representing connection of this element to reaction
   * @throws ConverterException
   *           thrown when there is a problem with conversion
   */
  private String getInteractionForAnchor(ReactionNode rn, String anchId, StringBuilder anchors, PolylineData mainLine)
      throws ConverterException {
    StringBuilder interaction = new StringBuilder("");

    PolylineData line = getPolyline(rn, mainLine);
    Point2D ps = line.getBeginPoint();
    Point2D pe = line.getEndPoint();
    double anchorPosition = -1;

    interaction.append("  <Interaction GraphId=\"" + getNewId() + "\">\n");
    interaction.append("    <Attribute Key=\"org.pathvisio.core.ds\" Value=\"\"/>\n");
    interaction.append("    <Attribute Key=\"org.pathvisio.core.id\" Value=\"\"/>\n");
    interaction.append("    <Graphics "
        + "ConnectorType=\"Segmented\" "
        + "ZOrder=\"" + rn.getReaction().getZ() + "\" "
        + "LineThickness=\"" + rn.getLine().getWidth() + "\">\n");

    if (rn instanceof Reactant) {
      for (Point2D p2d : line.getPoints()) {
        if (p2d.equals(ps)) {
          interaction.append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\""
              + rn.getElement().getElementId() + "\"/>\n");
        } else if (p2d.equals(pe)) {
          interaction
              .append("      <Point X=\"" + pe.getX() + "\" Y=\"" + pe.getY() + "\" GraphRef=\"" + anchId + "\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX() + "\" Y=\"" + p2d.getY() + "\"/>\n");
        }
      }
    } else if (rn instanceof Product) {
      for (Point2D p2d : line.getPoints()) {
        if (p2d.equals(ps)) {
          interaction
              .append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\"" + anchId + "\"/>\n");
        } else if (p2d.equals(pe)) {
          interaction.append(
              "      <Point X=\"" + pe.getX() + "\" Y=\"" + pe.getY() + "\" GraphRef=\""
                  + rn.getElement().getElementId()
                  + "\" ArrowHead=\"mim-conversion\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX() + "\" Y=\"" + p2d.getY() + "\"/>\n");
        }
      }
    } else if (rn instanceof Modifier) {
      for (Point2D p2d : line.getPoints()) {
        if (p2d.equals(ps)) {
          interaction.append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\""
              + rn.getElement().getElementId() + "\"/>\n");
        } else if (p2d.equals(pe)) {
          interaction.append("      <Point X=\"" + pe.getX() + "\" Y=\"" + pe.getY() + "\" GraphRef=\"" + anchId
              + "\" ArrowHead=\"mim-catalysis\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX() + "\" Y=\"" + p2d.getY() + "\"/>\n");
        }
      }
    }
    if (rn instanceof Reactant) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, pe);
    } else if (rn instanceof Product) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, ps);
    } else if (rn instanceof Modifier) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, pe);
    } else {
      throw new InvalidArgumentException("Invalid class type: " + rn.getClass());
    }
    anchors.append("      <Anchor Position=\"" + anchorPosition + "\" Shape=\"None\" GraphId=\"" + anchId + "\"/>\n");
    interaction.append("    </Graphics>\n");
    interaction.append(referenceParser.toXml((MiriamData) null));
    interaction.append("  </Interaction>\n");

    return interaction.toString();
  }

  /**
   * This function encode SpeciesAliases into DataNodes from GPML format. Empty
   * complexes are also transformed into DataNodes.
   * 
   * @param model
   *          model where aliases are placed
   * @return string representing {@link lcsb.mapviewer.wikipathway.model.DataNode
   *         data nodes}
   * @throws ConverterException
   *           thrown when there is a problem with conversion
   */
  protected String getDataNodes(Model model) throws ConverterException {
    StringBuilder dataNodes = new StringBuilder("");

    for (Species species : model.getNotComplexSpeciesList()) {
      if (!(species instanceof Complex)) {
        dataNodes.append(
            "  <DataNode TextLabel=\"" + species.getName() + "\" GraphId=\"" + species.getElementId() + "\" Type=\""
                + getType(species) + "\"");
        if (species.getComplex() != null) {
          dataNodes.append(" GroupRef=\"" + species.getComplex().getElementId() + "\"");
        }
        dataNodes.append(">\n");

        dataNodes.append("    <Comment>" + species.getNotes() + "</Comment>\n");
        dataNodes.append(biopaxParser.toReferenceXml(species.getMiriamData()));

        Rectangle2D rec = species.getBorder();

        dataNodes.append(
            "    <Graphics CenterX=\"" + rec.getCenterX() + "\" " +
                "CenterY=\"" + rec.getCenterY() + "\" " +
                "Width=\"" + rec.getWidth() + "\" " +
                "Height=\"" + rec.getHeight() + "\" " +
                "ZOrder=\"" + species.getZ() + "\" " +
                "FontSize=\"" + species.getFontSize() + "\" " +
                "Valign=\"Middle\" " +
                "Color=\"" + colorToString(species.getFontColor()) + "\" " +
                "FillColor=\"" + colorToString(species.getFillColor()) + "\"/>\n");

        dataNodes.append(referenceParser.toXml(species.getMiriamData()));
        dataNodes.append("  </DataNode>\n");
      }
    }

    /** Special Case for empty Complexes **/
    for (Complex ca : model.getComplexList()) {
      if (ca.getElements().size() == 0) {
        dataNodes.append(
            "  <DataNode TextLabel=\"" + ca.getName() + "\" GraphId=\"" + ca.getElementId() + "\" Type=\"Complex\"");
        if (ca.getComplex() != null) {
          dataNodes.append(" GroupRef=\"" + ca.getComplex().getElementId() + "\"");
        }
        dataNodes.append(">\n");

        dataNodes.append(biopaxParser.toReferenceXml(ca.getMiriamData()));
        Rectangle2D rec = ca.getBorder();

        dataNodes.append(
            "    <Graphics CenterX=\"" + rec.getCenterX() + "\" " +
                "CenterY=\"" + rec.getCenterY() + "\" " +
                "Width=\"" + rec.getWidth() + "\" " +
                "Height=\"" + rec.getHeight() + "\" " +
                "ZOrder=\"" + ca.getZ() + "\" " +
                "FontSize=\"" + ca.getFontSize() + "\" " +
                "Valign=\"Middle\" " +
                "Color=\"" + colorToString(ca.getFontColor()) + "\" " +
                "FillColor=\"" + colorToString(ca.getFillColor()) + "\" " +
                "LineStyle=\"Broken\"/>\n");

        dataNodes.append(referenceParser.toXml(ca.getMiriamData()));
        dataNodes.append("  </DataNode>\n");
      }
    }

    return dataNodes.toString();
  }

  /**
   * This function encode ComplexAliases into Groups from GPML format.
   * 
   * @param model
   *          model where aliases are placed
   * @return string representing groups in GPML format
   */
  protected String getGroups(Model model) {
    StringBuilder groups = new StringBuilder("");

    for (Complex ca : model.getComplexList()) {
      if (ca.getElements().size() > 0) {
        groups.append("  <Group GroupId=\"" + ca.getElementId() + "\" GraphId=\"" + ca.getElementId() + "\"");
        if (ca.getComplex() != null) {
          groups.append(" GroupRef=\"" + ca.getComplex().getElementId() + "\"");
        }
        groups.append("/>\n");
      }
    }
    return groups.toString();
  }

  /**
   * This function encode Reactions into Interactions from GPML format.
   * 
   * @param model
   *          model where reactions are placed
   * @return string with interactions in GPML format
   * @throws ConverterException
   *           thrown when there is a problem with conversion
   */
  protected String getInteractions(Model model) throws ConverterException {
    StringBuilder interactions = new StringBuilder("");
    for (Reaction reaction : model.getReactions()) {

      StringBuilder anchors = new StringBuilder("");
      StringBuilder tmp = new StringBuilder("");

      interactions.append("  <Interaction GraphId=\"" + reaction.getIdReaction() + "\">\n");
      interactions.append(biopaxParser.toReferenceXml(reaction.getMiriamData()));
      interactions.append("    <Graphics "
          + "ConnectorType=\"Segmented\" "
          + "ZOrder=\"" + reaction.getZ() + "\" "
          + "LineThickness=\"" + reaction.getLine().getWidth() + "\">\n");

      /** Start and End **/
      Reactant start = reaction.getReactants().get(0);
      Product end = reaction.getProducts().get(0);

      PolylineData line = getPolyline(start, end);
      Point2D ps = line.getBeginPoint();
      Point2D pe = line.getEndPoint();
      String sid = start.getElement().getElementId();
      String eid = end.getElement().getElementId();

      for (Point2D p2d : line.getPoints()) {
        if (p2d.equals(ps)) {
          interactions
              .append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\"" + sid + "\"/>\n");
        } else if (p2d.equals(pe)) {
          interactions.append("      <Point X=\"" + pe.getX() + "\" Y=\"" + pe.getY() + "\" GraphRef=\"" + eid
              + "\" ArrowHead=\"mim-conversion\"/>\n");
        } else {
          interactions.append("      <Point X=\"" + p2d.getX() + "\" Y=\"" + p2d.getY() + "\"/>\n");
        }
      }
      /** Reactants **/
      boolean first = true;
      for (Reactant reactant : reaction.getReactants()) {
        if (first) {
          first = false;
        } else {
          tmp.append(getInteractionForAnchor(reactant, getNewId(), anchors, line));
        }
      }
      /** Products **/
      first = true;
      for (Product product : reaction.getProducts()) {
        if (first) {
          first = false;
        } else {
          tmp.append(getInteractionForAnchor(product, getNewId(), anchors, line));
        }
      }
      /** Modifiers **/
      for (Modifier modifier : reaction.getModifiers()) {
        tmp.append(getInteractionForAnchor(modifier, getNewId(), anchors, line));
      }

      interactions.append(anchors.toString());
      interactions.append("    </Graphics>\n");
      interactions.append(referenceParser.toXml(reaction.getMiriamData()));
      interactions.append("  </Interaction>\n");
      interactions.append(tmp.toString());
    }

    return interactions.toString();
  }

  /**
   * This function returns Model in GPML format.
   * 
   * @param model
   *          model to transform
   * @return string in GPML format representing model
   * @throws ConverterException
   *           thrown when there is a problem with conversion
   */
  public String getGPML(Model model) throws ConverterException {

    biopaxParser = new BiopaxParser(model.getName());

    StringBuilder gpml = new StringBuilder("");
    gpml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    gpml.append("<Pathway xmlns=\"http://pathvisio.org/GPML/2013a\" Name=\"Generated\">\n");
    gpml.append("  <Graphics BoardWidth=\"" + model.getWidth() + "\" BoardHeight=\"" + model.getHeight() + "\"/>\n");

    gpml.append(getDataNodes(model));
    String groups = getGroups(model);
    gpml.append(getInteractions(model));
    gpml.append(getComparments(model));
    gpml.append(groups);
    Set<MiriamData> set = new HashSet<>();
    for (BioEntity element : model.getBioEntities()) {
      set.addAll(element.getMiriamData());
    }

    // the order here is important...
    gpml.append("  <InfoBox CenterX=\"0.0\" CenterY=\"0.0\"/>\n");
    gpml.append(biopaxParser.toXml(set));

    gpml.append("</Pathway>");

    return gpml.toString();
  }

  private String colorToString(Color color) {
    return colorParser.colorToHtml(color).substring(1);
  }
}
