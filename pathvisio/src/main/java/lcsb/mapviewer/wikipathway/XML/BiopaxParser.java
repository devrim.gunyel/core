package lcsb.mapviewer.wikipathway.XML;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.model.biopax.*;

/**
 * Parser of Biopax data from the GPML file.
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxParser {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(BiopaxParser.class);

  /**
   * Hash used for next {@link BiopaxPublication} processed by parser.
   */
  private Integer hash = 1;

  /**
   * Mapping between {@link MiriamData} and hash value.
   */
  private Map<MiriamData, String> miriamHash = new HashMap<>();
  
  private String mapName;

  public BiopaxParser(String mapName) {
    this.mapName = mapName;
  }
  
  /**
   * Creates data structure from BioPax xml node.
   * 
   * @param biopax
   *          xml node
   * @return {@link BiopaxData} structure containing BioPax data
   */
  public BiopaxData parse(Node biopax) {
    BiopaxData result = new BiopaxData();
    NodeList nodes = biopax.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if ("bp:PublicationXref".equalsIgnoreCase(node.getNodeName())) {
          BiopaxPublication publication = parsePublication(node);
          if (publication.getId() == null || publication.getId().isEmpty()) {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, publication.getId(), "BioPax", mapName), "No pubmed identifier defined for publication: title: " + publication.getTitle());
          }
          result.addPublication(publication);
        } else if ("bp:openControlledVocabulary".equalsIgnoreCase(node.getNodeName())) {
          result.addOpenControlledVocabulary(parseOpenControlledVocabulary(node));
        } else {
          logger.warn("Unknown biopax node: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Method which parse biopax vocabulary xml node.
   * 
   * @param biopaxNode
   *          xml node
   * @return {@link BiopaxOpenControlledVocabulary}
   */
  private BiopaxOpenControlledVocabulary parseOpenControlledVocabulary(Node biopaxNode) {
    BiopaxOpenControlledVocabulary result = new BiopaxOpenControlledVocabulary();
    NodeList nodes = biopaxNode.getChildNodes();

    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if ("bp:ID".equalsIgnoreCase(node.getNodeName())) {
          result.setId(node.getTextContent());
        } else if ("bp:TERM".equalsIgnoreCase(node.getNodeName())) {
          result.setTerm(node.getTextContent());
        } else if ("bp:Ontology".equalsIgnoreCase(node.getNodeName())) {
          result.setOntology(node.getTextContent());
        } else {
          logger.warn("Unknown biopax node: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Method which parse biopax publication xml node.
   * 
   * @param publication
   *          xml node
   * @return {@link BiopaxPublication}
   */
  protected BiopaxPublication parsePublication(Node publication) {
    BiopaxPublication result = new BiopaxPublication();
    NodeList nodes = publication.getChildNodes();

    result.setReferenceId(XmlParser.getNodeAttr("rdf:id", publication));
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if ("bp:ID".equalsIgnoreCase(node.getNodeName())) {
          result.setId(node.getTextContent());
        } else if ("bp:DB".equalsIgnoreCase(node.getNodeName())) {
          result.setDb(node.getTextContent());
        } else if ("bp:TITLE".equalsIgnoreCase(node.getNodeName())) {
          result.setTitle(node.getTextContent());
        } else if ("bp:SOURCE".equalsIgnoreCase(node.getNodeName())) {
          result.setSource(node.getTextContent());
        } else if ("bp:YEAR".equalsIgnoreCase(node.getNodeName())) {
          result.setYear(node.getTextContent());
        } else if ("bp:AUTHORS".equalsIgnoreCase(node.getNodeName())) {
          result.setAuthors(node.getTextContent());
        } else {
          logger.warn("Unknown biopax node: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Returns xml string representing biopax reference to set of {@link MiriamData}
   * form parameters.
   * 
   * @param miriamData
   *          set of {@link MiriamData} for which we want to obtain biopax
   *          references
   * @return xml string representing biopax reference to set of {@link MiriamData}
   *         form parameters. Only references to {@link MiriamType#PUBMED} are
   *         included.
   */
  public String toReferenceXml(Set<MiriamData> miriamData) {
    StringBuilder sb = new StringBuilder("");
    for (MiriamData md : miriamData) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        sb.append("<BiopaxRef>" + getHash(md) + "</BiopaxRef>\n");
      }
    }
    return sb.toString();
  }

  /**
   * Returns unique hash for the {@link MiriamData} that can be used as a key in
   * BioPax xml.
   * 
   * @param md
   *          {@link MiriamData} for which we want to have unique hash value
   * @return unique hash for the {@link MiriamData} that can be used as a key in
   *         BioPax xml
   */
  private String getHash(MiriamData md) {
    if (miriamHash.get(md) == null) {
      miriamHash.put(md, "ann" + hash);
      hash++;
    }
    return miriamHash.get(md);
  }

  /**
   * Converts collection of {@link MiriamData} into an xml {@link String}
   * representing this collection as a BioPax data.
   * 
   * @param miriamData
   *          collection of {@link MiriamData}
   * @return xml {@link String} representing this collection as a BioPax data
   */
  public String toXml(Collection<MiriamData> miriamData) {
    StringBuilder sb = new StringBuilder("");
    sb.append("<Biopax>\n");
    for (MiriamData md : miriamData) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        sb.append(toXml(md));
      }
    }
    sb.append("</Biopax>\n");

    return sb.toString();
  }

  /**
   * Converts {@link MiriamData} into xml string in BioPax format.
   * 
   * @param md
   *          {@link MiriamData} to transform
   * @return xml string in BioPax format representing {@link MiriamData}
   */
  public String toXml(MiriamData md) {
    StringBuilder sb = new StringBuilder();
    sb.append(
        "<bp:PublicationXref xmlns:bp=\"http://www.biopax.org/release/biopax-level3.owl#\" "
            + "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" rdf:id=\"" + getHash(md) + "\">\n");
    sb.append("  <bp:ID rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + md.getResource() + "</bp:ID>\n");
    sb.append("  <bp:DB rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">PubMed</bp:DB>\n");
    sb.append("</bp:PublicationXref>\n");
    return sb.toString();
  }
}
