package lcsb.mapviewer.wikipathway.XML;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.*;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.celldesigner.reaction.ReactionLineData;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.*;
import lcsb.mapviewer.model.map.layout.graphics.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.wikipathway.model.*;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxFactory;

/**
 * Class contains methods for GraphToModel conversion.
 * 
 * @author Jan Badura
 * 
 */
public class ModelContructor {

  /**
   * Default color used by complexes.
   */
  private static final Color DEFAULT_COMPLEX_ALIAS_COLOR = new Color(102, 255, 255);

  /**
   * Epsilon used for double comparison.
   */
  private static final double EPSILON = 1e-6;
  /**
   * List of {@link Shape#shape shapes} that are not supported to be part of a
   * {@link Complex complex}.
   */
  private static final Set<String> INALID_COMPLEX_SHAPE_CHILDREN = new HashSet<>();

  static {
    INALID_COMPLEX_SHAPE_CHILDREN.add("Rectangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Oval");
    INALID_COMPLEX_SHAPE_CHILDREN.add("RoundedRectangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Golgi Apparatus");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Brace");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Sarcoplasmic Reticulum");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Mitochondria");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Endoplasmic Reticulum");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Arc");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Triangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Hexagon");
  }

  /**
   * CellDesigner util class used for retrieving information about modifier
   * graphics.
   */
  private ModifierTypeUtils mtu = new ModifierTypeUtils();
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Parser used for extracting {@link MiriamData references} from GPML model.
   */
  private BiopaxFactory biopaxFactory;

  public ModelContructor(String mapName) {
    biopaxFactory = new BiopaxFactory(mapName);
  }

  /**
   * This function creates Species from DataNode.
   *
   * @param dataNode
   *          object from which species is created
   * @param data
   *          ...
   * @return {@link Species} created from input {@link DataNode}
   */
  protected Species createSpecies(DataNode dataNode, Data data) {
    Species res = null;
    String type = dataNode.getType();
    if (type == null || type.equals("")) {
      type = null;
    }

    if (type != null && type.equalsIgnoreCase("Metabolite")) {
      res = new SimpleMolecule(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("GeneProduct")) {
      res = new Gene(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Pathway")) {
      res = new Phenotype(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Rna")) {
      res = new Rna(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Protein")) {
      res = new GenericProtein(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Complex")) {
      res = new Complex(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("None")) {
      res = new Unknown(dataNode.getGraphId());
    } else {
      logger.warn("[" + dataNode.getGraphId() + "]\tUnknown species type: " + type + ". Using Unknown");
      res = new Unknown(dataNode.getGraphId());
    }

    res.addMiriamData(dataNode.getReferences());
    res.setName(dataNode.getName());
    StringBuilder notes = new StringBuilder();
    for (String comment : dataNode.getComments()) {
      notes.append(comment + "\n\n");
    }
    res.setNotes(notes.toString());
    return res;
  }

  /**
   * This function adds {@link Complex} to model from graph. ComplexName is set as
   * groupId from GPML
   *
   * @param model
   *          to this model complexes will be added
   * @param graph
   *          gpml data model
   * @param data
   *          ...
   */
  protected void addComplex(Model model, Graph graph, Data data) {
    for (Group group : graph.getGroups()) {
      Complex complex = new Complex(group.getGraphId());
      complex.setName(group.getGraphId());
      if ("Complex".equalsIgnoreCase(group.getStyle())) {
        complex.setHypothetical(false);
      } else {
        complex.setHypothetical(true);
      }

      Rectangle2D rec = group.getRectangle();
      if (rec == null) {
        rec = new Rectangle2D.Double(0, 0, 0, 0);
      }
      complex.setX(rec.getX());
      complex.setY(rec.getY());
      complex.setWidth((int) rec.getWidth());
      complex.setHeight((int) rec.getHeight());
      complex.setZ(group.getzOrder());
      complex.setFillColor(DEFAULT_COMPLEX_ALIAS_COLOR);

      data.id2alias.put(group.getGraphId(), complex);
      model.addElement(complex);
    }
  }

  /**
   * This function adds Species, TextLabels, Compartments and Shapes from graph.
   *
   * @param model
   *          model to which species will be added
   * @param graph
   *          GPML data model
   * @param data
   *          ...
   */
  protected void addElement(Model model, Graph graph, Data data) {
    for (DataNode dataNode : graph.getDataNodes()) {
      Species species = createSpecies(dataNode, data);
      species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), dataNode.getBiopaxReference()));

      Element alias = updateAlias(dataNode, species);

      data.id2alias.put(dataNode.getGraphId(), alias);
      model.addElement(alias);
    }
    for (Label label : graph.getLabels()) {
      if (label.isTreatAsNode()) {
        Species species = createSpecies(label);
        species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), label.getBiopaxReference()));

        Element alias = updateAlias(label, species);

        data.id2alias.put(label.getGraphId(), alias);
        model.addElement(alias);
      } else if (label.getGroupRef() == null || label.getGroupRef().isEmpty()) {
        LayerText text = createText(label);
        data.layer.addLayerText(text);
      }
    }
    for (Shape shape : graph.getShapes()) {
      if (shape.isTreatAsNode()) {
        Species species = createSpecies(shape);
        species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), shape.getBiopaxReference()));

        Element alias = updateAlias(shape, species);

        data.id2alias.put(shape.getGraphId(), alias);
        model.addElement(alias);

      } else {
        if (!shape.isCompartment()) {
          if (shape.getShape().equalsIgnoreCase("oval")) {
            LayerOval oval = createOval(shape);
            data.layer.addLayerOval(oval);
          } else if (shape.getComments().size() > 0 || shape.getTextLabel() != null) {
            data.shapeLabelLayer.addLayerText(createText(shape));
          } else {
            LayerRect rect = createRectangle(shape);
            data.layer.addLayerRect(rect);
          }
        } else {
          Compartment compartment = new Compartment(shape.getGraphId());
          Element cmpAl = updateAlias(shape, compartment);
          model.addElement(cmpAl);
        }
      }
    }

    for (State state : graph.getStates()) {
      // TODO this might not work
      Element species = data.id2alias.get(state.getGraphRef());
      if (state.getType() != null) {
        AbstractSiteModification mr = null;

        if (species instanceof Protein) {
          mr = new Residue();
          ((Protein) species).addResidue((Residue) mr);
        } else if (species instanceof Gene) {
          mr = new ModificationSite();
          ((Gene) species).addModificationSite((ModificationSite) mr);
        } else {
          logger.warn(
              state.getLogMarker(), "state for " + species.getClass().getSimpleName() + " is not supported.");
        }
        if (mr != null) {
          mr.setIdModificationResidue(state.getGraphId());
          mr.setState(state.getType());

          Double x = state.getRelX() * species.getWidth() / 2 + species.getCenterX();
          Double y = state.getRelY() * species.getHeight() / 2 + species.getCenterY();
          mr.setPosition(new Point2D.Double(x, y));
          adjustModificationCoordinates(species);
        }
      } else if (state.getStructuralState() != null) {
        if (species instanceof Protein) {
          Protein protein = ((Protein) species);
          if (protein.getStructuralState() == null) {
            protein.setStructuralState(createStructuralState(state, protein));
          } else {
            logger.warn(state.getLogMarker(), " tries to override another state: " + protein.getStructuralState());
          }
        } else {
          logger.warn(state.getLogMarker(), "structural state for " + species.getClass().getSimpleName()
              + " is not supported.");
        }
      }
    }
  }

  private StructuralState createStructuralState(State state, Species species) {
    Double x = state.getRelX() * species.getWidth() + species.getX();
    Double y = state.getRelY() * species.getHeight() + species.getY();
    StructuralState structuralState = new StructuralState();
    structuralState.setPosition(new Point2D.Double(x, y));
    structuralState.setFontSize(10);
    structuralState.setHeight(state.getHeight());
    structuralState.setWidth(state.getWidth());
    structuralState.setValue(state.getStructuralState());
    return structuralState;
  }

  /**
   * {@link ModificationResidue} in element might have slightly off coordinates
   * (due to different symbol shapes). For that we need to align them to match our
   * model.
   *
   * @param species
   */
  protected void adjustModificationCoordinates(Element species) {
    if (species instanceof Protein) {
      Protein protein = (Protein) species;
      if (protein.getModificationResidues().size() > 0) {
        CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
        for (ModificationResidue mr : protein.getModificationResidues()) {
          double angle = converter.getAngleForPoint(species, mr.getPosition());
          mr.setPosition(converter.getResidueCoordinates(species, angle));
        }
      }
    } else if (species instanceof Gene) {
      Gene gene = (Gene) species;
      if (gene.getModificationResidues().size() > 0) {
        CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
        for (ModificationResidue mr : gene.getModificationResidues()) {
          double angle = converter.getCellDesignerPositionByCoordinates(mr);
          mr.setPosition(converter.getCoordinatesByPosition(species, angle));
        }
      }

    }
  }

  /**
   * Creates {@link LayerRect} object from {@link Shape}.
   *
   * @param shape
   *          source GPML object to be transformed
   * @return {@link LayerRect} obtained from {@link Shape} object
   */
  public LayerRect createRectangle(Shape shape) {
    LayerRect rect = new LayerRect();
    Rectangle2D rec;
    if (shouldRotate90degrees(shape)) {
      rec = rotate90degrees(shape.getRectangle());
    } else {
      rec = shape.getRectangle();
    }
    rect.setX(rec.getX());
    rect.setY(rec.getY());
    rect.setHeight(rec.getHeight());
    rect.setWidth(rec.getWidth());
    rect.setColor(shape.getColor());
    return rect;
  }

  Rectangle2D rotate90degrees(Rectangle2D rectangle) {
    return new Rectangle2D.Double(rectangle.getCenterX() - rectangle.getHeight() / 2,
        rectangle.getCenterY() - rectangle.getWidth() / 2, rectangle.getHeight(), rectangle.getWidth());
  }

  boolean shouldRotate90degrees(Shape shape) {
    return shouldRotate90degrees(shape.getRotation(), shape);
  }

  boolean shouldRotate90degrees(Double rot) {
    return shouldRotate90degrees(rot, null);
  }

  boolean shouldRotate90degrees(Double rot, Shape shape) {
    if (rot == null) {
      return false;
    }
    double rotation = rot;
    while (rotation > Math.PI / 2) {
      rotation -= Math.PI;
    }
    while (rotation < -Math.PI / 2) {
      rotation += Math.PI;
    }
    if (Math.abs(rotation) < Math.PI / 4) {
      if (Math.abs(rotation) > Configuration.EPSILON) {
        LogMarker marker = null;
        if (shape != null) {
          marker = shape.getLogMarker();
        }
        logger.warn(marker, "Rotation of objects is not supported. Rotation=" + rot);
      }
      return false;
    }
    if ((Math.PI / 2 - Math.abs(rotation)) > Configuration.EPSILON) {
      LogMarker marker = null;
      if (shape != null) {
        marker = shape.getLogMarker();
      }
      logger.warn(marker, "Rotation of objects is not supported. Rotation=" + rot);
    }
    return true;
  }

  /**
   * Creates {@link LayerOval} object from {@link Shape}.
   *
   * @param shape
   *          source GPML object to be transformed
   * @return {@link LayerOval} obtained from {@link Shape} object
   */
  public LayerOval createOval(Shape shape) {
    LayerOval oval = new LayerOval();
    Rectangle2D rec = shape.getRectangle();
    oval.setX(rec.getX());
    oval.setY(rec.getY());
    oval.setHeight(rec.getHeight());
    oval.setWidth(rec.getWidth());
    oval.setColor(shape.getColor());
    return oval;
  }

  /**
   * Creates {@link LayerText} from {@link Label}.
   *
   * @param label
   *          object from which result will be created
   * @return {@link LayerText} from {@link Label}
   */
  public LayerText createText(Label label) {
    Rectangle2D rec = label.getRectangle();

    LayerText text = new LayerText();
    text.setX(rec.getX());
    text.setY(rec.getY());
    text.setHeight(rec.getHeight());
    text.setWidth(rec.getWidth());
    text.setColor(label.getColor());
    if (text.getColor().equals(Color.WHITE)) {
      text.setColor(Color.BLACK);
    }
    StringBuilder notes = new StringBuilder(label.getName());
    for (String comment : label.getComments()) {
      notes.append(comment + "\n\n");
    }
    text.setNotes(notes.toString());
    return text;
  }

  public LayerText createText(Shape shape) {
    Rectangle2D rec = shape.getRectangle();

    LayerText text = new LayerText();
    text.setX(rec.getX());
    text.setY(rec.getY());
    text.setHeight(rec.getHeight());
    text.setWidth(rec.getWidth());
    text.setColor(shape.getColor());
    if (text.getColor().equals(Color.WHITE)) {
      text.setColor(Color.BLACK);
    }

    String name = shape.getTextLabel();
    if (name == null) {
      name = "";
    }

    StringBuilder notes = new StringBuilder(name);
    for (String comment : shape.getComments()) {
      notes.append(comment + "\n\n");
    }
    text.setNotes(notes.toString());

    return text;
  }

  /**
   * Creates alias for {@link GraphicalPathwayElement}. Type of the alias is
   * defined by the parameter {@link Species}
   *
   * @param gpmlElement
   *          object from which alias will be create
   * @param element
   *          element for which alias will be created
   * @return {@link Species} created from input {@link Label}
   */
  public Element updateAlias(GraphicalPathwayElement gpmlElement, Element element) {
    if (element instanceof Compartment) {
      if (((Shape) gpmlElement).getShape().equalsIgnoreCase("oval")) {
        element = new OvalCompartment((Compartment) element);
      } else {
        element = new SquareCompartment((Compartment) element);
      }
      String name = ((Shape) gpmlElement).getTextLabel();
      if (name == null) {
        name = "";
      }
      element.setName(name);
    }

    Rectangle2D rec = gpmlElement.getRectangle();
    element.setX(rec.getX());
    element.setY(rec.getY());
    element.setWidth((int) rec.getWidth());
    element.setHeight((int) rec.getHeight());
    element.setFillColor(gpmlElement.getFillColor());
    if (element instanceof Compartment && !Objects.equals(Color.WHITE, gpmlElement.getFillColor())) {
      element.setBorderColor(gpmlElement.getFillColor());
    }
    element.setZ(gpmlElement.getzOrder());
    element.setFontColor(gpmlElement.getColor());
    if (gpmlElement.getFontSize() != null) {
      element.setFontSize(gpmlElement.getFontSize());
    }
    return element;
  }

  /**
   * Creates {@link Unknown species} from {@link Label}.
   *
   * @param label
   *          original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(Label label) {
    logger.warn(label.getLogMarker(), " Label cannot be part of reaction. Tranforming to Unknown");

    Species res = new Unknown(label.getGraphId());
    res.addMiriamData(label.getReferences());
    res.setName(label.getName());
    StringBuilder notes = new StringBuilder();
    for (String comment : label.getComments()) {
      notes.append(comment + "\n\n");
    }
    res.setNotes(notes.toString());
    return res;
  }

  /**
   * Creates {@link Unknown species} from {@link Shape}.
   *
   * @param shape
   *          original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(Shape shape) {
    logger.warn(shape.getLogMarker(), " Shape can not be part of reaction. Tranforming to Unknown");

    Species res = new Unknown(shape.getGraphId());
    StringBuilder notes = new StringBuilder();
    for (String comment : shape.getComments()) {
      notes.append(comment + "\n\n");
    }
    res.setNotes(notes.toString());
    res.setName(shape.getName());
    return res;
  }

  /**
   * This function add Species to right Complexes.
   *
   * @param graph
   *          GPML data model
   * @param data
   *          ...
   * @throws UnknownChildClassException
   *           thrown when complex contain invalid child
   */
  protected void putSpeciesIntoComplexes(Graph graph, Data data) throws UnknownChildClassException {
    for (Group group : graph.getGroups()) {

      Complex complex = (Complex) data.id2alias.get(group.getGraphId());
      for (PathwayElement pe : group.getNodes()) {
        Element element = data.id2alias.get(pe.getGraphId());
        if (element != null) {
          if (element instanceof Species) {
            Species species = (Species) element;
            complex.addSpecies(species);
          }
          // we have label
        } else if (graph.getLabelByGraphId(pe.getGraphId()) != null) {
          Label label = graph.getLabelByGraphId(pe.getGraphId());
          // if complex has generic name then change it into label
          if (complex.getName().equals(group.getGraphId())) {
            complex.setName(label.getTextLabel());
          } else {
            // if there are more labels than one then merge labels
            complex.setName(complex.getName() + "\n" + label.getTextLabel());
          }
        } else if (graph.getShapeByGraphId(pe.getGraphId()) != null) {
          Shape shape = graph.getShapeByGraphId(pe.getGraphId());
          if (INALID_COMPLEX_SHAPE_CHILDREN.contains(shape.getShape())) {
            logger.warn(shape.getLogMarker(), shape.getShape() + " cannot be part of a complex. Skipping.");
          } else {
            throw new UnknownChildClassException("Unknown class type with id \"" + pe.getGraphId() + "\": "
                + shape.getShape() + ". Group id: " + group.getGraphId());
          }
        } else {
          throw new UnknownChildClassException(
              "Cannot find child with id: " + pe.getGraphId() + ". Group id: " + group.getGraphId());
        }
      }
    }
  }

  /**
   * This function creates {@link Reaction} from {@link Interaction} from graph.
   *
   * @param interaction
   *          GPML interaction
   * @param graph
   *          GPML data model
   * @param data
   *          ...
   * @return reaction created from GPML {@link Interaction}
   */
  protected Reaction createReactionFromInteraction(Interaction interaction, Graph graph, Data data) {
    InteractionMapping mapping = InteractionMapping.getInteractionMapping(interaction.getType(),
        interaction.getLine().getType());
    if (mapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + interaction.getType());
    }
    Reaction reaction = createReactionByType(mapping.getModelReactionType());
    reaction.setIdReaction(interaction.getGraphId());
    reaction.setReversible(mapping.isReversible() || interaction.isReversible());

    reaction.addMiriamData(interaction.getReferences());
    reaction.setZ(interaction.getzOrder());
    reaction.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), interaction.getBiopaxReferences()));

    ReactionLayoutFinder layoutFinder = new ReactionLayoutFinder();
    Map<Class<?>, PolylineData> lines = layoutFinder.getNodeLines(interaction);
    PolylineData reactantLine = lines.get(Reactant.class);
    PolylineData productLine = lines.get(Product.class);

    Reactant reactant = new Reactant();
    reactant.setElement(data.id2alias.get(interaction.getStart()));
    reactant.setLine(reactantLine);
    reaction.addReactant(reactant);

    Product product = new Product();
    product.setElement(data.id2alias.get(interaction.getEnd()));
    product.setLine(productLine);
    reaction.addProduct(product);

    for (Edge e : interaction.getReactants()) {
      Reactant reac = createReactantFromEdge(data, e);
      reaction.addReactant(reac);
    }

    for (Edge e : interaction.getProducts()) {
      Product pro = createProductFromEdge(data, e);
      reaction.addProduct(pro);
    }

    if (reaction.getReactants().size() > 1) {
      PolylineData operatorLine = lines.get(AndOperator.class);
      NodeOperator andOperator;
      // heterodimer association use association operator
      if (reaction instanceof HeterodimerAssociationReaction) {
        andOperator = new AssociationOperator();
        // all other reaction just force and operator between inputs
      } else {
        andOperator = new AndOperator();
      }
      andOperator.addInput(reactant);
      andOperator.setLine(operatorLine);
      for (int i = 1; i < reaction.getReactants().size(); i++) {
        Reactant r = reaction.getReactants().get(i);
        r.getLine().getEndPoint().setLocation(operatorLine.getBeginPoint());
        andOperator.addInput(r);
      }
      reaction.addNode(andOperator);
    } else {
      PolylineData operatorLine = lines.get(AndOperator.class);
      for (int i = 1; i < operatorLine.getPoints().size(); i++) {
        reactantLine.addPoint(operatorLine.getPoints().get(i));
      }
      reactant.setLine(PolylineDataFactory.removeCollinearPoints(reactantLine));
    }

    if (reaction.getProducts().size() > 1) {
      PolylineData operatorLine = lines.get(SplitOperator.class);

      SplitOperator splitOperator = new SplitOperator();
      splitOperator.addOutput(product);
      splitOperator.setLine(operatorLine.reverse());
      for (int i = 1; i < reaction.getProducts().size(); i++) {
        Product r = reaction.getProducts().get(i);
        r.getLine().getBeginPoint().setLocation(operatorLine.getEndPoint());
        splitOperator.addOutput(r);
      }
      reaction.addNode(splitOperator);
    } else {
      PolylineData operatorLine = lines.get(SplitOperator.class);
      for (int i = 0; i < operatorLine.getPoints().size() - 1; i++) {
        productLine.addPoint(i, operatorLine.getPoints().get(i));
      }
      product.setLine(PolylineDataFactory.removeCollinearPoints(productLine));
    }

    for (Edge e : interaction.getModifiers()) {
      Modifier mod = createModifierFromEdge(data, e);
      reaction.addModifier(mod);
    }
    if (reaction instanceof TwoReactantReactionInterface && reaction.getReactants().size() < 2) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
          "Reaction should contain at least 2 reactants. GraphId: " + interaction.getGraphId());
      reaction = new UnknownTransitionReaction(reaction);
    }
    if (reaction instanceof ReducedNotation && ((reaction.getReactants().size() > 1)
        || reaction.getProducts().size() > 1 || reaction.getModifiers().size() > 0)) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
          "Reaction should contain only one reactant and one product. GraphId: " + interaction.getGraphId());
      reaction = new UnknownTransitionReaction(reaction);
    }
    ReactionLineData rld = ReactionLineData.getByReactionType(reaction.getClass());
    if (rld == null) {
      throw new InvalidArgumentException("Don't know how to process reaction type: " + reaction.getClass());
    }
    for (AbstractNode node : reaction.getNodes()) {
      if (!(node instanceof Modifier)) {
        node.getLine().setType(rld.getLineType());
      }
    }
    for (Product p : reaction.getProducts()) {
      p.getLine().getEndAtd().setArrowType(rld.getProductArrowType());
      if (rld.getProductLineTrim() > 0) {
        p.getLine().trimEnd(rld.getProductLineTrim());
      }

    }
    if (reaction.isReversible()) {
      for (Reactant r : reaction.getReactants()) {
        r.getLine().getBeginAtd().setArrowType(rld.getProductArrowType());
        if (rld.getProductLineTrim() > 0) {
          r.getLine().trimBegin(rld.getProductLineTrim());
        }
      }
    }

    PolylineData modifierLine = lines.get(Modifier.class);
    modifierLine.setType(rld.getLineType());
    reaction.setLine(modifierLine);

    ModifierTypeUtils mtu = new ModifierTypeUtils();
    for (Modifier m : reaction.getModifiers()) {
      m.getLine().setEndPoint(m.getLine().getPoints().get(m.getLine().getPoints().size() - 2));
      m.getLine().setEndPoint(mtu.getAnchorPointOnReactionRect(reaction, mtu.getTargetLineIndexByModifier(m)));
    }

    return reaction;
  }

  /**
   * Creates {@link Reactant} from GPML edge.
   *
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Reactant} from gpml edge
   */
  protected Reactant createReactantFromEdge(Data data, Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This reactant is invalid");
    }

    Reactant reac = new Reactant();
    reac.setElement(data.id2alias.get(id));
    reac.setLine(e.getLine());

    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getEnd())) {
      reac.setLine(reac.getLine().reverse());
    }
    return reac;
  }

  /**
   * Creates {@link Product} from GPML edge.
   *
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Product} from GPML edge
   */
  protected Product createProductFromEdge(Data data, Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("[" + e.getGraphId() + "]\tThis product is invalid");
    }
    Product pro = new Product();
    pro.setElement(data.id2alias.get(id));
    pro.setLine(e.getLine());
    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getStart())) {
      pro.setLine(pro.getLine().reverse());
    }
    return pro;
  }

  /**
   * Creates {@link Modifier} from gpml edge.
   *
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Modifier} from gpml edge
   */
  protected Modifier createModifierFromEdge(Data data, Edge e) {
    InteractionMapping modifierMapping = InteractionMapping.getInteractionMapping(e.getType(), e.getLine().getType());
    if (modifierMapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + e.getType());
    }

    Class<? extends ReactionNode> modifierType = null;
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      modifierType = modifierMapping.getModelInputReactionNodeType();
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      modifierType = modifierMapping.getModelOutputReactionNodeType();
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This modifier is invalid");
    }

    Modifier mod = createModifierByType(modifierType, (Species) data.id2alias.get(id));

    ModifierType mt = mtu.getModifierTypeForClazz(mod.getClass());

    e.getLine().setEndAtd(mt.getAtd());
    e.getLine().setType(mt.getLineType());
    mod.setLine(e.getLine());
    return mod;
  }

  /**
   * Creates {@link Reaction} for a given class type.
   *
   * @param reactionType
   *          type of reaction to create
   * @return new instance of the reactionType
   */
  private Reaction createReactionByType(Class<? extends Reaction> reactionType) {
    try {
      Reaction result = reactionType.getConstructor().newInstance();
      return result;
    } catch (Exception e) {
      throw new InvalidArgumentException("Cannot create reaction.", e);
    }
  }

  /**
   * Creates {@link Modifier} for a given class type.
   *
   * @param modifierType
   *          type of modifier in reaction to create
   * @param alias
   *          {@link Species alias } to which modifier is attached
   * @return new instance of the modifierType
   */
  private Modifier createModifierByType(Class<? extends ReactionNode> modifierType, Species alias) {
    try {
      Modifier result = (Modifier) modifierType.getConstructor(Element.class).newInstance(alias);
      return result;
    } catch (Exception e) {
      throw new InvalidArgumentException("Cannot create modifier.", e);
    }
  }

  /**
   * This function creates {@link Model} from {@link Graph}.
   *
   * @param graph
   *          object with data obtained from gpml
   * @return {@link Model} object representing gpml
   * @throws ConverterException
   *           exception thrown when conversion from graph couldn't be performed
   *
   */
  public Model getModel(Graph graph) throws ConverterException {
    try {
      Data data = new Data();
      Model model = new ModelFullIndexed(null);
      model.setWidth(graph.getBoardWidth());
      model.setHeight(graph.getBoardHeight());

      addComplex(model, graph, data);
      addElement(model, graph, data);
      putSpeciesIntoComplexes(graph, data);

      for (Interaction interaction : graph.getInteractions()) {
        Reaction reaction = createReactionFromInteraction(interaction, graph, data);
        model.addReaction(reaction);
      }

      removeEmptyComplexes(model);

      fixCompartmentAliases(model);

      StringBuilder tmp = new StringBuilder();
      for (String string : graph.getComments()) {
        tmp.append(string + "\n");
      }
      String notes = tmp.toString();
      boolean different = false;
      do {
        String newNotes = StringEscapeUtils.unescapeHtml4(notes);
        different = newNotes.compareTo(notes) != 0;
        notes = newNotes;
      } while (different);
      if (notes != null) {
        notes = StringEscapeUtils.unescapeHtml4(notes);
      }
      model.setNotes(notes);
      model.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), graph.getBiopaxReferences()));

      data.layer.addLayerLines(createLines(graph));

      if (!data.layer.isEmpty()) {
        model.getLayers().add(data.layer);
      }
      if (!data.shapeLabelLayer.isEmpty()) {
        model.getLayers().add(data.shapeLabelLayer);
      }

      assignNamesToComplexes(model);
      assignNamesToCompartments(model);

      putAliasesIntoCompartments(model);

      putMissingZIndexes(model.getDrawables());

      return model;
    } catch (UnknownChildClassException e) {
      throw new ConverterException(e);
    }
  }

  private void putMissingZIndexes(Collection<Drawable> bioEntities) {
    int maxZIndex = 0;
    for (Drawable bioEntity : bioEntities) {
      if (bioEntity.getZ() != null) {
        maxZIndex = Math.max(maxZIndex, bioEntity.getZ());
      }
    }

    for (Drawable bioEntity : bioEntities) {
      if (bioEntity.getZ() == null) {
        Integer index = null;
        if (bioEntity instanceof Complex) {
          index = getMinZIndex(((Complex) bioEntity).getAllChildren());
        }

        if (index == null) {
          index = ++maxZIndex;
        } else {
          index--;
        }
        bioEntity.setZ(index);
      }
    }

  }

  private Integer getMinZIndex(List<Species> allChildren) {
    Integer result = null;
    for (Species species : allChildren) {
      if (species.getZ() != null) {
        if (result == null)
          result = species.getZ();
        result = Math.min(result, species.getZ());
      }
    }
    return result;
  }

  /**
   * Method that put {@link Species aliases} that are not assigned into any
   * {@link Compartment} into a proper compartment.
   *
   * @param model
   *          model where aliases will be modified
   */
  private void putAliasesIntoCompartments(Model model) {
    for (Element element : model.getElements()) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          element.setCompartment(null);
        } else if (element.getCompartment() == null) {
          Compartment selectedAlias = null;
          for (Compartment cAlias : model.getCompartments()) {
            if (cAlias.cross(element)) {
              if (selectedAlias == null) {
                selectedAlias = cAlias;
              } else if (selectedAlias.getSize() > cAlias.getSize()) {
                selectedAlias = cAlias;
              }
            }
          }
          if (selectedAlias != null) {
            selectedAlias.addElement(element);
          }
        }
      }
    }
  }

  /**
   * Removes empty complexes (with size 0) from model.
   *
   * @param model
   *          model where operation is performed
   */
  private void removeEmptyComplexes(Model model) {
    Set<Element> aliasesInReaction = new HashSet<>();
    for (Reaction reaction : model.getReactions()) {
      for (ReactionNode node : reaction.getReactionNodes()) {
        aliasesInReaction.add(node.getElement());
      }
    }
    List<Element> toRemove = new ArrayList<>();
    ElementUtils eu = new ElementUtils();
    for (Element element : model.getElements()) {
      if (element instanceof Complex) {
        Complex complex = (Complex) element;
        if (complex.getSize() <= EPSILON && complex.getAllChildren().size() == 0) {
          if (aliasesInReaction.contains(element)) {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
                "Empty element is invalid, but it's a part of reaction.");
          } else {
            toRemove.add(element);
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element), "Empty element is invalid");
          }
        }
      }
    }
    for (Element alias : toRemove) {
      model.removeElement(alias);
    }
  }

  /**
   * Tries to find a name to assign to complexes if complexes don't contain them.
   *
   * @param model
   *          model where complexes are placed
   */
  private void assignNamesToComplexes(Model model) {
    for (Element alias : model.getElements()) {
      if (alias instanceof Complex) {
        if (alias.getName() == null || (alias.getName().isEmpty())) {
          for (Layer layer : model.getLayers()) {
            if (layer.getName().equals("defaultLayer")) {
              LayerText toRemove = null;
              for (LayerText lt : layer.getTexts()) {
                if (alias.contains(lt)) {
                  alias.setName(lt.getNotes());
                  toRemove = lt;
                  break;
                }
              }
              if (toRemove != null) {
                layer.removeLayerText(toRemove);
                break;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Tries to find a name to assign to compartments if compartments don't contain
   * them.
   *
   * @param model
   *          model where compartments are placed
   */
  private void assignNamesToCompartments(Model model) {
    for (Compartment compartment : model.getCompartments()) {
      if (compartment.getName() == null || compartment.getName().isEmpty()) {
        for (Layer layer : model.getLayers()) {
          if (layer.getName().equals("defaultLayer")) {
            LayerText toRemove = null;
            for (LayerText lt : layer.getTexts()) {
              if (compartment.contains(lt)) {
                compartment.setName(lt.getNotes());
                compartment.setNamePoint(new Point2D.Double(lt.getX(), lt.getY()));
                toRemove = lt;
                break;
              }
            }
            if (toRemove != null) {
              layer.removeLayerText(toRemove);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Creates list of {@link LayerLine} in the model from gpml model.
   *
   * @param graph
   *          gpml model
   * @return list of {@link LayerLine}
   */
  private Collection<PolylineData> createLines(Graph graph) {
    List<PolylineData> result = new ArrayList<PolylineData>();
    for (PolylineData pd : graph.getLines()) {
      result.add(pd);
    }
    return result;
  }

  /**
   * By default gpml doesn't offer information about compartments structure. This
   * function fixes assignments to compartment and compartment aliases.
   *
   * @param model
   *          model where assignments are fixed.
   */
  private void fixCompartmentAliases(Model model) {
    List<Compartment> aliases = model.getCompartments();
    // clear all assignments
    for (Compartment compartmentAlias : aliases) {
      compartmentAlias.getElements().clear();
    }

    for (Element element : model.getElements()) {
      // elements inside complexes shouldn't be considered
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          continue;
        }
      }
      Compartment parentAlias = null;
      for (Compartment compartmentAlias : aliases) {
        if (compartmentAlias.contains(element)) {
          if (parentAlias == null) {
            parentAlias = compartmentAlias;
          } else if (parentAlias.getSize() > compartmentAlias.getSize()) {
            parentAlias = compartmentAlias;
          }
        }
      }
      if (parentAlias != null) {
        parentAlias.addElement(element);
        element.setCompartment(parentAlias);
      }
    }
  }

  /**
   * Support class to send less parameters in functions.
   *
   * @author Jan Badura
   */
  private final class Data {
    /**
     * Map between graphId and aliases created from gpml elements.
     */
    private Map<String, Element> id2alias;

    /**
     * Default layer.
     */
    private Layer layer;

    /**
     * Default layer.
     */
    private Layer shapeLabelLayer;

    /**
     * Default constructor.
     */
    private Data() {
      id2alias = new HashMap<String, Element>();
      layer = new Layer();
      layer.setVisible(true);
      layer.setLayerId("1");
      layer.setName("defaultLayer");
      shapeLabelLayer = new Layer();
      shapeLabelLayer.setVisible(true);
      shapeLabelLayer.setLayerId("2");
      shapeLabelLayer.setName("shapeLabelLayer");
    }
  }
}
