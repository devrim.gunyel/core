package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;

/**
 * Class used to store data from Shape from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Shape extends GraphicalPathwayElement {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Shape of the node. ???
   */
  private String shape;

  /**
   * ???
   */
  private String textLabel;

  /**
   * Where this node belongs to.
   */
  private String groupRef;

  /**
   * Is shape a compartment.
   */
  private Boolean compartment;

  /**
   * Thickness of the line used for drawing shape.
   */
  private Double lineThickness;

  /**
   * Align of the description.
   */
  private String vAlign;

  /**
   * Rotation of the element.
   */
  private Double rotation;

  /**
   * Sometimes {@link Shape labels} are connected to reactions.
   */
  private boolean treatAsNode = false;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link PathwayElement#graphId}
   */
  public Shape(String graphId, String mapName) {
    super(graphId, mapName);
    setRectangle(null);
    setShape(null);
    setTextLabel(null);
    setGroupRef(null);
    setCompartment(false);
    setFillColor(Color.GRAY);
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected Shape() {
  }

  @Override
  public String getName() {
    return shape + ":" + textLabel;
  }

  @Override
  public LogMarker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.PARSING_ISSUE, getShape(), getGraphId(), getMapName());
  }

  /**
   * @return the shape
   * @see #shape
   */
  public String getShape() {
    return shape;
  }

  /**
   * @param shape
   *          the shape to set
   * @see #shape
   */
  public void setShape(String shape) {
    this.shape = shape;
  }

  /**
   * @return the textLabel
   * @see #textLabel
   */
  public String getTextLabel() {
    return textLabel;
  }

  /**
   * @param textLabel
   *          the textLabel to set
   * @see #textLabel
   */
  public void setTextLabel(String textLabel) {
    this.textLabel = textLabel;
  }

  /**
   * @return the compartment
   * @see #compartment
   */
  public Boolean isCompartment() {
    return compartment;
  }

  /**
   * @param compartment
   *          the compartment to set
   * @see #compartment
   */
  public void setCompartment(Boolean compartment) {
    this.compartment = compartment;
  }

  /**
   * @return the groupRef
   * @see #groupRef
   */
  public String getGroupRef() {
    return groupRef;
  }

  /**
   * @param groupRef
   *          the groupRef to set
   * @see #groupRef
   */
  public void setGroupRef(String groupRef) {
    this.groupRef = groupRef;
  }

  /**
   * @return the vAlign
   * @see #vAlign
   */
  public String getvAlign() {
    return vAlign;
  }

  /**
   * @param vAlign
   *          the vAlign to set
   * @see #vAlign
   */
  public void setvAlign(String vAlign) {
    this.vAlign = vAlign;
  }

  /**
   * @return the rotation
   * @see #rotation
   */
  public Double getRotation() {
    return rotation;
  }

  /**
   * @param rotation
   *          the rotation to set
   * @see #rotation
   */
  public void setRotation(Double rotation) {
    this.rotation = rotation;
  }

  /**
   * @return the lineThickness
   * @see #lineThickness
   */
  public Double getLineThickness() {
    return lineThickness;
  }

  /**
   * @param lineThickness
   *          the lineThickness to set
   * @see #lineThickness
   */
  public void setLineThickness(Double lineThickness) {
    this.lineThickness = lineThickness;
  }

  /**
   * @return the treatAsNode
   * @see #treatAsNode
   */
  public boolean isTreatAsNode() {
    return treatAsNode;
  }

  /**
   * @param treatAsNode
   *          the treatAsNode to set
   * @see #treatAsNode
   */
  public void setTreatAsNode(boolean treatAsNode) {
    this.treatAsNode = treatAsNode;
  }

}
