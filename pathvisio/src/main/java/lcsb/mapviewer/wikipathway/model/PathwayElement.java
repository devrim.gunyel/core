package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;

/**
 * Abstract class for pathway elements. It defines common functionalities for
 * all elements in the model. There are two known subclasses:
 * <ul>
 * <li>{@link GraphicalPathwayElement}, representing elements with some graphical
 * representation</li>
 * <li>{@link Group}, representing just groups of elements</li>
 * </ul>
 * 
 * @author Jan Badura
 * 
 */
public abstract class PathwayElement implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Comment of the element.
   */
  private List<String> comments = new ArrayList<>();

  /**
   * Z order of the element.
   */
  private Integer zOrder;

  /**
   * Identifier of the element in model.
   */
  private String graphId;

  /**
   * 
   * Reference to BioPax node with references about this element.
   */
  private List<String> biopaxReferences = new ArrayList<>();

  private String mapName;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link #graphId} value
   */
  public PathwayElement(String graphId, String mapName) {
    this.graphId = graphId;
    this.mapName = mapName;
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected PathwayElement() {
  }

  /**
   * 
   * @return {@link #graphId}
   */
  public String getGraphId() {
    return this.graphId;
  }

  /**
   * @param graphId
   *          the graphId to set
   * @see #graphId
   */
  void setGraphId(String graphId) {
    this.graphId = graphId;
  }

  /**
   * Returns name of the element.
   *
   * @return name of the element
   */
  abstract String getName();

  /**
   * Return boundary of the element.
   *
   * @return boundary of the element
   */
  abstract Rectangle2D getRectangle();

  public LogMarker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.PARSING_ISSUE, this.getClass().getSimpleName(), getGraphId(), getMapName());
  }

  /**
   * @return the biopaxReference
   * @see #biopaxReference
   */
  public List<String> getBiopaxReference() {
    return biopaxReferences;
  }

  /**
   * @param biopaxReferences
   *          the biopaxReference to set
   * @see #biopaxReference
   */
  public void setBiopaxReference(List<String> biopaxReferences) {
    this.biopaxReferences = biopaxReferences;
  }

  /**
   * @return the zOrder
   * @see #zOrder
   */
  public Integer getzOrder() {
    return zOrder;
  }

  /**
   * @param zOrder
   *          the zOrder to set
   * @see #zOrder
   */
  public void setzOrder(Integer zOrder) {
    this.zOrder = zOrder;
  }

  /**
   * Adds reference to {@link #biopaxReferences}.
   * 
   * @param biopaxString
   *          reference to add
   */
  public void addBiopaxReference(String biopaxString) {
    biopaxReferences.add(biopaxString);
  }

  /**
   * @return the comments
   * @see #comments
   */
  public List<String> getComments() {
    return comments;
  }

  /**
   * @param comment
   *          the comment to set
   * @see #comment
   */
  public void addComment(String comment) {
    this.comments.add(comment);
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(String mapName) {
    this.mapName = mapName;
  }
}
