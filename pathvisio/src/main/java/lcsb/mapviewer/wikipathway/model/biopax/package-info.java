/**
 * This package contains elements parsed from biopax format. Main element is
 * {@link lcsb.mapviewer.wikipathway.model.biopax.BiopaxData BiopaxData}.
 * 
 */
package lcsb.mapviewer.wikipathway.model.biopax;
