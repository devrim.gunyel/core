package lcsb.mapviewer.wikipathway;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.*;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;
import lcsb.mapviewer.wikipathway.XML.ModelToGPML;

@Component
public class GpmlParser extends Converter {
  Logger logger = LogManager.getLogger();

  @Override
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption, ConverterException {
    GPMLToModel gpmlToModel = new GPMLToModel();
    File inputFile = null;
    try {
      Model result = gpmlToModel.getModel(params.getInputStream());
      return result;

    } catch (IOException e) {
      throw new ConverterException("Failed to convert input stream to file.", e);
    } finally {
      assert inputFile == null || inputFile.delete();
    }
  }

  @Override
  public String model2String(Model model) throws InconsistentModelException, ConverterException {
    ModelToGPML modelToGPML = new ModelToGPML(model.getName());
    return modelToGPML.getGPML(model);
  }

  @Override
  public String getCommonName() {
    return "GPML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.XML;
  }

  @Override
  public String getFileExtension() {
    return "gpml";
  }

}
