package lcsb.mapviewer.wikipathway;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LoggerContext;
import org.pathvisio.core.model.*;
import org.pathvisio.desktop.PvDesktop;
import org.pathvisio.desktop.plugin.Plugin;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;
import lcsb.mapviewer.wikipathway.XML.ModelToGPML;

/**
 * Pathvisio plugin that allows to import/export CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 */
public class ImportExport implements Plugin {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(ImportExport.class);
  /**
   * List of extensions supported by import plugin.
   */
  private final String[] importExtensions = new String[] { "xml" };
  /**
   * List of extensions supported by export plugin.
   */
  private final String[] exportExtensions = new String[] { "cell" };

  @Override
  public void init(PvDesktop desktop) {
    try {
      LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
      File file = new File("log4j2.properties");
      context.setConfigLocation(file.toURI());
    } catch (Exception e) {
      e.printStackTrace();
    }
    ExportCellDesigner exportPlugin = new ExportCellDesigner();
    ImportCellDesigner importPlugin = new ImportCellDesigner();
    desktop.getSwingEngine().getEngine().addPathwayImporter(importPlugin);
    desktop.getSwingEngine().getEngine().addPathwayExporter(exportPlugin);
  }

  @Override
  public void done() {
  }

  /**
   * Implementation of {@link PathwayExporter} that allows PathVisio to import
   * from CellDesigner xml file.
   * 
   * @author Piotr Gawron
   * 
   */
  protected class ImportCellDesigner implements PathwayImporter {

    /**
     * List of warnings that occured during conversion.
     */
    private List<LogEvent> warnings = new ArrayList<>();

    /**
     * Default constructor.
     */
    public ImportCellDesigner() {
    }

    @Override
    public String getName() {
      return "CellDesigner";
    }

    @Override
    public String[] getExtensions() {
      return importExtensions;
    }

    @Override
    public List<String> getWarnings() {
      List<String> result = new ArrayList<>();
      for (LogEvent event : warnings) {
        result.add(event.getMessage().getFormattedMessage());
      }
      return result;
    }

    @Override
    public boolean isCorrectType(File arg0) {
      return true;
    }

    @Override
    public Pathway doImport(File file) throws ConverterException {
      MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
      try {
        Pathway pathway = new Pathway();
        String fileName = file.getPath();

        CellDesignerXmlParser parser = new CellDesignerXmlParser();
        Model model = (Model) parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
        String tmp = new ModelToGPML(model.getName()).getGPML(model);
        InputStream stream = new ByteArrayInputStream(tmp.getBytes(StandardCharsets.UTF_8));
        Boolean validate = false;
        pathway.readFromXml(stream, validate);

        MinervaLoggerAppender.unregisterLogEventStorage(appender);
        warnings.addAll(appender.getWarnings());
        return pathway;
      } catch (Exception e) {
        logger.error(e, e);
        throw new ConverterException(e);
      } finally {
        MinervaLoggerAppender.unregisterLogEventStorage(appender);
      }
    }
  }

  /**
   * Implementation of {@link PathwayExporter} that allows PathVisio to export
   * into CellDesigner xml file.
   * 
   * @author Piotr Gawron
   * 
   */
  protected class ExportCellDesigner implements PathwayExporter {

    /**
     * {@link Model} that was created using this {@link PathwayExporter}.
     */
    private Model model = null;

    /**
     * List of export warnings.
     */
    private List<String> warnings = new ArrayList<>();

    @Override
    public String getName() {
      return "CellDesigner";
    }

    @Override
    public String[] getExtensions() {
      return exportExtensions;
    }

    @Override
    public List<String> getWarnings() {
      return warnings;
    }

    @Override
    public void doExport(File file, Pathway pathway) throws ConverterException {
      MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
      try {
        pathway.writeToXml(new File("tmp.gpml"), false);
        model = new GPMLToModel().getModel("tmp.gpml");

        MinervaLoggerAppender.unregisterLogEventStorage(appender);
        warnings = createWarnings(appender);

        CellDesignerXmlParser parser = new CellDesignerXmlParser();
        String xml = parser.model2String(model);
        PrintWriter writer = new PrintWriter(file.getPath(), "UTF-8");
        writer.println(xml);
        writer.close();

        warnings.add("Please manually change extension of saved file from .cell to .xml");
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
        throw new ConverterException(e);
      } finally {
        MinervaLoggerAppender.unregisterLogEventStorage(appender);
      }
    }

    /**
     * Creates list of warnings from log4j appender data.
     * 
     * @param appender
     *          appender with the logs
     * @return list of warnings from log4j appender data
     */
    private List<String> createWarnings(MinervaLoggerAppender appender) {
      List<String> warnings = new ArrayList<>();
      for (LogEvent event : appender.getWarnings()) {
        warnings.add(event.getMessage().getFormattedMessage().replaceAll("\n", "_NEW_LINE_"));
      }
      return warnings;
    }
  }
}
