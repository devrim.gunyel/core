package lcsb.mapviewer.commands;

import static org.junit.Assert.*;

import java.awt.*;
import java.util.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

public class ColorModelCommandTest extends CommandTestFunctions {
  Logger logger = LogManager.getLogger(ColorModelCommandTest.class);

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  /**
   * Test if all elements were copied - if not hibernate will complain
   * 
   * @throws Exception
   * 
   */
  @Test
  public void testColorFullModel() throws Exception {
    ModelComparator comparator = new ModelComparator();
    Model model = getModelForFile("testFiles/sample.xml", false);
    Model model2 = getModelForFile("testFiles/sample.xml", false);
    Model coloredModel = new CopyCommand(model).execute();

    List<ColorSchema> schemas = new ArrayList<>();
    ColorSchema schema = new GenericColorSchema();
    schema.setName("CNC");
    schema.setValue(-1.0);
    schemas.add(schema);
    ColorModelCommand factory = new ColorModelCommand(coloredModel, schemas, colorExtractor);

    assertFalse(Color.RED.equals(coloredModel.getElementByElementId("sa14").getFillColor()));

    factory.execute();

    assertEquals(0, comparator.compare(model, model2));

    assertFalse(comparator.compare(model, coloredModel) == 0);

    assertEquals(Color.RED, coloredModel.getElementByElementId("sa14").getFillColor());
  }

  @Test
  public void testColoring2() throws Exception {
    Model model = getModelForFile("testFiles/coloring_model.xml", true);
    Collection<ColorSchema> schemas = new ArrayList<>();
    ColorSchema schema = new GenericColorSchema();
    schema.addMiriamData(new MiriamData(MiriamType.HGNC, "11138"));
    schema.setValue(1.0);
    schemas.add(schema);
    schema = new GenericColorSchema();
    schema.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:15377"));
    schema.setValue(1.0);
    schemas.add(schema);
    schema = new GenericColorSchema();
    schema.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:15376"));
    schema.setValue(1.0);
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Collection<ColorSchema> missing = factory.getMissingSchema();

    assertEquals(1, missing.size());
  }

  @Test
  public void testReactionColoring1() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);
    Reaction re4 = model.getReactionByReactionId("re4");
    Collection<ColorSchema> schemas = new ArrayList<ColorSchema>();

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    assertFalse(Color.BLACK.equals(re4.getNodes().get(0).getLine().getColor()));

    factory.execute();
    re4 = model.getReactionByReactionId("re4");

    assertEquals(Color.BLACK, re4.getNodes().get(0).getLine().getColor());
  }

  @Test
  public void testReactionColoring2() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);
    Reaction re1 = model.getReactionByReactionId("re1");
    Collection<ColorSchema> schemas = new ArrayList<ColorSchema>();
    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re1");
    schema.setColor(Color.RED);
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    assertEquals(Color.BLACK, re1.getNodes().get(0).getLine().getColor());

    factory.execute();
    re1 = model.getReactionByReactionId("re1");

    assertEquals(Color.RED, re1.getNodes().get(0).getLine().getColor());
  }

  @Test
  public void testReactionColoring3() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);
    Reaction re2 = model.getReactionByReactionId("re2");
    Collection<ColorSchema> schemas = new ArrayList<ColorSchema>();
    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re2");
    schema.setValue(-1.0);
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    assertEquals(Color.BLACK, re2.getNodes().get(0).getLine().getColor());

    factory.execute();
    re2 = model.getReactionByReactionId("re2");

    assertEquals(Color.RED, re2.getNodes().get(0).getLine().getColor());
  }

  @Test
  public void testReactionColoring4() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);
    Reaction re3 = model.getReactionByReactionId("re3");
    Collection<ColorSchema> schemas = new ArrayList<ColorSchema>();
    ColorSchema schema = new GenericColorSchema();
    schema.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
    schema.setValue(-1.0);
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);

    assertEquals(Color.BLACK, re3.getNodes().get(0).getLine().getColor());

    factory.execute();
    re3 = model.getReactionByReactionId("re3");

    assertEquals(Color.RED, re3.getNodes().get(0).getLine().getColor());
  }

  @Test
  public void testColoringComplexModel() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);
    Model model2 = getModelForFile("testFiles/sample.xml", false);

    model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.UNKNOWN, "BLA"));

    Collection<ColorSchema> schemas = new ArrayList<>();
    Model coloredModel = new CopyCommand(model).execute();
    new ColorModelCommand(coloredModel, schemas, colorExtractor).execute();

    Model coloredModel2 = coloredModel.getSubmodelConnections().iterator().next().getSubmodel().getModel();
    Model coloredModel3 = coloredModel.getSubmodelByConnectionName("BLA");

    assertFalse(
        coloredModel2.getElementByElementId("sa2").getFillColor().equals(model2.getElementByElementId("sa2").getFillColor()));
    assertFalse(
        coloredModel3.getElementByElementId("sa2").getFillColor().equals(model2.getElementByElementId("sa2").getFillColor()));
  }

  @Test
  public void testColoredAliases() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);
    Collection<ColorSchema> schemas = new ArrayList<>();
    ColorSchema schema = new GenericColorSchema();
    schema.setName("CNC");
    schema.setColor(Color.BLUE);
    schemas.add(schema);
    schema = new GenericColorSchema();
    schema.setName("BDH1");
    schema.setColor(Color.BLUE);
    schemas.add(schema);
    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);

    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(2, map.size());
    for (Map.Entry<Object, ColorSchema> entry : map.entrySet()) {
      assertTrue(entry.getKey() instanceof Element);
    }
  }

  @Test
  public void testColoredReactions() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);
    Collection<ColorSchema> schemas = new ArrayList<>();
    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re1");
    schema.setLineWidth(3.0);
    schema.setColor(Color.BLUE);
    schema.setName("");
    schema.setReverseReaction(true);
    schemas.add(schema);
    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);

    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(0, map.size());
  }

  @Test
  public void testColoredReactions2() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);
    Collection<ColorSchema> schemas = new ArrayList<>();
    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re1");
    schema.setLineWidth(3.0);
    schema.setColor(Color.BLUE);
    schema.setName(null);
    schema.setReverseReaction(true);
    schemas.add(schema);
    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);

    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(1, map.size());
    for (Map.Entry<Object, ColorSchema> entry : map.entrySet()) {
      assertTrue(entry.getKey() instanceof Reaction);
      assertEquals("re1", ((Reaction) entry.getKey()).getIdReaction());
      assertEquals(entry.getValue(), schema);
    }
  }

  @Test
  public void testAliasMatchWithInvalidElementId() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.setName(null);
    colorSchema.setElementId("1");

    BioEntity protein = new GenericProtein("id");
    protein.setName("test");

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertFalse(factory.match(protein, colorSchema));

    colorSchema.setElementId(null);
    assertTrue(factory.match(protein, colorSchema));
  }

  @Test
  public void testSpeciesMatchWithProteinType() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.setName("s1");
    colorSchema.addType(Protein.class);

    GenericProtein species = new GenericProtein("id");
    species.setName("s1");

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertTrue(factory.match(species, colorSchema));
  }

  @Test
  public void testSpeciesMatchWithMiriamData() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.setName("s1");
    colorSchema.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));

    GenericColorSchema colorSchema2 = new GenericColorSchema();
    colorSchema2.setName("s1");
    colorSchema2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "PARK7"));

    GenericProtein species = new GenericProtein("id");
    species.setName("s1");
    species.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertTrue(factory.match(species, colorSchema));
    assertFalse(factory.match(species, colorSchema2));
  }

  @Test
  public void testSpeciesMatchWithMiriamDataDifferentAnnotator() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.setName("s1");
    colorSchema.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));

    GenericProtein species = new GenericProtein("id");
    species.setName("s1");
    species.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA", Object.class));

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertTrue(factory.match(species, colorSchema));
  }

  @Test
  public void testReactionMatchWithProteinMiriamData() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));

    Reaction reaction = new Reaction();

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertFalse(factory.match(reaction, colorSchema));
  }

  @Test
  public void testReactionMatchWithMiriamData() throws Exception {
    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    Reaction reaction = new Reaction();
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(new ModelFullIndexed(null), schemas, colorExtractor);

    assertTrue(factory.match(reaction, colorSchema));
  }

  @Test
  public void testReactionColoringWithModelNotMatching() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re4");
    schema.setName(null);
    schema.setModelName(model.getName() + "XXX");

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(0, map.values().size());
  }

  @Test
  public void testReactionColoringWithModelMatch() throws Exception {
    Model model = getModelForFile("testFiles/reactions_to_color.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("re4");
    schema.setName(null);
    schema.setModelName(model.getName());

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(1, map.values().size());
  }

  @Test
  public void testAliasColoringWithModelNotMatching() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setName("CNC");
    schema.setModelName(model.getName() + "XXX");

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(0, map.values().size());
  }

  @Test
  public void testAliasColoringWithModelMatch() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setName("CNC");
    schema.setModelName(model.getName());

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(1, map.values().size());
  }

  public void testAliasColoringWithUnknownElementSourceId() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setElementId("xxx");

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(0, map.values().size());
  }

  @Test
  public void testAliasColoringWithElementIdMatch() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);

    ColorSchema schema = new GenericColorSchema();
    schema.setElementId(model.getElements().iterator().next().getElementId());

    Collection<ColorSchema> schemas = new ArrayList<>();
    schemas.add(schema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
    Map<Object, ColorSchema> map = factory.getModifiedElements();
    assertEquals(1, map.values().size());
  }

  @Test
  public void testGetModifiedElements() throws Exception {
    Reaction reaction = new Reaction();
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    Model model = new ModelFullIndexed(null);
    model.addReaction(reaction);

    GenericColorSchema colorSchema = new GenericColorSchema();
    colorSchema.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    List<ColorSchema> schemas = new ArrayList<>();
    schemas.add(colorSchema);

    ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);

    assertNotNull(factory.getModifiedElements().get(reaction));
  }

}
