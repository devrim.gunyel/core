package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;

import org.junit.*;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

public class ChangeElementMiriamDataCommandTest extends CommandTestFunctions {

  Model model;

  @Before
  public void setUp() throws Exception {
    model = createSimpleModel();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testWithoutChange() throws Exception {
    Element alias = model.getElementByElementId("alias_id");
    MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    alias.addMiriamData(md);
    ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
    command.execute();
    assertTrue(alias.getMiriamData().contains(md2));
    assertEquals(1, alias.getMiriamData().size());
  }

  @Test
  public void testInvalidChange() throws Exception {
    Element alias = model.getElementByElementId("alias_id");
    MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA2");
    alias.addMiriamData(md);
    alias.addMiriamData(md2);
    ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
    try {
      command.execute();
      fail("Exception expected");
    } catch (CommandExecutionException e) {
      assertTrue(alias.getMiriamData().contains(md));
      assertTrue(alias.getMiriamData().contains(md2));
    }
  }

  @Test
  public void testChange() throws Exception {
    Element alias = model.getElementByElementId("alias_id");
    MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA2");
    MiriamData md3 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    alias.addMiriamData(md);
    ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
    command.execute();
    assertTrue(alias.getMiriamData().contains(md2));
    assertFalse(alias.getMiriamData().contains(md3));
  }

}
