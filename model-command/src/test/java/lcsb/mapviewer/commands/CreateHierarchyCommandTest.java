package lcsb.mapviewer.commands;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class CreateHierarchyCommandTest extends CommandTestFunctions {
  Logger logger = LogManager.getLogger(CreateHierarchyCommandTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCyclicComplexes() throws Exception {
    Model model = getModelForFile("testFiles/cyclic_hierarchy_problem.xml", false);

    new CreateHierarchyCommand(model, 8, 80).execute();

    Species alias = model.getElementByElementId("sa5033");

    Set<Element> parents = new HashSet<Element>();
    while (alias.getComplex() != null) {
      assertFalse("Cyclic nesting", parents.contains(alias.getComplex()));
      alias = alias.getComplex();
      parents.add(alias);
    }

    Set<String> levels = new HashSet<>();
    for (Element a : model.getElements()) {
      levels.add(a.getVisibilityLevel());
    }
    assertTrue(levels.size() > 2);
  }

  @Test
  public void testCreateHierachy() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    new CreateHierarchyCommand(model, 2, 2).execute();
    // check if second call will throw an exception...
    new CreateHierarchyCommand(model, 2, 2).execute();
  }

  @Test
  public void testCreateHierachy2() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    new CreateHierarchyCommand(model, 2, 2).execute();

    boolean artifitial = false;
    for (Compartment a : model.getCompartments()) {
      if (a instanceof PathwayCompartment) {
        artifitial = true;
      }
    }
    assertTrue("No hierarchical structure element found", artifitial);
  }

  @Test
  public void testRecreateHierachy() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    int aliasSize0 = model.getCompartments().size();

    new CreateHierarchyCommand(model, 2, 2).execute();

    int aliasSize = model.getCompartments().size();

    assertTrue(aliasSize0 != aliasSize);
    new CreateHierarchyCommand(model, 2, 2).execute();

    int aliasSize2 = model.getCompartments().size();

    assertEquals(aliasSize, aliasSize2);
  }

  @Test
  public void testParenting() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    new CreateHierarchyCommand(model, 2, 2).execute();

    assertFalse(model.getElementByElementId("sa1").getCompartment() instanceof PathwayCompartment);
  }

  @Test
  public void testCreateComponentsMinVisibility() throws Exception {
    double zoomFactor = 39.0625;
    int levels = 6;

    Model model = getModelForFile("testFiles/other_full/GSTP1 subnetwork_220214.xml", false);

    new CreateHierarchyCommand(model, levels, zoomFactor).execute();

    for (Compartment compartment : model.getCompartments()) {
      if (compartment.getCompartment() == null) {
        int visibilityLevel = Integer.valueOf(compartment.getVisibilityLevel());
        assertTrue("Alias " + compartment.getElementId() + " is not visible at levels highers than "
            + compartment.getVisibilityLevel(), visibilityLevel <= 1);
      }
    }

    for (Species species : model.getSpeciesList()) {
      if (species.getCompartment() == null) {
        int visibilityLevel = Integer.valueOf(species.getVisibilityLevel());
        assertTrue("Alias " + species.getElementId() + " is not visible at levels highers than "
            + species.getVisibilityLevel(), visibilityLevel <= 1);
      }
    }
  }

  @Test
  public void testCreateComponentsMaxVisibility() throws Exception {
    double zoomFactor = 39.0625;
    int levels = 6;

    Model model = getModelForFile("testFiles/other_full/GSTP1 subnetwork_220214.xml", false);

    new CreateHierarchyCommand(model, levels, zoomFactor).execute();

    for (Element element : model.getElements()) {
      int visibilityLevel = Integer.valueOf(element.getVisibilityLevel());
      assertTrue("Alias " + element.getElementId() + " is not visible even at the bottom level (visibility: "
          + element.getVisibilityLevel() + ") ", visibilityLevel <= levels);
    }
  }

  @Test
  public void testCompactComplexesInNestedView() throws Exception {
    Model model = getModelForFile("testFiles/problematic/compact_complex_view_problem.xml", false);
    Element alias = model.getElementByElementId("sa1");
    Object parent1 = alias.getCompartment();
    new CreateHierarchyCommand(model, 3, 16).execute();
    Object parent2 = alias.getCompartment();
    assertEquals(parent1, parent2);
  }

  @Test(expected = InvalidStateException.class)
  public void testRecallHierachyCreation() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    CreateHierarchyCommand command = new CreateHierarchyCommand(model, 2, 2);
    command.execute();
    command.execute();
  }

  @Test
  public void testClear() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);

    CreateHierarchyCommand command = new CreateHierarchyCommand(model, 2, 2);
    command.clean();
    for (Element alias : model.getElements()) {
      assertNull(alias.getCompartment());
    }
    for (Compartment alias : model.getCompartments()) {
      assertNull(alias.getCompartment());
      assertTrue(alias.getElements().isEmpty());
    }
  }

  @Test
  public void testNestedProblem() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/nested_test.xml").sizeAutoAdjust(true));

    double zoomFactor = Math.max(model.getHeight(), model.getWidth()) / (256);
    int zoomLevels = (int) Math.ceil(Math.log(zoomFactor) / Math.log(2));

    CreateHierarchyCommand command = new CreateHierarchyCommand(model, zoomLevels, zoomFactor);
    command.execute();

    for (Element alias : model.getElements()) {
      Element parentAlias = alias.getCompartment();
      if (parentAlias != null) {
        int parentVisibilityLevel = Integer.valueOf(parentAlias.getVisibilityLevel());
        int aliasVisibilityLevel = Integer.valueOf(alias.getVisibilityLevel());
        assertTrue(aliasVisibilityLevel >= parentVisibilityLevel);
      }
      if (alias instanceof Species) {
        parentAlias = ((Species) alias).getComplex();
        if (parentAlias != null) {
          int parentVisibilityLevel = Integer.valueOf(parentAlias.getVisibilityLevel());
          int aliasVisibilityLevel = Integer.valueOf(alias.getVisibilityLevel());
          assertTrue(aliasVisibilityLevel >= parentVisibilityLevel);
        }
      }
    }
  }

  @Test
  public void testDisconnectedChildInComplex() throws Exception {
    Model model = getModelForFile("testFiles/problematic/disconnected_child_in_complex.xml", false);

    new CreateHierarchyCommand(model, 8, 80).execute();

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xmlString = parser.model2String(model);

    InputStream stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    parser.createModel(new ConverterParams().inputStream(stream));
  }

  @Test
  public void testHierarchyWithCustomSemanticZooming() throws Exception {
    Model model = getModelForFile("testFiles/custom_semantic_zooming.xml", false);

    Map<Element, String> visibilityLevels = new HashMap<>();
    for (Element element : model.getElements()) {
      if (element.getVisibilityLevel() != null && !element.getVisibilityLevel().isEmpty()) {
        visibilityLevels.put(element, element.getVisibilityLevel());
      }
    }

    new CreateHierarchyCommand(model, 4, 80).execute();

    for (Element element : model.getElements()) {
      if (visibilityLevels.get(element) != null) {
        assertEquals("Visibility level changed, but shouldn't", visibilityLevels.get(element),
            element.getVisibilityLevel());
      }
    }
  }

  @Test
  public void testHierarchyWithCustomSemanticZoomingPathwayLevels() throws Exception {
    ElementUtils eu = new ElementUtils();
    Model model = getModelForFile("testFiles/custom_semantic_zooming.xml", false);

    Map<Element, String> visibilityLevels = new HashMap<>();
    for (Element element : model.getElements()) {
      if (element.getVisibilityLevel() != null && !element.getVisibilityLevel().isEmpty()) {
        visibilityLevels.put(element, element.getVisibilityLevel());
      }
    }

    new CreateHierarchyCommand(model, 4, 80).execute();

    for (Element element : model.getElements()) {
      if (visibilityLevels.get(element) == null) {
        int visibilityLevel = Integer.parseInt(element.getVisibilityLevel());
        if (element.getCompartment() != null) {
          int parentTransparency = Integer.parseInt(element.getCompartment().getTransparencyLevel());
          assertEquals(
              eu.getElementTag(element) + eu.getElementTag(element.getCompartment())
                  + "Element should be directly visible when parent is transparent",
              parentTransparency, visibilityLevel);
        } else if (element instanceof Species) {
          int parentTransparency = Integer.parseInt(((Species) element).getComplex().getTransparencyLevel());
          assertEquals(
              eu.getElementTag(element) + eu.getElementTag(((Species) element).getComplex())
                  + "Element should be directly visible when parent is transparent",
              parentTransparency, visibilityLevel);
        }
      }
    }
  }

  @Test
  public void testLayerWithNotes() throws Exception {
    Model model = getModelForFile("testFiles/layer_text_with_notes.xml", false);

    new CreateHierarchyCommand(model, 4, 80).execute();

    PathwayCompartment pathway = (PathwayCompartment) model.getCompartments().get(0);

    assertEquals("test", pathway.getName());
    assertEquals("5", pathway.getVisibilityLevel());
    assertEquals((Integer) 11, pathway.getZ());
  }

}
