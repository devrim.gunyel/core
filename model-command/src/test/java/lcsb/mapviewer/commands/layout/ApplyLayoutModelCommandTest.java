package lcsb.mapviewer.commands.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.junit.Test;

import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class ApplyLayoutModelCommandTest {

  @Test
  public void testGetStartPoint() {
    Double minX = 1.0;
    Double minY = 2.0;
    ApplyLayoutModelCommand command = new ApplySimpleLayoutModelCommand(new ModelFullIndexed(null), new ArrayList<>(),
        minX, minY, null, null, false);

    Point2D minPoint = command.getStartPoint();
    assertNotNull(minPoint);
    assertEquals(minX, (Double) minPoint.getX());
    assertEquals(minY, (Double) minPoint.getY());
  }

  @Test
  public void testGetStartDimension() {
    Double maxX = 1.0;
    Double maxY = 2.0;
    ApplyLayoutModelCommand command = new ApplySimpleLayoutModelCommand(new ModelFullIndexed(null), new ArrayList<>(),
        0.0, 0.0, maxX, maxY, false);

    Dimension2D minPoint = command.getStartDimension();
    assertNotNull(minPoint);
    assertEquals(maxX, (Double) minPoint.getWidth());
    assertEquals(maxY, (Double) minPoint.getHeight());
  }

}
