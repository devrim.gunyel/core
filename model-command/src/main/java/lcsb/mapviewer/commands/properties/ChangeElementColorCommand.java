package lcsb.mapviewer.commands.properties;

import java.awt.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that
 * {@link Element#color}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementColorCommand extends ChangeElementPropertyCommand<Color> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(ChangeElementColorCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param newName
   *          new {@link Element#color} value
   */
  public ChangeElementColorCommand(Model model, Element alias, Color newName) {
    super(model, alias, newName);
  }

  @Override
  protected void executeImplementation() {

    includeInAffectedRegion(getAlias());

    setOldValue(getAlias().getFillColor());
    getAlias().setFillColor((Color) getNewValue());

    includeInAffectedRegion(getAlias());
  }
}
