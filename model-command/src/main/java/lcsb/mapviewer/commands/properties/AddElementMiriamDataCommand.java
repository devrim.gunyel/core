package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * {@link MiriamData annotations} of the {@link Element Element} .
 * 
 * @author Piotr Gawron
 *
 */
public class AddElementMiriamDataCommand extends AddElementPropertyListEntryCommand<MiriamData> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(AddElementMiriamDataCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          new annotation values to be added
   */
  public AddElementMiriamDataCommand(Model model, Element alias, List<MiriamData> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getMiriamData().removeAll(getNewValues());

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (MiriamData md : getNewValues()) {
      if (getAlias().getMiriamData().contains(md)) {
        throw new CommandExecutionException("Cannot add miriam \"" + md + "\" to the list. Element already exists.");
      }
    }

    getAlias().getMiriamData().addAll(getNewValues());

    // miriam data is not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
