package lcsb.mapviewer.commands.properties;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that removes former
 * symbols from {@link Element Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class RemoveElementFormerSymbolsCommand extends RemoveElementPropertyListEntryCommand<String> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(RemoveElementFormerSymbolsCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link lcsb.mapviewer.commands.ModelCommand#model}
   * @param alias
   *          {@link ChangeElementPropertyCommand#alias}
   * @param values
   *          {@link Element#synonym} values to be removed
   */
  public RemoveElementFormerSymbolsCommand(Model model, Element alias, List<String> values) {
    super(model, alias, values);
  }

  @Override
  protected void undoImplementation() {
    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    getAlias().getFormerSymbols().addAll(getValues());

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());

    for (String string : getValues()) {
      if (!getAlias().getFormerSymbols().contains(string)) {
        throw new CommandExecutionException(
            "Cannot remove former symbol \"" + string + "\" from the list. Element doesn't exist.");
      }
    }

    getAlias().getFormerSymbols().removeAll(getValues());

    // former symbols are not visualized, so we don't need to report any
    // visualization changes
    // includeInAffectedRegion(getAlias());
  }
}
