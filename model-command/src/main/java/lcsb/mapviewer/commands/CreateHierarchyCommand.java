package lcsb.mapviewer.commands;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.*;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.*;

/**
 * This {@link ModelCommand command} class allows to transform model into
 * multilevel (nested) component structure. Some artificial compartments will
 * appear. All compartments have information about children compartments. All
 * objects have information about parents.
 * 
 * 
 */
public class CreateHierarchyCommand extends ModelCommand {
  /**
   * Natural logarithm of four.
   */
  private static final double LOG_4 = Math.log(4);
  /**
   * Top left corner x coordinate of the text associated with compartment.
   */
  private static final double DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT = 10;
  /**
   * Top left corner y coordinate of the text associated with compartment.
   */
  private static final double DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT = 10;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CreateHierarchyCommand.class);
  /**
   * How many levels are possible.
   */
  private int zoomLevels;
  /**
   * What is the maximum zoom factor in the view.
   */
  private double maxZoomFactor;

  /**
   * Default constructor that initializes data.
   *
   * @param model
   *          model on which command will be executed
   * @param maxZoomFactor
   *          what is the maximum zoom out factor
   * @param zoomLevels
   *          how many levels are possible
   */
  public CreateHierarchyCommand(Model model, int zoomLevels, double maxZoomFactor) {
    super(model);
    this.zoomLevels = zoomLevels;
    this.maxZoomFactor = maxZoomFactor;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {
    if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
      throw new InvalidStateException(
          "To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
    }
    Model model = getModel();
    List<Species> compacts = new ArrayList<>();
    for (Element alias : model.getElements()) {
      if (alias instanceof Complex) {
        if (((Complex) alias).getState().contains("brief")) {
          compacts.add((Species) alias);
          compacts.addAll(((Complex) alias).getElements());
        }
      }
    }
    model.getElements().removeAll(compacts);
    clean();
    createArtificials();
    assignAliases();

    List<Element> sortedAliases = model.getElementsSortedBySize();
    setParentingAndChildreningOfNonComplexChildrens(sortedAliases);
    model.getElements().addAll(compacts);
    sortedAliases = model.getElementsSortedBySize();
    settingOfTransparencyLevel(sortedAliases);

    setDefaultVisibilityLevel(sortedAliases);

    setStatus(ModelCommandStatus.EXECUTED);
  }

  private void setDefaultVisibilityLevel(List<Element> sortedAliases) {
    for (Element element : sortedAliases) {
      if (element.getVisibilityLevel() == null || element.getVisibilityLevel().isEmpty()) {
        element.setVisibilityLevel(0);
      }
    }
  }

  /**
   * Cleans hierarchical information from the model.
   */
  protected void clean() {
    for (Element alias : getModel().getElements()) {
      alias.setCompartment(null);
      if (alias.getTransparencyLevel() == null || alias.getTransparencyLevel().isEmpty()) {
        alias.setTransparencyLevel("");
      }
    }
    Set<Compartment> toRemove = new HashSet<>();
    for (Compartment alias : getModel().getCompartments()) {
      if (alias instanceof PathwayCompartment) {
        toRemove.add(alias);
      }
    }
    for (Compartment alias : toRemove) {
      getModel().removeElement(alias);
    }
  }

  /**
   * Creates artificial compartment alias for rectangles and texts object on
   * additional graphic layers.
   */
  private void createArtificials() {
    RestAnnotationParser rap = new RestAnnotationParser();
    Model model = getModel();
    int id = 0;
    for (Layer layer : model.getLayers()) {
      for (LayerRect rect : layer.getRectangles()) {
        PathwayCompartment compartment = new PathwayCompartment("art" + (id++));
        compartment.setX(rect.getX());
        compartment.setY(rect.getY());
        compartment.setWidth(rect.getWidth());
        compartment.setHeight(rect.getHeight());
        compartment.setFontColor(rect.getColor());
        compartment.setNamePoint(new Point2D.Double(rect.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT,
            rect.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT));
        compartment.setZ(rect.getZ());

        model.addElement(compartment);
      }
      for (LayerText text : layer.getTexts()) {
        PathwayCompartment compartment = new PathwayCompartment("art" + (id++));
        compartment.setX(text.getX());
        compartment.setY(text.getY());
        compartment.setWidth(text.getWidth());
        compartment.setHeight(text.getHeight());
        compartment.setFontColor(text.getColor());
        compartment.setFillColor(text.getBackgroundColor());
        compartment.setBorderColor(text.getBorderColor());
        compartment.setName(extractNameFromText(text.getNotes()));
        compartment.setNotes(extractNotesFromText(text.getNotes()));
        compartment.setZ(text.getZ());
        compartment.setGlyph(text.getGlyph());
        rap.processNotes(compartment);

        compartment.setNamePoint(new Point2D.Double(text.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT,
            text.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT));

        model.addElement(compartment);

      }
    }
  }

  private String extractNotesFromText(String notes) {
    if (notes.indexOf("\n") >= 0) {
      return notes.substring(notes.indexOf("\n"));
    } else {
      return "";
    }
  }

  private String extractNameFromText(String notes) {
    if (notes.indexOf("\n") >= 0) {
      return notes.substring(0, notes.indexOf("\n"));
    } else {
      return notes;
    }
  }

  /**
   * Assign aliases in hierarchical structure.
   */
  private void assignAliases() {
    assignToCompartments();
  }

  private int computeVisibility(Element alias) {
    if (!NumberUtils.isCreatable(alias.getVisibilityLevel())) {
      double rate = computeRate(alias, Configuration.MIN_VISIBLE_OBJECT_SIZE);
      int logValue = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      boolean hasCompartment = alias.getCompartment() != null;
      boolean hasComplex = false;
      if (alias instanceof Species) {
        hasComplex = ((Species) alias).getComplex() != null;
      }
      if (!hasCompartment && !hasComplex) {
        logValue = 0;
      }
      if (logValue >= zoomLevels) {
        if (hasComplex) {
          logValue = zoomLevels - 1;
        } else {
          logValue = zoomLevels;
        }
      }
      return logValue;
    } else {
      return Integer.valueOf(alias.getVisibilityLevel());
    }
  }

  /**
   * Sets transparency level in hierarchical view for compartment alias.
   *
   * @param compartment
   *          compartment alias
   */
  private void settingTransparencyLevelForCompartment(Compartment compartment) {
    if (compartment.getTransparencyLevel() == null || compartment.getTransparencyLevel().isEmpty()) {
      int maxVisibilityLevel = Integer.MAX_VALUE;
      double rate = computeRate(compartment, Configuration.MAX_VISIBLE_OBJECT_SIZE);
      maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      for (Element child : compartment.getElements()) {
        maxVisibilityLevel = Math.min(maxVisibilityLevel, Integer.valueOf(computeVisibility(child)));
      }
      if (maxVisibilityLevel >= zoomLevels) {
        maxVisibilityLevel = zoomLevels;
      }
      if (maxVisibilityLevel <= 0) {
        maxVisibilityLevel = 1;
      }
      compartment.setTransparencyLevel(maxVisibilityLevel + "");
    }
    for (Element child : compartment.getElements()) {
      if (child.getVisibilityLevel() == null || child.getVisibilityLevel().isEmpty()) {
        child.setVisibilityLevel(compartment.getTransparencyLevel());
      }
    }
  }

  /**
   * Sets transparency level in hierarchical view for complex alias.
   *
   * @param complex
   *          complex alias
   */
  private void settingTransparencyLevelForComplex(Complex complex) {
    if (complex.getTransparencyLevel() == null || complex.getTransparencyLevel().isEmpty()) {
      int maxVisibilityLevel = Integer.MAX_VALUE;
      double rate = computeRate(complex, Configuration.MAX_VISIBLE_OBJECT_SIZE);
      maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      for (Element child : complex.getElements()) {
        maxVisibilityLevel = Math.min(maxVisibilityLevel, Integer.valueOf(computeVisibility(child)));
      }
      if (maxVisibilityLevel >= zoomLevels) {
        maxVisibilityLevel = zoomLevels;
      }
      if (maxVisibilityLevel <= 0) {
        maxVisibilityLevel = 1;
      }
      complex.setTransparencyLevel(maxVisibilityLevel + "");
    }
    for (Element child : complex.getElements()) {
      if (child.getVisibilityLevel() == null || child.getVisibilityLevel().isEmpty()) {
        child.setVisibilityLevel(complex.getTransparencyLevel());
      }
    }
  }

  /**
   * Sets transparency level in hierarchical view for all elements.
   *
   * @param sortedAliases
   *          list of aliases
   */
  private void settingOfTransparencyLevel(List<Element> sortedAliases) {
    for (Element alias : sortedAliases) {
      if (alias instanceof Compartment) {
        settingTransparencyLevelForCompartment((Compartment) alias);
      } else if (alias instanceof Complex) {
        settingTransparencyLevelForComplex((Complex) alias);
      } else {
        if (alias.getTransparencyLevel() == null || alias.getTransparencyLevel().isEmpty()) {
          alias.setTransparencyLevel("0");
        }
      }
    }
  }

  /**
   * Removes invalid compartment children.
   *
   * @param sortedAliases
   *          list of aliases
   */
  private void setChildrening(List<Element> sortedAliases) {
    for (Element compartment : sortedAliases) {
      if (compartment instanceof Compartment) {
        Set<Element> removable = new HashSet<>();
        for (Element alias : ((Compartment) compartment).getElements()) {
          if (alias.getCompartment() != compartment) {
            removable.add(alias);
          }
        }
        ((Compartment) compartment).getElements().removeAll(removable);
      }
    }
  }

  /**
   * Sets parent for elements.
   *
   * @param sortedAliases
   *          list of aliases
   */
  private void setParentingOfNonComplexChildrens(List<Element> sortedAliases) {
    for (Element element : getModel().getElements()) {
      if (element.getCompartment() == null) {
        for (Element compartment : sortedAliases) {
          if (compartment instanceof Compartment) {
            if (((Compartment) compartment).getElements().contains(element)) {
              element.setCompartment((Compartment) compartment);
            }
          }
        }

      }
      if (element instanceof Species) {
        Species species = (Species) element;
        if (species.getComplex() == null) {
          for (Element complex : sortedAliases) {
            if (complex instanceof Complex) {
              if (((Complex) complex).getElements().contains(element)) {
                species.setComplex((Complex) complex);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Set parents for the elements in hierarchical view.
   *
   * @param sortedAliases
   *          list of aliases
   */
  private void setParentingAndChildreningOfNonComplexChildrens(List<Element> sortedAliases) {
    setParentingOfNonComplexChildrens(sortedAliases);
    setChildrening(sortedAliases);
  }

  /**
   * Assign aliases to compartments.
   */
  private void assignToCompartments() {
    Compartment nullCompartment = new Compartment("null");
    nullCompartment.setWidth(Double.MAX_VALUE);
    nullCompartment.setHeight(Double.MAX_VALUE);
    for (Element element : getModel().getElements()) {
      Compartment supposedParent = nullCompartment;
      for (Compartment alias : getModel().getCompartments()) {
        if (alias.contains(element) && alias.getSize() < supposedParent.getSize()) {
          supposedParent = alias;
        }
      }
      if (supposedParent != nullCompartment) {
        supposedParent.addElement(element);
      }
    }
  }

  /**
   * Computes the ratio between the minimal object that should be visible, and
   * alias visible on the top level.
   *
   * @param alias
   *          alias for which computation is done
   * @param limit
   *          size of the minimal visible object
   * @return ratio between the minimal object that should be visible, and alias
   *         visible on the top level
   */
  private double computeRate(Element alias, double limit) {
    double length = alias.getWidth() / maxZoomFactor;
    double height = alias.getHeight() / maxZoomFactor;
    double size = length * height;
    return (double) Math.ceil(limit / size);
  }

}
