package lcsb.mapviewer.commands;

import lcsb.mapviewer.model.map.model.Model;

/**
 * Abstract class representing operation on the model that creates a new model
 * without changing the old one (kind of creational pattern).
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class NewModelCommand {

  /**
   * Model on which command is performed.
   */
  private Model model;

  /**
   * Default constructor.
   *
   * @param model
   *          {@link #model}
   */
  public NewModelCommand(Model model) {
    this.model = model;
  }

  /**
   * Executed the operation.
   *
   * @return new {@link Model} object accoriding to initialization params
   */
  public abstract Model execute();

  /**
   * @return the model
   * @see #model
   */
  protected Model getModel() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  protected void setModel(Model model) {
    this.model = model;
  }
}
