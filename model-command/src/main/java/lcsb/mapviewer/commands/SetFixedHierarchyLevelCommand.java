package lcsb.mapviewer.commands;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This {@link ModelCommand command} class allows to transform model into
 * multilevel (nested) component structure. Some artificial compartments will
 * appear. All compartments have information about children compartments. All
 * objects have information about parents.
 * 
 * 
 */
public class SetFixedHierarchyLevelCommand extends ModelCommand {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SetFixedHierarchyLevelCommand.class);
  private Integer level;

  public SetFixedHierarchyLevelCommand(Model model, Integer level) {
    super(model);
    this.level = level;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {
    if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
      throw new InvalidStateException(
          "To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
    }
    if (level != null) {
      SemanticZoomLevelMatcher matcher = new SemanticZoomLevelMatcher();
      Set<Model> models = new HashSet<>();
      Model output = getModel();
      models.add(output);
      models.addAll(output.getSubmodels());
      for (Model model : models) {
        for (BioEntity bioEntity : model.getBioEntities()) {
          if (matcher.isVisible(level, bioEntity.getVisibilityLevel())) {
            bioEntity.setVisibilityLevel("0");
          } else {
            bioEntity.setVisibilityLevel(Integer.MAX_VALUE + "");
          }
        }
        for (Element element : model.getElements()) {
          if (matcher.isVisible(level, element.getTransparencyLevel())) {
            element.setTransparencyLevel("0");
          } else {
            element.setTransparencyLevel(Integer.MAX_VALUE + "");
          }
        }
      }
    }

    setStatus(ModelCommandStatus.EXECUTED);
  }

}
