package lcsb.mapviewer.commands;

/**
 * Exception that is thrown when the data for {@link ModelCommand} is invalid.
 * 
 * @author Piotr Gawron
 *
 */
public class CommandExecutionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public CommandExecutionException() {
    super();
  }

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public CommandExecutionException(String string) {
    super(string);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */
  public CommandExecutionException(Exception e) {
    super(e.getMessage(), e);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param string
   *          message of this exception
   * @param e
   *          parent exception
   */
  public CommandExecutionException(String string, Exception e) {
    super(string, e);
  }

}
