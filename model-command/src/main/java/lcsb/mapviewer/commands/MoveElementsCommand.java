package lcsb.mapviewer.commands;

import java.awt.geom.Point2D;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

/**
 * Command which moves elements in model by dx, dy coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class MoveElementsCommand extends ModelCommand {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(MoveElementsCommand.class);

  /**
   * Delta x.
   */
  private double dx;

  /**
   * Delta y.
   */
  private double dy;

  /**
   * List of objects to move.
   */
  private List<Object> objectsToMove = new ArrayList<>();

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * @param elements
   *          elements that should be moved
   * @param dx
   *          delta x
   * @param dy
   *          delta y
   */
  public MoveElementsCommand(Model model, Collection<? extends BioEntity> elements, double dx, double dy) {
    super(model);
    this.dx = dx;
    this.dy = dy;
    for (Object object : elements) {
      if (object instanceof Element) {
        if (((Element) object).getModel() != model) {
          throw new InvalidArgumentException("Object doesnt belong to specified model: " + object);
        }
      } else if (object instanceof Reaction) {
        if (((Reaction) object).getModel() != model) {
          throw new InvalidArgumentException("Object doesnt belong to specified model: " + object);
        }
      } else {
        throw new InvalidArgumentException("Cannot move element: " + object);
      }
    }
    objectsToMove.addAll(elements);
  }

  @Override
  protected void undoImplementation() {
    dx = -dx;
    dy = -dy;
    executeImplementation();
    dx = -dx;
    dy = -dy;
    setStatus(ModelCommandStatus.UNDONE);
  }

  @Override
  protected void redoImplementation() {
    executeImplementation();
    setStatus(ModelCommandStatus.EXECUTED);
  }

  @Override
  protected void executeImplementation() {
    Set<Element> aliases = new HashSet<>();

    for (Object object : objectsToMove) {
      if (object instanceof Element) {
        Element alias = (Element) object;

        includeInAffectedRegion(alias);

        alias.setX(alias.getX() + dx);
        alias.setY(alias.getY() + dy);
        if (alias instanceof Compartment) {
          ((Compartment) alias)
              .setNamePoint(((Compartment) alias).getNamePoint().getX() + dx,
                  ((Compartment) alias).getNamePoint().getY() + dy);
        }
        if (alias instanceof SpeciesWithModificationResidue) {
          for (ModificationResidue mr : ((SpeciesWithModificationResidue) alias).getModificationResidues()) {
            Point2D position = mr.getPosition();
            position.setLocation(position.getX() + dx, position.getY() + dy);
          }
        }
        includeInAffectedRegion(alias);

        aliases.add(alias);
      } else if (object instanceof Reaction) {
        Reaction reaction = (Reaction) object;
        for (Reactant node : reaction.getReactants()) {
          for (int i = 1; i < node.getLine().getPoints().size(); i++) {
            Point2D point = node.getLine().getPoints().get(i);
            node.getLine().getPoints().set(i, new Point2D.Double(point.getX() + dx, point.getY() + dy));
          }
        }
        for (Product node : reaction.getProducts()) {
          for (int i = 0; i < node.getLine().getPoints().size() - 1; i++) {
            Point2D point = node.getLine().getPoints().get(i);
            node.getLine().getPoints().set(i, new Point2D.Double(point.getX() + dx, point.getY() + dy));
          }
        }
        for (Modifier node : reaction.getModifiers()) {
          for (int i = 1; i < node.getLine().getPoints().size(); i++) {
            Point2D point = node.getLine().getPoints().get(i);
            node.getLine().getPoints().set(i, new Point2D.Double(point.getX() + dx, point.getY() + dy));
          }
        }
        for (NodeOperator node : reaction.getOperators()) {
          for (int i = 0; i < node.getLine().getPoints().size(); i++) {
            Point2D point = node.getLine().getPoints().get(i);
            node.getLine().getPoints().set(i, new Point2D.Double(point.getX() + dx, point.getY() + dy));
          }
        }
        includeInAffectedRegion(reaction);
      } else {
        throw new InvalidStateException("Unknown class type: " + object);
      }
    }
    if (aliases.size() > 0) {
      // TODO this must be improved, we cannot do full search on every move
      for (Reaction reaction : getModel().getReactions()) {
        for (ReactionNode node : reaction.getReactionNodes()) {
          if (aliases.contains(node.getElement())) {
            if (node instanceof Reactant) {
              Point2D point = node.getLine().getBeginPoint();
              point.setLocation(point.getX() + dx, point.getY() + dy);

              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getPoints().get(1));
            } else if (node instanceof Modifier) {
              Point2D point = node.getLine().getBeginPoint();
              point.setLocation(point.getX() + dx, point.getY() + dy);
              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getPoints().get(1));
            } else if (node instanceof Product) {
              Point2D point = node.getLine().getEndPoint();
              point.setLocation(point.getX() + dx, point.getY() + dy);
              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getPoints().get(node.getLine().getPoints().size() - 1));
            }
          }
        }
      }
    }
  }
}
