package lcsb.mapviewer.modelutils.map;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

/**
 * List of {@link BioEntity} that must be annotated with at least one
 * {@link lcsb.mapviewer.model.map.MiriamData}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum RequireAnnotationMap {

  /**
   * {@link Reaction}.
   */
  REACTION(Reaction.class),

  /**
   * {@link Protein}.
   */
  PROTEIN(Protein.class),

  /**
   * {@link Chemical}.
   */
  CHECMICAL(Chemical.class),

  /**
   * {@link Rna}.
   */
  RNA(Rna.class),

  /**
   * {@link Gene}.
   */
  GENE(Gene.class),

  /**
   * {@link Drug}.
   */
  DRUG(Drug.class),

  /**
   * {@link ComplexSpecies}.
   */
  COMPLEX(Complex.class),

  /**
   * {@link AntisenseRna}.
   */
  ANTISENSE_RNA(AntisenseRna.class),

  /**
   * {@link Phenotype}.
   */
  PHENOTYPE(Phenotype.class);

  /**
   * Class which should be annotated.
   */
  private Class<? extends BioEntity> clazz;

  /**
   * Default constructor.
   * 
   * @param clazz
   *          {@link #clazz}
   */
  RequireAnnotationMap(Class<? extends BioEntity> clazz) {
    this.clazz = clazz;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<?> getClazz() {
    return clazz;
  }
}
