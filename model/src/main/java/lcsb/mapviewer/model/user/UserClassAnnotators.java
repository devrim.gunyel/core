package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.user.annotator.AnnotatorData;

/**
 * This class defines set of default class annotators for a single class type
 * that are used by a user.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class UserClassAnnotators implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this set of
   * annotators.
   */
  @ManyToOne
  private UserAnnotationSchema annotationSchema;

  /**
   * Class for which this set of annotators is defined.
   */
  private String className;

  /**
   * List of strings defining set of annotators (canonical class names).
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "userClassAnnotators", orphanRemoval = true)
  @OrderBy("order_index")
  private List<AnnotatorData> annotators = new ArrayList<>();

  /**
   * Default constructor.
   */
  public UserClassAnnotators() {

  }

  /**
   * Default constructor.
   * 
   * @param objectClass
   *          {@link #className}
   * @param annotators
   *          {@link #annotators}
   */
  public UserClassAnnotators(Class<?> objectClass, List<AnnotatorData> annotators) {
    setClassName(objectClass);
    for (AnnotatorData annotator : annotators) {
      addAnnotator(annotator);
    }
  }

  /**
   * Default constructor.
   * 
   * @param objectClass
   *          {@link #className}
   */
  public UserClassAnnotators(Class<?> objectClass) {
    this(objectClass, new ArrayList<>());
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the annotationSchema
   * @see #annotationSchema
   */
  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  /**
   * @param annotationSchema
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }

  /**
   * @return the className
   * @see #className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   *          the className to set
   * @see #className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * Sets {@link #className}.
   *
   * @param clazz
   *          new {@link #className} value
   */
  public void setClassName(Class<?> clazz) {
    setClassName(clazz.getCanonicalName());
  }

  /**
   * @return the annotators
   * @see #annotators
   */
  public List<AnnotatorData> getAnnotators() {
    return annotators;
  }

  /**
   * @param annotators
   *          the annotators to set
   * @see #annotators
   */
  public void setAnnotators(List<AnnotatorData> annotators) {
    for (AnnotatorData annotator : this.annotators) {
      annotator.setUserClassAnnotators(null);
    }
    this.annotators.clear();
    this.annotators.addAll(annotators);
    for (AnnotatorData annotator : this.annotators) {
      annotator.setUserClassAnnotators(this);
    }
    fixAnnotatorsOrder();
  }

  /**
   * Adds annotator to {@link #annotators list of annotators}.
   * 
   * @param annotator
   *          object to add
   */
  public void addAnnotator(AnnotatorData annotator) {
    annotators.add(annotator);
    annotator.setUserClassAnnotators(this);
    fixAnnotatorsOrder();
  }

  private void fixAnnotatorsOrder() {
    for (int i = 0; i < annotators.size(); i++) {
      annotators.get(i).setOrderIndex(i);
    }
  }
}
