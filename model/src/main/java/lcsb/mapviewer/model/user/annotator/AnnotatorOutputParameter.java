package lcsb.mapviewer.model.user.annotator;

import javax.persistence.*;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * Definition of annotator output. It describes which property/identifier should
 * be set by the annotator.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("OUTPUT_PARAMETER")
public class AnnotatorOutputParameter extends AnnotatorParameter {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private BioEntityField field;

  @Enumerated(EnumType.STRING)
  private MiriamType identifierType;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorOutputParameter() {
  }

  public AnnotatorOutputParameter(BioEntityField field) {
    this.field = field;
  }

  public AnnotatorOutputParameter(MiriamType type) {
    this.identifierType = type;
  }

  public BioEntityField getField() {
    return field;
  }

  public MiriamType getIdentifierType() {
    return identifierType;
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    return this.toString().equals(o.toString());
  }

  @Override
  public String toString() {
    return "[" + field + "," + identifierType + "]";
  }
}
