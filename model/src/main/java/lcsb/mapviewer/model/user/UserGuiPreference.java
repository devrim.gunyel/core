package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.*;

/**
 * This class defines GUI preference for the {@link User}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class UserGuiPreference implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this parameter.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "annotation_schema_id")
  private UserAnnotationSchema annotationSchema;

  /**
   * GUI parameter name.
   */
  @Column(nullable = false)
  private String key;

  /**
   * GUI parameter value.
   */
  @Column(nullable = false)
  private String value;

  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
