package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.*;

/**
 * This class represents one configurable parameter of the system.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class ConfigurationOption implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Type of the configuration element.
   */
  @Enumerated(EnumType.STRING)
  private ConfigurationElementType type;

  /**
   * What is the value of the configuration parameter.
   */
  private String value;

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the type
   * @see #type
   */
  public ConfigurationElementType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(ConfigurationElementType type) {
    this.type = type;
  }

  /**
   * @return the value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value
   *          the value to set
   * @see #value
   */
  public void setValue(String value) {
    this.value = value;
  }

}
