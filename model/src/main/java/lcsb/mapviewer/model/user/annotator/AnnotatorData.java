package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.UserClassAnnotators;

@Entity
public class AnnotatorData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * Order of the annotator in the annotator list.
   */
  private int orderIndex;

  /**
   * Class name of the annotator {@link #parameterType parameter} of which is
   * being set to the {@link #paramValue value}.
   */
  @Column(nullable = false)
  private Class<?> annotatorClassName;

  @ManyToOne
  private UserClassAnnotators userClassAnnotators;

  /**
   * List of class annotators params.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotatorData")
  @OrderBy("id")
  private List<AnnotatorParameter> annotatorParams = new ArrayList<>();

  /**
   * Constructor for hibernate.
   */
  protected AnnotatorData() {

  }

  /**
   * Default constructor.
   */
  public AnnotatorData(Class<?> annotatorClassName) {
    setAnnotatorClassName(annotatorClassName);
  }

  public AnnotatorData(String annotatorClassName) throws ClassNotFoundException {
    setAnnotatorClassName(annotatorClassName);
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the className
   * @see #className
   */
  public Class<?> getAnnotatorClassName() {
    return annotatorClassName;
  }

  /**
   * @param className
   *          the className to set
   * @see #className
   */
  public void setAnnotatorClassName(Class<?> annotatorClassName) {
    this.annotatorClassName = annotatorClassName;
  }

  public void setAnnotatorClassName(String annotatorClassName) throws ClassNotFoundException {
    setAnnotatorClassName(Class.forName(annotatorClassName));
  }

  public UserClassAnnotators getUserClassAnnotators() {
    return userClassAnnotators;
  }

  public void setUserClassAnnotators(UserClassAnnotators userClassAnnotators) {
    this.userClassAnnotators = userClassAnnotators;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(int orderIndex) {
    this.orderIndex = orderIndex;
  }

  public List<AnnotatorParameter> getAnnotatorParams() {
    return annotatorParams;
  }

  public void setAnnotatorParams(List<AnnotatorParameter> annotatorParams) {
    this.annotatorParams = annotatorParams;
  }

  public AnnotatorData addAnnotatorParameter(AnnotatorParameter parameter) {
    this.annotatorParams.add(parameter);
    parameter.setAnnotatorData(this);
    fixOrder();
    return this;
  }

  public AnnotatorData addAnnotatorParameters(List<? extends AnnotatorParameter> parameters) {
    for (AnnotatorParameter object : parameters) {
      addAnnotatorParameter(object);
    }
    return this;
  }

  public String getValue(AnnotatorParamDefinition type) {
    for (AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorConfigParameter) {
        AnnotatorConfigParameter configParameter = (AnnotatorConfigParameter) annotatorParameter;
        if (configParameter.getType().equals(type)) {
          return configParameter.getValue();
        }
      }
    }
    return null;
  }

  public List<AnnotatorInputParameter> getInputParameters() {
    List<AnnotatorInputParameter> result = new ArrayList<>();
    for (AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorInputParameter) {
        result.add((AnnotatorInputParameter) annotatorParameter);
      }
    }
    return result;
  }

  public boolean hasOutputField(BioEntityField field) {
    for (AnnotatorOutputParameter annotatorParameter : getOutputParameters()) {
      if (annotatorParameter.getField() != null && annotatorParameter.getField().equals(field)) {
        return true;
      }
    }
    return false;
  }

  public List<AnnotatorOutputParameter> getOutputParameters() {
    List<AnnotatorOutputParameter> result = new ArrayList<>();
    for (AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorOutputParameter) {
        result.add((AnnotatorOutputParameter) annotatorParameter);
      }
    }
    return result;
  }

  public AnnotatorData addAnnotatorParameter(AnnotatorParamDefinition parameterType, String paramValue) {
    for (AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorConfigParameter) {
        AnnotatorConfigParameter configParameter = (AnnotatorConfigParameter) annotatorParameter;
        configParameter.setValue(paramValue);
        return this;
      }
    }
    return addAnnotatorParameter(new AnnotatorConfigParameter(parameterType, paramValue));
  }

  public void removeAnnotatorParameters(ArrayList<AnnotatorParameter> parameters) {
    for (AnnotatorParameter annotatorParameter : parameters) {
      removeAnnotatorParameter(annotatorParameter);
    }
  }

  private void removeAnnotatorParameter(AnnotatorParameter parameter) {
    this.annotatorParams.remove(parameter);
    parameter.setAnnotatorData(null);
    fixOrder();
  }

  private void fixOrder() {
    for (int i = 0; i < annotatorParams.size(); i++) {
      annotatorParams.get(i).setOrderPosition(i);
    }
  }

}
