package lcsb.mapviewer.model.map.model;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This abstract class defines connection between models. It points to submodel
 * and describe type of the connection.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "submodel_connections_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("SUBMODEL_CONNECTION")
public abstract class SubmodelConnection implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SubmodelConnection.class);

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Sub model.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
  private ModelData submodel;

  /**
   * Name of the connection.
   */
  private String name;

  /**
   * Type of the connection.
   */
  @Enumerated(EnumType.STRING)
  private SubmodelType type;

  /**
   * Default constructor.
   */
  public SubmodelConnection() {
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public SubmodelConnection(ModelData submodel, SubmodelType type) {
    setSubmodel(submodel);
    setType(type);
  }

  /**
   * Constructor that creates copy of the {@link SubmodelConnection} object.
   * 
   * @param original
   *          original object from which copy is prepared
   */
  public SubmodelConnection(SubmodelConnection original) {
    setSubmodel(original.getSubmodel());
    setName(original.getName());
    setType(original.getType());
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   * @param name
   *          {@link SubmodelConnection#name}
   */
  public SubmodelConnection(Model submodel, SubmodelType type, String name) {
    setSubmodel(submodel);
    setName(name);
    setType(type);
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the type
   * @see #type
   */
  public SubmodelType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(SubmodelType type) {
    this.type = type;
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ModelData getSubmodel() {
    return submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodel(ModelData submodel) {
    // sometimes we change submodel in the runtime (for instance when we do
    // create a copy), then we should clean information about connection from
    // both sides
    if (this.submodel != null) {
      this.submodel.getParentModels().remove(this);
    }
    this.submodel = submodel;
    submodel.getParentModels().add(this);
  }

  /**
   * Sets submodel to which this connection points to.
   *
   * @param submodel
   *          submodel to set.
   * @see #submodel
   */
  public void setSubmodel(Model submodel) {
    setSubmodel(submodel.getModelData());
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Creates a copy of this object.
   * 
   * @return copy of the object
   */
  public abstract SubmodelConnection copy();

  /**
   * This method assign values from the original model. It's different than
   * default constructor - doesn't interfere with the models.
   * 
   * @param original
   *          original connection from which data is assigned
   */
  protected void assignValuesFromOriginal(SubmodelConnection original) {
    this.submodel = original.getSubmodel();
    this.name = original.getName();
    this.type = original.getType();
  }
}
