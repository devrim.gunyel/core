package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Representation of a single SBML parameter
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@XmlRootElement
public class SbmlParameter implements Serializable, SbmlArgument {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SbmlParameter.class);
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String parameterId;

  private String name;

  private Double value;

  @ManyToOne()
  private SbmlUnit units;

  /**
   * Constructor required by hibernate.
   */
  SbmlParameter() {
    super();
  }

  public SbmlParameter(String parameterId) {
    this.parameterId = parameterId;
  }

  public SbmlParameter(SbmlParameter original) {
    this.parameterId = original.getParameterId();
    this.name = original.getName();
    this.value = original.getValue();
    this.units = original.getUnits();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getParameterId() {
    return parameterId;
  }

  public void setParameterId(String parameterId) {
    this.parameterId = parameterId;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

  public SbmlUnit getUnits() {
    return units;
  }

  public void setUnits(SbmlUnit units) {
    this.units = units;

  }

  @Override
  public SbmlParameter copy() {
    return new SbmlParameter(this);
  }

  @Override
  public String getElementId() {
    return getParameterId();
  }

  public int getId() {
    return id;
  }

}
