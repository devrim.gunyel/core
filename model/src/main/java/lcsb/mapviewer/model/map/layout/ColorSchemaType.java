package lcsb.mapviewer.model.map.layout;

/**
 * Type of the {@link ColorSchema}.
 * 
 * @author Piotr Gawron
 *
 */
public enum ColorSchemaType {
  /**
   * Generic color schema (used for expression levels, etc).
   */
  GENERIC,

  /**
   * Customized color schema used for highlighting genetic variants.
   */
  GENETIC_VARIANT
}
