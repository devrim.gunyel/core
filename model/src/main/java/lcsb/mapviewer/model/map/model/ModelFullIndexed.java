package lcsb.mapviewer.model.map.model;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.*;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

/**
 * This class implements {@link Model} interface. It's is very simple
 * implementation containing structures that index the data from
 * {@link ModelData} structure and provide access method to this indexed data.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModelFullIndexed implements Model {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ModelFullIndexed.class);

  /**
   * Object that map {@link Element#elementId element identifier} into
   * {@link Element}.
   */
  private Map<String, Element> elementByElementId = new HashMap<>();

  /**
   * Object that map {@link Element#id element database identifier} into
   * {@link Element}.
   */
  private Map<Integer, Element> elementByDbId = new HashMap<>();

  /**
   * Object that map {@link Reaction#idReaction reaction identifier} into
   * {@link Reaction}.
   */
  private Map<String, Reaction> reactionByReactionId = new HashMap<>();

  /**
   * Object that map {@link Reaction#id reaction database identifier} into
   * {@link Reaction}.
   */
  private Map<Integer, Reaction> reactionByDbId = new HashMap<>();

  /**
   * {@link ModelData} object containing "raw" data about the model.
   */
  private ModelData modelData = null;

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link ModelData} object containing "raw" data about the model
   */
  public ModelFullIndexed(ModelData model) {
    if (model == null) {
      this.modelData = new ModelData();
    } else {
      this.modelData = model;
      for (Element element : model.getElements()) {
        elementByElementId.put(element.getElementId(), element);
        elementByDbId.put(element.getId(), element);
      }
      for (Reaction reaction : model.getReactions()) {
        reactionByReactionId.put(reaction.getIdReaction(), reaction);
        reactionByDbId.put(reaction.getId(), reaction);
      }
      if (getProject() != null) {
        getProject().getProjectId();
      }

      for (ModelSubmodelConnection connection : model.getSubmodels()) {
        if (connection.getSubmodel().getModel() == null) {
          connection.getSubmodel().setModel(new ModelFullIndexed(connection.getSubmodel()));
        }
      }
    }
    modelData.setModel(this);
  }

  @Override
  public void addElement(Element element) {
    if (element.getElementId() == null || element.getElementId().isEmpty()) {
      throw new InvalidArgumentException("Element identifier cannot be empty");
    }
    Element element2 = elementByElementId.get(element.getElementId());
    if (element2 == null) {
      modelData.addElement(element);
      elementByElementId.put(element.getElementId(), element);
      elementByDbId.put(element.getId(), element);
    } else {
      throw new InvalidArgumentException("Element with duplicated id: " + element.getElementId());
    }
  }

  @Override
  public double getWidth() {
    return modelData.getWidth();
  }

  @Override
  public void setWidth(double width) {
    modelData.setWidth(width);
  }

  @Override
  public void setWidth(String text) {
    modelData.setWidth(Double.parseDouble(text));
  }

  @Override
  public void setWidth(int width) {
    setWidth(Double.valueOf(width));
  }

  @Override
  public double getHeight() {
    return modelData.getHeight();
  }

  @Override
  public void setHeight(double height) {
    modelData.setHeight(height);
  }

  @Override
  public void setHeight(String text) {
    modelData.setHeight(Double.parseDouble(text));
  }

  @Override
  public void setHeight(int height) {
    setHeight(Double.valueOf(height));
  }

  @Override
  public Set<Element> getElements() {
    return modelData.getElements();
  }

  @Override
  public void setElements(Set<Element> elements) {
    this.modelData.setElements(elements);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends Element> T getElementByElementId(String elementId) {
    return (T) elementByElementId.get(elementId);
  }

  @Override
  public void addReaction(Reaction reaction) {
    modelData.addReaction(reaction);
    if (reactionByReactionId.get(reaction.getIdReaction()) != null) {
      throw new InvalidArgumentException("Reaction with id already exists: " + reaction.getElementId());
    }
    reactionByReactionId.put(reaction.getIdReaction(), reaction);
    reactionByDbId.put(reaction.getId(), reaction);
  }

  @Override
  public Set<Reaction> getReactions() {
    return modelData.getReactions();
  }

  @Override
  public List<Compartment> getCompartments() {
    List<Compartment> result = new ArrayList<>();
    for (Element element : modelData.getElements()) {
      if (element instanceof Compartment) {
        result.add((Compartment) element);
      }
    }
    return result;
  }

  @Override
  public void addLayer(Layer layer) {
    modelData.addLayer(layer);
  }

  @Override
  public Set<Layer> getLayers() {
    return modelData.getLayers();
  }

  @Override
  public void addElements(Collection<? extends Element> elements) {
    for (Element element : elements) {
      addElement(element);
    }
  }

  @Override
  public String getNotes() {
    return modelData.getNotes();
  }

  @Override
  public void setNotes(String notes) {
    if (notes != null && notes.contains("<html")) {
      throw new InvalidArgumentException("notes cannot contain <html> tag");
    }
    modelData.setNotes(notes);
  }

  @Override
  public Reaction getReactionByReactionId(String idReaction) {
    return reactionByReactionId.get(idReaction);
  }

  @Override
  public void addLayers(Collection<Layer> layers) {
    for (Layer layer : layers) {
      addLayer(layer);
    }
  }

  @Override
  public void addElementGroup(ElementGroup elementGroup) {
    modelData.addElementGroup(elementGroup);
  }

  @Override
  public void addBlockDiagream(BlockDiagram blockDiagram) {
    modelData.addBlockDiagream(blockDiagram);
  }

  @Override
  public String getIdModel() {
    return modelData.getIdModel();
  }

  @Override
  public void setIdModel(String idModel) {
    this.modelData.setIdModel(idModel);
  }

  @Override
  public int getTileSize() {
    return modelData.getTileSize();
  }

  @Override
  public void setTileSize(int tileSize) {
    this.modelData.setTileSize(tileSize);
  }

  @Override
  public int getZoomLevels() {
    return modelData.getZoomLevels();
  }

  @Override
  public void setZoomLevels(int zoomLevels) {
    this.modelData.setZoomLevels(zoomLevels);
  }

  @Override
  public void removeReaction(Reaction reaction) {
    modelData.removeReaction(reaction);
    reactionByReactionId.remove(reaction.getIdReaction());
    reactionByDbId.remove(reaction.getId());
  }

  @Override
  public void removeElement(Element element) {
    modelData.removeElement(element);
    elementByElementId.remove(element.getElementId());
    elementByDbId.remove(element.getId());

    if (element.getCompartment() != null) {
      Compartment ca = element.getCompartment();
      ca.removeElement(element);
    }

    if (element instanceof Species) {
      Species al = (Species) element;
      if (al.getComplex() != null) {
        Complex ca = ((Species) element).getComplex();
        ca.removeElement(al);
      }
    }
  }

  @Override
  public Collection<Species> getNotComplexSpeciesList() {
    List<Species> result = new ArrayList<>();
    for (Element element : modelData.getElements()) {
      if (element instanceof Species && !(element instanceof Complex)) {
        result.add((Species) element);
      }
    }
    return result;
  }

  @Override
  public List<Species> getSpeciesList() {
    List<Species> result = new ArrayList<>();
    for (Element element : modelData.getElements()) {
      if (element instanceof Species) {
        result.add((Species) element);
      }
    }
    return result;
  }

  @Override
  public Collection<Complex> getComplexList() {
    List<Complex> result = new ArrayList<>();
    for (Element element : modelData.getElements()) {
      if (element instanceof Complex) {
        result.add((Complex) element);
      }
    }
    return result;
  }

  @Override
  public void addReactions(List<Reaction> reactions2) {
    for (Reaction reaction : reactions2) {
      addReaction(reaction);
    }
  }

  @Override
  public Set<BioEntity> getElementsByAnnotation(MiriamData miriamData) {
    Set<BioEntity> result = new HashSet<>();
    for (Element element : getElements()) {
      for (MiriamData md : element.getMiriamData()) {
        if (md.equals(miriamData)) {
          result.add(element);
        }
      }
    }

    for (Reaction element : getReactions()) {
      for (MiriamData md : element.getMiriamData()) {
        if (md.equals(miriamData)) {
          result.add(element);
        }
      }
    }

    return result;
  }

  @Override
  public List<Element> getElementsByName(String name) {
    List<Element> result = new ArrayList<>();
    for (Element element : getElements()) {
      if (element.getName().equalsIgnoreCase(name)) {
        result.add(element);
      }
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends Element> T getElementByDbId(Integer dbId) {
    return (T) elementByDbId.get(dbId);
  }

  @Override
  public Reaction getReactionByDbId(Integer dbId) {
    return reactionByDbId.get(dbId);
  }

  @Override
  public List<Compartment> getSortedCompartments() {
    List<Compartment> result = getCompartments();
    Collections.sort(result, Element.SIZE_COMPARATOR);
    return result;
  }

  @Override
  public List<Element> getElementsSortedBySize() {
    List<Element> sortedElements = new ArrayList<>();
    sortedElements.addAll(getElements());
    Collections.sort(sortedElements, Element.SIZE_COMPARATOR);
    return sortedElements;
  }

  @Override
  public Project getProject() {
    return modelData.getProject();
  }

  @Override
  public void setProject(Project project) {
    modelData.setProject(project);
  }

  @Override
  public ModelData getModelData() {
    return modelData;
  }

  @Override
  public Integer getId() {
    return modelData.getId();
  }

  @Override
  public void setId(int id) {
    modelData.setId(id);
  }

  @Override
  public void addSubmodelConnection(ModelSubmodelConnection submodel) {
    modelData.addSubmodel(submodel);
    submodel.setParentModel(this);
  }

  @Override
  public Collection<ModelSubmodelConnection> getSubmodelConnections() {
    return modelData.getSubmodels();
  }

  @Override
  public String getName() {
    return modelData.getName();
  }

  @Override
  public void setName(String name) {
    modelData.setName(name);
  }

  @Override
  public Model getSubmodelById(Integer id) {
    if (id == null) {
      return null;
    }
    if (id.equals(getId())) {
      return this;
    }
    SubmodelConnection connection = getSubmodelConnectionById(id);
    if (connection != null) {
      return connection.getSubmodel().getModel();
    } else {
      return null;
    }
  }

  @Override
  public Collection<SubmodelConnection> getParentModels() {
    return modelData.getParentModels();
  }

  @Override
  public Model getSubmodelByConnectionName(String name) {
    if (name == null) {
      return null;
    }
    for (ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (name.equals(connection.getName())) {
        return connection.getSubmodel().getModel();
      }
    }
    return null;
  }

  @Override
  public SubmodelConnection getSubmodelConnectionById(Integer id) {
    for (ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (id.equals(connection.getSubmodel().getId())) {
        return connection;
      }
    }
    return null;
  }

  @Override
  public Model getSubmodelById(String identifier) {
    if (identifier == null) {
      return null;
    }
    Integer id = Integer.parseInt(identifier);
    return getSubmodelById(id);
  }

  @Override
  public Collection<Model> getSubmodels() {
    List<Model> models = new ArrayList<Model>();
    for (ModelSubmodelConnection connection : getSubmodelConnections()) {
      models.add(connection.getSubmodel().getModel());
    }
    return models;
  }

  @Override
  public Model getSubmodelByName(String name) {
    if (name == null) {
      return null;
    }
    for (ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (name.equals(connection.getSubmodel().getName())) {
        return connection.getSubmodel().getModel();
      }
    }
    if (name.equals(getName())) {
      return this;
    }
    return null;
  }

  @Override
  public List<BioEntity> getBioEntities() {
    List<BioEntity> result = new ArrayList<>();
    result.addAll(getElements());
    result.addAll(getReactions());
    return result;
  }

  @Override
  public Double getDefaultCenterX() {
    return modelData.getDefaultCenterX();
  }

  @Override
  public void setDefaultCenterX(Double defaultCenterX) {
    modelData.setDefaultCenterX(defaultCenterX);
  }

  @Override
  public Double getDefaultCenterY() {
    return modelData.getDefaultCenterY();
  }

  @Override
  public void setDefaultCenterY(Double defaultCenterY) {
    modelData.setDefaultCenterY(defaultCenterY);
  }

  @Override
  public Integer getDefaultZoomLevel() {
    return modelData.getDefaultZoomLevel();
  }

  @Override
  public void setDefaultZoomLevel(Integer defaultZoomLevel) {
    modelData.setDefaultZoomLevel(defaultZoomLevel);
  }

  @Override
  public Set<SbmlFunction> getFunctions() {
    return modelData.getFunctions();
  }

  @Override
  public void addFunctions(Collection<SbmlFunction> functions) {
    modelData.addFunctions(functions);
  }

  @Override
  public Set<SbmlParameter> getParameters() {
    return modelData.getParameters();
  }

  @Override
  public void addUnits(Collection<SbmlUnit> units) {
    modelData.addUnits(units);
  }

  @Override
  public Set<SbmlUnit> getUnits() {
    return modelData.getUnits();
  }

  @Override
  public void addUnit(SbmlUnit unit) {
    modelData.addUnit(unit);

  }

  @Override
  public SbmlUnit getUnitsByUnitId(String unitId) {
    for (SbmlUnit unit : getUnits()) {
      if (unit.getUnitId().equals(unitId)) {
        return unit;
      }
    }
    return null;
  }

  @Override
  public void addParameters(Collection<SbmlParameter> sbmlParameters) {
    modelData.addParameters(sbmlParameters);
  }

  @Override
  public void addParameter(SbmlParameter sbmlParameter) {
    modelData.addParameter(sbmlParameter);
  }

  @Override
  public SbmlParameter getParameterById(String parameterId) {
    for (SbmlParameter parameter : getParameters()) {
      if (parameter.getParameterId().equals(parameterId)) {
        return parameter;
      }
    }
    return null;
  }

  @Override
  public void addFunction(SbmlFunction sbmlFunction) {
    modelData.addFunction(sbmlFunction);
  }

  @Override
  public SbmlFunction getFunctionById(String functionId) {
    for (SbmlFunction function : getFunctions()) {
      if (function.getFunctionId().equals(functionId)) {
        return function;
      }
    }
    return null;
  }

  @Override
  public void removeReactions(Collection<Reaction> reactions) {
    Set<Reaction> reactionsToRemove = new HashSet<>();
    reactionsToRemove.addAll(reactions);
    for (Reaction reaction : reactionsToRemove) {
      removeReaction(reaction);
    }
  }

  @Override
  public Set<MiriamData> getMiriamData() {
    return modelData.getMiriamData();
  }

  @Override
  public void addMiriamData(MiriamData md) {
    modelData.addMiriamData(md);
  }

  @Override
  public void addMiriamData(Collection<MiriamData> miriamData) {
    for (MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addAuthor(Author author) {
    modelData.addAuthor(author);
  }

  @Override
  public void addAuthors(Collection<Author> authors) {
    for (Author author : authors) {
      addAuthor(author);
    }
  }

  @Override
  public List<Author> getAuthors() {
    return modelData.getAuthors();
  }

  @Override
  public Calendar getCreationDate() {
    return modelData.getCreationDate();
  }

  @Override
  public void setCreationDate(Calendar creationDate) {
    modelData.setCreationDate(creationDate);
  }

  @Override
  public void addModificationDate(Calendar modificationDate) {
    modelData.addModificationDate(modificationDate);
  }

  @Override
  public List<Calendar> getModificationDates() {
    return modelData.getModificationDates();
  }

  @Override
  public void addModificationDates(Collection<Calendar> modificationDates) {
    for (Calendar calendar : modificationDates) {
      addModificationDate(calendar);
    }
  }

  @Override
  public Set<Drawable> getDrawables() {
    Set<Drawable> result = new HashSet<>();
    result.addAll(getBioEntities());
    for (Layer layer : getLayers()) {
      result.addAll(layer.getDrawables());
    }
    return result;
  }
}
