package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner positive influence reaction. It
 * must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("POSITIVE_INFLUENCE_REACTION")
public class PositiveInfluenceReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public PositiveInfluenceReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public PositiveInfluenceReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Positive influence";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public PositiveInfluenceReaction copy() {
    if (this.getClass() == PositiveInfluenceReaction.class) {
      return new PositiveInfluenceReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
