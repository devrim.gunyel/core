package lcsb.mapviewer.model.map.species;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.field.StructuralStateComparator;

/**
 * This class implements comparator interface for {@link Complex}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexComparator extends Comparator<Complex> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(ComplexComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ComplexComparator(double epsilon) {
    super(Complex.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ComplexComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(Complex arg0, Complex arg1) {
    ElementComparator elementComparator = new ElementComparator(epsilon);
    IntegerComparator integerComparator = new IntegerComparator();

    if (integerComparator.compare(arg0.getElements().size(), arg1.getElements().size()) != 0) {
      logger.debug("children elements size different: " + arg0.getElements().size() + ", " + arg1.getElements().size());
      return integerComparator.compare(arg0.getElements().size(), arg1.getElements().size());
    }

    Map<String, Element> map1 = new HashMap<>();
    Map<String, Element> map2 = new HashMap<>();

    for (Element element : arg0.getElements()) {
      if (map1.get(element.getElementId()) != null) {
        throw new InvalidArgumentException("Few elements with the same id: " + element.getElementId());
      }
      map1.put(element.getElementId(), element);
    }

    for (Element element : arg1.getElements()) {
      if (map2.get(element.getElementId()) != null) {
        throw new InvalidArgumentException("Few elements with the same id: " + element.getElementId());
      }
      map2.put(element.getElementId(), element);
    }

    for (Element element : arg0.getElements()) {
      Element element2 = map2.get(element.getElementId());
      int status = elementComparator.compare(element, element2);
      if (status != 0) {
        logger.debug("child doesn't have a match: " + element.getElementId());
        return status;
      }
    }

    StructuralStateComparator structuralStateComparator = new StructuralStateComparator(epsilon);

    if (structuralStateComparator.compare(arg0.getStructuralState(), arg1.getStructuralState()) != 0) {
      logger
          .debug("Species structuralState different: " + arg0.getStructuralState() + ", " + arg1.getStructuralState());
      return structuralStateComparator.compare(arg0.getStructuralState(), arg1.getStructuralState());
    }

    return 0;
  }
}
