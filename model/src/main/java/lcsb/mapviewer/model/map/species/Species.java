package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Structure used for representing information about single element.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("SPECIES")
public abstract class Species extends Element {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Species.class);

  /**
   * Is the element active.
   */
  private Boolean activity;

  /**
   * Width of the element border.
   */
  private Double lineWidth;

  /**
   * State of the element visualization (should the inside be presented in details
   * or not). TODO this should be transformed into enum
   */
  private String state;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the first part of the state.
   */
  private String statePrefix;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the second part of the state.
   */
  private String stateLabel;

  /**
   * Complex to which this element belongs to. Null if such complex doesn't exist.
   */
  @ManyToOne
  @Cascade({ CascadeType.ALL })
  @JoinColumn(name = "idcomplexdb")
  private Complex complex;

  /**
   * Initial amount of species.
   */
  private Double initialAmount = null;

  /**
   * Charge of the species.
   */
  private Integer charge = null;

  /**
   * Initial concentration of species.
   */
  private Double initialConcentration = 0.0;

  /**
   * Is only substance units allowed.
   */
  private Boolean onlySubstanceUnits = false;

  /**
   * How many dimers are in this species.
   */
  private int homodimer = 1;

  /**
   * Position on the compartment.
   */
  @Enumerated(EnumType.STRING)
  private PositionToCompartment positionToCompartment = null;

  /**
   * Is species hypothetical.
   */
  private Boolean hypothetical = null;

  /**
   * List of uniprot records which are associated with this species.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "species", orphanRemoval = true)
  private Set<UniprotRecord> uniprots = new HashSet<>();

  private Boolean boundaryCondition = false;

  private Boolean constant = false;

  /**
   * SBML allows to store substanceUnit as either basic SI unit type or complex
   * unit definition. To map it properly to database we need to separate the type
   * into two properties. This property is responsible for basic SI unit type.
   */
  @Enumerated(EnumType.STRING)
  private SbmlUnitType substanceUnitRawType;

  /**
   * SBML allows to store substanceUnit as either basic SI unit type or complex
   * unit definition. To map it properly to database we need to separate the type
   * into two properties. This property is responsible for complex unit
   * definition.
   */
  @ManyToOne
  @Cascade({ CascadeType.ALL })
  private SbmlUnit substanceUnitComlexType;

  /**
   * State of the complex/protein species.
   */
  // it should be in protein/complex, but due to hibernate reversed mappedBy
  // property it
  // must be in species.
  @OneToOne(orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
  private StructuralState structuralState = null;

  /**
   * Constructor that set element identifier.
   * 
   * @param elementId
   *          {@link Element#elementId}
   */
  public Species(String elementId) {
    this();
    setElementId(elementId);
    setName(elementId);
  }

  /**
   * Empty constructor required by hibernate.
   */
  Species() {
    super();
    activity = false;
    lineWidth = 1.0;
    state = "usual";
  }

  /**
   * Constructor that initializes data with the information given in the
   * parameter.
   * 
   * @param original
   *          object from which data will be initialized
   */
  protected Species(Species original) {
    super(original);
    activity = original.getActivity();
    lineWidth = original.getLineWidth();
    state = original.getState();

    complex = original.getComplex();
    stateLabel = original.getStateLabel();
    statePrefix = original.getStatePrefix();

    initialAmount = original.getInitialAmount();
    charge = original.getCharge();
    initialConcentration = original.getInitialConcentration();
    onlySubstanceUnits = original.getOnlySubstanceUnits();
    homodimer = original.getHomodimer();
    positionToCompartment = original.getPositionToCompartment();
    hypothetical = original.getHypothetical();
    boundaryCondition = original.getBoundaryCondition();
    constant = original.getConstant();
    substanceUnitComlexType = original.substanceUnitComlexType;
    substanceUnitRawType = original.substanceUnitRawType;

    uniprots = original.getUniprots();
    if (original.structuralState != null) {
      setStructuralState(original.structuralState.copy());
    }

    // don't copy reaction nodes
  }

  /**
   * @return the activity
   * @see #activity
   */
  public Boolean getActivity() {
    return activity;
  }

  /**
   * @param activity
   *          the activity to set
   * @see #activity
   */
  public void setActivity(Boolean activity) {
    this.activity = activity;
  }

  /**
   * @return the lineWidth
   * @see #lineWidth
   */
  public Double getLineWidth() {
    return lineWidth;
  }

  /**
   * @param lineWidth
   *          the lineWidth to set
   * @see #lineWidth
   */
  public void setLineWidth(Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  /**
   * @return the state
   * @see #state
   */
  public String getState() {
    return state;
  }

  /**
   * @param state
   *          the state to set
   * @see #state
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public Complex getComplex() {
    return complex;
  }

  /**
   * @param complex
   *          the complex to set
   * @see #complex
   */
  public void setComplex(Complex complex) {
    this.complex = complex;
  }

  /**
   * @return the statePrefix
   * @see #statePrefix
   */
  public String getStatePrefix() {
    return statePrefix;
  }

  /**
   * @param statePrefix
   *          the statePrefix to set
   * @see #statePrefix
   */
  public void setStatePrefix(String statePrefix) {
    this.statePrefix = statePrefix;
  }

  /**
   * @return the stateLabel
   * @see #stateLabel
   */
  public String getStateLabel() {
    return stateLabel;
  }

  /**
   * @param stateLabel
   *          the stateLabel to set
   * @see #stateLabel
   */
  public void setStateLabel(String stateLabel) {
    this.stateLabel = stateLabel;
  }

  @Override
  public abstract Species copy();

  /**
   * @return the initialAmount
   * @see #initialAmount
   */
  public Double getInitialAmount() {
    return initialAmount;
  }

  /**
   * @param initialAmount
   *          the initialAmount to set
   * @see #initialAmount
   */
  public void setInitialAmount(Double initialAmount) {
    this.initialAmount = initialAmount;
    if (initialAmount != null) {
      this.setInitialConcentration(null);
    }
  }

  /**
   * @return the charge
   * @see #charge
   */
  public Integer getCharge() {
    return charge;
  }

  /**
   * @param charge
   *          the charge to set
   * @see #charge
   */
  public void setCharge(Integer charge) {
    this.charge = charge;
  }

  /**
   * @return the initialConcentration
   * @see #initialConcentration
   */
  public Double getInitialConcentration() {
    return initialConcentration;
  }

  /**
   * @param initialConcentration
   *          the initialConcentration to set
   * @see #initialConcentration
   */
  public void setInitialConcentration(Double initialConcentration) {
    this.initialConcentration = initialConcentration;
    if (initialConcentration != null) {
      this.setInitialAmount(null);
    }
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean getOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * @param onlySubstanceUnits
   *          the onlySubstanceUnits to set
   * @see #onlySubstanceUnits
   */
  public void setOnlySubstanceUnits(Boolean onlySubstanceUnits) {
    this.onlySubstanceUnits = onlySubstanceUnits;
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer
   *          the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(int homodimer) {
    this.homodimer = homodimer;
  }

  /**
   * @return the positionToCompartment
   * @see #positionToCompartment
   */
  public PositionToCompartment getPositionToCompartment() {
    return positionToCompartment;
  }

  /**
   * @param positionToCompartment
   *          the positionToCompartment to set
   * @see #positionToCompartment
   */
  public void setPositionToCompartment(PositionToCompartment positionToCompartment) {
    this.positionToCompartment = positionToCompartment;
  }

  /**
   * @return the hypothetical
   * @see #hypothetical
   */
  public Boolean getHypothetical() {
    return hypothetical;
  }

  /**
   * @return the uniprot
   * @see #uniprots
   */
  public Set<UniprotRecord> getUniprots() {
    return uniprots;
  }

  /**
   * @param uniprots
   *          set of uniprot records for this species
   * @see #uniprots
   */
  public void setUniprots(Set<UniprotRecord> uniprots) {
    this.uniprots = uniprots;
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean hasOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * Is species hypothetical or not.
   *
   * @return <code>true</code> if species is hypothetical, <code>false</code>
   *         otherwise
   */
  public boolean isHypothetical() {
    if (hypothetical == null) {
      return false;
    }
    return hypothetical;
  }

  /**
   * @param hypothetical
   *          the hypothetical to set
   * @see #hypothetical
   */
  public void setHypothetical(Boolean hypothetical) {
    this.hypothetical = hypothetical;
  }

  public boolean isBoundaryCondition() {
    if (boundaryCondition == null) {
      return false;
    }
    return boundaryCondition;
  }

  public Boolean getBoundaryCondition() {
    return boundaryCondition;
  }

  public void setBoundaryCondition(Boolean boundaryCondition) {
    this.boundaryCondition = boundaryCondition;
  }

  public boolean isConstant() {
    if (constant == null) {
      return false;
    }
    return constant;
  }

  public Boolean getConstant() {
    return constant;
  }

  public void setConstant(Boolean constant) {
    this.constant = constant;
  }

  public Object getSubstanceUnits() {
    if (this.substanceUnitRawType != null) {
      return this.substanceUnitRawType;
    } else {
      return this.substanceUnitComlexType;
    }
  }

  public void setSubstanceUnits(SbmlUnitType substanceUnits) {
    this.substanceUnitRawType = substanceUnits;
    this.substanceUnitComlexType = null;
  }

  public void setSubstanceUnits(SbmlUnit substanceUnits) {
    this.substanceUnitComlexType = substanceUnits;
    this.substanceUnitRawType = null;
  }

  public StructuralState getStructuralState() {
    return structuralState;
  }

  public void setStructuralState(StructuralState structuralState) {
    this.structuralState = structuralState;
    if (structuralState != null) {
      structuralState.setSpecies(this);
    }
  }

}
