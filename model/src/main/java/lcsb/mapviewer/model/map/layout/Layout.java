package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;

/**
 * This object represents type of visualization for the model.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class Layout implements Serializable {
  public static final Comparator<Layout> ID_COMPARATOR = new Comparator<Layout>() {

    @Override
    public int compare(Layout o1, Layout o2) {
      return o1.getId() - o2.getId();
    }
  };
  public static final Comparator<? super Layout> ORDER_COMPARATOR = new Comparator<Layout>() {

    @Override
    public int compare(Layout o1, Layout o2) {
      return o1.getOrderIndex() - o2.getOrderIndex();
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Layout.class);

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Title of this layout.
   */
  private String title;

  /**
   * Is the layout publicly available.
   */
  private boolean publicLayout = false;

  /**
   * Is the overlay considered as default (should it be open on startup).
   */
  private boolean defaultOverlay = false;

  /**
   * Does the layout present data in hierarchical view.
   */
  private boolean hierarchicalView = false;

  private boolean googleLicenseConsent = false;

  /**
   * Data overlay type. It can be null in such case it should be obtained from
   * {@link #inputData}.
   */
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ColorSchemaType colorSchemaType = null;

  /**
   * If overlay contain hierarchical view then it might be fixed on some specific
   * level. This parameter defines the level at which it's fixed or contains null
   * if it's general hierarchical view.
   */
  private Integer hierarchyViewLevel;

  /**
   * Project to which this data overlay is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Project project;

  /**
   * Who created the layout.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User creator;

  /**
   * Describes progress of layout uploading process.
   */
  @Enumerated(EnumType.STRING)
  private LayoutStatus status = LayoutStatus.UNKNOWN;

  /**
   * What is the progress.
   */
  private double progress = 0.0;

  private int orderIndex = 0;

  /**
   * List of image folders for generated background images.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "dataOverlay", orphanRemoval = true)
  private Set<DataOverlayImageLayer> dataOverlayImageLayers = new HashSet<>();

  /**
   * Short description used for this layout.
   */
  @Column(columnDefinition = "TEXT")
  private String description;

  /**
   * Here we store input file.
   */
  @Cascade({ CascadeType.SAVE_UPDATE })
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "file_entry_id")
  private UploadedFileEntry inputData;

  /**
   * Default constructor.
   */
  public Layout() {

  }

  /**
   * Constructor that initializes object with basic parameters.
   *
   * @param title
   *          title of the layout
   * @param publicLayout
   *          is the layout publicly available
   */
  public Layout(String title, boolean publicLayout) {
    this.title = title;
    this.publicLayout = publicLayout;
  }

  /**
   * Constructor that initializes object with the copy of the data from parameter.
   *
   * @param layout
   *          from this object the data will be initialized
   */
  public Layout(Layout layout) {
    this.description = layout.getDescription();
    this.title = layout.getTitle();
    this.publicLayout = layout.publicLayout;
    this.status = layout.status;
    this.progress = layout.progress;
    this.hierarchicalView = layout.hierarchicalView;
    if (layout.inputData != null) {
      this.inputData = new UploadedFileEntry(layout.getInputData());
    }
    this.hierarchyViewLevel = layout.hierarchyViewLevel;
    this.orderIndex = layout.orderIndex;
    for (DataOverlayImageLayer dataOverlayImageLayer : layout.getDataOverlayImageLayers()) {
      DataOverlayImageLayer dataOverlayImageLayerCopy = new DataOverlayImageLayer(dataOverlayImageLayer.getModel(),
          dataOverlayImageLayer.getDirectory());
      this.addDataOverlayImageLayer(dataOverlayImageLayerCopy);
    }
  }

  public void addDataOverlayImageLayer(DataOverlayImageLayer dataOverlayImageLayer) {
    this.dataOverlayImageLayers.add(dataOverlayImageLayer);
    dataOverlayImageLayer.setDataOverlay(this);
  }

  /**
   * Prepares a copy of layout.
   *
   * @return copy of layout
   */
  public Layout copy() {
    if (this.getClass() == Layout.class) {
      return new Layout(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the hierarchicalView
   * @see #hierarchicalView
   */
  public boolean isHierarchicalView() {
    return hierarchicalView;
  }

  /**
   * @param hierarchicalView
   *          the hierarchicalView to set
   * @see #hierarchicalView
   */
  public void setHierarchicalView(boolean hierarchicalView) {
    this.hierarchicalView = hierarchicalView;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the title
   * @see #title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   *          the title to set
   * @see #title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the publicLayout
   * @see #publicLayout
   */
  public boolean isPublicLayout() {
    return publicLayout;
  }

  /**
   * @param publicLayout
   *          the publicLayout to set
   * @see #publicLayout
   */
  public void setPublicLayout(boolean publicLayout) {
    this.publicLayout = publicLayout;
  }

  /**
   * @return the creator
   * @see #creator
   */
  public User getCreator() {
    return creator;
  }

  /**
   * @param creator
   *          the creator to set
   * @see #creator
   */
  public void setCreator(User creator) {
    this.creator = creator;
  }

  /**
   * @return the status
   * @see #status
   */
  public LayoutStatus getStatus() {
    return status;
  }

  /**
   * @param status
   *          the status to set
   * @see #status
   */
  public void setStatus(LayoutStatus status) {
    this.status = status;
  }

  /**
   * @return the progress
   * @see #progress
   */
  public double getProgress() {
    return progress;
  }

  /**
   * @param progress
   *          the progress to set
   * @see #progress
   */
  public void setProgress(double progress) {
    this.progress = progress;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the inputData
   * @see #inputData
   */
  public UploadedFileEntry getInputData() {
    return inputData;
  }

  /**
   * @param inputData
   *          the inputData to set
   * @see #inputData
   */
  public void setInputData(UploadedFileEntry inputData) {
    this.inputData = inputData;
  }

  /**
   * @return the hierarchyViewLevel
   * @see #hierarchyViewLevel
   */
  public Integer getHierarchyViewLevel() {
    return hierarchyViewLevel;
  }

  /**
   * @param hierarchyViewLevel
   *          the hierarchyViewLevel to set
   * @see #hierarchyViewLevel
   */
  public void setHierarchyViewLevel(Integer hierarchyViewLevel) {
    this.hierarchyViewLevel = hierarchyViewLevel;
  }

  public boolean isDefaultOverlay() {
    return defaultOverlay;
  }

  public void setDefaultOverlay(boolean defaultOverlay) {
    this.defaultOverlay = defaultOverlay;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(int orderIndex) {
    this.orderIndex = orderIndex;
  }

  public boolean isGoogleLicenseConsent() {
    return googleLicenseConsent;
  }

  public void setGoogleLicenseConsent(boolean googleLicenseConsent) {
    this.googleLicenseConsent = googleLicenseConsent;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    Project oldProject = this.project;
    this.project = project;
    if (project == null && oldProject != null) {
      oldProject.removeLayout(this);
    }
  }

  public Set<DataOverlayImageLayer> getDataOverlayImageLayers() {
    return dataOverlayImageLayers;
  }

  public void setDataOverlayImageLayers(Set<DataOverlayImageLayer> dataOverlayImageLayers) {
    this.dataOverlayImageLayers = dataOverlayImageLayers;
  }

  public ColorSchemaType getColorSchemaType() {
    return colorSchemaType;
  }

  public void setColorSchemaType(ColorSchemaType colorSchemaType) {
    this.colorSchemaType = colorSchemaType;
  }

}
