package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.species.field.*;

/**
 * Entity representing protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("PROTEIN")
public abstract class Protein extends Species implements SpeciesWithBindingRegion, SpeciesWithResidue {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of modifications for the Protein.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Protein() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  protected Protein(Protein original) {
    super(original);
    for (ModificationResidue mr : original.getModificationResidues()) {
      addModificationResidue(mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  protected Protein(String elementId) {
    super(elementId);
  }

  /**
   * Adds modification to the protein.
   * 
   * @param modificationResidue
   *          modification to add
   */
  private void addModificationResidue(ModificationResidue modificationResidue) {
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);
  }

  @Override
  public void addBindingRegion(BindingRegion bindingRegion) {
    this.addModificationResidue(bindingRegion);
  }

  @Override
  public void addResidue(Residue residue) {
    this.addModificationResidue(residue);
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(List<ModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  public String getStringType() {
    return "Protein";
  }

}
