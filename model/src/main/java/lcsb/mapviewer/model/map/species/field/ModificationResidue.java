package lcsb.mapviewer.model.map.species.field;

import java.awt.geom.Point2D;
import java.io.Serializable;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class represent modification residue in a {@link Species}.
 * 
 * @author Piotr Gawron
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "modification_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_MODIFICATION_RESIDUE")
public abstract class ModificationResidue implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ModificationResidue.class.getName());

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Identifier of the modification. Must be unique in single map model.
   */
  private String idModificationResidue = "";

  /**
   * Name of the modification.
   */
  private String name = "";

  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D position = null;

  /**
   * Species to which this modification belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "idSpeciesDb", nullable = false)
  private Species species;

  /**
   * Default constructor.
   */
  public ModificationResidue() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param mr
   *          original object from which data is taken
   */
  public ModificationResidue(ModificationResidue mr) {
    this.idModificationResidue = mr.idModificationResidue;
    this.name = mr.name;
    this.position = mr.position;
  }

  public ModificationResidue(String modificationId) {
    this.idModificationResidue = modificationId;
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the idModificationResidue to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the id
   * @see #idModificationResidue
   */
  public String getIdModificationResidue() {
    return idModificationResidue;
  }

  /**
   * @param idModificationResidue
   *          the id to set
   * @see #idModificationResidue
   */
  public void setIdModificationResidue(String idModificationResidue) {
    this.idModificationResidue = idModificationResidue;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the species
   * @see #species
   */
  public Species getSpecies() {
    return species;
  }

  /**
   * @param species
   *          the species to set
   * @see #species
   */
  public void setSpecies(Species species) {
    this.species = species;
  }

  public Point2D getPosition() {
    return position;
  }

  public void setPosition(Point2D position) {
    this.position = position;
  }

  public ModificationResidue copy() {
    throw new NotImplementedException();
  }

}
