package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing "truncation operator" of one node into two nodes in the
 * reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRUNCATION_OPERATOR_NODE")
public class TruncationOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(TruncationOperator.class);

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public TruncationOperator(TruncationOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public TruncationOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "";
  }

  @Override
  public TruncationOperator copy() {
    if (this.getClass() == TruncationOperator.class) {
      return new TruncationOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
