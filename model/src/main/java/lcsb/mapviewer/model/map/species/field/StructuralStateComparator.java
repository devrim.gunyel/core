package lcsb.mapviewer.model.map.species.field;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.*;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Comparator of {@link MiriamData} object.
 * 
 * @author Piotr Gawron
 *
 */
public class StructuralStateComparator extends Comparator<StructuralState> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  private static Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   */
  public StructuralStateComparator() {
    this(Configuration.EPSILON);
  }

  public StructuralStateComparator(double epsilon) {
    super(StructuralState.class);
    this.epsilon=epsilon;
  }

  @Override
  protected int internalCompare(StructuralState arg0, StructuralState arg1) {
    StringComparator stringComparator = new StringComparator();
    if (stringComparator.compare(arg0.getValue(), arg1.getValue()) != 0) {
      return stringComparator.compare(arg0.getValue(), arg1.getValue());
    }
    PointComparator pointComparator = new PointComparator(epsilon);
    if (pointComparator.compare(arg0.getPosition(), arg1.getPosition()) != 0) {
      logger.debug("position different: " + arg0.getPosition() + ", " + arg1.getPosition());
      return pointComparator.compare(arg0.getPosition(), arg1.getPosition());
    }

    DoubleComparator doubleComparator = new DoubleComparator();
    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("width different: " + arg0.getWidth() + ", " + arg1.getWidth());
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }
    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      logger.debug("height different: " + arg0.getHeight() + ", " + arg1.getHeight());
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    return 0;
  }

}
