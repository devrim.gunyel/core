package lcsb.mapviewer.model.map;

/**
 * Exception thrown when model is inconsistent.
 * 
 * @author Piotr Gawron
 * 
 */
public class InconsistentModelException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with a message passed in the argument.
   * 
   * @param message
   *          text message of this exception
   */
  public InconsistentModelException(String message) {
    super(message);
  }

  public InconsistentModelException(String message, Throwable e) {
    super(message, e);
  }

  public InconsistentModelException(Exception e) {
    super(e);
  }
}
