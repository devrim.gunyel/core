package lcsb.mapviewer.model.map.model;

import java.util.*;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.*;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

/**
 * This interface defines functionality that the model container class should
 * implement to access data efficiently. It shouldn't be implemented by data
 * model.
 * 
 * @author Piotr Gawron
 * 
 */
public interface Model {

  Comparator<? super Model> ID_COMPARATOR = new Comparator<Model>() {

    @Override
    public int compare(Model o1, Model o2) {
      return o1.getId() - o2.getId();
    }
  };

  /**
   * Adds element to the model.
   * 
   * @param element
   *          element to add
   */
  void addElement(Element element);

  /**
   * 
   * @return model width
   */
  double getWidth();

  /**
   * Sets model width.
   * 
   * @param width
   *          new model width
   */
  void setWidth(double width);

  /**
   * Sets model width.
   *
   * @param text
   *          new model width
   */
  void setWidth(String text);

  /**
   *
   * @param width
   *          new {@link ModelData#width}
   */
  void setWidth(int width);

  /**
   *
   * @return model height
   */
  double getHeight();

  /**
   * Sets model height.
   *
   * @param height
   *          new model height
   */
  void setHeight(double height);

  /**
   * Sets model height.
   *
   * @param text
   *          new model height
   */
  void setHeight(String text);

  /**
   *
   * @param height
   *          new {@link ModelData#height}
   */
  void setHeight(int height);

  /**
   * Returns set of all elements.
   *
   * @return set of all elements
   */
  Set<Element> getElements();

  /**
   *
   * @param elements
   *          new {@link ModelData#elements} collection
   */
  void setElements(Set<Element> elements);

  /**
   * Returns element with the given element identifier ({@link Element#elementId}
   * ).
   *
   * @param idElement
   *          element identifier
   * @param <T>
   *          type of the object to be returned
   * @return {@link Element} with the given id
   */
  <T extends Element> T getElementByElementId(String idElement);

  /**
   * Adds reaction to the model.
   *
   * @param reaction
   *          reaction to add
   */
  void addReaction(Reaction reaction);

  /**
   * Returns set of reactions.
   *
   * @return set of reaction in the model
   */
  Set<Reaction> getReactions();

  /**
   * @return list of compartments
   */
  List<Compartment> getCompartments();

  /**
   * Adds layer to the model.
   *
   * @param layer
   *          object to add
   */
  void addLayer(Layer layer);

  /**
   *
   * @return set of layers
   */
  Set<Layer> getLayers();

  /**
   * Adds list of elements into model.
   *
   * @param elements
   *          list of elements
   */
  void addElements(Collection<? extends Element> elements);

  /**
   *
   * @return short description of the model
   */
  String getNotes();

  /**
   * Sets new short description of the model.
   *
   * @param notes
   *          new short description
   */
  void setNotes(String notes);

  /**
   * Returns reaction with the id given in the parameter.
   *
   * @param idReaction
   *          reaction identifier ({@link Reaction#idReaction})
   * @return reaction with the id given in the parameter
   */
  Reaction getReactionByReactionId(String idReaction);

  /**
   * Adds set of layers to the model.
   *
   * @param layers
   *          object to add
   */
  void addLayers(Collection<Layer> layers);

  /**
   * Adds {@link ElementGroup} to the model.
   *
   * @param elementGroup
   *          object to add
   */
  void addElementGroup(ElementGroup elementGroup);

  /**
   * Adds {@link BlockDiagram} to the model.
   *
   * @param blockDiagram
   *          object to add
   */
  void addBlockDiagream(BlockDiagram blockDiagram);

  /**
   * @return the idModel
   * @see Model#idModel
   */
  String getIdModel();

  /**
   * @param idModel
   *          the idModel to set
   * @see Model#idModel
   */
  void setIdModel(String idModel);

  /**
   * @return the tileSize
   * @see ModelData#tileSize
   */
  int getTileSize();

  /**
   * @param tileSize
   *          the tileSize to set
   * @see ModelData#tileSize
   */
  void setTileSize(int tileSize);

  /**
   * @return the zoomLevels
   * @see ModelData#zoomLevels
   */
  int getZoomLevels();

  /**
   * @param zoomLevels
   *          the zoomLevels to set
   * @see ModelData#zoomLevels
   */
  void setZoomLevels(int zoomLevels);

  /**
   * Removes reaction from model.
   *
   * @param reaction
   *          reaction to remove
   */
  void removeReaction(Reaction reaction);

  /**
   * Removes {@link Element} from the model.
   *
   * @param element
   *          element to remove
   */
  void removeElement(Element element);

  /**
   * Returns collection of all {@link Species} excluding {@link Complex}.
   *
   * @return collection of all {@link Species} excluding {@link Complex}.
   */
  Collection<Species> getNotComplexSpeciesList();

  /**
   * Returns list of all {@link Species} in the model.
   *
   * @return list of all {@link Species} in the model
   */
  List<Species> getSpeciesList();

  /**
   * Returns collection of {@link Complex}.
   *
   * @return collection of {@link Complex}
   */
  Collection<Complex> getComplexList();

  /**
   * Adds reactions to model.
   *
   * @param reactions2
   *          list of reaction to add
   */
  void addReactions(List<Reaction> reactions2);

  /**
   * Returns list of elements annotated by the {@link MiriamData}.
   *
   * @param miriamData
   *          {@link MiriamData}
   * @return list of elements
   */
  Set<BioEntity> getElementsByAnnotation(MiriamData miriamData);

  /**
   * Returns list of elements with given name.
   *
   * @param name
   *          name of the element
   * @return list of elements with given name
   */
  List<Element> getElementsByName(String name);

  /**
   * Returns {@link Element} for given database identifier.
   *
   * @param dbId
   *          element database identifier ({@link Element#id})
   * @return {@link Element} for a given id
   */
  <T extends Element> T getElementByDbId(Integer dbId);

  /**
   * Returns {@link Reaction} for given database identifier.
   *
   * @param dbId
   *          reaction database identifier ({@link Reaction#id})
   * @return {@link Reaction} for a given id
   */
  Reaction getReactionByDbId(Integer dbId);

  /**
   * Returns sorted by size list of compartments.
   *
   * @return list of compartment sorted by size
   */
  List<Compartment> getSortedCompartments();

  /**
   * Returns list of elements sorted by the size.
   *
   * @return list of elements sorted by the size
   */
  List<Element> getElementsSortedBySize();

  /**
   * 
   * @return {@link ModelData#project}
   */
  Project getProject();

  /**
   *
   * @param project
   *          new {@link ModelData#project}
   */
  void setProject(Project project);

  /**
   * @return the modelData
   */
  ModelData getModelData();

  /**
   * 
   * @return {@link ModelData#id}
   */
  Integer getId();

  /**
   * Sets database identifier of the model.
   *
   * @param id
   *          database identifier
   */
  void setId(int id);

  /**
   * Adds submodel connection.
   *
   * @param submodel
   *          submodel to add
   */
  void addSubmodelConnection(ModelSubmodelConnection submodel);

  /**
   * Returns set of submodel connections.
   *
   * @return collection of submodels
   */
  Collection<ModelSubmodelConnection> getSubmodelConnections();

  /**
   * Returns name of the model.
   *
   * @return name of the model
   */
  String getName();

  /**
   * Sets name of the model.
   *
   * @param name
   *          name of the model
   */
  void setName(String name);

  /**
   * Returns {@link Model submodel} by the {@link ModelData#id database
   * identifier} given in the parameter.
   *
   * @param idObject
   *          the {@link ModelData#id database identifier} that identifies
   *          submodel
   * @return {@link Model submodel} by the {@link ModelData#id database
   *         identifier} given in the parameter
   */
  Model getSubmodelById(Integer idObject);

  /**
   * Returns set of connections that point to this model. Be very carefoul with
   * using this function as the implementation forces lazy loading of the maps.
   *
   * @return set of connections that point to this model
   */
  Collection<SubmodelConnection> getParentModels();

  /**
   * Returns connection to a submodel identified by connection name.
   *
   * @param name
   *          name of the connection
   * @return connection to a submodel identified by connection name
   */
  Model getSubmodelByConnectionName(String name);

  /**
   * Returns connection to a submodel identified by connection identifier.
   *
   * @param id
   *          id of the connection
   * @return connection to a submodel identified by connection identifier
   */
  SubmodelConnection getSubmodelConnectionById(Integer id);

  /**
   * Returns submodel identified by submodel identifier.
   *
   * @param identifier
   *          identifier of the model
   * @return submodel identified by identifier
   */
  Model getSubmodelById(String identifier);

  /**
   * Returns collection of {@link Model submodels}.
   *
   * @return collection of {@link Model submodels}
   */
  Collection<Model> getSubmodels();

  /**
   * Returns {@link Model submodel} identified by the {@link ModelData#name model
   * name}. It returns this 'parent' object when the names matches.
   *
   * @param name
   *          name of the submodel that should be returned
   * @return {@link Model submodel} identified by the {@link ModelData#name model
   *         name}
   */
  Model getSubmodelByName(String name);

  /**
   * Return list of all {@link BioEntity} in the map. This includes all
   * {@link Reaction reactions} and {@link Element elements}.
   * 
   * @return list of all {@link BioEntity} in the map
   */
  List<BioEntity> getBioEntities();

  Double getDefaultCenterX();

  void setDefaultCenterX(Double defaultCenterX);

  Double getDefaultCenterY();

  void setDefaultCenterY(Double defaultCenterY);

  Integer getDefaultZoomLevel();

  void setDefaultZoomLevel(Integer defaultZoomLevel);

  Set<SbmlFunction> getFunctions();

  void addFunctions(Collection<SbmlFunction> sbmlFunctions);

  Set<SbmlParameter> getParameters();

  void addUnits(Collection<SbmlUnit> units);

  Set<SbmlUnit> getUnits();

  void addUnit(SbmlUnit volume);

  SbmlUnit getUnitsByUnitId(String unitId);

  void addParameters(Collection<SbmlParameter> sbmlParamters);

  void addParameter(SbmlParameter sbmlParameter);

  SbmlParameter getParameterById(String parameterId);

  void addFunction(SbmlFunction sbmlFunction);

  SbmlArgument getFunctionById(String id);

  void removeReactions(Collection<Reaction> reactions);

  Set<MiriamData> getMiriamData();

  void addMiriamData(MiriamData md);

  void addMiriamData(Collection<MiriamData> parseRdfNode);

  public void addAuthor(Author author);

  void addAuthors(Collection<Author> authorsFromRdf);

  public List<Author> getAuthors();

  public Calendar getCreationDate();

  public void setCreationDate(Calendar creationDate);

  public void addModificationDate(Calendar modificationDate);

  public List<Calendar> getModificationDates();

  void addModificationDates(Collection<Calendar> modificationDatesFromRdf);

  Set<Drawable> getDrawables();
}
