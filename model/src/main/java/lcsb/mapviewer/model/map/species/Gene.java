package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Entity representing gene element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("GENE")
public class Gene extends Species implements SpeciesWithCodingRegion, SpeciesWithModificationSite,
    SpeciesWithRegulatoryRegion, SpeciesWithTranscriptionSite {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Gene.class);
  /**
   * List of modifications for the Gene.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Gene() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Gene(Gene original) {
    super(original);
    for (ModificationResidue mr : original.getModificationResidues()) {
      addModificationResidue(mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public Gene(String elementId) {
    setElementId(elementId);
  }

  /**
   * Adds {@link ModificationResidue}.
   * 
   * @param modificationResidue
   *          {@link ModificationResidue} to be added
   */
  private void addModificationResidue(ModificationResidue modificationResidue) {
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);

  }

  @Override
  public void addCodingRegion(CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(ModificationSite modificationSite) {
    this.addModificationResidue(modificationSite);
  }

  @Override
  public void addRegulatoryRegion(RegulatoryRegion regulatoryRegion) {
    this.addModificationResidue(regulatoryRegion);
  }

  @Override
  public void addTranscriptionSite(TranscriptionSite transcriptionSite) {
    this.addModificationResidue(transcriptionSite);
  }

  @Override
  public Gene copy() {
    if (this.getClass() == Gene.class) {
      return new Gene(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(List<ModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  public String getStringType() {
    return "Gene";
  }

}
