package lcsb.mapviewer.model.map.species.field;

import java.text.DecimalFormat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

@Entity
@DiscriminatorValue("ABSTRACT_REGION_MODIFICATION")
public abstract class AbstractRegionModification extends ModificationResidue {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default size of the object (in graphical representation).
   */
  private static final double DEFAULT_SIZE = 10;

  /**
   * Width of the region in the graphic representation.
   */
  private double width = DEFAULT_SIZE;

  /**
   * Height of the region in the graphic representation.
   */
  private double height = DEFAULT_SIZE;

  public AbstractRegionModification() {
    super();
  }

  public AbstractRegionModification(AbstractRegionModification mr) {
    super(mr);
    this.width = mr.getWidth();
    this.height = mr.getHeight();
  }

  @Override
  public String toString() {
    DecimalFormat format = new DecimalFormat("#.##");
    String position;
    if (getPosition() == null) {
      position = null;
    } else {
      position = "Point2D[" + format.format(getPosition().getX()) + "," + format.format(getPosition().getY()) + "]";
    }
    String result = "[" + this.getClass().getSimpleName() + "]: " + getName() + "," + getWidth() + "," + getHeight()
        + "," + position;
    return result;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  /**
   * Update data in this object from parameter (only if values in parameter object
   * are valid).
   * 
   * @param mr
   *          object from which we are updating data
   */
  public void update(AbstractRegionModification mr) {
    if (this.getIdModificationResidue() != null && !this.getIdModificationResidue().equals("")
        && !this.getIdModificationResidue().equals(mr.getIdModificationResidue())) {
      throw new InvalidArgumentException("Cannot update from mr with different id");
    }
    if (mr.getWidth() > 0 && mr.getWidth() != DEFAULT_SIZE) {
      this.setWidth(mr.getWidth());
    }
    if (mr.getHeight() > 0 && mr.getHeight() != DEFAULT_SIZE) {
      this.setHeight(mr.getHeight());
    }
    if (mr.getName() != null) {
      this.setName(mr.getName());
    }
    if (mr.getPosition() != null) {
      this.setPosition(mr.getPosition());
    }
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }
}