package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner unknown catalysis reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_CATALYSIS_REACTION")
public class UnknownCatalysisReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public UnknownCatalysisReaction() {
    super();
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param result
   *          parent reaction from which we copy data
   */
  public UnknownCatalysisReaction(Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown catalysis";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownCatalysisReaction copy() {
    if (this.getClass() == UnknownCatalysisReaction.class) {
      return new UnknownCatalysisReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
