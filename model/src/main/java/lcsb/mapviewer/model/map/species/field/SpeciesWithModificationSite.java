package lcsb.mapviewer.model.map.species.field;

public interface SpeciesWithModificationSite extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link ModificationSite } to the species.
   * 
   * @param codingRegion
   *          {@link ModificationSite } to add
   */
  void addModificationSite(ModificationSite modificationSite);

}
