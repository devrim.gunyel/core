package lcsb.mapviewer.model.map.species.field;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Protein;

/**
 * This structure contains information about Residue for one of {@link Protein}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("RESIDUE")
public class Residue extends AbstractSiteModification {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Residue() {
    super();
  }

  public Residue(Residue residue) {
    super(residue);
  }

  public Residue(String modificationId) {
    super(modificationId);
  }

  /**
   * Creates copy of the object.
   * 
   * @return copy of the object.
   */
  public Residue copy() {
    if (this.getClass() == Residue.class) {
      return new Residue(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
