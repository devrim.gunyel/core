package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;

import javax.persistence.*;

/**
 * This object defines information about mapping genes to gene. This data is
 * stored in external file (from some database, server, etc.). For now this file
 * should be in
 * <a href="https://genome.ucsc.edu/goldenpath/help/bigBed.html">big bed
 * format</a>.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
public class ReferenceGenomeGeneMapping implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * {@link ReferenceGenome Reference genome} for which gene mapping is provided.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ReferenceGenome referenceGenome;

  /**
   * Name of the mapping.
   */
  private String name;

  /**
   * Url to source file which provides mapping.
   */
  private String sourceUrl;

  /**
   * What is the progress of obtaining file.
   */
  private double downloadProgress;

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the referenceGenome
   * @see #referenceGenome
   */
  public ReferenceGenome getReferenceGenome() {
    return referenceGenome;
  }

  /**
   * @param referenceGenome
   *          the referenceGenome to set
   * @see #referenceGenome
   */
  public void setReferenceGenome(ReferenceGenome referenceGenome) {
    this.referenceGenome = referenceGenome;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the sourceUrl
   * @see #sourceUrl
   */
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * @param sourceUrl
   *          the sourceUrl to set
   * @see #sourceUrl
   */
  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * @return the downloadProgress
   * @see #downloadProgress
   */
  public double getDownloadProgress() {
    return downloadProgress;
  }

  /**
   * @param downloadProgress
   *          the downloadProgress to set
   * @see #downloadProgress
   */
  public void setDownloadProgress(double downloadProgress) {
    this.downloadProgress = downloadProgress;
  }
}
