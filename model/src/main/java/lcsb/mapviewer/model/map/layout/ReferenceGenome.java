package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * Reference genome describes data used as reference genome. This data is
 * obtained usually from external server, database. Right now we support only
 * .2bit file format.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
public class ReferenceGenome implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Organism for which the genome is provided.
   */
  @Cascade({ CascadeType.ALL })
  @OneToOne
  private MiriamData organism;

  /**
   * Type of reference genome. This describe source (database) from which
   * reference genome was taken.
   */
  @Enumerated(EnumType.STRING)
  private ReferenceGenomeType type;

  /**
   * Version of the reference genome (databases have different releases, this
   * string represent specific release of data).
   */
  private String version;

  /**
   * How much of the file we already downloaded.
   */
  private double downloadProgress;

  /**
   * Source url used to obtain data.
   */
  private String sourceUrl;

  /**
   * List of different mappings to this genome. Reference genome is a string
   * containing nucleotides. However interpretation of these nucleotides is a
   * different thing. Many databases provides different mappings between genes and
   * genome (even for the same genome). Therefore we allow to have more gene
   * mappings.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "referenceGenome", orphanRemoval = true)
  @OrderBy("id")
  private List<ReferenceGenomeGeneMapping> geneMapping = new ArrayList<>();

  /**
   * @return the type
   * @see #type
   */
  public ReferenceGenomeType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(ReferenceGenomeType type) {
    this.type = type;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public MiriamData getOrganism() {
    return organism;
  }

  /**
   * @param organism
   *          the organism to set
   * @see #organism
   */
  public void setOrganism(MiriamData organism) {
    this.organism = organism;
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version
   *          the version to set
   * @see #version
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * @return the sourceUrl
   * @see #sourceUrl
   */
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * @param sourceUrl
   *          the sourceUrl to set
   * @see #sourceUrl
   */
  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * @return the downloadProgress
   * @see #downloadProgress
   */
  public double getDownloadProgress() {
    return downloadProgress;
  }

  /**
   * @param downloadProgress
   *          the downloadProgress to set
   * @see #downloadProgress
   */
  public void setDownloadProgress(double downloadProgress) {
    this.downloadProgress = downloadProgress;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Adds {@link ReferenceGenomeGeneMapping gene mapping} to the reference genome.
   * 
   * @param mapping
   *          mapping to add
   */
  public void addReferenceGenomeGeneMapping(ReferenceGenomeGeneMapping mapping) {
    geneMapping.add(mapping);
    mapping.setReferenceGenome(this);
  }

  /**
   * @return the geneMapping
   * @see #geneMapping
   */
  public List<ReferenceGenomeGeneMapping> getGeneMapping() {
    return geneMapping;
  }

  /**
   * @param geneMapping
   *          the geneMapping to set
   * @see #geneMapping
   */
  public void setGeneMapping(List<ReferenceGenomeGeneMapping> geneMapping) {
    this.geneMapping = geneMapping;
  }
}
