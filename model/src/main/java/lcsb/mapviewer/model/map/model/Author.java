package lcsb.mapviewer.model.map.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing author object.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@XmlRootElement
public class Author implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Author.class);
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * First name.
   */
  private String firstName;

  /**
   * Last name.
   */
  private String lastName;

  /**
   * Email address.
   */
  private String email;

  /**
   * Organization.
   */
  private String organisation;

  /**
   * Default constructor
   * 
   * @param firstName
   *          first name
   * @param lastName
   *          last name
   */
  public Author(String firstName, String lastName) {
    setFirstName(firstName);
    setLastName(lastName);
  }

  /**
   * Empty constructor for hibernate.
   */
  protected Author() {

  }

  /**
   * Constructor that creates a copy of the author.
   * 
   * @param author
   *          original object from which we copy
   */
  public Author(Author author) {
    setFirstName(author.getFirstName());
    setLastName(author.getLastName());
    setEmail(author.getEmail());
    setOrganisation(author.getOrganisation());
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(String organisation) {
    this.organisation = organisation;
  }

  public Author copy() {
    return new Author(this);
  }

  @Override
  public String toString() {
    return "[" + this.getClass() + "] " + firstName + " " + lastName + "; " + email + "; " + organisation;
  }
}
