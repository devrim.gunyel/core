package lcsb.mapviewer.model.map.species;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.*;

/**
 * Abstract class representing objects in the model. Elements are objects that
 * contains graphical representation data for element and meta information about
 * element (like symbol, name, etc.).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "element_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC")
public abstract class Element implements BioEntity, Serializable, SbmlArgument {

  /**
   * Comparator of elements that takes into consideration size (width*height) of
   * elements.
   */
  public static final Comparator<Element> SIZE_COMPARATOR = new Comparator<Element>() {
    @Override
    public int compare(Element element1, Element element2) {

      double size = element1.getWidth() * element1.getHeight();
      double size2 = element2.getWidth() * element2.getHeight();
      if (size < size2) {
        return 1;
      } else if (size > size2) {
        return -1;
      } else {
        return 0;
      }
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default font size for element description.
   */
  private static final double DEFAULT_FONT_SIZE = 12.0;
  /**
   * Maximum length of the valid synonym name.
   */
  private static final int MAX_SYNONYM_LENGTH = 255;
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(Element.class);

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Map model object to which element belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * When defined this represent glyph that should be used for drawing this
   * element.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Glyph glyph;

  /**
   * Submodel that is extension of these element.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
  private ElementSubmodelConnection submodel;

  /**
   * {@link Compartment} where element is located. When element lies outside of
   * every compartment then null value is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @Cascade({ CascadeType.SAVE_UPDATE })
  private Compartment compartment;

  /**
   * Unique string identifier within one model object (usually imported from
   * external source from which map was imported).
   */
  private String elementId;

  /**
   * X coordinate on the map where element is located (top left corner).
   */
  private Double x;

  /**
   * Y coordinate on the map where element is located (top left corner).
   */
  private Double y;

  /**
   * Z-index of elements on the map.
   */
  private Integer z = null;

  /**
   * Width of the element.
   */
  private Double width;

  /**
   * Height of the element.
   */
  private Double height;

  /**
   * Size of the font used for description.
   */
  @Column(nullable = false)
  private Double fontSize;

  /**
   * Color of the font.
   */
  @Column(nullable = false)
  private Color fontColor = Color.BLACK;

  /**
   * Color of filling.
   */
  @Column(nullable = false)
  private Color fillColor;

  /**
   * Color of filling.
   */
  @Column(nullable = false)
  private Color borderColor = Color.BLACK;

  /**
   * From which level element should be visible.
   *
   * @see #transparencyLevel
   * @see lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params#level
   *      AbstractImageGenerator.Params#level
   */
  private String visibilityLevel = "";

  /**
   * From which level element should be transparent.
   *
   * @see #visibilityLevel
   * @see AbstractImageGenerator.Params#level
   */
  private String transparencyLevel = "";

  /**
   * List of search indexes that describe this element.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "source", orphanRemoval = true)
  private List<SearchIndex> searchIndexes = new ArrayList<>();

  /**
   * Notes describing this element.
   */
  @Column(columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Symbol of the element.
   */
  private String symbol;

  /**
   * Full name of the element.
   */
  private String fullName;

  /**
   * Abbreviation associated with the element.
   */
  private String abbreviation;

  /**
   * Formula associated with the element.
   */
  private String formula;

  /**
   * Short name of the element.
   */
  private String name = "";

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "element_synonyms", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "synonym")
  @OrderColumn(name = "idx")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<String> synonyms = new ArrayList<>();

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "element_former_symbols", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "symbol")
  @OrderColumn(name = "idx")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<String> formerSymbols = new ArrayList<>();

  /**
   * Set of miriam annotations for this element.
   */
  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "element_miriam", joinColumns = {
      @JoinColumn(name = "element_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original
   *          original object that will be used for creating copy
   */
  protected Element(Element original) {
    elementId = original.getElementId();
    x = original.getX();
    y = original.getY();
    z = original.getZ();
    width = original.getWidth();
    height = original.getHeight();
    fontSize = original.getFontSize();
    fillColor = original.getFillColor();
    fontColor = original.getFontColor();
    for (SearchIndex searchIndex : original.getSearchIndexes()) {
      searchIndexes.add(searchIndex.copy());
    }
    if (original.getSubmodel() != null) {
      setSubmodel(original.getSubmodel().copy());
    }

    this.notes = original.notes;
    this.symbol = original.symbol;
    this.fullName = original.fullName;
    this.name = original.getName();
    addSynonyms(original.getSynonyms());
    addFormerSymbols(original.getFormerSymbols());

    addMiriamData(original.getMiriamData());
    this.abbreviation = original.getAbbreviation();
    this.formula = original.getFormula();
    setVisibilityLevel(original.getVisibilityLevel());
    setTransparencyLevel(original.getTransparencyLevel());
    if (original.getGlyph() != null) {
      setGlyph(new Glyph(original.getGlyph()));
    }
    setBorderColor(original.getBorderColor());
  }

  /**
   * Empty constructor required by hibernate.
   */
  protected Element() {
    super();
    elementId = "";
    x = 0.0;
    y = 0.0;
    width = 0.0;
    height = 0.0;
    fontSize = DEFAULT_FONT_SIZE;
    fillColor = Color.white;
  }

  /**
   * Adds list of former symbol to the object.
   *
   * @param formerSymbols
   *          list of former symbols to add
   */
  public void addFormerSymbols(List<String> formerSymbols) {
    this.formerSymbols.addAll(formerSymbols);
  }

  /**
   * Returns x coordinate of the element center.
   *
   * @return x coordinate of the element center
   */
  public double getCenterX() {
    return getX() + getWidth() / 2;
  }

  /**
   * Returns y coordinate of the element center.
   *
   * @return y coordinate of the element center
   */
  public double getCenterY() {
    return getY() + getHeight() / 2;
  }

  /**
   * Returns coordinates of the element center point.
   *
   * @return coordinates of the element center point
   */
  public Point2D getCenter() {
    return new Point2D.Double(getCenterX(), getCenterY());
  }

  /**
   * Methods that increase size of the element by increaseSize. It influence x, y,
   * width and height fields of the element.
   *
   * @param increaseSize
   *          by how many units we want to increase size of the element
   */
  public void increaseBorder(double increaseSize) {
    x -= increaseSize;
    y -= increaseSize;
    this.width += increaseSize * 2;
    this.height += increaseSize * 2;
  }

  /**
   * This method computes the distance between point and the element. It assumes
   * that element is a rectangle.
   *
   * @param point
   *          point from which we are looking for a distance
   * @return distance between point and this element
   */
  public double getDistanceFromPoint(Point2D point) {
    if (contains(point)) {
      return 0;
    } else if (getX() <= point.getX() && getX() + getWidth() >= point.getX()) {
      return Math.min(Math.abs(point.getY() - getY()), Math.abs(point.getY() - (getY() + getHeight())));
    } else if (getY() <= point.getY() && getY() + getHeight() >= point.getY()) {
      return Math.min(Math.abs(point.getX() - getX()), Math.abs(point.getX() - (getY() + getWidth())));
    } else {
      double distance1 = point.distance(getX(), getY());
      double distance2 = point.distance(getX() + getWidth(), getY());
      double distance3 = point.distance(getX(), getY() + getHeight());
      double distance4 = point.distance(getX() + getWidth(), getY() + getHeight());
      return Math.min(Math.min(distance1, distance2), Math.min(distance3, distance4));
    }
  }

  /**
   * Checks if element contains a point. It assumes that element is a rectangle.
   *
   * @param point
   *          point to check
   * @return <i>true</i> if point belongs to the element, <i>false</i> otherwise
   */
  public boolean contains(Point2D point) {
    if (getX() <= point.getX() && getY() <= point.getY() && getX() + getWidth() >= point.getX()
        && getY() + getHeight() >= point.getY()) {
      return true;
    }
    return false;
  }

  /**
   * Returns a rectangle that determines a rectangle border.
   *
   * @return rectangle border
   */
  public Rectangle2D getBorder() {
    if (x == null || y == null || width == null || height == null || width == 0.0 || height == 0.0) {
      return null;
    }
    return new Rectangle2D.Double(x, y, width, height);
  }

  public void setBorder(Rectangle2D border) {
    setX(border.getX());
    setY(border.getY());
    setWidth(border.getWidth());
    setHeight(border.getHeight());
  }

  /**
   * Checks if the element2 (given as parameter) is placed inside this element.
   * This method should be overridden by all inheriting classes - it should
   * properly handle other shapes than rectangle.
   *
   * @param element2
   *          object to be checked
   * @return true if element2 lies in this object, false otherwise
   */
  public boolean contains(Element element2) {
    if (element2 instanceof Species) {
      Point2D p1 = new Point2D.Double(element2.getX(), element2.getY());
      Point2D p2 = new Point2D.Double(element2.getX(), element2.getY() + element2.getHeight());
      Point2D p3 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY());
      Point2D p4 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY() + element2.getHeight());
      return contains(p1) || contains(p2) || contains(p3) || contains(p4);
    } else if (getX() < element2.getX() && getY() < element2.getY()
        && getX() + getWidth() > element2.getX() + element2.getWidth()
        && getY() + getHeight() > element2.getY() + element2.getHeight()) {
      return true;
    }
    return false;
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * Parse and set x coordinate from string.
   *
   * @param string
   *          text representing x coordinate
   * @see x
   */
  public void setX(String string) {
    setX(Double.parseDouble(string));
  }

  /**
   *
   * @param x
   *          the x value to set
   * @see x
   */
  public void setX(int x) {
    setX((double) x);
  }

  /**
   * @param x
   *          the x to set
   * @see #x
   */
  public void setX(Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * Parse and set y coordinate from string.
   *
   * @param string
   *          text representing y coordinate
   * @see y
   */
  public void setY(String string) {
    this.y = Double.parseDouble(string);
  }

  /**
   *
   * @param y
   *          the y value to set
   * @see y
   */
  public void setY(int y) {
    setY((double) y);
  }

  /**
   * @param y
   *          the y to set
   * @see #y
   */
  public void setY(Double y) {
    this.y = y;
  }

  /**
   * Makes a copy of the element.
   *
   * @return copy of the element
   */
  public abstract Element copy();

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Parse and set width.
   *
   * @param string
   *          text representing width
   * @see width
   */
  public void setWidth(String string) {
    try {
      width = Double.parseDouble(string);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width format: " + string, e);
    }
  }

  /**
   *
   * @param width
   *          the width value to set
   * @see #width
   */
  public void setWidth(int width) {
    this.width = (double) width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Parse and set height.
   *
   * @param string
   *          text representing height
   * @see height
   */
  public void setHeight(String string) {
    try {
      height = Double.parseDouble(string);
    } catch (Exception e) {
      throw new InvalidArgumentException("Invalid height format: " + string, e);
    }
  }

  /**
   *
   * @param height
   *          the height value to set
   * @see #height
   */
  public void setHeight(int height) {
    this.height = (double) height;
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(Double height) {
    this.height = height;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * Parse and set font size.
   *
   * @param string
   *          text representing font size
   * @see fontSize
   */
  public void setFontSize(String string) {
    this.fontSize = Double.parseDouble(string);
  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(Double fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(int fontSize) {
    setFontSize((double) fontSize);
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModelData(ModelData model) {
    this.model = model;
  }

  /**
   * @return the compartment
   * @see #compartment
   */
  public Compartment getCompartment() {
    return compartment;
  }

  @Override
  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  /**
   * @param compartment
   *          the compartment to set
   * @see #compartment
   */
  public void setCompartment(Compartment compartment) {
    if (this.compartment != null && compartment != this.compartment) {
      Compartment formerCompartment = this.compartment;
      this.compartment = null;
      formerCompartment.removeElement(this);
    }
    this.compartment = compartment;
  }

  @Override
  public void setVisibilityLevel(String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  /**
   * @return the color
   * @see #fillColor
   */
  public Color getFillColor() {
    return fillColor;
  }

  @Override
  public void setVisibilityLevel(Integer visibilityLevel) {
    if (visibilityLevel == null) {
      this.visibilityLevel = null;
    } else {
      this.visibilityLevel = visibilityLevel + "";
    }
  }

  /**
   * @param color
   *          the color to set
   * @see #fillColor
   */
  public void setFillColor(Color color) {
    this.fillColor = color;
  }

  /**
   * @return the transparencyLevel
   * @see #transparencyLevel
   */
  public String getTransparencyLevel() {
    return transparencyLevel;
  }

  /**
   * @param transparencyLevel
   *          the transparencyLevel to set
   * @see #transparencyLevel
   */
  public void setTransparencyLevel(String transparencyLevel) {
    this.transparencyLevel = transparencyLevel;
  }

  /**
   * @return the searchIndexes
   * @see #searchIndexes
   */
  public List<SearchIndex> getSearchIndexes() {
    return searchIndexes;
  }

  /**
   *
   * @param searchIndexes
   *          new {@link #searchIndexes} list
   */
  public void setSearchIndexes(List<SearchIndex> searchIndexes) {
    this.searchIndexes.clear();
    for (SearchIndex searchIndex : searchIndexes) {
      searchIndex.setSource(this);
    }
    this.searchIndexes.addAll(searchIndexes);
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ElementSubmodelConnection getSubmodel() {
    return submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodel(ElementSubmodelConnection submodel) {
    this.submodel = submodel;
    if (submodel != null) {
      this.submodel.setFromElement(this);
    }
  }

  /**
   * Checks if element contains a {@link LayerText}.
   *
   * @param lt
   *          text to check
   * @return <i>true</i> if {@link LayerText} belongs to the element, <i>false</i>
   *         otherwise
   */
  public boolean contains(LayerText lt) {
    return getX() < lt.getX() && getY() < lt.getY() && getX() + getWidth() > lt.getX() + lt.getWidth()
        && getY() + getHeight() > lt.getY() + lt.getHeight();
  }

  /**
   * Adds {@link SearchIndex} to the element.
   *
   * @param searchIndex
   *          search index to add
   * @see #searchIndexes
   */
  public void addSearchIndex(SearchIndex searchIndex) {
    searchIndexes.add(searchIndex);
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName
   *          the fullName to set
   * @see #fullName
   */
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  @XmlTransient
  @Override
  public Model getModel() {
    if (model == null) {
      return null;
    }
    return model.getModel();
  }

  /**
   * @return the formerSymbols
   * @see #formerSymbols
   */
  public List<String> getFormerSymbols() {
    return formerSymbols;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModel(Model model) {
    this.model = model.getModelData();
  }

  /**
   * @param formerSymbols
   *          the formerSymbols to set
   * @see #formerSymbols
   */
  public void setFormerSymbols(List<String> formerSymbols) {
    this.formerSymbols = formerSymbols;
  }

  /**
   * @return the miriamData
   * @see #miriamData
   */
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * @param miriamData
   *          the miriamData to set
   * @see #miriamData
   */
  public void setMiriamData(Set<MiriamData> miriamData) {
    this.miriamData = miriamData;
  }

  @Override
  public void addMiriamData(Collection<MiriamData> miriamData) {
    for (MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addMiriamData(MiriamData md) {
    if (this.miriamData.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getElementId()
          + " already exists. Ignoring...");
    } else {
      this.miriamData.add(md);
    }
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String getNotes() {
    return notes;
  }

  @Override
  public void setNotes(String notes) {
    if (notes != null) {
      if (notes.contains("</html>")) {
        throw new InvalidArgumentException("Notes cannot contain html tags...");
      }
    }
    this.notes = notes;
  }

  /**
   * @return the symbol
   * @see #symbol
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * @param symbol
   *          the symbol to set
   * @see #symbol
   */
  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @return the formula
   * @see #formula
   */
  public String getFormula() {
    return formula;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @param formula
   *          the formula to set
   * @see #formula
   */
  public void setFormula(String formula) {
    this.formula = formula;
  }

  /**
   * @return the abbreviation
   * @see #abbreviation
   */
  public String getAbbreviation() {
    return abbreviation;
  }

  /**
   * @param abbreviation
   *          the abbreviation to set
   * @see #abbreviation
   */
  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  /**
   * Adds synonyms to the element.
   *
   * @param synonyms
   *          list of synonyms to be added
   */
  public void addSynonyms(List<String> synonyms) {
    for (String string : synonyms) {
      if (string.length() > MAX_SYNONYM_LENGTH) {
        String message = " <Synonym too long. Only " + MAX_SYNONYM_LENGTH + " characters are allowed>";
        this.getSynonyms().add(string.substring(0, MAX_SYNONYM_LENGTH - message.length()) + message);
      } else {
        this.getSynonyms().add(string);
      }
    }

  }

  /**
   * Adds former symbol to the object.
   *
   * @param formerSymbol
   *          former symbol to add
   */
  public void addFormerSymbol(String formerSymbol) {
    formerSymbols.add(formerSymbol);
  }

  /**
   * Adds synonym to object.
   *
   * @param synonym
   *          synonym to be added
   */
  public void addSynonym(String synonym) {
    synonyms.add(synonym);
  }

  public Color getFontColor() {
    return fontColor;
  }

  public void setFontColor(Color fontColor) {
    this.fontColor = fontColor;
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return elementId;
  }

  /**
   * Returns size of the element in square units.
   *
   * @return size of the element
   */
  public double getSize() {
    return getWidth() * getHeight();
  }

  /**
   * @param elementId
   *          the elementId to set
   * @see #elementId
   */
  public void setElementId(String elementId) {
    this.elementId = elementId;
  }

  public Glyph getGlyph() {
    return glyph;
  }

  public void setGlyph(Glyph glyph) {
    this.glyph = glyph;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(Color borderColor) {
    this.borderColor = borderColor;
  }

}