package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Regulatory Region for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Gene}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("REGULATORY_REGION")
public class RegulatoryRegion extends AbstractRegionModification implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(RegulatoryRegion.class);

  /**
   * Default constructor.
   */
  public RegulatoryRegion() {

  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public RegulatoryRegion(RegulatoryRegion original) {
    super(original);
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  public RegulatoryRegion copy() {
    if (this.getClass() == RegulatoryRegion.class) {
      return new RegulatoryRegion(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }
}
