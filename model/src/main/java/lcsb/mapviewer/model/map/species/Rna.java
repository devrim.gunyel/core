package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.*;

/**
 * Entity representing rna element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("RNA")
public class Rna extends Species
    implements SpeciesWithCodingRegion, SpeciesWithProteinBindingDomain, SpeciesWithModificationSite {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of rna regions (some rna sequences) in this object.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ModificationResidue> regions = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Rna() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Rna(Rna original) {
    super(original);
    for (ModificationResidue region : original.getRegions()) {
      addModificationResidue(region.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          uniqe (within model) element identifier
   */
  public Rna(String elementId) {
    super();
    setElementId(elementId);
  }

  /**
   * Adds {@link RnaRegion}.
   * 
   * @param rnaRegion
   *          object to be added
   */
  private void addModificationResidue(ModificationResidue antisenseRnaRegion) {
    regions.add(antisenseRnaRegion);
    antisenseRnaRegion.setSpecies(this);
  }

  @Override
  public void addCodingRegion(CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addProteinBindingDomain(ProteinBindingDomain codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(ModificationSite codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public Rna copy() {
    if (this.getClass() == Rna.class) {
      return new Rna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the regions
   * @see #regions
   */
  public List<ModificationResidue> getRegions() {
    return regions;
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(List<ModificationResidue> regions) {
    this.regions = regions;
  }

  @Override
  public List<ModificationResidue> getModificationResidues() {
    return getRegions();
  }

  @Override
  public String getStringType() {
    return "RNA";
  }

}
