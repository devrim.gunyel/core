package lcsb.mapviewer.model.graphics;

public enum MapCanvasType {
  GOOGLE_MAPS_API("Google Maps API"),
  OPEN_LAYERS("OpenLayers");

  private String commonName;

  private MapCanvasType(String commonName) {
    this.commonName = commonName;
  }

  public String getCommonName() {
    return commonName;
  }
}
