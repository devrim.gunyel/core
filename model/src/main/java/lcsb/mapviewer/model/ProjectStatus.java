package lcsb.mapviewer.model;

/**
 * Defines possible statuses when uploading project into system.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ProjectStatus {

  /**
   * Unknown status.
   */
  UNKNOWN("uninitialized"),

  /**
   * Data are parsed by the CellDesigner parser.
   */
  PARSING_DATA("Parsing data"),

  /**
   * Model is annotated by the annotation tool.
   */
  ANNOTATING("Annotating"),

  /**
   * Model is being uploaded to the database.
   */
  UPLOADING_TO_DB("Uploading to db"),

  /**
   * Images for the projects are being generated.
   */
  GENERATING_IMAGES("Generating images"),

  /**
   * Data that are linked to pubmed articles are being cached.
   */
  CACHING("Caching pubmed data"),

  /**
   * Project is ready to use.
   */
  DONE("Ok"),

  /**
   * There was problem during project creation.
   */
  FAIL("Failure"),

  /**
   * Data that are linked to miriam are being cached.
   */
  CACHING_MIRIAM("Caching miriam data"),

  /**
   * Miriam data validation.
   */
  VALIDATING_MIRIAM("Validating miriam data"),

  /**
   * Removing of the project.
   */
  REMOVING("Project removing"),

  /**
   * All chemicals that can be found by map elements are cached.
   */
  CACHING_CHEMICAL("Caching chemical data"),

  /**
   * All drugs that can be found by map elements are cached.
   */
  CACHING_DRUG("Caching drug data"),

  /**
   * All mirna that can be found by map elements are cached.
   */
  CACHING_MI_RNA("Caching miRNA data");

  /**
   * Message used to present the status on the client side.
   */
  private String readableString;

  /**
   * Default constructor of the enum with {@link #readableString} parameter.
   * 
   * @param readableForm
   *          {@link #readableString}
   */
  ProjectStatus(String readableForm) {
    this.readableString = readableForm;
  }

  /**
   * @return {@link #readableString}
   */
  public String toString() {
    return readableString;
  }

}
