package lcsb.mapviewer.model.security;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "type", "objectId" }))
public class Privilege implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private PrivilegeType type;

  private String objectId;

  protected Privilege() {
  }

  public Privilege(PrivilegeType type) {
    this.type = type;
  }

  public Privilege(PrivilegeType type, String objectId) {
    this.type = type;
    this.objectId = objectId;
  }

  @Override
  public int hashCode() {
    if (type == null) {
      return 0;
    }

    if (isObjectPrivilege()) {
      return Objects.hash(type, objectId);
    } else {
      return type.hashCode();
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Privilege))
      return false;
    Privilege that = (Privilege) o;
    return type == that.type
        && Objects.equals(objectId, that.objectId);
  }

  @Override
  public String toString() {
    if (isObjectPrivilege()) {
      return type.name() + ":" + objectId;
    } else {
      return type.name();
    }
  }

  public boolean isObjectPrivilege() {
    return objectId != null;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public PrivilegeType getType() {
    return type;
  }

  public void setType(PrivilegeType type) {
    this.type = type;
  }

  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

}
