package lcsb.mapviewer.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class ProjectLogEntry implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(optional = false)
  private Project project;

  @Column(nullable = false)
  private String severity;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ProjectLogEntryType type;

  private String objectIdentifier;

  private String objectClass;

  private String mapName;

  private String source;

  @Column(nullable = false)
  private String content;

  public ProjectLogEntry() {

  }

  public ProjectLogEntry(ProjectLogEntry entry) {
    this.project = entry.getProject();
    this.severity = entry.getSeverity();
    this.type = entry.getType();
    this.objectIdentifier = entry.getObjectIdentifier();
    this.objectClass = entry.getObjectClass();
    this.mapName = entry.getMapName();
    this.source = entry.getSource();
    this.content = entry.getContent();
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(String mapName) {
    this.mapName = mapName;
  }

  public String getObjectClass() {
    return objectClass;
  }

  public void setObjectClass(String objectClass) {
    this.objectClass = objectClass;
  }

  public String getObjectIdentifier() {
    return objectIdentifier;
  }

  public void setObjectIdentifier(String objectIdentifier) {
    this.objectIdentifier = objectIdentifier;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public ProjectLogEntryType getType() {
    return type;
  }

  public void setType(ProjectLogEntryType type) {
    this.type = type;
  }

  public int getId() {
    return id;
  }

}
