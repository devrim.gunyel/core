package lcsb.mapviewer.model;

import org.apache.logging.log4j.Marker;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;

public class LogMarker implements Marker {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public static final Marker IGNORE = new IgnoredLogMarker();

  private ProjectLogEntry entry;

  public LogMarker(ProjectLogEntryType type, BioEntity bioEntity) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    if (bioEntity != null) {
      entry.setObjectIdentifier(bioEntity.getElementId());
      entry.setObjectClass(bioEntity.getClass().getSimpleName());
      if (bioEntity.getModel() != null) {
        entry.setMapName(bioEntity.getModel().getName());
      }
    }
  }

  public LogMarker(LogMarker marker) {
    entry = new ProjectLogEntry(marker.getEntry());
  }

  public LogMarker(ProjectLogEntryType type, String objectClass, String objectIdentifier, String mapName) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    entry.setObjectIdentifier(objectIdentifier);
    entry.setObjectClass(objectClass);
    entry.setMapName(mapName);
  }

  @Override
  public Marker addParents(Marker... markers) {
    throw new NotImplementedException();
  }

  @Override
  public String getName() {
    throw new NotImplementedException();
  }

  @Override
  public Marker[] getParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean hasParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(Marker m) {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(String name) {
    throw new NotImplementedException();
  }

  @Override
  public boolean remove(Marker marker) {
    throw new NotImplementedException();
  }

  @Override
  public Marker setParents(Marker... markers) {
    throw new NotImplementedException();
  }

  public ProjectLogEntry getEntry() {
    return entry;
  }
}
