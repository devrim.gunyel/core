package lcsb.mapviewer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.AllModelTests;
import lcsb.mapviewer.modelutils.map.AllMapUtilTests;

@RunWith(Suite.class)
@SuiteClasses({ AllModelTests.class,
    AllMapUtilTests.class,
})
public class AllTests {

}
