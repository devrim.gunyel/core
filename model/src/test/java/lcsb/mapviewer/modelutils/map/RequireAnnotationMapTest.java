package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class RequireAnnotationMapTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (RequireAnnotationMap type : RequireAnnotationMap.values()) {
      assertNotNull(type);

      // for coverage tests
      RequireAnnotationMap.valueOf(type.toString());
    }
  }

}
