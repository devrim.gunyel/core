package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.species.*;

public class ElementUtilsTest extends ModelTestFunctions {
  Logger logger = LogManager.getLogger(ElementUtilsTest.class);

  @Before
  public void setUp() throws Exception {
    ElementUtils.setElementClasses(null);
    ElementUtils.setReactionClasses(null);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testClassTree() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();

    assertNotNull(top);

    assertTrue(top.getChildren().size() > 0);
  }

  @Test
  public void testGetTag() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    assertNotNull(elementUtils.getElementTag(Mockito.spy(new Reaction())));
  }

  @Test
  public void testGetTag2() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    GenericProtein protein = new GenericProtein("id");

    assertNotNull(elementUtils.getElementTag(protein, new Object()));
  }

  @Test
  public void testTagForNull() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    assertNotNull(elementUtils.getElementTag(null, null));
  }

  protected void print(ClassTreeNode top, int indent) {
    if (indent > 10) {
      throw new InvalidArgumentException();
    }
    String tmp = "";
    for (int i = 0; i < indent; i++)
      tmp += "  ";
    logger.debug(tmp + top.getCommonName());
    for (ClassTreeNode node : top.getChildren()) {
      print(node, indent + 1);
    }

  }

  @Test
  public void tesGetAvailableElementSubclasses() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    List<Class<? extends Element>> list = elementUtils.getAvailableElementSubclasses();
    assertTrue(list.contains(IonChannelProtein.class));
    assertFalse(list.contains(Protein.class));
  }

  @Test
  public void tesClassByName() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    assertEquals(Ion.class, elementUtils.getClassByName(Ion.class.getSimpleName()));
    assertEquals(Ion.class, elementUtils.getClassByName("Ion"));
    assertNull(elementUtils.getClassByName("unknown class name"));
  }

  @Test
  public void tesGetAvailableReactionSubclasses() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    List<Class<? extends Reaction>> list = elementUtils.getAvailableReactionSubclasses();
    assertTrue(list.contains(UnknownNegativeInfluenceReaction.class));
    assertFalse(list.contains(Reaction.class));
  }

  @Test
  public void tesClassByName2() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    assertEquals(UnknownNegativeInfluenceReaction.class, elementUtils.getClassByName("UnknownNegativeInfluence"));
  }

  @Test
  public void tesAnnotatedElementClassTree() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
    queue.add(tree);
    while (!queue.isEmpty()) {
      ClassTreeNode node = queue.poll();
      for (ClassTreeNode child : node.getChildren()) {
        queue.add(child);
      }
      assertNotNull("Data for class " + node.getClazz() + " is null", node.getData());
    }
  }

  @Test
  public void tesAnnotatedElementClassTreeSorted() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
    queue.add(tree);
    while (!queue.isEmpty()) {
      ClassTreeNode node = queue.poll();
      String lastChild = "";
      for (ClassTreeNode child : node.getChildren()) {
        queue.add(child);
        assertTrue(lastChild.compareTo(child.getCommonName()) <= 0);
        lastChild = child.getCommonName();
      }
    }
  }

}
