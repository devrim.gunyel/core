package lcsb.mapviewer;

import java.util.List;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.UnitTestFailedWatcher;

public class ModelTestFunctions {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

}
