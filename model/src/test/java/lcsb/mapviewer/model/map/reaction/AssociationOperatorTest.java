package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class AssociationOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new AssociationOperator());
  }

  @Test
  public void testConstructor() {
    AssociationOperator op = new AssociationOperator();
    op.setLine(new PolylineData());
    AssociationOperator operator = new AssociationOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertFalse("".equals(new AssociationOperator().getSBGNOperatorText()));
    assertFalse("".equals(new AssociationOperator().getOperatorText()));
  }

  @Test
  public void testCopy1() {
    AssociationOperator op = new AssociationOperator();
    op.setLine(new PolylineData());
    AssociationOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(AssociationOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
