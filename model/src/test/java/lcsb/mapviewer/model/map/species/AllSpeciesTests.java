package lcsb.mapviewer.model.map.species;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.map.species.field.AllFieldTests;

@RunWith(Suite.class)
@SuiteClasses({ AllFieldTests.class,
    AntisenseRnaComparatorTest.class,
    AntisenseRnaTest.class,
    ChemicalComparatorTest.class,
    ComplexComparatorTest.class,
    ComplexTest.class,
    DegradedComparatorTest.class,
    DegradedTest.class,
    DrugComparatorTest.class,
    DrugTest.class,
    ElementComparatorTest.class,
    ElementTest.class,
    GeneComparatorTest.class,
    GenericProteinComparatorTest.class,
    GenericProteinTest.class,
    GeneTest.class,
    IonChannelProteinTest.class,
    IonChannelProteinComparatorTest.class,
    IonComparatorTest.class,
    IonTest.class,
    PhenotypeComparatorTest.class,
    PhenotypeTest.class,
    ProteinComparatorTest.class,
    ProteinTest.class,
    ReceptorProteinTest.class,
    ReceptorProteinComparatorTest.class,
    RnaComparatorTest.class,
    RnaTest.class,
    SimpleMoleculeComparatorTest.class,
    SimpleMoleculeTest.class,
    SpeciesComparatorTest.class,
    SpeciesTest.class,
    TruncatedProteinTest.class,
    TruncatedProteinComparatorTest.class,
    UnknownComparatorTest.class,
    UnknownTest.class,
})
public class AllSpeciesTests {

}
