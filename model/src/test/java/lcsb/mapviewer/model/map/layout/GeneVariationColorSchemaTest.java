package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GeneVariationColorSchemaTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GeneVariationColorSchema());
  }

  @Test
  public void testConstructor() {
    GeneVariationColorSchema schema = new GeneVariationColorSchema();
    schema.setDescription("desc");
    GeneVariationColorSchema copy = new GeneVariationColorSchema(schema);
    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testCopy() {
    GeneVariationColorSchema schema = new GeneVariationColorSchema();
    schema.setDescription("desc");
    GeneVariationColorSchema copy = schema.copy();

    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testGetters() {
    List<GeneVariation> geneVariations = new ArrayList<>();

    GeneVariationColorSchema schema = new GeneVariationColorSchema();
    schema.setGeneVariations(geneVariations);

    assertEquals(geneVariations, schema.getGeneVariations());
  }

  @Test
  public void testAddGeneVariations() {
    List<GeneVariation> geneVariations = new ArrayList<>();
    geneVariations.add(new GeneVariation());

    GeneVariationColorSchema schema = new GeneVariationColorSchema();
    schema.addGeneVariations(geneVariations);

    assertEquals(1, schema.getGeneVariations().size());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(GeneVariationColorSchema.class).copy();
  }

}
