package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ChemicalComparatorTest extends ModelTestFunctions {

  ChemicalComparator comparator = new ChemicalComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Chemical drug1 = createChemical();
    Chemical drug2 = createChemical();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    Chemical drug2 = createChemical();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Chemical drug = createChemical();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(Chemical.class)) != 0);
  }

  public Chemical createChemical() {
    Chemical result = new Ion();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Chemical object = Mockito.mock(Chemical.class);
    comparator.compare(object, object);
  }

}
