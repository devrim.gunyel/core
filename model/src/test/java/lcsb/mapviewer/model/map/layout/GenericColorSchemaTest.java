package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GenericColorSchemaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GenericColorSchema());
  }

  @Test
  public void testConstructor() {
    GenericColorSchema schema = new GenericColorSchema();
    schema.setDescription("desc");
    GenericColorSchema copy = new GenericColorSchema(schema);
    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testCopy() {
    GenericColorSchema schema = new GenericColorSchema();
    schema.setDescription("desc");
    GenericColorSchema copy = schema.copy();

    assertEquals("desc", copy.getDescription());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(GenericColorSchema.class).copy();
  }
}
