package lcsb.mapviewer.model.map;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class MiriamTypeTest extends ModelTestFunctions {
  Logger logger = LogManager.getLogger(MiriamDataTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testIdUniqness() {
    Map<String, MiriamType> ids = new HashMap<>();
    for (MiriamType mt : MiriamType.values()) {
      assertNull(mt.getCommonName() + " doesn't have a unique key", ids.get(mt.getRegistryIdentifier()));
      ids.put(mt.getRegistryIdentifier(), mt);

      // coverage tests
      assertNotNull(MiriamType.valueOf(mt.name()));
      assertNotNull(mt.getUris());
      assertNotNull(mt.getValidClass());
      assertNotNull(mt.getRequiredClass());
      if (mt != MiriamType.UNKNOWN) {
        assertNotNull("No homepage for: " + mt, mt.getDbHomepage());
      }
    }
  }

  @Test
  public void testGetMiriamDataFromIdentifier() {
    MiriamData md = MiriamType.getMiriamDataFromIdentifier("HGNC Symbol:SNCA");
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"), md);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamDataFromInvalidIdentifier() {
    MiriamType.getMiriamDataFromIdentifier("xyz:SNCA");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamDataFromInvalidIdentifier2() {
    MiriamType.getMiriamDataFromIdentifier("xyz");
  }

  @Test
  public void testTypeByUri() {
    assertNotNull(MiriamType.getTypeByUri("urn:miriam:cas"));
    assertNull(MiriamType.getTypeByUri("xyz"));
  }

  @Test
  public void testTypeByCommonName() {
    assertNotNull(MiriamType.getTypeByCommonName("Consensus CDS"));
    assertNull(MiriamType.getTypeByCommonName("xyz"));
  }

  @Test
  public void testGetMiriamByUri1() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384:SF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test
  public void testGetMiriamForVmhMetabolite() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:vmhmetabolite:o2");
    assertNotNull(md);
  }

  @Test
  public void testGetMiriamByUri2() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384%3ASF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test
  public void testGetMiriamByUri3() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family%3APTHR19384%3ASF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamByInvalidUri() throws Exception {
    MiriamType.getMiriamByUri("invalid_uri");
  }

  @Test
  public void testGetMiriamByIncompleteUri() throws Exception {
    for (MiriamType mt : MiriamType.values()) {
      for (String uri : mt.getUris()) {
        try {
          MiriamType.getMiriamByUri(uri);
          fail("Exception expected");
        } catch (InvalidArgumentException e) {
        }
      }
    }
  }

}
