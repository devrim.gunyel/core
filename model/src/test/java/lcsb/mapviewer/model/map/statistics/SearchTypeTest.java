package lcsb.mapviewer.model.map.statistics;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class SearchTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (SearchType type : SearchType.values()) {
      assertNotNull(type);

      // for coverage tests
      SearchType.valueOf(type.toString());
    }
  }
}
