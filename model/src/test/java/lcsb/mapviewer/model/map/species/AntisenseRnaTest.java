package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

public class AntisenseRnaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new AntisenseRna());
  }

  @Test
  public void testConstructor1() {
    AntisenseRna original = new AntisenseRna();
    original.addCodingRegion(new CodingRegion());
    AntisenseRna aRna = new AntisenseRna(original);
    assertNotNull(aRna);
  }

  @Test
  public void testGetters() {
    AntisenseRna aRna = new AntisenseRna("id");
    assertNotNull(aRna.getStringType());

    List<ModificationResidue> regions = new ArrayList<>();

    aRna.setRegions(regions);

    assertEquals(regions, aRna.getRegions());
  }

  @Test
  public void testCopy() {
    AntisenseRna aRna = new AntisenseRna().copy();
    assertNotNull(aRna);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    AntisenseRna element = Mockito.spy(AntisenseRna.class);
    element.copy();
  }

}
