package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class DegradedComparatorTest extends ModelTestFunctions {

  DegradedComparator comparator = new DegradedComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Degraded degraded1 = createDegraded();
    Degraded degraded2 = createDegraded();

    assertEquals(0, comparator.compare(degraded1, degraded1));

    assertEquals(0, comparator.compare(degraded1, degraded2));
    assertEquals(0, comparator.compare(degraded2, degraded1));
  }

  @Test
  public void testDifferent() {
    Degraded degraded1 = createDegraded();
    Degraded degraded2 = createDegraded();
    degraded1 = createDegraded();
    degraded2 = createDegraded();
    degraded1.setCharge(54);
    assertTrue(comparator.compare(degraded1, degraded2) != 0);
    assertTrue(comparator.compare(degraded2, degraded1) != 0);

    degraded1 = createDegraded();
    degraded2 = createDegraded();
    assertTrue(comparator.compare(null, degraded2) != 0);
    assertTrue(comparator.compare(degraded2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Degraded degraded = createDegraded();
    degraded.setName("n");
    assertTrue(comparator.compare(degraded, degraded1) != 0);

    assertTrue(comparator.compare(degraded, Mockito.mock(Degraded.class)) != 0);
  }

  public Degraded createDegraded() {
    Degraded result = new Degraded();
    result.setCharge(12);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Degraded object = Mockito.mock(Degraded.class);

    comparator.compare(object, object);
  }
}
