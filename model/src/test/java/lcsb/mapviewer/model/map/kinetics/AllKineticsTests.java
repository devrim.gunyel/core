package lcsb.mapviewer.model.map.kinetics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SbmlFunctionTest.class,
    SbmlFunctionComparatorTest.class,
    SbmlKineticsTest.class,
    SbmlKineticsComparatorTest.class,
    SbmlParameterTest.class,
    SbmlParameterComparatorTest.class,
    SbmlUnitTest.class,
    SbmlUnitComparatorTest.class,
    SbmlUnitTypeFactorTest.class,
    SbmlUnitTypeFactorComparatorTest.class,
})
public class AllKineticsTests {

}
