package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.*;

public class TranscriptionReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new TranscriptionReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction();
    new TranscriptionReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    new TranscriptionReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction();
    reaction.addProduct(new Product());
    reaction.addReactant(new Reactant());
    TranscriptionReaction validReaction = new TranscriptionReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    TranscriptionReaction original = new TranscriptionReaction();
    original.addProduct(new Product());
    original.addReactant(new Reactant());
    original.addReactant(new Reactant());
    TranscriptionReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new TranscriptionReaction()).copy();
  }

  @Test
  public void testGetters() {
    TranscriptionReaction original = new TranscriptionReaction();
    assertNotNull(original.getReactionRect());
    assertNotNull(original.getStringType());
  }
}
