package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Rna;

public class ProteinBindingDomainTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ProteinBindingDomain());
  }

  @Test
  public void testConstructor() {
    ProteinBindingDomain region = new ProteinBindingDomain(new ProteinBindingDomain());
    assertNotNull(region);
  }

  @Test
  public void testUpdate() {
    ProteinBindingDomain region = new ProteinBindingDomain();
    ProteinBindingDomain region2 = new ProteinBindingDomain();
    region2.setName("asd");
    region2.setPosition(new Point2D.Double(0, 10));
    region.update(region2);
    assertEquals(region2.getName(), region.getName());
    assertTrue(region2.getPosition().distance(region.getPosition()) <= Configuration.EPSILON);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdate2() {
    ProteinBindingDomain region = new ProteinBindingDomain();
    region.setIdModificationResidue("1");
    ProteinBindingDomain region2 = new ProteinBindingDomain();
    region2.setIdModificationResidue("2");
    region.update(region2);
  }

  @Test
  public void testGetters() {
    ProteinBindingDomain region = new ProteinBindingDomain();
    double size = 2.5;
    int id = 58;
    Rna species = new Rna("id");
    region.setWidth(size);
    region.setSpecies(species);
    region.setId(id);
    assertEquals(id, region.getId());
    assertEquals(size, region.getWidth(), Configuration.EPSILON);
    assertEquals(species, region.getSpecies());
  }
}
