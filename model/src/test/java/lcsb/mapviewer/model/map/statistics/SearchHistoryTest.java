package lcsb.mapviewer.model.map.statistics;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class SearchHistoryTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new SearchHistory());
  }

  @Test
  public void testGetters() {
    SearchHistory sh = new SearchHistory();

    int id = 60;
    String query = "q";
    String map = "e";
    Calendar timestamp = Calendar.getInstance();
    SearchType type = SearchType.DRUG;
    String ipAddress = "3.4";

    sh.setId(id);
    sh.setQuery(query);
    sh.setMap(map);
    sh.setTimestamp(timestamp);
    sh.setType(type);
    sh.setIpAddress(ipAddress);

    assertEquals(id, sh.getId());
    assertEquals(query, sh.getQuery());
    assertEquals(map, sh.getMap());
    assertEquals(timestamp, sh.getTimestamp());
    assertEquals(type, sh.getType());
    assertEquals(ipAddress, sh.getIpAddress());
  }
}
