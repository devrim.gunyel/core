package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;

public class InvalidColorSchemaExceptionTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new InvalidColorSchemaException("x"));
  }

  @Test
  public void testConstructor1() {
    Exception e = new InvalidColorSchemaException(new Exception());
    assertNotNull(e);
  }

  @Test
  public void testConstructor2() {
    Exception e = new InvalidColorSchemaException("message", new Exception());
    assertNotNull(e);
  }

}
