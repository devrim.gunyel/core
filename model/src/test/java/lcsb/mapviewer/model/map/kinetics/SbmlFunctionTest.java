package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SbmlFunctionTest {

  @Test
  public void testConstructor() {
    String id = "function_i";
    SbmlFunction function = new SbmlFunction(id);
    assertEquals(id, function.getFunctionId());
  }

  @Test
  public void testSetName() {
    String name = "name";
    SbmlFunction function = new SbmlFunction("");
    function.setName(name);
    assertEquals(name, function.getName());
  }

  @Test
  public void testSetDefinition() {
    String definition = "<lambda><bvar><ci> x </ci></bvar></lambda>";
    SbmlFunction function = new SbmlFunction("");
    function.setDefinition(definition);
    assertEquals(definition, function.getDefinition());
  }

  @Test
  public void testGetArguments() {
    List<String> arguments = new ArrayList<>();
    arguments.add("x");

    SbmlFunction function = new SbmlFunction("");
    function.setArguments(arguments);
    assertEquals(arguments, function.getArguments());
  }

  @Test
  public void testCopy() {
    List<String> arguments = new ArrayList<>();
    arguments.add("x");

    String definition = "<lambda><bvar><ci> x </ci></bvar></lambda>";
    String name = "name";
    SbmlFunction function = new SbmlFunction("");
    function.setName(name);
    function.setDefinition(definition);
    function.setArguments(arguments);
    SbmlFunction copy = function.copy();

    SbmlFunctionComparator comparator = new SbmlFunctionComparator();
    assertEquals(0, comparator.compare(function, copy));
  }

}
