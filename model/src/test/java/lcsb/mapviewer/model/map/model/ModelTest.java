package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

public class ModelTest extends ModelTestFunctions {
  Logger logger = LogManager.getLogger(ModelTest.class);

  private Species species;

  private Complex complex;
  private Model model;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);

    species = new GenericProtein("sa1");

    complex = new Complex("sa2");

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddElement() {
    model.addElement(species);
    Element element = model.getElementByElementId(species.getElementId());
    assertNotNull(element);
    element = model.getElementByElementId(species.getElementId() + "blbla");
    assertNull(element);
  }

  @Test
  public void testGetElements() {
    Set<Element> elements = model.getElements();
    assertEquals(0, elements.size());
    model.addElement(species);
    assertEquals(1, elements.size());

    Element element = model.getElementByElementId(species.getElementId());
    assertNotNull(element);
    element = model.getElementByElementId(species.getElementId() + "blbla");
    assertNull(element);

    element = model.getElementByElementId(complex.getElementId());
    assertNull(element);

    model.addElement(complex);
    assertEquals(2, elements.size());
    element = model.getElementByElementId(complex.getElementId());
    assertNotNull(element);
  }

  @Test
  public void testAddReaction() {
    Reaction reaction = new Reaction();
    model.addReaction(reaction);
    assertNotNull(model.getReactions());
    assertEquals(1, model.getReactions().size());
  }

  @Test
  public void testAddLayer() {
    Layer layer = new Layer();
    layer.setLayerId("bla id");
    Set<Layer> list = model.getLayers();
    assertEquals(0, list.size());
    model.addLayer(layer);
    list = model.getLayers();
    assertEquals(1, list.size());
  }

  @Test
  public void testElementsByAnnotation() {
    model = createModel();

    Set<BioEntity> elements = model.getElementsByAnnotation(
        new MiriamData(MiriamRelationType.BQ_MODEL_IS, MiriamType.CHEMBL_TARGET, "CHEMBL12345"));
    assertNotNull(elements);
    assertEquals(1, elements.size());
    BioEntity element = elements.iterator().next();
    assertTrue(element instanceof Species);
    Species species = (Species) element;
    assertEquals("s5", species.getName());

    elements = model
        .getElementsByAnnotation(new MiriamData(MiriamRelationType.BQ_MODEL_IS, MiriamType.CHEBI, "CHEBI:12"));
    assertNotNull(elements);
    assertEquals(1, elements.size());
    element = elements.iterator().next();
    assertTrue(element instanceof Compartment);
    Compartment compartment = (Compartment) element;
    assertEquals("c1", compartment.getName());

    elements = model
        .getElementsByAnnotation(new MiriamData(MiriamRelationType.BQ_MODEL_IS, MiriamType.CHEBI, "CHEBI:12234"));
    assertNotNull(elements);
    assertEquals(0, elements.size());
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Species protein = new GenericProtein("sa1");
    protein.setName("s1");
    model.addElement(protein);

    Species protein2 = new GenericProtein("sa2");
    protein2.setName("s1");
    model.addElement(protein2);

    Species protein3 = new GenericProtein("sa3");
    protein3.setName("s1");
    model.addElement(protein3);

    Species protein4 = new GenericProtein("sa4");
    protein4.setName("s5");
    protein4.addMiriamData(new MiriamData(MiriamRelationType.BQ_MODEL_IS, MiriamType.CHEMBL_TARGET, "CHEMBL12345"));
    model.addElement(protein4);

    Compartment compartment = new Compartment("cca");
    compartment.setName("c1");
    compartment.addMiriamData(new MiriamData(MiriamRelationType.BQ_MODEL_IS, MiriamType.CHEBI, "CHEBI:12"));

    model.addElement(compartment);

    return model;
  }

  @Test
  public void testSorting() throws Exception {
    Model model = new ModelFullIndexed(null);

    Species species = new GenericProtein("2");
    species.setWidth(100);
    species.setHeight(100);
    model.addElement(species);

    species = new GenericProtein("3");
    species.setWidth(10);
    species.setHeight(10);
    model.addElement(species);

    species = new GenericProtein("4");
    species.setWidth(200);
    species.setHeight(100);
    model.addElement(species);

    Compartment compartment = new Compartment("5");
    compartment.setWidth(10);
    compartment.setHeight(20);
    model.addElement(compartment);

    compartment = new Compartment("6");
    compartment.setWidth(100);
    compartment.setHeight(200);
    model.addElement(compartment);

    compartment = new Compartment("7");
    compartment.setWidth(20);
    compartment.setHeight(30);
    model.addElement(compartment);

    List<Element> sortedElements = model.getElementsSortedBySize();
    if (sortedElements.size() > 0) {
      double last = (sortedElements.get(0)).getHeight() * (sortedElements.get(0)).getWidth();
      for (Element a : sortedElements) {
        assertTrue(last >= a.getHeight() * a.getWidth());
      }
    }
  }
}
