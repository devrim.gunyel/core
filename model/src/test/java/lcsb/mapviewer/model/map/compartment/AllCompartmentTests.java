package lcsb.mapviewer.model.map.compartment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BottomSquareCompartmentTest.class,
    CompartmentComparatorTest.class,
    CompartmentTest.class,
    LeftSquareCompartmentTest.class,
    OvalCompartmentTest.class,
    PathwayCompartmentTest.class,
    RightSquareCompartmentTest.class,
    SquareCompartmentTest.class,
    TopSquareCompartmentTest.class,
})
public class AllCompartmentTests {

}
