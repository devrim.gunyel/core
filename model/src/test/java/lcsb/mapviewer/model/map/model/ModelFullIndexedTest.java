package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.*;

public class ModelFullIndexedTest extends ModelTestFunctions {
  Logger logger = LogManager.getLogger(ModelFullIndexedTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    Species protein = new GenericProtein("1a");
    Species protein2 = new GenericProtein("f");
    Species protein3 = new GenericProtein("fa");

    Element compartment = new Compartment("aaa");
    Element compartment3 = new Compartment("aaa3");
    Element compartment2 = new PathwayCompartment("aaa2");
    model.addElement(protein);
    model.addElement(protein2);
    model.addElement(protein3);
    model.addElement(compartment);
    model.addElement(compartment2);
    model.addElement(compartment3);

    Reaction reaction = new TransportReaction();
    reaction.addReactant(new Reactant(protein));
    reaction.addProduct(new Product(protein2));
    model.addReaction(reaction);
    model.setProject(new Project());

    ModelData submodel = new ModelData();
    model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.DOWNSTREAM_TARGETS));
    Model submodel2 = new ModelFullIndexed(null);
    model.addSubmodelConnection(new ModelSubmodelConnection(submodel2, SubmodelType.DOWNSTREAM_TARGETS));

    ModelFullIndexed model2 = new ModelFullIndexed(model.getModelData());

    assertNotNull(model2);
  }

  @Test
  public void testGetByName() {
    String name1 = "1a";
    String name2 = "2a";
    String unknownName = "unk";
    ModelFullIndexed model = new ModelFullIndexed(null);
    Species protein = new GenericProtein("id1");
    protein.setName(name1);
    Species protein2 = new GenericProtein("id2");
    protein2.setName(name2);

    model.addElement(protein);
    model.addElement(protein2);

    List<Element> elements = model.getElementsByName(name1);
    List<Element> unkElements = model.getElementsByName(unknownName);

    assertEquals(1, elements.size());
    assertEquals(0, unkElements.size());
  }

  @Test
  public void testGetSpeciesList() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    Species protein = new GenericProtein("id1");
    Species protein2 = new GenericProtein("id2");

    model.addElement(protein);
    model.addElement(protein2);
    model.addElement(new Compartment("compId"));

    List<Species> speciesList = model.getSpeciesList();

    assertEquals(2, speciesList.size());
  }

  @Test
  public void testConstructor2() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    ModelFullIndexed model2 = new ModelFullIndexed(model.getModelData());

    assertNotNull(model2);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidElement6() {
    Element elementMock = Mockito.mock(Element.class);
    ModelFullIndexed model = new ModelFullIndexed(null);
    model.addElement(elementMock);

    new ModelFullIndexed(model.getModelData());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidElement5() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Complex complex = new Complex("1");

    model.addElement(complex);
    model.addElement(complex);
  }

  @Test
  public void testAddSpeciesWithParent() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    model.addElement(new Compartment("default"));

    Complex complex = new Complex("1");

    GenericProtein protein = new GenericProtein("asd");
    protein.setElementId("zz");
    protein.setComplex(complex);

    model.addElement(complex);

    model.addElement(protein);

    assertEquals(3, model.getElements().size());
  }

  @Test
  public void testAddSpeciesWithParentWithCompartment() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Compartment compartment = new Compartment("default");
    model.addElement(compartment);

    Complex complex = new Complex("1");
    complex.setCompartment(compartment);

    GenericProtein protein = new GenericProtein("asd");
    protein.setElementId("zz");
    protein.setComplex(complex);

    model.addElement(complex);

    model.addElement(protein);

    assertEquals(3, model.getElements().size());
  }

  @Test
  public void testGetElementByElementId() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    assertNull(model.getElementByElementId("id"));
    Species protein = new GenericProtein("asd");
    protein.setElementId("id");

    model.addElement(protein);

    assertNotNull(model.getElementByElementId("id"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddCompartment2() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Compartment compartment = new Compartment("1");

    assertEquals(0, model.getCompartments().size());
    model.addElement(compartment);
    assertEquals(1, model.getCompartments().size());
    model.addElement(compartment);
  }

  @Test
  public void testGetReactionByReactionId() {
    String reactionId = "id_r";
    ModelFullIndexed model = new ModelFullIndexed(null);
    Reaction reaction = new Reaction();
    reaction.setIdReaction(reactionId);
    assertNull(model.getReactionByReactionId(reactionId));
    model.addReaction(reaction);
    assertEquals(reaction, model.getReactionByReactionId(reactionId));
  }

  @Test
  public void testGetReactionByDbId() {
    int reactionId = 12;
    ModelFullIndexed model = new ModelFullIndexed(null);
    Reaction reaction = new Reaction();
    reaction.setId(reactionId);
    assertNull(model.getReactionByDbId(reactionId));
    model.addReaction(reaction);
    assertEquals(reaction, model.getReactionByDbId(reactionId));
  }

  @Test
  public void testGetElementByDbId() {
    int elementId = 12;
    ModelFullIndexed model = new ModelFullIndexed(null);
    Element protein = new GenericProtein("1");
    protein.setId(elementId);
    assertNull(model.getElementByDbId(elementId));
    model.addElement(protein);
    assertEquals(protein, model.getElementByDbId(elementId));
  }

  @Test
  public void testGetSortedCompartments() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    Compartment compartment = new Compartment("a1");
    compartment.setWidth(12);
    compartment.setHeight(12);
    model.addElement(compartment);
    compartment = new Compartment("a2");
    compartment.setWidth(14);
    compartment.setHeight(14);
    model.addElement(compartment);
    compartment = new Compartment("a3");
    compartment.setWidth(13);
    compartment.setHeight(13);
    model.addElement(compartment);

    List<Compartment> compartments = model.getSortedCompartments();
    assertEquals(196.0, compartments.get(0).getSize(), Configuration.EPSILON);
    assertEquals(169.0, compartments.get(1).getSize(), Configuration.EPSILON);
    assertEquals(144.0, compartments.get(2).getSize(), Configuration.EPSILON);
  }

  @Test
  public void testRemoveCompartment2() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    Compartment compartment = new Compartment("a1");
    compartment.setWidth(12);
    compartment.setHeight(12);
    model.addElement(compartment);

    compartment = new Compartment("a2");
    compartment.setWidth(14);
    compartment.setHeight(14);
    model.addElement(compartment);
    assertEquals(2, model.getElements().size());

    model.removeElement(compartment);
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testAddElements() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Compartment compartment = new Compartment("1");
    compartment.setWidth(100);
    compartment.setHeight(100);
    List<Element> elements = new ArrayList<>();
    elements.add(compartment);

    Complex complex = new Complex("ca1");
    elements.add(complex);
    complex.setWidth(10);
    complex.setHeight(10);

    Species species = new GenericProtein("a1");
    elements.add(species);
    species.setWidth(20);
    species.setHeight(20);

    assertEquals(0, model.getCompartments().size());
    model.addElements(elements);
    assertEquals(1, model.getCompartments().size());
    assertEquals(1, model.getComplexList().size());
    assertEquals(1, model.getNotComplexSpeciesList().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidElement2() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    GenericProtein protein = new GenericProtein((String) null);
    model.addElement(protein);
  }

  @Test
  public void testRemoveCompartment() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Compartment parent = new Compartment("parent_id");

    Compartment compartment = new Compartment("child_id");
    compartment.setCompartment(parent);

    model.addElement(compartment);

    assertEquals(1, model.getElements().size());

    model.removeElement(compartment);
    assertEquals(0, model.getElements().size());
  }

  @Test
  public void testRemoveComplex() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    model.addElement(new Compartment("default"));

    Complex parentComplex = new Complex("a");

    Complex childComplex = new Complex("b");
    childComplex.setComplex(parentComplex);
    childComplex.setElementId("1");

    childComplex.setCompartment(new Compartment("comp_id"));
    childComplex.setComplex(new Complex("d"));

    model.addElement(childComplex);

    assertEquals(2, model.getElements().size());

    childComplex.setComplex(new Complex("xxx"));

    model.removeElement(childComplex);
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testAddLayers() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Layer layer = new Layer();
    List<Layer> layers = new ArrayList<>();
    layers.add(layer);

    assertEquals(0, model.getLayers().size());
    model.addLayers(layers);
    assertEquals(1, model.getLayers().size());
  }

  @Test
  public void testGetElementByAnnotation() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    Reaction reaction = new Reaction();
    reaction.addMiriamData(new MiriamData(MiriamType.CAS, "C"));
    model.addReaction(reaction);

    Set<BioEntity> objects = model.getElementsByAnnotation(new MiriamData());
    assertEquals(0, objects.size());
    objects = model.getElementsByAnnotation(new MiriamData(MiriamType.CAS, "C"));
    assertTrue(objects.contains(reaction));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetNotes() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    model.setNotes("<html/>");
  }

  @Test
  public void testStubCoverageTests() {
    ModelFullIndexed model = new ModelFullIndexed(null);

    model.addElementGroup(null);
    model.addBlockDiagream(null);
  }

  @Test
  public void testGetSubmodelByName() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    ModelFullIndexed child = new ModelFullIndexed(null);
    String parentName = "PARENT M";
    String childName = "MY CHILD";
    model.setName(parentName);
    child.setName(childName);
    model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

    assertNull(model.getSubmodelByName("xyZ"));
    assertNull(model.getSubmodelByName(null));
    assertEquals(model, model.getSubmodelByName(parentName));
    assertEquals(child, model.getSubmodelByName(childName));
  }

  @Test
  public void testGetSubmodelById() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    ModelFullIndexed child = new ModelFullIndexed(null);
    Integer parentId = 73;
    Integer childId = 92;
    model.setId(parentId);
    child.setId(childId);
    model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

    assertNull(model.getSubmodelById(543543));
    assertNull(model.getSubmodelById((String) null));
    assertNull(model.getSubmodelById((Integer) null));
    assertEquals(model, model.getSubmodelById(parentId + ""));
    assertEquals(child, model.getSubmodelById(childId + ""));
  }

  @Test
  public void testGetSubmodelByConnectionName() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    ModelFullIndexed child = new ModelFullIndexed(null);
    String name = "bla";
    model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS, name));

    assertNull(model.getSubmodelByConnectionName(null));
    assertNull(model.getSubmodelByConnectionName("basdkjas"));
    assertEquals(child, model.getSubmodelByConnectionName(name));
  }

  @Test
  public void testAddReactions() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    Reaction reaction = new Reaction();
    reaction.setIdReaction("b");
    List<Reaction> reactions = new ArrayList<>();
    reactions.add(reaction);
    Reaction reaction2 = new Reaction();
    reaction2.setIdReaction("a");
    reactions.add(reaction2);

    model.addReactions(reactions);

    assertTrue(model.getReactions().contains(reaction));

    model.removeReaction(reaction2);

    assertEquals(1, model.getReactions().size());
  }

  @Test
  public void testSubmodels() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    ModelFullIndexed child = new ModelFullIndexed(null);
    model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

    assertTrue(model.getSubmodels().contains(child));
  }

  @Test
  public void testAddMiriam() {
    ModelFullIndexed model = new ModelFullIndexed(null);
    model.addMiriamData(new MiriamData());
    model.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:12345"));

    assertEquals(2, model.getMiriamData().size());
  }

  @Test
  public void testGetters() {
    Model model = new ModelFullIndexed(null);

    double width = 12.0;
    int widthInt = 12;
    String widthStr = "12.0";
    double height = 13.0;
    String heightStr = "13.0";
    int heightInt = 13;
    Set<Element> elements = new HashSet<>();
    int zoomLevels = 98;
    int tileSize = 1024;
    String idModel = "model_ID";

    model.setWidth(widthStr);
    assertEquals(width, model.getWidth(), Configuration.EPSILON);
    model.setWidth(0.0);
    model.setWidth(width);
    assertEquals(width, model.getWidth(), Configuration.EPSILON);
    model.setWidth(0.0);
    model.setWidth(widthInt);
    assertEquals(width, model.getWidth(), Configuration.EPSILON);

    model.setHeight(heightStr);
    assertEquals(height, model.getHeight(), Configuration.EPSILON);
    model.setHeight(0.0);
    model.setHeight(height);
    assertEquals(height, model.getHeight(), Configuration.EPSILON);
    model.setHeight(0.0);
    model.setHeight(heightInt);
    assertEquals(height, model.getHeight(), Configuration.EPSILON);

    model.setElements(elements);
    assertEquals(elements, model.getElements());
    model.setZoomLevels(zoomLevels);
    assertEquals(zoomLevels, model.getZoomLevels());
    model.setTileSize(tileSize);
    assertEquals(tileSize, model.getTileSize());
    model.setIdModel(idModel);
    assertEquals(idModel, model.getIdModel());

    assertNotNull(model.getParentModels());
  }

  @Test
  public void testGetAnnotatedObjects() {
    String reactionId = "id_r";
    ModelFullIndexed model = new ModelFullIndexed(null);
    Reaction reaction = new Reaction();
    reaction.setIdReaction(reactionId);
    model.addReaction(reaction);
    Species protein = new GenericProtein("2");
    model.addElement(protein);

    Collection<BioEntity> obj = model.getBioEntities();
    assertEquals(2, obj.size());
  }

}
