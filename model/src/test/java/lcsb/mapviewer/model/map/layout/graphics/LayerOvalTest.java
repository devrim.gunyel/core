package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.*;

import java.awt.*;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerOvalTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(createOval());
  }

  @Test
  public void testConstructor1() {
    LayerOval layerOval = createOval();

    LayerOval copy = new LayerOval(layerOval);

    assertNotNull(copy);
    assertEquals(0, new LayerOvalComparator().compare(layerOval, copy));
  }

  private LayerOval createOval() {
    LayerOval result = new LayerOval();
    result.setZ(14);
    return result;
  }

  @Test
  public void testCopy() {
    LayerOval copy = createOval().copy();

    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(LayerOval.class).copy();
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidX() {
    LayerOval oval = createOval();
    oval.setX("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidY() {
    LayerOval oval = createOval();
    oval.setY("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidWidth() {
    LayerOval oval = createOval();
    oval.setWidth("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidHeight() {
    LayerOval oval = createOval();
    oval.setHeight("a1.6");
  }

  @Test
  public void testGetters() {
    LayerOval oval = createOval();

    String yParam = "1.2";
    Double y = 1.2;

    String xParam = "2.2";
    Double x = 2.2;

    String widthParam = "10.2";
    Double width = 10.2;

    String heightParam = "72.2";
    Double height = 72.2;

    int id = 52;
    Color color = Color.BLACK;

    oval.setY(yParam);
    assertEquals(y, oval.getY(), Configuration.EPSILON);
    oval.setY((Double) null);
    assertNull(oval.getY());
    oval.setY(y);
    assertEquals(y, oval.getY(), Configuration.EPSILON);

    oval.setX(xParam);
    assertEquals(x, oval.getX(), Configuration.EPSILON);
    oval.setX((Double) null);
    assertNull(oval.getX());
    oval.setX(x);
    assertEquals(x, oval.getX(), Configuration.EPSILON);

    oval.setWidth(widthParam);
    assertEquals(width, oval.getWidth(), Configuration.EPSILON);
    oval.setWidth((Double) null);
    assertNull(oval.getWidth());
    oval.setWidth(width);
    assertEquals(width, oval.getWidth(), Configuration.EPSILON);

    oval.setHeight(heightParam);
    assertEquals(height, oval.getHeight(), Configuration.EPSILON);
    oval.setHeight((Double) null);
    assertNull(oval.getHeight());
    oval.setHeight(height);
    assertEquals(height, oval.getHeight(), Configuration.EPSILON);

    oval.setId(id);
    assertEquals(id, oval.getId());
    oval.setColor(color);
    assertEquals(color, oval.getColor());
  }

}
