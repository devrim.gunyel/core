package lcsb.mapviewer.model.security;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class PrivilegeTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (PrivilegeType type : PrivilegeType.values()) {
      assertNotNull(type);

      // for coverage tests
      PrivilegeType.valueOf(type.toString());
      assertNotNull(type.getDescription());
    }
  }

}
