package lcsb.mapviewer.model.security;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PrivilegeTest.class, PrivilegeTypeTest.class })
public class AllSecurityTests {

}
