package lcsb.mapviewer.model.cache;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BigFileEntryTest.class,
    CacheQueryTest.class,
    CacheTypeTest.class,
    UploadedFileEntryTest.class,

})
public class AllCacheTests {

}
