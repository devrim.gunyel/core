package lcsb.mapviewer.model.user;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.user.annotator.AllAnnotatorTests;

@RunWith(Suite.class)
@SuiteClasses({ AllAnnotatorTests.class,
    ConfigurationElementTypeTest.class,
    ConfigurationElementEditTypeTest.class,
    ConfigurationTest.class,
    UserAnnotationSchemaTest.class,
    UserClassAnnotatorsTest.class,
    UserClassRequiredAnnotationsTest.class,
    UserClassValidAnnotationsTest.class,
    UserTest.class,
})
public class AllUserTests {

}
