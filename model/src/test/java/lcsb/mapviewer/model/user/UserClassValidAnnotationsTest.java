package lcsb.mapviewer.model.user;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.MiriamType;

public class UserClassValidAnnotationsTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UserClassValidAnnotations());
  }

  @Test
  public void testConstructor() {
    UserClassValidAnnotations usva = new UserClassValidAnnotations(Object.class, new MiriamType[] { MiriamType.CAS });
    assertNotNull(usva);
  }

  @Test
  public void testGetters() {
    UserClassValidAnnotations usva = new UserClassValidAnnotations();
    List<MiriamType> validMiriamTypes = new ArrayList<>();
    UserAnnotationSchema schema = new UserAnnotationSchema();
    Integer id = 69;
    usva.setValidMiriamTypes(validMiriamTypes);
    assertEquals(validMiriamTypes, usva.getValidMiriamTypes());
    usva.setId(id);
    assertEquals(id, usva.getId());
    usva.setAnnotationSchema(schema);
    assertEquals(schema, usva.getAnnotationSchema());
  }

  @Test
  public void testAddValidMiriamType() {
    UserClassValidAnnotations usva = new UserClassValidAnnotations();
    usva.addValidMiriamType(MiriamType.CAS);
    assertTrue(usva.getValidMiriamTypes().contains(MiriamType.CAS));
  }
}
