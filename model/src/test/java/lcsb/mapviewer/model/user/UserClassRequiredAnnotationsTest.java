package lcsb.mapviewer.model.user;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.*;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.MiriamType;

public class UserClassRequiredAnnotationsTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UserClassRequiredAnnotations());
  }

  @Test
  public void testGetters() {
    UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations();
    Integer id = 24;
    List<MiriamType> requiredMiriamTypes = new ArrayList<>();
    Boolean requireAtLestOneAnnotation = true;
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

    obj.setId(id);
    obj.setRequiredMiriamTypes(requiredMiriamTypes);
    obj.setRequireAtLeastOneAnnotation(requireAtLestOneAnnotation);
    obj.setAnnotationSchema(annotationSchema);

    assertEquals(id, obj.getId());
    assertEquals(requiredMiriamTypes, obj.getRequiredMiriamTypes());
    assertEquals(requireAtLestOneAnnotation, obj.getRequireAtLeastOneAnnotation());

    assertEquals(annotationSchema, obj.getAnnotationSchema());

    obj.addRequiredMiriamType(MiriamType.CCDS);
    assertEquals(1, obj.getRequiredMiriamTypes().size());
  }

  @Test
  public void testConstructor() {
    UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class, (Collection<MiriamType>) null);
    assertFalse(obj.getRequireAtLeastOneAnnotation());
  }

  @Test
  public void testConstructor2() {
    UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class,
        new MiriamType[] { MiriamType.CAS });
    assertTrue(obj.getRequireAtLeastOneAnnotation());
  }

  @Test
  public void testConstructor3() {
    UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class, new MiriamType[] {});
    assertFalse(obj.getRequireAtLeastOneAnnotation());
  }
}
