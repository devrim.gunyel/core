package lcsb.mapviewer.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.model.cache.AllCacheTests;
import lcsb.mapviewer.model.graphics.AllGraphicsTests;
import lcsb.mapviewer.model.map.AllMapTests;
import lcsb.mapviewer.model.security.AllSecurityTests;
import lcsb.mapviewer.model.user.AllUserTests;

@RunWith(Suite.class)
@SuiteClasses({ AllCacheTests.class,
    AllGraphicsTests.class,
    AllMapTests.class,
    AllSecurityTests.class,
    AllUserTests.class,
    ProjectStatusTest.class,
    ProjectTest.class,
})
public class AllModelTests {

}
