package lcsb.mapviewer.converter;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ProjectFactoryTest extends ConverterTestFunctions {
  Logger logger = LogManager.getLogger(ProjectFactoryTest.class);

  int elementCounter = 0;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    MockConverter.modelToBeReturned = null;
  }

  @Test
  public void testOverviewImageLink() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ProjectFactory projectFactory = new ProjectFactory(converter);
    ZipFile zipFile = new ZipFile("testFiles/complex_model_with_img.zip");
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(zipFile);

    ZipEntryFileFactory factory = new ZipEntryFileFactory();
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    Project project = projectFactory.create(params);
    assertNotNull(project);
    ModelData model = project.getModels().iterator().next();
    assertEquals("main", model.getName());

    List<OverviewImage> result = project.getOverviewImages();

    assertNotNull(result);
    assertEquals(1, result.size());

    OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    OverviewLink link = img.getLinks().get(0);
    List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    OverviewModelLink mLink = (OverviewModelLink) link;
    assertEquals((Integer) 10, mLink.getxCoord());
    assertEquals((Integer) 10, mLink.getyCoord());
    assertEquals((Integer) 3, mLink.getZoomLevel());
    assertEquals(model, mLink.getLinkedModel());
  }

  @Test
  public void testOverviewImageLinkToSubmapPath() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ProjectFactory projectFactory = new ProjectFactory(converter);
    ZipFile zipFile = new ZipFile("testFiles/complex_model_with_images_path.zip");
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(zipFile);

    ZipEntryFileFactory factory = new ZipEntryFileFactory();
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    Project project = projectFactory.create(params);
    assertNotNull(project);
    ModelData model = project.getModels().iterator().next();
    assertEquals("main", model.getName());

    List<OverviewImage> result = project.getOverviewImages();

    assertNotNull(result);
    assertEquals(2, result.size());
  }

  @Test
  public void testParseGlyphs() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ProjectFactory projectFactory = new ProjectFactory(converter);

    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    ZipEntryFileFactory factory = new ZipEntryFileFactory();
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    Project project = projectFactory.create(params);
    assertNotNull(project);
    assertEquals(1, project.getGlyphs().size());
  }

  @Test
  public void testParseGlyphsAndPutThemAsElementGlyphs() throws Exception {
    Model model = new ModelFullIndexed(null);
    GenericProtein protein = createProtein();
    protein.setNotes("Glyph: glyphs/g1.png");
    model.addElement(protein);

    MockConverter.modelToBeReturned = model;

    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ProjectFactory projectFactory = new ProjectFactory(converter);

    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    ZipEntryFileFactory factory = new ZipEntryFileFactory();
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    Project project = projectFactory.create(params);
    assertNotNull(project);
    model = project.getModels().iterator().next().getModel();

    GenericProtein fetchedProtein = (GenericProtein) model.getElements().iterator().next();

    assertFalse("Glyph field should be removed from notes", fetchedProtein.getNotes().contains("Glyph"));
    assertNotNull("Glyph in the protein is not defined but should", fetchedProtein.getGlyph());
  }

  @Test
  public void testParseGlyphsAndPutThemAsTextGlyphs() throws Exception {
    Model model = new ModelFullIndexed(null);
    Layer layer = new Layer();

    LayerText text = new LayerText();
    text.setNotes("Glyph: glyphs/g1.png");
    layer.addLayerText(text);
    model.addLayer(layer);

    MockConverter.modelToBeReturned = model;

    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ProjectFactory projectFactory = new ProjectFactory(converter);

    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    ZipEntryFileFactory factory = new ZipEntryFileFactory();
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    Project project = projectFactory.create(params);
    assertNotNull(project);
    model = project.getModels().iterator().next().getModel();

    LayerText fetchedProtein = model.getLayers().iterator().next().getTexts().get(0);

    assertFalse("Glyph field should be removed from notes", fetchedProtein.getNotes().contains("Glyph"));
    assertNotNull("Glyph in the protein is not defined but should", fetchedProtein.getGlyph());
  }

  private GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + elementCounter++);
    return result;
  }

}
