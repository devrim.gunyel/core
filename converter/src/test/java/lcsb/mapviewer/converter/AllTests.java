package lcsb.mapviewer.converter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.annotation.XmlAnnotationParserTest;

@RunWith(Suite.class)
@SuiteClasses({ ComplexZipConverterParamsTest.class,
    ComplexZipConverterTest.class,
    OverviewParserTest.class,
    ProjectFactoryTest.class,
    XmlAnnotationParserTest.class,
    ZIndexPopulatorTest.class
})
public class AllTests {

}
