package lcsb.mapviewer.converter.annotation;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.*;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterTestFunctions;
import lcsb.mapviewer.model.map.*;

public class XmlAnnotationParserTest extends ConverterTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseRdf() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/rdf.xml");

    Set<MiriamData> set = xap.parse(xml);
    assertEquals(2, set.size());
  }

  @Test
  public void testMiriamDataToXmlAndBack() throws Exception {
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "e:f");
    XmlAnnotationParser parser = new XmlAnnotationParser();
    String xml = parser.miriamDataToXmlString(md);
    assertNotNull(xml);
    Set<MiriamData> set = parser.parseMiriamNode(super.getNodeFromXmlString(xml));
    assertNotNull(set);
    assertEquals(1, set.size());
    MiriamData md1 = set.iterator().next();
    assertNotNull(md1);
    assertEquals(md.getDataType(), md1.getDataType());
    assertEquals(md.getRelationType(), md1.getRelationType());
    assertEquals(md.getResource(), md1.getResource());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidRdf() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    xap.parseRdfNode((Node) null);
  }

  @Test
  public void testParseRdfWithCreatorTag() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();
    String xml = readFile("testFiles/annotation/rdf_with_creator_tag.xml");

    Set<MiriamData> set = xap.parse(xml);
    assertTrue(set.size() > 0);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidRdf3() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf2.xml");

    xap.parse(xml);
  }

  @Test
  public void testParseInvalidRdf4() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf3.xml");

    xap.parse(xml);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testParseInvalidRdf5() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf4.xml");

    xap.parse(xml);

    assertEquals(1, getWarnings().size());
  }

}
