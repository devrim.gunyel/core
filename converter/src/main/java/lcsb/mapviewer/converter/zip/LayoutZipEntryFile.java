package lcsb.mapviewer.converter.zip;

import java.io.Serializable;

/**
 * Structure used to describe a file in a zip archive with single entry about
 * {@link lcsb.mapviewer.model.map.layout.Layout Layout}.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayoutZipEntryFile extends ZipEntryFile implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name of the layout.
   */
  private String name = "";

  /**
   * Description of the layout.
   */
  private String description = "";

  /**
   * Default constructor.
   */
  public LayoutZipEntryFile() {

  }

  /**
   * Default constructor.
   * 
   * @param filename
   *          {@link ZipEntryFile#filename}
   */
  public LayoutZipEntryFile(String filename, String name, String description) {
    super(filename);
    this.name = name;
    this.description = description;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(String description) {
    this.description = description;
  }

}
