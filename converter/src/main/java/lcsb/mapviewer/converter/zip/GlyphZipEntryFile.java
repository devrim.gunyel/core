package lcsb.mapviewer.converter.zip;

import java.io.IOException;
import java.io.Serializable;

/**
 * Structure used to describe a file in a zip archive with single entry about
 * {@link lcsb.mapviewer.model.map.layout.graphics.Glyph}.
 * 
 * @author Piotr Gawron
 * 
 */
public class GlyphZipEntryFile extends ZipEntryFile implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public GlyphZipEntryFile() {

  }

  /**
   * Default constructor.
   * 
   * @param filename
   *          {@link ZipEntryFile#filename}
   * @param inputStream
   *          input stream with the data for this entry.
   * @see #baos
   * @throws IOException
   *           thrown when there is a problem with accessing input stream
   */
  public GlyphZipEntryFile(String filename) {
    super(filename);
  }

}
