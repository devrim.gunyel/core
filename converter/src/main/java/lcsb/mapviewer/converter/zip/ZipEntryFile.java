package lcsb.mapviewer.converter.zip;

import java.util.HashMap;
import java.util.Map;

/**
 * Class defining entry file for zip containing complex model (consisting of
 * many submodels and other files).
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class ZipEntryFile {
  /**
   * Name of the file in zip archive.
   */
  private String filename;

  /**
   * This map contains information that are stored in the header (first few lines)
   * in a text file. This information is stored as a pair:
   * 
   * <pre>
   * # key=value
   * </pre>
   */
  private Map<String, String> headerParameters = new HashMap<String, String>();

  /**
   * Default constructor.
   */
  public ZipEntryFile() {
  }

  /**
   * Default constructor.
   * 
   * @param filename
   *          {@link #filename}
   */
  public ZipEntryFile(String filename) {
    this.filename = filename;
  }

  /**
   * @return the filename
   * @see #filename
   */
  public String getFilename() {
    return filename;
  }

  /**
   * @param filename
   *          the filename to set
   * @see #filename
   */
  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * Adds new param (or replace old one) to {@link #headerParameters}.
   * 
   * @param key
   *          key identifing param
   * @param value
   *          value of the param
   */
  public void setHeaderParameter(String key, String value) {
    headerParameters.put(key, value);
  }

  /**
   * Returns value of the param identified by string key.
   * 
   * @param key
   *          key used to identify param
   * @return value of the param identified by string key
   */
  public String getHeaderParameter(String key) {
    return headerParameters.get(key);
  }

}
