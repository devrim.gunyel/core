package lcsb.mapviewer.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.geom.Point2D;
import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class CommentControllerIntegrationTest extends ControllerIntegrationTest {

  Logger logger = LogManager.getLogger();

  private static final String TEST_PROJECT = "test_project";
  private static final String TEST_USER_PASSWORD = "test_user_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  private static final String TEST_CURATOR_PASSWORD = "test_curator_pass";
  private static final String TEST_CURATOR_LOGIN = "test_curator";
  private static final String TEST_ADMIN_PASSWORD = "test_admin";
  private static final String TEST_ADMIN_LOGIN = "test_admin";
  private ModelData map;
  private Reaction reaction;
  private Element element;
  private Project project;

  @Autowired
  private CommentDao commentDao;

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  @Before
  public void setup() {
    project = createProject(TEST_PROJECT);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    map = project.getModels().iterator().next();
    reaction = map.getReactions().iterator().next();
    element = map.getElements().iterator().next();
  }

  @After
  public void tearDown() {
    modelService.removeModelFromCacheByProjectId(TEST_PROJECT);
  }

  @Test
  public void testAdminSeesAllComments() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    createComments();

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(3, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testListOfCommentsWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    createComments();

    RequestBuilder request = get("/projects/*/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCuratorSeesAllComments() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createComments();

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(3, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  private void createComments() {
    Comment comment = createComment(map);
    commentDao.add(comment);

    comment = createComment(map);
    comment.setPinned(true);
    commentDao.add(comment);

    comment = createComment(map);
    comment.setDeleted(true);
    commentDao.add(comment);
  }

  @Test
  public void testUserSeesOnlyPinnedComments() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    createComments();

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testUserCannotSeeAuthorDataInComment() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createComment(map);
    comment.setName("author name");
    comment.setPinned(true);
    commentDao.add(comment);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertFalse("Normal user shouldn't see author name", response.contains("author name"));
    assertFalse("Normal user shouldn't see author email", response.contains("email"));
    assertFalse("Normal user shouldn't see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testAdminCanSeeAuthorDataInComment() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map);
    comment.setName("author name");
    comment.setPinned(true);
    commentDao.add(comment);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertTrue("Admin should see author name", response.contains("author name"));
    assertTrue("Admin should see author email", response.contains("email"));
    assertTrue("Admin should see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testCuratorCanSeeAuthorDataInComment() throws Exception {
    createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createComment(map);
    comment.setName("author name");
    comment.setPinned(true);
    commentDao.add(comment);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertTrue("Curator should see author name", response.contains("author name"));
    assertTrue("Curator should see author email", response.contains("email"));
    assertTrue("Curator should see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testUserWithoutProjectAccessGetsForbidden() throws Exception {
    createCurator(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    createComments();

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/comments/models/" + map.getIdModel() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testRemoveAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(true, comment.isDeleted());
  }

  @Test
  public void testRemoveAsCurator() throws Exception {
    createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(true, comment.isDeleted());
  }

  @Test
  public void testRemoveAsCuratorWithoutAccess() throws Exception {
    createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testRemoveAsOwner() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createComment(map, user);
    commentDao.add(comment);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(true, comment.isDeleted());
  }

  @Test
  public void testRemoveAsGuestAccount() throws Exception {
    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetReactionCommentsAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createReactionComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetReactionCommentsWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createReactionComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/*/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionCommentsAsCurator() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createReactionComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetReactionCommentsWithoutAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createReactionComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetElementCommentsAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createElementComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetElementCommentsWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createElementComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/*/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementCommentsAsCurator() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createElementComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetElementCommentsWithoutAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createElementComment();
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetPointCommentsAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10.00,20.00")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetPointCommentsWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/*/comments/models/" + map.getId() + "/points/10.00,20.00")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetPointCommentsAsCurator() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, map.getProject().getProjectId());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10.00,20.00")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetPointCommentsWithoutAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createComment(map);
    commentDao.add(comment);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10.00,20.00")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testAddElementCommentAsUserWithAccess() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());

    Comment comment = commentDao.getCommentByModel(map, null, null).get(0);
    assertEquals("Owner of the comment wasn't set properly", user, comment.getUser());
  }

  @Test
  public void testAddElementCommentAsAnonymous() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    userService.grantUserPrivilege(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN),
        PrivilegeType.READ_PROJECT, TEST_PROJECT);

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());

    Comment comment = commentDao.getCommentByModel(map, null, null).get(0);
    assertNull("Owner of the comment wasn't set properly", comment.getUser());
  }

  @Test
  public void testAddElementCommentWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/*/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testAddElementCommentWithoutAllNecessaryData() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testAddElementCommentAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/elements/" + element.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());
  }

  @Test
  public void testAddReactionCommentAsUserWithAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());
  }

  @Test
  public void testAddReactionCommentAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());
  }

  @Test
  public void testAddReactionCommentWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/*/comments/models/" + map.getId() + "/bioEntities/reactions/" + reaction.getId())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testAddPointCommentAsUserWithAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, map.getProject());

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10,2")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());
  }

  @Test
  public void testAddPointCommentAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10,2")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, commentDao.getCommentByModel(map, null, null).size());
  }

  @Test
  public void testAddPointCommentWithUndefinedProject() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "test_user"),
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "tes content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId().toString()))));

    RequestBuilder request = post(
        "/projects/*/comments/models/" + map.getId() + "/points/10,2")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .content(body)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  private Comment createReactionComment() {
    Comment comment = createComment(map);
    comment.setTableName(reaction.getClass());
    comment.setTableId(reaction.getId());
    return comment;
  }

  private Comment createElementComment() {
    Comment comment = createComment(map);
    comment.setTableName(element.getClass());
    comment.setTableId(element.getId());
    return comment;
  }
}
