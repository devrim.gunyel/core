package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.common.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class UserControllerAnonymousIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_ADMIN_PASSWORD = "test_admin";
  private static final String TEST_ADMIN_LOGIN = "test_admin";
  Logger logger = LogManager.getLogger();

  @Before
  public void setup() {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
  }

  @Test
  public void grantPrivilegeForAnonymousUser() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"IS_ADMIN\":true}}";

    RequestBuilder request = patch("/users/" + Configuration.ANONYMOUS_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    RequestBuilder anonymousRequest = get("/users/");
    mockMvc.perform(anonymousRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void loginAsAnonymousUser() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"user\":{\"password\":\"test\"}}";

    RequestBuilder request = patch("/users/" + Configuration.ANONYMOUS_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    RequestBuilder loginRequest = post("/api/doLogin")
        .param("login", Configuration.ANONYMOUS_LOGIN)
        .param("password", "test");
    mockMvc.perform(loginRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void removeAnonymousUser() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/users/" + Configuration.ANONYMOUS_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void checkIsAuthenticatedAdmin() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/users/isSessionValid")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String login = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("login")
        .getAsString();
    assertEquals(TEST_ADMIN_LOGIN, login);
  }

  @Test
  public void checkIsAuthenticatedAnonymous() throws Exception {
    RequestBuilder request = get("/users/isSessionValid")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

}
