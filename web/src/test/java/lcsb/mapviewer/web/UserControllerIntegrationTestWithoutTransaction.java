package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Rollback
public class UserControllerIntegrationTestWithoutTransaction extends ControllerIntegrationTest {

  private static final String TEST_USER_PASSWORD = "test_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  private static final String ADMIN_PASSWORD = "admin";
  private static final String ADMIN_LOGIN = "admin";
  Logger logger = LogManager.getLogger();
  @Autowired
  private IUserService userService;

  @Before
  public void setup() {
  }

  @Test
  public void createUser() throws Exception {
    try {
      MockHttpSession session = createSession(ADMIN_LOGIN, ADMIN_PASSWORD);

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("login", TEST_USER_LOGIN),
          new BasicNameValuePair("password", TEST_USER_PASSWORD),
          new BasicNameValuePair("name", ""),
          new BasicNameValuePair("surname", ""),
          new BasicNameValuePair("email", ""))));
      RequestBuilder request = post("/users/" + TEST_USER_LOGIN)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());
    } finally {
      removeUserInSeparateThread(userService.getUserByLogin(TEST_USER_LOGIN));
    }
  }
}
