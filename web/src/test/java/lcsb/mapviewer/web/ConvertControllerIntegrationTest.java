package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConvertControllerIntegrationTest extends ControllerIntegrationTest {

  @Test
  public void testConvertInvalidFile() throws Exception {
    String body = "invalid content";
    RequestBuilder request = post("/convert/image/SBGN-ML:svg")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testConvertInvalidMapFormat() throws Exception {
    String body = "invalid content";
    RequestBuilder request = post("/convert/image/unknown:svg")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

}
