package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class OverlayControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "test_project";
  private static final String TEST_USER_PASSWORD = "test_user_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  private static final String TEST_CURATOR_PASSWORD = "test_curator_pass";
  private static final String TEST_CURATOR_LOGIN = "test_curator";
  Logger logger = LogManager.getLogger();
  @Autowired
  private LayoutDao layoutDao;

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  private Project project;
  private ModelData map;
  private Reaction reaction;
  private Element element;

  @Before
  public void setup() {
    project = createProject(TEST_PROJECT);
    map = project.getModels().iterator().next();
    reaction = map.getReactions().iterator().next();
    element = map.getElements().iterator().next();
  }

  @After
  public void tearDown() {
    modelService.removeModelFromCacheByProjectId(TEST_PROJECT);
  }

  @Test
  public void testListOverlaysByCreator() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    createOverlay(project, user);

    createOverlay(project, curator);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/?creator=" + TEST_CURATOR_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("Curator has only one overlay", 1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testListPublicOverlaysOverlaysWhenCreatorEmpty() throws Exception {
    Layout layout = createOverlay(project, null);
    layout.setPublicLayout(true);
    layoutDao.add(layout);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/?publicOverlay=true&creator=xxx")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("There are no public overlays created by user xxx", 0, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testListNonPublicOverlaysOverlaysWhenCreatorEmpty() throws Exception {
    Layout layout = createOverlay(project, null);
    layout.setPublicLayout(true);
    layoutDao.add(layout);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/?publicOverlay=false&creator=" + BUILT_IN_TEST_ADMIN_LOGIN)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("There are no private overlays created by user xxx", 0, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testListOverlaysByPublicFlag() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout layout = createOverlay(project, null);
    layout.setPublicLayout(true);
    layoutDao.add(layout);

    Layout curatorLayout = createOverlay(project, curator);
    curatorLayout.setPublicLayout(false);
    layoutDao.add(curatorLayout);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/?publicOverlay=true")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("There is only one public overlay", 1, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testCuratorShouldSeeAllOverlays() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout layout = createOverlay(project, null);
    layout.setPublicLayout(true);
    layoutDao.add(layout);

    Layout userLayout = createOverlay(project, user);
    userLayout.setPublicLayout(false);
    layoutDao.add(userLayout);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("Curator should see all overlays", 2, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testUserShouldSeeAllPublicOverlaysAndOverlays() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout layout = createOverlay(project, null);
    layout.setPublicLayout(true);
    layoutDao.add(layout);

    Layout hiddenLayout = createOverlay(project, null);
    hiddenLayout.setPublicLayout(false);
    layoutDao.add(hiddenLayout);

    Layout userLayout = createOverlay(project, user);
    userLayout.setPublicLayout(false);
    layoutDao.add(userLayout);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals("User should see his own overlays and public ones", 2, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testNoPrivilegesShouldResultInForbidden() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testAdminCanAccessUserOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(project, user);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanAccessUserOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    Layout overlay = createOverlay(project, user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessHisOwnOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(project, user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCannotAccessAnotherUserOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(project, admin);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetBioEntitiesForOverlay() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId() + "/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int count = map.getElements().size() + map.getReactions().size();
    assertEquals("All bioEntities should be returned", count, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetBioEntitiesForOverlayAsUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId() + "/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int count = map.getElements().size() + map.getReactions().size();
    assertEquals("All bioEntities should be returned", count, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetBioEntitiesForOverlayAsCurator() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId() + "/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int count = map.getElements().size() + map.getReactions().size();
    assertEquals("All bioEntities should be returned", count, new JsonParser()
        .parse(response)
        .getAsJsonArray().size());
  }

  @Test
  public void testGetReactionsForOverlay() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/reactions/" + reaction.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetReactionsForOverlayAsUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/reactions/" + reaction.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetReactionsForOverlayAsCurator() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/reactions/" + reaction.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetElementsForOverlay() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/elements/" + element.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetElementsForOverlayAsUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/elements/" + element.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetElementsForOverlayAsCurator() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/elements/" + element.getId() + "/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testAdminCreateOverlay() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testCuratorCreateOverlay() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", curator);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testUserCreateOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    userService.grantUserPrivilege(user, PrivilegeType.CAN_CREATE_OVERLAYS);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testUserWithoutCreateOverlayAccessCannotCreateOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUserWithoutAccessToProjectCannotCreateOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.CAN_CREATE_OVERLAYS);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testAdminCanRemoveOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testCuratorCanRemoveOverlay() throws Exception {
    createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testUserCanRemoveOwnOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, layoutDao.getLayoutsByProject(project).size());
  }

  @Test
  public void testUserCannotRemoveOtherUserOverlays() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createUser(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);

    Layout overlay = createOverlay(curator);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testAdminCanUpdateOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);

    String body = "{\"overlay\":{}}";

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanUpdateOverlay() throws Exception {
    createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);
    String body = "{\"overlay\":{}}";

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanUpdateOwnOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    String body = "{\"overlay\":{}}";

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCannotUpdateOtherUserOverlays() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createUser(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);

    Layout overlay = createOverlay(curator);
    String body = "{\"overlay\":{}}";

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testAdminCanAccessOverlaySource() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanAccessOverlaySource() throws Exception {
    User curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessOwnOverlaySource() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCannotAccessOtherUserOverlaysSource() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    User curator = createUser(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);

    Layout overlay = createOverlay(curator);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testOverlayCreatedAsFirstShouldHaveProperOrderIndex() throws Exception {
    User user = createAdmin(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    createOverlay(user);

    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", admin);
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int orderNumber = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("order")
        .getAsInt();

    assertEquals("First user data overlay should be ordered with 1", 1, orderNumber);
  }

  @Test
  public void testUserCanAccessPublicOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(project, null);
    overlay.setPublicLayout(true);
    layoutDao.add(overlay);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessDataInPublicOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(null);
    overlay.setPublicLayout(true);
    layoutDao.update(overlay);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/*/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessReactionDataInPublicOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(null);
    overlay.setPublicLayout(true);
    layoutDao.update(overlay);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/reactions/-1/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUserCanAccessElementDataInPublicOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(null);
    overlay.setPublicLayout(true);
    layoutDao.update(overlay);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/models/" + map.getId()
            + "/bioEntities/elements/-1/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUserCanDownloadSourceOfPublicOverlay() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);

    Layout overlay = createOverlay(null);
    overlay.setPublicLayout(true);
    layoutDao.update(overlay);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDeprecatedOverlayInformation() throws Exception {
    Layout overlay = createOverlay(null,
        "elementIdentifier\t" + ColorSchemaColumn.REACTION_IDENTIFIER + "\tvalue\n\t\t-1");
    overlay.setPublicLayout(true);
    layoutDao.update(overlay);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int deprecatedColumns = new JsonParser()
        .parse(response)
        .getAsJsonObject().get("deprecatedColumns").getAsJsonArray().size();

    assertEquals(2, deprecatedColumns);
  }

  private Layout createOverlay(User user, String string) {
    return createOverlay(project, user, string);
  }

  private Layout createOverlay(User user) {
    return createOverlay(project, user);
  }

  @Test
  public void testListOverlaysWithUnknownProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetOverlayByIdWithUnknownProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/overlays/" + overlay.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetBioEntitiesForOverlayWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/*/overlays/" + overlay.getId() + "/models/" + map.getId() + "/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionsForOverlayWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/overlays/" + overlay.getId() + "/models/" + map.getId()
        + "/bioEntities/reactions/" + reaction.getId() + "/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementsForOverlayWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/overlays/" + overlay.getId() + "/models/" + map.getId()
        + "/bioEntities/elements/" + element.getId() + "/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCreateOverlayWithUndefinedProjectId() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/*/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveOverlayWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/projects/*/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateOverlayWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    String body = "{\"overlay\":{}}";

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/projects/*/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testAccessOverlaySourceWithUndefinedProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay = createOverlay(admin);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/overlays/" + overlay.getId() + ":downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testOrderAfterRemoveOverlay() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Layout overlay1 = createOverlay(user);
    overlay1.setOrderIndex(1);
    Layout overlay2 = createOverlay(user);
    overlay2.setOrderIndex(2);
    Layout overlay3 = createOverlay(user);
    overlay3.setOrderIndex(3);

    layoutDao.update(overlay1);
    layoutDao.update(overlay2);
    layoutDao.update(overlay3);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay2.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, overlay1.getOrderIndex());
    assertEquals(2, overlay3.getOrderIndex());
  }

  @Test
  public void testOrderAfterRemoveOverlayForPublicOverlay() throws Exception {
    Layout overlay1 = createOverlay(null);
    overlay1.setOrderIndex(1);
    Layout overlay2 = createOverlay(null);
    overlay2.setOrderIndex(2);
    Layout overlay3 = createOverlay(null);
    overlay3.setOrderIndex(3);

    layoutDao.update(overlay1);
    layoutDao.update(overlay2);
    layoutDao.update(overlay3);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/projects/" + TEST_PROJECT + "/overlays/" + overlay2.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, overlay1.getOrderIndex());
    assertEquals(2, overlay3.getOrderIndex());
  }

  @Test
  public void testChangeOrderAfterChaningOwner() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    Layout overlay1 = createOverlay(user);
    overlay1.setOrderIndex(1);
    Layout overlay2 = createOverlay(user);
    overlay2.setOrderIndex(2);

    Layout overlay3 = createOverlay(admin);
    overlay3.setOrderIndex(1);
    Layout overlay4 = createOverlay(admin);
    overlay4.setOrderIndex(2);

    String body = "{\"overlay\":{\"creator\":\"" + admin.getLogin() + "\"}}";

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/overlays/" + overlay1.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("Order of overlays wasn't updated for original owner of overlay", 1, overlay2.getOrderIndex());

    assertTrue(overlay1.getOrderIndex() == 1 || overlay3.getOrderIndex() == 1);
    assertTrue(overlay1.getOrderIndex() == 2 || overlay3.getOrderIndex() == 2);
    assertEquals(3, overlay4.getOrderIndex());
  }

  @Test
  public void testCreateOverlayWithoutCreateDataOverlayAccess() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, project.getProjectId());

    UploadedFileEntry file = createFile("elementIdentifier\tvalue\n\t-1", user);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("googleLicenseConsent", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    RequestBuilder request = post("/projects/"+TEST_PROJECT+"/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }


}
