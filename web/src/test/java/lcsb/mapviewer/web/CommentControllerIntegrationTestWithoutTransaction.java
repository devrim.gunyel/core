package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommentControllerIntegrationTestWithoutTransaction extends ControllerIntegrationTest {

  Logger logger = LogManager.getLogger();

  @Autowired
  private IUserService userService;
  
  @Before
  public void setup() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testRemoveNonExistingCommentOnNonExistingMap() throws Exception {
    RequestBuilder request = delete("/projects/*/comments/-1/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void testRemoveCommentAsUserWithAccess() throws Exception {
    User user = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);

    Comment comment = createCommentInSeparateThread(getBuildInModel(), user);

    RequestBuilder request = delete("/projects/" + BUILT_IN_PROJECT + "/comments/" + comment.getId() + "/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

}
