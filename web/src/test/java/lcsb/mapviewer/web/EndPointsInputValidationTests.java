package lcsb.mapviewer.web;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.net.URI;
import java.util.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@RunWith(SpringJUnit4ClassRunner.class)
public class EndPointsInputValidationTests extends ControllerIntegrationTest {

  private static String[] testValues = { " ", "-1", "0", "empty", "admin", "1.00,2.00", "17.00", "1" };
  private static List<HttpStatus> validResponses = Arrays.asList(HttpStatus.OK, HttpStatus.BAD_REQUEST,
      HttpStatus.NOT_FOUND, HttpStatus.FORBIDDEN);
  @Autowired
  public RequestMappingHandlerMapping requestMappingHandlerMapping;
  Logger logger = LogManager.getLogger();
  @Autowired
  ServletContext container;

  @Before
  public void setUp() throws Exception {
    Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.FATAL);
  }

  @After
  public void tearDown() throws Exception {
    String file = "src/test/resources/log4j2.properties";
    LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) org.apache.logging.log4j.LogManager
        .getContext(false);
    context.setConfigLocation(new URI(file));
  }

  @Test
  public void testResponseStatusCodeFromEndpoints() throws Exception {
    for (RequestMappingInfo t : requestMappingHandlerMapping.getHandlerMethods().keySet()) {
      for (String url : t.getPatternsCondition().getPatterns()) {
        for (RequestMethod method : t.getMethodsCondition().getMethods()) {
          testUrl(url, method);
        }
      }
    }
  }

  private void testUrl(String url, RequestMethod method) throws Exception {
    for (String urlWithData : getAllPossibleUrls(url)) {
      RequestBuilder request = null;
      switch (method) {
      case GET:
        request = get(urlWithData);
        break;
      case PATCH:
        request = patch(urlWithData).content("XX=YY");
        break;
      case POST:
        request = post(urlWithData).content("XX=YY");
        break;
      case DELETE:
        request = delete(urlWithData);
        break;
      default:
        fail(method.toString());
      }
      HttpServletResponse response = mockMvc.perform(request).andReturn().getResponse();

      HttpStatus status = HttpStatus.valueOf(response.getStatus());
      assertTrue("[" + method + " \"" + urlWithData + "\"]\tInvalid response: " + status.name(),
          validResponses.contains(status));
    }

  }

  private List<String> getAllPossibleUrls(String url) {
    List<String> result = new ArrayList<>();
    Set<String> parameters = getParameters(url);
    if (parameters.size() == 0) {
      result.add(url);
    } else {
      String parameter = "{" + parameters.iterator().next() + "}";
      for (String s : testValues) {
        result.addAll(getAllPossibleUrls(url.replace(parameter, s)));
      }
    }
    return result;
  }

  private Set<String> getParameters(String url) {
    Set<String> result = new HashSet<>();
    int pos = url.indexOf("{");
    while (pos > 0) {
      int endPos = url.indexOf("}", pos);
      result.add(url.substring(pos + 1, endPos));
      pos = url.indexOf("{", pos + 1);
    }
    return result;
  }

}
