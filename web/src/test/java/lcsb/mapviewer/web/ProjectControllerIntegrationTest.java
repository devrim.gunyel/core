package lcsb.mapviewer.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class ProjectControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "test_project";
  private static final String CURATOR_PASSWORD = "test_pass";
  private static final String CURATOR_LOGIN = "test_user";
  Logger logger = LogManager.getLogger();
  @Autowired
  private IUserService userService;

  @Autowired
  private ProjectDao projectDao;

  private User curator;

  @Before
  public void setup() {
    curator = createCurator(CURATOR_LOGIN, CURATOR_PASSWORD);
  }

  @Test
  public void testGetAllProjectsAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createProject(TEST_PROJECT);

    RequestBuilder request = get("/projects/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int projects = new JsonParser()
        .parse(response)
        .getAsJsonArray().size();

    assertEquals("admin should see all projects", projectDao.getCount(), projects);
  }

  @Test
  public void testGetLimitedProjectsAsCurator() throws Exception {
    Project project = createProject(TEST_PROJECT);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());
    Project project2 = new Project("test_project2");
    project2.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project2);

    MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    RequestBuilder request = get("/projects/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int projects = new JsonParser()
        .parse(response)
        .getAsJsonArray().size();

    assertEquals("curator should see only limited list of projects", 1, projects);
  }

  @Test
  public void testUserPrivilegesChangeDuringActiveSession() throws Exception {
    MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    Project project = new Project(TEST_PROJECT);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetLogsForProject() throws Exception {
    Project project = createProject(TEST_PROJECT);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/logs/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGrantPrivilege() throws Exception {
    createProject(TEST_PROJECT);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + ":grantPrivileges")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertTrue(curator.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void testRevokePrivilege() throws Exception {
    Project project = createProject(TEST_PROJECT);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + ":revokePrivileges")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertFalse(curator.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void testGetNonExistingProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createProject(TEST_PROJECT);

    String content = "{\"project\":{\"version\":\"xxx\"}}";

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("xxx", project.getVersion());

  }

  @Test
  public void testUpdateProjectWithTooLongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createProject(TEST_PROJECT);

    String content = "{\"project\":{\"version\":\"12345678901234567890123456\"}}";

    RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateProjectWithUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String content = "{\"project\":{\"version\":\"xxx\"}}";

    RequestBuilder request = patch("/projects/*/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGrantPrivilegeForUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    RequestBuilder request = patch("/projects/*:grantPrivileges")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRevokePrivilegeForUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    RequestBuilder request = patch("/projects/*:revokePrivileges")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveProjectForUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/projects/*/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetStatisticsForUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/statistics")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadSourceForUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*:downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLogsForUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/logs/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetSubmapConnectionsForUndefinedProjectId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/submapConnections/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test(timeout = 10000)
  public void addProjectWithInvalidFileId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf("")),
        new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    RequestBuilder request = post("/projects/projectId")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request).andExpect(status().is4xxClientError());
  }

}
