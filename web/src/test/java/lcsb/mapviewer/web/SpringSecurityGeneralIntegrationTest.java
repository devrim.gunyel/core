package lcsb.mapviewer.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.web.config.SpringWebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
@WebAppConfiguration
@ContextConfiguration(classes = SpringWebConfig.class)
public class SpringSecurityGeneralIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_USER_PASSWORD = "test_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  static Logger logger = LogManager.getLogger(SpringSecurityGeneralIntegrationTest.class);

  @Autowired
  IConfigurationService configurationService;

  @Before
  public void setup() {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @Test
  public void login2xxWithValidCredentials() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void loginReturnsJsonWithAuthToken() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);

    mockMvc.perform(request).andExpect((mvcResult) -> {
      assertTrue(mvcResult.getResponse().getContentAsString().contains("token"));
      assertTrue(mvcResult.getResponse().getContentAsString().contains("Login successful"));
    });
  }

  @Test
  public void loginCreateLogEntries() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);

    mockMvc.perform(request);
    assertEquals("User test_user successfully logged in using LOCAL authentication method",
        getDebugLogs().get(0).getMessage().getFormattedMessage());
  }

  @Test
  public void login4xxWithInvalidCredentials() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", "test_foo");
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void login4xxWithNonexistingPassword() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN);
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void login4xxWithNonexistingLogin() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("password", "test_foo");
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void logout2xxWithValidSession() throws Exception {
    RequestBuilder request = post("/api/doLogout")
        .session(createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD));
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void logoutReturnsJson() throws Exception {
    RequestBuilder request = post("/api/doLogout")
        .session(createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD));
    mockMvc.perform(request).andExpect((mvcResult) -> {
      assertTrue(mvcResult.getResponse().getContentAsString().contains("OK"));
    });
  }

  @Test
  public void logout4xxWithInvalidSession() throws Exception {
    RequestBuilder request = post("/api/doLogout");
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testAllowCacheForNonApiRequest() throws Exception {
    RequestBuilder request = get("index.xhtml");
    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is4xxClientError())
        .andReturn().getResponse();
    assertFalse(response.getHeaderNames().contains("Pragma"));
    assertFalse(response.getHeaderNames().contains("Vary"));

  }

  @Test
  public void testAllowCacheForRootReuqest() throws Exception {
    RequestBuilder request = get("/");
    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    assertFalse(response.getHeaderNames().contains("Pragma"));
    assertFalse(response.getHeaderNames().contains("Vary"));
  }

  @Test
  public void testXFrameFilter() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN, "https://minerva.uni.lu");
    RequestBuilder request = get("/asd");
    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is4xxClientError())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Content-Security-Policy"));
  }

  @Test
  public void testXFrameFilterDisabled() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN, "");
    RequestBuilder request = get("/asd");
    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is4xxClientError())
        .andReturn().getResponse();
    assertFalse(response.getHeaderNames().contains("Content-Security-Policy"));
  }

  @Test
  public void testDisableCacheForApiRequest() throws Exception {
    RequestBuilder request = get("/configuration/");
    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Pragma"));
    assertTrue(response.getHeaderNames().contains("Vary"));

  }

  @Test
  public void checkSessionLength() throws Exception {
    HttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    assertTrue("Max session length  is not defined", session.getMaxInactiveInterval() > 0);
  }

  @Test
  public void loginValidJsonResponse() throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);
    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String login = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("login")
        .getAsString();
    assertEquals(TEST_USER_LOGIN, login);
  }

}
