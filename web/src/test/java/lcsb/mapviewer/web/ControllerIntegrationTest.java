package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.Filter;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.layout.ColorSchemaType;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.persist.dao.map.*;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.config.SpringWebConfig;

@WebAppConfiguration
@ContextConfiguration(classes = SpringWebConfig.class)
abstract public class ControllerIntegrationTest {

  protected static final String BUILT_IN_TEST_ADMIN_PASSWORD = "admin";
  protected static final String BUILT_IN_TEST_ADMIN_LOGIN = "admin";

  protected static final String BUILT_IN_PROJECT = "empty";

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  protected MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private Filter springSecurityFilterChain;
  @Autowired
  private IUserService userService;
  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private CommentDao commentDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UploadedFileEntryDao fileDao;

  @Autowired
  private LayoutDao layoutDao;

  @Autowired
  private DbUtils dbUtils;
  private MinervaLoggerAppender appender;

  private ExecutorService executorService;

  @PostConstruct
  public void construct() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .addFilter(springSecurityFilterChain)
        .build();
    executorService = Executors.newFixedThreadPool(1);
  }

  @PreDestroy
  public void tearDown() throws Exception {
    executorService.shutdown();
  }

  /**
   * This method can be used to work around the fact that MockMvc cannot retrieve
   * cookies from Spring Security. The Reason for that is that MockMvc calls the
   * controller directly and (partially?) bypasses Spring.
   *
   * This method creates a mocked session that can be used in a request as opposed
   * to the token.
   *
   * FIXME: Find a better solution, that does not violate the spirit of
   * integration tests.
   */
  protected MockHttpSession createSession(String login, String password) throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", login)
        .param("password", password);
    return (MockHttpSession) mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn()
        .getRequest()
        .getSession();
  }

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarningLogs() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrorLogs() {
    return appender.getErrors();
  }

  protected List<LogEvent> getInfoLogs() {
    return appender.getInfos();
  }

  protected List<LogEvent> getDebugLogs() {
    return appender.getDebugs();
  }

  protected User createUser(String login, String password) {
    User user = new User();
    user.setLogin(login);
    user.setCryptedPassword(passwordEncoder.encode(password));
    userService.addUser(user);
    return user;
  }

  protected User createUser(String login, String password, Project project) {
    User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId());
    return user;
  }

  protected User createAdmin(String login, String password) {
    User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    return user;
  }

  protected User createCurator(String login, String password) {
    User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.IS_CURATOR);
    return user;
  }

  protected User createCurator(String login, String password, Project project) {
    User user = createCurator(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId());
    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, project.getProjectId());
    return user;
  }

  protected Project createProject(String projectId) {
    Project project = new Project(projectId);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    ModelData map = new ModelData();
    map.setTileSize(256);
    map.setWidth(100);
    map.setHeight(100);

    Reaction reaction = new TransportReaction();
    reaction.setZ(2);
    reaction.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 0)));
    map.addReaction(reaction);
    Element element = new GenericProtein("p1");
    element.setWidth(100.0);
    element.setHeight(20.0);
    element.setX(10);
    element.setX(20);
    element.setZ(2);
    map.addElement(element);

    Reactant reactant = new Reactant(element);
    reactant.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant);

    Product product = new Product(element);
    product.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(20, 0)));
    reaction.addProduct(product);

    project.addModel(map);
    projectDao.add(project);
    return project;
  }

  protected UploadedFileEntry createFile(String content, User user) {
    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent(content.getBytes());
    file.setOriginalFileName("test_file");
    file.setLength(content.getBytes().length);
    file.setOwner(user);
    fileDao.add(file);
    return file;
  }

  /**
   * Executes lambda function in new thread with separate hibernate session. The
   * function returns type <T> which can be void.
   * 
   * @param fun
   *          lambda to be executed in separate thread
   * @return
   * @throws Exception
   */
  protected <T> T callInSeparateThread(Supplier<T> fun) throws Exception {
    List<Future<T>> elements = executorService.invokeAll(Arrays.asList(new Callable<T>() {
      @Override
      public T call() throws Exception {
        try {
          dbUtils.createSessionForCurrentThread();
          return fun.get();
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }
    }));
    return elements.get(0).get();
  }

  protected void removeUserInSeparateThread(User user) throws Exception {
    callInSeparateThread(() -> {
      userDao.delete(userDao.getById(user.getId()));
      return null;
    });
  }

  protected void removeFileInSeparateThread(UploadedFileEntry fileEntry) throws Exception {
    callInSeparateThread(() -> {
      fileDao.delete(fileDao.getById(fileEntry.getId()));
      return null;
    });
  }

  protected UploadedFileEntry createFileInSeparateThread(String content, User user) throws Exception {
    return callInSeparateThread(() -> {
      return createFile(content, user);
    });
  }

  protected void waitForProjectToFinishLoading(String projectId) throws InterruptedException {
    Project project;
    do {
      project = projectDao.getProjectByProjectId(projectId);
      projectDao.evict(project);
      Thread.sleep(100);
    } while (project.getStatus() != ProjectStatus.DONE);
  }

  public void removeProjectInSeparateThread(Project project) throws Exception {
    removeProjectInSeparateThread(project.getProjectId());
  }

  public void removeProjectInSeparateThread(String projectId) throws Exception {
    callInSeparateThread(() -> {
      Project project = projectDao.getProjectByProjectId(projectId);
      if (project != null) {
        projectDao.delete(project);
      }
      return null;
    });
  }

  protected User createUserInSeparateThread(String login, String password) throws Exception {
    return callInSeparateThread(() -> {
      return createUser(login, password);
    });
  }

  protected Project createProjectInSeparateThread(String projectId) throws Exception {
    return callInSeparateThread(() -> {
      return createProject(projectId);
    });
  }

  protected Layout createOverlay(Project project, User admin) {
    return createOverlay(project, admin, "elementIdentifier\tvalue\n\t-1");
  }

  protected Layout createOverlay(Project project, User admin, String content) {
    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent(content.getBytes());
    file.setLength(content.getBytes().length);

    Layout overlay = new Layout();
    overlay.setColorSchemaType(ColorSchemaType.GENERIC);
    overlay.setInputData(file);
    overlay.setProject(project);
    overlay.setCreator(admin);
    layoutDao.add(overlay);
    return overlay;
  }

  protected Layout createOverlayInSeparateThread(String projectId, User user) throws Exception {
    return callInSeparateThread(() -> {
      Project project = projectDao.getProjectByProjectId(projectId);
      return createOverlay(project, user);
    });
  }

  protected void removeOverlayInSeparateThread(Layout layout) throws Exception {
    callInSeparateThread(() -> {
      layoutDao.delete(layoutDao.getById(layout.getId()));
      return null;
    });
  }

  protected Comment createCommentInSeparateThread(ModelData model, User owner) throws Exception {
    return callInSeparateThread(() -> {
      Comment comment = createComment(model, owner);
      commentDao.add(comment);
      return comment;
    });
  }

  protected ModelData getBuildInModel() throws Exception {
    return callInSeparateThread(() -> {
      return modelDao.getLastModelForProjectIdentifier(BUILT_IN_PROJECT, true);
    });
  }

  protected Comment createComment(ModelData map) {
    return createComment(map, null);
  }

  protected Comment createComment(ModelData map, User owner) {
    Comment comment = new Comment();
    comment.setModel(map);
    comment.setUser(owner);
    comment.setCoordinates(new Point2D.Double(10, 20));
    return comment;
  }

}
