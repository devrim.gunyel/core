package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerIntegrationTestWithoutTransaction extends ControllerIntegrationTest {

  private static final String BUILT_IN_ADMIN_PASSWORD = "admin";
  private static final String BUILT_IN_ADMIN_LOGIN = "admin";

  private static final String TEST_PROJECT = "test_project";

  Logger logger = LogManager.getLogger();
  @Autowired
  private IUserService userService;

  @Before
  public void setup() {
  }

  @Test
  public void addProjectAsCurator() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_ADMIN_LOGIN);
    try {
      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          admin);

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      RequestBuilder request = post("/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_ADMIN_LOGIN, BUILT_IN_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    } finally {
      callInSeparateThread(() -> {
        try {
          waitForProjectToFinishLoading(TEST_PROJECT);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        return null;
      });
      removeProjectInSeparateThread(TEST_PROJECT);
    }
  }

  @Test
  public void addInvalidProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_ADMIN_LOGIN);
    UploadedFileEntry fileEntry = createFileInSeparateThread(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
        admin);
    try {
      String invalidId = "aaaaaaaaxvncnbvscbnmcnbmccbnsbnsdsnbmdsvbnmsdvnbmsdbmnbndvmsbnmsvdnbmnmbdsvnbmdsvxncbmbnmscbnzdnbnabnsbnamsdbmnsadbmnasdbnmnbmsadbnmasdnbasdbnmsadnbnbmsadbnmadsnbmadsnbnbsadnbmadsbnndsabnbmdasbnmdsajqwrhgjrwhjghgjwerghjwreghwewnjnnbbbnbnbmbnbnzcmnnbmzcnmbcsbnmcsnbcnbzmnbczxnbmczxnbmcxznbcnxbmznbmxzcnbzcxnnbcxznbmzcnbczxnbmnbzcxnbmcznnczbnbzcnbmzcbnmbncznbcznbcz";

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      RequestBuilder request = post("/projects/" + invalidId)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_ADMIN_LOGIN, BUILT_IN_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFileInSeparateThread(fileEntry);
    }
  }

  @Test
  public void addProjectWithTooLongName() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_ADMIN_LOGIN);
    UploadedFileEntry fileEntry = createFileInSeparateThread(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
        admin);
    try {
      String invalidName = "aaaaaaaaxvncnbvscbnmcnbmccbnsbnsdsnbmdsvbnmsdvnbmsdbmnbndvmsbnmsvdnbmnmbdsvnbmdsvxncbmbnmscbnzdnbnabnsbnamsdbmnsadbmnasdbnmnbmsadbnmasdnbasdbnmsadnbnbmsadbnmadsnbmadsnbnbsadnbmadsbnndsabnbmdasbnmdsajqwrhgjrwhjghgjwerghjwreghwewnjnnbbbnbnbmbnbnzcmnnbmzcnmbcsbnmcsnbcnbzmnbczxnbmczxnbmcxznbcnxbmznbmxzcnbzcxnnbcxznbmzcnbczxnbmnbzcxnbmcznnczbnbzcnbmzcbnmbncznbcznbcz";

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
          new BasicNameValuePair("name", invalidName),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      RequestBuilder request = post("/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_ADMIN_LOGIN, BUILT_IN_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFileInSeparateThread(fileEntry);
    }
  }

  @Test
  public void addProjectWithTooLongVersion() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_ADMIN_LOGIN);
    UploadedFileEntry fileEntry = createFileInSeparateThread(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
        admin);
    try {
      String invalidVersion = "12345678901234567890123456";

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
          new BasicNameValuePair("version", invalidVersion),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      RequestBuilder request = post("/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_ADMIN_LOGIN, BUILT_IN_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFileInSeparateThread(fileEntry);
      removeProjectInSeparateThread(TEST_PROJECT);
    }
  }

  @Test
  public void modifyProjectWithTooLongName() throws Exception {
    createProjectInSeparateThread(TEST_PROJECT);
    try {
      String invalidName = "aaaaaaaaxvncnbvscbnmcnbmccbnsbnsdsnbmdsvbnmsdvnbmsdbmnbndvmsbnmsvdnbmnmbdsvnbmdsvxncbmbnmscbnzdnbnabnsbnamsdbmnsadbmnasdbnmnbmsadbnmasdnbasdbnmsadnbnbmsadbnmadsnbmadsnbnbsadnbmadsbnndsabnbmdasbnmdsajqwrhgjrwhjghgjwerghjwreghwewnjnnbbbnbnbmbnbnzcmnnbmzcnmbcsbnmcsnbcnbzmnbczxnbmczxnbmcxznbcnxbmznbmxzcnbzcxnnbcxznbmzcnbczxnbmnbzcxnbmcznnczbnbzcnbmzcbnmbncznbcznbcz";

      String content = "{\"project\":{\"name\":\"" + invalidName + "\"}}";

      RequestBuilder request = patch("/projects/" + TEST_PROJECT + "/")
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(content)
          .session(createSession(BUILT_IN_ADMIN_LOGIN, BUILT_IN_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeProjectInSeparateThread(TEST_PROJECT);
    }
  }

}
