package lcsb.mapviewer.web;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class ReactionControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "test_project";

  Logger logger = LogManager.getLogger();

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  private User anonymous;

  private Project project;
  private ModelData map;

  @Before
  public void setup() {
    project = createProject(TEST_PROJECT);
    map = project.getModels().iterator().next();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
  }

  @After
  public void tearDown() {
    modelService.removeModelFromCache(map);
  }

  @Test
  public void testGetAllReactions() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/*/bioEntities/reactions/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int reactions = new JsonParser()
        .parse(response)
        .getAsJsonArray().size();

    assertTrue("user should be able to access elements", reactions > 0);
  }
  
  @Test
  public void testGetReactionsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/*/bioEntities/reactions/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }
}
