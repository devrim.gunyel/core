package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class DrugControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_ADMIN_PASSWORD = "test_admin";
  private static final String TEST_ADMIN_LOGIN = "test_admin";
  Logger logger = LogManager.getLogger();
  @Autowired
  IConfigurationService configurationService;

  String project_id;

  @Before
  public void setup() {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    project_id = configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP);
  }

  @Test
  public void testSearchDrugsInProjectUrl() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + project_id + "/drugs:search")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSearchDrugsInUndefinedProject() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/drugs:search?query=xyz")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchDrugsByTargetInUndefinedProject() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/drugs:search?target=ALIAS:123")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetSuggestedList() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + project_id + "/drugs/suggestedQueryList")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetSuggestedListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/drugs/suggestedQueryList")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

}
