package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RunWith(SpringJUnit4ClassRunner.class)
public class OverlayControllerIntegrationTestWithoutTransaction extends ControllerIntegrationTest {

  private static final String TEST_LOGIN = "test_l";
  private static final String TEST_PASSWORD = "test_p";

  Logger logger = LogManager.getLogger();
  @Autowired
  IConfigurationService configurationService;
  private String defaultProjectId;

  @Before
  public void setup() throws Exception {
    defaultProjectId = configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP);
  }

  @Test
  public void testGetBioEntitiesForNonExistingOverlay() throws Exception {
    RequestBuilder request = get(
        "/projects/" + defaultProjectId + "/overlays/-1/models/*/bioEntities/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetReactionForNonExistingOverlay() throws Exception {
    RequestBuilder request = get(
        "/projects/" + defaultProjectId + "/overlays/-1/models/*/bioEntities/reactions/123/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetElementForNonExistingOverlay() throws Exception {
    RequestBuilder request = get(
        "/projects/" + defaultProjectId + "/overlays/-1/models/*/bioEntities/elements/123/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testRemoveNonExistingOverlay() throws Exception {
    RequestBuilder request = delete("/projects/" + defaultProjectId + "/overlays/-1/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testUpdateNonExistingOverlay() throws Exception {
    String body = "xxx";
    RequestBuilder request = patch("/projects/" + defaultProjectId + "/overlays/-1/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testFetchSourceForExistingOverlay() throws Exception {
    RequestBuilder request = get("/projects/" + defaultProjectId + "/overlays/-1:downloadSource")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test

  public void testRemoveOwnOverlayAsSimpleUser() throws Exception {
    User user = createUserInSeparateThread(TEST_LOGIN, TEST_PASSWORD);
    Layout overlay = null;
    try {
      overlay = createOverlayInSeparateThread(defaultProjectId, user);
      RequestBuilder request = delete("/projects/" + defaultProjectId + "/overlays/" + overlay.getId())
          .contentType(MediaType.APPLICATION_FORM_URLENCODED);

      mockMvc.perform(request)
          .andExpect(status().is4xxClientError());
    } finally {
      if (overlay != null) {
        removeOverlayInSeparateThread(overlay);
      }
      removeUserInSeparateThread(user);
    }
  }
}
