package lcsb.mapviewer.web;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConfigurationControllerIntegrationTest.class,
    ChemicalControllerIntegrationTest.class,
    CommentControllerIntegrationTest.class,
    CommentControllerIntegrationTestWithoutTransaction.class,
    ConvertControllerIntegrationTest.class,
    DrugControllerIntegrationTest.class,
    EndPointsInputValidationTests.class,
    FileControllerIntegrationTest.class,
    FileControllerIntegrationTestWithoutTransaction.class,
    FunctionControllerIntegrationTest.class,
    MapControllerIntegrationTest.class,
    MiRnaControllerIntegrationTest.class,
    OverlayControllerIntegrationTest.class,
    OverlayControllerIntegrationTestWithoutTransaction.class,
    ParameterControllerIntegrationTest.class,
    PluginControllerIntegrationTest.class,
    ProjectControllerIntegrationTest.class,
    ProjectControllerIntegrationTestForAsyncCalls.class,
    ProjectControllerIntegrationTestWithoutTransaction.class,
    PublicationsControllerIntegrationTest.class,
    ReactionControllerIntegrationTest.class,
    SpringSecurityGeneralIntegrationTest.class,
    SpringSecurityLdapIntegrationTest.class,
    TaxonomiesControllerIntegrationTest.class,
    UnitsControllerIntegrationTest.class,
    UserControllerAnonymousIntegrationTest.class,
    UserControllerIntegrationTest.class,
    UserControllerIntegrationTestWithoutTransaction.class
})

public class AllIntegrationTests {

}
