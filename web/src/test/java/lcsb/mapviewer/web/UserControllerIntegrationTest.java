package lcsb.mapviewer.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.*;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.*;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class UserControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_USER_PASSWORD = "test_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  private static final String TEST_ADMIN_PASSWORD = "test_admin";
  private static final String TEST_ADMIN_LOGIN = "test_admin";
  private static final String TEST_PROJECT = "test_project";
  Logger logger = LogManager.getLogger();
  private User user;
  private User admin;

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private ProjectDao projectDao;

  @Before
  public void setup() {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    admin = createUser(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    userService.grantUserPrivilege(admin, PrivilegeType.IS_ADMIN);
  }

  @Test
  public void grantPrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"IS_ADMIN\":true}}";

    RequestBuilder request = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, user.getPrivileges().size());
  }

  @Test
  public void grantPrivilegeForNonExistingUser() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"IS_ADMIN\":true}}";

    RequestBuilder request = patch("/users/unkown_login:updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void revokePrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    assertEquals(1, admin.getPrivileges().size());

    String body = "{\"privileges\":{\"IS_ADMIN\":false}}";

    RequestBuilder request = patch("/users/" + TEST_ADMIN_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("Privilege wasn't revoked", 0, admin.getPrivileges().size());
  }

  @Test
  public void grantProjectPrivilege() throws Exception {
    Project project = new Project("test_project");
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"READ_PROJECT:" + project.getId() + "\":true}}";

    RequestBuilder request = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, user.getPrivileges().size());
  }

  @Test
  public void grantInvalidPrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"XYZ\":true}}";

    RequestBuilder request = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void grantInvalidProjectPrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"READ_PROJECT:-5:-1\":true}}";

    RequestBuilder request = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void updateInvalidPrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"IS_ADMIN\":\"surprise\"}}";

    RequestBuilder request = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void revokeProjectPrivilege() throws Exception {
    Project project = new Project("test_project");
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String grantBody = "{\"privileges\":{\"READ_PROJECT:" + project.getId() + "\":true}}";
    String revokeBody = "{\"privileges\":{\"READ_PROJECT:" + project.getId() + "\":false}}";

    RequestBuilder grantRequest = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(grantBody)
        .session(session);
    RequestBuilder revokeRequest = patch("/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(revokeBody)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
    mockMvc.perform(revokeRequest)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, user.getPrivileges().size());
  }

  @Test
  public void fetchUserPrivilege() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<?, ?> responseObject = new Gson().fromJson(response, Map.class);

    List<?> privileges = (List<?>) responseObject.get("privileges");
    assertEquals(0, privileges.size());
  }

  @Test
  public void testUsersShouldBeFetchedInAlphabeticOrder() throws Exception {
    for (int i = 10; i > 0; i--) {
      createUser("random_user_" + i, "passwd" + i);
    }

    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/users/?columns=login")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray result = new JsonParser()
        .parse(response)
        .getAsJsonArray();
    for (int i = 1; i < result.size(); i++) {
      String login1 = result.get(i - 1).getAsJsonObject().get("login").toString();
      String login2 = result.get(i).getAsJsonObject().get("login").toString();
      assertTrue("[sort issue]: " + login1 + " should be before " + login2, login1.compareTo(login2) > 0);
    }
  }

  @Test
  public void testInformationAboutLdapForAllUsers() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/users/?columns=login,ldapAccountAvailable,connectedToLdap")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray result = new JsonParser()
        .parse(response)
        .getAsJsonArray();
    for (int i = 0; i < result.size(); i++) {
      assertNotNull(result.get(i).getAsJsonObject().get("ldapAccountAvailable"));
      assertNotNull(result.get(i).getAsJsonObject().get("connectedToLdap"));
    }
  }

  @Test
  public void testInformationAboutLdapForSingleUser() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/users/admin/?columns=login,ldapAccountAvailable,connectedToLdap")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonObject result = new JsonParser()
        .parse(response)
        .getAsJsonObject();
    assertNotNull(result.get("ldapAccountAvailable"));
    assertNotNull(result.get("connectedToLdap"));
  }

  @Test
  public void testAddUser() throws Exception {
    String testLogin = "xyz";
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"),
        new BasicNameValuePair("password", "FirstName"))));

    RequestBuilder grantRequest = post("/users/" + testLogin)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());

    assertNotNull(userService.getUserByLogin(testLogin));
  }

  @Test
  public void testAddUserWithoutPassword() throws Exception {
    String testLogin = "xyz";
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"))));

    RequestBuilder grantRequest = post("/users/" + testLogin)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGrantDefaultPrivilegesWhenAddingUser() throws Exception {
    String testLogin = "xyz";
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    configurationService.setConfigurationValue(ConfigurationElementType.DEFAULT_WRITE_PROJECT, "true");
    configurationService.setConfigurationValue(ConfigurationElementType.DEFAULT_READ_PROJECT, "false");

    createProject(TEST_PROJECT);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"),
        new BasicNameValuePair("password", "FirstName"),
        new BasicNameValuePair("defaultPrivileges", "true"))));

    RequestBuilder grantRequest = post("/users/" + testLogin)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(testLogin);

    assertTrue("Default privilege wasn't added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, "*")));
    assertTrue("Default privilege wasn't added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));

    assertFalse("Default privilege shouldn't be added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, "*")));
    assertFalse("Default privilege shouldn't be added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));

  }

  @Test
  public void testGrantDefaultGlobalPrivilegesWhenAddingUser() throws Exception {
    String testLogin = "xyz";
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    configurationService.setConfigurationValue(ConfigurationElementType.DEFAULT_CAN_CREATE_OVERLAYS, "true");

    createProject(TEST_PROJECT);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"),
        new BasicNameValuePair("password", "FirstName"),
        new BasicNameValuePair("defaultPrivileges", "true"))));

    RequestBuilder grantRequest = post("/users/" + testLogin)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(testLogin);

    assertTrue("Default privilege wasn't added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.CAN_CREATE_OVERLAYS)));
  }

  @Test
  public void addUserWithoutBodyShouldThrowBadRequest() throws Exception {
    String testLogin = "xyz";
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = post("/users/" + testLogin)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void userUpdateOwnPassword() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String newPassword = "new pass";
    String body = "{\"user\":{\"password\":\"" + newPassword + "\"}}";

    RequestBuilder grantRequest = patch("/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());

    MockHttpSession sessionWithNewPass = createSession(TEST_USER_LOGIN, newPassword);
    assertNotNull(sessionWithNewPass);
  }

  @Test
  public void userCannotUpdateOwnLdapConnection() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"connectedtoldap\":false}}";

    RequestBuilder grantRequest = patch("/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isForbidden());
  }

}
