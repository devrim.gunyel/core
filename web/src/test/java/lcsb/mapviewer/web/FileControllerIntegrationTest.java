package lcsb.mapviewer.web;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class FileControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_USER_PASSWORD = "test_pass";

  private static final String TEST_USER_LOGIN = "test_user";

  @Autowired
  private UploadedFileEntryDao uploadedFileEntryDao;

  @Before
  public void setup() {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @Test
  public void canCreateAndUploadFile() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "test_file"),
        new BasicNameValuePair("length", "999999"))));

    RequestBuilder request = post("/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    String fileId = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("id")
        .getAsString();

    body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("id", fileId),
        new BasicNameValuePair("data", "test_content"))));

    request = post("/files/" + fileId + ":uploadContent")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNotNull(uploadedFileEntryDao.getById(Integer.valueOf(fileId)));
  }

  @Test
  public void tryToAppendToFileWithoutOwner() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    UploadedFileEntry object = new UploadedFileEntry();
    object.setFileContent(new byte[] {});
    object.setLength(99999);
    uploadedFileEntryDao.add(object);

    String fileId = String.valueOf(object.getId());

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("id", fileId),
        new BasicNameValuePair("data", "test_content"))));

    RequestBuilder request = post("/files/" + fileId + ":uploadContent")
        .content(body)
        .session(session);

    mockMvc.perform(request).andDo(MockMvcResultHandlers.print())
        .andExpect(status().isForbidden());
  }

}
