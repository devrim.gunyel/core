package lcsb.mapviewer.web;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.*;

import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class ConfigurationControllerIntegrationTest extends ControllerIntegrationTest {
  private static final String TEST_USER_PASSWORD = "test_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  private static final String TEST_ADMIN_PASSWORD = "test_admin_pass";
  private static final String TEST_ADMIN_LOGIN = "test_admin";
  Logger logger = LogManager.getLogger();
  @Autowired
  private IUserService userService;

  private User user;

  @Before
  public void setup() {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @Test
  public void testSetSmtpPortToInvalid() throws Exception {
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", "not a number");
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);
    String body = new Gson().toJson(data);

    RequestBuilder request = patch("/configuration/options/" + ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void accessConfigurationAsAnonymous() throws Exception {
    RequestBuilder request = get("/configuration/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void accessConfigurationOptionsAsAnonymous() throws Exception {
    RequestBuilder request = get("/configuration/options/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();

    JsonArray options = new JsonParser()
        .parse(response)
        .getAsJsonArray();
    for (JsonElement jsonElement : options) {
      assertFalse(jsonElement.getAsJsonObject().get("isServerSide").getAsBoolean());
    }

  }

  @Test
  public void accessConfigurationOptionsAsAdmin() throws Exception {
    createAdmin(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/configuration/options/").session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();

    JsonArray options = new JsonParser()
        .parse(response)
        .getAsJsonArray();

    boolean includeServerSide = false;
    for (JsonElement jsonElement : options) {
      includeServerSide |= jsonElement.getAsJsonObject().get("isServerSide").getAsBoolean();
    }
    assertTrue(includeServerSide);
  }

  @Test
  public void testSetSmtpPortWithoutAdminPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", "123");
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);
    String body = new Gson().toJson(data);

    RequestBuilder request = patch("/configuration/options/" + ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
