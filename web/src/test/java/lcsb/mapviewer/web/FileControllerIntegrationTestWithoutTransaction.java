package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonParser;

@RunWith(SpringJUnit4ClassRunner.class)
public class FileControllerIntegrationTestWithoutTransaction extends ControllerIntegrationTest {

  Logger logger = LogManager.getLogger();

  private static final String BUILT_IN_TEST_ADMIN_PASSWORD = "admin";
  private static final String BUILT_IN_TEST_ADMIN_LOGIN = "admin";

  @Before
  public void setup() {
  }

  @Test
  public void appendToNonExistingFile() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("id", "-1"),
        new BasicNameValuePair("data", "test_content"))));

    RequestBuilder request = post("/files/-1:uploadContent")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void createAndAppendToExistingFile() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "unknown.txt"),
        new BasicNameValuePair("length", "29"))));

    RequestBuilder request = post("/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    int id = new JsonParser().parse(response).getAsJsonObject().get("id").getAsInt();

    String fileContent = "elementIdentifier\tvalue\nxx\t-1";

    RequestBuilder postContentRequest = post("/files/" + id + ":uploadContent")
        .content(fileContent)
        .session(session);

    mockMvc.perform(postContentRequest)
        .andExpect(status().is2xxSuccessful());
  }
}
