package lcsb.mapviewer.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.impl.LdapService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;

@Profile("ldapTest")
@Configuration
public class LdapServiceTestConfiguration {
  static String TEST_LOGIN = "john.doe.test";
  static String TEST_PASSWD = "test_passwd";
  static String LDAP_FILE_CONTENT = "./src/test/resources/ldap/john-doe-test-example.ldif";
  static String TEST_INVALID_PASSWD = "incorrect password";
  @Autowired
  IConfigurationService configurationService;
  Logger logger = LogManager.getLogger();

  @Bean
  @Primary
  public ILdapService createMockLdapService() throws LDAPException {
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "dc=uni,dc=lu");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS, "person");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER,
        "memberof=cn=gitlab,cn=groups,cn=accounts,dc=uni,dc=lu");

    LdapService ldapService = Mockito.spy(new LdapService(null));
    ldapService.setConfigurationService(configurationService);
    Mockito.doAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=" + TEST_LOGIN + ",cn=users,cn=accounts,dc=uni,dc=lu", TEST_PASSWD);
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, LDAP_FILE_CONTENT);
        ds.startListening();
        return ds.getConnection();
      }
    }).when(ldapService).getConnection();
    return ldapService;
  }

}
