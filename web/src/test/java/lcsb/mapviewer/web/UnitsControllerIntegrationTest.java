package lcsb.mapviewer.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class UnitsControllerIntegrationTest extends ControllerIntegrationTest {

  @Test
  public void testGetUnitListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/*/units/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }
  
  @Test
  public void testGetUnitWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/*/units/1")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }
}
