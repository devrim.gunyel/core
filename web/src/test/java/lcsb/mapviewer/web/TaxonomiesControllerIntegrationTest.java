package lcsb.mapviewer.web;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class TaxonomiesControllerIntegrationTest extends ControllerIntegrationTest {

  Logger logger = LogManager.getLogger();

  @Before
  public void setup() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testGetTaxonomies() throws Exception {

    RequestBuilder request = get("/genomics/taxonomies/").contentType(MediaType.APPLICATION_FORM_URLENCODED);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int taxonomies = new JsonParser()
        .parse(response)
        .getAsJsonArray().size();

    assertTrue("list of taxonomies shouldn't be empty", taxonomies > 0);
  }

}
