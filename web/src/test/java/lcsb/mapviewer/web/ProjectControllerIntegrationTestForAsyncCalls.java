package lcsb.mapviewer.web;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.transaction.Transactional;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.IUserService;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerIntegrationTestForAsyncCalls extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "test_project";
  private static final String CURATOR_PASSWORD = "curator_pass";
  private static final String CURATOR_LOGIN = "test_curator";
  private static final String ADMIN_PASSWORD = "admin_pass";
  private static final String ADMIN_LOGIN = "admin_user";
  private static final String USER_PASSWORD = "user_pass";
  private static final String USER_LOGIN = "test_user";
  Logger logger = LogManager.getLogger();
  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private IUserService userService;

  @Before
  public void setup() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    Project project = projectDao.getProjectByProjectId(TEST_PROJECT);
    if (project != null) {
      removeProjectInSeparateThread(project);
    }
  }

  @Test(timeout = 10000)
  public void addProjectAsCurator() throws Exception {
    User curator = createCuratorInSeparateThread(CURATOR_LOGIN, CURATOR_PASSWORD);
    try {
      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          curator);

      callInSeparateThread(() -> {
        try {
          String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
              new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
              new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
              new BasicNameValuePair("parser",
                  "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

          RequestBuilder request = post("/projects/" + TEST_PROJECT)
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .content(body)
              .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

          mockMvc.perform(request).andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      });

      waitForProjectToFinishLoading(TEST_PROJECT);

    } finally {
      removeUserInSeparateThread(curator);
    }
  }

  private void waitForProjectToFinishRemoving(String projectId) throws InterruptedException {
    Project project;
    do {
      project = projectDao.getProjectByProjectId(projectId);
      if (project != null) {
        projectDao.evict(project);
      }
      Thread.sleep(100);
    } while (project != null);
  }

  @Test(timeout = 10000)
  public void checkDefaultPrivilegesAfterAddingProject() throws Exception {
    User curator = createCuratorInSeparateThread(CURATOR_LOGIN, CURATOR_PASSWORD);
    User user = createCuratorInSeparateThread(USER_LOGIN, USER_PASSWORD);
    grantPrivilegeInSeparateThread(user, PrivilegeType.READ_PROJECT.name() + ":*");
    try {
      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          curator);

      callInSeparateThread(() -> {
        try {
          String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
              new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
              new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
              new BasicNameValuePair("parser",
                  "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

          RequestBuilder request = post("/projects/" + TEST_PROJECT)
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .content(body)
              .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

          mockMvc.perform(request).andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      });

      waitForProjectToFinishLoading(TEST_PROJECT);

      user = userDao.getById(user.getId());
      curator = userDao.getById(curator.getId());

      assertTrue("User privileges weren't updated after project was created",
          user.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
      assertTrue("Curator privileges weren't updated after project was created",
          curator.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));
      assertTrue("Curator privileges weren't updated after project was created",
          curator.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));

    } finally {
      userDao.evict(user);
      userDao.evict(curator);
      removeUserInSeparateThread(curator);
      removeUserInSeparateThread(user);
    }
  }

  @Test(timeout = 10000)
  public void checkDropPrivilegesAfterRemovingProject() throws Exception {
    createProjectInSeparateThread(TEST_PROJECT);
    User curator = createCuratorInSeparateThread(CURATOR_LOGIN, CURATOR_PASSWORD);
    grantPrivilegeInSeparateThread(curator, PrivilegeType.WRITE_PROJECT.name() + ":" + TEST_PROJECT);
    try {
      callInSeparateThread(() -> {
        try {
          RequestBuilder request = delete("/projects/" + TEST_PROJECT)
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

          mockMvc.perform(request).andExpect(status().is2xxSuccessful());
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      });

      waitForProjectToFinishRemoving(TEST_PROJECT);

      curator = userDao.getById(curator.getId());

      assertFalse("Curator privileges weren't updated after project was removed",
          curator.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));

    } finally {
      userDao.evict(curator);
      removeUserInSeparateThread(curator);
    }
  }

  private void grantPrivilegeInSeparateThread(User user, String string) throws Exception {
    callInSeparateThread(() -> {
      userService.grantUserPrivilege(user, PrivilegeType.valueOf(string.split(":")[0]), string.split(":")[1]);
      return null;
    });
  }

  @Test(timeout = 10000)
  public void addProjectWithInvalidId() throws Exception {
    User curator = createAdminInSeparateThread(ADMIN_LOGIN, ADMIN_PASSWORD);
    try {
      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          curator);

      callInSeparateThread(() -> {
        try {
          String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
              new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
              new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
              new BasicNameValuePair("parser",
                  "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

          RequestBuilder request = post("/projects/*")
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .content(body)
              .session(createSession(ADMIN_LOGIN, ADMIN_PASSWORD));

          mockMvc.perform(request).andExpect(status().is4xxClientError());
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      });

      assertNull(projectDao.getProjectByProjectId("*"));

    } finally {
      removeUserInSeparateThread(curator);
      Project project = projectDao.getProjectByProjectId("*");
      if (project != null) {
        projectDao.evict(project);
        waitForProjectToFinishLoading("*");
        removeProjectInSeparateThread(project);
      }
    }
  }

  @Test(timeout = 10000)
  public void addProjectWithoutPrivileges() throws Exception {
    User curator = createUserInSeparateThread(CURATOR_LOGIN, CURATOR_PASSWORD);
    try {
      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          curator);

      callInSeparateThread(() -> {
        try {
          String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
              new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
              new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
              new BasicNameValuePair("parser",
                  "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

          RequestBuilder request = post("/projects/" + TEST_PROJECT)
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .content(body)
              .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

          mockMvc.perform(request).andExpect(status().isForbidden());
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      });

      assertNull(projectDao.getProjectByProjectId(TEST_PROJECT));

    } finally {
      removeUserInSeparateThread(curator);
    }
  }

  @Test
  public void testUserPrivilegeChangeDuringUpload() throws Exception {
    User admin = createAdminInSeparateThread(ADMIN_LOGIN, ADMIN_PASSWORD);
    User curator = createAdminInSeparateThread(CURATOR_LOGIN, CURATOR_PASSWORD);

    String TEST_PROJECT_2 = "test_id2";
    try {
      Project defaultProject = createProjectInSeparateThread(TEST_PROJECT_2);
      grantPrivilegeInSeparateThread(admin, PrivilegeType.READ_PROJECT.name() + ":" + defaultProject.getProjectId());
      grantPrivilegeInSeparateThread(admin, PrivilegeType.WRITE_PROJECT.name() + ":" + defaultProject.getProjectId());

      UploadedFileEntry fileEntry = createFileInSeparateThread(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), "UTF-8"),
          curator);

      Thread addProjectThread = new Thread(new Runnable() {

        @Override
        public void run() {
          try {
            callInSeparateThread(() -> {
              try {
                String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
                    new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
                    new BasicNameValuePair("mapCanvasType", "OPEN_LAYERS"),
                    new BasicNameValuePair("parser",
                        "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

                RequestBuilder request = post("/projects/" + TEST_PROJECT)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .content(body)
                    .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

                mockMvc.perform(request).andExpect(status().is2xxSuccessful());
                waitForProjectToFinishLoading(TEST_PROJECT);
              } catch (Exception e) {
                e.printStackTrace();
              }
              return null;
            });
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
      addProjectThread.start();

      Thread dropPrivilegesThread = new Thread(new Runnable() {

        @Override
        public void run() {
          try {
            Thread.sleep(600);
            userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.READ_PROJECT,
                defaultProject.getProjectId());
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });

      dropPrivilegesThread.start();
      dropPrivilegesThread.join();

      addProjectThread.join();

      projectDao.evict(defaultProject);
      Project project = projectDao.getProjectByProjectId(TEST_PROJECT);
      assertEquals(ProjectStatus.DONE, project.getStatus());
      removeProjectInSeparateThread(project);

      assertNull(projectDao.getProjectByProjectId(TEST_PROJECT));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      removeUserInSeparateThread(curator);
      removeUserInSeparateThread(admin);
      removeProjectInSeparateThread(TEST_PROJECT_2);
      removeProjectInSeparateThread(TEST_PROJECT);
    }
  }


  private User createCuratorInSeparateThread(String login, String password) throws Exception {
    return callInSeparateThread(() -> {
      return createCurator(login, password);
    });
  }

  private User createAdminInSeparateThread(String login, String password) throws Exception {
    return callInSeparateThread(() -> {
      return createAdmin(login, password);
    });
  }

}
