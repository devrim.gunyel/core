package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.plugin.PluginDao;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class PluginControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_USER_PASSWORD = "test_pass";
  private static final String TEST_USER_LOGIN = "test_user";
  Logger logger = LogManager.getLogger();
  @Autowired
  private PluginDao pluginDao;

  @Autowired
  private IUserService userService;

  private User user;

  @Before
  public void setup() {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @Test
  public void createPublicPluginWithoutPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", "x"),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "true"),
        new BasicNameValuePair("url", "x"))));

    RequestBuilder request = post("/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void createPublicPluginWithPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", "x"),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "true"),
        new BasicNameValuePair("url", "x"))));

    RequestBuilder request = post("/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, pluginDao.getAll().size());
  }

  @Test
  public void createPrivatePlugin() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", "x"),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "false"),
        new BasicNameValuePair("url", "x"))));

    RequestBuilder request = post("/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, pluginDao.getAll().size());
  }

  @Test
  public void createDefaultPluginWithoutAccess() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", "x"),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "false"),
        new BasicNameValuePair("isDefault", "true"),
        new BasicNameValuePair("url", "x"))));

    RequestBuilder request = post("/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void setAndGetGlobalPluginData() throws Exception {
    String body = "value=xxx";
    Plugin plugin = createPlugin();

    RequestBuilder request = post("/plugins/" + plugin.getHash() + "/data/global/key/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    request = get("/plugins/" + plugin.getHash() + "/data/global/key/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    String result = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("value")
        .getAsString();

    assertEquals("xxx", result);
  }

  private Plugin createPlugin() {
    Plugin plugin = new Plugin();
    plugin.setHash("XYZ");
    plugin.setName("a");
    plugin.setVersion("0.0.1");
    pluginDao.add(plugin);
    return plugin;
  }

  @Test
  public void updatePluginWithPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);

    Plugin plugin = createPlugin();
    Plugin updatedData = new Plugin();
    updatedData.setHash(plugin.getHash());
    updatedData.setName("new name");
    updatedData.setVersion("v0.0.13");
    updatedData.setDefault(true);

    Map<String, Object> bodyMap = new HashedMap<>();
    bodyMap.put("plugin", updatedData);

    String body = new Gson().toJson(bodyMap);

    RequestBuilder request = patch("/plugins/" + plugin.getHash())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(updatedData.getName(), plugin.getName());
    assertEquals(updatedData.getVersion(), plugin.getVersion());
    assertEquals(updatedData.isDefault(), plugin.isDefault());
  }

}
