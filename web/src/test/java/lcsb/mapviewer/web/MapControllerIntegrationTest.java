package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonParser;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
public class MapControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "test_project";

  Logger logger = LogManager.getLogger();

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  private User anonymous;

  private Project project;
  private ModelData map;

  @Before
  public void setup() {
    project = createProject(TEST_PROJECT);
    map = project.getModels().iterator().next();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
  }

  @After
  public void tearDown() {
    modelService.removeModelFromCache(map);
  }

  @Test
  public void testGetAllElements() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/*/bioEntities/elements/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int elements = new JsonParser()
        .parse(response)
        .getAsJsonArray().size();

    assertTrue("user should be able to access elements", elements > 0);
  }

  @Test
  public void testGetAllElementsForUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/*/bioEntities/elements/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesAsAnonymous() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/models/" + map.getId() + "/bioEntities:search?coordinates=104.36,182.81")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSearchBioEntitiesByCoordinatesWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/*/models/" + map.getId() + "/bioEntities:search?coordinates=104.36,182.81")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesByQueryWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/*/models/" + map.getId() + "/bioEntities:search?query=s1")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesByCoordinatesWithInvalidModelId() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/models/-1/bioEntities:search?coordinates=104.36,182.81")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesWithInvalidModelId() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/models/-1/bioEntities:search?query=s1")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesOnEverySubmap() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get(
        "/projects/" + TEST_PROJECT + "/models/*/bioEntities:search?query=s1")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSuggestedQueryListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/projects/*/models/*/bioEntities/suggestedQueryList")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetMapById() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/" + map.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int mapId = new JsonParser()
        .parse(response)
        .getAsJsonObject().get("idObject").getAsInt();

    assertEquals((int) map.getId(), mapId);
  }

  @Test
  public void testGetMapByIdWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/" + map.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetMapsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateMapWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    String content = "{}";

    RequestBuilder request = patch("/projects/*/models/" + map.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session)
        .content(content);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadImage() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadImage?" +
        "handlerClass=" + PngImageGenerator.class.getCanonicalName())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadImageWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/" + map.getId() + ":downloadImage?" +
        "handlerClass=" + PngImageGenerator.class.getCanonicalName())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadModel() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadModel?" +
        "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName())
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadModelAsZip() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadModel?" +
        "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName())
            .accept(MimeType.ZIP.getTextRepresentation()+";q=0.8")
            .session(session);

    MvcResult result = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn();
    assertEquals(MimeType.ZIP.getTextRepresentation(), result.getResponse().getContentType());
    assertTrue(result.getResponse().getHeader("Content-Disposition").endsWith(".zip"));
  }

  @Test
  public void testDownloadModelWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_LOGIN,
        ControllerIntegrationTest.BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/projects/*/models/" + map.getId() + ":downloadModel?" +
        "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }


}
