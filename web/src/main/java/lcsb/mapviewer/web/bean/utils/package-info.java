/**
 * Provides util classes used by the web interface.
 */
package lcsb.mapviewer.web.bean.utils;
