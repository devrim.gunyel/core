package lcsb.mapviewer.web.bean.utils;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;

/**
 * This filter enables ajax queries from all domains. It should be used for
 * restfull API.
 * 
 * @author Piotr Gawron
 *
 */
public class CORSFilter implements Filter {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(CORSFilter.class);

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    HttpServletRequest request = (HttpServletRequest) req;

    String origin = request.getHeader("ORIGIN");
    if (origin == null || origin.trim().isEmpty() || !Configuration.isDisableCors()) {
      origin = "*";
    }
    response.setHeader("Access-Control-Allow-Origin", origin);
    chain.doFilter(req, response);
  }

  @Override
  public void destroy() {
  }

}
