package lcsb.mapviewer.web.bean.utils;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.*;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.services.interfaces.*;

/**
 * Bean where init script of the application is placed. The method in this bean
 * should be called only once, during application start.
 */
@Component
public class StartupBean {

  private final transient Logger logger = LogManager.getLogger();

  private transient IProjectService projectService;
  private transient IConfigurationService configurationService;
  private transient IReferenceGenomeService referenceGenomeService;
  private transient DbUtils dbUtils;
  private ServletContext servletContext;

  @Autowired
  public StartupBean(IProjectService projectService,
      IConfigurationService configurationService,
      IReferenceGenomeService referenceGenomeService,
      ServletContext servletContext,
      DbUtils dbUtils) {
    this.projectService = projectService;
    this.configurationService = configurationService;
    this.referenceGenomeService = referenceGenomeService;
    this.servletContext = servletContext;
    this.dbUtils = dbUtils;
  }

  /**
   * Method that process initial script of application.
   */
  @PostConstruct
  public void init() {
    loadCustomLog4jProperties();
    logger.debug("Application startup script starts");
    Configuration.setWebAppDir(servletContext.getRealPath(".") + "/../");
    setInterruptedProjectsStatuses();
    modifyXFrameDomain();
    modifyCorsDomain();
    setSessionLength();
    removeInterruptedReferenceGenomeDownloads();
    logger.debug("Application startup script ends");
  }

  private void loadCustomLog4jProperties() {
    String filename = "/etc/minerva/log4j2.properties";
    File file = new File(filename);
    if (file.exists()) {
      try {
        LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        context.setConfigLocation(file.toURI());
        logger.info("log4j  configuration loaded from: " + filename);
      } catch (Exception e) {
        logger.error("Problem with loading log4j configuration: " + filename);
      }
    }

  }

  private void setSessionLength() {
    try {
      String sessionLength = configurationService.getConfigurationValue(ConfigurationElementType.SESSION_LENGTH);
      Integer value = Integer.valueOf(sessionLength);
      Configuration.setSessionLength(value);
    } catch (Exception e) {
      logger.error("Problem with setting default session length.", e);
    }
  }

  private void modifyXFrameDomain() {
    try {
      for (String domain : configurationService.getConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN)
          .split(";")) {
        Configuration.getxFrameDomain().add(domain);
      }
    } catch (Exception e) {
      logger.error("Problem with modyfing x frame domain...", e);
    }
  }

  private void modifyCorsDomain() {
    try {
      Configuration.setDisableCors(
          configurationService.getConfigurationValue(ConfigurationElementType.CORS_DOMAIN).equalsIgnoreCase("true"));
    } catch (Exception e) {
      logger.error("Problem with modyfing cors...", e);
    }
  }

  /**
   * Removes downloads of reference genomes that were interrupted by tomcat
   * restart.
   */
  private void removeInterruptedReferenceGenomeDownloads() {
    try {
      for (ReferenceGenome genome : referenceGenomeService.getDownloadedGenomes()) {
        if (genome.getDownloadProgress() < IProgressUpdater.MAX_PROGRESS) {
          logger.warn("Removing genome that was interrupted: " + genome);
          try {
            referenceGenomeService.removeGenome(genome);
          } catch (IOException e) {
            logger.error("Problem with removing genome: " + genome);
          }
        }
      }
    } catch (Exception e) {
      logger.error("Problem with removing interrupted downloads...", e);
    }

  }

  /**
   * Set {@link ProjectStatus#FAIL} statuses to projects that were uploading when
   * application was shutdown.
   */
  private void setInterruptedProjectsStatuses() {
    try {
      dbUtils.createSessionForCurrentThread();
      for (Project project : projectService.getAllProjects()) {
        if (!ProjectStatus.DONE.equals(project.getStatus()) && !ProjectStatus.FAIL.equals(project.getStatus())) {
          ProjectLogEntry entry = new ProjectLogEntry();
          entry.setSeverity("ERROR");
          entry.setType(ProjectLogEntryType.OTHER);
          entry.setContent(
              "Project uploading was interrupted by application restart (at the stage: " + project.getStatus() + ").");
          project.setStatus(ProjectStatus.FAIL);
          project.addLogEntry(entry);
          projectService.updateProject(project);
          logger.info("Status of project: " + project.getProjectId() + " changed to fail.");
        }
      }
    } catch (Exception e) {
      logger.error("Problem with changing project status ...", e);
    } finally {
      dbUtils.closeSessionForCurrentThread();
    }
  }

}
