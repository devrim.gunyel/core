package lcsb.mapviewer.web.bean.utils;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This filter disables caching for API queries.
 * 
 * @author Piotr Gawron
 *
 */
public class CacheFilter implements Filter {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(CacheFilter.class);

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    // caching on Safari
    response.addHeader("Vary", "*");
    // generic cache prevent mechanism
    response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.addHeader("Pragma", "no-cache");
    response.addHeader("Expires", "0");
    chain.doFilter(req, response);
  }

  @Override
  public void destroy() {
  }

}
