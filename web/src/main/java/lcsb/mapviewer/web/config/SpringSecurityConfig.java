package lcsb.mapviewer.web.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.*;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import lcsb.mapviewer.api.SpringRestApiConfig;
import lcsb.mapviewer.services.SpringServiceConfig;
import lcsb.mapviewer.web.bean.utils.*;

@Configuration
@ComponentScan(basePackages = { "lcsb.mapviewer.web.config" })
@Import({ SpringRestApiConfig.class, SpringServiceConfig.class })
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

  private AuthenticationSuccessHandler successHandler;
  private AuthenticationFailureHandler failureHandler;
  private LogoutSuccessHandler logoutSuccessHandler;
  private List<AuthenticationProvider> authenticationProviders;

  @Autowired
  SpringSecurityConfig(AuthenticationSuccessHandler successHandler,
      AuthenticationFailureHandler failureHandler,
      LogoutSuccessHandler logoutSuccessHandler,
      List<AuthenticationProvider> authenticationProviders) {
    super(false);
    this.successHandler = successHandler;
    this.failureHandler = failureHandler;
    this.logoutSuccessHandler = logoutSuccessHandler;
    this.authenticationProviders = authenticationProviders;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    authenticationProviders.forEach(auth::authenticationProvider);
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers("/")
        .antMatchers("*.xhtml")
        .antMatchers("/resources/**")
        .antMatchers("/javax.faces.resource/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .and()
        .anonymous().principal(lcsb.mapviewer.common.Configuration.ANONYMOUS_LOGIN)
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(new Http403ForbiddenEntryPoint())
        .and()
        .formLogin()
        .usernameParameter("login")
        .passwordParameter("password")
        .loginProcessingUrl("/api/doLogin")
        .successHandler(successHandler)
        .failureHandler(failureHandler)
        .and()
        .logout()
        .logoutUrl("/api/doLogout").permitAll()
        .logoutSuccessHandler(logoutSuccessHandler)
        .deleteCookies(lcsb.mapviewer.common.Configuration.AUTH_TOKEN)
        .and()
        .authorizeRequests()
        .and()
        .headers()
        .frameOptions().disable() // is managed by XFrameFilter
        .and()
        .cors().disable() // is managed by CORSFilter
        .csrf().disable()
        .addFilterAfter(new XFrameFilter(), BasicAuthenticationFilter.class)
        .addFilterAfter(new CacheFilter(), BasicAuthenticationFilter.class)
        .addFilterAfter(new CORSFilter(), BasicAuthenticationFilter.class);
  }

}