package lcsb.mapviewer.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SpringSecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {
}
