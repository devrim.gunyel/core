package lcsb.mapviewer.web.config;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

  private static Logger logger = LogManager.getLogger(AuthenticationSuccessHandlerImpl.class);

  private IUserService userService;

  public AuthenticationSuccessHandlerImpl(IUserService userService) {
    this.userService = userService;
  }

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest request,
      HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    response.setStatus(HttpStatus.OK.value());

    User user = userService.getUserByLogin(authentication.getName());
    if (user == null) {
      logger.fatal("Inconsistent state. User doesn't exist");
    } else {
      String method;
      if (user.isConnectedToLdap()) {
        method = "LDAP";
      } else {
        method = "LOCAL";
      }
      logger.debug(
          "User " + authentication.getName() + " successfully logged in using " + method + " authentication method");

      request.getSession().setMaxInactiveInterval(Configuration.getSessionLength());

      Map<String, Object> result = new TreeMap<>();
      result.put("info", "Login successful.");
      result.put("token", request.getSession().getId());
      result.put("login", user.getLogin());

      String json = new ObjectMapper().writeValueAsString(result);

      response.setContentType("application/json");
      response.getWriter().print(json);

    }

  }

}
