package lcsb.mapviewer.web.config;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.user.UserDao;

@Transactional
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  private UserDao userDao;

  @Autowired
  public UserDetailsServiceImpl(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userDao.getUserByLogin(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    List<GrantedAuthority> authorities = user.getPrivileges().stream()
        .map(privilege -> new SimpleGrantedAuthority(privilege.toString()))
        .collect(Collectors.toList());
    return org.springframework.security.core.userdetails.User
        .withUsername(username)
        .password(user.getCryptedPassword())
        .disabled(user.isRemoved() || username.equals(Configuration.ANONYMOUS_LOGIN))
        .authorities(authorities)
        .build();
  }
}