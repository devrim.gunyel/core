package lcsb.mapviewer.web.config;

import java.net.URL;
import java.util.EnumSet;

import javax.servlet.*;

import org.apache.logging.log4j.core.LoggerContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.api.SpringRestApiConfig;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.persist.SpringPersistConfig;
import lcsb.mapviewer.services.SpringServiceConfig;
import lcsb.mapviewer.web.bean.utils.XFrameFilter;

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] {
        SpringSecurityConfig.class,
        SpringPersistConfig.class,
        SpringAnnotationConfig.class,
        SpringServiceConfig.class,
        SpringRestApiConfig.class,
        SpringWebConfig.class
    };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[] {};
  }

  @Override
  public void onStartup(ServletContext container) throws ServletException {
    assert container.setInitParameter("com.sun.faces.enableMissingResourceLibraryDetection", "true");
    assert container.setInitParameter("primefaces.THEME", "home");
    assert container.setInitParameter("javax.faces.FACELETS_BUFFER_SIZE", "65535");
    assert container.setInitParameter("facelets.BUFFER_SIZE", "65535");
    assert container.setInitParameter("javax.faces.FACELETS_LIBRARIES", "/WEB-INF/springsecurity.taglib.xml");
    assert container.setInitParameter("javax.faces.CONFIG_FILES", "/WEB-INF/faces-config.xml");
    assert container.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
    assert container.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");

    // FIXME: Why does this not work?
    // assert container.setInitParameter("log4jConfigLocation",
    // "/WEB-INF/resources/log4j2.properties");

    // Workaround for above
    String file = "/WEB-INF/resources/log4j2.properties";
    try {
      URL configStream = container.getResource(file);
      LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) org.apache.logging.log4j.LogManager
          .getContext(false);
      context.setConfigLocation(configStream.toURI());
    } catch (Exception e) {
      logger.error("Problem with loading log4j configuration: " + file);
    }

    SessionCookieConfig cookieConfig = container.getSessionCookieConfig();
    cookieConfig.setName(Configuration.AUTH_TOKEN);
    cookieConfig.setHttpOnly(false);
    container.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));

    super.onStartup(container);
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/", "/api/*" };
  }

  @Override
  protected Filter[] getServletFilters() {
    return new Filter[] { new XFrameFilter() };
  }
}
