package lcsb.mapviewer.web.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import lcsb.mapviewer.api.SpringRestApiConfig;

@Configuration
@EnableWebMvc
@Import(SpringRestApiConfig.class)
@ComponentScan(basePackages = { "lcsb.mapviewer.web" })
public class SpringWebConfig implements WebMvcConfigurer {

  private List<HandlerInterceptor> interceptors;

  @Autowired
  public SpringWebConfig(List<HandlerInterceptor> interceptors) {
    this.interceptors = interceptors;
  }

  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/");
    resolver.setSuffix(".xhtml");
    return resolver;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    interceptors.forEach(registry::addInterceptor);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry
        .addResourceHandler("/resources/**")
        .addResourceLocations("/resources/");
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**").allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry
        .addViewController("/")
        .setViewName("index");
  }

}
