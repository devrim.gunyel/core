#file with common script functions for postrm/prerm/postinst/preinst
log(){
	echo "[" $CURRENT_VERSION "]" "$@" >>__LOG_FILE__
}
LOG_FILE="__LOG_FILE__"

DB_SCRIPT_DIR="__DB_SCRIPT_DIR__"
MAX_DB_VERSION_FOR_MIGRTION="__MAX_DB_VERSION_FOR_MIGRTION__"

#new (current) version of the package
CURRENT_VERSION="__CURRENT_VERSION__"
#if we update package then this will be the old version of the package
OLD_VERSION=$2

if [ "$OLD_VERSION" = "$CURRENT_VERSION" ]
then
	OLD_VERSION="";
fi

TOMCAT_PACKAGE="";
TOMCAT8_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat8|grep "install ok installed")
TOMCAT9_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat9|grep "install ok installed")
if [ "$TOMCAT8_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat8'
fi

if [ "$TOMCAT9_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat9'
fi

POSTGRES_OK=$(dpkg-query -W --showformat='${Status}\n' postgresql|grep "install ok installed")

