package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Shape;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing BottomSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class BottomSquareCompartmentConverter extends CompartmentConverter<BottomSquareCompartment> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(BottomSquareCompartmentConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public BottomSquareCompartmentConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(BottomSquareCompartment compartment) {
    return new Line2D.Double(0, compartment.getY(), compartment.getWidth(), compartment.getY());
  }

  @Override
  protected Shape getInnerShape(BottomSquareCompartment compartment) {
    return new Line2D.Double(0, compartment.getY() + compartment.getThickness(), compartment.getWidth(),
        compartment.getY() + compartment.getThickness());
  }

  @Override
  protected Shape getBorderShape(BottomSquareCompartment compartment) {
    return new Area(new Rectangle2D.Double(0.0, compartment.getY(), compartment.getWidth(), compartment.getHeight()));
  }

}
