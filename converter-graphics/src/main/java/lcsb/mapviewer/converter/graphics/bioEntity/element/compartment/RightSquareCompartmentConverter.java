package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Shape;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing RightSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class RightSquareCompartmentConverter extends CompartmentConverter<RightSquareCompartment> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(RightSquareCompartmentConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public RightSquareCompartmentConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(RightSquareCompartment compartment) {
    return new Line2D.Double(compartment.getX(), compartment.getHeight(), compartment.getX(), 0);
  }

  @Override
  protected Shape getInnerShape(RightSquareCompartment compartment) {
    return new Line2D.Double(compartment.getX() + compartment.getThickness(), compartment.getHeight(),
        compartment.getX(), 0);
  }

  @Override
  protected Shape getBorderShape(RightSquareCompartment compartment) {
    return new Area(
        new Rectangle2D.Double(compartment.getX(), 0.0, compartment.getWidth(), compartment.getHeight()));
  }
}
