package lcsb.mapviewer.converter.graphics.bioEntity;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.SemanticZoomLevelMatcher;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.species.*;

/**
 * This interface defines what operations should be possible to convert
 * {@link BioEntity} into a graphics on Graphics2D object.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class of alias to convert
 */
public abstract class BioEntityConverter<T extends BioEntity> {

  /**
   * Alpha value (0..255) used for visualizing overlay data that are normally
   * visualized in javascript.
   */
  public static final int LAYOUT_ALPHA = 200;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger(BioEntityConverter.class);
  /**
   * Class that allows to check if element is visible (or transparent) when
   * drawing. It's used to filter out invisible elements when drawing
   * semantic/hierarchy view.
   */
  private SemanticZoomLevelMatcher zoomLevelMatcher = new SemanticZoomLevelMatcher();

  /**
   * This function draw {@link BioEntity} on the {@link Graphics2D} object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  public void draw(T bioEntity, Graphics2D graphics, ConverterParams params) throws DrawingException {
    draw(bioEntity, graphics, params, new ArrayList<>());
  }

  /**
   * This function draw {@link BioEntity} on the {@link Graphics2D} object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  protected abstract void drawImpl(T bioEntity, Graphics2D graphics, ConverterParams params) throws DrawingException;

  /**
   * This function draw representation of the alias on the graphics object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @param visualizedOverlaysColorSchemas
   *          list of {@link ColorSchema} that were used for visualizing this
   *          BioEntity in different data overlays that should be overlaid on the
   *          alias
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  protected abstract void draw(T bioEntity, Graphics2D graphics, ConverterParams params,
      List<ColorSchema> visualizedOverlaysColorSchemas)
      throws DrawingException;

  /**
   * This function will find proper font size to display text within it. Then it
   * will print this text.
   * 
   * @param bioEntity
   *          {@link BioEntity} with description to be drawn
   * @param graphics
   *          where the description should be drawn
   * @param params
   *          parameters of visualization (centering, scale)
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   */
  public abstract void drawText(T bioEntity, Graphics2D graphics, ConverterParams params) throws DrawingException;

  /**
   * Checks if {@link AbstractNode} is visible according to visualization given in
   * params.
   * 
   * @param node
   *          visibility of this object will be checked
   * @param params
   *          visualization params
   * @return true if object is visible
   */
  protected boolean isVisible(AbstractNode node, ConverterParams params) {
    if (node instanceof NodeOperator) {
      return isVisible((NodeOperator) node, params);
    } else if (node instanceof ReactionNode) {
      return isVisible(((ReactionNode) node).getElement(), params);
    } else {
      throw new InvalidArgumentException("Unknown class type: " + node.getClass());
    }
  }

  /**
   * Checks if {@link NodeOperator} is visible according to visualization given in
   * params.
   * 
   * @param operator
   *          visibility of this object will be checked
   * @param params
   *          visualization params
   * @return true if object is visible
   */
  protected boolean isVisible(NodeOperator operator, ConverterParams params) {
    boolean result = false;
    if (operator.isModifierOperator() || operator.isReactantOperator()) {
      for (AbstractNode input : operator.getInputs()) {
        result |= isVisible(input, params);
      }
    } else if (operator.isProductOperator()) {
      for (AbstractNode output : operator.getOutputs()) {
        result |= isVisible(output, params);
      }
    } else {
      throw new InvalidStateException("Unknown class state: " + operator);
    }
    return result;
  }

  /**
   * Checks if {@link BioEntity} is visible according to visualization given in
   * params.
   * 
   * @param bioEntity
   *          visibility of this object will be checked
   * @param params
   *          visualization params
   * @return true if object is visible
   */
  protected boolean isVisible(BioEntity bioEntity, ConverterParams params) {
    if (params.isNested()) {
      boolean result = zoomLevelMatcher.isVisible(params.getLevel(), bioEntity.getVisibilityLevel());
      if (bioEntity instanceof Element) {
        Compartment compartment = ((Element) bioEntity).getCompartment();
        if (compartment != null) {
          result &= isVisible(compartment, params);
        }
        if (bioEntity instanceof Species) {
          Complex complex = ((Species) bioEntity).getComplex();
          if (complex != null) {
            result &= isVisible(complex, params);
          }
        }
      } else if (bioEntity instanceof Reaction) {
        if (!isAnyProductVisible(((Reaction) bioEntity).getProducts(), params)) {
          result = false;
        } else if (!isAnyReactantVisible(((Reaction) bioEntity).getReactants(), params)) {
          result = false;
        }
      } else {
        throw new InvalidArgumentException("Unknown class type: " + bioEntity.getClass());
      }
      return result;
    }
    return true;
  }

  /**
   * Checks if {@link Element} given in the argument is transparent according to
   * the level given in the {@link ConverterParams}.
   * 
   * @param bioEntity
   *          {@link BioEntity} to be checked
   * @param params
   *          params against which check is run
   * @return <code>true</code> if object is transparent, <code>false</code>
   *         otherwise
   */
  protected boolean isTransparent(Element bioEntity, ConverterParams params) {
    return zoomLevelMatcher.isTransparent(params.getLevel(), bioEntity.getTransparencyLevel()) || !params.isNested();
  }

  /**
   * Checks if at least one reactant is visible.
   * 
   * @param reactants
   *          list of reactants
   * @param params
   *          params against which check is run
   * @return true if at least one reactant is visible
   */
  private boolean isAnyReactantVisible(List<Reactant> reactants, ConverterParams params) {
    boolean result = false;
    for (Reactant reactant : reactants) {
      if (isVisible(reactant, params)) {
        result = true;
      }
    }
    return result;
  }

  /**
   * Checks if at least one product is visible.
   * 
   * @param products
   *          list of products
   * @param params
   *          params against which check is run
   * @return true if at least one product is visible
   */
  private boolean isAnyProductVisible(List<Product> products, ConverterParams params) {
    boolean result = false;
    for (Product product : products) {
      if (isVisible(product, params)) {
        result = true;
      }
    }
    return result;
  }

}
