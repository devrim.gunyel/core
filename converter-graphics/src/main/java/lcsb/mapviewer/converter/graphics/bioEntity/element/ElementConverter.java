package lcsb.mapviewer.converter.graphics.bioEntity.element;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioEntity.BioEntityConverter;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This interface defines what operations should be possible to convert
 * {@link Element} into a graphics on Graphics2D object.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class of alias to convert
 */
public abstract class ElementConverter<T extends Element> extends BioEntityConverter<T> {

  @Override
  public final void draw(T bioEntity, Graphics2D graphics, ConverterParams params) throws DrawingException {
    if (bioEntity.getGlyph() != null) {
      drawGlyph(bioEntity, graphics);
    } else {
      super.draw(bioEntity, graphics, params);
    }
  }

  /**
   * Draws a {@link Glyph} for given bioEntity.
   * 
   * @param bioEntity
   *          element that should be visualized as a {@link Glyph}
   * @param graphics
   *          {@link Graphics2D} where we are drawing
   * @throws DrawingException
   *           thrown when there is a problem with drawing
   */
  protected void drawGlyph(T bioEntity, Graphics2D graphics) throws DrawingException {
    try {
      Image img = ImageIO.read(new ByteArrayInputStream(bioEntity.getGlyph().getFile().getFileContent()));
      graphics.drawImage(img,
          bioEntity.getX().intValue(), bioEntity.getY().intValue(),
          (int) (bioEntity.getX() + bioEntity.getWidth()), (int) (bioEntity.getY() + bioEntity.getHeight()),
          0, 0, img.getWidth(null), img.getHeight(null),
          null);
    } catch (IOException e) {
      throw new DrawingException(
          "Problem with processing glyph file: " + bioEntity.getGlyph().getFile().getOriginalFileName(), e);
    }
  }

}
