package lcsb.mapviewer.converter.graphics.layer;

import java.awt.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.*;

/**
 * This class allows to draw layer on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerConverter {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(LayerConverter.class.getName());

  LayerLineConverter lineConverter;
  LayerRectConverter rectConverter;
  LayerOvalConverter ovalConverter;
  LayerTextConverter textConverter;

  /**
   * Default constructor.
   * 
   * @param visibleTextBorder
   *          should the text have border
   */
  public LayerConverter(final boolean visibleTextBorder) {
    this.lineConverter = new LayerLineConverter();
    this.ovalConverter = new LayerOvalConverter();
    this.rectConverter = new LayerRectConverter();
    this.textConverter = new LayerTextConverter(visibleTextBorder);
  }

  /**
   * Draw the whole layer on the Graphics2D.
   * 
   * @param layer
   *          object to be drawn
   * @param graphics
   *          where we want to draw the object
   */
  public void drawLayer(final Layer layer, final Graphics2D graphics) {
    if (layer.isVisible()) {
      for (LayerText text : layer.getTexts()) {
        textConverter.draw(text, graphics);
      }
      for (LayerRect rect : layer.getRectangles()) {
        rectConverter.draw(rect, graphics);
      }
      for (LayerOval oval : layer.getOvals()) {
        ovalConverter.draw(oval, graphics);
      }
      for (PolylineData line : layer.getLines()) {
        lineConverter.draw(line, graphics);
      }
    }

  }
}