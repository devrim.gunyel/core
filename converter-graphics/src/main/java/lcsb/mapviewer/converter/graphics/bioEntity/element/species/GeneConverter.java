package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * This class defines methods used for drawing Gene SpeciesAlias on the
 * graphics2d object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneConverter extends SpeciesConverter<Gene> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(GeneConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public GeneConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Gene gene, final Graphics2D graphics, final ConverterParams params) {
    Shape shape = getGeneShape(gene);
    Color c = graphics.getColor();
    graphics.setColor(gene.getFillColor());
    graphics.fill(shape);
    graphics.setColor(gene.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(gene));
    graphics.draw(shape);
    graphics.setStroke(stroke);

    for (ModificationResidue mr : gene.getModificationResidues()) {
      drawModification(mr, graphics, false);
    }

    drawText(gene, graphics, params);
    graphics.setColor(c);
  }

  /**
   * Shape representation of the {@link Gene}.
   * 
   * @param gene
   *          {@link Gene} for which we are looking for a {@link Shape}
   * @return {@link Shape} that represents {@link Gene}
   */
  private Shape getGeneShape(final Gene gene) {
    Shape shape;
    shape = new Rectangle(gene.getX().intValue(), gene.getY().intValue(), gene.getWidth().intValue(),
        gene.getHeight().intValue());
    return shape;
  }

  @Override
  public PathIterator getBoundPathIterator(final Gene gene) {
    return getGeneShape(gene).getPathIterator(new AffineTransform());
  }

}
