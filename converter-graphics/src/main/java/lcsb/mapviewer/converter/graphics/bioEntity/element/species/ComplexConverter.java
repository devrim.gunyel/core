package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.*;
import java.awt.geom.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.geometry.FontFinder;
import lcsb.mapviewer.converter.graphics.geometry.RectangleTooSmallException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing ComplexAlias on the graphics2d
 * object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexConverter extends SpeciesConverter<Complex> {

  /**
   * How big is the triangle trimmed part of the complex.
   */
  private static final int TRIMMED_CORNER_SIZE = 5;

  /**
   * Describes the distance between border of complex and internal border in brief
   * view (without children).
   */
  private static final double INTERNAL_BORDER_DIST = 5.0;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ComplexConverter.class);

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public ComplexConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Complex complex, final Graphics2D graphics, final ConverterParams params) {
    if (complex.getState().equalsIgnoreCase("complexnoborder")) {
      return;
    }
    Color oldColor = graphics.getColor();

    int homodir;
    if (params.isSbgnFormat()) {
      // If the SBGN display mode is set, multimer is shown as two stacked glyphs
      if (complex.getHomodimer() > 1) {
        homodir = 2;
      } else {
        homodir = 1;
      }
    } else {
      homodir = complex.getHomodimer();
    }

    complex.setWidth(complex.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (complex.getHomodimer() - 1));
    complex.setHeight(complex.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (complex.getHomodimer() - 1));

    complex.setX(complex.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));
    complex.setY(complex.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));

    for (int i = 0; i < homodir; i++) {
      complex.setX(complex.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      complex.setY(complex.getY() - SpeciesConverter.HOMODIMER_OFFSET);

      GeneralPath path = getAliasPath(complex);

      Stroke stroke = graphics.getStroke();
      graphics.setColor(complex.getFillColor());
      graphics.fill(path);
      graphics.setColor(complex.getBorderColor());
      graphics.setStroke(getBorderLine(complex));
      graphics.draw(path);

      if (complex.getState().equals("brief")) {
        complex.increaseBorder(-INTERNAL_BORDER_DIST);
        path = getAliasPath(complex);
        complex.increaseBorder(INTERNAL_BORDER_DIST);
        graphics.setStroke(LineType.DOTTED.getStroke());
        graphics.draw(path);
      }
      if (complex.getActivity() && !params.isSbgnFormat()) {
        complex.increaseBorder(INTERNAL_BORDER_DIST);
        path = getAliasPath(complex);
        complex.increaseBorder(-INTERNAL_BORDER_DIST);
        graphics.setStroke(LineType.DOTTED.getStroke());
        graphics.draw(path);
      }
      graphics.setStroke(stroke);
    }

    complex.setWidth(complex.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (complex.getHomodimer() - 1));
    complex.setHeight(complex.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (complex.getHomodimer() - 1));

    // SBGN view - multimers are displayed with a unit of information containing
    // cardinality
    if (params.isSbgnFormat()) {
      String unitOfInformationText = null;
      if (complex.getStatePrefix() != null && complex.getStateLabel() != null) {
        unitOfInformationText = complex.getStatePrefix() + ":" + complex.getStateLabel();
      }
      if (homodir == 2 && (unitOfInformationText == null || !unitOfInformationText.contains("N:"))) {
        if (unitOfInformationText != null) {
          unitOfInformationText += "; ";
        } else {
          unitOfInformationText = "";
        }
        unitOfInformationText += "N:" + complex.getHomodimer();
      }

      drawUnitOfInformation(unitOfInformationText, complex, graphics);
    }

    drawStructuralState(complex.getStructuralState(), graphics);

    drawText(complex, graphics, params);
    graphics.setColor(oldColor);
  }

  @Override
  public void drawText(final Complex complex, final Graphics2D graphics, final ConverterParams params) {
    if (complex.getElements().size() > 0) {
      if (isTransparent(complex, params)) {

        super.drawText(complex, graphics, params);
        return;
      }
    }
    Color oldColor = graphics.getColor();
    String text = getText(complex);
    try {
      graphics.setColor(complex.getFontColor());
      double fontSize = complex.getFontSize();
      int size = (int) FontFinder.findMaxFontSize(params.getScale() * fontSize, Font.SANS_SERIF, graphics,
          complex.getBorder(), text);
      FontFinder.drawText(size, Font.SANS_SERIF, graphics, complex.getBorder(), text);
    } catch (RectangleTooSmallException e) {
    } finally {
      graphics.setColor(oldColor);
    }
  }

  @Override
  public PathIterator getBoundPathIterator(final Complex complex) {
    return getAliasPath(complex).getPathIterator(new AffineTransform());
  }

  /**
   * Returns the border of {@link Complex}.
   *
   * @param complex
   *          exact object for which we want to get a border
   * @return border of the {@link Complex}
   */
  private GeneralPath getAliasPath(final Complex complex) {
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD);
    path.moveTo(complex.getX() + TRIMMED_CORNER_SIZE, complex.getY());
    path.lineTo(complex.getX() + complex.getWidth() - TRIMMED_CORNER_SIZE, complex.getY());
    path.lineTo(complex.getX() + complex.getWidth(), complex.getY() + TRIMMED_CORNER_SIZE);
    path.lineTo(complex.getX() + complex.getWidth(), complex.getY() + complex.getHeight() - TRIMMED_CORNER_SIZE);
    path.lineTo(complex.getX() + complex.getWidth() - TRIMMED_CORNER_SIZE, complex.getY() + complex.getHeight());
    path.lineTo(complex.getX() + TRIMMED_CORNER_SIZE, complex.getY() + complex.getHeight());
    path.lineTo(complex.getX(), complex.getY() + complex.getHeight() - TRIMMED_CORNER_SIZE);
    path.lineTo(complex.getX(), complex.getY() + TRIMMED_CORNER_SIZE);
    path.closePath();
    return path;
  }
}
