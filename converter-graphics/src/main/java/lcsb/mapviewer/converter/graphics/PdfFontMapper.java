package lcsb.mapviewer.converter.graphics;

import java.awt.*;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;

public class PdfFontMapper extends DefaultFontMapper {

  Logger logger = LogManager.getLogger(PdfFontMapper.class);

  @Override
  public BaseFont awtToPdf(Font font) {
    BaseFont result = null;
    String fontName = font.getName();
    if (fontName.equalsIgnoreCase("DialogInput") || fontName.equalsIgnoreCase("Monospaced")
        || fontName.equalsIgnoreCase("Courier") || fontName.equalsIgnoreCase("Serif")
        || fontName.equalsIgnoreCase("TimesRoman") || fontName.equals("sanserif")
        || fontName.equalsIgnoreCase("Helvetica")) {
      result = super.awtToPdf(font);
    }

    if (fontName.equalsIgnoreCase("SansSerif")) {
      try {
        result = resolveFont("/fonts/truetype/dejavu/DejaVuSans.ttf", font);
      } catch (Exception e) {
        logger.error("Problem with getting font", e);
      }
    }

    if (result == null) {
      logger.warn("Unsupported font: " + fontName);
      result = super.awtToPdf(font);
    }
    return result;
  }

  private BaseFont resolveFont(String baseUrl, Font font) throws DocumentException, IOException {
    String url;
    if (font.isItalic()) {
      if (font.isBold()) {
        url = baseUrl.replace(".ttf", "-BoldOblique.ttf");
      } else {
        url = baseUrl.replace(".ttf", "-Oblique.ttf");
      }

    } else {
      if (font.isBold()) {
        url = baseUrl.replace(".ttf", "-Bold.ttf");

      } else {
        url = baseUrl;
      }
    }
    if (!FontFactory.isRegistered(url)) {
      FontFactory.register(url);
    }
    return BaseFont.createFont(url, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
  }

}
