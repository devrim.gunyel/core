package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing OvalCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class OvalCompartmentConverter extends CompartmentConverter<OvalCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public OvalCompartmentConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape representing alias.
   * 
   * @param compartment
   *          alias for which we are looking for a Shape
   * @return Shape object that represents alias
   */
  private Shape getShape(final Compartment compartment) {
    return new Ellipse2D.Double(compartment.getX(), compartment.getY(), compartment.getWidth(),
        compartment.getHeight());
  }

  @Override
  protected Shape getOuterShape(OvalCompartment compartment) {
    return getShape(compartment);
  }

  @Override
  protected Shape getInnerShape(OvalCompartment compartment) {
    compartment.increaseBorder(-compartment.getThickness());
    Shape result = getShape(compartment);
    compartment.increaseBorder(compartment.getThickness());
    return result;
  }
}
