package lcsb.mapviewer.converter.graphics.geometry;

import static org.junit.Assert.assertTrue;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;

public class ArrowTransformationTest extends GraphicsTestFunctions {
  Logger logger = LogManager.getLogger(ArrowTransformationTest.class);

  ArrowTransformation at = new ArrowTransformation();
  PointTransformation pointTransformation = new PointTransformation();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawingArrows() throws Exception {
    int coordX = 0;
    int coordY = 10;
    int height = 60;

    for (ArrowType type : ArrowType.values()) {
      BufferedImage bi = new BufferedImage((int) 65, (int) 20, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      graphics.setColor(Color.BLACK);

      PolylineData pd = new PolylineData(new Point2D.Double(coordX, coordY),
          new Point2D.Double(coordX + height, coordY));
      pd.getEndAtd().setArrowType(type);
      at.drawLine(pd, graphics);
    }
  }

  @Test
  public void testGetArrowTrianglePointsForInvalidLibe() throws Exception {
    double angle = 1;
    double len = 3;
    Line2D line = new Line2D.Double(10, 20, 10, 20);
    List<Point2D> points = at.getArrowTrianglePoints(line, len, angle);
    for (Point2D point : points) {
      assertTrue("Invalid point found: " + point, pointTransformation.isValidPoint(point));
    }
  }

}
