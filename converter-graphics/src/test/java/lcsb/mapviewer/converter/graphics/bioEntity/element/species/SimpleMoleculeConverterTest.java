package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeConverterTest extends GraphicsTestFunctions {
  Logger logger = LogManager.getLogger();

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawText() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    SimpleMoleculeConverter rc = new SimpleMoleculeConverter(colorExtractor);

    SimpleMolecule simpleMolecule = createSimpleMolecule();
    double x = simpleMolecule.getX();
    double width = simpleMolecule.getWidth();
    rc.drawImpl(simpleMolecule, graphics, new ConverterParams().sbgnFormat(true));
    assertEquals("Coordinates shouldn't be changed when drawing SBGN-ML", x, simpleMolecule.getX(),
        Configuration.EPSILON);
    assertEquals("Width shouldn't be changed when drawing SBGN-ML", width, simpleMolecule.getWidth(),
        Configuration.EPSILON);
  }
}
