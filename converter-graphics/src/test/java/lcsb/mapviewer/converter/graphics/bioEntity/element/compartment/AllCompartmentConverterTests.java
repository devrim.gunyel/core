package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PathwayCompartmentConverterTest.class, SquareCompartmentConverterTest.class })
public class AllCompartmentConverterTests {

}
