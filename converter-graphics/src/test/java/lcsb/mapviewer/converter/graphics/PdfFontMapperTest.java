package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.*;

import org.junit.Test;

import com.itextpdf.text.pdf.BaseFont;

public class PdfFontMapperTest {

  @Test
  public void testUnknownFont() {
    PdfFontMapper mapper = new PdfFontMapper();
    BaseFont font = mapper.awtToPdf(new Font("xxx", 0, 12));
    BaseFont helveticaFont = mapper.awtToPdf(new Font("Helvetica", 0, 12));
    assertEquals(helveticaFont.getPostscriptFontName(), font.getPostscriptFontName());
  }

  @Test
  public void testSansSerifFont() {
    PdfFontMapper mapper = new PdfFontMapper();
    BaseFont font = mapper.awtToPdf(new Font("SansSerif", 0, 12));
    BaseFont helveticaFont = mapper.awtToPdf(new Font("Helvetica", 0, 12));
    assertFalse(helveticaFont.getPostscriptFontName().equalsIgnoreCase(font.getPostscriptFontName()));
  }

}
