package lcsb.mapviewer.annotation.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;

/**
 * Cache used by the application. It contains two sub-classes responsible for
 * application level cache (for single run of the application) and database
 * level cache (for information that were gathered since the beginning).
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class GeneralCache implements GeneralCacheInterface {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(GeneralCache.class);

  /**
   * Application level cache. More information can be found
   * {@link ApplicationLevelCache here}.
   */
  private QueryCacheInterface cache1 = ApplicationLevelCache.getInstance();

  /**
   * Database level cache. More information can be found
   * {@link PermanentDatabaseLevelCache here}.
   */
  private QueryCacheInterface cache2;

  /**
   * Constructor
   */
  @Autowired
  public GeneralCache(@Qualifier(value = "permanentDatabaseLevelCache") QueryCacheInterface cache2) {
    this.cache2 = cache2;
  }

  @Override
  public Node getXmlNodeByQuery(String query, CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    Node result = null;
    if (cache1 != null) {
      result = cache1.getXmlNodeByQuery(query, type);
    }
    if (result == null && cache2 != null) {
      result = cache2.getXmlNodeByQuery(query, type);
      if (result != null && cache1 != null) {
        cache1.setCachedQuery(query, type, result);
      }
    }
    return result;
  }

  @Override
  public String getStringByQuery(String query, CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }
    String result = null;
    if (cache1 != null) {
      result = cache1.getStringByQuery(query, type);
    }
    if (result == null && cache2 != null) {
      result = cache2.getStringByQuery(query, type);
      if (result != null && cache1 != null) {
        cache1.setCachedQuery(query, type, result);
      }
    }
    return result;
  }

  @Override
  public void setCachedQuery(String query, CacheType type, Object object) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }
    setCachedQuery(query, type, object, type.getValidity());
  }

  @Override
  public void setCachedQuery(String query, CacheType type, Object object, int validDays) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }
    if (cache1 != null) {
      cache1.setCachedQuery(query, type, object, validDays);
    }
    if (cache2 != null) {
      cache2.setCachedQuery(query, type, object, validDays);
    }
  }

  @Override
  public void clearCache() {
    if (cache1 != null) {
      cache1.clearCache();
    }
    if (cache2 != null) {
      cache2.clearCache();
    }
  }

  @Override
  public void removeByQuery(String query, CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }
    if (cache1 != null) {
      cache1.removeByQuery(query, type);
    }
    if (cache2 != null) {
      cache2.removeByQuery(query, type);
    }
  }

  @Override
  public void invalidateByQuery(String query, CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }
    if (cache1 != null) {
      cache1.invalidateByQuery(query, type);
    }
    if (cache2 != null) {
      cache2.invalidateByQuery(query, type);
    }

  }

  /**
   * @return the cache2
   * @see #cache2
   */
  public QueryCacheInterface getCache2() {
    return cache2;
  }

  /**
   * @param cache2
   *          the cache2 to set
   * @see #cache2
   */
  public void setCache2(QueryCacheInterface cache2) {
    this.cache2 = cache2;
  }

}
