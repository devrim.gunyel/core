package lcsb.mapviewer.annotation.cache;

import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.net.ftp.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.persist.dao.cache.BigFileEntryDao;

/**
 * Interface for accessing and storing big files. They are stored in local file
 * system.
 * 
 * @author Piotr Gawron
 *
 */
@Service
public class BigFileCache {

  /**
   * Buffer size used for downloading single chunk of a file.
   */
  private static final int DOWNLOAD_BUFFER_SIZE = 1024;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(BigFileCache.class);

  /**
   * Access class for database objects storing statuses of the files.
   */
  private BigFileEntryDao bigFileEntryDao;

  /**
   * Access class for database objects storing some configuration information.
   */
  private ConfigurationDao configurationDao;

  /**
   * Utils that help to manage the sessions in custom multithreaded
   * implementation.
   */
  private DbUtils dbUtils;

  /**
   * Service used for executing tasks in separate thread.
   */
  private ExecutorService asyncExecutorService;

  /**
   * Service used for executing tasks immediately.
   */
  private ExecutorService syncExecutorService;

  /**
   * Factory class used to create {@link FTPClient} objects.
   */
  private FtpClientFactory ftpClientFactory = new FtpClientFactory();

  /**
   * Constructor.
   */
  @Autowired
  public BigFileCache(BigFileEntryDao bigFileEntryDao, ConfigurationDao configurationDao, DbUtils dbUtils) {
    this.bigFileEntryDao = bigFileEntryDao;
    this.configurationDao = configurationDao;
    this.dbUtils = dbUtils;
    // the executor is a daemon thread so that it will get killed automatically
    // when the main program exits
    asyncExecutorService = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });
    syncExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });

    // put in the queue empty task to make sure that everything was initialized
    // (additional managing thread was createed)
    asyncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
    syncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
  }

  /**
   * Returns a path for a file in local file system.
   * 
   * @param url
   *          url to the file
   * @return a path for a file in local file system
   * @throws FileNotFoundException
   *           thrown when file should be in file system, but couldn't be found
   *           there (somebody manually removed it)
   */
  public String getAbsolutePathForFile(String url) throws FileNotFoundException {
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      return null;
    }
    if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
      throw new FileNotFoundException("File is not complete: " + entry.getLocalPath() + ". Downloaded from: " + url);
    }
    File f = new File(Configuration.getWebAppDir() + entry.getLocalPath());
    if (!f.exists()) {
      throw new FileNotFoundException(
          "Missing big file: " + Configuration.getWebAppDir() + entry.getLocalPath() + ". Downloaded from: " + url);
    }

    return f.getAbsolutePath();
  }

  /**
   * Returns a path for a file in local file system for an ftp url.
   * 
   * @param url
   *          ftp url to the file
   * @return a path for a file in local file system for an ftp url
   * @throws FileNotFoundException
   *           thrown when file should be in file system, but couldn't be found
   *           there (somebody manually removed it)
   */
  public String getLocalPathForFile(String url) throws FileNotFoundException {
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      return null;
    }
    if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
      throw new FileNotFoundException("File is not complete: " + entry.getLocalPath() + ". Downloaded from: " + url);
    }
    File f = new File(Configuration.getWebAppDir() + entry.getLocalPath());
    if (!f.exists()) {
      throw new FileNotFoundException(
          "Missing big file: " + Configuration.getWebAppDir() + entry.getLocalPath() + ". Downloaded from: " + url);
    }

    return entry.getLocalPath();
  }

  /**
   * Download big file from ftp server.
   * 
   * @param url
   *          url for file to download
   * @param async
   *          <code>true</code> if should the download be asynchronous
   * @param updater
   *          callback method that will be called with info updates about
   *          downloading process
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public void downloadFile(String url, boolean async, IProgressUpdater updater) throws IOException, URISyntaxException {
    Callable<Void> computations = null;
    if (url.toLowerCase().startsWith("ftp:")) {
      computations = new GetFtpFileTask(url, updater);
    } else if (url.toLowerCase().startsWith("http:") || url.toLowerCase().startsWith("https:")) {
      computations = new GetHttpFileTask(url, updater);
    } else {
      throw new URISyntaxException(url, "Unknown protocol for url");
    }

    if (async) {
      asyncExecutorService.submit(computations);
    } else {
      Future<Void> task = syncExecutorService.submit(computations);
      executeTask(task);
    }
  }

  /**
   * Creates new {@link BigFileEntry} for given url.
   * 
   * @param url
   *          url for which entry will be created
   * @return new {@link BigFileEntry} for given url
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws IOException
   */
  private BigFileEntry createEntryForBigFile(String url) throws URISyntaxException, IOException {
    String localPath = configurationDao.getValueByType(ConfigurationElementType.BIG_FILE_STORAGE_DIR);
    BigFileEntry entry = new BigFileEntry();
    entry.setDownloadDate(Calendar.getInstance());
    entry.setLocalPath(localPath);
    entry.setUrl(url);
    bigFileEntryDao.add(entry);

    localPath = entry.getLocalPath() + "/" + entry.getId() + "/";
    File dirFile = new File(Configuration.getWebAppDir() + localPath);

    if (!dirFile.mkdirs()) {
      throw new IOException("Cannot create directory: " + dirFile.getAbsolutePath());
    }

    localPath += getFileName(url);

    entry.setLocalPath(localPath);
    bigFileEntryDao.update(entry);
    bigFileEntryDao.flush();
    return entry;
  }

  /**
   * Checks if local file for given url is up to date. The check is based on file
   * size check (ftp doesn't provide checksums for files, so we cannot compare it
   * differently without downloading file).
   *
   * @param url
   *          url to ftp file
   * @return <code>true</code> if file is up to date, <code>false</code> in other
   *         case
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws IOException
   *           thrown when there is a problem with accessing local or remote file
   */
  public boolean isLocalFileUpToDate(String url) throws URISyntaxException, IOException {
    if (url.toLowerCase().startsWith("ftp")) {
      return isLocalFtpFileUpToDate(url);
    } else if (url.toLowerCase().startsWith("http")) {
      return isLocalHttpFileUpToDate(url);
    } else {
      throw new URISyntaxException(url, "Unknown protocol");
    }
  }

  /**
   * Checks if local file fetched from ftp is up to date.
   *
   * @param url
   *          url to remote file
   * @return <code>true</code> if file is up to date, <code>false</code> otherwise
   * @throws FileNotFoundException
   *           thrown when local file cannot be found
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws IOException
   *           thrown when there is a problem with connection to remote server
   */
  public boolean isLocalFtpFileUpToDate(String url) throws FileNotFoundException, IOException, URISyntaxException {
    boolean result = false;
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      throw new FileNotFoundException("File wasn't downloaded: " + url);
    }
    String path = Configuration.getWebAppDir() + entry.getLocalPath();
    File f = new File(path);
    if (!f.exists()) {
      throw new FileNotFoundException("Missing file: " + path + ". Downloaded from: " + url);
    }
    long localSize = f.length();

    FTPClient ftp = ftpClientFactory.createFtpClient();
    try {
      String server = getDomainName(url);
      ftp.connect(server);
      // After connection attempt, you should check the reply code to verify
      // success.
      int reply = ftp.getReplyCode();

      if (!FTPReply.isPositiveCompletion(reply)) {
        throw new IOException("FTP server refused connection.");
      }
      ftp.enterLocalPassiveMode();
      ftp.login("anonymous", "");
      ftp.setFileType(FTP.BINARY_FILE_TYPE);

      long fileSize = -1;
      String remotePath = getFilePath(url);
      FTPFile[] files = ftp.listFiles(remotePath);
      if (files.length == 1 && files[0].isFile()) {
        fileSize = files[0].getSize();
      }

      result = (fileSize == localSize);
      ftp.logout();
    } finally {
      if (ftp.isConnected()) {
        ftp.disconnect();
      }
    }
    return result;
  }

  /**
   * Checks if local file fetched from http is up to date.
   *
   * @param url
   *          url to remote file
   * @return <code>true</code> if file is up to date, <code>false</code> otherwise
   * @throws IOException
   *           thrown when local file cannot be found or there is a problem with
   *           accessing remote file
   */
  public boolean isLocalHttpFileUpToDate(String url) throws IOException {
    boolean result = false;
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      throw new FileNotFoundException("File wasn't downloaded: " + url);
    }
    String path = Configuration.getWebAppDir() + entry.getLocalPath();
    File f = new File(path);
    if (!f.exists()) {
      throw new FileNotFoundException("Missing file: " + path + ". Downloaded from: " + url);
    }
    long localSize = f.length();

    long remoteSize = getRemoteHttpFileSize(url);

    result = (localSize == remoteSize);
    return result;
  }

  /**
   * Returns size of the remote file access via http url.
   *
   * @param url
   *          url address to the file
   * @return size of the remote file access via http url
   * @throws IOException
   *           thrown when there is a problem with accessing remote file
   */
  long getRemoteHttpFileSize(String url) throws IOException {
    long remoteSize = -1;
    HttpURLConnection conn = null;
    try {
      URL website = new URL(url);
      conn = (HttpURLConnection) website.openConnection();
      conn.setRequestMethod("HEAD");
      conn.getInputStream();
      remoteSize = conn.getContentLength();
    } finally {
      if (conn != null) {
        conn.disconnect();
      }
    }
    return remoteSize;
  }

  /**
   * Removes local file copy of a file given in a parameter.
   *
   * @param url
   *          ftp url of a file
   * @throws IOException
   *           thrown when there is a problem with deleting file
   */
  public void removeFile(String url) throws IOException {
    List<BigFileEntry> entries = bigFileEntryDao.getAllByUrl(url);
    for (BigFileEntry entry : entries) {
      String path = Configuration.getWebAppDir() + entry.getLocalPath();
      File f = new File(path);
      if (!f.exists()) {
        logger.warn("Missing file: " + path + ". Downloaded from: " + url);
      }
      String dirPath = FilenameUtils.getFullPath(path);

      FileUtils.deleteDirectory(new File(dirPath));

      bigFileEntryDao.delete(entry);
    }
  }

  /**
   * Updates local copy of a file that can be downloaded from a url given in
   * parameter.
   *
   * @param url
   *          url to an ftp file to be updated
   * @param async
   *          <code>true</code> if update should be done asynchronously,
   *          <code>false</code> otherwise
   * @throws IOException
   *           thrown when there is a problem with accessing remote file
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public void updateFile(String url, boolean async) throws URISyntaxException, IOException {
    if (isLocalFileUpToDate(url)) {
      logger.warn("File is up to date. Skipping...");
      return;
    }
    UpdateFtpFileTask updateTask = new UpdateFtpFileTask(url);
    if (async) {
      asyncExecutorService.submit(updateTask);
    } else {
      Future<Void> task = syncExecutorService.submit(updateTask);
      executeTask(task);
    }
  }

  /**
   * Executes download/update task.
   *
   * @param task
   *          task to be executed
   * @throws URISyntaxException
   *           thrown when task finished with {@link URISyntaxException}
   * @throws IOException
   *           thrown when task finished with {@link IOException}
   */
  void executeTask(Future<?> task) throws URISyntaxException, IOException {
    try {
      task.get();
    } catch (InterruptedException e) {
      logger.error(e, e);
    } catch (ExecutionException e) {
      if (e.getCause() instanceof URISyntaxException) {
        throw (URISyntaxException) e.getCause();
      } else if (e.getCause() instanceof IOException) {
        throw new IOException((IOException) e.getCause());
      } else {
        throw new InvalidStateException(e);
      }
    }
  }

  /**
   * Returns server domain name from url.
   *
   * @param url
   *          url to be processed
   * @return server domain name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  String getDomainName(String url) throws URISyntaxException {
    URI uri = new URI(url);
    String domain = uri.getHost();
    if (domain.startsWith("www.")) {
      domain = domain.substring("www.".length());
    }

    return domain;
  }

  /**
   * Returns path to file on server without server domain name from url.
   *
   * @param url
   *          url to be processed
   * @return path to file on server without server domain name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public String getFilePath(String url) throws URISyntaxException {
    URI uri = new URI(url);
    return uri.getPath();
  }

  /**
   * Returns simple file name from url.
   *
   * @param url
   *          url to be processed
   * @return simple file name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public String getFileName(String url) throws URISyntaxException {
    URI uri = new URI(url);
    return FilenameUtils.getName(uri.getPath());
  }

  /**
   * Checks if the file identified by url is cached.
   *
   * @param sourceUrl
   *          url that identifies file
   * @return <code>true</code> if the file is cached, <code>false</code> otherwise
   */
  public boolean isCached(String sourceUrl) {
    BigFileEntry entry = bigFileEntryDao.getByUrl(sourceUrl);
    if (entry != null) {
      File f = new File(Configuration.getWebAppDir() + entry.getLocalPath());
      if (!f.exists()) {
        logger.warn("File is supposed to be cached but it's not there... " + sourceUrl);
        return false;
      }
      if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
        logger.warn("File is not complete: " + entry.getLocalPath() + ".");
        return false;
      }
    }
    return entry != null;
  }

  /**
   * @param bigFileEntryDao
   *          the bigFileEntryDao to set
   * @see #bigFileEntryDao
   */
  public void setBigFileEntryDao(BigFileEntryDao bigFileEntryDao) {
    this.bigFileEntryDao = bigFileEntryDao;
  }

  /**
   * Return number of tasks that are executed or are waiting for execution.
   *
   * @return number of tasks that are executed or are waiting for execution
   */
  public int getDownloadThreadCount() {
    return ((ScheduledThreadPoolExecutor) asyncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) asyncExecutorService).getActiveCount()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getActiveCount();
  }

  /**
   * @param ftpClientFactory
   *          the ftpClientFactory to set
   * @see #ftpClientFactory
   */
  protected void setFtpClientFactory(FtpClientFactory ftpClientFactory) {
    this.ftpClientFactory = ftpClientFactory;
  }

  /**
   * @return the configurationDao
   * @see #configurationDao
   */
  protected ConfigurationDao getConfigurationDao() {
    return configurationDao;
  }

  /**
   * @param configurationDao
   *          the configurationDao to set
   * @see #configurationDao
   */
  protected void setConfigurationDao(ConfigurationDao configurationDao) {
    this.configurationDao = configurationDao;
  }

  /**
   * Task that will be able to fetch file from ftp server.
   *
   * @author Piotr Gawron
   *
   */
  private final class GetFtpFileTask implements Callable<Void> {

    /**
     * Url to the file that we want to download.
     *
     */
    private String url;

    /**
     * Callback listener that will receive information about upload progress.
     *
     */
    private IProgressUpdater updater;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     * @param updater
     *          {@link #updater}
     */
    private GetFtpFileTask(String url, IProgressUpdater updater) {
      this.url = url;
      if (updater != null) {
        this.updater = updater;
      } else {
        this.updater = new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        };
      }
    }

    @Override
    public Void call() throws Exception {
      dbUtils.createSessionForCurrentThread();
      FTPClient ftp = ftpClientFactory.createFtpClient();
      try {
        try {
          if (getAbsolutePathForFile(url) != null) {
            logger.warn("File already downloaded. Skipping...");
            return null;
          }
        } catch (FileNotFoundException e) {
          removeFile(url);
        }

        BigFileEntry entry = createEntryForBigFile(url);
        entry.setDownloadThreadId(Thread.currentThread().getId());
        bigFileEntryDao.update(entry);

        String server = getDomainName(url);
        ftp.connect(server);
        // After connection attempt, you should check the reply code to verify
        // success.
        int reply = ftp.getReplyCode();

        if (!FTPReply.isPositiveCompletion(reply)) {
          throw new IOException("FTP server refused connection.");
        }
        ftp.enterLocalPassiveMode();
        ftp.login("anonymous", "");
        ftp.setFileType(FTP.BINARY_FILE_TYPE);

        long fileSize = -1;
        String remotePath = getFilePath(url);
        FTPFile[] files = ftp.listFiles(remotePath);
        if (files.length == 1 && files[0].isFile()) {
          fileSize = files[0].getSize();
        }
        final long size = fileSize;

        OutputStream output = new FileOutputStream(Configuration.getWebAppDir() + entry.getLocalPath());
        CountingOutputStream cos = new CountingOutputStream(output) {
          private double lastProgress = -1;

          protected void beforeWrite(int n) {
            super.beforeWrite(n);
            double newProgress = ((double) getCount()) / ((double) size) * IProgressUpdater.MAX_PROGRESS;
            if (newProgress - lastProgress >= IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION) {
              lastProgress = newProgress;
              entry.setDownloadProgress(lastProgress);
              bigFileEntryDao.update(entry);
              bigFileEntryDao.commit();
              updater.setProgress(lastProgress);
            }
          }
        };
        ftp.retrieveFile(remotePath, cos);

        entry.setDownloadProgress(IProgressUpdater.MAX_PROGRESS);
        bigFileEntryDao.update(entry);
        bigFileEntryDao.commit();
        updater.setProgress(IProgressUpdater.MAX_PROGRESS);

        output.close();

        ftp.logout();

        return null;
      } finally {
        bigFileEntryDao.commit();
        // close the transaction for this thread
        dbUtils.closeSessionForCurrentThread();
        if (ftp.isConnected()) {
          ftp.disconnect();
        }

      }
    }

  }

  /**
   * Class that describes task of downloading http file.
   *
   * @author Piotr Gawron
   *
   */
  private final class GetHttpFileTask implements Callable<Void> {

    /**
     * Url to the file that we want to download.
     *
     */
    private String url;

    /**
     * Callback listener that will receive information about upload progress.
     *
     */
    private IProgressUpdater updater;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     * @param updater
     *          {@link #updater}
     */
    private GetHttpFileTask(String url, IProgressUpdater updater) {
      this.url = url;
      if (updater != null) {
        this.updater = updater;
      } else {
        this.updater = new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        };
      }
    }

    @Override
    public Void call() throws Exception {
      dbUtils.createSessionForCurrentThread();
      BufferedInputStream in = null;
      CountingOutputStream cos = null;
      try {
        try {
          if (getAbsolutePathForFile(url) != null) {
            logger.warn("File already downloaded. Skipping...");
            return null;
          }
        } catch (FileNotFoundException e) {
          removeFile(url);
        }
        BigFileEntry entry = createEntryForBigFile(url);
        entry.setDownloadThreadId(Thread.currentThread().getId());
        bigFileEntryDao.update(entry);

        final long size = getRemoteHttpFileSize(url);

        OutputStream output = new FileOutputStream(Configuration.getWebAppDir() + entry.getLocalPath());
        cos = new CountingOutputStream(output) {
          private double lastProgress = -1;

          protected void beforeWrite(int n) {
            super.beforeWrite(n);
            double newProgress = ((double) getCount()) / ((double) size) * IProgressUpdater.MAX_PROGRESS;
            if (newProgress - lastProgress >= IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION) {
              lastProgress = newProgress;
              entry.setDownloadProgress(lastProgress);
              bigFileEntryDao.update(entry);
              bigFileEntryDao.commit();
              updater.setProgress(lastProgress);
            }
          }
        };
        URL website = new URL(url);
        in = new BufferedInputStream(website.openStream());

        final byte[] data = new byte[DOWNLOAD_BUFFER_SIZE];
        int count;
        while ((count = in.read(data, 0, DOWNLOAD_BUFFER_SIZE)) != -1) {
          cos.write(data, 0, count);
        }

        entry.setDownloadProgress(IProgressUpdater.MAX_PROGRESS);
        bigFileEntryDao.update(entry);
        bigFileEntryDao.commit();
        updater.setProgress(IProgressUpdater.MAX_PROGRESS);

        output.close();

        return null;
      } catch (Exception e) {
        logger.error(e, e);
        throw e;
      } finally {
        bigFileEntryDao.commit();
        // close the transaction for this thread
        dbUtils.closeSessionForCurrentThread();
        // close opened streams
        if (in != null) {
          in.close();
        }
        if (cos != null) {
          cos.close();
        }
      }
    }
  }

  /**
   * Task that describes updating file from ftp.
   *
   * @author Piotr Gawron
   *
   */
  private final class UpdateFtpFileTask implements Callable<Void> {

    /**
     * Url to file.
     */
    private String url;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     */
    private UpdateFtpFileTask(String url) {
      this.url = url;
    }

    @Override
    public Void call() throws Exception {
      dbUtils.createSessionForCurrentThread();
      FTPClient ftp = ftpClientFactory.createFtpClient();
      try {
        BigFileEntry localEntryCopy = bigFileEntryDao.getByUrl(url);
        localEntryCopy.setDownloadThreadId(Thread.currentThread().getId());
        localEntryCopy.setDownloadProgress(0.0);
        bigFileEntryDao.update(localEntryCopy);

        // remove current version
        new File(Configuration.getWebAppDir() + localEntryCopy.getLocalPath()).delete();

        String server = getDomainName(url);
        ftp.connect(server);
        // After connection attempt, you should check the reply code to
        // verify success.
        int reply = ftp.getReplyCode();

        if (!FTPReply.isPositiveCompletion(reply)) {
          throw new IOException("FTP server refused connection.");
        }
        ftp.enterLocalPassiveMode();
        ftp.login("anonymous", "");
        ftp.setFileType(FTP.BINARY_FILE_TYPE);

        long fileSize = -1;
        String remotePath = getFilePath(url);
        FTPFile[] files = ftp.listFiles(remotePath);
        if (files.length == 1 && files[0].isFile()) {
          fileSize = files[0].getSize();
        }
        final long size = fileSize;

        OutputStream output = new FileOutputStream(localEntryCopy.getLocalPath());
        CountingOutputStream cos = new CountingOutputStream(output) {
          private double lastProgress = -1;

          protected void beforeWrite(int n) {
            super.beforeWrite(n);
            double newProgress = ((double) getCount()) / ((double) size) * IProgressUpdater.MAX_PROGRESS;
            if (newProgress - lastProgress >= IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION) {
              lastProgress = newProgress;
              localEntryCopy.setDownloadProgress(lastProgress);
              bigFileEntryDao.update(localEntryCopy);
            }
          }
        };
        ftp.retrieveFile(remotePath, cos);

        output.close();

        ftp.logout();
        return null;
      } finally {
        dbUtils.closeSessionForCurrentThread();
        if (ftp.isConnected()) {
          ftp.disconnect();
        }
      }
    }

  }

}