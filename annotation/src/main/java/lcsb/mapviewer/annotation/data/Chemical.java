package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.*;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Object representing element obtained from Toxigenomic database.
 * 
 * @author Ayan Rota
 * 
 */
@XmlRootElement
public class Chemical implements Serializable, TargettingStructure {

  /**
   * 
   */
  private static final long serialVersionUID = 3892326511802845188L;

  /**
   * Name.
   */
  private String chemicalName;

  /**
   * Toxigenomic database chemcial ID (MeSH).
   */
  private MiriamData chemicalId;

  /**
   * Toxigenomic database ID (CAS).
   */
  private MiriamData casID;

  /**
   * Evidence evidence either marker/mechanism and/or T therapeutic.
   */
  private ChemicalDirectEvidence directEvidence;

  /**
   * direct Evidence Publication.
   */
  private List<MiriamData> directEvidencePublication = new ArrayList<>();

  /**
   * Genes interacting with the chemical plus publications.
   */
  private List<Target> inferenceNetwork = new ArrayList<>();

  /**
   * Inference score.
   */
  private Float inferenceScore;

  /**
   * Reference count.
   */
  private Integer refScore;

  /**
   * Known synonyms.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * default constructor.
   */
  public Chemical() {
    super();
  }

  /**
   * @param chemicalName
   *          name.
   * @param chemicalId
   *          id.
   * @param casID
   *          cas id.
   * @param directEvidence
   *          string value for direct Evidence.
   * @param inferenceNetwork
   *          inference network.
   * @param directEvidencePublication
   *          publications associated with evidence publications.
   * @param inferenceScore
   *          score.
   * @param refScore
   *          number of references.
   */
  public Chemical(String chemicalName, MiriamData chemicalId, MiriamData casID, ChemicalDirectEvidence directEvidence,
      List<Target> inferenceNetwork,
      List<MiriamData> directEvidencePublication, Float inferenceScore, Integer refScore) {
    super();
    this.chemicalName = chemicalName;
    this.chemicalId = chemicalId;
    this.casID = casID;
    this.directEvidence = directEvidence;
    this.inferenceNetwork = inferenceNetwork;
    this.directEvidencePublication = directEvidencePublication;
    this.inferenceScore = inferenceScore;
    this.refScore = refScore;
  }

  /**
   * @return the chemicalName
   */
  public String getChemicalName() {
    return chemicalName;
  }

  /**
   * @param chemicalName
   *          the chemicalName to set
   */
  public void setChemicalName(String chemicalName) {
    this.chemicalName = chemicalName;
  }

  /**
   * @return the chemicalId
   */
  public MiriamData getChemicalId() {
    return chemicalId;
  }

  /**
   * @param chemicalId
   *          the chemicalId to set
   */
  public void setChemicalId(MiriamData chemicalId) {
    this.chemicalId = chemicalId;
  }

  /**
   * @return the casType
   */
  public MiriamData getCasID() {
    return casID;
  }

  /**
   * @param casID
   *          the casType to set
   */
  public void setCasID(MiriamData casID) {
    this.casID = casID;
  }

  /**
   * @return the directEvidence
   */
  public ChemicalDirectEvidence getDirectEvidence() {
    return directEvidence;
  }

  /**
   * @param directEvidence
   *          the directEvidence to set
   */
  public void setDirectEvidence(ChemicalDirectEvidence directEvidence) {
    this.directEvidence = directEvidence;
  }

  /**
   * @return the inferenceNetwork
   */
  public List<Target> getInferenceNetwork() {
    return inferenceNetwork;
  }

  /**
   * @param inferenceNetwork
   *          the inferenceNetwork to set
   */
  public void setInferenceNetwork(List<Target> inferenceNetwork) {
    this.inferenceNetwork = inferenceNetwork;
  }

  /**
   * @return the inferenceScore
   */
  public Float getInferenceScore() {
    return inferenceScore;
  }

  /**
   * @param inferenceScore
   *          the inferenceScore to set
   */
  public void setInferenceScore(Float inferenceScore) {
    this.inferenceScore = inferenceScore;
  }

  /**
   * @return the refScore
   */
  public Integer getRefScore() {
    return refScore;
  }

  /**
   * @param refScore
   *          the refScore to set
   */
  public void setRefScore(Integer refScore) {
    this.refScore = refScore;
  }

  /**
   * @return the directEvidencePublication
   */
  public List<MiriamData> getDirectEvidencePublication() {
    return directEvidencePublication;
  }

  /**
   * @param directEvidencePublication
   *          the directEvidencePublication to set
   */
  public void setDirectEvidencePublication(List<MiriamData> directEvidencePublication) {
    this.directEvidencePublication = directEvidencePublication;
  }

  /**
   * @return list of synonyms.
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          list of all names that are synonyms.
   */
  public void setSynonyms(List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return list of synonyms as string.
   */
  public String getSynonymsString() {
    return StringUtils.join(synonyms, ",");
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("\nname: " + chemicalName + "\nchemicalId:" + chemicalId + "\ncasID:" + casID);
    result.append("\nsynonyms:" + getSynonymsString());
    result.append("\ninferenceScore:" + inferenceScore + "\nrefScore:" + refScore);
    result.append("\ninferenceNetwork: ");
    for (Target item : getInferenceNetwork()) {
      if (item != null) {
        result.append(item.toString());
      }
    }
    result.append("\ndirectEvidence:" + directEvidence + "\ndirectEvidencePublication:\n");
    for (MiriamData publication : directEvidencePublication) {
      result.append(
          "publication DB: " + publication.getDataType() + ", publication Id: " + publication.getResource() + "\n");
    }

    return result.toString();
  }

  @Override
  public Collection<MiriamData> getSources() {
    List<MiriamData> sources = new ArrayList<>();
    if (getCasID() != null) {
      sources.add(getCasID());
    }
    if (getChemicalId() != null) {
      sources.add(getChemicalId());
    }
    return sources;
  }

  @Override
  public Collection<Target> getTargets() {
    return getInferenceNetwork();
  }

  public void addSynonyms(List<String> synonyms) {
    this.synonyms.addAll(synonyms);
  }

  /**
   * Comparator of the objects by their name.
   *
   * @author Piotr Gawron
   *
   */
  public static class NameComparator implements Comparator<Chemical> {
    /**
     * Default string comparator.
     */
    private StringComparator stringComparator = new StringComparator();

    @Override
    public int compare(Chemical arg0, Chemical arg1) {
      return stringComparator.compare(arg0.getChemicalName(), arg1.getChemicalName());
    }

  }

}
