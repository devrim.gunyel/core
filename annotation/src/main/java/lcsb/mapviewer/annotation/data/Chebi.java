package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.ebi.chebi.webapps.chebiWS.model.*;

/**
 * This class represents <a href="http://www.ebi.ac.uk/chebi/">chebi</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
@XmlRootElement
public class Chebi implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(Chebi.class);

  /**
   * Name of the object.
   */
  private String name;
  /**
   * Chebi identifier.
   */
  private String chebiId;
  /**
   * <a href =
   * "http://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system"
   * >Smiles</a> description.
   */
  private String smiles;

  /**
   * <a href = "http://en.wikipedia.org/wiki/International_Chemical_Identifier"
   * >InChI</a> description.
   */
  private String inchi;

  /**
   * Key for the
   * <a href = "http://en.wikipedia.org/wiki/International_Chemical_Identifier"
   * >InChI</a> description. It's a hashed value of InChI.
   */
  private String inchiKey;

  /**
   * List of synonyms.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * List of elements that stands above this object in chebi ontology.
   */
  private List<ChebiRelation> incomingChebi = new ArrayList<>();

  /**
   * List of elements that stands below this object in chebi ontology.
   */
  private List<ChebiRelation> outgoingChebi = new ArrayList<>();

  /**
   * Default constructor.
   */
  public Chebi() {

  }

  /**
   * Constructor that initilizes object from the data taken from entity acquired
   * from Chebi API.
   * 
   * @param entity
   *          object received from Chebi API representing chebi object
   */
  public Chebi(Entity entity) {
    this.chebiId = entity.getChebiId();
    this.name = entity.getChebiAsciiName().trim();
    this.smiles = entity.getSmiles();
    this.inchi = entity.getInchi();
    this.inchiKey = entity.getInchiKey();
    for (DataItem di : entity.getSynonyms()) {
      synonyms.add(di.getData());
    }
    for (OntologyDataItem item : entity.getOntologyParents()) {
      ChebiRelation relation = new ChebiRelation(item);
      incomingChebi.add(relation);
    }
    for (OntologyDataItem item : entity.getOntologyChildren()) {
      ChebiRelation relation = new ChebiRelation(item);
      outgoingChebi.add(relation);
    }
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the chebiId
   * @see #chebiId
   */
  public String getChebiId() {
    return chebiId;
  }

  /**
   * @param chebiId
   *          the chebiId to set
   * @see #chebiId
   */
  public void setChebiId(String chebiId) {
    this.chebiId = chebiId;
  }

  /**
   * @return the smiles
   * @see #smiles
   */
  public String getSmiles() {
    return smiles;
  }

  /**
   * @param smiles
   *          the smiles to set
   * @see #smiles
   */
  public void setSmiles(String smiles) {
    this.smiles = smiles;
  }

  /**
   * @return the inchi
   * @see #inchi
   */
  public String getInchi() {
    return inchi;
  }

  /**
   * @param inchi
   *          the inchi to set
   * @see #inchi
   */
  public void setInchi(String inchi) {
    this.inchi = inchi;
  }

  /**
   * @return the inchiKey
   * @see #inchiKey
   */
  public String getInchiKey() {
    return inchiKey;
  }

  /**
   * @param inchiKey
   *          the inchiKey to set
   * @see #inchiKey
   */
  public void setInchiKey(String inchiKey) {
    this.inchiKey = inchiKey;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return the incomingChebi
   * @see #incomingChebi
   */
  public List<ChebiRelation> getIncomingChebi() {
    return incomingChebi;
  }

  /**
   * @param incomingChebi
   *          the incomingChebi to set
   * @see #incomingChebi
   */
  public void setIncomingChebi(List<ChebiRelation> incomingChebi) {
    this.incomingChebi = incomingChebi;
  }

  /**
   * @return the outgoingChebi
   * @see #outgoingChebi
   */
  public List<ChebiRelation> getOutgoingChebi() {
    return outgoingChebi;
  }

  /**
   * @param outgoingChebi
   *          the outgoingChebi to set
   * @see #outgoingChebi
   */
  public void setOutgoingChebi(List<ChebiRelation> outgoingChebi) {
    this.outgoingChebi = outgoingChebi;
  }

}
