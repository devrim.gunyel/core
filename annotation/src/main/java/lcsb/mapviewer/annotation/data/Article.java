package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class represents <a href="http://www.ncbi.nlm.nih.gov/pubmed">pubmed</a>
 * article.
 * 
 * @author Piotr Gawron
 * 
 */
@XmlRootElement
public class Article implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Title of the article.
   */
  private String title;

  /**
   * List of authors.
   */
  private List<String> authors = new ArrayList<>();

  /**
   * Journal.
   */
  private String journal;

  /**
   * Year of issue.
   */
  private Integer year;

  /**
   * Url that points to this article.
   */
  private String link;

  /**
   * Pubmed identifier of the article.
   */
  private String id;

  /**
   * How many citiations were made to this article.
   */
  private int citationCount;

  /**
   * 
   * @return {@link #title}
   */
  public String getTitle() {
    return title;
  }

  /**
   * 
   * @param title
   *          new {@link #title} value
   */
  @XmlElement
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * 
   * @return {@link #authors}
   */
  public List<String> getAuthors() {
    return authors;
  }

  /**
   * 
   * @param authors
   *          new {@link #title} value
   */
  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  /**
   * 
   * @return {@link #journal}
   */
  public String getJournal() {
    return journal;
  }

  /**
   * 
   * @param journal
   *          new {@link #journal} value
   */
  @XmlElement
  public void setJournal(String journal) {
    this.journal = journal;
  }

  /**
   * 
   * @return {@link #year}
   */
  public Integer getYear() {
    return year;
  }

  /**
   * 
   * @param year
   *          new {@link #year}
   */
  @XmlElement
  public void setYear(Integer year) {
    this.year = year;
  }

  /**
   * 
   * @return comma separated string with authors
   */
  public String getStringAuthors() {
    String result = "";
    for (String string : authors) {
      if (!result.equalsIgnoreCase("")) {
        result += ", ";
      }
      result += string;
    }
    return result;
  }

  /**
   * 
   * @return {@link #link}
   */
  public String getLink() {
    return link;
  }

  /**
   * 
   * @param link
   *          new {@link #link}
   */
  public void setLink(String link) {
    this.link = link;
  }

  /**
   * 
   * @return {@link #id}
   */
  public String getId() {
    return id;
  }

  /**
   * 
   * @param id
   *          new {@link #id} value
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * 
   * @return {@link #citationCount}
   */
  public int getCitationCount() {
    return citationCount;
  }

  /**
   * 
   * @param citationCount
   *          new {@link #citationCount} value
   */
  public void setCitationCount(int citationCount) {
    this.citationCount = citationCount;
  }
}
