package lcsb.mapviewer.annotation.data;

import java.util.Comparator;

/**
 * Comparator of {@link Article} class based on the number of
 * {@link Article#citationCount citations}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ArticleCitationComparator implements Comparator<Article> {

  @Override
  public int compare(Article o1, Article o2) {
    return -((Integer) o1.getCitationCount()).compareTo(o2.getCitationCount());
  }

}
