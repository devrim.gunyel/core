package lcsb.mapviewer.annotation.services.annotators;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements STITCH annotation which is derived from
 * existing ChEBI annotation.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class StitchAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(StitchAnnotator.class);

  /**
   * Service used for annotation of entities using {@link MiriamType#STITCH
   * STITCH}.
   */
  private ChebiAnnotator chebiAnnotator;

  /**
   * Default constructor.
   */
  public StitchAnnotator(ChebiAnnotator chebiAnnotator) {
    super(StitchAnnotator.class, new Class[] { SimpleMolecule.class }, false);
    this.chebiAnnotator = chebiAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    return chebiAnnotator.getServiceStatus();
  }

  /**
   * Returns main layer of the InchKey, * i.e. everything before first hyphen:
   * WBYWAXJHAXSJNI-VOTSOKGWSA-N -> WBYWAXJHAXSJNI
   * 
   * @param inchiKey
   *          Full inchiKey
   * @return Main layer of the InchiKey
   */
  private String inchiKeyExtractMainLayer(String inchiKey) {
    return inchiKey.replaceFirst("-.*", "");
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.CHEBI)) {
      try {

        Chebi chebi = chebiAnnotator.getChebiElementForChebiId(identifier);
        if (chebi == null) {
          return false;
        }
        String inchiKey = chebi.getInchiKey();
        if (inchiKey == null) {
          return false;
        }

        object.addMiriamData(MiriamType.STITCH, inchiKeyExtractMainLayer(inchiKey));
        return true;
      } catch (ChebiSearchException exception) {
        logger.warn("No ChEBI element retrieved from ChEBI ID: " + identifier);
        return false;
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.STITCH.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.STITCH.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.CHEBI));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.STITCH));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.CHEBI, "CHEBI:35697");
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
