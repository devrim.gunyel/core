package lcsb.mapviewer.annotation.services.annotators;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a mapping to STRING database.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class StringAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * TAIR}. Note that STRING annotation process will annotate only records which
   * have a TAIR ID assigned by a human annotator. Otherwise, it would generate
   * UniProt miriam records also for TAIR IDs generated from, e.g., KEGG
   * annotator, i.e. for homologues and these UniProt IDs would be
   * indistinguishable from the UniProt IDs describing the molecule.
   */
  private TairAnnotator tairAnnotator;

  /**
   * Default constructor.
   */
  @Autowired
  public StringAnnotator(TairAnnotator tairAnnotator) {
    super(StringAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
    this.tairAnnotator = tairAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    return tairAnnotator.getServiceStatus();
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    List<MiriamData> mdUniprots = new ArrayList<>();
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      if (identifier.getAnnotator() == null) {
        mdUniprots.addAll(tairAnnotator.tairToUniprot(identifier));
      }
    } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      mdUniprots.add(identifier);
    } else {
      throw new NotImplementedException();
    }

    List<String> stringIds = new ArrayList<String>();
    for (MiriamData mdUniprot : mdUniprots) {
      MiriamData mdString = uniprotToString(mdUniprot);
      if (mdString != null && stringIds.indexOf(mdString.getResource()) < 0) {
        stringIds.add(mdString.getResource());
        object.addMiriamData(mdString);
      }
    }
    return stringIds.size() > 0;
  }

  @Override
  public String getCommonName() {
    return MiriamType.STRING.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.STRING.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.STRING));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P53350");
  }

  /**
   * Transform UniProt {@link MiriamData} data to STRING {@link MiriamData}.
   *
   * @param UniProt
   *          {@link MiriamData} with UniProt identifier
   * @return {@link MiriamData} with STRING identifier
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing external database
   */
  public MiriamData uniprotToString(MiriamData uniprot) throws AnnotatorException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    return new MiriamData(MiriamType.STRING, uniprot.getResource());
  }
}
