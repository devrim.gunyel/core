package lcsb.mapviewer.annotation.services.annotators;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.common.comparator.StringSetComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerChemical;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * Interface that allows to annotate {@link BioEntity elements} in the system.
 * Different implementation use different resources to perform annotation. They
 * can annotate different types of elements.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class ElementAnnotator extends CachableInterface {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(ElementAnnotator.class);

  /**
   * List of classes that can be annotated by this {@link IElementAnnotator
   * annotator}.
   */
  private final List<Class<? extends BioEntity>> validClasses = new ArrayList<>();
  /**
   * Parameters which this annotator can be provided. Should be set in
   * constructor.
   */
  protected List<AnnotatorParamDefinition> paramsDefs = new ArrayList<>();
  /**
   * Should be this annotator used as a default annotator.
   */
  private boolean isDefault = false;

  /**
   * Default constructor.
   *
   * @param validClasses
   *          list of classes for which this annotator is valid
   * @param isDefault
   *          {@link #isDefault}
   * @param clazz
   *          type that defines this interface
   */
  @SuppressWarnings("unchecked")
  public ElementAnnotator(Class<? extends CachableInterface> clazz, Class<?>[] validClasses, boolean isDefault) {
    super(clazz);
    for (Class<?> validClass : validClasses) {
      if (BioEntity.class.isAssignableFrom(validClass)) {
        addValidClass((Class<? extends BioEntity>) validClass);
      } else {
        throw new InvalidArgumentException("Cannot pass class of type: " + validClass + ". Only classes extending "
            + BioEntity.class + " are accepted.");
      }
    }
    this.isDefault = isDefault;
  }

  /**
   * Annotate element.
   *
   * @param element
   *          object to be annotated
   * @throws AnnotatorException
   *           thrown when there is a problem with annotating not related to data
   */
  public void annotateElement(BioEntity element) throws AnnotatorException {
    annotateElement(element, createAnnotatorData());
  }

  /**
   * Annotate element using parameters.
   *
   * @param bioEntity
   *          object to be annotated
   * @param parameters
   *          list of parameters passed to the annotator which is expected to be
   *          in the same order as its {@link this#parameterDefs}
   * @throws AnnotatorException
   *           thrown when there is a problem with annotating not related to data
   */
  public final void annotateElement(BioEntity bioEntity, AnnotatorData parameters) throws AnnotatorException {
    if (isAnnotatable(bioEntity)) {
      BioEntityProxy proxy = new BioEntityProxy(bioEntity, parameters);
      List<AnnotatorInputParameter> inputParameters = parameters.getInputParameters();
      if (inputParameters.size() == 0) {
        inputParameters = getAvailableInputParameters();
      }
      if (parameters.getOutputParameters().size() == 0) {
        parameters.addAnnotatorParameters(this.getAvailableOuputProperties());
      }
      List<Set<Object>> inputs = getInputsParameters(bioEntity, inputParameters);
      for (Set<Object> inputSet : inputs) {
        boolean annotated = false;
        for (Object object : inputSet) {
          if (object instanceof MiriamData) {
            if (annotateElement(proxy, (MiriamData) object, parameters)) {
              annotated = true;
            }
          } else if (object instanceof String) {
            if (annotateElement(proxy, (String) object, parameters)) {
              annotated = true;
            }
          } else {
            throw new NotImplementedException();
          }
        }
        if (annotated) {
          break;
        }
      }
    }
  }

  public abstract boolean annotateElement(BioEntityProxy element, MiriamData identifier,
      AnnotatorData parameters)
      throws AnnotatorException;

  public boolean annotateElement(BioEntityProxy element, String name, AnnotatorData parameters)
      throws AnnotatorException {
    throw new NotImplementedException();
  }

  /**
   * Returns a list of all classes that can be annotated using this annotator.
   *
   * @return a list of all classes that can be annotated using this annotator
   */
  public List<Class<? extends BioEntity>> getValidClasses() {
    return validClasses;
  };

  /**
   * Returns <code>true</code> if this annotator can annotate the object given in
   * the parameter.
   *
   * @param object
   *          object to be tested if can be annotated
   * @return <code>true</code> if object can be annotated by this annotator,
   *         <code>false</code> otherwise
   */
  public boolean isAnnotatable(BioEntity object) {
    Class<?> clazz = object.getClass();
    for (Class<?> validClazz : getValidClasses()) {
      if (validClazz.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a class to list of classes that can be annotated by the annotator.
   *
   * @param clazz
   *          class to add
   */
  private void addValidClass(Class<? extends BioEntity> clazz) {
    validClasses.add(clazz);
  }

  /**
   * Returns the common name that should be presented to user.
   *
   * @return the common name
   */
  public abstract String getCommonName();

  /**
   * Returns url to the external resource used for annotation.
   *
   * @return url
   */
  public abstract String getUrl();

  /**
   * Provides description of the extraction process for {@link ElementAnnotator}
   * to be used in the front end.
   *
   * @return the description
   */
  public String getDescription() {
    return "";
  }

  /**
   * Returns list with definitions of the parameters available for this annotator.
   *
   * @return the parameters {@link AnnotatorParamDefinition} list
   */
  public Collection<AnnotatorParamDefinition> getParametersDefinitions() {
    return paramsDefs;
  }

  /**
   * Sets definitions of parameters for given annotator.
   *
   * @param paramDefs
   *          definitions to be set
   */
  public void setParametersDefinitions(List<AnnotatorParamDefinition> paramDefs) {
    this.paramsDefs = paramDefs;
  }

  /**
   * Returns <code>true</code> if this annotator can annotate the object of given
   * class type.
   * 
   * @param clazz
   *          class to be tested if can be annotated
   * @return <code>true</code> if class can be annotated by this annotator,
   *         <code>false</code> otherwise
   */
  public boolean isAnnotatable(Class<?> clazz) {
    for (Class<?> clazz2 : validClasses) {
      if (clazz2.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 
   * @return {@link #isDefault}
   */
  public boolean isDefault() {
    return isDefault;
  }

  /**
   * Adds parameter definition to the definitions of parameters for given
   * annotator
   *
   * @param paramDef
   *          parameter definition to be added
   */
  public void addParameterDefinition(AnnotatorParamDefinition paramDef) {
    this.paramsDefs.add(paramDef);
  }

  /**
   * Returns list of available {@link AnnotatorInputParameter}. Order indicates
   * the default order that should be considered when extracting identifier.
   *
   * @return list of available {@link AnnotatorInputParameter} for class
   */
  public abstract List<AnnotatorInputParameter> getAvailableInputParameters();

  /**
   * Returns list of available {@link AnnotatorOutputParameter}. Order indicates
   * the default order that should be considered when extracting identifier.
   *
   * @return list of available {@link AnnotatorOutputParameter} for class
   */
  public abstract List<AnnotatorOutputParameter> getAvailableOuputProperties();

  public List<Set<Object>> getInputsParameters(BioEntity bioEntity, List<AnnotatorInputParameter> inputParameters) {
    List<Set<Object>> result = new ArrayList<>();
    for (AnnotatorInputParameter parameter : inputParameters) {
      Set<Object> inputs = new HashSet<>();
      if (parameter.getField() != null && parameter.getIdentifierType() == null) {
        inputs.add(BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField()));
      } else if (parameter.getField() != null && parameter.getIdentifierType() != null) {
        String value = BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField());
        if (value != null && !value.isEmpty()) {
          inputs.add(new MiriamData(parameter.getIdentifierType(),
              BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField())));
        }
      } else if (parameter.getField() == null && parameter.getIdentifierType() != null) {
        for (MiriamData md : bioEntity.getMiriamData()) {
          if (md.getDataType().equals(parameter.getIdentifierType())) {
            inputs.add(md);
          }
        }
      } else {
        throw new InvalidArgumentException("Input parameter must have either field or identifierType defined");
      }
      result.add(inputs);
    }

    return result;
  }

  public abstract MiriamData getExampleValidAnnotation();

  public List<AnnotatorConfigParameter> getExampleValidParameters() {
    return new ArrayList<>();
  }

  /**
   * Creates default {@link AnnotatorData} for this {@link ElementAnnotator}.
   *
   * @return
   */
  public AnnotatorData createAnnotatorData() {
    AnnotatorData result = new AnnotatorData(this.getClass());
    // by default use everything as input
    result.addAnnotatorParameters(getAvailableInputParameters());
    // and provide all available output
    result.addAnnotatorParameters(getAvailableOuputProperties());
    // and provide all available output
    for (AnnotatorParamDefinition type : getParametersDefinitions()) {
      result.addAnnotatorParameter(type, "");
    }
    return result;
  }

  class BioEntityProxy {
    private BioEntity originalBioEntity;
    private AnnotatorData parameters;

    public BioEntityProxy(BioEntity bioEntity, AnnotatorData parameters) {
      originalBioEntity = bioEntity;
      this.parameters = parameters;

    }

    public void addMiriamData(Collection<MiriamData> annotations) {
      for (MiriamData miriamData : annotations) {
        addMiriamData(miriamData);
      }
    }

    public void addMiriamData(MiriamData miriamData) {
      if (!contains(miriamData)) {
        miriamData.setAnnotator(ElementAnnotator.this.getClass());
        originalBioEntity.addMiriamData(miriamData);
      }
    }

    public boolean contains(MiriamData identifier) {
      MiriamData copy = new MiriamData(identifier);
      copy.setAnnotator(null);
      MiriamData copy2 = new MiriamData(identifier);
      copy.setAnnotator(ElementAnnotator.this.getClass());
      return originalBioEntity.getMiriamData().contains(copy) || originalBioEntity.getMiriamData().contains(copy2);
    }

    public LogMarker getLogMarker(ProjectLogEntryType type) {
      LogMarker result = new LogMarker(type, originalBioEntity);
      result.getEntry().setSource(ElementAnnotator.this.getClass().getSimpleName());
      return result;
    }

    public boolean isElement() {
      return originalBioEntity instanceof Element;
    }

    public void addUniprot(UniprotRecord ur) {
      if (originalBioEntity instanceof Species) {
        Species species = ((Species) originalBioEntity);
        species.getUniprots().add(ur);
        ur.setSpecies(species);
      } else {
        logger.warn("Cannot add uniprot object to: " + originalBioEntity.getClass().getSimpleName());
      }
    }

    public boolean isReaction() {
      return originalBioEntity instanceof Reaction;
    }

    /**
     * Sets synonyms to the element.
     *
     * @param synonyms
     *          new synonyms list
     */
    public void setSynonyms(Collection<String> synonyms) {
      if (canAssignStringSet(synonyms, originalBioEntity.getSynonyms(), BioEntityField.SYNONYMS)) {
        List<String> sortedSynonyms = new ArrayList<>(synonyms);
        Collections.sort(sortedSynonyms);

        originalBioEntity.setSynonyms(sortedSynonyms);
      }
    }

    private boolean canAssignStringSet(Collection<String> newCollection, Collection<String> oldCollection,
        BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (oldCollection == null || oldCollection.size() == 0) {
        return true;
      } else if (newCollection == null || newCollection.size() == 0) {
        return false;
      } else {
        StringSetComparator stringSetComparator = new StringSetComparator();
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();

        set1.addAll(newCollection);
        set2.addAll(oldCollection);

        if (stringSetComparator.compare(set1, set2) != 0) {
          logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT),
              field.getCommonName() + " don't match: \"" + set1 + "\", \"" + set2 + "\"");
          return false;
        }
        return true;
      }
    }

    /**
     * Sets symbol value to the element.
     *
     * @param symbol
     *          new symbol
     */
    public void setSymbol(String symbol) {
      if (canAssign(symbol, originalBioEntity.getSymbol(), BioEntityField.SYMBOL)) {
        originalBioEntity.setSymbol(symbol);
      }
    }

    public void setName(String nam) {
      if (canAssign(nam, originalBioEntity.getName(), BioEntityField.NAME)) {
        originalBioEntity.setName(nam);
      }
    }

    private boolean canAssign(String newValue, String oldValue, BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (oldValue == null || oldValue.trim().equals("") || oldValue.equals(newValue)) {
        return true;
      } else {
        logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT), field.getCommonName() +
            " doesn't match: \"" + newValue + "\", \"" + oldValue + "\"");
        return false;
      }
    }

    private boolean canAssign(String newValue, Integer oldValue, BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (newValue == null || newValue.isEmpty()) {
        return false;
      }
      if (oldValue == null || oldValue == 0 || oldValue.toString().equals(newValue)) {
        return true;
      } else {
        logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT),
            field.getCommonName() + " doesn't match: \"" + newValue + "\", \"" + oldValue + "\"");
        return false;
      }
    }

    public void setFormerSymbols(Collection<String> formerSymbols) {
      if (originalBioEntity instanceof Element) {
        Element element = (Element) originalBioEntity;
        if (canAssignStringSet(formerSymbols, element.getFormerSymbols(), BioEntityField.SYMBOL)) {
          List<String> sorted = new ArrayList<>(formerSymbols);
          Collections.sort(sorted);
          element.setFormerSymbols(sorted);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.SYMBOL.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets name to the element.
     *
     * @param name
     *          new name
     */
    public void setFullName(String name) {
      if (originalBioEntity instanceof Element) {
        Element element = (Element) originalBioEntity;
        if (canAssign(name, element.getFullName(), BioEntityField.FULL_NAME)) {
          element.setFullName(name);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.FULL_NAME.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Adds description to {@link BioEntity#getNotes()}.
     *
     * @param description
     *          value to set
     */
    public void setDescription(String description) {
      if (canAssign(description, "", BioEntityField.DESCRIPTION) && description != null) {
        if (originalBioEntity.getNotes() == null
            || description.toLowerCase().contains(originalBioEntity.getNotes().toLowerCase())) {
          originalBioEntity.setNotes(description);
        } else if (!originalBioEntity.getNotes().toLowerCase().contains(description.toLowerCase())) {
          originalBioEntity.setNotes(originalBioEntity.getNotes() + "\n" + description);
        }
      }
    }

    public void addMiriamData(String generalIdentifier) {
      MiriamData md = null;
      try {
        md = MiriamType.getMiriamByUri(generalIdentifier);
      } catch (InvalidArgumentException e) {
        try {
          md = MiriamType.getMiriamByUri("urn:miriam:" + generalIdentifier);
        } catch (InvalidArgumentException e1) {
          logger.warn(getLogMarker(ProjectLogEntryType.INVALID_IDENTIFIER), "Unknown miriam uri: " + generalIdentifier);
        }
      }
      addMiriamData(md);
    }

    /**
     * Sets {@link CellDesignerChemical#inChI}.
     *
     * @param inchi
     *          value to set
     */
    public void setInchi(String inchi) {
      if (originalBioEntity instanceof Chemical) {
        Chemical element = (Chemical) originalBioEntity;
        if (canAssign(inchi, element.getInChI(), BioEntityField.INCHI)) {
          element.setInChI(inchi);
        }
      } else {
        logger.warn("Cannot assign inchi to " + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link CellDesignerChemical#inChIKey}.
     *
     * @param inchiKey
     *          value to set
     */
    public void setInchiKey(String inchiKey) {
      if (originalBioEntity instanceof Chemical) {
        Chemical element = (Chemical) originalBioEntity;
        if (canAssign(inchiKey, element.getInChIKey(), BioEntityField.INCHI_KEY)) {
          element.setInChIKey(inchiKey);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.INCHI_KEY.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link CellDesignerChemical#smiles}.
     *
     * @param smile
     *          value to set
     */
    public void setSmile(String smile) {
      if (originalBioEntity instanceof Chemical) {
        Chemical element = (Chemical) originalBioEntity;
        if (canAssign(smile, element.getSmiles(), BioEntityField.SMILE)) {
          element.setSmiles(smile);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.SMILE.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    public void addMiriamData(MiriamType miriamType, String resource) {
      addMiriamData(new MiriamData(miriamType, resource));
    }

    /**
     * Sets {@link Species#charge}.
     *
     * @param charge
     *          value to set
     */
    public void setCharge(String charge) {
      if (originalBioEntity instanceof Species) {
        Species element = (Species) originalBioEntity;
        if (canAssign(charge, element.getCharge(), BioEntityField.CHARGE)) {
          element.setCharge(Integer.valueOf(charge));
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.CHARGE.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link Reaction#subsystem}.
     *
     * @param subsystem
     *          value to set
     */
    public void setSubsystem(String subsystem) {
      if (originalBioEntity instanceof Reaction) {
        Reaction element = (Reaction) originalBioEntity;
        if (canAssign(subsystem, element.getSubsystem(), BioEntityField.SUBSYSTEM)) {
          element.setSubsystem(subsystem);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.SUBSYSTEM.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link BioEntity#getFormula()}.
     *
     * @param formula
     *          value to set
     */
    public void setFormula(String formula) {
      if (canAssign(formula, originalBioEntity.getFormula(), BioEntityField.FORMULA)) {
        originalBioEntity.setFormula(formula);
      }
    }

    /**
     * Sets {@link BioEntity#getAbbreviation()}.
     *
     * @param value
     *          value to set
     */
    public void setAbbreviation(String value) {
      if (canAssign(value, originalBioEntity.getAbbreviation(), BioEntityField.ABBREVIATION)) {
        originalBioEntity.setAbbreviation(value);
      }
    }

    /**
     * Sets {@link Reaction#getMechanicalConfidenceScore()}.
     *
     * @param value
     *          value to set
     */
    public void setMechanicalConfidenceScore(String value) {
      if (originalBioEntity instanceof Reaction) {
        Reaction element = (Reaction) originalBioEntity;
        if (canAssign(value, element.getMechanicalConfidenceScore(), BioEntityField.MCS)) {
          element.setMechanicalConfidenceScore(Integer.valueOf(value));
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.MCS.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    public Object getFieldValue(AnnotatorOutputParameter output) {
      if (output.getIdentifierType() != null) {
        Set<MiriamData> collection = new HashSet<>();
        for (MiriamData md : originalBioEntity.getMiriamData()) {
          if (md.getDataType().equals(output.getIdentifierType())) {
            collection.add(md);
          }
        }
        return collection;
      } else {
        switch (output.getField()) {
        case ABBREVIATION:
          return originalBioEntity.getAbbreviation();
        case CHARGE:
          if (originalBioEntity instanceof Species) {
            return ((Species) originalBioEntity).getCharge();
          } else {
            return null;
          }
        case DESCRIPTION:
          return originalBioEntity.getNotes();
        case FORMULA:
          return originalBioEntity.getFormula();
        case FULL_NAME:
          if (originalBioEntity instanceof Element) {
            return ((Element) originalBioEntity).getFullName();
          } else {
            return null;
          }
        case INCHI:
          if (originalBioEntity instanceof Chemical) {
            return ((Chemical) originalBioEntity).getInChI();
          } else {
            return null;
          }
        case INCHI_KEY:
          if (originalBioEntity instanceof Chemical) {
            return ((Chemical) originalBioEntity).getInChIKey();
          } else {
            return null;
          }
        case NAME:
          return originalBioEntity.getName();
        case MCS:
          if (originalBioEntity instanceof Reaction) {
            return ((Reaction) originalBioEntity).getMechanicalConfidenceScore();
          } else {
            return null;
          }
        case PREVIOUS_SYMBOLS:
          if (originalBioEntity instanceof Element) {
            return ((Element) originalBioEntity).getFormerSymbols();
          } else {
            return null;
          }
        case SMILE:
          if (originalBioEntity instanceof Chemical) {
            return ((Chemical) originalBioEntity).getSmiles();
          } else {
            return null;
          }
        case SUBSYSTEM:
          if (originalBioEntity instanceof Reaction) {
            return ((Reaction) originalBioEntity).getSubsystem();
          } else {
            return null;
          }
        case SYMBOL:
          return originalBioEntity.getSymbol();
        case SYNONYMS:
          return originalBioEntity.getSynonyms();
        default:
          throw new NotImplementedException(output.getField() + "");
        }
      }
    }

  }

}
