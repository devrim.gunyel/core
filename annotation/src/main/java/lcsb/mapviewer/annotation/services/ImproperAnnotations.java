package lcsb.mapviewer.annotation.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Marker;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class describing improper annotation. All elements are annotated in the
 * source file, but sometimes those annotations are wrong. The aim of this class
 * is to describe such annotation.
 * 
 * @author Piotr Gawron
 * 
 */
public class ImproperAnnotations implements ProblematicAnnotation {

  /**
   * {@link BioEntity} improperly annotated.
   */
  private BioEntity bioEntity;

  /**
   * Wrong miriam data.
   */
  private List<MiriamData> wrongAnnotations = new ArrayList<>();

  /**
   * Constructor that initializes the data with {@link #bioEntity bioEntity} and
   * list of improper {@link MiriamData}.
   * 
   * @param list
   *          list of improper {@link MiriamData}
   * @param bioEntity
   *          bio entity
   */
  public ImproperAnnotations(BioEntity bioEntity, List<MiriamData> list) {
    if (list.size() == 0) {
      throw new InvalidArgumentException("List of improper annotations cannot be null");
    }
    this.bioEntity = bioEntity;
    wrongAnnotations.addAll(list);
  }

  /**
   * Constructor that initializes the data with {@link #bioEntity bio entity} and
   * improper {@link MiriamData}.
   * 
   * @param miriamData
   *          invalid {@link MiriamData}
   * @param bioEntity
   *          annotated object
   */
  public ImproperAnnotations(BioEntity bioEntity, MiriamData miriamData) {
    this.bioEntity = bioEntity;
    wrongAnnotations.add(miriamData);
  }

  @Override
  public String getMessage() {
    StringBuilder result = new StringBuilder("Invalid annotations found: ");
    for (MiriamData miriamData : wrongAnnotations) {
      result.append(miriamData.getDataType().getCommonName() + "(" + miriamData.getResource() + "), ");
    }
    return result.toString();
  }

  @Override
  public String toString() {
    return getMessage();
  }

  @Override
  public Marker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.INVALID_ANNOTATION, bioEntity);
  }

}
