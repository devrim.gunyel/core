package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.ensembl.org">Ensembl API</a> and annotate elements with
 * information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public class EnsemblAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Version of the rest API that is supported by this annotator.
   */
  static final String SUPPORTED_VERSION = "11.1";

  /**
   * Url address of ensembl restful service.
   */
  private static final String REST_SERVICE_URL = "https://rest.ensembl.org/xrefs/id/";

  /**
   * Suffix that is needed for getting proper result using
   * {@link #REST_SERVICE_URL}.
   */
  private static final String URL_SUFFIX = "?content-type=text/xml";

  /**
   * Url used for retrieving version of the restful API.
   */
  private static final String REST_SERVICE_VERSION_URL = "https://rest.ensembl.org/info/rest?content-type=text/xml";
  /**
   * Standard class logger.
   */
  private final Logger logger = LogManager.getLogger(EnsemblAnnotator.class);

  /**
   * Default constructor.
   */
  public EnsemblAnnotator() {
    super(EnsemblAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {

      GenericProtein proteinAlias = new GenericProtein("mock_id");
      proteinAlias.addMiriamData(getExampleValidAnnotation());
      annotateElement(proteinAlias);

      if (proteinAlias.getFullName() == null || proteinAlias.getFullName().isEmpty()) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }

      String version = getRestfulApiVersion();
      if (!SUPPORTED_VERSION.equals(version)) {
        logger.debug("Version of Ensembl API changed... (new version: " + version + ")");
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }

    } catch (Exception e) {
      logger.error(getCommonName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns current version of restful API.
   *
   * @return version of Ensembl restful API
   * @throws IOException
   *           thrown when there is a problem with accessing API
   * @throws InvalidXmlSchemaException
   *           thrown when the result returned by API is invalid
   */
  private String getRestfulApiVersion() throws IOException, InvalidXmlSchemaException {
    String content = getWebPageContent(REST_SERVICE_VERSION_URL);
    Node xml = XmlParser.getXmlDocumentFromString(content);
    Node response = XmlParser.getNode("opt", xml.getChildNodes());
    Node data = XmlParser.getNode("data", response.getChildNodes());

    String version = XmlParser.getNodeAttr("release", data);
    return version;
  }

  @Override
  public boolean annotateElement(BioEntityProxy element, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.ENSEMBL)) {
      String query = REST_SERVICE_URL + identifier.getResource() + URL_SUFFIX;
      try {
        String content = getWebPageContent(query);
        Node xml = XmlParser.getXmlDocumentFromString(content);
        Node response = XmlParser.getNode("opt", xml.getChildNodes());

        NodeList list = response.getChildNodes();
        Set<String> synonyms = new HashSet<String>();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("data")) {
              String error = XmlParser.getNodeAttr("error", node);
              if (error != null && !error.isEmpty()) {
                logger.warn(element.getLogMarker(ProjectLogEntryType.OTHER), error);
              }
              String dbname = XmlParser.getNodeAttr("dbname", node);

              if ("EntrezGene".equals(dbname)) {
                String entrezId = XmlParser.getNodeAttr("primary_id", node);
                if (entrezId != null && !entrezId.isEmpty()) {
                  element.addMiriamData(MiriamType.ENTREZ, entrezId);
                }
                String symbol = XmlParser.getNodeAttr("display_id", node);
                if (symbol != null) {
                  element.setSymbol(symbol);
                }
                String fullName = XmlParser.getNodeAttr("description", node);
                if (fullName != null) {
                  element.setFullName(fullName);
                }
                NodeList synonymNodeList = node.getChildNodes();

                for (int j = 0; j < synonymNodeList.getLength(); j++) {
                  Node synonymNode = synonymNodeList.item(j);
                  if (synonymNode.getNodeType() == Node.ELEMENT_NODE
                      && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
                    synonyms.add(synonymNode.getTextContent());
                  }
                }
              } else if ("HGNC".equals(dbname)) {
                String hgncId = XmlParser.getNodeAttr("primary_id", node);
                if (hgncId != null && !hgncId.isEmpty()) {
                  hgncId = hgncId.replaceAll("HGNC:", "");
                  element.addMiriamData(MiriamType.HGNC, hgncId);
                }
                String hgncSymbol = XmlParser.getNodeAttr("display_id", node);
                if (hgncSymbol != null && !hgncSymbol.isEmpty()) {
                  element.addMiriamData(MiriamType.HGNC_SYMBOL, hgncId);
                }
                NodeList synonymNodeList = node.getChildNodes();

                for (int j = 0; j < synonymNodeList.getLength(); j++) {
                  Node synonymNode = synonymNodeList.item(j);
                  if (synonymNode.getNodeType() == Node.ELEMENT_NODE
                      && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
                    synonyms.add(synonymNode.getTextContent());
                  }
                }
              }
            }
          }
        }
        if (synonyms.size() > 0) {
          element.setSynonyms(synonyms);
        }
        return true;
      } catch (WrongResponseCodeIOException e) {
        logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Cannot find information for ensembl: " + identifier.getResource());
        return false;
      } catch (Exception e) {
        throw new AnnotatorException(e);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.ENSEMBL.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.ENSEMBL.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.ENSEMBL));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.ENTREZ),
        new AnnotatorOutputParameter(MiriamType.HGNC),
        new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(BioEntityField.SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
