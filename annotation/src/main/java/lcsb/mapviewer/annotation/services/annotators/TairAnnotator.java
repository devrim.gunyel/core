package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a backend to TAIR. Note that TAIR annotation
 * process will annotate only records which have a TAIR ID assigned by a human
 * annotator. Otherwise, it would generate UniProt miriam records also for TAIR
 * IDs generated from, e.g., KEGG annotator, i.e. for homologues and these
 * UniProt IDs would be indistinguishable from the UniProt IDs describing the
 * molecule.
 *
 *
 * @author David Hoksza
 * 
 */
@Service
public class TairAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(TairAnnotator.class);

  /**
   * Default constructor.
   */
  public TairAnnotator() {
    super(TairAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
  }

  /**
   * Pattern used for getting Tair Locus ID symbol from UniProt result page.
   */
  private Pattern getUniprotIdParsePattern(String tairId) {
    return Pattern.compile("(\\w*)\\tlocus:" + tairId);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<MiriamData> collection = tairToUniprot(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      if (collection.size() == 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (!collection.iterator().next().getResource().equalsIgnoreCase("Q9MAN1")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {

    if (identifier.getAnnotator() != null) {
      return false;
    }
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      // UniProt are only obained from TAIR's which were provided by the annotator
      // (otherwise we would get
      // also UniProt IDs for, e.g., homologous genes' TAIR IDs obtained from KEGG
      Collection<MiriamData> collection = tairToUniprot(identifier);
      if (collection.size() > 0) {
        object.addMiriamData(collection);
        return true;
      } else {
        logger.warn("Cannot find uniprot data for id: " + identifier.getResource() + " in the tair page");
        return false;
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return "TAIR";
  }

  @Override
  public String getUrl() {
    return MiriamType.TAIR_LOCUS.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.UNIPROT));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.TAIR_LOCUS, "2200950");
  }

  /**
   * Returns URL to UniProt result containing mapped UniProtIds for submitted TAIR
   * entry.
   *
   * @param tairId
   *          TAIR identifier
   * @return URL to UniProt result page with the TAIR mapping
   */
  private String getUniProtUrl(String tairId) {
    return "https://www.uniprot.org/uniprot/?query=database%3A%28type%3Atair%29+" + tairId
        + "&format=tab&columns=id,database(tair)";
    // return
    // "https://www.uniprot.org/uniprot/?query=database%3A%28type%3Atair+"+tairId+"%29&format=list&columns=id";
  }

  /**
   * Parse UniProt result page which contains list of Uniprot ids mapped to
   * submitted TAIR id. The procedure obtains first {@link MiriamType#UNIPROT} and
   * returns it.
   *
   * @param pageContent
   *          uniprot REST API result page
   * @return uniprot identifier found on the page
   */
  private Collection<MiriamData> parseUniprotUniprot(String pageContent, String tairId) {
    Collection<MiriamData> result = new HashSet<MiriamData>();
    if (!pageContent.isEmpty()) {
      // the query returns a list of possible matches which needs to be pruned
      Matcher m = getUniprotIdParsePattern(tairId).matcher(pageContent);
      if (m.find()) {
        result.add(new MiriamData(MiriamType.UNIPROT, m.group(1)));
      }
    }
    return result;
  }

  /**
   * Transform TAIR identifier into uniprot identifier.
   *
   * Used to use the TAIR record page, but that tends to change and moreover TAIR
   * limits number of accesses from an address. So now the transformation queries
   * directly UniProt from which the mapping can be obtained as well.
   *
   * @param tair
   *          {@link MiriamData} with TAIR identifier
   * @return {@link MiriamData} with UniProt identifier
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing external database
   */
  public Collection<MiriamData> tairToUniprot(MiriamData tair) throws AnnotatorException {
    if (tair == null) {
      return null;
    }

    if (!MiriamType.TAIR_LOCUS.equals(tair.getDataType())) {
      throw new InvalidArgumentException(MiriamType.TAIR_LOCUS + " expected.");
    }

    try {
      String accessUrl = getUniProtUrl(tair.getResource());
      String pageContent = getWebPageContent(accessUrl);
      return parseUniprotUniprot(pageContent, tair.getResource());
    } catch (WrongResponseCodeIOException exception) {
      logger.warn("Wrong reponse code when accessing tair data with id: " + tair.getResource());
      return null;
    } catch (IOException exception) {
      throw new AnnotatorException(exception);
    }
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
