/**
 * This package contains classes that can annotate
 * {@link lcsb.mapviewer.model.map.BioEntity} objects. General annotation
 * interface is defined in
 * {@link lcsb.mapviewer.annotation.services.annotators.ElementAnnotator} class.
 * 
 */
package lcsb.mapviewer.annotation.services.annotators;
