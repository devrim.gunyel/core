package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.*;

import javax.xml.xpath.*;

import org.apache.commons.lang3.SerializationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * This class is a backend to publicly available pubmed API in
 * <a href="https://europepmc.org/RestfulWebService">Europe PubMed Central </a>.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public class PubmedParser extends CachableInterface implements IExternalService {

  /**
   * Pubmed identifier used to check the status of service (functionality from
   * {@link IExternalService}).
   */
  static final int SERVICE_STATUS_PUBMED_ID = 12345;

  /**
   * Prefix used for caching elements with pubmed identifier as a key.
   */
  static final String PUBMED_PREFIX = "pubmed: ";
  /**
   * Version of the remote API that is supported by this connecting class.
   */
  static final String SUPPORTED_VERSION = "6.2";
  static final String API_URL = "https://www.ebi.ac.uk/europepmc/webservices/rest/";
  /**
   * Length of {@link #PUBMED_PREFIX} string.
   */
  private static final int PUBMED_PREFIX_LENGTH = PUBMED_PREFIX.length();
  /**
   * Connector used for accessing data from miriam registry.
   */
  private MiriamConnector miriamConnector;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(PubmedParser.class);

  /**
   * Object that allows to serialize {@link Article} elements into xml string and
   * deserialize xml into {@link Article} objects.
   */
  private XmlSerializer<Article> articleSerializer;

  /**
   * Constructor. Initializes structures used for transforming {@link Article}
   * from/to xml.
   */
  @Autowired
  public PubmedParser(MiriamConnector miriamConnector) {
    super(PubmedParser.class);
    articleSerializer = new XmlSerializer<>(Article.class);
    this.miriamConnector = miriamConnector;
  }

  @Override
  public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
    Object result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(PUBMED_PREFIX)) {
          Integer id = Integer.valueOf(name.substring(PUBMED_PREFIX_LENGTH));
          result = articleSerializer.objectToString(getPubmedArticleById(id));
        } else {
          result = super.refreshCacheQuery(query);
        }
      } else {
        result = super.refreshCacheQuery(query);
      }
    } catch (PubmedSearchException e) {
      throw new SourceNotAvailable(e);
    }

    return result;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  /**
   * Returns article data for given pubmed identifier.
   *
   * @param id
   *          pubmed identifier
   * @return article data
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  public Article getPubmedArticleById(Integer id) throws PubmedSearchException {
    String queryString = "pubmed: " + id;
    Article result = null;
    try {
      result = articleSerializer.xmlToObject(getCacheNode(queryString));
    } catch (SerializationException e) {
      logger.warn("Problem with deserialization of the string: " + queryString);
    }
    if (result != null && result.getTitle() != null) {
      return result;
    } else {
      logger.debug("Pubmed article (id=" + id + ") not found in cache. Accessing WebService...");
    }

    result = new Article();
    try {
      String url = API_URL + "search/resulttype=core&query="
          + java.net.URLEncoder.encode("src:med ext_id:" + id, "UTF-8");

      String content = getWebPageContent(url);

      Document mainDoc = XmlParser.getXmlDocumentFromString(content);
      // Document citationDoc = dBuilder.parse(citationStream);
      mainDoc.getDocumentElement().normalize();
      // citationDoc.getDocumentElement().normalize();
      XPathFactory xPathFactory = XPathFactory.newInstance();
      XPath xpath = xPathFactory.newXPath();
      NodeList nodeList = null;
      // Hit Count
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/citedByCount").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        try {
          result.setCitationCount(Integer.valueOf(nodeList.item(0).getFirstChild().getNodeValue()));
        } catch (Exception e) {
        }
      }
      // Title
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/title").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        result.setTitle(nodeList.item(0).getFirstChild().getNodeValue());
      }
      // Year
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/journalInfo/yearOfPublication")
          .evaluate(mainDoc, XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        try {
          result.setYear(Integer.valueOf(nodeList.item(0).getFirstChild().getNodeValue()));
        } catch (Exception e) {
        }
      }
      // Journal
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/journalInfo/journal/title")
          .evaluate(mainDoc, XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        result.setJournal(nodeList.item(0).getFirstChild().getNodeValue());
      }
      // Authors
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/authorString").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        List<String> authors = new ArrayList<String>();
        String value = nodeList.item(0).getFirstChild().getNodeValue();
        if (value != null && !value.isEmpty()) {
          authors = Arrays.asList(value.split(","));
        }
        result.setAuthors(authors);
      }
      result.setLink(miriamConnector.getUrlString(new MiriamData(MiriamType.PUBMED, id + "")));
      result.setId(id + "");

      if (result.getTitle() != null && !result.getTitle().trim().isEmpty()) {
        try {
          setCacheValue(queryString, articleSerializer.objectToString(result));
        } catch (SerializationException e) {
          logger.warn("Problem with serialization of the string: " + queryString);
        }
      } else {
        result = null;
      }

    } catch (Exception e) {
      throw new PubmedSearchException(e);
    }
    return result;
  }

  public Article getPubmedArticleById(String id) throws PubmedSearchException {
    if (id == null) {
      return null;
    }
    return getPubmedArticleById(Integer.valueOf(id.trim()));
  }

  /**
   * This method return html \< a\ > tag with link for pubmed id (with some
   * additional information).
   *
   * @param id
   *          pubmed identifier
   * @param withTextPrefix
   *          should prefix be added to the tag
   * @return link to webpage with pubmed article
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  public String getHtmlFullLinkForId(Integer id, boolean withTextPrefix) throws PubmedSearchException {
    String result = "";
    Article article = getPubmedArticleById(id);
    result += "<div style=\"float:left;\" title=\"" + article.getTitle() + ", ";
    result += article.getStringAuthors() + ", ";
    result += article.getYear() + ", ";
    result += article.getJournal() + "\">";
    if (withTextPrefix) {
      result += "pubmed: ";
    }
    result += "<a target=\"_blank\" href = \"" + article.getLink() + "\">" + id + "</a>&nbsp;</div>";
    return result;
  }

  /**
   * This method return html \< a\ > tag with link for pubmed id (with some
   * additional information).
   *
   * @param id
   *          pubmed identifier
   * @return link to webpage with pubmed article
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  public String getHtmlFullLinkForId(Integer id) throws PubmedSearchException {
    return getHtmlFullLinkForId(id, true);
  }

  /**
   * Get the summary of the article.
   *
   * @param id
   *          pubmed identifier
   * @return summary of the article.
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  public String getSummary(Integer id) throws PubmedSearchException {
    Article article = getPubmedArticleById(id);
    if (article == null) {
      return null;
    }
    String result = article.getTitle() + ", " + article.getStringAuthors() + ", " + article.getYear() + ", "
        + article.getJournal();
    return result;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("Europe PubMed Central",
        "https://europepmc.org/RestfulWebService");
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Article art = getPubmedArticleById(SERVICE_STATUS_PUBMED_ID);
      if (!getApiVersion().equals(SUPPORTED_VERSION)) {
        logger.debug("New europepmc API version: " + getApiVersion());
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (art == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns current version of the pmc API.
   *
   * @return version of the API to which this class is connected
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing external database
   */
  public String getApiVersion() throws PubmedSearchException {
    try {
      String url = API_URL + "search/resulttype=core&query=src%3Amed+ext_id%3A23644949";

      String content = getWebPageContent(url);

      Document mainDoc = XmlParser.getXmlDocumentFromString(content);
      mainDoc.getDocumentElement().normalize();
      XPathFactory xPathFactory = XPathFactory.newInstance();
      XPath xpath = xPathFactory.newXPath();
      NodeList nodeList = (NodeList) xpath.compile("/responseWrapper/version").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList.getLength() > 0) {
        return nodeList.item(0).getTextContent();
      }
      return null;
    } catch (IOException e) {
      throw new PubmedSearchException("Problem with accessing pubmed db", e);
    } catch (InvalidXmlSchemaException | XPathExpressionException e) {
      throw new PubmedSearchException("Invalid response from pubmed db", e);
    }
  }

  /**
   * Get the summary of the article.
   *
   * @param id
   *          pubmed identifier
   * @return summary of the article.
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  public String getSummary(String id) throws PubmedSearchException {
    return getSummary(Integer.valueOf(id));
  }

  /**
   * @return the miriamConnector
   * @see #miriamConnector
   */
  public MiriamConnector getMiriamConnector() {
    return miriamConnector;
  }

  /**
   * @param miriamConnector
   *          the miriamConnector to set
   * @see #miriamConnector
   */
  public void setMiriamConnector(MiriamConnector miriamConnector) {
    this.miriamConnector = miriamConnector;
  }

  /**
   * @return the articleSerializer
   * @see #articleSerializer
   */
  protected XmlSerializer<Article> getArticleSerializer() {
    return articleSerializer;
  }

  /**
   * @param articleSerializer
   *          the articleSerializer to set
   * @see #articleSerializer
   */
  protected void setArticleSerializer(XmlSerializer<Article> articleSerializer) {
    this.articleSerializer = articleSerializer;
  }

}
