package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a backend to uniprot restfull API.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public class UniprotAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(UniprotAnnotator.class);

  /**
   * Pattern used for finding hgnc symbol from uniprot info page .
   */
  private Pattern uniprotToHgnc = Pattern.compile("GN[\\ ]+Name=([^;\\ ]+)");

  /**
   * Pattern used for finding entrez identifier from uniprot info page .
   */
  private Pattern uniprotToEntrez = Pattern.compile("DR[\\ ]+GeneID;\\ ([^;\\ ]+)");

  /**
   * Pattern used for finding EC symbol from UniProt info page .
   */
  private Pattern uniprotToEC = Pattern
      .compile("EC=((\\d+\\.-\\.-\\.-)|(\\d+\\.\\d+\\.-\\.-)|(\\d+\\.\\d+\\.\\d+\\.-)|(\\d+\\.\\d+\\.\\d+\\.\\d+))");

  /**
   * Pattern used for getting Tair Locus ID symbol from UniProt result page.
   */
  private Pattern uniprotTairLocusToId = Pattern
      .compile("locus:(\\d*)");

  /**
   * Default constructor.
   */
  public UniprotAnnotator() {
    super(UniprotAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      MiriamData md = uniProtToHgnc(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      if (md == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      } else if (!md.getResource().equalsIgnoreCase("LRRK2")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.UNIPROT)) {

      String accessUrl = getUniprotUrl(identifier.getResource());
      try {
        String pageContent = getWebPageContent(accessUrl);
        // check if we have data over there
        if (!pageContent.contains("<title>Error</title>")) {
          Set<MiriamData> annotations = new HashSet<>();
          annotations.addAll(parseHgnc(pageContent));
          annotations.addAll(parseEntrez(pageContent));
          annotations.addAll(parseEC(pageContent));
          object.addMiriamData(MiriamType.UNIPROT, identifier.getResource());
          object.addMiriamData(annotations);
          return true;
        } else {
          // this means that entry with a given uniprot id doesn't exist
          if (object.contains(identifier)) {
            logger.warn(object.getLogMarker(ProjectLogEntryType.INVALID_IDENTIFIER),
                " Invalid uniprot id: " + identifier);
          }
          return false;
        }
      } catch (WrongResponseCodeIOException exception) {
        logger.warn("Cannot find uniprot data for id: " + identifier);
        return false;
      } catch (IOException exception) {
        throw new AnnotatorException(exception);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.UNIPROT.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.UNIPROT.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.UNIPROT),
        new AnnotatorInputParameter(BioEntityField.NAME, MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorOutputParameter(MiriamType.UNIPROT),
        new AnnotatorOutputParameter(MiriamType.EC),
        new AnnotatorOutputParameter(MiriamType.ENTREZ));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "Q5S007");
  }

  /**
   * Returns url to uniprot restfull API about uniprot entry.
   *
   * @param uniprotId
   *          uniprot identifier
   * @return url to uniprot restfull API about uniprot entry
   */
  private String getUniprotUrl(String uniprotId) {
    return "https://www.uniprot.org/uniprot/" + uniprotId + ".txt";
  }

  /**
   * Returns URL to UniProt result containing mapped UniProtIds for submitted TAIR
   * entry.
   *
   * @param tairId
   *          TAIR identifier
   * @return URL to UniProt result page with the TAIR mapping
   */
  private String getUniProtTairLocus2IdUrl(String tairLocusName) {
    return "https://www.uniprot.org/uniprot/?query=database%3A%28type%3Atair+" + tairLocusName
        + "%29&format=tab&columns=id,database(tair)";
  }

  /**
   * Parse uniprot webpage to find information about {@link MiriamType#ENTREZ} and
   * returns them.
   *
   * @param pageContent
   *          uniprot info page
   * @return set of entrez identifiers found on the webpage
   */
  private Collection<MiriamData> parseEntrez(String pageContent) {
    Collection<MiriamData> result = new HashSet<MiriamData>();
    Matcher m = uniprotToEntrez.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.ENTREZ, m.group(1)));
    }
    return result;
  }

  /**
   * Parse uniprot webpage to find information about
   * {@link MiriamType#HGNC_SYMBOL} and returns them.
   *
   * @param pageContent
   *          uniprot info page
   * @return set of entrez identifiers found on the webpage
   */
  private Collection<MiriamData> parseHgnc(String pageContent) {
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = uniprotToHgnc.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.HGNC_SYMBOL, m.group(1)));
    }
    return result;
  }

  /**
   * Parse UniProt webpage to find information about {@link MiriamType#EC}s and
   * returns them.
   *
   * @param pageContent
   *          UniProt info page
   * @return EC found on the page
   */
  private Collection<MiriamData> parseEC(String pageContent) {
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = uniprotToEC.matcher(pageContent);
    while (m.find()) {
      result.add(new MiriamData(MiriamType.EC, m.group(1)));
    }
    return result;
  }

  /**
   * Transform uniprot identifier into hgnc name.
   *
   * @param uniprot
   *          {@link MiriamData} with uniprot identifier
   * @return {@link MiriamData} with hgnc name
   * @throws UniprotSearchException
   *           thrown when there is a problem with accessing external database
   */
  public MiriamData uniProtToHgnc(MiriamData uniprot) throws UniprotSearchException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getUniprotUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      Collection<MiriamData> collection = parseHgnc(pageContent);
      if (collection.size() > 0) {
        return collection.iterator().next();
      } else {
        return null;
      }
    } catch (IOException e) {
      throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
    }

  }

  /**
   * Transform uniprot identifier into EC identifiers.
   *
   * @param uniprot
   *          {@link MiriamData} with uniprot identifier
   * @return ArrayList of {@link MiriamData} with EC codes
   * @throws UniprotSearchException
   *           thrown when there is a problem with accessing external database
   */
  public Collection<MiriamData> uniProtToEC(MiriamData uniprot) throws UniprotSearchException {
    if (uniprot == null) {
      return new HashSet<>();
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getUniprotUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      return parseEC(pageContent);
    } catch (WrongResponseCodeIOException e) {
      return new HashSet<>();
    } catch (IOException e) {
      throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
    }

  }

  /**
   * Transform TAIR Locus name into TAIR Locus identifier. UniProt is used for
   * this task because TAIR i) does not have an API and ii) restricts the number
   * of accesses.
   *
   * @param tairLocus
   *          String with the TAIR Locus name.
   * @return {@link MiriamData} with TAIR Locus ID
   * @throws UniprotSearchException
   *           thrown when there is a problem with accessing external database
   */
  public MiriamData uniprotTairLocusNameToId(String tairLocus) throws UniprotSearchException {
    String accessUrl = getUniProtTairLocus2IdUrl(tairLocus);
    try {
      String pageContent = getWebPageContent(accessUrl);
      Matcher m = uniprotTairLocusToId.matcher(pageContent);
      if (m.find()) {
        return new MiriamData(MiriamType.TAIR_LOCUS, m.group(1));
      } else {
        logger.warn("No TAIR ID found for locus: " + tairLocus);
        return new MiriamData();
      }
    } catch (IOException e) {
      throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
    }
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }
}
