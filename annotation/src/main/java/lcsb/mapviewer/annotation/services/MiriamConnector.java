package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.AnnotationException;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;

/**
 * Class responsible for connection to Miriam DB. It allows to update URI of
 * database being used and retrieve urls for miriam uri.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public final class MiriamConnector extends CachableInterface implements IExternalService {
  /**
   * String used to distinguish cached data for links.
   */
  static final String LINK_DB_PREFIX = "Link: ";
  /**
   * String describing invalid miriam entries that will be put into db (instead of
   * null).
   */
  private static final String INVALID_LINK = "INVALID";
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(MiriamConnector.class);

  /**
   * Default class constructor. Prevent initialization.
   */
  public MiriamConnector() {
    super(MiriamConnector.class);
  }

  @Override
  public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
    Object result = null;
    if (query instanceof String) {
      String name = (String) query;
      if (name.startsWith(LINK_DB_PREFIX)) {
        String tmp = name.substring(LINK_DB_PREFIX.length());
        String[] rows = tmp.split("\n");
        if (rows.length != 2) {
          throw new InvalidArgumentException("Miriam link query is invalid: " + query);
        }
        MiriamType dataType = MiriamType.getTypeByUri(rows[0]);
        String resource = rows[1];
        result = getUrlString(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, dataType, resource));
      } else {
        result = super.refreshCacheQuery(query);
      }
    } else {
      result = super.refreshCacheQuery(query);
    }
    return result;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  /**
   * Returns url to the web page represented by {@link MiriamData} parameter.
   *
   * @param miriamData
   *          miriam data
   * @return url to resource pointed by miriam data
   */
  public String getUrlString(MiriamData miriamData) {
    if (miriamData.getDataType() == null) {
      throw new InvalidArgumentException("Data type cannot be empty.");
    } else if (miriamData.getDataType().getUris().size() == 0) {
      throw new InvalidArgumentException("Url for " + miriamData.getDataType() + " cannot be retreived.");
    }
    String id;
    if (miriamData.getDataType().getNamespace().isEmpty()) {
      id = miriamData.getResource();
    } else {
      id = miriamData.getDataType().getNamespace() + ":" + miriamData.getResource();
    }

    String query = LINK_DB_PREFIX + miriamData.getDataType().getUris().get(0) + "\n" + miriamData.getResource();
    String result = getCacheValue(query);
    if (result != null) {
      if (INVALID_LINK.equals(result)) {
        return null;
      }
      return result;
    }
    try {
      result = getRedirectURL("https://identifiers.org/" + id);

      if (result != null) {
        setCacheValue(query, result);
        return result;
      } else {
        logger.warn("Cannot find url for miriam: " + miriamData);
        // if url cannot be found then mark miriam data as invalid for one day
        setCacheValue(query, INVALID_LINK, 1);
        return null;
      }
    } catch (Exception e) {
      throw new AnnotationException("Problem with accessing information about miriam id: " + miriamData, e);
    }
  }

  private String getRedirectURL(String address) throws IOException {
    URL url = new URL(address);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setInstanceFollowRedirects(false);
    con.setRequestProperty("User-Agent", "minerva-framework");
    con.connect();
    int resCode = con.getResponseCode();
    if (resCode == HttpURLConnection.HTTP_SEE_OTHER
        || resCode == HttpURLConnection.HTTP_MOVED_PERM
        || resCode == HttpURLConnection.HTTP_MOVED_TEMP) {
      String location = con.getHeaderField("Location");
      if (location.startsWith("/")) {
        location = url.getProtocol() + "://" + url.getHost() + location;
      } else if (!location.startsWith("http")) {
        location = address.substring(0, address.lastIndexOf("/") + 1) + location;
      }
      return location;
    }
    if (resCode == HttpStatus.SC_NOT_FOUND || HttpStatus.SC_BAD_REQUEST == resCode) {
      return null;
    }
    return address;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("MIRIAM Registry", "https://www.ebi.ac.uk/miriam/main/");
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      String url = getUrlString(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "3453"));
      status.setStatus(ExternalServiceStatusType.OK);
      if (url == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Check if identifier can be transformed into {@link MiriamData}.
   *
   * @param string
   *          identifier in the format NAME:IDENTIFIER. Where NAME is the name
   *          from {@link MiriamType#getCommonName()} and IDENTIFIER is resource
   *          identifier.
   * @return <code>true</code> if identifier can be transformed into
   *         {@link MiriamData}
   */
  public boolean isValidIdentifier(String string) {
    try {
      MiriamType.getMiriamDataFromIdentifier(string);
      return true;
    } catch (InvalidArgumentException e) {
      return false;
    }
  }

  /**
   * Checks if {@link MiriamType} is valid.
   *
   * @param type
   *          type to be checked
   * @return <code>true</code> if {@link MiriamType} is valid, <code>false</code>
   *         otherwise
   */
  public boolean isValidMiriamType(MiriamType type) {
    if (type.getNamespace() == null || type.getExampleIdentifier() == null) {
      return false;
    }
    return getUrlString(new MiriamData(type, type.getExampleIdentifier())) != null;
  }

  /**
   * Returns uri to miriam resource.
   *
   * @param md
   *          {@link MiriamData} object for which uri should be returned
   * @return uri to miriam resource
   */
  public String miriamDataToUri(MiriamData md) {
    return md.getDataType().getUris().get(0) + ":" + md.getResource();
  }
}
