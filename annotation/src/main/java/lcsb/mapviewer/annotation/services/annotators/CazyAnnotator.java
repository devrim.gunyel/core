package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a backend to CAZy.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class CazyAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(CazyAnnotator.class);

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * tair}.
   */
  private TairAnnotator tairAnnotator;

  /**
   * Pattern used for finding UniProt symbol from TAIR info page .
   */
  private Pattern cazyIdMatcher = Pattern.compile("\\/((GT|GH|PL|CE|CBM)\\d+(\\_\\d+)?)\\.html");

  @Autowired
  public CazyAnnotator(TairAnnotator tairAnnotator) {
    super(CazyAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
    this.tairAnnotator = tairAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      MiriamData md = uniprotToCazy(getExampleValidAnnotation(), new LogMarker(ProjectLogEntryType.OTHER, new GenericProtein("")));

      status.setStatus(ExternalServiceStatusType.OK);
      if (md == null || !md.getResource().equalsIgnoreCase("GH5_7")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    List<MiriamData> mdUniprots = new ArrayList<>();
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      mdUniprots.addAll(tairAnnotator.tairToUniprot(identifier));
    } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      mdUniprots.add(identifier);
    } else {
      throw new NotImplementedException();
    }
    List<String> cazyIds = new ArrayList<String>();
    for (MiriamData mdUniprot : mdUniprots) {
      MiriamData mdCazy = uniprotToCazy(mdUniprot, object.getLogMarker(ProjectLogEntryType.OTHER));
      if (mdCazy != null && cazyIds.indexOf(mdCazy.getResource()) == -1) {
        cazyIds.add(mdCazy.getResource());
        object.addMiriamData(mdCazy);
      }
    }
    return cazyIds.size() > 0;
  }

  @Override
  public String getCommonName() {
    return MiriamType.CAZY.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.CAZY.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.CAZY));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "Q9SG95");
  }

  /**
   * Returns URL to TAIR page about TAIR entry.
   *
   * @param uniProtId
   *          UniProt identifier
   * @return URL to CAZY UniProt accession search result page
   */
  private String getCazyUrl(String uniProtId) {
    return "http://www.cazy.org/search?page=recherche&recherche=" + uniProtId + "&tag=10";
  }

  /**
   * Parse CAZy webpage to find information about {@link MiriamType#CAZY} and
   * returns them.
   *
   * @param pageContent
   *          CAZy info page
   * @return CAZy family identifier found on the page
   */
  private Collection<MiriamData> parseCazy(String pageContent) {
    Collection<MiriamData> result = new HashSet<MiriamData>();
    Matcher m = cazyIdMatcher.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.CAZY, m.group(1)));
    }
    return result;
  }

  /**
   * Transform UniProt identifier to CAZy identifier.
   *
   * @param UniProt
   *          {@link MiriamData} with UniProt identifier
   * @return {@link MiriamData} with CAZy identifier
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing external database
   */
  public MiriamData uniprotToCazy(MiriamData uniprot, LogMarker marker) throws AnnotatorException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getCazyUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      Collection<MiriamData> collection = parseCazy(pageContent);
      if (collection.size() > 0) {
        return collection.iterator().next();
      } else {
        marker.getEntry().setType(ProjectLogEntryType.INVALID_IDENTIFIER);
        logger.warn(marker, "Cannot find CAZy data for UniProt id: " + uniprot.getResource());
        return null;
      }
    } catch (WrongResponseCodeIOException exception) {
      marker.getEntry().setType(ProjectLogEntryType.OTHER);
      logger.warn(marker, "Wrong response code when retrieving CAZy data for UniProt id: " + uniprot.getResource());
      return null;
    } catch (IOException exception) {
      throw new AnnotatorException(exception);
    }
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
