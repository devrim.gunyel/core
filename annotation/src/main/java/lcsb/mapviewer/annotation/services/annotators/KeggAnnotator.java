package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements KEGG annotator which extract from KEGG PUBMED
 * records and homologous information about homologous genes in different
 * organisms based on parameterization of the annotator.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class KeggAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(KeggAnnotator.class);

  /**
   * Pattern used for finding PUBMED IDs in KEGG page.
   */
  private Pattern pubmedMatcher = Pattern.compile("\\[PMID:(\\d+)\\]");

  /**
   * Pattern used for finding ATH orghologs in KEGG GENE sectionpage.
   */
  private Pattern athOrthologMatcher = Pattern.compile(" *ATH: (.*)");

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * TAIR}.
   */
  private TairAnnotator tairAnnotator;

  /**
   * Service used for retrieving EC numbers based on {@link MiriamType#UNIPROT}
   */
  private UniprotAnnotator uniprotAnnotator;

  /**
   * Constructor.
   */
  public KeggAnnotator(TairAnnotator tairAnnotator, UniprotAnnotator uniprotAnnotator) {
    super(KeggAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
    this.addParameterDefinition(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER);
    this.tairAnnotator = tairAnnotator;
    this.uniprotAnnotator = uniprotAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Species protein = new GenericProtein("id");
      protein.addMiriamData(getExampleValidAnnotation());
      annotateElement(protein);

      status.setStatus(ExternalServiceStatusType.OK);

      Set<String> pmids = new HashSet<String>();
      pmids.add("30409");
      pmids.add("3134");
      int cntMatches = 0;
      for (MiriamData md : protein.getMiriamData()) {
        if (pmids.contains(md.getResource())) {
          cntMatches++;
        }
      }

      if (cntMatches != 2) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    Set<MiriamData> ecIdentifiers = new HashSet<>();
    try {
      if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
        for (MiriamData uniprotId : tairAnnotator.tairToUniprot(identifier)) {
          ecIdentifiers.addAll(uniprotAnnotator.uniProtToEC(uniprotId));
        }
      } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
        ecIdentifiers.addAll(uniprotAnnotator.uniProtToEC(identifier));
      } else if (identifier.getDataType().equals(MiriamType.EC)) {
        ecIdentifiers.add(identifier);
      } else {
        throw new NotImplementedException();
      }
    } catch (UniprotSearchException e) {
      logger.warn(e, e);
      return false;
    }
    if (ecIdentifiers.size() == 0) {
      logger.warn("Cannot find kegg data for id: " + identifier);
      return false;
    }

    // annotate from KEGG
    Set<MiriamData> annotations = new HashSet<>();
    for (MiriamData ecIdentifier : ecIdentifiers) {
      String accessUrl = getKeggUrl(ecIdentifier.getResource());

      try {
        String pageContent = getWebPageContent(accessUrl);
        annotations.addAll(parseKegg(pageContent, parameters));

      } catch (WrongResponseCodeIOException exception) {
        logger.warn("Cannot find kegg data for id: " + identifier);
      } catch (IOException exception) {
        throw new AnnotatorException(exception);
      } catch (UniprotSearchException e) {
        logger.warn(e, e);
        return false;
      }
    }
    object.addMiriamData(annotations);
    return annotations.size() > 0;
  }

  @Override
  public String getCommonName() {
    return "KEGG";
  }

  @Override
  public String getUrl() {
    return "http://www.genome.jp/kegg/";
  }

  @Override
  public String getDescription() {
    return "Annotations extracted from KEGG ENZYME Database based on species EC numbers. "
        + "Annotation include relevant publications and homologous genes for given EC numbers.";
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT),
        new AnnotatorInputParameter(MiriamType.EC));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.PUBMED),
        new AnnotatorOutputParameter(MiriamType.TAIR_LOCUS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.EC, "3.1.2.14");
  }

  @Override
  public List<AnnotatorConfigParameter> getExampleValidParameters() {
    return Arrays.asList(new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "ATH"));
  }

  /**
   * Returns url to KEGG restful API about enzyme classification.
   *
   * @param ecId
   *          enzyme classification
   * @return url to KEGG restful API about given EC
   */
  private String getKeggUrl(String ecId) {
    return "http://rest.kegg.jp/get/" + ecId;
  }

  /**
   * Parse KEGG webpage to find information about {@link MiriamType#PUBMED}s and
   * returns them.
   *
   * @param pageContent
   *          Kegg page
   * @param params
   *          List of {@link UserAnnotatorsParam} to be used for parameterization.
   *          Should contain only at most one record with space separated KEGG
   *          organisms names. If the value has not been set by the user, null
   *          will be passed.
   * @return {@link MiriamType#PUBMED}s found on the page
   * @throws UniprotSearchException
   */
  private Collection<MiriamData> parseKegg(String pageContent, AnnotatorData params) throws UniprotSearchException {

    // Retrieve Pubmeds
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = pubmedMatcher.matcher(pageContent);
    while (m.find()) {
      result.add(new MiriamData(MiriamType.PUBMED, m.group(1)));
    }

    String organismParameter = params.getValue(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER);
    // Retrieve homologous organisms based on parameterization
    if (organismParameter != null) {
      String[] keggOrgnismCodes = organismParameter.trim().split(" +");
      for (String code : keggOrgnismCodes) {
        if (!code.equalsIgnoreCase("ATH")) {
          logger.warn("KEGG annotator currently supports only ATH (" + code + " passed)");
        } else {
          m = athOrthologMatcher.matcher(pageContent);
          if (m.find()) {
            String[] tairLocusNames = m.group(1).trim().split(" ");
            for (String tairLocusName : tairLocusNames) {
              tairLocusName = tairLocusName.split("\\(")[0]; // some codes are in the form AT1G08510(FATB)
              MiriamData md = uniprotAnnotator.uniprotTairLocusNameToId(tairLocusName);
              if (!md.equals(new MiriamData())) {
                result.add(md);
              }
            }
          }
        }
      }
    }

    return result;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }
}
