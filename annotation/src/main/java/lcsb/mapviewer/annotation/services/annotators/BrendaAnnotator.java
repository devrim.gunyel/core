package lcsb.mapviewer.annotation.services.annotators;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a backend to Brenda enzyme database.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class BrendaAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(BrendaAnnotator.class);

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * TAIR}.
   */
  private TairAnnotator tairAnnotator;

  private UniprotAnnotator uniprotAnnotator;

  @Autowired
  public BrendaAnnotator(TairAnnotator tairAnnotator, UniprotAnnotator uniprotAnnotator) {
    super(BrendaAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
    this.tairAnnotator = tairAnnotator;
    this.uniprotAnnotator = uniprotAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<MiriamData> mds = new UniprotAnnotator().uniProtToEC(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      List<String> ecs = new ArrayList<>();
      if (mds != null) {
        for (MiriamData md : mds) {
          ecs.add(md.getResource());
        }
      }
      if (mds == null || mds.size() != 2 || ecs.indexOf("2.6.1.1") < 0 || ecs.indexOf("2.6.1.7") < 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    List<MiriamData> mdUniprots = new ArrayList<>();
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      mdUniprots.addAll(tairAnnotator.tairToUniprot(identifier));
    } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      mdUniprots.add(identifier);
    } else {
      throw new NotImplementedException();
    }

    List<String> ecIds = new ArrayList<>();
    for (MiriamData mdUniprot : mdUniprots) {
      try {
        Collection<MiriamData> mdECs = uniprotAnnotator.uniProtToEC(mdUniprot);
        for (MiriamData mdEC : mdECs) {
          if (ecIds.indexOf(mdEC.getResource()) == -1) {
            ecIds.add(mdEC.getResource());
            object.addMiriamData(MiriamType.BRENDA, mdEC.getResource());
          }
        }
      } catch (UniprotSearchException e) {
        logger.warn(object.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Cannot find EC data for UniProt id: " + mdUniprot.getResource());
      }

    }
    return ecIds.size() > 0;
  }

  @Override
  public String getCommonName() {
    return MiriamType.BRENDA.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.BRENDA.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.BRENDA));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P12345");
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
