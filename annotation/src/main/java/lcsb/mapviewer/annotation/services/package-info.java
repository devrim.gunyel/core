/**
 * This package contains classes that access different type of annotation data
 * from different type of resources.
 */
package lcsb.mapviewer.annotation.services;
