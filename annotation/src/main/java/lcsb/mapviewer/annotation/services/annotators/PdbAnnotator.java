package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This is a class that implements a backend to the EBI's PDB SIFTS REST API
 * mapping.
 * 
 * @author David Hoksza
 * 
 */
@Service
public class PdbAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(PdbAnnotator.class);

  /**
   * Default constructor.
   */
  @Autowired
  public PdbAnnotator() {
    super(PdbAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, false);
  }

  /**
   * Tests if given input string is a valid JSON document.
   *
   * @param json
   *          Input document as a string.
   * @return True or false dependent on whether the input string is a valid JSON
   *         document
   */
  public static boolean isJson(String json) {
    Gson gson = new Gson();
    try {
      gson.fromJson(json, Object.class);
      return true;
    } catch (com.google.gson.JsonSyntaxException ex) {
      return false;
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<Structure> structures = uniProtToPdb(getExampleValidAnnotation());

      if (structures.size() > 0) {
        if (structures.iterator().next().getPdbId() != null) { // TODO - is this id?
          status.setStatus(ExternalServiceStatusType.OK);
        } else {
          status.setStatus(ExternalServiceStatusType.CHANGED);
        }
      } else {
        status.setStatus(ExternalServiceStatusType.DOWN);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  public Set<MiriamData> getUnitProts(BioEntity bioEntity) {
    HashSet<MiriamData> mds = new HashSet<>();
    for (MiriamData md : bioEntity.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        mds.add(md);
      }
    }
    return mds;
  }

  @Override
  public boolean annotateElement(BioEntityProxy bioEntity, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      try {
        Collection<Structure> structures = uniProtToPdb(identifier);
        if (structures.size() == 0) {
          logger.warn(bioEntity.getLogMarker(ProjectLogEntryType.OTHER),
              " No PDB mapping for UniProt ID: " + identifier.getResource());
          return false;
        } else {
          // add the annotations to the set of annotation irrespective on
          // which uniprot record the structures belong to (since one molecule
          // can have multiple uniprot records associated, but annotations do
          // do not have the concept of hierarchy or complex data types)
          Set<MiriamData> annotations = new HashSet<MiriamData>();
          for (Structure s : structures) {
            annotations.add(new MiriamData(MiriamType.PDB, s.getPdbId()));
          }
          bioEntity.addMiriamData(annotations);

          // insert the full information directly into species, .i.e.
          // create new uniprot record which includes the mapped structures
          // and add it to the species (bioentity)
          UniprotRecord ur = new UniprotRecord();
          ur.setUniprotId(identifier.getResource());
          for (Structure s : structures) {
            s.setUniprot(ur);
          }
          ur.addStructures(structures);
          bioEntity.addUniprot(ur);
          return true;
        }
      } catch (WrongResponseCodeIOException exception) {
        logger.warn("Not found PDB mapping (wrong response code) for UniProt ID: " + identifier.getResource());
        return false;
      } catch (IOException exception) {
        throw new AnnotatorException(exception);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.PDB.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.PDB.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.PDB));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P29373");
  }

  /**
   * Returns url to JSON with best mapping PDB entries given the UniProt entry.
   *
   * @param uniprotId
   *          uniprot identifier
   * @return url with best mapping PDB entries to the UniProt entry
   */
  private String getPdbMappingUrl(String uniprotId) {
    return "https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/" + uniprotId;
  }

  /**
   * Parse UniProt-to-PDB mapping JSON file. {@link MiriamType#PDB} and returns
   * them.
   *
   * @param pageContentJson
   *          JSON file with the UniProt to PDB mapping
   * @return set of PDB identifiers found on the webpage
   */
  private Collection<Structure> processMappingData(String pageContentJson) {
    Collection<Structure> result = new HashSet<Structure>();
    Gson g = new Gson();
    java.lang.reflect.Type t = new TypeToken<Map<String, List<PdbBestMappingEntry>>>() {
    }.getType();
    Map<String, List<PdbBestMappingEntry>> m = g.fromJson(pageContentJson, t);
    if (m != null) {
      for (String key : m.keySet()) {
        for (PdbBestMappingEntry e : m.get(key)) {
          if (e != null) {
            result.add(e.convertToStructure());
            // result.add(new MiriamData(MiriamType.PDB, e.pdb_id));
          }
        }
      }
    }

    return result;
  }

  /**
   * Transform UniProt identifier into PDB IDs.
   *
   * @param uniprot
   *          {@link MiriamData} with UniProt identifier
   * @return JSON String with mapping. thrown when there is a problem with
   *         accessing external database
   */
  public Collection<Structure> uniProtToPdb(MiriamData uniprot) throws IOException {
    if (uniprot == null) {
      return new ArrayList<>();
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getPdbMappingUrl(uniprot.getResource());
    String json = getWebPageContent(accessUrl);

    return isJson(json) ? processMappingData(json) : new ArrayList<>();
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
