package lcsb.mapviewer.annotation.services;

import org.apache.logging.log4j.Marker;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Defines annotation problem with element when there are no annotations for the
 * element.
 * 
 * @author Piotr Gawron
 *
 */
public class MissingAnnotation implements ProblematicAnnotation {

  /**
   * BioEntity improperly annotated.
   */
  private BioEntity bioEntity;

  /**
   * Constructor that initializes the data with {@link #bioEntity bioEntity} .
   * 
   * @param bioEntity
   *          bioEntity that misses annotation
   */
  public MissingAnnotation(BioEntity bioEntity) {
    this.bioEntity = bioEntity;
  }

  @Override
  public String getMessage() {
    return "Missing annotations.";
  }

  @Override
  public String toString() {
    return getMessage();
  }

  @Override
  public Marker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.MISSING_ANNOTATION, bioEntity);
  }

}
