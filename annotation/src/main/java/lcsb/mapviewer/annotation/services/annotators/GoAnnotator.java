package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.user.annotator.*;

/**
 * This class is a backend to Gene Ontology API.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public class GoAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Prefix string used for marking the query in database as query by go term.
   */
  static final String GO_TERM_CACHE_PREFIX = "GO_TERM: ";

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger(GoAnnotator.class);

  /**
   * Connector used for accessing data from miriam registry.
   */
  private MiriamConnector mc;

  /**
   * Object that allows to serialize {@link Go} elements into xml string and
   * deserialize xml into {@link Go} objects.
   */
  private XmlSerializer<Go> goSerializer;

  /**
   * Constructor. Initializes structures used for transforming {@link Go} from/to
   * xml.
   */
  @Autowired
  public GoAnnotator(MiriamConnector mc) {
    super(GoAnnotator.class, new Class[] { Phenotype.class, Compartment.class, Complex.class }, true);
    goSerializer = new XmlSerializer<>(Go.class);
    this.mc = mc;
  }

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(GO_TERM_CACHE_PREFIX)) {
          String term = name.substring(GO_TERM_CACHE_PREFIX.length());
          MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, term);
          result = goSerializer.objectToString(getGoElement(md));
        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with query: " + query);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (IOException | GoSearchException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  @Override
  public boolean annotateElement(BioEntityProxy object, MiriamData identifier, AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.GO)) {
      try {
        Go go = getGoElement(identifier);
        if (go != null) {
          object.setFullName(go.getCommonName());
          object.setDescription(go.getDescription());
          return true;
        }
        return false;
      } catch (GoSearchException e) {
        throw new AnnotatorException(e);
      }
    } else {
      throw new NotImplementedException();
    }

  }

  @Override
  public String getCommonName() {
    return MiriamType.GO.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.GO.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.GO));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(BioEntityField.DESCRIPTION));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.GO, "GO:0046902");
  }

  /**
   * Returns go entry from the Gene Ontology database for the goTerm (identifier).
   *
   * @param md
   *          {@link MiriamData} object referencing to entry in GO database
   * @return entry in Gene Ontology database
   * @throws GoSearchException
   *           thrown when there is a problem with accessing data in go database
   */
  protected Go getGoElement(MiriamData md) throws GoSearchException {
    Go result = goSerializer.xmlToObject(getCacheNode(GO_TERM_CACHE_PREFIX + md.getResource()));

    if (result != null) {
      return result;
    } else {
      result = new Go();
    }

    String accessUrl = "https://www.ebi.ac.uk/QuickGO/services/ontology/go/terms/" + md.getResource();
    try {
      String page = getWebPageContent(accessUrl);

      Gson gson = new Gson();

      Map<?, ?> gsonObject = (Map<?, ?>) gson.fromJson(page, HashMap.class);
      Double hits = (Double) gsonObject.get("numberOfHits");
      if (hits < 1) {
        return null;
      }
      List<?> objects = (List<?>) gsonObject.get("results");

      Map<?, ?> object = (Map<?, ?>) objects.get(0);
      result.setGoTerm((String) object.get("id"));
      result.setCommonName((String) object.get("name"));

      Map<?, ?> descr = (Map<?, ?>) object.get("definition");
      if (descr != null) {
        result.setDescription((String) descr.get("text"));
      }

      return result;
    } catch (Exception e) {
      throw new GoSearchException("Problem with accesing go database", e);
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Compartment compartment = new Compartment("some_id");
      compartment.addMiriamData(getExampleValidAnnotation());
      annotateElement(compartment);

      if (compartment.getFullName() == null || compartment.getFullName().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (compartment.getNotes() == null || compartment.getNotes().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (Exception e) {
      logger.error("GeneOntology is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * @return the mc
   * @see #mc
   */
  public MiriamConnector getMc() {
    return mc;
  }

  /**
   * @param mc
   *          the mc to set
   * @see #mc
   */
  public void setMc(MiriamConnector mc) {
    this.mc = mc;
  }

}
