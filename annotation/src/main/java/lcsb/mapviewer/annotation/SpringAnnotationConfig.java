package lcsb.mapviewer.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "lcsb.mapviewer.annotation" })
public class SpringAnnotationConfig {
}
