package lcsb.mapviewer.annotation;

import org.springframework.context.annotation.*;

import lcsb.mapviewer.annotation.cache.MockCacheInterface;
import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringAnnotationConfig.class, SpringPersistConfig.class })
public class SpringAnnotationTestConfig {

  @Bean
  public MockCacheInterface mockCacheInterface() {
    return new MockCacheInterface();
  }

}
