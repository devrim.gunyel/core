package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class MiRNATest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    List<Target> inferenceNetwork = new ArrayList<>();
    MiRNA mirna = new MiRNA();

    mirna.setTargets(inferenceNetwork);
    assertEquals(inferenceNetwork, mirna.getTargets());
  }

  @Test
  public void testToString() {
    MiRNA mirna = new MiRNA();
    mirna.addTarget(new Target());
    String str = mirna.toString();
    assertNotNull(str);
  }

}
