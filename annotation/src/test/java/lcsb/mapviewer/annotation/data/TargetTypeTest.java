package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertNotNull;

import org.junit.*;

public class TargetTypeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValues() {
    for (TargetType type : TargetType.values()) {
      assertNotNull(type.getCommonName());
      assertNotNull(TargetType.valueOf(type.toString()));
    }
  }

}
