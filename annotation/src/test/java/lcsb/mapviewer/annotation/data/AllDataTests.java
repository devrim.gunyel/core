package lcsb.mapviewer.annotation.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ArticleCitationComparatorTest.class,
    ArticleTest.class,
    ChebiRelationTest.class,
    ChebiTest.class,
    ChemicalTest.class,
    ChemicalDirectEvidenceTest.class,
    DrugTest.class,
    EntrezDataTest.class,
    GoTest.class,
    MeSHTest.class,
    MiRNATest.class,
    TargetTest.class,
    TargetTypeTest.class,
})
public class AllDataTests {

}
