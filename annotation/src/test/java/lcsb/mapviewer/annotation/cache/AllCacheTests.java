package lcsb.mapviewer.annotation.cache;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ApplicationLevelCacheTest.class,
    BigFileCacheTest.class,
    CachableInterfaceTest.class,
    GeneralCacheTest.class,
    GeneralCacheWithExclusionTest.class,
    PermanentDatabaseLevelCacheTest.class,
    SourceNotAvailableTest.class,
    WebPageDownloaderTest.class,
})
public class AllCacheTests {

}
