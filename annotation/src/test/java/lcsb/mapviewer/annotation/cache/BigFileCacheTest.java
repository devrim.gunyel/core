package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.*;
import java.net.URISyntaxException;
import java.util.concurrent.*;

import org.apache.commons.net.ftp.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.persist.dao.cache.BigFileEntryDao;

public class BigFileCacheTest extends AnnotationTestFunctions {

  Logger logger = LogManager.getLogger(BigFileCacheTest.class);

  String ftpUrl = "ftp://ftp.informatik.rwth-aachen.de/README";
  String ftpUrlPartContent = "Welcome";

  String httpUrl = "https://www.google.pl/humans.txt";

  @Autowired
  BigFileCache bigFileCache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDownloadFile() throws Exception {
    bigFileCache.downloadFile(ftpUrl, false, null);
    String localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
    assertTrue(bigFileCache.isCached(ftpUrl));
    assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));
    File f = new File(localFile);
    assertTrue(f.exists());
    String content = super.readFile(localFile);
    assertTrue(content.contains(ftpUrlPartContent));

    int warnigsCount = getWarnings().size();
    // when we try to download file that is already downloaded there should be
    // a warning
    bigFileCache.downloadFile(ftpUrl, false, null);
    assertEquals(warnigsCount + 1, getWarnings().size());

    // however when due to some reason the file will be removed from file
    // system
    f.delete();
    // the file should be downloaded again
    bigFileCache.downloadFile(ftpUrl, false, null);
    // additional warning about it
    assertEquals(warnigsCount + 2, getWarnings().size());

    localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
    f = new File(localFile);
    assertTrue(f.exists());

    // when we are trying to update file that is up to date then we will have
    // a warning about it (but file shouldn't be changed)
    bigFileCache.updateFile(ftpUrl, false);
    assertEquals(warnigsCount + 3, getWarnings().size());

    bigFileCache.removeFile(ftpUrl);

    assertNull(bigFileCache.getAbsolutePathForFile(ftpUrl));

    assertFalse(f.exists());
  }

  @Test
  public void testDownloadFileWithUnknownProtocol() throws Exception {
    try {
      bigFileCache.downloadFile("wtf://file.com", false, null);

    } catch (URISyntaxException e) {
      assertTrue(e.getMessage().contains("Unknown protocol for url"));
    }
  }

  @Test
  public void testDownloadAsyncFileFromHttp() throws Exception {
    bigFileCache.downloadFile(httpUrl, true, null);
    waitForDownload();
    String localFile = bigFileCache.getAbsolutePathForFile(httpUrl);
    assertTrue(bigFileCache.isLocalFileUpToDate(httpUrl));
    File f = new File(localFile);
    assertTrue(f.exists());
    String content = super.readFile(localFile);
    assertTrue(content.contains("Google"));
    bigFileCache.removeFile(httpUrl);

    assertNull(bigFileCache.getAbsolutePathForFile(httpUrl));

    assertFalse(f.exists());
  }

  @Test(expected = IOException.class)
  public void testSavingToInvalidDirectory() throws Exception {
    try {
      String url = httpUrl + "?invalid";
      Configuration.setWebAppDir("/dev/random/xxx");
      bigFileCache.downloadFile(url, false, null);
    } finally {
      Configuration.setWebAppDir("./");
    }
  }

  private void waitForDownload() throws InterruptedException {
    while (bigFileCache.getDownloadThreadCount() > 0) {
      logger.debug("Waiting for download to finish");
      Thread.sleep(100);
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testIsLocalHttpFileUpToDateWhenMissing() throws Exception {
    bigFileCache.isLocalFileUpToDate(httpUrl + "?some_get_param");
  }

  @Test(expected = FileNotFoundException.class)
  public void testIsLocalHttpFileUpToDateWhenFileRemovedManually() throws Exception {
    bigFileCache.downloadFile(httpUrl, false, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    });
    new File(bigFileCache.getAbsolutePathForFile(httpUrl)).delete();
    try {
      bigFileCache.isLocalFileUpToDate(httpUrl);
    } finally {
      bigFileCache.removeFile(httpUrl);
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testIsLocalFtpFileUpToDateWhenMissing() throws Exception {
    bigFileCache.isLocalFileUpToDate(ftpUrl);
  }

  @Test(expected = IOException.class)
  public void testRefreshCacheQueryWhenFtpConnectionFails() throws Exception {

    try {
      bigFileCache.downloadFile(ftpUrl, false, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      });
      assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));

      FTPClient mockClient = Mockito.mock(FTPClient.class);
      Mockito.doAnswer(new Answer<Integer>() {

        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.REQUEST_DENIED;
        }
      }).when(mockClient).getReplyCode();

      FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
      when(factory.createFtpClient()).thenReturn(mockClient);

      bigFileCache.setFtpClientFactory(factory);

      bigFileCache.isLocalFileUpToDate(ftpUrl);
    } finally {
      bigFileCache.setFtpClientFactory(new FtpClientFactory());
      bigFileCache.removeFile(ftpUrl);
    }
  }

  @Test(expected = IOException.class)
  public void testDownloadWhenFtpConnectionFails() throws Exception {
    try {
      FTPClient mockClient = Mockito.mock(FTPClient.class);
      Mockito.doAnswer(new Answer<Integer>() {
        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.REQUEST_DENIED;
        }
      }).when(mockClient).getReplyCode();

      FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
      when(factory.createFtpClient()).thenReturn(mockClient);

      bigFileCache.setFtpClientFactory(factory);
      bigFileCache.downloadFile(ftpUrl, false, null);
    } finally {
      bigFileCache.setFtpClientFactory(new FtpClientFactory());
      bigFileCache.removeFile(ftpUrl);
    }
  }

  @Test(expected = InvalidStateException.class)
  public void testDownloadHttpWhenDownloadFails() throws Exception {
    ConfigurationDao configurationDao = bigFileCache.getConfigurationDao();

    try {

      ConfigurationDao mockDao = Mockito.mock(ConfigurationDao.class);
      when(mockDao.getValueByType(any())).thenThrow(new RuntimeException());
      bigFileCache.setConfigurationDao(mockDao);
      bigFileCache.downloadFile(httpUrl, false, null);
    } finally {
      bigFileCache.setConfigurationDao(configurationDao);
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testDownloadedFileRemovedException() throws Exception {
    String url = ftpUrl;
    bigFileCache.downloadFile(url, false, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    });
    String localFile = bigFileCache.getAbsolutePathForFile(url);
    File f = new File(localFile);
    assertTrue(f.exists());
    f.delete();

    try {
      bigFileCache.getAbsolutePathForFile(url);
    } finally {
      bigFileCache.removeFile(url);
      assertNull(bigFileCache.getAbsolutePathForFile(url));
      assertFalse(f.exists());
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testDownloadedFileRemovedException2() throws Exception {
    String url = ftpUrl;
    bigFileCache.downloadFile(url, false, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    });
    String localFile = bigFileCache.getAbsolutePathForFile(url);
    File f = new File(localFile);
    assertTrue(f.exists());
    f.delete();

    try {
      bigFileCache.isLocalFileUpToDate(url);
    } finally {
      bigFileCache.removeFile(url);
      assertNull(bigFileCache.getAbsolutePathForFile(url));
      assertFalse(f.exists());
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testGetPathForFileBeingDownloaded() throws Exception {
    String url = ftpUrl;
    BigFileCache bigFileCacheUnderTest = new BigFileCache(null, null, null);
    BigFileEntryDao mockDao = Mockito.mock(BigFileEntryDao.class);
    BigFileEntry entry = new BigFileEntry();
    when(mockDao.getByUrl(anyString())).thenReturn(entry);

    bigFileCacheUnderTest.setBigFileEntryDao(mockDao);
    bigFileCacheUnderTest.getAbsolutePathForFile(url);
  }

  @Test
  public void testUpdateFile() throws Exception {
    bigFileCache.downloadFile(ftpUrl, false, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    });
    String localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
    File f = new File(localFile);
    f.delete();
    PrintWriter out = new PrintWriter(localFile);
    out.println("tesxt");
    out.close();

    String content = super.readFile(localFile);
    assertFalse(content.contains(ftpUrlPartContent));

    assertFalse(bigFileCache.isLocalFileUpToDate(ftpUrl));
    bigFileCache.updateFile(ftpUrl, false);

    localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);

    content = super.readFile(localFile);
    assertTrue(content.contains(ftpUrlPartContent));

    // now try update asynchronously
    f = new File(localFile);
    f.delete();
    out = new PrintWriter(localFile);
    out.println("tesxt");
    out.close();

    assertFalse(bigFileCache.isLocalFileUpToDate(ftpUrl));
    bigFileCache.updateFile(ftpUrl, true);

    waitForDownload();

    assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));

    // check when connection fails
    f = new File(localFile);
    f.delete();
    out = new PrintWriter(localFile);
    out.println("tesxt");
    out.close();

    FTPClient mockClient = Mockito.mock(FTPClient.class);
    Mockito.doAnswer(new Answer<Integer>() {

      // first answer (for checking if the file should be updated)
      @Override
      public Integer answer(InvocationOnMock invocation) throws Throwable {
        return FTPReply.COMMAND_OK;
      }
    }).doAnswer(new Answer<Integer>() {
      // second answer (for downloading)
      @Override
      public Integer answer(InvocationOnMock invocation) throws Throwable {
        return FTPReply.REQUEST_DENIED;
      }
    }).when(mockClient).getReplyCode();

    Mockito.doAnswer(new Answer<FTPFile[]>() {

      @Override
      public FTPFile[] answer(InvocationOnMock invocation) throws Throwable {
        return new FTPFile[] {};
      }
    }).when(mockClient).listFiles(anyString());

    FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
    when(factory.createFtpClient()).thenReturn(mockClient);

    bigFileCache.setFtpClientFactory(factory);
    try {
      bigFileCache.updateFile(ftpUrl, false);
      fail("Exception expected");
    } catch (IOException e) {
      assertTrue(e.getMessage().contains("FTP server refused connection"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      bigFileCache.setFtpClientFactory(new FtpClientFactory());
    }

    bigFileCache.removeFile(ftpUrl);

    assertNull(bigFileCache.getAbsolutePathForFile(ftpUrl));

    assertFalse(f.exists());
  }

  @Test(expected = IOException.class)
  public void testGetRemoteHttpFileSizeForInvalidUrl() throws Exception {
    bigFileCache.getRemoteHttpFileSize("invalidurl");
  }

  @Test
  public void testRemoveFileThatDoesntExist() throws Exception {
    bigFileCache.removeFile("unexisting file");
  }

  @Test(expected = URISyntaxException.class)
  public void testIsLocalFileUpToDateForInvalidURI() throws Exception {
    bigFileCache.isLocalFileUpToDate("unexistingFileProtocol://bla");
  }

  @Test
  public void testGetDomainName() throws Exception {
    String domain = bigFileCache.getDomainName("http://www.eskago.pl/radio/eska-rock");
    assertEquals("eskago.pl", domain);
  }

  @Test(expected = InvalidStateException.class)
  public void testExecuteProblematicTask() throws Exception {
    Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    }).submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        throw new Exception();
      }
    });
    bigFileCache.executeTask(task);
  }

  @Test(expected = URISyntaxException.class)
  public void testExecuteProblematicTask2() throws Exception {
    Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    }).submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        throw new URISyntaxException("", "");
      }
    });
    bigFileCache.executeTask(task);
  }

  @Test
  public void testExecuteInterruptedTask() throws Exception {
    Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    }).submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        Thread.sleep(100);
        return null;
      }
    });
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          bigFileCache.executeTask(task);
        } catch (Exception e) {
          logger.fatal(e, e);
        }
      }
    });

    t.start();
    // interrupt thread
    t.interrupt();
    t.join();

    assertEquals(1, getErrors().size());
    assertEquals(0, getFatals().size());
  }

}
