package lcsb.mapviewer.annotation.cache;

/**
 * Mock implementation of {@link CachableInterface}. It's used for invalid cache
 * elements: {@link lcsb.mapviewer.model.cache.CacheType#INVALID_CACHE}.
 * 
 * @author Piotr Gawron
 * 
 */
public final class MockCacheInterface extends CachableInterface {

  public static RuntimeException exceptionToThrow = null;
  public static Error errorToThrow;

  /**
   * Defsault constructor.
   */
  public MockCacheInterface() {
    super(MockCacheInterface.class);
  }

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    if (exceptionToThrow != null) {
      throw exceptionToThrow;
    }
    if (errorToThrow != null) {
      throw errorToThrow;
    }
    return "<node>[" + query + "] Random: " + Math.random() + "</node>";
  }

}
