package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;

public class GeneralCacheWithExclusionTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(GeneralCacheWithExclusionTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidParams() {
    new GeneralCacheWithExclusion(null, 1);
  }

  @Test
  public void testExclusion() throws Exception {
    try {
      CacheType type = new CacheType();
      GeneralCache cache = new GeneralCache(null);
      cache.setCache2(ApplicationLevelCache.getInstance());
      GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 2);

      cacheWithExclusion.setCachedQuery("str", type, super.getNodeFromXmlString("<val/>"));

      // after two tries we should get the data that was set
      assertNull(cacheWithExclusion.getXmlNodeByQuery("str", type));
      assertNull(cacheWithExclusion.getXmlNodeByQuery("str", type));
      assertNotNull(cacheWithExclusion.getXmlNodeByQuery("str", type));

    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache passed as argument cannot be null"));
    }
  }

  @Test
  public void testClear() throws Exception {
    try {
      CacheType type = new CacheType();
      GeneralCache cache = new GeneralCache(null);
      cache.setCache2(ApplicationLevelCache.getInstance());
      GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 0);

      cacheWithExclusion.setCachedQuery("str", type, super.getNodeFromXmlString("<val/>"));

      assertNotNull(cacheWithExclusion.getXmlNodeByQuery("str", type));
      cacheWithExclusion.clearCache();

      assertNull(cacheWithExclusion.getXmlNodeByQuery("str", type));

    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache passed as argument cannot be null"));
    }
  }

  @Test
  public void testRemove() throws Exception {
    try {
      CacheType type = new CacheType();
      GeneralCache cache = new GeneralCache(null);
      cache.setCache2(ApplicationLevelCache.getInstance());
      GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 0);

      cacheWithExclusion.setCachedQuery("str", type, super.getNodeFromXmlString("<val/>"));

      assertNotNull(cacheWithExclusion.getXmlNodeByQuery("str", type));
      cacheWithExclusion.removeByQuery("str", type);

      assertNull(cacheWithExclusion.getXmlNodeByQuery("str", type));

    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache passed as argument cannot be null"));
    }
  }

  @Test
  public void testInvalidate() throws Exception {
    try {
      CacheType type = new CacheType();
      GeneralCache cache = new GeneralCache(null);
      cache.setCache2(ApplicationLevelCache.getInstance());
      GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 0);

      cacheWithExclusion.setCachedQuery("str", type, super.getNodeFromXmlString("<val/>"));
      cacheWithExclusion.invalidateByQuery("str", type);

      assertNull(cacheWithExclusion.getXmlNodeByQuery("str", type));
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache passed as argument cannot be null"));
    }
  }

}
