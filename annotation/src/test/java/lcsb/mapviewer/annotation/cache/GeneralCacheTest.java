package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;

public class GeneralCacheTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(GeneralCacheTest.class);

  GeneralCache cache = new GeneralCache(null);

  @Autowired
  PermanentDatabaseLevelCacheInterface databaseLevelCache;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    cache.setCache2(databaseLevelCache);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testClear() {
    QueryCacheInterface origianlCache = cache.getCache2();
    try {
      CacheType type = new CacheType();
      type.setId(-1);
      cache.setCache2(ApplicationLevelCache.getInstance());
      ApplicationLevelCache.getInstance().setCachedQuery("str", type, "val");
      assertNotNull(cache.getStringByQuery("str", type));
      cache.clearCache();
      assertNull(cache.getStringByQuery("str", type));
    } finally {
      cache.setCache2(origianlCache);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetXmlNodeByQueryForInvalidType() {
    cache.getXmlNodeByQuery("str", null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetCachedQueryForInvalidType() {
    cache.setCachedQuery("str", null, null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRemoveByQueryForInvalidType() {
    cache.removeByQuery("str", null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidateByQueryForInvalidType() {
    cache.invalidateByQuery("str", null);
  }

  @Test
  public void testCachableInterfaceInvalidateUrl() throws Exception {
    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

    String query = "http://google.lu/";
    String newRes = "hello";
    cache.setCachedQuery(query, type, newRes);
    cache.invalidateByQuery(query, type);

    databaseLevelCache.waitToFinishTasks();

    String res = cache.getStringByQuery(query, type);

    assertNotNull(res);

    assertFalse("Value wasn't refreshed from db", newRes.equals(res));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetStringByQueryForInvalidType() {
    cache.getStringByQuery("str", null);
  }

  @Test
  public void testRemoveByQuery() {
    try {
      CacheType type = new CacheType();
      cache.setCachedQuery("str", type, "val");
      assertNotNull(cache.getStringByQuery("str", type));
      cache.removeByQuery("str", type);
      assertNull(cache.getStringByQuery("str", type));
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache type cannot be null"));
    }
  }

}
