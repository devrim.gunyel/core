package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.cache.CacheType;

public class ApplicationLevelCacheTest extends AnnotationTestFunctions {

  Logger logger = LogManager.getLogger(ApplicationLevelCacheTest.class);

  boolean status;

  @Before
  public void setUp() throws Exception {
    status = Configuration.isApplicationCacheOn();
  }

  @After
  public void tearDown() throws Exception {
    Configuration.setApplicationCacheOn(status);
  }

  @Test
  public void testApplicationCacheByQuery() throws Exception {
    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());
    String xml = "<hello/>";
    String query = "blabla";
    Node sourceNode = getNodeFromXmlString(xml);
    ApplicationLevelCache cache = ApplicationLevelCache.getInstance();
    cache.clearCache();

    Node node = cache.getXmlNodeByQuery(query, type);
    assertNull(node);

    cache.setCachedQuery(query, type, sourceNode);
    node = cache.getXmlNodeByQuery(query, type);
    assertNotNull(node);

    cache.clearCache();
    node = cache.getXmlNodeByQuery(query, type);
    assertNull(node);
  }

  @Test
  public void testApplicationCacheOff() {
    boolean value = Configuration.isApplicationCacheOn();

    try {
      Configuration.setApplicationCacheOn(false);
      assertNull(ApplicationLevelCache.getInstance());
    } finally {
      Configuration.setApplicationCacheOn(value);
    }
  }

  @Test
  public void testSetInvalidValue() {
    try {
      ApplicationLevelCache cache = ApplicationLevelCache.getInstance();
      cache.setCachedQuery("bla", null, new Object());
    } catch (CacheException e) {
      assertTrue(e.getMessage().contains("Unknown object type"));
    }
  }

  @Test
  public void testClearCacheWhenMemoryUsageIsHigh() throws Exception {
    Double oldVal = Configuration.getMemorySaturationRatioTriggerClean();
    try {
      CacheType type = new CacheType();
      type.setId(-13);
      ApplicationLevelCache cache = ApplicationLevelCache.getInstance();
      cache.setCachedQuery("bla", type, "test");

      assertNotNull(cache.getStringByQuery("bla", type));
      // fill cache with some dumb data
      for (int i = 0; i < ApplicationLevelCache.MIN_CACHED_VALUES_BEFORE_CLEAN; i++) {
        cache.setCachedQuery("bla" + i, type, "test");
      }
      assertNotNull(cache.getStringByQuery("bla", type));

      Configuration.setMemorySaturationRatioTriggerClean(0.0);
      cache.performMemoryBalance();
      assertNull(cache.getStringByQuery("bla", type));

    } catch (CacheException e) {
      assertTrue(e.getMessage().contains("Unknown object type"));
    } finally {
      // restore old value
      Configuration.setMemorySaturationRatioTriggerClean(oldVal);
    }
  }

}
