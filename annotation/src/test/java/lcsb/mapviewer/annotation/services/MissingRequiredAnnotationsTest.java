package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class MissingRequiredAnnotationsTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructorWithInvalidParams() {
    try {
      new MissingRequiredAnnotations(new GenericProtein("id"), new ArrayList<>());
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("List of improper annotations cannot be null"));
    }
  }

  @Test
  public void testGetMessage() {
    List<MiriamType> list = new ArrayList<>();
    list.add(MiriamType.CAS);
    MissingRequiredAnnotations mre = new MissingRequiredAnnotations(new GenericProtein("id"), list);
    assertTrue(mre.toString().contains("Missing one of the required annotations"));
  }

}
