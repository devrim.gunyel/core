package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class BrendaAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(BrendaAnnotatorTest.class);

  @Autowired
  BrendaAnnotator brendaAnnotator;
  @Autowired
  UniprotAnnotator uniprotAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateFromUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));

    brendaAnnotator.annotateElement(protein);

    int cntMds = 0;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.BRENDA)) {
        cntMds++;
        assertTrue("Invalid BRENDA annotation extracted from BRENDA annotator",
            md.getResource().equals("2.6.1.1") || md.getResource().equals("2.6.1.7"));
      }
    }

    assertTrue("Incorrect BRENDA annotations extracted from BRENDA annotator", cntMds == 2);
  }

  @Test
  @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateFromTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT5G48930"));

    // this is slow because accessing
    // https://arabidopsis.org/servlets/TairObject?type=locus&name=AT5G48930
    // is very slow
    brendaAnnotator.annotateElement(protein);

    MiriamData mdBrenda = null;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.BRENDA)) {
        mdBrenda = md; // there should be only one EC number for that TAIR<->UNIPROT record
      }
    }

    assertTrue("No BRENDA annotation extracted from BRENDA annotator", mdBrenda != null);
    assertTrue("Invalid BRENDA annotation extracted from BRENDA annotator based on TAIR",
        mdBrenda.getResource().equals("2.3.1.133"));
  }

  @Test
  public void testAnnotateMultipleUniprots() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q12540"));

    brendaAnnotator.annotateElement(protein);

    assertTrue("Wrong number of BRENDA identifiers extracted from BRENDA annotator",
        protein.getMiriamData().size() == 4);
  }

  @Test
  public void testAnnotateMultipleUniprotsWithIdenticalEC() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q8L5J1"));

    brendaAnnotator.annotateElement(protein);

    assertTrue("Wrong number of BRENDA identifiers extracted from BRENDA annotator",
        protein.getMiriamData().size() == 3);
  }

  @Test
  public void testAnnotateInvalidEmpty() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    brendaAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      Species protein = new GenericProtein("id");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      uniprotAnnotator.setWebPageDownloader(mockDownloader);

      brendaAnnotator.annotateElement(protein);

      assertEquals(1, protein.getMiriamData().size());

      assertEquals(1, getWarnings().size());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    brendaAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, brendaAnnotator.getServiceStatus().getStatus());
  }
}
