package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;

import java.util.*;

import org.apache.commons.lang3.mutable.MutableDouble;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.persist.dao.map.ModelDao;

public class ModelAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(ModelAnnotatorTest.class);
  IProgressUpdater updater = new IProgressUpdater() {
    @Override
    public void setProgress(double progress) {
    }
  };

  @Autowired
  private ModelAnnotator modelAnnotator;

  @Autowired
  private ModelDao modelDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateModel() {
    Model model = new ModelFullIndexed(null);

    model.addReaction(new Reaction());

    Species proteinAlias1 = new GenericProtein("a1");
    proteinAlias1.setName("SNCA");

    Species proteinAlias2 = new GenericProtein("a2");
    proteinAlias2.setName("PDK1");
    proteinAlias2.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CAS, "c"));

    model.addElement(proteinAlias1);
    model.addElement(proteinAlias2);

    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());

    assertTrue(model.getElementByElementId("a1").getMiriamData().size() > 0);
    assertTrue(model.getElementByElementId("a2").getMiriamData().size() >= 1);

    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());
  }

  @Test
  public void testAnnotateModelWithGivenAnnotators() throws Exception {
    Model model = new ModelFullIndexed(null);
    GenericProtein species = new GenericProtein("id1");
    species.setName("SNCA");
    model.addElement(species);

    Ion species2 = new Ion("id2");
    species2.setName("h2o");
    species2.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "12345"));
    model.addElement(species2);
    model.addReaction(new Reaction());

    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

    UserClassAnnotators proteinAnnotators = new UserClassAnnotators(GenericProtein.class);
    proteinAnnotators.setAnnotators(new ArrayList<>());
    annotationSchema.addClassAnnotator(proteinAnnotators);

    UserClassAnnotators ionAnnotators = new UserClassAnnotators(Ion.class);
    ionAnnotators.setAnnotators(modelAnnotator.getDefaultAnnotators(Ion.class));
    annotationSchema.addClassAnnotator(ionAnnotators);

    modelAnnotator.annotateModel(model, updater, annotationSchema);

    // we didn't annotate protein
    assertEquals(0, species.getMiriamData().size());
    // but we should annotate ion
    assertFalse(species2.getFullName().isEmpty());
  }

  @Test
  public void testCopyMissingAnnotationsInCD() throws Exception {
    Model model = getModelForFile("testFiles/annotation/copyingAnnotationModel.xml", true);

    modelAnnotator.copyAnnotationFromOtherSpecies(model, updater);

    for (Species element : model.getSpeciesList()) {
      if (element.getName().equals("s4"))
        assertEquals(0, element.getMiriamData().size());
      else if (element.getName().equals("hello"))
        assertEquals(1, element.getMiriamData().size());
    }
  }

  @Test
  public void testDuplicateAnnotations() throws Exception {
    int counter = 0;
    Model model = getModelForFile("testFiles/annotation/duplicate.xml", false);

    Set<String> knowAnnotations = new HashSet<String>();

    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());
    for (MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
      knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
    }
    counter = 0;
    for (String string : knowAnnotations) {
      if (string.contains("29108")) {
        counter++;
      }
    }
    assertEquals(1, counter);

    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());

    knowAnnotations.clear();
    for (MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
      knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
    }
    counter = 0;
    for (String string : knowAnnotations) {
      if (string.contains("29108")) {
        counter++;
      }
    }
    assertEquals(1, counter);
  }

  @Test
  public void testGetAnnotationsForSYN() throws Exception {
    Model model = getModelForFile("testFiles/annotation/emptyAnnotationsSyn1.xml", true);
    Element sa1 = model.getElementByElementId("sa1");
    Element sa2 = model.getElementByElementId("sa2");

    assertFalse(sa1.getNotes().contains("Symbol"));
    assertFalse(sa2.getNotes().contains("Symbol"));
    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());
    assertFalse(sa2.getNotes().contains("Symbol"));
    assertNotNull(sa1.getSymbol());
    assertFalse(sa1.getSymbol().equals(""));
    // modelAnnotator.removeIncorrectAnnotations(model, updater);
    assertNotNull(sa1.getSymbol());
    assertFalse(sa1.getSymbol().equals(""));
    assertFalse(sa1.getNotes().contains("Symbol"));
    assertFalse(sa2.getNotes().contains("Symbol"));
    assertNull(sa2.getSymbol());

    for (Species el : model.getSpeciesList()) {
      if (el.getNotes() != null) {
        assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("Symbol"));
        assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("HGNC"));
      }
    }
  }

  @Test
  public void testFindInmproperAnnotations() throws Exception {
    Model model = getModelForFile("testFiles/annotation/centeredAnchorInModifier.xml", true);
    Collection<? extends ProblematicAnnotation> results = modelAnnotator.findImproperAnnotations(model,
        new IProgressUpdater() {

          @Override
          public void setProgress(double progress) {
            // TODO Auto-generated method stub

          }
        }, null);
    assertTrue(results.size() > 0);
  }

  @Test
  public void testFindInmproperAnnotations2() throws Exception {
    List<MiriamType> list = new ArrayList<>();
    list.add(MiriamType.PUBMED);
    Reaction reaction = new Reaction();
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
    List<ImproperAnnotations> result = modelAnnotator.findImproperAnnotations(reaction, list);
    assertEquals("Unexpected improper annotations found: " + result, 0, result.size());
  }

  @Test
  public void testFindInmproperAnnotations3() throws Exception {
    List<MiriamType> list = new ArrayList<>();
    list.add(MiriamType.PUBMED);
    Reaction reaction = new Reaction();
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "inv_id"));
    List<ImproperAnnotations> result = modelAnnotator.findImproperAnnotations(reaction, list);
    assertEquals(1, result.size());
  }

  @Test
  public void testFindMissingAnnotations() throws Exception {
    Model model = getModelForFile("testFiles/annotation/missingAnnotations.xml", false);
    Collection<? extends ProblematicAnnotation> results = modelAnnotator.findMissingAnnotations(model, null);
    assertEquals(7, results.size());
  }

  @Test
  public void testFindMissingAnnotations2() throws Exception {
    Model model = getModelForFile("testFiles/annotation/missingAnnotations.xml", false);
    Map<Class<? extends BioEntity>, Set<MiriamType>> requestedAnnotations = new HashMap<>();
    requestedAnnotations.put(GenericProtein.class, new HashSet<>());
    Collection<? extends ProblematicAnnotation> results = modelAnnotator.findMissingAnnotations(model,
        requestedAnnotations);
    assertEquals(1, results.size());
  }

  @Test
  public void testAnnotateModelWithDrugMolecule() throws Exception {
    Model model = super.getModelForFile("testFiles/annotation/problematic.xml", false);

    modelAnnotator.annotateModel(model, updater, modelAnnotator.createDefaultAnnotatorSchema());
    modelDao.add(model);

    modelDao.delete(model);
  }

  @Test
  public void testGetDefaultRequired() throws Exception {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map1 = modelAnnotator.getDefaultRequiredClasses();
    assertNotNull(map1);
  }

  @Test
  public void testGetDefaultValid() throws Exception {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map2 = modelAnnotator.getDefaultValidClasses();
    assertNotNull(map2);
  }

  @Test
  public void testPerformAnnotationAndCheckProgress() throws Exception {
    Model model = new ModelFullIndexed(null);
    Model submodel = new ModelFullIndexed(null);
    Model submodel2 = new ModelFullIndexed(null);

    GenericProtein protein = new GenericProtein("el");

    model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));
    model.addSubmodelConnection(new ModelSubmodelConnection(submodel2, SubmodelType.UNKNOWN));

    model.addElement(protein);
    submodel.addElement(protein);
    submodel2.addElement(protein);

    final MutableDouble maxProgress = new MutableDouble(0.0);

    modelAnnotator.performAnnotations(model, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        maxProgress.setValue(Math.max(progress, maxProgress.getValue()));
      }
    });
    assertTrue(maxProgress.getValue() <= IProgressUpdater.MAX_PROGRESS);

  }

  @Test
  public void testGetAvailableAnnotators() {
    List<ElementAnnotator> list = modelAnnotator.getAvailableAnnotators(Protein.class);
    assertTrue(list.size() > 0);
  }

  @Test
  public void testGetAvailableAnnotators2() {
    List<ElementAnnotator> list = modelAnnotator.getAvailableAnnotators();
    assertTrue(list.size() > 0);
  }

  @Test
  public void testGetAvailableDefaultAnnotators() {
    List<ElementAnnotator> list = modelAnnotator.getAvailableDefaultAnnotators(Protein.class);
    assertTrue(list.size() > 0);
  }

}
