package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.*;

public class MiRNASearchExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    MiRNASearchException exception = new MiRNASearchException(new IOException());
    assertTrue(exception.getCause().getClass().equals(IOException.class));
  }

  @Test
  public void testConstructor2() {
    MiRNASearchException exception = new MiRNASearchException("xsd");
    assertEquals("xsd", exception.getMessage());
  }

}
