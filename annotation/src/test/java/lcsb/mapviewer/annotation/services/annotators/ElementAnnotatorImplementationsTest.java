package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.SpringAnnotationTestConfig;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator.BioEntityProxy;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.user.annotator.*;
import lcsb.mapviewer.persist.DbUtils;

@RunWith(Parameterized.class)
public class ElementAnnotatorImplementationsTest extends AnnotationTestFunctions {
  static ApplicationContext applicationContext;
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ElementAnnotatorImplementationsTest.class);
  @Parameter
  public String testName;
  @Parameter(1)
  public Class<? extends ElementAnnotator> elementAnnotator;
  @Parameter(2)
  public MiriamData sourceIdentifier;
  @Parameter(3)
  public BioEntity bioEntity;

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();
    applicationContext = new AnnotationConfigApplicationContext(
        SpringAnnotationTestConfig.class);

    ModelAnnotator modelAnnotator = (ModelAnnotator) applicationContext.getBean("modelAnnotator");
    for (ElementAnnotator annotator : modelAnnotator.getAvailableAnnotators()) {
      if (annotator.getExampleValidAnnotation() != null) {
        Object[] row = new Object[] { annotator.getClass().getSimpleName(), annotator.getClass(),
            annotator.getExampleValidAnnotation(), null };
        if (annotator.isAnnotatable(TransportReaction.class)) {
          row[3] = new TransportReaction();
        } else if (annotator.isAnnotatable(GenericProtein.class)) {
          row[3] = new GenericProtein("");
        } else if (annotator.isAnnotatable(SimpleMolecule.class)) {
          row[3] = new SimpleMolecule("");
        } else if (annotator.isAnnotatable(SquareCompartment.class)) {
          row[3] = new SquareCompartment("");
        } else {
          throw new NotImplementedException(annotator.getValidClasses() + "");
        }
        result.add(row);
      }
    }
    return result;
  }

  @Test
  @Transactional
  public void testExpectedOutputAppeard() throws Exception {
    ElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    DbUtils dbUtils = applicationContext.getBean(DbUtils.class);
    dbUtils.createSessionForCurrentThread();
    try {
      for (AnnotatorOutputParameter output : annotator.getAvailableOuputProperties()) {
        AnnotatorData parameters = annotator.createAnnotatorData();
        parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
        parameters.addAnnotatorParameter(output);
        BioEntityProxy sourceProxy = annotator.new BioEntityProxy(bioEntity, parameters);

        BioEntity copy = bioEntity.copy();

        BioEntityProxy proxy = annotator.new BioEntityProxy(copy, parameters);

        assertEquals(sourceProxy.getFieldValue(output), proxy.getFieldValue(output));

        copy.addMiriamData(sourceIdentifier);
        parameters.addAnnotatorParameters(annotator.getExampleValidParameters());
        annotator.annotateElement(copy, parameters);

        assertNotEquals("Field " + output + " wasn't set by annotator", sourceProxy.getFieldValue(output),
            proxy.getFieldValue(output));

      }
    } finally {
      dbUtils.closeSessionForCurrentThread();
    }
  }

  @Test
  @Transactional
  public void testUnexpectedOutputAppeard() throws Exception {
    ElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    DbUtils dbUtils = applicationContext.getBean(DbUtils.class);
    dbUtils.createSessionForCurrentThread();
    try {
      AnnotatorData parameters = annotator.createAnnotatorData();
      parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
      for (BioEntityField field : BioEntityField.values()) {
        AnnotatorOutputParameter parameter = new AnnotatorOutputParameter(field);
        if (!annotator.getAvailableOuputProperties().contains(parameter)) {
          parameters.addAnnotatorParameter(parameter);
        }
      }
      for (MiriamType type : MiriamType.values()) {
        AnnotatorOutputParameter parameter = new AnnotatorOutputParameter(type);
        if (!annotator.getAvailableOuputProperties().contains(parameter)) {
          parameters.addAnnotatorParameter(parameter);
        }
      }
      BioEntity sourceCopy = bioEntity.copy();
      BioEntity annotatedCopy = bioEntity.copy();
      sourceCopy.addMiriamData(sourceIdentifier);
      annotatedCopy.addMiriamData(sourceIdentifier);

      BioEntityProxy sourceProxy = annotator.new BioEntityProxy(sourceCopy, parameters);
      BioEntityProxy proxy = annotator.new BioEntityProxy(annotatedCopy, parameters);

      annotator.annotateElement(annotatedCopy, parameters);

      for (AnnotatorOutputParameter output : parameters.getOutputParameters()) {
        assertEquals("Field " + output + " was set by annotator but it's not defined as output parameter",
            sourceProxy.getFieldValue(output),
            proxy.getFieldValue(output));
      }

    } finally {
      dbUtils.closeSessionForCurrentThread();
    }
  }

  @Test
  @Transactional
  public void testOutputNotModifiedWithEmptyParams() throws Exception {
    ElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    DbUtils dbUtils = applicationContext.getBean(DbUtils.class);
    dbUtils.createSessionForCurrentThread();
    try {
      AnnotatorData parameters = annotator.createAnnotatorData();
      parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
      parameters.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.UNKNOWN));
      BioEntity sourceCopy = bioEntity.copy();
      BioEntity annotatedCopy = bioEntity.copy();
      sourceCopy.addMiriamData(sourceIdentifier);
      annotatedCopy.addMiriamData(sourceIdentifier);

      BioEntityProxy sourceProxy = annotator.new BioEntityProxy(sourceCopy, parameters);
      BioEntityProxy proxy = annotator.new BioEntityProxy(annotatedCopy, parameters);

      annotator.annotateElement(annotatedCopy, parameters);

      for (BioEntityField field : BioEntityField.values()) {
        AnnotatorOutputParameter output = new AnnotatorOutputParameter(field);
        assertEquals("Field " + output + " was set by annotator but it's not defined as output parameter",
            sourceProxy.getFieldValue(output),
            proxy.getFieldValue(output));
      }
    } finally {
      dbUtils.closeSessionForCurrentThread();
    }
  }

}
