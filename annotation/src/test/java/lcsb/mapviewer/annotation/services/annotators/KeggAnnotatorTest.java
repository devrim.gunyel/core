package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.*;

import org.apache.http.HttpStatus;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;

public class KeggAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  KeggAnnotator keggAnnotator;
  @Autowired
  UniprotAnnotator uniprotAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private void Evaluate_3_1_2_14(Species protein) {

    Collection<MiriamData> mdPubmed = new ArrayList<>();

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        mdPubmed.add(md);
      }
    }

    assertTrue("No PUBMED annotation extracted from KEGG annotator", mdPubmed.size() > 0);
    assertTrue("Wrong number of publications extracted from KEGG annotator", mdPubmed.size() == 2);

    Set<String> pmids = new HashSet<String>();
    pmids.add("30409");
    pmids.add("3134");
    int cntMatches = 0;
    for (MiriamData md : protein.getMiriamData()) {
      if (pmids.contains(md.getResource())) {
        cntMatches++;
      }
    }

    assertTrue("Wrong PUBMED IDs extracted from KEGG annotator", cntMatches == 2);

  }

  @Test
  public void testAnnotateFromUniprotWithoutParams() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));

    keggAnnotator.annotateElement(protein);

    Evaluate_3_1_2_14(protein);
  }

  @Test
  public void testAnnotateFromUniprotWithParams() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));

    AnnotatorData parameters = keggAnnotator.createAnnotatorData()
        .addAnnotatorParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "ATH");
    keggAnnotator.annotateElement(protein, parameters);

    int cntTairs = 0;
    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
        cntTairs++;
      }
    }

    assertTrue("Invalid number of TAIR annotators from KEGG", cntTairs == 3);
  }

  @Test
  public void testAnnotateFromUniprotWithWrongParams1() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));

    AnnotatorData parameters = keggAnnotator.createAnnotatorData()
        .addAnnotatorParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "XXX");
    keggAnnotator.annotateElement(protein, parameters);

    assertEquals("There should be warning about unsupported parameter", 1, getWarnings().size());
  }

  @Test
  public void testAnnotateFromUniprotWithWrongParams2() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));
    AnnotatorData parameters = keggAnnotator.createAnnotatorData()
        .addAnnotatorParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "ATH AAA");
    keggAnnotator.annotateElement(protein, parameters);

    int cntTairs = 0;
    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
        cntTairs++;
      }
    }

    assertTrue("Invalid number of TAIR annotators from KEGG", cntTairs == 3);
    assertEquals("There should be warning about unsupported parameter", 1, getWarnings().size());
  }

  @Test
  public void testAnnotateFromEc() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.EC, "3.1.2.14"));

    keggAnnotator.annotateElement(protein);

    Evaluate_3_1_2_14(protein);
  }

  @Test
  @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateFromTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2090285")); // TAIR locus AT3G25110

    keggAnnotator.annotateElement(protein);

    Evaluate_3_1_2_14(protein);
  }

  @Test
  public void testAnnotateFromUniprotWithMultipleECs() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));

    keggAnnotator.annotateElement(protein);

    Collection<MiriamData> mdPubmed = new ArrayList<>();

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        mdPubmed.add(md);
      }
    }

    assertEquals("Wrong number of publications extracted from KEGG annotator", 9, mdPubmed.size());
  }

  @Test
  public void testAnnotateFromUniprotWithMultipleECsAndEC() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));
    protein.addMiriamData(new MiriamData(MiriamType.EC, "3.1.2.14"));

    keggAnnotator.annotateElement(protein);

    Collection<MiriamData> mdPubmed = new ArrayList<>();

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        mdPubmed.add(md);
      }
    }

    assertTrue("Wrong number of publications extracted from KEGG annotator", mdPubmed.size() >= 9);
  }

  @Test
  public void testAnnotateInvalidEmpty() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    keggAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(new IOException(), HttpStatus.SC_NOT_FOUND));
      uniprotAnnotator.setWebPageDownloader(mockDownloader);

      Species protein = new GenericProtein("id");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
      keggAnnotator.annotateElement(protein);

      assertEquals(1, protein.getMiriamData().size());

      assertTrue(getWarnings().size() > 0);
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    keggAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());

    assertTrue(getWarnings().size() > 0);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, keggAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = keggAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      keggAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, keggAnnotator.getServiceStatus().getStatus());
    } finally {
      keggAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = keggAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      keggAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, keggAnnotator.getServiceStatus().getStatus());
    } finally {
      keggAnnotator.setWebPageDownloader(downloader);
    }
  }

}
