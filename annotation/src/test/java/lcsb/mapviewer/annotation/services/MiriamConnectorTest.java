package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;

public class MiriamConnectorTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(MiriamConnectorTest.class);

  @Autowired
  MiriamConnector miriamConnector;

  GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    cache = miriamConnector.getCache();
  }

  @After
  public void tearDown() throws Exception {
    miriamConnector.setCache(cache);
  }

  @Test
  public void testGoUri() throws Exception {
    assertFalse(MiriamType.GO.getUris().size() == 1);
  }

  @Test(timeout = 15000)
  public void testMeshUrl() throws Exception {
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.MESH_2012, "D004249");

    assertNotNull(getWebpage(miriamConnector.getUrlString(md)));
  }

  @Test
  public void testGetUrl() throws Exception {
    MiriamData md = TaxonomyBackend.HUMAN_TAXONOMY;

    String url = miriamConnector.getUrlString(md);
    assertNotNull(url);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetUrlForInvalidMiriam() throws Exception {
    miriamConnector.getUrlString(new MiriamData());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetUrlForInvalidMiriam2() throws Exception {
    miriamConnector.getUrlString(new MiriamData(MiriamType.UNKNOWN, ""));
  }

  @Test
  public void testGetUrlForInvalidMiriam3() throws Exception {
    String url = miriamConnector.getUrlString(new MiriamData(MiriamType.ENTREZ, "abc"));
    assertNull(url);
  }

  @Test
  public void testIsValidIdentifier() throws Exception {
    assertTrue(miriamConnector.isValidIdentifier(MiriamType.ENTREZ.getCommonName() + ":" + "1234"));
    assertFalse(miriamConnector.isValidIdentifier("blablabla"));
  }

  @Test
  public void testResolveUrl() throws Exception {
    for (MiriamType mt : MiriamType.values()) {
      boolean deprecated = false;
      try {
        Field f = MiriamType.class.getField(mt.name());
        if (f.isAnnotationPresent(Deprecated.class))
          deprecated = true;
      } catch (NoSuchFieldException | SecurityException e) {
      }

      if (!deprecated) {

        assertTrue("Invalid MiriamType (" + mt + ") for MiriamType: " + mt, miriamConnector.isValidMiriamType(mt));
      }
    }
  }

  @Test
  public void testGetUrlForDoi() throws Exception {
    // exclude first cached value
    MiriamData md = new MiriamData(MiriamType.DOI, "10.1038/npjsba.2016.20");

    String url = miriamConnector.getUrlString(md);
    assertNotNull(url);
  }

  @Test
  public void testMiriamDataToUri() throws Exception {
    assertNotNull(miriamConnector.miriamDataToUri(TaxonomyBackend.HUMAN_TAXONOMY));
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = miriamConnector.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      miriamConnector.setWebPageDownloader(mockDownloader);
      miriamConnector.refreshCacheQuery("http://google.pl/");
    } finally {
      miriamConnector.setWebPageDownloader(downloader);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    miriamConnector.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    miriamConnector.refreshCacheQuery(new Object());
  }

  @Test
  public void testCheckValidyOfUnknownMiriamType() throws Exception {
    // user connector without cache
    boolean value = miriamConnector.isValidMiriamType(MiriamType.UNKNOWN);
    assertFalse(value);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, miriamConnector.getServiceStatus().getStatus());
  }

}
