package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class MeSHParserTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(MeSHParserTest.class);

  @Autowired
  MeSHParser meshParser;

  @Autowired
  private PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCache;

  @Autowired
  private GeneralCacheInterface cache;

  @Test
  public void testGetMesh() throws Exception {
    // Parkinson disease
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
    MeSH mesh = meshParser.getMeSH(meshID);
    assertTrue(mesh != null);
    assertTrue(mesh.getMeSHId() != null);
    assertTrue(mesh.getName() != null);
    assertTrue(mesh.getName().toLowerCase().equals("dopamine"));
    assertTrue(mesh.getDescription() != null);
    assertTrue(mesh.getSynonyms().size() > 0);
  }

  @Test
  public void testGetMeshBySynonym() throws Exception {
    String synonym = "MPTP";
    List<MeSH> result = meshParser.getMeshBySynonym(synonym);
    assertEquals(1, result.size());
    MeSH mesh = result.get(0);
    assertTrue(mesh.getSynonyms().contains(synonym));
  }

  @Test
  public void testIsValidMesh() throws Exception {
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
    assertTrue(meshParser.isValidMeshId(meshID));
  }

  @Test
  public void testIsValidMesh3() throws Exception {
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "blablabla");
    assertFalse(meshParser.isValidMeshId(meshID));

  }

  @Test
  public void testIsValidMesh2() throws Exception {
    assertFalse(meshParser.isValidMeshId(null));
  }

  @Test
  public void testIsValidWithSpace() throws Exception {
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "some disease");
    assertFalse(meshParser.isValidMeshId(meshID));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetInvalidMesh() throws Exception {
    meshParser.getMeSH(null);
  }

  @Test
  public void testGetRotenoneMesh() throws Exception {
    // Rotenone disease -no synonyms
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D012402");
    MeSH mesh = meshParser.getMeSH(meshID);
    assertTrue(mesh != null);
    assertTrue(mesh.getMeSHId() != null);
    assertTrue(mesh.getName() != null);
    assertTrue(mesh.getName().toLowerCase().equals("rotenone"));
    assertTrue(mesh.getDescription() != null);
    assertEquals(0, mesh.getSynonyms().size());
  }

  @Test
  public void testCreateChemicalListFromDB3() throws Exception {
    // One synonyms
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "C503102");
    MeSH mesh = meshParser.getMeSH(meshID);
    assertTrue(mesh != null);
    assertTrue(mesh.getMeSHId() != null);
    assertTrue(mesh.getName() != null);
    assertTrue(
        mesh.getName().toLowerCase().contains("1-(3-(2-(1-benzothiophen-5-yl) ethoxy) propyl)-3-azetidinol maleate"));
    assertNull(mesh.getDescription());
    assertEquals(1, mesh.getSynonyms().size());
  }

  @Test(expected = AnnotatorException.class)
  public void testGetMEshWithNetworkProblems() throws Exception {
    // Parkinson disease
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
    MeSHParser parserUnderTest = new MeSHParser();
    WebPageDownloader webPageDownloader = Mockito.mock(WebPageDownloader.class);
    when(webPageDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    parserUnderTest.setWebPageDownloader(webPageDownloader);

    parserUnderTest.getMeSH(meshID);
  }

  /**
   * @throws Exception
   */
  @Test
  public void testExternalDBStatus() throws Exception {
    ExternalServiceStatus status = meshParser.getServiceStatus();
    logger.debug("Status of MeSH DB : " + status.getStatus().name());
    assertTrue(status.getStatus().equals(ExternalServiceStatusType.OK));
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D010300");

    Object result = meshParser.refreshCacheQuery(meshParser.getIdentifier(meshID));
    assertNotNull(result);
  }

  @Test
  public void testCleanHtml() {
    String withouthHtml = meshParser.cleanHtml(
        "<TD colspan=1>One of the catecholamine <A href=\"/cgi/mesh/2014/MB_cgi?mode=&term=NEUROTRANSMITTERS\">");
    assertFalse(withouthHtml.contains("<"));
    assertFalse(withouthHtml.contains(">"));
    assertTrue(withouthHtml.contains("catecholamine"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    meshParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    meshParser.refreshCacheQuery(new Object());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery3() throws Exception {
    String query = MeSHParser.MESH_PREFIX + "::::::";
    meshParser.refreshCacheQuery(query);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshWhenProblemWithSourceDb() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    GeneralCacheInterface cache = meshParser.getCache();
    try {
      meshParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      meshParser.setWebPageDownloader(mockDownloader);

      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D010300");

      meshParser.refreshCacheQuery(meshParser.getIdentifier(meshID));
    } finally {
      meshParser.setWebPageDownloader(downloader);
      meshParser.setCache(cache);
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      meshParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, meshParser.getServiceStatus().getStatus());
    } finally {
      meshParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(null, HttpStatus.SC_NOT_FOUND));
      meshParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, meshParser.getServiceStatus().getStatus());
    } finally {
      meshParser.setWebPageDownloader(downloader);
    }
  }

}
