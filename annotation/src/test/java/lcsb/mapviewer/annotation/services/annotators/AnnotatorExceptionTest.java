package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class AnnotatorExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    AnnotatorException e = new AnnotatorException("messss");
    assertEquals("messss", e.getMessage());
  }

}
