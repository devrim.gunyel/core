package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class TairAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  TairAnnotator tairAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotate1() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200950")); // AT1G01030

    tairAnnotator.annotateElement(protein);

    MiriamData mdUniprot = null;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        mdUniprot = md;
      }
    }

    assertTrue("No UNIPROT annotation extracted from TAIR annotator", mdUniprot != null);
    assertTrue("Invalid UNIPROT annotation extracted from TAIR annotator",
        mdUniprot.getResource().equalsIgnoreCase("Q9MAN1"));
  }

  @Test
  // @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateExistingUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200427"));
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P32246")); // Human version of the protein

    tairAnnotator.annotateElement(protein);

    int cntUniProts = 0;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        cntUniProts++;
      }
    }

    assertTrue("No UNIPROT annotation extracted from TAIR annotator", cntUniProts > 1);
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    tairAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidTairToUniprot() throws Exception {
    assertNull(tairAnnotator.tairToUniprot(null));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidTairToUniprot2() throws Exception {
    tairAnnotator.tairToUniprot(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
  }

  @Test
  public void testAnnotateInvalid() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    tairAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testTairToUniprot() throws Exception {
    assertTrue(tairAnnotator.tairToUniprot(new MiriamData(MiriamType.TAIR_LOCUS, "2200950")) // TAIR locus AT1G01030
        .contains(new MiriamData(MiriamType.UNIPROT, "Q9MAN1")));
  }

  @Test
  public void testTairToUniprotFromKEGG() throws Exception {
    // TAIR Loci comming from annotators should be ignored by TAIR (only TAIR LOCI
    // provided by the human annotator should be considered)
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200427", KeggAnnotator.class));
    tairAnnotator.annotateElement(protein);
    assertTrue(protein.getMiriamData().size() == 1);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, tairAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = tairAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      tairAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, tairAnnotator.getServiceStatus().getStatus());
    } finally {
      tairAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = tairAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      tairAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, tairAnnotator.getServiceStatus().getStatus());
    } finally {
      tairAnnotator.setWebPageDownloader(downloader);
    }
  }

}
