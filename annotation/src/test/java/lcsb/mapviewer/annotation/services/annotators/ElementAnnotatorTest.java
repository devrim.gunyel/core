package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.user.annotator.*;

public class ElementAnnotatorTest extends AnnotationTestFunctions {

  static AnnotatorData allOutputFieldsAndAnnotations = new AnnotatorData(Object.class);
  ElementAnnotator annotator = Mockito.mock(ElementAnnotator.class, Mockito.CALLS_REAL_METHODS);
  @Autowired
  HgncAnnotator autowiredAnnotator;

  {
    for (BioEntityField field : BioEntityField.values()) {
      allOutputFieldsAndAnnotations.addAnnotatorParameter(new AnnotatorOutputParameter(field));
    }
    for (MiriamType type : MiriamType.values()) {
      allOutputFieldsAndAnnotations.addAnnotatorParameter(new AnnotatorOutputParameter(type));
    }
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetNotMatchingSymbol() {
    GenericProtein species = new GenericProtein("id");
    species.setSymbol("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSymbol("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingFullname() {
    GenericProtein species = new GenericProtein("id");
    species.setFullName("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setFullName("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingNotes() {
    GenericProtein species = new GenericProtein("id");
    species.setNotes("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setDescription("Y");

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testSetEmptyDescription() {
    GenericProtein species = new GenericProtein("id");
    species.setNotes("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setDescription(null);
    assertEquals("X", species.getNotes());
  }

  @Test
  public void testSetNotMatchingIchi() {
    Ion species = new Ion("id");
    species.setInChI("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setInchi("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingSmile() {
    Ion species = new Ion("id");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSmile("X");
    proxy.setSmile("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingCharge() {
    Ion species = new Ion("id");
    species.setCharge(2);
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setCharge("3");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingSubsystem() {
    Reaction species = new Reaction();
    species.setSubsystem("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSubsystem("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingFormula() {
    Reaction species = new Reaction();
    species.setFormula("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setFormula("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingAbbreviation() {
    Reaction species = new Reaction();
    species.setAbbreviation("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setAbbreviation("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingMCS() {
    Reaction species = new Reaction();
    species.setMechanicalConfidenceScore(1);
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setMechanicalConfidenceScore("2");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetTheSameMcs() throws Exception {
    Reaction reaction = new Reaction();
    reaction.setMechanicalConfidenceScore(4);
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(reaction, allOutputFieldsAndAnnotations);
    proxy.setMechanicalConfidenceScore("4");
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAddKegg() {
    Reaction reaction = new Reaction();
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(reaction, allOutputFieldsAndAnnotations);
    proxy.addMiriamData(new MiriamData(MiriamType.KEGG_COMPOUND, "R123"));
    assertEquals(1, reaction.getMiriamData().size());
  }

  @Test
  public void testSetSynonymsWarning() {
    GenericProtein species = new GenericProtein("1");
    species.addSynonym("X");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSynonyms(Arrays.asList("Y"));

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetTheSameSynonyms() {
    GenericProtein species = new GenericProtein("1");
    species.addSynonym("X");
    species.addSynonym("Y");
    ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSynonyms(Arrays.asList("Y", "X"));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    Object res = autowiredAnnotator.refreshCacheQuery("http://google.cz/");
    assertNotNull(res);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    autowiredAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    autowiredAnnotator.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = autowiredAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = autowiredAnnotator.getCache();
    try {
      // exclude first cached value
      autowiredAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      autowiredAnnotator.setWebPageDownloader(mockDownloader);
      autowiredAnnotator.refreshCacheQuery("http://google.pl/");
    } finally {
      autowiredAnnotator.setWebPageDownloader(downloader);
      autowiredAnnotator.setCache(originalCache);
    }
  }

}
