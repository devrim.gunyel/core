package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class ChemicalSearchExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    ChemicalSearchException e = new ChemicalSearchException("msg");
    assertEquals("msg", e.getMessage());
  }

}
