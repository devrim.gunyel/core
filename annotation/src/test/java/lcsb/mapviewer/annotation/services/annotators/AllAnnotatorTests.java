package lcsb.mapviewer.annotation.services.annotators;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnnotatorExceptionTest.class,
    AnnotatorExceptionTest.class,
    BrendaAnnotatorTest.class,
    CazyAnnotatorTest.class,
    ChebiAnnotatorTest.class,
    ElementAnnotatorTest.class,
    ElementAnnotatorImplementationsTest.class,
    EnsemblAnnotatorTest.class,
    EntrezAnnotatorTest.class,
    GoAnnotatorTest.class,
    HgncAnnotatorTest.class,
    KeggAnnotatorTest.class,
    MultipleAnnotatorsTest.class,
    PdbAnnotatorTest.class,
    ReconAnnotatorTest.class,
    StitchAnnotatorTest.class,
    StringAnnotatorTest.class,
    TairAnnotatorTest.class,
    UniprotAnnotatorTest.class,
})
public class AllAnnotatorTests {

}
