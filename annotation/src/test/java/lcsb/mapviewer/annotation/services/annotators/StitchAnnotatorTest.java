package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

public class StitchAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  StitchAnnotator testedAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotate() throws Exception {
    Species bioEntity = new SimpleMolecule("id");
    bioEntity.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:35697"));

    testedAnnotator.annotateElement(bioEntity);

    MiriamData mdStitch = null;

    for (MiriamData md : bioEntity.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.STITCH)) {
        mdStitch = md; // there should be only one EC number for that TAIR<->UNIPROT record
      }
    }

    assertTrue("No STITCH annotation extracted from STITCH annotator", mdStitch != null);
    assertTrue("Wrong number of annotations extract from STITCH annotator", bioEntity.getMiriamData().size() == 2);
    assertTrue("Invalid STITCH annotation extracted from STITCH annotator based on CHEBI ID",
        mdStitch.getResource().equalsIgnoreCase("WBYWAXJHAXSJNI"));
  }

  @Test
  public void testAnnotateInvalidEmpty() throws Exception {
    Species bioEntity = new SimpleMolecule("id");
    testedAnnotator.annotateElement(bioEntity);

    assertEquals(0, bioEntity.getMiriamData().size());
  }

  @Test
  public void testAnnotateInvalidChebi() throws Exception {
    Species bioEntity = new SimpleMolecule("id");
    bioEntity.addMiriamData(new MiriamData(MiriamType.CHEBI, "bla"));
    testedAnnotator.annotateElement(bioEntity);

    assertEquals(1, bioEntity.getMiriamData().size());
  }
}
