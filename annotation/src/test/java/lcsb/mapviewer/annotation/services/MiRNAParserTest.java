package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

public class MiRNAParserTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(MiRNAParserTest.class);

  @Autowired
  MiRNAParser miRNAParser;

  @Autowired
  private PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testExternalDBStatus() throws Exception {
    ExternalServiceStatus status = miRNAParser.getServiceStatus();
    assertEquals(status.getStatus(), ExternalServiceStatusType.OK);
  }

  @Test
  public void testFindMultipleByIDFromDB() throws Exception {
    List<String> names = new ArrayList<>();
    names.add("gga-miR-489-3p");
    names.add("rno-miR-9a-5p");
    List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
    assertTrue(!listFromDB.isEmpty());
    assertEquals(2, listFromDB.size());
    assertEquals(1, listFromDB.get(0).getTargets().size());
    assertEquals(5, listFromDB.get(1).getTargets().size());
  }

  @Test
  public void testFindById() throws Exception {
    List<String> names = new ArrayList<>();
    names.add("mmu-miR-122-5p");
    List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
    assertEquals(1, listFromDB.size());
    MiRNA mirna = listFromDB.get(0);
    for (Target target : mirna.getTargets()) {
      assertNotNull(target.getName());
      assertEquals(new MiriamData(MiriamType.TAXONOMY, "10090"), target.getOrganism());
    }
  }

  @Test(expected = MiRNASearchException.class)
  public void testFindWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = miRNAParser.getTaxonomyBackend();
    GeneralCacheInterface cache = miRNAParser.getCache();
    try {
      List<String> names = new ArrayList<>();
      names.add("mmu-miR-122-5p");
      miRNAParser.setCache(null);
      TaxonomyBackend backend = Mockito.mock(TaxonomyBackend.class);
      when(backend.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
      miRNAParser.setTaxonomyBackend(backend);
      miRNAParser.getMiRnasByNames(names);
    } finally {
      miRNAParser.setCache(cache);
      miRNAParser.setTaxonomyBackend(taxonomyBackend);
    }
  }

  @Test
  public void testFindById2() throws Exception {
    List<String> names = new ArrayList<>();
    names.add("hsa-miR-122-5p");
    List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
    assertEquals(1, listFromDB.size());
    MiRNA mirna = listFromDB.get(0);
    for (Target target : mirna.getTargets()) {
      assertNotNull(target.getName());
    }
  }

  @Test
  public void testFindByTarget() throws Exception {
    Set<MiriamData> targets = new HashSet<MiriamData>();
    MiriamData hgncTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "PLAG1");
    targets.add(hgncTarget);
    List<MiRNA> listFromDB = miRNAParser.getMiRnaListByTargets(targets);
    assertTrue(listFromDB.size() > 0);
    for (MiRNA miRNA : listFromDB) {
      boolean found = false;
      for (Target targt : miRNA.getTargets()) {
        if (targt.getGenes().contains(hgncTarget)) {
          found = true;
        }
      }
      assertTrue(found);
    }
  }

  @Test
  public void testFindByTargetWithAnnotator() throws Exception {
    Set<MiriamData> targets = new HashSet<>();
    MiriamData hgncTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "PLAG1", String.class);
    targets.add(hgncTarget);
    List<MiRNA> listFromDB = miRNAParser.getMiRnaListByTargets(targets);
    assertTrue(listFromDB.size() > 0);
    hgncTarget.setAnnotator(null);
    for (MiRNA miRNA : listFromDB) {
      boolean found = false;
      for (Target targt : miRNA.getTargets()) {
        if (targt.getGenes().contains(hgncTarget)) {
          found = true;
        }
      }
      assertTrue(found);
    }
  }

  @Test
  public void testGetMiRna() throws Exception {
    String mirBaseName = "hsa-miR-296-5p";
    List<String> names = new ArrayList<>();
    names.add(mirBaseName);
    List<MiRNA> list = miRNAParser.getMiRnasByNames(names);
    assertTrue(!list.isEmpty());
    MiRNA mirna = list.get(0);

    for (Target target : mirna.getTargets()) {
      Set<MiriamData> mds = new HashSet<>();
      mds.addAll(target.getReferences());
      assertEquals("Target contains duplicat references: " + target, mds.size(), target.getReferences().size());
    }
  }

  @Test
  public void invalidMiRNAToString2() throws Exception {
    String str = miRNAParser.getMiRnaSerializer().objectToString(null);
    assertNull(str);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    miRNAParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    miRNAParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    Object result = miRNAParser.refreshCacheQuery(MiRNAParser.MI_RNA_PREFIX + "mmu-miR-122-5p");
    assertNotNull(result);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = miRNAParser.getTaxonomyBackend();
    try {
      TaxonomyBackend mockBackend = Mockito.mock(TaxonomyBackend.class);
      when(mockBackend.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
      miRNAParser.setTaxonomyBackend(mockBackend);
      miRNAParser.refreshCacheQuery(MiRNAParser.MI_RNA_PREFIX + "mmu-miR-122-5p");
    } finally {
      miRNAParser.setTaxonomyBackend(taxonomyBackend);
    }
  }

  @Test
  public void testRefreshCacheQuery2() throws Exception {
    Object result = miRNAParser
        .refreshCacheQuery(MiRNAParser.MI_RNA_TARGET_PREFIX + MiriamType.HGNC_SYMBOL + ":" + "PLAG1");
    assertNotNull(result);
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-1);

    List<String> result = miRNAParser.getSuggestedQueryList(project);

    assertEquals(0, result.size());
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-2);
    Model model = getModelForFile("testFiles/target_mirna/target.xml", false);
    project.addModel(model);

    List<String> result = miRNAParser.getSuggestedQueryList(project);

    assertTrue(result.size() > 0);
  }

}
