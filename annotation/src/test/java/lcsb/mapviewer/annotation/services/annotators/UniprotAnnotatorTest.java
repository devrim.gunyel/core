package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collection;

import org.apache.http.HttpStatus;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class UniprotAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  UniprotAnnotator uniprotAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotate1() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("P12345");
    uniprotAnnotator.annotateElement(protein);

    assertTrue(protein.getMiriamData().size() > 0);

    boolean entrez = false;
    boolean hgnc = false;
    boolean uniprot = false;
    boolean ec = false;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        uniprot = true;
      } else if (md.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
        hgnc = true;
      } else if (md.getDataType().equals(MiriamType.ENTREZ)) {
        entrez = true;
      } else if (md.getDataType().equals(MiriamType.EC)) {
        ec = true;
      }
    }
    assertTrue("No HGNC annotation extracted from uniprot annotator", hgnc);
    assertTrue("No ENTREZ annotation extracted from uniprot annotator", entrez);
    assertTrue("No UNIPROT annotation extracted from uniprot annotator", uniprot);
    assertTrue("No UNIPROT annotation extracted from uniprot annotator", ec);
  }

  @Test
  public void testEC1() throws Exception {
    Collection<MiriamData> mds = uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.UNIPROT, "P12345"));

    assertEquals(mds.size(), 2);
    MiriamData md1 = new MiriamData(MiriamType.EC, "2.6.1.1");
    MiriamData md2 = new MiriamData(MiriamType.EC, "2.6.1.7");
    for (MiriamData md : mds) {
      assertTrue(md.compareTo(md1) == 0 || md.compareTo(md2) == 0);
    }
  }

  @Test
  public void testEC2() throws Exception {
    Collection<MiriamData> mds = uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.UNIPROT, "P25405"));

    assertTrue("No EC miriam data extracted from uniprot annotator", mds.size() > 0);
  }

  @Test
  public void testInvalidUniprotToECNull() throws Exception {
    assertNotNull(uniprotAnnotator.uniProtToEC(null));
    assertEquals(0, uniprotAnnotator.uniProtToEC(null).size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidUniprotToECWrongMd() throws Exception {
    uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
  }

  @Test
  public void testAnnotateInvalid2() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(new IOException(), HttpStatus.SC_NOT_FOUND));
      uniprotAnnotator.setWebPageDownloader(mockDownloader);

      Species protein = new GenericProtein("id");
      uniprotAnnotator.annotateElement(protein);

      assertEquals(1, getWarnings().size());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testInvalidUniprotToHgnc() throws Exception {
    assertNull(uniprotAnnotator.uniProtToHgnc(null));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidUniprotToHgnc2() throws Exception {
    uniprotAnnotator.uniProtToHgnc(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
  }

  @Test
  public void testAnnotate2() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P01308"));
    uniprotAnnotator.annotateElement(protein);

    assertTrue(protein.getMiriamData().size() > 1);

    boolean entrez = false;
    boolean hgnc = false;
    boolean uniprot = false;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        uniprot = true;
      } else if (md.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
        hgnc = true;
      } else if (md.getDataType().equals(MiriamType.ENTREZ)) {
        entrez = true;
      }
    }
    assertTrue("No HGNC annotation extracted from uniprot annotator", hgnc);
    assertTrue("No ENTREZ annotation extracted from uniprot annotator", entrez);
    assertTrue("No UNIPROT annotation extracted from uniprot annotator", uniprot);
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(new IOException(), HttpStatus.SC_NOT_FOUND));
      uniprotAnnotator.setWebPageDownloader(mockDownloader);

      Species protein = new GenericProtein("id");
      protein.setName("bla");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
      uniprotAnnotator.annotateElement(protein);

      assertEquals(1, protein.getMiriamData().size());

      assertEquals(2, getWarnings().size());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testAnnotateInvalid() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(new IOException(), HttpStatus.SC_NOT_FOUND));
      uniprotAnnotator.setWebPageDownloader(mockDownloader);

      Species protein = new GenericProtein("id");
      protein.setName("bla");
      uniprotAnnotator.annotateElement(protein);

      assertEquals(0, protein.getMiriamData().size());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testUniprotToHgnc() throws Exception {
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "KDR"),
        uniprotAnnotator.uniProtToHgnc(new MiriamData(MiriamType.UNIPROT, "P35968")));
  }

  @Test
  public void testUniprotTairLocusToId() throws Exception {
    assertEquals(new MiriamData(MiriamType.TAIR_LOCUS, "2201786"),
        uniprotAnnotator.uniprotTairLocusNameToId("AT1G08510"));
  }

  @Test
  public void testUniprotWrongTairLocusToId() throws Exception {
    assertEquals(new MiriamData(),
        uniprotAnnotator.uniprotTairLocusNameToId("bla"));
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, uniprotAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, uniprotAnnotator.getServiceStatus().getStatus());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test(expected = AnnotatorException.class)
  public void testAnnotateWithUniprotServerError() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    GeneralCacheInterface cache = uniprotAnnotator.getCache();
    uniprotAnnotator.setCache(null);
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      Species protein = new GenericProtein("id");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P01308"));
      uniprotAnnotator.annotateElement(protein);
    } finally {
      uniprotAnnotator.setCache(cache);
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateDownStatus2() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, uniprotAnnotator.getServiceStatus().getStatus());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, uniprotAnnotator.getServiceStatus().getStatus());
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

}
