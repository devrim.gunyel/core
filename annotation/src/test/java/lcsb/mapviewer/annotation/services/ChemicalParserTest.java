package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.*;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.*;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

@Ignore("ctd decided to block us due to too many requests")
public class ChemicalParserTest extends AnnotationTestFunctions {
  final MiriamData parkinsonDiseaseId = new MiriamData(MiriamType.MESH_2012, "D010300");
  final MiriamData dystoniaDisease = new MiriamData(MiriamType.MESH_2012, "C538007");
  Logger logger = LogManager.getLogger(ChemicalParserTest.class);
  MiriamData glutathioneDisulfideChemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D019803");
  MiriamData stilbeneOxideChemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "C025906");

  // lazabemide with one Gene MAOB and therapeutic and 3 publications
  MiriamData lazabemideChemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "C059303");

  @Autowired
  private ChemicalParser chemicalParser;

  @Autowired
  private GeneralCacheInterface cache;

  @Autowired
  private PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCache;

  @Test
  public void testCreateChemicalListFromDB() throws Exception {
    // skip first call to cache, so we will have to at least parse the data
    // Parkinson disease
    Map<MiriamData, String> result = chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
    assertNotNull(result);
    assertTrue(!result.isEmpty());
  }

  @Test(expected = ChemicalSearchException.class)
  public void testGetChemicalListWhenProblemWithConnection() throws Exception {
    GeneralCacheInterface cache = chemicalParser.getCache();
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      // skip first call to cache, so we will have to at least parse the data
      chemicalParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      // Parkinson disease
      chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
    } finally {
      chemicalParser.setCache(cache);
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetChemicalListForInvalidDiseaseId() throws Exception {
    chemicalParser.getChemicalsForDisease(null);
  }

  @Test
  public void testCreateChemicalListFromDBWithInvalidId() throws Exception {
    // Parkinson disease
    MiriamData diseaseID = new MiriamData(MiriamType.MESH_2012, "D01030012");
    Map<MiriamData, String> result = chemicalParser.getChemicalsForDisease(diseaseID);
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  @Test
  public void testFindByIDFromChemicalList() throws Exception {
    List<MiriamData> idsList = new ArrayList<>();
    idsList.add(stilbeneOxideChemicalId);
    idsList.add(lazabemideChemicalId);

    List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
    assertEquals(idsList.size(), chemcials.size());
    for (Chemical chem : chemcials) {
      assertNotNull(chem);
      for (Target t : chem.getInferenceNetwork()) {
        for (MiriamData md : t.getReferences()) {
          assertEquals(MiriamType.PUBMED, md.getDataType());
          assertTrue(NumberUtils.isDigits(md.getResource()));
        }
      }
    }
  }

  @Test
  public void testGetGlutathioneDisulfideData() throws Exception {
    List<MiriamData> idsList = new ArrayList<>();
    idsList.add(glutathioneDisulfideChemicalId);
    List<Chemical> chemicals = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);

    assertEquals(1, chemicals.size());
    Chemical glutathioneDisulfide = chemicals.get(0);

    assertTrue(glutathioneDisulfide.getSynonyms().size() > 0);
  }

  @Test
  public void testGetChemicalBySynonym() throws Exception {
    String glutathioneDisulfideSynonym = "GSSG";
    List<Chemical> chemicals = chemicalParser.getChemicalsBySynonym(parkinsonDiseaseId, glutathioneDisulfideSynonym);

    assertEquals(1, chemicals.size());
    Chemical mptp = chemicals.get(0);

    assertTrue(mptp.getSynonyms().contains(glutathioneDisulfideSynonym));
  }

  @Test
  public void testFindByInvalidIdFromChemicalList() throws Exception {
    // Parkinson disease
    MiriamData chemicalId2 = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D0198012433");

    List<MiriamData> idsList = new ArrayList<MiriamData>();
    idsList.add(chemicalId2);

    List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);

    assertTrue(chemcials.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testFindByInvalidDiseaseIdFromChemicalList() throws Exception {
    chemicalParser.getChemicals(null, new ArrayList<>());
  }

  @Test(timeout = 30000)
  public void testCachableInterfaceInvalidateChemical() throws Exception {
    Map<MiriamData, String> listFromDB = chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
    assertNotNull(listFromDB);
    assertTrue(!listFromDB.isEmpty());
    List<MiriamData> idsList = new ArrayList<>();
    idsList.add(stilbeneOxideChemicalId);
    List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
    assertNotNull(chemcials);
    assertTrue(chemcials.size() > 0);

    cache.invalidateByQuery(chemicalParser.getIdentifier(parkinsonDiseaseId, chemcials.get(0).getChemicalId()),
        chemicalParser.getCacheType());

    permanentDatabaseLevelCache.waitToFinishTasks();

    idsList.clear();

    idsList.add(chemcials.get(0).getChemicalId());

    List<Chemical> chemcials2 = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
    assertNotNull(chemcials2);
    assertTrue(chemcials2.size() > 0);

    assertFalse("Value wasn't refreshed from db", chemcials.get(0).equals(chemcials2.get(0)));
  }

  @Test
  public void testGetByTarget() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "GCH1");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);

    List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, dystoniaDisease);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void testGetByTargetWithAnnotator() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "GCH1", String.class);
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);

    List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, dystoniaDisease);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetByInvalidTarget() throws Exception {
    MiriamData target = new MiriamData(MiriamType.WIKIPEDIA, "TNF");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
  }

  @Test
  public void testGetByUnknownHgnc() throws Exception {
    // Parkinson disease
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "UNK_HGNC");
    chemicalParser.getChemicalListByTarget(target, parkinsonDiseaseId);
    // we have one warning regarding unknown hgnc symbol (we shouldn't throw
    // exception here bcuase these symbols sometimes are some
    // generic gene names)
    assertEquals(1, getWarnings().size());
  }

  @Test(expected = ChemicalSearchException.class)
  public void testGetByTargetWhenProblemWithHgncConnection() throws Exception {
    HgncAnnotator annotator = chemicalParser.getHgncAnnotator();
    try {
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToEntrez(any())).thenThrow(new AnnotatorException(""));
      chemicalParser.setHgncAnnotator(mockAnnotator);
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "UNK_HGNC");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
    } finally {
      chemicalParser.setHgncAnnotator(annotator);
    }
  }

  @Test
  public void testGetByEntrezTarget() throws Exception {
    MiriamData target = new MiriamData(MiriamType.ENTREZ, "2643");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, dystoniaDisease);
    assertTrue(list.size() > 0);
  }

  @Test
  public void testGetByTarget2() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "GALM");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }

  @Test
  public void testRefreshQuery() throws Exception {
    Object object = chemicalParser.refreshCacheQuery(ChemicalParser.DISEASE_CHEMICALS_PREFIX + "D010300");
    assertNotNull(object);
  }

  @Test
  public void testRefreshQuery2() throws Exception {
    Object object = chemicalParser.refreshCacheQuery("https://www.google.pl/");
    assertNotNull(object);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    chemicalParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    chemicalParser.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWhenCtdDbNotAvailable() throws Exception {
    MiriamData chemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039");

    String query = chemicalParser.getIdentifier(parkinsonDiseaseId, chemicalId);

    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemicalParser.getCache();
    try {
      // disable cache
      chemicalParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      chemicalParser.setWebPageDownloader(mockDownloader);
      chemicalParser.refreshCacheQuery(query);
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
      chemicalParser.setCache(cache);
    }
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, chemicalParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, chemicalParser.getServiceStatus().getStatus());
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      chemicalParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemicalParser.getServiceStatus().getStatus());
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetChemicalFromChemicalLineWithInvalidHgnc() throws Exception {
    String line[] = new String[ChemicalParser.CHEMICALS_COUNT_COL + 1];
    for (int i = 0; i < line.length; i++) {
      line[i] = "";
    }
    line[ChemicalParser.CHEMICALS_DIRECT_COL] = ChemicalDirectEvidence.THERAPEUTIC.getValue();
    line[ChemicalParser.CHEMICALS_NETWORK_COL] = "UNK_HGNC";
    Chemical result = chemicalParser.getChemicalFromChemicalLine(line,
        new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039000"));
    // we have two errors related to parsing inference score and reference
    // count
    assertEquals(2, getErrors().size());
    // there are no genes (we have one that is invalid and should be
    // discarded)
    assertEquals(0, result.getInferenceNetwork().size());

    assertEquals(ChemicalDirectEvidence.THERAPEUTIC, result.getDirectEvidence());
  }

  @Test(expected = ChemicalSearchException.class)
  public void testGetChemicalFromChemicalLineWithHgncConnectionProblem() throws Exception {
    HgncAnnotator hgncAnnotator = chemicalParser.getHgncAnnotator();
    try {
      String line[] = new String[ChemicalParser.CHEMICALS_COUNT_COL + 1];
      for (int i = 0; i < line.length; i++) {
        line[i] = "";
      }
      line[ChemicalParser.CHEMICALS_DIRECT_COL] = ChemicalDirectEvidence.THERAPEUTIC.getValue();
      line[ChemicalParser.CHEMICALS_NETWORK_COL] = "UNK_HGNC";
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToEntrez(any())).thenThrow(new AnnotatorException(""));
      chemicalParser.setHgncAnnotator(mockAnnotator);
      chemicalParser.getChemicalFromChemicalLine(line, new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039000"));
    } finally {
      chemicalParser.setHgncAnnotator(hgncAnnotator);
    }
  }

  @Test(expected = ChemicalSearchException.class)
  public void testProblemWithPubmeds() throws Exception {
    GeneralCacheInterface cache = chemicalParser.getCache();
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      // Parkinson disease
      MiriamData chemicalID = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039");

      List<MiriamData> idsList = new ArrayList<MiriamData>();
      idsList.add(chemicalID);

      // remove info about single publication from cache (so we will have to download
      // it)
      chemicalParser.getCache().removeByQuery(
          "https://ctdbase.org/detail.go?6578706f7274=1&d-1340579-e=5&type=relationship&ixnId=4802115",
          chemicalParser.getCacheType());

      chemicalParser.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
    } finally {
      chemicalParser.setCache(cache);
      chemicalParser.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-1);

    List<String> result = chemicalParser.getSuggestedQueryList(project, parkinsonDiseaseId);

    assertEquals(0, result.size());
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    Project project = new Project();
    Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
    project.addModel(model);

    List<String> result = chemicalParser.getSuggestedQueryList(project, dystoniaDisease);

    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetSuggestedQueryListForUnknownDiseas() throws Exception {
    Project project = new Project();
    Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
    project.addModel(model);

    List<String> result = chemicalParser.getSuggestedQueryList(project, null);

    assertEquals(0, result.size());
  }

}
