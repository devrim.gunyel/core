package lcsb.mapviewer.annotation.services.genome;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.*;

import org.apache.commons.net.ftp.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;

public class UcscReferenceGenomeConnectorTest extends AnnotationTestFunctions {

  Logger logger = LogManager.getLogger(UcscReferenceGenomeConnectorTest.class);

  @Autowired
  UcscReferenceGenomeConnector connector;

  @Autowired
  ReferenceGenomeDao referenceGenomeDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    assertNotNull(connector.refreshCacheQuery("http://google.pl/"));
  }

  @Test
  public void testGetAvailableGenomeVersion() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    List<String> list = connector.getAvailableGenomeVersion(human);
    assertTrue(list.size() >= 17);
  }

  @Test(expected = ReferenceGenomeConnectorException.class)
  public void testGetAvailableGenomeVersionWhenProblemWithTaxonomyServer() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    TaxonomyBackend taxonomyBackend = connector.getTaxonomyBackend();
    try {
      // simulate problems with taxonomy server
      TaxonomyBackend taxonomyMock = Mockito.mock(TaxonomyBackend.class);
      when(taxonomyMock.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
      connector.setTaxonomyBackend(taxonomyMock);

      connector.getAvailableGenomeVersion(human);
    } finally {
      connector.setTaxonomyBackend(taxonomyBackend);
    }
  }

  @Test
  public void testGetAvailableGenomeVersionForInvalidTaxonomy() throws Exception {
    MiriamData unknown = new MiriamData(MiriamType.TAXONOMY, "10101010101");
    List<String> list = connector.getAvailableGenomeVersion(unknown);
    assertEquals(0, list.size());
  }

  @Test
  public void testGetAvailableOrganisms() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    MiriamData chicken = new MiriamData(MiriamType.TAXONOMY, "9031");
    List<MiriamData> list = connector.getAvailableOrganisms();
    assertTrue("At least 40 different organism were expected but found: " + list.size(), list.size() > 40);

    assertTrue(list.contains(human));
    assertTrue(list.contains(chicken));
  }

  @Test(expected = ReferenceGenomeConnectorException.class)
  public void testGetAvailableOrganismsWhenUcscWebpageDown() throws Exception {
    WebPageDownloader downloader = connector.getWebPageDownloader();
    GeneralCacheInterface originalCache = connector.getCache();
    try {
      // exclude first cached value
      connector.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      connector.setWebPageDownloader(mockDownloader);
      connector.getAvailableOrganisms();
    } finally {
      connector.setWebPageDownloader(downloader);
      connector.setCache(originalCache);
    }
  }

  @Test
  public void testDownloadGenomeVersion() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";
    dbUtils.createSessionForCurrentThread();
    List<String> list = connector.getDownloadedGenomeVersions(yeast);
    if (list.contains(version)) {
      connector.removeGenomeVersion(yeast, version);
    }
    dbUtils.closeSessionForCurrentThread();
    list = connector.getDownloadedGenomeVersions(yeast);

    assertFalse(list.contains(version));

    connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    }, false);

    list = connector.getDownloadedGenomeVersions(yeast);
    assertTrue(list.contains(version));

    List<ReferenceGenome> genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
    ReferenceGenome genome = null;
    for (ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getVersion().equals(version)) {
        genome = referenceGenome;
      }
    }
    assertNotNull(genome);
    connector.downloadGeneMappingGenomeVersion(genome, "test", null, false,
        "http://www.biodalliance.org/datasets/flyThickets.bb");

    dbUtils.createSessionForCurrentThread();
    genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
    genome = null;
    for (ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getVersion().equals(version)) {
        genome = referenceGenome;
      }
    }
    assertNotNull(genome);
    assertEquals(1, genome.getGeneMapping().size());
    dbUtils.closeSessionForCurrentThread();

    try {
      // try to download the same thing for the second time
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
      fail("Exception expected");
    } catch (ReferenceGenomeConnectorException e) {
      assertTrue(e.getMessage().contains("already exists"));
    }

    try {
      // try to download something that is not big bed
      connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, false, "http://www.biodalliance.org/datasets/flyThickets.txt");
      fail("Exception expected");
    } catch (ReferenceGenomeConnectorException e) {
      assertTrue(e.getMessage().contains("Only big bed format files are supported"));
    }

    // connector.removeGeneMapping(genome.getGeneMapping().get(0));

    int warningCount = getWarnings().size();

    // try to remove invalid mapping
    connector.removeGeneMapping(new ReferenceGenomeGeneMapping());

    assertEquals(warningCount + 1, getWarnings().size());

    dbUtils.createSessionForCurrentThread();
    connector.removeGenomeVersion(yeast, version);
    dbUtils.closeSessionForCurrentThread();
  }

  @SuppressWarnings("unchecked")
  @Test(expected = InvalidStateException.class)
  public void testDownloadGenomeVersionWhenInternalUrlIsInvalid() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    ExecutorService originalExecutorService = connector.getSyncExecutorService();
    try {
      ExecutorService executorService = Mockito.mock(ExecutorService.class);
      connector.setSyncExecutorService(executorService);
      Future<?> task = Mockito.mock(Future.class);
      when(task.get()).thenThrow(new ExecutionException(new URISyntaxException("", "")));
      when(executorService.submit(any(Callable.class))).thenReturn(task);

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, false);
    } finally {
      connector.setSyncExecutorService(originalExecutorService);
    }
  }

  @Test
  public void testDownloadGenomeVersionAsync() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      dbUtils.createSessionForCurrentThread();
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      dbUtils.closeSessionForCurrentThread();

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);
      Mockito.doAnswer(new Answer<Void>() {

        @Override
        public Void answer(InvocationOnMock invocation) throws Throwable {
          IProgressUpdater updater = (IProgressUpdater) invocation.getArguments()[2];
          updater.setProgress(100);
          return null;
        }
      }).when(mockCache).downloadFile(anyString(), anyBoolean(), any());

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, null, true);

      waitForDownload();

      dbUtils.createSessionForCurrentThread();
      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      dbUtils.closeSessionForCurrentThread();
      assertNotNull(genome);
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      referenceGenomeDao.refresh(genome);
      assertEquals(1, genome.getGeneMapping().size());

      try {
        // try to download the same thing for the second time
        connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (ReferenceGenomeConnectorException e) {
        assertTrue(e.getMessage().contains("already exists"));
      }

      try {
        // try to download something that is not big bed
        connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.txt");
        fail("Exception expected");
      } catch (ReferenceGenomeConnectorException e) {
        assertTrue(e.getMessage().contains("Only big bed format files are supported"));
      }

      connector.removeGeneMapping(genome.getGeneMapping().get(0));

      int warningCount = getWarnings().size();

      // try to remove invalid mapping
      connector.removeGeneMapping(new ReferenceGenomeGeneMapping());

      assertEquals(warningCount + 1, getWarnings().size());

      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  @Test
  public void testDownloadGenomeVersionAsyncWithException() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      dbUtils.createSessionForCurrentThread();
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      dbUtils.closeSessionForCurrentThread();

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, true);

      waitForDownload();

      dbUtils.createSessionForCurrentThread();
      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      dbUtils.closeSessionForCurrentThread();
      assertNotNull(genome);

      Mockito.doThrow(new IOException()).when(mockCache).downloadFile(anyString(), anyBoolean(), any());
      assertEquals(0, getErrors().size());
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      logger.debug(getErrors());
      // during this downlad we expect exception thrown by downloader
      assertEquals(1, getErrors().size());

      Mockito.doThrow(new OutOfMemoryError()).when(mockCache).downloadFile(anyString(), anyBoolean(), any());
      connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();
      // during this downlad we expect error, this shouldn't change list of
      // errors
      assertEquals(1, getErrors().size());

      Mockito.doThrow(new URISyntaxException("", "")).when(mockCache).downloadFile(anyString(), anyBoolean(), any());
      connector.downloadGeneMappingGenomeVersion(genome, "test3", new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      // during this downlad we expect error (exception thrown by downloader)
      assertEquals(2, getErrors().size());

      genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      referenceGenomeDao.refresh(genome);
      assertEquals(3, genome.getGeneMapping().size());

      connector.removeGeneMapping(genome.getGeneMapping().get(2));
      connector.removeGeneMapping(genome.getGeneMapping().get(1));
      connector.removeGeneMapping(genome.getGeneMapping().get(0));
      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  @Test
  public void testDownloadGenomeVersionWithException() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      }, false);

      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);

      Mockito.doThrow(new IOException()).when(mockCache).downloadFile(anyString(), anyBoolean(), any());
      assertEquals(0, getErrors().size());
      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (IOException e) {

      }

      Mockito.doThrow(new RuntimeException()).when(mockCache).downloadFile(anyString(), anyBoolean(), any());
      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");

        fail("Exception expected");
      } catch (InvalidStateException e) {

      }

      Mockito.doThrow(new URISyntaxException("", "")).when(mockCache).downloadFile(anyString(), anyBoolean(), any());

      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test3", new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (URISyntaxException e) {

      }

      genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      referenceGenomeDao.refresh(genome);
      assertEquals(3, genome.getGeneMapping().size());

      connector.removeGeneMapping(genome.getGeneMapping().get(2));
      connector.removeGeneMapping(genome.getGeneMapping().get(1));
      connector.removeGeneMapping(genome.getGeneMapping().get(0));
      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  @Test
  public void testGetGenomeVersionFile() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";
    String url = connector.getGenomeVersionFile(yeast, version);
    assertNotNull(url);
    url.contains("sacCer3.2bit");
    url.contains("ftp");
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetUnknownGenomeVersionFile() throws FileNotAvailableException {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    String version = "hg8";
    connector.getGenomeVersionFile(human, version);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalid() throws Exception {
    connector.refreshCacheQuery("invalid query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalid2() throws Exception {
    connector.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefresh() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    String query = UcscReferenceGenomeConnector.FILENAME_BY_ORGANISM_VERSION_PREFIX + yeast.getResource() + "\n"
        + version;
    Object res = connector.refreshCacheQuery(query);
    assertNotNull(res);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshUnknownFile() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "unkVer";

    String query = UcscReferenceGenomeConnector.FILENAME_BY_ORGANISM_VERSION_PREFIX + yeast.getResource() + "\n"
        + version;
    connector.refreshCacheQuery(query);
  }

  @Test
  public void testGetters() throws Exception {
    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null);
    connectorUnderTest.setBigFileCache(connector.getBigFileCache());
    connectorUnderTest.setDbUtils(connector.getDbUtils());
    connectorUnderTest.setReferenceGenomeDao(connector.getReferenceGenomeDao());
    connectorUnderTest.setReferenceGenomeGeneMappingDao(connector.getReferenceGenomeGeneMappingDao());

    assertEquals(connector.getBigFileCache(), connectorUnderTest.getBigFileCache());
    assertEquals(connector.getDbUtils(), connectorUnderTest.getDbUtils());
    assertEquals(connector.getReferenceGenomeDao(), connectorUnderTest.getReferenceGenomeDao());
    assertEquals(connector.getReferenceGenomeGeneMappingDao(), connectorUnderTest.getReferenceGenomeGeneMappingDao());
  }

  @Test
  public void testGenomeVersionExtractorForInvalidGenome() throws Exception {
    int version = connector.extractInt("");
    assertEquals(0, version);
  }

  @Test
  public void testGenomeVersionExtractorForInvalidGenome2() throws Exception {
    int version = connector.extractInt("x");
    assertEquals(0, version);
  }

  @Test
  public void testGenomeVersionExtractorForValid() throws Exception {
    int version = connector.extractInt("x1");
    assertEquals(1, version);
  }

  @Test
  public void testGenomeVersionExtractorForValid2() throws Exception {
    int version = connector.extractInt("xy12z");
    assertEquals(12, version);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = connector.getWebPageDownloader();
    GeneralCacheInterface originalCache = connector.getCache();
    try {
      // exclude first cached value
      connector.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      connector.setWebPageDownloader(mockDownloader);
      connector.refreshCacheQuery("http://google.pl/");
    } finally {
      connector.setWebPageDownloader(downloader);
      connector.setCache(originalCache);
    }
  }

  @Test(expected = FileNotAvailableException.class)
  public void testRefreshCacheQueryWhenFtpConnectionFails() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        Mockito.doReturn(FTPReply.REQUEST_DENIED).when(mockClient).getReplyCode();
        return mockClient;
      }
    };

    // exclude first cached value
    connectorUnderTest.getGenomeVersionFile(yeast, version);
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetGenomeVersionFileWhenFtpConnectionFails2() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        try {
          Mockito.doThrow(new IOException()).when(mockClient).connect(anyString());
          Mockito.doReturn(false).when(mockClient).isConnected();
        } catch (Exception e) {
          fail();
        }
        return mockClient;
      }
    };
    // exclude first cached value
    connectorUnderTest.getGenomeVersionFile(yeast, version);
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetGenomeVersionFileWhenFtpConnectionFails() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        try {
          Mockito.doThrow(new IOException()).when(mockClient).connect(anyString());
          Mockito.doReturn(true).when(mockClient).isConnected();
        } catch (Exception e) {
          fail();
        }
        return mockClient;
      }
    };
    // exclude first cached value
    connectorUnderTest.getGenomeVersionFile(yeast, version);
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetGenomeVersionFileWhenFtpConnectionFails3() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        try {
          Mockito.doThrow(new IOException()).when(mockClient).connect(anyString());
          Mockito.doReturn(true).when(mockClient).isConnected();
          Mockito.doThrow(new IOException()).when(mockClient).disconnect();
        } catch (Exception e) {
          fail();
        }
        return mockClient;
      }
    };

    // exclude first cached value
    connectorUnderTest.getGenomeVersionFile(yeast, version);
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetGenomeVersionFileWhenFtpDisconnectConnectionFails() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        try {
          Mockito.doThrow(new IOException()).when(mockClient).disconnect();
          Mockito.doReturn(new FTPFile[] {}).when(mockClient).listFiles(anyString());
          Mockito.doReturn(FTPReply.COMMAND_OK).when(mockClient).getReplyCode();
          Mockito.doReturn(true).when(mockClient).isConnected();
        } catch (Exception e) {
          fail();
        }
        return mockClient;
      }
    };

    // exclude first cached value
    connectorUnderTest.getGenomeVersionFile(yeast, version);
  }

  @Test
  public void testGetGenomeVersionFileWithMore2bitFiles() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";

    UcscReferenceGenomeConnector connectorUnderTest = new UcscReferenceGenomeConnector(null) {
      @Override
      FTPClient createFtpClient() {
        FTPClient mockClient = Mockito.mock(FTPClient.class);
        try {
          FTPFile file1 = new FTPFile();
          file1.setName("1.2bit");
          FTPFile file2 = new FTPFile();
          file2.setName("2.2bit");
          FTPFile[] files = new FTPFile[] { file1, file2 };

          Mockito.doReturn(files).when(mockClient).listFiles(anyString());
          Mockito.doReturn(FTPReply.COMMAND_OK).when(mockClient).getReplyCode();
        } catch (Exception e) {
          fail();
        }
        return mockClient;
      }
    };

    // exclude first cached value
    String url = connectorUnderTest.getGenomeVersionFile(yeast, version);
    assertEquals(1, getWarnings().size());
    assertTrue(url.endsWith("1.2bit"));
  }

  private void waitForDownload() throws InterruptedException {
    while (connector.getDownloadThreadCount() > 0) {
      logger.debug("Waiting for download to finish");
      Thread.sleep(100);
    }
  }
}
