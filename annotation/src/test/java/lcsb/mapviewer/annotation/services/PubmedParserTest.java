package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

public class PubmedParserTest extends AnnotationTestFunctions {
  @Autowired
  protected PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCacheInterface;
  Logger logger = LogManager.getLogger(PubmedParserTest.class);
  @Autowired
  PubmedParser pubmedParser;
  private boolean status2;
  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    status2 = Configuration.isApplicationCacheOn();
  }

  @After
  public void tearDown() throws Exception {
    Configuration.setApplicationCacheOn(status2);
  }

  @Test
  public void test() throws PubmedSearchException {
    Configuration.setApplicationCacheOn(false);
    Article art = pubmedParser.getPubmedArticleById(9481670);
    assertNotNull(art);
    assertEquals(
        "Adjacent asparagines in the NR2-subunit of the NMDA receptor channel control the voltage-dependent block by extracellular Mg2+.",
        art.getTitle());
    assertEquals((Integer) 1998, art.getYear());
    assertEquals("The Journal of physiology", art.getJournal());
    assertTrue(art.getStringAuthors().contains("Wollmuth"));
    assertTrue(art.getStringAuthors().contains("Kuner"));
    assertTrue(art.getStringAuthors().contains("Sakmann"));
  }

  @Test
  public void testCache() throws PubmedSearchException {
    Configuration.setApplicationCacheOn(true);

    Article art = pubmedParser.getPubmedArticleById(9481671);
    assertNotNull(art);
  }

  @Test
  public void testSerialization() throws InvalidXmlSchemaException, IOException {
    Article art = new Article();
    List<String> list = new ArrayList<String>();
    list.add("aaa");
    list.add("bbb");
    art.setTitle("ttt");
    art.setAuthors(list);
    art.setJournal("jjjj");
    art.setYear(123);

    String serialString = pubmedParser.getArticleSerializer().objectToString(art);

    assertTrue(serialString.contains("aaa"));
    assertTrue(serialString.contains("bbb"));
    assertTrue(serialString.contains("ttt"));
    assertTrue(serialString.contains("jjjj"));
    assertTrue(serialString.contains("123"));

    Article art2 = pubmedParser.getArticleSerializer().xmlToObject(getNodeFromXmlString(serialString));

    assertEquals(art.getStringAuthors(), art2.getStringAuthors());
    assertEquals(art.getTitle(), art2.getTitle());
    assertEquals(art.getJournal(), art2.getJournal());
    assertEquals(art.getYear(), art2.getYear());
  }

  /**
   * This case was problematic with old API used to retrieve data from pubmed
   * 
   * @throws PubmedSearchException
   */
  @Test
  public void testProblematicCase() throws PubmedSearchException {
    Article art = pubmedParser.getPubmedArticleById(22363258);
    assertNotNull(art);
  }

  @Test
  public void testCitationCount() throws PubmedSearchException {
    Article art = pubmedParser.getPubmedArticleById(18400456);
    assertNotNull(art);
    assertTrue(art.getCitationCount() >= 53);
  }

  @Test
  public void testGetSummary() throws Exception {
    String summary = pubmedParser.getSummary(18400456);
    assertNotNull(summary);
    assertFalse(summary.isEmpty());
  }

  @Test
  public void testGetSummary2() throws Exception {
    String summary = pubmedParser.getSummary(23644949);
    assertNull(summary);
  }

  @Test
  public void testGetSummary3() throws Exception {
    String summary = pubmedParser.getSummary("18400456");
    assertNotNull(summary);
    assertFalse(summary.isEmpty());
  }

  @Test
  public void testCachableInterface() throws Exception {
    String query = "pubmed: 2112";
    String newRes = "hello";
    permanentDatabaseLevelCacheInterface.waitToFinishTasks();

    cache.setCachedQuery(query, pubmedParser.getCacheType(), newRes);
    cache.invalidateByQuery(query, pubmedParser.getCacheType());

    permanentDatabaseLevelCacheInterface.waitToFinishTasks();

    String res = cache.getStringByQuery(query, pubmedParser.getCacheType());

    assertNotNull(res);

    assertFalse("Value wasn't refreshed from db", newRes.equals(res));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    pubmedParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshWhenExternalNotAvailable() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    try {
      pubmedParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      pubmedParser.setWebPageDownloader(mockDownloader);

      String query = PubmedParser.PUBMED_PREFIX + PubmedParser.SERVICE_STATUS_PUBMED_ID;

      pubmedParser.refreshCacheQuery(query);
    } finally {
      pubmedParser.setWebPageDownloader(downloader);
      pubmedParser.setCache(cache);
    }

  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    pubmedParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testGetHtmlFullLinkForId() throws Exception {
    String htmlString = pubmedParser.getHtmlFullLinkForId(1234);
    assertTrue(htmlString.contains("Change in the kinetics of sulphacetamide tissue distribution"));
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, pubmedParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      pubmedParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
    } finally {
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateDownStatus2() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<responseWrapper><version>" + PubmedParser.SUPPORTED_VERSION + "</version></responseWrapper>");
      pubmedParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
    } finally {
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(
          "<responseWrapper><version>" + PubmedParser.SUPPORTED_VERSION + "blabla</version></responseWrapper>");
      pubmedParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, pubmedParser.getServiceStatus().getStatus());
    } finally {
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetApiVersion() throws Exception {
    assertNotNull(pubmedParser.getApiVersion());
  }

  @Test(expected = PubmedSearchException.class)
  public void testGetApiVersionWhenProblemWithExternalDb() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    pubmedParser.setCache(null);
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("unknown response");
      pubmedParser.setWebPageDownloader(mockDownloader);
      pubmedParser.getApiVersion();
    } finally {
      pubmedParser.setCache(cache);
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetApiVersionWhenProblemWithExternalDb3() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    pubmedParser.setCache(null);
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("<xml/>");
      pubmedParser.setWebPageDownloader(mockDownloader);
      assertNull(pubmedParser.getApiVersion());
    } finally {
      pubmedParser.setCache(cache);
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test(expected = PubmedSearchException.class)
  public void testGetApiVersionWhenProblemWithExternalDb2() throws Exception {
    WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
    pubmedParser.setCache(null);
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      pubmedParser.setWebPageDownloader(mockDownloader);
      pubmedParser.getApiVersion();
    } finally {
      pubmedParser.setCache(cache);
      pubmedParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetters() throws Exception {
    MiriamConnector mc = new MiriamConnector();
    XmlSerializer<Article> serializer = new XmlSerializer<>(Article.class);

    PubmedParser parser = new PubmedParser(null);

    parser.setMiriamConnector(mc);
    assertEquals(mc, parser.getMiriamConnector());

    parser.setArticleSerializer(serializer);

    assertEquals(serializer, parser.getArticleSerializer());
  }

}
