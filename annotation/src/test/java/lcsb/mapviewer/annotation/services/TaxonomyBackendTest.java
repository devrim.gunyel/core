package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class TaxonomyBackendTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(TaxonomyBackendTest.class);

  @Autowired
  TaxonomyBackend taxonomyBackend;
  @Autowired
  private GeneralCacheInterface cache;

  @Test
  public void testByName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Human");
    assertTrue(md.equals(TaxonomyBackend.HUMAN_TAXONOMY));
  }

  @Test
  public void testByHumansName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Humans");
    assertTrue(md.equals(TaxonomyBackend.HUMAN_TAXONOMY));
  }

  @Test
  public void testByComplexName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("Aplysia californica (Sea Hare)"));
  }

  @Test
  public void testByCommonName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("Rat"));
  }

  @Test
  public void testByEmptyName() throws Exception {
    assertNull(taxonomyBackend.getByName(null));
    assertNull(taxonomyBackend.getByName(""));
  }

  @Test
  public void testByAbreviationName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("C. elegans"));
    assertNotNull(taxonomyBackend.getByName("D. sechellia"));
    assertNotNull(taxonomyBackend.getByName("P. pacificus"));
    assertNotNull(taxonomyBackend.getByName("T. castaneum"));
  }

  @Test
  public void testGetName() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9606"));
    logger.debug(name);
    assertTrue("Taxonomy id should refer to homo sapien, but this found: " + name, name.contains("Homo sapien"));
    assertNotNull(taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9605")));
  }

  @Test(expected = TaxonomySearchException.class)
  public void testGetNameWhenProblemWithDb() throws Exception {
    WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      taxonomyBackend.setWebPageDownloader(mockDownloader);
      taxonomyBackend.setCache(null);
      taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9606"));
    } finally {
      taxonomyBackend.setCache(cache);
      taxonomyBackend.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetNameForInvalidTax() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "-a-"));
    assertNull(name);
  }

  @Test
  public void testGetNameForInvalidInputData() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(null);
    assertNull(name);
  }

  @Test
  public void testByName2() throws Exception {
    MiriamData md = taxonomyBackend.getByName("homo sapiens");
    assertTrue(md.equals(TaxonomyBackend.HUMAN_TAXONOMY));
  }

  @Test
  public void testByName3() throws Exception {
    MiriamData md = taxonomyBackend.getByName("bla bla kiwi");
    assertNull(md);
  }

  @Test
  public void testByMusMusculusName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Mus musculus");
    assertTrue(md.equals(new MiriamData(MiriamType.TAXONOMY, "10090")));
  }

  @Test
  public void testByMouseName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Mouse");
    assertTrue(md.equals(new MiriamData(MiriamType.TAXONOMY, "10090")));
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    String result = taxonomyBackend.refreshCacheQuery(TaxonomyBackend.TAXONOMY_CACHE_PREFIX + "homo sapiens");
    assertNotNull(result);
  }

  @Test
  public void testRefreshCacheQuery2() throws Exception {
    String result = taxonomyBackend
        .refreshCacheQuery(TaxonomyBackend.TAXONOMY_NAME_CACHE_PREFIX + TaxonomyBackend.HUMAN_TAXONOMY.getResource());
    assertNotNull(result);
  }

  @Test
  public void testRefreshCacheQuery3() throws Exception {
    String result = taxonomyBackend.refreshCacheQuery("http://google.pl/");
    assertNotNull(result);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    taxonomyBackend.refreshCacheQuery("unk_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    taxonomyBackend.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      taxonomyBackend.setWebPageDownloader(mockDownloader);
      taxonomyBackend.setCache(null);

      taxonomyBackend.refreshCacheQuery("http://google.pl/");
    } finally {
      taxonomyBackend.setWebPageDownloader(downloader);
      taxonomyBackend.setCache(cache);
    }
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, taxonomyBackend.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      taxonomyBackend.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, taxonomyBackend.getServiceStatus().getStatus());
    } finally {
      taxonomyBackend.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      taxonomyBackend.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, taxonomyBackend.getServiceStatus().getStatus());
    } finally {
      taxonomyBackend.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<em>Taxonomy ID: </em>1234");
      taxonomyBackend.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, taxonomyBackend.getServiceStatus().getStatus());
    } finally {
      taxonomyBackend.setWebPageDownloader(downloader);
    }
  }

}
