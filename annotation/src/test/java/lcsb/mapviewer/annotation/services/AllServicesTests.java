package lcsb.mapviewer.annotation.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.annotation.services.annotators.AllAnnotatorTests;
import lcsb.mapviewer.annotation.services.genome.AllGenomeTests;

@RunWith(Suite.class)
@SuiteClasses({ AllAnnotatorTests.class,
    AllGenomeTests.class,
    ChEMBLParserTest.class,
    ChemicalParserTest.class,
    ChemicalSearchExceptionTest.class,
    DrugAnnotationTest.class,
    DrugbankHTMLParserTest.class,
    ExternalServiceStatusTest.class,
    ExternalServiceStatusTypeTest.class,
    ImproperAnnotationsTest.class,
    MeSHParserTest.class,
    MiriamConnectorTest.class,
    MiRNASearchExceptionTest.class,
    MissingAnnotationTest.class,
    MissingRequiredAnnotationsTest.class,
    MiRNAParserTest.class,
    ModelAnnotatorTest.class,
    PubmedParserTest.class,
    TaxonomyBackendTest.class,
})
public class AllServicesTests {

}
