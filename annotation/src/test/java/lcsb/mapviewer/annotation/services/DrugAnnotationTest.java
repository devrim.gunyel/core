package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.model.map.MiriamData;

public class DrugAnnotationTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(DrugAnnotationTest.class);

  @Autowired
  private DrugbankHTMLParser drugBankHTMLParser;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testXmlSerialization() throws Exception {
    DrugAnnotation drugAnnotation = new DrugAnnotation(DrugAnnotation.class) {
      @Override
      public Drug findDrug(String name) {
        // TODO Auto-generated method stub
        return null;
      }

      @Override
      public List<Drug> getDrugListByTarget(MiriamData target, Collection<MiriamData> organisms) {
        // TODO Auto-generated method stub
        return null;
      }

    };
    Drug drug = drugBankHTMLParser.findDrug("aspirin");

    String xml = drugAnnotation.getDrugSerializer().objectToString(drug);

    Drug drug2 = drugAnnotation.getDrugSerializer().xmlToObject(getNodeFromXmlString(xml));

    assertEquals(drug.getTargets().size(), drug2.getTargets().size());
    assertEquals(drug.getTargets().get(0).getGenes().size(), drug2.getTargets().get(0).getGenes().size());
    assertEquals(drug.getTargets().get(0).getReferences().size(), drug2.getTargets().get(0).getReferences().size());
  }

}
