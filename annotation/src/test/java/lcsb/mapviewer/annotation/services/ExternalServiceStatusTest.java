package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class ExternalServiceStatusTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String name = "b";
    String page = "page";

    ExternalServiceStatus status = new ExternalServiceStatus(name, page);
    assertEquals(name, status.getName());
    assertEquals(page, status.getPage());
  }

}
