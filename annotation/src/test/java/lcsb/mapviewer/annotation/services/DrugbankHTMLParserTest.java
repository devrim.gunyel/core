package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

public class DrugbankHTMLParserTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(DrugbankHTMLParserTest.class);

  @Autowired
  private DrugbankHTMLParser drugBankHTMLParser;

  @Autowired
  private PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCache;

  @Test
  public void test1FindDrug() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Urokinase");
    assertNotNull(test);
    assertEquals(MiriamType.DRUGBANK, test.getSources().get(0).getDataType());
    assertEquals("DB00013", test.getSources().get(0).getResource());
    assertEquals("Urokinase", test.getName());
    assertTrue(test.getBloodBrainBarrier().equalsIgnoreCase("N/A"));
    boolean res = test.getDescription().contains(
        "Low molecular weight form of human urokinase, that consists of an A chain of 2,000 daltons linked by a sulfhydryl bond to a B chain of 30,400 daltons. Recombinant urokinase plasminogen activator");
    assertTrue(res);
    assertEquals(10, test.getTargets().size());
    assertNull(test.getApproved());
  }

  @Test
  public void testFindRapamycin() throws Exception {
    // finding synonym
    Drug rapamycinDrug = drugBankHTMLParser.findDrug("Rapamycin");
    assertNotNull(rapamycinDrug);
    assertEquals("Sirolimus", rapamycinDrug.getName());
    assertEquals("DB00877", rapamycinDrug.getSources().get(0).getResource());
    assertTrue(rapamycinDrug.getBloodBrainBarrier().equalsIgnoreCase("NO"));
    boolean res = rapamycinDrug.getDescription().contains("A macrolide compound obtained from Streptomyces");
    assertTrue(res);
    assertEquals(3, rapamycinDrug.getTargets().size());
  }

  @Test
  public void testFindAfegostat() throws Exception {
    // finding synonym
    Drug afegostatDrug = drugBankHTMLParser.findDrug("Afegostat");
    assertNotNull(afegostatDrug);
    assertFalse(afegostatDrug.getApproved());
  }

  @Test
  public void test4FindDrug() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Methylamine");
    assertNotNull(test);
    assertEquals("Methylamine", test.getName());
    assertEquals("DB01828", test.getSources().get(0).getResource());
    assertEquals(null, test.getDescription());
    assertEquals(1, test.getTargets().size());

    List<Target> tmp;
    tmp = test.getTargets();
    for (Target a : tmp) {
      assertEquals("Ammonia channel", a.getName());
      assertEquals(2, a.getReferences().size());
      assertEquals(1, a.getGenes().size());
    }
  }

  @Test
  public void testFindInvalidDrug() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("qwertyuiop");
    assertNull(test);
  }

  @Test
  public void test7FindDrug() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Aluminum hydroxide");
    assertNotNull(test);
    assertEquals("Aluminum hydroxide", test.getName());
    assertEquals("DB06723", test.getSources().get(0).getResource());
    assertTrue(test.getDescription().contains(
        "Aluminum hydroxide is an inorganic salt used as an antacid. It is a basic compound that acts by neutralizing hydrochloric acid in gastric secretions. Subsequent increases in pH may inhibit the action of pepsin. An increase in bicarbonate ions and prostaglandins may also confer cytoprotective effects."));
    assertEquals(0, test.getTargets().size());
  }

  @Test
  public void testFindDrugAmantadineWithBrandNames() throws Exception {
    Drug drug = drugBankHTMLParser.findDrug("amantadine");

    assertNotNull(drug);
    assertTrue(drug.getBrandNames().contains("PK-Merz"));
    assertTrue(drug.getBrandNames().contains("Symadine"));
  }

  @Test
  public void testFindDrugSelegiline() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Selegiline");
    assertNotNull(test);
    assertEquals(2, test.getTargets().size());
    for (String string : new String[] { "MAOA", "MAOB" }) {
      boolean contain = false;
      for (Target target : test.getTargets()) {
        for (MiriamData md : target.getGenes()) {
          if (md.getResource().equals(string)) {
            contain = true;
          }
        }
      }
      assertTrue("Missing traget: " + string, contain);
    }
  }

  @Test
  public void testFindDrugDaidzinWithHtmlCharacters() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Daidzin");
    assertNotNull(test);
    for (String str : test.getSynonyms()) {
      assertFalse(str.contains("&#39;"));
    }
  }

  @Test
  public void testFindDrugGliclazideWithHtmlTags() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Gliclazide");
    assertNotNull(test);
    assertFalse(test.getDescription().contains("<span"));
  }

  @Test
  public void testFindDrugSevofluraneWithHtmlTags() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("sevoflurane");
    assertNotNull(test);
    for (String str : test.getSynonyms()) {
      assertFalse(str.contains("<div"));
    }
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    assertNotNull(drugs);
    assertTrue("At least 1 drug was expected", drugs.size() >= 1);
  }

  @Test
  public void testFindDrugByHgncTargetWithAnnotatorAssigned() throws Exception {
    List<Drug> drugs = drugBankHTMLParser
        .getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3", String.class));
    assertNotNull(drugs);
    assertTrue("At least 1 drug was expected", drugs.size() >= 1);
  }

  @Test
  public void testTargetPageMatchTargetForTFProtein() throws Exception {
    MiriamData tfMiriam = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
    String content = drugBankHTMLParser.getWebPageDownloader()
        .getFromNetwork("https://www.drugbank.ca/biodb/bio_entities/BE0000510");
    assertTrue(drugBankHTMLParser.targetPageMatchTarget(content, tfMiriam));
  }

  @Test
  public void testTargetPageMatchTargetForNotMatchingTFProtein() throws Exception {
    MiriamData tfMiriam = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
    String content = drugBankHTMLParser.getWebPageDownloader()
        .getFromNetwork("https://www.drugbank.ca/biodb/bio_entities/BE0002803");
    assertFalse(drugBankHTMLParser.targetPageMatchTarget(content, tfMiriam));
  }

  @Test
  public void testIsDrugContainTarget() throws Exception {
    Drug afegostat = drugBankHTMLParser.findDrug("Urokinase");
    MiriamData plgMiriam = new MiriamData(MiriamType.HGNC_SYMBOL, "PLG");
    MiriamData tfMiriam = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
    assertTrue(drugBankHTMLParser.isDrugContainTarget(plgMiriam, afegostat));
    assertFalse(drugBankHTMLParser.isDrugContainTarget(tfMiriam, afegostat));
  }

  @Test
  public void testFindAluminiumByName() throws Exception {
    Drug drug = drugBankHTMLParser.findDrug("Aluminium");
    assertTrue(drug.getName().equalsIgnoreCase("Aluminium"));
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
    List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"),
        organisms);
    assertNotNull(drugs);
    assertEquals("No drugs for this organisms should be found", 0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
    List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"),
        organisms);
    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testFindDrugByInvalidTarget() throws Exception {
    drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.WIKIPEDIA, "h2o"));
  }

  @Test
  public void testFindDrugByEmptyTarget() throws Exception {
    List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(null);
    assertEquals(0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargets() throws Exception {
    MiriamData target1 = new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3");
    MiriamData target2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT5");
    List<Drug> drug1 = drugBankHTMLParser.getDrugListByTarget(target1);
    List<Drug> drug2 = drugBankHTMLParser.getDrugListByTarget(target2);
    int size = drug1.size() + drug2.size();

    List<MiriamData> list = new ArrayList<>();
    list.add(target1);
    list.add(target2);

    List<Drug> drugs = drugBankHTMLParser.getDrugListByTargets(list);
    assertNotNull(drugs);
    assertTrue("Merged list should be shorter then sum of the two lists", drugs.size() < size);
  }

  @Test(timeout = 150000)
  public void testRefreshCacheForDrug() throws Exception {
    String query = "drug:amantadine";

    String res = drugBankHTMLParser.refreshCacheQuery(query);

    assertNotNull(res);
  }

  @Test
  public void testByTargetsForCCNB1() throws Exception {
    List<MiriamData> targets = new ArrayList<>();
    MiriamData miriamTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "CCNB1");
    targets.add(miriamTarget);
    List<Drug> drugs = drugBankHTMLParser.getDrugListByTargets(targets);
    for (Drug drug : drugs) {
      boolean found = false;
      for (Target target : drug.getTargets()) {
        for (MiriamData md : target.getGenes()) {
          if (miriamTarget.equals(md)) {
            found = true;
          }
        }
      }
      assertTrue("Drug " + drug.getName() + " doesn't contain expected target", found);
    }
  }

  @Test
  public void testFindIbuprofen() throws Exception {
    Drug drug = drugBankHTMLParser.findDrug("Ibuprofen");
    assertNotNull(drug);
    assertEquals("Ibuprofen", drug.getName());
    assertEquals("DB01050", drug.getSources().get(0).getResource());
    assertNotNull(drug.getDescription());
    assertTrue(drug.getTargets().size() >= 8);
  }

  @Test
  public void testFindIronSaccharate() throws Exception {
    Drug drug = drugBankHTMLParser.findDrug("Iron saccharate");
    assertNotNull(drug);
    MiriamData tfProtein = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
    for (Target target : drug.getTargets()) {
      for (MiriamData md : target.getGenes()) {
        assertFalse("TF is a carrier, not a target", md.equals(tfProtein));
      }
    }
  }

  @Test
  public void testFindDopamine() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Dopamine");
    assertNotNull(test);
    for (Target target : test.getTargets()) {
      assertFalse(target.getName().contains("Details"));
    }
  }

  @Test
  public void testFindGadobutrol() throws Exception {
    Drug gadobutrolDrug = drugBankHTMLParser.findDrug("Gadobutrol");
    assertNotNull(gadobutrolDrug);
    assertTrue(gadobutrolDrug.getBloodBrainBarrier().equalsIgnoreCase("NO"));
  }

  @Test
  public void testFindDrugWithSpecialCharacters() throws Exception {
    Drug test = drugBankHTMLParser.findDrug("Thymidine-5'- Diphosphate");
    assertNotNull(test);
    assertFalse(test.getName().contains("&"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    drugBankHTMLParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    drugBankHTMLParser.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      drugBankHTMLParser.refreshCacheQuery("http://google.pl/");
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWhenDbNotAvailable() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    GeneralCacheInterface cache = drugBankHTMLParser.getCache();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      drugBankHTMLParser.setCache(null);

      drugBankHTMLParser.refreshCacheQuery(DrugbankHTMLParser.DRUG_NAME_PREFIX + "aspirin");
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
      drugBankHTMLParser.setCache(cache);
    }
  }

  @Test
  public void testGetPubmedFromRef() throws Exception {
    String descriptionString = "<a href=\"/pubmed/1234\">link to 1234</a>";
    List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
    assertEquals(1, result.size());
    assertEquals(new MiriamData(MiriamType.PUBMED, "1234"), result.get(0));
  }

  @Test
  public void testGetPubmedFromRef2() throws Exception {
    String descriptionString = "<a href=\"/pubmed/12345678901234_\">link to 123456789</a>";
    List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
    assertEquals(1, result.size());
    assertEquals(new MiriamData(MiriamType.PUBMED, "123456789"), result.get(0));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testGetPubmedFromRef3() throws Exception {
    String descriptionString = "<a href=\"/pubmed/invalid\">link to invalid</a>";
    List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
    assertEquals(0, result.size());
  }

  @Test
  public void testGetDescription() throws Exception {
    String descriptionString = "untagged description";
    assertNull(drugBankHTMLParser.getDescriptionForDrug(descriptionString));
  }

  @Test
  public void testGetTargets() throws Exception {
    String descriptionString = "invalid content";
    List<Target> targets = drugBankHTMLParser.getTargetsForDrug(descriptionString);
    assertEquals(0, targets.size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, drugBankHTMLParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, drugBankHTMLParser.getServiceStatus().getStatus());
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, drugBankHTMLParser.getServiceStatus().getStatus());
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetBBB() throws Exception {
    assertTrue(
        drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>-</td><td>0.7581</td></tr>")
            .equalsIgnoreCase("NO"));
    assertTrue(
        drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>+</td><td>0.7581</td></tr>")
            .equalsIgnoreCase("YES"));
    assertTrue(drugBankHTMLParser
        .getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>don't know</td><td>0.7581</td></tr>")
        .equalsIgnoreCase("N/A"));
    assertTrue(drugBankHTMLParser.getBloodBrainBarrier("invalid str").equalsIgnoreCase("N/A"));
    assertTrue(drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td>invalid str")
        .equalsIgnoreCase("N/A"));
  }

  @Test
  public void testGetters() throws Exception {
    DrugbankHTMLParser parser = new DrugbankHTMLParser(null);
    UniprotAnnotator uniprotAnnotator = new UniprotAnnotator();
    parser.setUniprotAnnotator(uniprotAnnotator);
    assertEquals(uniprotAnnotator, parser.getUniprotAnnotator());
  }

  @Test
  public void parseTarget() throws Exception {
    String content = super.readFile("testFiles/drugbank/target-html-part.html");
    Target target = drugBankHTMLParser.parseTarget(content);
    assertNotNull(target);
    assertEquals(6, target.getReferences().size());
    assertEquals("Matrix protein 2", target.getName());
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-2);

    List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, TaxonomyBackend.HUMAN_TAXONOMY);

    assertEquals(0, result.size());
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-1);
    Model model = getModelForFile("testFiles/target_drugbank/target.xml", false);
    project.addModel(model);

    List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, TaxonomyBackend.HUMAN_TAXONOMY);

    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetSuggestedQueryListForUnknownOrganism() throws Exception {
    Project project = new Project();
    project.setId(-2);
    Model model = getModelForFile("testFiles/target_drugbank/target.xml", false);
    project.addModel(model);

    List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, null);

    assertEquals(0, result.size());
  }

  @Test
  public void testExtractTargetsFromPageContent() throws Exception {
    String pageContent = " Details</a>blablablablabla Details</a>";
    List<Target> result = drugBankHTMLParser.extractTargetsFromPageContent(pageContent, 2, 3);

    assertEquals(0, result.size());
  }

}
