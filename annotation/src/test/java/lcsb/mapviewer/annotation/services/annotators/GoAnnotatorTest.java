package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class GoAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(GoAnnotatorTest.class);

  @Autowired
  GoAnnotator goAnnotator;

  @Autowired
  private PermanentDatabaseLevelCacheInterface permanentDatabaseLevelCache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testContent() throws Exception {
    Compartment compartmentAlias = new Compartment("id");
    compartmentAlias
        .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0046902"));
    goAnnotator.annotateElement(compartmentAlias);
    assertFalse(compartmentAlias.getFullName().equals(""));
    assertFalse(compartmentAlias.getNotes().equals(""));
  }

  @Test
  public void testGetGoData() throws Exception {
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0042644");
    Go go = goAnnotator.getGoElement(md);
    assertEquals("GO:0042644", go.getGoTerm());
    assertNotNull(go.getCommonName());
    assertNotNull(go.getDescription());
    assertTrue(go.getCommonName().toLowerCase().contains("chloroplast nucleoid"));
  }

  @Test(expected = GoSearchException.class)
  public void testAnnotateWhenProblemWithNetworkResponse() throws Exception {
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0042644");
    GoAnnotator annotator = new GoAnnotator(null);
    annotator.setMc(goAnnotator.getMc());
    WebPageDownloader downloader = Mockito.mock(WebPageDownloader.class);
    when(downloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
    annotator.setWebPageDownloader(downloader);
    annotator.getGoElement(md);
  }

  @Test(timeout = 15000)
  public void testCachableInterface() throws Exception {
    String query = GoAnnotator.GO_TERM_CACHE_PREFIX + "GO:0046902";
    String newRes = "hello";
    cache.setCachedQuery(query, goAnnotator.getCacheType(), newRes);
    cache.invalidateByQuery(query, goAnnotator.getCacheType());

    permanentDatabaseLevelCache.waitToFinishTasks();

    String res = cache.getStringByQuery(query, goAnnotator.getCacheType());

    assertNotNull(res);

    assertFalse("Value wasn't refreshed from db", newRes.equals(res));
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = goAnnotator.getCache();
    try {
      // exclude first cached value
      goAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      goAnnotator.setWebPageDownloader(mockDownloader);
      goAnnotator.refreshCacheQuery("http://google.pl/");
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
      goAnnotator.setCache(originalCache);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    goAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    goAnnotator.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    String res = goAnnotator.refreshCacheQuery("http://google.pl/");
    assertNotNull(res);
  }

  @Test(timeout = 15000)
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, goAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, goAnnotator.getServiceStatus().getStatus());
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("{\"numberOfHits\": 1,\"results\": [{\"name\":\"x\"}]}");
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("{\"numberOfHits\": 1,\"results\": [{}]}");
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

}
