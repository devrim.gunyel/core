package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ImproperAnnotationsTest extends AnnotationTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor3() {
    List<MiriamData> list = new ArrayList<>();
    list.add(new MiriamData(MiriamType.CAS, "a"));
    ImproperAnnotations ie = new ImproperAnnotations(new GenericProtein("id"), list);
    assertNotNull(ie.toString());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructor4() {
    new ImproperAnnotations(new GenericProtein("id"), new ArrayList<>());
  }

}
