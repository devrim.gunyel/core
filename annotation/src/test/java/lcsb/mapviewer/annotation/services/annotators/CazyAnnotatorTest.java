package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class CazyAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  CazyAnnotator cazyAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testUniprotToCazy() throws Exception {
    assertEquals(new MiriamData(MiriamType.CAZY, "GH5_7"),
        cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.UNIPROT, "Q9SG95"), null));
  }

  @Test
  public void testAnnotateFromUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));

    cazyAnnotator.annotateElement(protein);

    MiriamData mdCazy = null;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        mdCazy = md;
      }
    }

    assertTrue("No UNIPROT annotation extracted from TAIR annotator", mdCazy != null);
    assertTrue("Invalid UNIPROT annotation extracted from TAIR annotator",
        mdCazy.getResource().equalsIgnoreCase("GH5_7"));
  }

  @Test
  @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateFromTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT3G53010"));

    cazyAnnotator.annotateElement(protein);

    MiriamData mdCazy = null;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        mdCazy = md;
      }
    }

    assertTrue("No CAZy annotation extracted from CAZy annotator", mdCazy != null);
    assertTrue("Invalid CAZy annotation extracted from CAZy annotator", mdCazy.getResource().equalsIgnoreCase("CE6"));
  }

  @Test
  public void testAnnotateMultipleUniprots() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q12540"));

    cazyAnnotator.annotateElement(protein);

    int cntMDs = 0;

    for (MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        cntMDs++;
      }
    }

    assertTrue("Wrong number of CAZy identifiers extracted from CAZy annotator", cntMDs == 2);
  }

  @Test
  public void testAnnotateInvalidEmpty() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    cazyAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
    cazyAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    cazyAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());
  }

  @Test
  public void testInvalidUniprotToCazyNull() throws Exception {
    assertNull(cazyAnnotator.uniprotToCazy(null, null));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidUniprotToCazyWrongMd() throws Exception {
    cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.WIKIPEDIA, "bla"), null);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, cazyAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = cazyAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      cazyAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, cazyAnnotator.getServiceStatus().getStatus());
    } finally {
      cazyAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = cazyAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      cazyAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, cazyAnnotator.getServiceStatus().getStatus());
    } finally {
      cazyAnnotator.setWebPageDownloader(downloader);
    }
  }

}
