package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;

public class ReconAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(ReconAnnotatorTest.class);
  @Autowired
  ReconAnnotator reconAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateReaction() throws Exception {
    Reaction reaction = new Reaction();
    reaction.setAbbreviation("P5CRm");
    reconAnnotator.annotateElement(reaction);
    assertTrue("No new annotations from recon db imported", reaction.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotating_O16G2e_Reaction() throws Exception {
    Reaction reaction = new Reaction();
    reaction.setName("O16G2e");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testSpeciesAnnotationWithInvalidResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Ion ion = new Ion("id");
      ion.setName("O16G2e");

      // exclude first cached value
      reconAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(ion);
      assertEquals(1, getWarnings().size());
      assertTrue(getWarnings().get(0).getMessage().getFormattedMessage().contains("No recon annotations"));
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testSpeciesAnnotationWithReactionResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Ion ion = new Ion("id");
      ion.setName("O16G2e");

      // exclude first cached value
      reconAnnotator.setCache(null);

      String response = super.readFile("testFiles/annotation/recon_reaction_response.json");

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(ion);
      assertEquals(2, getWarnings().size());
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testReactionAnnotationWithChemicalResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Reaction reaction = new Reaction();
      reaction.setName("nad[m]");

      // exclude first cached value
      reconAnnotator.setCache(null);

      String response = super.readFile("testFiles/annotation/recon_chemical_response.json");

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(reaction);
      assertEquals(2, getWarnings().size());
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testAnnotateUnknownElement() throws Exception {
    BioEntity obj = Mockito.mock(BioEntity.class);
    when(obj.getName()).thenReturn("O16G2e");

    ReconAnnotator tweakedReconAnnotator = Mockito.spy(reconAnnotator);
    doReturn(true).when(tweakedReconAnnotator).isAnnotatable(any(BioEntity.class));

    tweakedReconAnnotator.annotateElement(obj);
    assertTrue(getWarnings().size() > 0);
    assertTrue(getWarnings().get(0).getMessage().getFormattedMessage().contains("Unknown class type"));
  }

  @Test
  public void testAnnotatingWithParentehesis() throws Exception {
    Reaction reaction = new Reaction();
    reaction.setName("gm2_hs[g]");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotatingForOxygen() throws Exception {
    SimpleMolecule molecule = new SimpleMolecule("id");
    molecule.setName("o2");
    reconAnnotator.annotateElement(molecule);
    assertTrue(molecule.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
    assertEquals("O2", molecule.getFormula());
  }

  @Test
  public void testAnnotatingWithWhitespace() throws Exception {
    Reaction reaction = new Reaction();
    reaction.setName("NDPK4m ");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotateElement() throws Exception {
    SimpleMolecule smallMolecule = new SimpleMolecule("id");
    smallMolecule.setAbbreviation("h2o");
    reconAnnotator.annotateElement(smallMolecule);
    assertTrue("No new annotations from recon db imported", smallMolecule.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotateElement2() throws Exception {
    Species smallMolecule = new SimpleMolecule("id");
    smallMolecule.setAbbreviation("P5CRm");
    reconAnnotator.annotateElement(smallMolecule);
    assertEquals(0, smallMolecule.getMiriamData().size());
    assertFalse(getWarnings().size() == 0);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, reconAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, reconAnnotator.getServiceStatus().getStatus());
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("{\"results\":[{}]}");
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus2() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("{\"results\":[{\"keggId\":\"C00001\",\"xxx\":\"yyy\"}]}");
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

}
