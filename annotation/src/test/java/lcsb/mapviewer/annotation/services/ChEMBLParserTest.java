package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.*;
import lcsb.mapviewer.annotation.services.annotators.*;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.*;

public class ChEMBLParserTest extends AnnotationTestFunctions {
  Logger logger = LogManager.getLogger(ChEMBLParserTest.class);

  @Autowired
  private GeneralCacheInterface cache;

  @Autowired
  private ChEMBLParser chemblParser;

  @Test
  public void test1FindDrug() throws Exception {
    Drug drug = chemblParser.findDrug("CABOZANTINIB");

    assertEquals("CHEMBL2105717", drug.getSources().get(0).getResource());
    assertEquals("CABOZANTINIB", drug.getName());
    assertNull(drug.getDescription());
    assertEquals(2, drug.getTargets().size());
    List<String> lowerCaseSynonyms = new ArrayList<>();
    for (String s : drug.getSynonyms()) {
      lowerCaseSynonyms.add(s.toLowerCase());
    }
    assertTrue(lowerCaseSynonyms.contains("cabozantinib"));
    assertTrue(drug.getApproved());
  }

  @Test
  public void test2FindDrug() throws Exception {
    String n = "DIMETHYL FUMARATE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL2107333", test.getSources().get(0).getResource());
    assertEquals("DIMETHYL FUMARATE", test.getName());
    // assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());
  }

  @Test
  public void test3FindDrug() throws Exception {
    String n = "LOMITAPIDE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL354541", test.getSources().get(0).getResource());
    assertEquals("LOMITAPIDE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test4FindDrug() throws Exception {
    String n = "COBICISTAT";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL2095208", test.getSources().get(0).getResource());
    assertEquals("COBICISTAT", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test5FindDrug() throws Exception {
    String n = "TALIGLUCERASE ALFA";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL1964120", test.getSources().get(0).getResource());
    assertEquals("TALIGLUCERASE ALFA", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test6FindDrug() throws Exception {
    String n = "ABIRATERONE ACETATE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL271227", test.getSources().get(0).getResource());
    assertEquals("ABIRATERONE ACETATE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test7FindDrug() throws Exception {
    String n = "asdwrgwedas";
    Drug test = chemblParser.findDrug(n);
    assertNull(test);
  }

  @Test
  public void test8FindDrug() throws Exception {
    String n = "aa a32";
    Drug test = chemblParser.findDrug(n);
    assertNull(test);
  }

  @Test
  public void test9FindDrug() throws Exception {
    String name = "PONATINIB";
    Drug test = chemblParser.findDrug(name);

    assertEquals("CHEMBL1171837", test.getSources().get(0).getResource());
    assertEquals(name, test.getName());
    assertNull(test.getDescription());
    assertEquals(3, test.getTargets().size());
  }

  @Test
  public void test10FindDrug() throws Exception {
    String n = "DEXAMETHASONE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL384467", test.getSources().get(0).getResource());
    assertEquals("DEXAMETHASONE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (Target t : test.getTargets()) {
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16891588")));
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16956592")));
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16971495")));
    }
  }

  /**
   * It is impossible to get references just from target, that's why after
   * getTargetFromId PubMedRef should be empty
   */
  @Test
  public void test1getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));

    assertEquals(test.getSource().getResource(), "CHEMBL3717");
    assertEquals(test.getName(), "Hepatocyte growth factor receptor");
    assertEquals(test.getOrganism().getResource(), "9606");
    assertEquals(TargetType.SINGLE_PROTEIN, test.getType());
    boolean contains = false;
    for (MiriamData md : test.getGenes()) {
      if (md.getResource().equals("MET")) {
        contains = true;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void testGetTargetWithRnaComponent() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2363135"));

    assertEquals(test.getSource().getResource(), "CHEMBL2363135");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetFromInvalidId() throws Exception {
    chemblParser.getTargetFromId(new MiriamData(MiriamType.WIKIPEDIA, "water"));
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithDbConnection() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugWhenProblemWithDbConnection() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.findDrug("test");
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithDbConnection2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithUniprot() throws Exception {
    UniprotAnnotator uniprotAnnotator = chemblParser.getUniprotAnnotator();

    try {
      UniprotAnnotator mockAnnotator = Mockito.mock(UniprotAnnotator.class);
      when(mockAnnotator.uniProtToHgnc(any())).thenThrow(new UniprotSearchException(null, null));
      chemblParser.setUniprotAnnotator(mockAnnotator);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));
    } finally {
      chemblParser.setUniprotAnnotator(uniprotAnnotator);
    }

  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = chemblParser.getTaxonomyBackend();

    try {
      TaxonomyBackend mockAnnotator = Mockito.mock(TaxonomyBackend.class);
      when(mockAnnotator.getNameForTaxonomy(any())).thenThrow(new TaxonomySearchException(null, null));
      chemblParser.setTaxonomyBackend(mockAnnotator);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));
    } finally {
      chemblParser.setTaxonomyBackend(taxonomyBackend);
    }

  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<target><unk/></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<target><target_components><target_component/></target_components></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse3() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<target><target_chembl_id>inv id</target_chembl_id></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void test2getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3522"));

    assertEquals(test.getSource().getResource(), "CHEMBL3522");
    assertEquals(test.getName(), "Cytochrome P450 17A1");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    for (MiriamData md : test.getGenes()) {
      if (md.getResource().equals("CYP17A1")) {
        contains = true;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test4getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364681"));

    assertEquals(test.getSource().getResource(), "CHEMBL2364681");
    assertEquals(test.getName(), "Microsomal triglyceride transfer protein");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    boolean contains2 = false;
    for (MiriamData md : test.getGenes()) {
      if (md.getResource().equals("MTTP")) {
        contains = true;
      }
      if (md.getResource().equals("P4HB")) {
        contains2 = true;
      }
    }
    assertTrue(contains);
    assertTrue(contains2);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test5getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364176"));

    assertEquals(test.getSource().getResource(), "CHEMBL2364176");
    assertEquals(test.getName(), "Glucocerebroside");
    assertEquals(test.getOrganism().getResource(), "9606");
    assertEquals(0, test.getGenes().size());
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test6getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2069156"));

    assertEquals(test.getSource().getResource(), "CHEMBL2069156");
    assertEquals(test.getName(), "Kelch-like ECH-associated protein 1");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    for (MiriamData md : test.getGenes()) {
      if (md.getResource().equals("KEAP1")) {
        contains = true;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void testGetTargetsForAmantadine() throws Exception {
    Drug drug = chemblParser.findDrug("AMANTADINE");
    for (Target target : drug.getTargets()) {
      assertEquals(0, target.getReferences().size());
    }
    assertTrue(drug.getTargets().size() > 0);
  }

  @Test
  public void testGetTargetsWithReferences() throws Exception {
    Drug drug = chemblParser.findDrug("TRIMETHOPRIM");
    for (Target target : drug.getTargets()) {
      assertEquals(0, target.getReferences().size());
    }
    assertTrue(drug.getTargets().size() > 0);
  }

  @Test
  public void testFindDrugByInvalidHgncTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
    assertNotNull(drugs);
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByHgncWhenHgncCrash() throws Exception {
    HgncAnnotator hgncAnnotator = chemblParser.getHgncAnnotator();
    try {
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToUniprot(any())).thenThrow(new AnnotatorException(""));
      chemblParser.setHgncAnnotator(mockAnnotator);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
    } finally {
      chemblParser.setHgncAnnotator(hgncAnnotator);
    }
  }

  @Test
  public void testFindDrugByHgncTargetWithManyUniprot() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "AKAP7"));
    assertNotNull(drugs);
    // we should have a warning that akap7 has more than one uniprot id
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testFindDrugByUniprotTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
    assertTrue(drugs.size() > 0);
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByInvalidTarget() throws Exception {
    chemblParser.getDrugListByTarget(new MiriamData(MiriamType.MESH_2012, "D010300"));
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test
  public void testFindDrugByPFKMTarget() throws Exception {
    // this gene raised some errors at some point
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "PFKM"));
    assertNotNull(drugs);
    assertTrue(drugs.size() >= 0);
  }

  @Test
  public void testFindDrugsByHgncTargets() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<Drug> drugs2 = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
    List<MiriamData> hgnc = new ArrayList<MiriamData>();
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
    List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
    assertNotNull(drugs3);
    assertTrue(drugs.size() + drugs2.size() > drugs3.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
    assertNotNull(drugs);
    assertEquals("No drugs for this organisms should be found", 0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test
  public void testFindDrugsByRepeatingHgncTargets() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<MiriamData> hgnc = new ArrayList<MiriamData>();
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
    assertNotNull(drugs3);
    assertEquals(drugs.size(), drugs3.size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    chemblParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    chemblParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    Object res = chemblParser.refreshCacheQuery("http://google.pl/");
    assertNotNull(res);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.refreshCacheQuery("http://google.pl/");
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugsByChemblTargetWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugsByChemblTarget(new MiriamData());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugsByChemblTargetWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugsByChemblTarget(new MiriamData());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWhenChemblDbNotAvailable() throws Exception {
    String query = ChEMBLParser.NAME_PREFIX + "TRIMETHOPRIM";

    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.refreshCacheQuery(query);
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, chemblParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, chemblParser.getServiceStatus().getStatus());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<response><molecules/></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);

      // valid xml but with empty data
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<response><molecules>"
              + "<molecule><pref_name/><max_phase/><molecule_chembl_id/><molecule_synonyms/></molecule>"
              + "</molecules><mechanisms/><molecule_forms/></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetter() throws Exception {
    ChEMBLParser parser = new ChEMBLParser(null, null);
    XmlSerializer<Drug> drugSerializer = new XmlSerializer<>(Drug.class);
    parser.setDrugSerializer(drugSerializer);
    assertEquals(drugSerializer, parser.getDrugSerializer());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetsByInvalidDrugId() throws Exception {
    chemblParser.getTargetsByDrugId(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsByDrugIdWithExternalDbDown() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsByDrugIdWithExternalDbDown2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByUniprotTargetWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }

  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByUniprotTargetWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }

  }

  @Test
  public void testParseReferences() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/chembl/references.xml");
    Set<MiriamData> references = chemblParser.parseReferences(node.getFirstChild());
    // we might have more references (if we decide to parse wikipedia, isbn,
    // etc)
    assertTrue(references.size() >= 3);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testParseReferencesWithSpecInId() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/chembl/references_with_space.xml");
    Set<MiriamData> references = chemblParser.parseReferences(node.getFirstChild());
    assertEquals(1, references.size());
    assertFalse(references.iterator().next().getResource().contains(" "));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetDrugByInvalidDrugId() throws Exception {
    chemblParser.getDrugById(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugByDrugIdWhenCehmblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugByDrugIdWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testParseSynonymsNode() throws Exception {
    Node node = super.getNodeFromXmlString(
        "<molecule_synonyms><synonym><synonyms>some synonym</synonyms></synonym><unknode/></molecule_synonyms>");
    List<String> synonyms = chemblParser.parseSynonymsNode(node);
    assertEquals(1, synonyms.size());
    assertEquals(1, getWarnings().size());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsForChildElementsWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsForChildElementsWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetTargetsForChildElementsWhenChemblReturnPartlyInvalidResult() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("<response><molecule_forms><node/></molecule_forms></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
      assertEquals(1, getWarnings().size());
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testFindDrugBryBrandName() throws Exception {
    try {
      Drug drug = chemblParser.findDrug("picato");
      assertNotNull(drug);
      assertEquals("CHEMBL1863513", drug.getSources().get(0).getResource());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
