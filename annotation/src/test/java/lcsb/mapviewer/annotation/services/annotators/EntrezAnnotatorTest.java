package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.*;
import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class EntrezAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  EntrezAnnotator entrezAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnnotationsForEntrezId() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "6647");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    entrezAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertNotNull(proteinAlias.getFullName());
    assertNotNull(proteinAlias.getNotes());
    assertFalse(proteinAlias.getNotes().isEmpty());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertTrue(proteinAlias.getSynonyms().size() > 0);

    boolean ensemble = false;
    boolean hgncId = false;
    boolean entrez = false;
    for (MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        hgncId = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Entrez cannot be found", entrez);
  }

  @Test(timeout = 15000)
  public void testGetAnnotationsForInvalid() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "blabla");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    entrezAnnotator.annotateElement(proteinAlias);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, entrezAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, entrezAnnotator.getServiceStatus().getStatus());
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    XmlSerializer<EntrezData> entrezSerializer = entrezAnnotator.getEntrezSerializer();
    try {
      @SuppressWarnings("unchecked")
      XmlSerializer<EntrezData> mockSerializer = Mockito.mock(XmlSerializer.class);
      when(mockSerializer.xmlToObject(any())).thenReturn(new EntrezData());
      entrezAnnotator.setEntrezSerializer(mockSerializer);
      assertEquals(ExternalServiceStatusType.CHANGED, entrezAnnotator.getServiceStatus().getStatus());
    } finally {
      entrezAnnotator.setEntrezSerializer(entrezSerializer);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    entrezAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    entrezAnnotator.refreshCacheQuery(new Object());
  }

  @Test(timeout = 15000)
  public void testRefreshEntrezData() throws Exception {
    assertNotNull(entrezAnnotator.refreshCacheQuery(EntrezAnnotator.ENTREZ_DATA_PREFIX + "6647"));
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = entrezAnnotator.getCache();
    try {
      // exclude first cached value
      entrezAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      entrezAnnotator.refreshCacheQuery("http://google.pl/");
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
      entrezAnnotator.setCache(originalCache);
    }
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWithInvalidEntrezServerResponse() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = entrezAnnotator.getCache();
    try {
      // exclude first cached value
      entrezAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      entrezAnnotator.refreshCacheQuery(EntrezAnnotator.ENTREZ_DATA_PREFIX + "6647");
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
      entrezAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testAnnotateElementWithTwoEntrez() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6647"));
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6648"));
    entrezAnnotator.annotateElement(proteinAlias);

    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testAnnotateElementWithEncodedSynonyms() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "834106"));
    entrezAnnotator.annotateElement(proteinAlias);

    for (String synonym : proteinAlias.getSynonyms()) {
      assertFalse("Invalid character found in synonym: " + synonym, synonym.contains("&"));
    }
  }

  @Test
  public void testParseEntrezResponse() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    GeneralCacheInterface cache = entrezAnnotator.getCache();
    try {
      entrezAnnotator.setCache(null);
      String response = super.readFile("testFiles/annotation/entrezResponse.xml");
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      EntrezData data = entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
      boolean ensembl = false;
      boolean hgnc = false;
      for (MiriamData md : data.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.HGNC)) {
          hgnc = true;
        }
        if (md.getDataType().equals(MiriamType.ENSEMBL)) {
          ensembl = true;
        }
      }
      assertTrue(ensembl);
      assertTrue(hgnc);
      assertNotNull(data.getDescription());
      assertFalse(data.getDescription().isEmpty());
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
      entrezAnnotator.setCache(cache);
    }
  }

  @Test(expected = AnnotatorException.class)
  public void testParseInvalidEntrezResponse() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    GeneralCacheInterface cache = entrezAnnotator.getCache();
    try {
      entrezAnnotator.setCache(null);
      String response = "";
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
      entrezAnnotator.setCache(cache);
    }

  }

  @Test(expected = AnnotatorException.class)
  public void testParseInvalidEntrezResponse2() throws Exception {
    WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
    GeneralCacheInterface cache = entrezAnnotator.getCache();
    try {
      entrezAnnotator.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new WrongResponseCodeIOException(null, 404));
      entrezAnnotator.setWebPageDownloader(mockDownloader);
      entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
    } finally {
      entrezAnnotator.setWebPageDownloader(downloader);
      entrezAnnotator.setCache(cache);
    }

  }

}
