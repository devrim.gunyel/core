package lcsb.mapviewer.annotation;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lcsb.mapviewer.annotation.cache.PermanentDatabaseLevelCacheInterface;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;

@Transactional
@ContextConfiguration(classes = SpringAnnotationTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AnnotationTestFunctions extends AbstractTransactionalJUnit4SpringContextTests {

  private static Map<String, Model> models = new HashMap<String, Model>();
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  @Autowired
  protected PermanentDatabaseLevelCacheInterface cache;
  @Autowired
  protected CacheTypeDao cacheTypeDao;
  @Autowired
  protected DbUtils dbUtils;
  private Logger logger = LogManager.getLogger(AnnotationTestFunctions.class);
  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender(false);
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrors() {
    return appender.getErrors();
  }

  protected List<LogEvent> getFatals() {
    return appender.getFatals();
  }

  protected String readFile(String file) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader reader = new BufferedReader(new FileReader(file));
    try {
      String line = null;
      String ls = System.getProperty("line.separator");

      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
        stringBuilder.append(ls);
      }
    } finally {
      reader.close();
    }

    return stringBuilder.toString();
  }

  protected Node getNodeFromXmlString(String text) throws InvalidXmlSchemaException, IOException {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(text));
    return getXmlDocumentFromInputSource(is).getChildNodes().item(0);
  }

  protected Document getXmlDocumentFromFile(String fileName) throws InvalidXmlSchemaException, IOException {
    File file = new File(fileName);
    InputStream inputStream = new FileInputStream(file);
    Reader reader = null;
    try {
      reader = new InputStreamReader(inputStream, "UTF-8");
      InputSource is = new InputSource(reader);

      Document result = getXmlDocumentFromInputSource(is);
      inputStream.close();
      return result;
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  protected Document getXmlDocumentFromInputSource(InputSource stream) throws InvalidXmlSchemaException, IOException {
    DocumentBuilder db;
    try {
      db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser");
    }
    Document doc = null;
    try {
      doc = db.parse(stream);
    } catch (SAXException e) {
      throw new InvalidXmlSchemaException(e);
    }
    return doc;
  }

  protected Model getModelForFile(String fileName, boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      return new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
    }
    Model result = AnnotationTestFunctions.models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: " + fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      AnnotationTestFunctions.models.put(fileName, result);
    }
    return result;
  }

  protected String createTmpFileName() {
    try {
      File f = File.createTempFile("prefix", ".txt");
      String filename = f.getName();
      f.delete();
      return filename;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  protected String nodeToString(Node node) {
    return nodeToString(node, false);
  }

  protected String nodeToString(Node node, boolean includeHeadNode) {
    if (node == null)
      return null;
    StringWriter sw = new StringWriter();
    try {
      Transformer t = TransformerFactory.newInstance().newTransformer();
      t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      t.setOutputProperty(OutputKeys.INDENT, "yes");
      t.setOutputProperty(OutputKeys.METHOD, "xml");

      NodeList list = node.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        Node element = list.item(i);
        t.transform(new DOMSource(element), new StreamResult(sw));
      }
    } catch (TransformerException te) {
      logger.debug("nodeToString Transformer Exception");
    }
    if (includeHeadNode) {
      return "<" + node.getNodeName() + ">" + sw.toString() + "</" + node.getNodeName() + ">";
    }
    return sw.toString();
  }

  protected boolean equalFiles(String fileA, String fileB) throws IOException {
    int BLOCK_SIZE = 65536;
    FileInputStream inputStreamA = new FileInputStream(fileA);
    FileInputStream inputStreamB = new FileInputStream(fileB);
    // vary BLOCK_SIZE to suit yourself.
    // it should probably a factor or multiple of the size of a disk
    // sector/cluster.
    // Note that your max heap size may need to be adjused
    // if you have a very big block size or lots of these comparators.

    // assume inputStreamA and inputStreamB are streams from your two files.
    byte[] streamABlock = new byte[BLOCK_SIZE];
    byte[] streamBBlock = new byte[BLOCK_SIZE];
    boolean match = true;
    int bytesReadA = 0;
    int bytesReadB = 0;
    do {
      bytesReadA = inputStreamA.read(streamABlock);
      bytesReadB = inputStreamB.read(streamBBlock);
      match = ((bytesReadA == bytesReadB) && Arrays.equals(streamABlock, streamBBlock));
    } while (match && (bytesReadA > -1));
    inputStreamA.close();
    inputStreamB.close();
    return match;
  }

  public File createTempDirectory() throws IOException {
    final File temp;

    temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

    if (!(temp.delete())) {
      throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
    }

    if (!(temp.mkdir())) {
      throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
    }

    return (temp);
  }

  protected String getWebpage(String accessUrl) throws IOException {
    String inputLine;
    IOException exception = null;
    for (int i = 0; i < 3; i++) {
      try {
        StringBuilder tmp = new StringBuilder();
        URL url = new URL(accessUrl);
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        urlConn.setRequestMethod("GET");
        urlConn.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
          tmp.append(inputLine);
        }
        in.close();
        return tmp.toString();
      } catch (IOException e) {
        exception = e;
      }
    }
    throw exception;
  }

}
