package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.*;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ModelDaoTest extends PersistTestFunctions {
  ModelComparator modelComparator = new ModelComparator();

  Logger logger = LogManager.getLogger(ModelDaoTest.class);
  String projectId = "Some_id";
  int identifierCounter = 0;
  private Project project;

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void testLoadFromDb() throws Exception {
    Model model = createModel();
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);

    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());
    assertNotNull(model2);
    assertFalse(model2 == model);

    assertEquals(model.getElements().size(), model2.getElements().size());
    assertEquals(model.getLayers().size(), model2.getLayers().size());
    assertEquals(model.getReactions().size(), model2.getReactions().size());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, new ModelFullIndexed(model2)));

    modelDao.delete(model2);
    model2 = modelDao.getById(model.getId());
    assertNull(model2);
    project = projectDao.getById(project.getId());
  }

  @Test
  public void testModelWithAuthor() throws Exception {
    Model model = createModel();
    model.addAuthor(new Author("Piotr", "G"));
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);

    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testIndexesReload() throws Exception {
    Model model = createModel();
    project.addModel(model);
    modelDao.add(model);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());
    Model fullModel = new ModelFullIndexed(model2);
    assertNotNull(model2);

    assertEquals(model.getElements().size(), model2.getElements().size());
    assertEquals(model.getLayers().size(), model2.getLayers().size());
    assertEquals(model.getReactions().size(), model2.getReactions().size());

    // check if we really performed a test
    boolean test = false;

    for (Element alias : model.getElements()) {
      assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
      test = true;
    }
    assertTrue(test);

    test = false;
    for (Element alias : model.getElements()) {
      if (alias instanceof Compartment) {
        assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
        test = true;
      }
    }
    assertTrue(test);

    model2.setHeight(32);
    modelDao.update(model2.getModel());

    modelDao.delete(model2);
    model2 = modelDao.getById(model.getId());
    assertNull(model2);
  }

  @Test
  public void testReactionInModelAfterReload() throws Exception {
    Model model = createModel();
    Reaction reaction = model.getReactions().iterator().next();
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    Reaction reaction2 = null;
    for (Reaction r : model2.getReactions()) {
      if (r.getIdReaction().equals(reaction.getIdReaction())) {
        reaction2 = r;
      }
    }
    assertNotNull(reaction2);
    assertFalse(reaction.equals(reaction2));

    assertEquals(reaction.getNodes().size(), reaction2.getNodes().size());

    modelDao.delete(model2);
    model2 = modelDao.getById(model.getId());
    assertNull(model2);
    project = projectDao.getById(project.getId());
  }

  @Test
  public void testReactionInWithKinetics() throws Exception {
    Model model = createModel();
    Reaction reaction = model.getReactions().iterator().next();
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addElement(reaction.getReactants().get(0).getElement());
    kinetics.addFunction(createFunction());
    model.addFunctions(kinetics.getFunctions());
    kinetics.addParameter(createParameter());
    model.addUnit(kinetics.getParameters().iterator().next().getUnits());
    reaction.setKinetics(kinetics);
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testModulationReaction() throws Exception {
    Model model = createModel();
    ModulationReaction reaction = createModulationReaction(model.getElementByElementId("sa1117"),
        model.getElementByElementId("sa2"));
    model.addReaction(reaction);

    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testModelWithParameters() throws Exception {
    Model model = createModel();
    model.addParameter(createParameter());
    model.addUnit(model.getParameters().iterator().next().getUnits());
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);
    projectDao.evict(project);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  private SbmlParameter createParameter() {
    SbmlParameter parameter = new SbmlParameter("param_id");
    parameter.setName("X");
    parameter.setValue(4.7);
    parameter.setUnits(createUnits());
    return parameter;
  }

  private SbmlUnit createUnits() {
    SbmlUnit unit = new SbmlUnit("unit_id");
    unit.setName("u name");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.AMPERE, 1, 2, 3));
    return unit;
  }

  private SbmlFunction createFunction() {
    SbmlFunction result = new SbmlFunction("fun_id");
    result.setDefinition("def(k1)");
    result.addArgument("k1");
    result.setName("fun name");
    return result;
  }

  @Test
  public void testGetLastModelForProjectName() throws Exception {
    ModelData model3 = modelDao.getLastModelForProjectIdentifier(projectId, false);
    assertNull(model3);

    Model model = createModel();
    project.addModel(model);
    modelDao.add(model);

    ModelData newModel = modelDao.getLastModelForProjectIdentifier(projectId, false);
    assertNotNull(newModel);
    assertEquals(model.getId(), newModel.getId());

    Model model2 = createModel();
    project.addModel(model2);
    modelDao.add(model2);

    newModel = modelDao.getLastModelForProjectIdentifier(projectId, false);
    assertNotNull(newModel);
    assertEquals(model2.getId(), newModel.getId());

    modelDao.delete(model2);
    modelDao.delete(model);
  }

  /**
   * After adding model to db, modification residues disappear from the model...
   * 
   * @throws Exception
   */
  @Test
  public void testModificationsInProteins() throws Exception {
    Model model = createModel();
    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);

    modelDao.evict(model);
    projectDao.evict(project);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    Protein originalSpecies = (Protein) model.getElementByElementId("pr1");
    Protein fromDbSpecies = (Protein) model2.getElementByElementId("pr1");

    assertFalse(originalSpecies.equals(fromDbSpecies));
    assertEquals(originalSpecies.getModificationResidues().size(), fromDbSpecies.getModificationResidues().size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  /**
   * After adding model to db, miriam annotations disappear...
   * 
   * @throws Exception
   */
  @Test
  public void testMiriamInSpecies() throws Exception {
    Model model = createModel();
    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);

    modelDao.evict(model);
    projectDao.evict(project);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    Protein originalSpecies = (Protein) model.getElementByElementId("pr1");
    Protein fromDbSpecies = (Protein) model2.getElementByElementId("pr1");

    assertFalse(originalSpecies.equals(fromDbSpecies));
    assertEquals(originalSpecies.getMiriamData().size(), fromDbSpecies.getMiriamData().size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    layer.addLayerRect(createRect());

    Reaction reaction = createReaction(alias2, alias);
    model.addReaction(reaction);

    alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    Residue mr = new Residue();
    mr.setName("mr");
    mr.setPosition(new Point2D.Double(10, 20));
    alias.addResidue(mr);

    alias.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

}
