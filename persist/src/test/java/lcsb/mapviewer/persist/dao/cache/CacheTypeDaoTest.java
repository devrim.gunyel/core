package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.*;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CacheTypeDaoTest extends PersistTestFunctions {

  @Autowired
  protected CacheTypeDao cacheTypeDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testReactomeCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.reactome.utils.ReactomeConnector");
    assertNotNull(cacheType);
    assertEquals(0, cacheType.getId());
  }

  @Test
  public void testEmpty() throws Exception {
    assertNull(cacheTypeDao.getByClassName("blablabla"));
  }

  @Test
  public void testChemblCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.ChEMBLParser");
    assertNotNull(cacheType);
    assertEquals(2, cacheType.getId());
  }

  @Test
  public void testArticleCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.PubmedParser");
    assertNotNull(cacheType);
  }

  @Test
  public void testDrugBankCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.DrugbankHTMLParser");
    assertNotNull(cacheType);
  }

  @Test
  public void testMockCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.cache.MockCacheInterface");
    assertNotNull(cacheType);
  }

  @Test
  public void testChebiCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator");
    assertNotNull(cacheType);
  }

  @Test
  public void testGoCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.annotators.GoAnnotator");
    assertNotNull(cacheType);
  }
}
