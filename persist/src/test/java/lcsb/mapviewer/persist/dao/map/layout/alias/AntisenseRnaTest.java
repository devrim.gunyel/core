package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;

@Rollback(true)
public class AntisenseRnaTest extends PersistTestFunctions {
  Logger logger = LogManager.getLogger(AntisenseRnaTest.class);

  User admin;

  int identifierCounter = 0;
  String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    admin = userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN);
    Project project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAntisenseRnaRegionInDb() throws Exception {
    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(admin);

    Model model = createModel();

    project.addModel(model);
    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(projectId);
    assertNotNull(project2);
    assertFalse(project2.equals(project));
    assertEquals(project.getId(), project2.getId());

    Model model2 = new ModelFullIndexed(project2.getModels().iterator().next());

    Element sp = model.getElements().iterator().next();
    AntisenseRna ar = (AntisenseRna) sp;

    Element sp2 = model2.getElements().iterator().next();
    AntisenseRna ar2 = (AntisenseRna) sp2;

    projectDao.delete(project2);

    assertEquals(ar.getRegions().size(), ar2.getRegions().size());
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    AntisenseRna species = createAntisenseRna("As");
    species.addCodingRegion(new CodingRegion());
    model.addElement(species);

    return model;
  }

}
