package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CommentDaoTest extends PersistTestFunctions {

  @Autowired
  protected CommentDao commentDao;

  User user;
  String projectId = "Some_id";
  private Project project;

  @Before
  public void setUp() throws Exception {
    user = createUser();
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setOwner(user);
    project.setProjectId(projectId);
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
    userDao.delete(user);
  }

  @Test
  public void testGetById() throws Exception {
    Model model = createModel();
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);

    int counter = (int) commentDao.getCount();
    Comment comment = new Comment();
    comment.setUser(user);
    comment.setModel(model);
    commentDao.add(comment);
    int counter2 = (int) commentDao.getCount();
    assertEquals(counter + 1, counter2);
    Comment comment2 = commentDao.getById(comment.getId());
    assertNotNull(comment2);
    commentDao.delete(comment);
  }

  @Test
  public void testGetComments() {
    Model model = createModel();
    project.addModel(model);
    modelDao.add(model);
    projectDao.update(project);

    Comment comment = new Comment();
    comment.setDeleted(true);
    comment.setModel(model);
    commentDao.add(comment);

    Comment comment2 = new Comment();
    comment2.setPinned(true);
    comment2.setModel(model);
    commentDao.add(comment2);

    assertEquals(0, commentDao.getCommentByModel(model, true, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, false, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, null, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, true, false).size());
    assertEquals(0, commentDao.getCommentByModel(model, false, false).size());
    assertEquals(1, commentDao.getCommentByModel(model, null, false).size());
    assertEquals(1, commentDao.getCommentByModel(model, true, null).size());
    assertEquals(1, commentDao.getCommentByModel(model, false, null).size());
    assertEquals(2, commentDao.getCommentByModel(model, null, null).size());

    assertEquals(0, commentDao.getCommentByModel(model.getModelData(), true, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, false).size());
    assertEquals(0, commentDao.getCommentByModel(model.getModelData(), false, false).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, false).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, null).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, null).size());
    assertEquals(2, commentDao.getCommentByModel(model.getModelData(), null, null).size());

    commentDao.delete(comment);
    commentDao.delete(comment2);
    modelDao.delete(model);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    LayerRect lr = createRect();
    layer.addLayerRect(lr);

    Reaction reaction = createReaction(alias2, alias);
    reaction.setIdReaction("re" + identifierCounter++);
    model.addReaction(reaction);

    alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    Residue mr = new Residue();
    mr.setName("mr");
    alias.addResidue(mr);

    alias.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

}
