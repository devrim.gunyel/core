package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.*;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;

public class ElementDaoTest extends PersistTestFunctions {
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(ElementDaoTest.class);

  @Autowired
  private ElementDao elementDao;

  private Project project;
  private String projectId = "Some_id";

  private Integer testChargeVal = 1;
  private String testIdAlias = "a";
  private Double testInitialAmount = 2.0;
  private Double testInitialConcentration = 3.0;
  private String testName = "d";
  private String testNotes = "e";
  private Boolean testOnlySubstanceunits = true;

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void anotherSaveDaoTest() throws Exception {
    Model model = createModel();
    project.addModel(model);

    Element element = model.getElementByElementId("sa2");

    element.getSearchIndexes().add(new SearchIndex("blabla"));

    modelDao.add(model);

    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    Element element2 = elementDao.getById(model2.getElementByElementId("sa2").getId());

    assertEquals(1, element2.getSearchIndexes().size());

    modelDao.delete(model);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);
    model.addElement(createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
    model.addElement(createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
    model.addElement(createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
    model.addElement(createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

    Species alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    model.addElement(createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1"));
    model.setWidth(2000);
    model.setHeight(2000);
    return model;
  }

  @Test
  public void saveAliasWithSubmodelTest() throws Exception {
    long count = modelDao.getCount();
    Model model = createModel();
    Model model1 = createModel();
    Element element = model.getElementByElementId("sa2");
    ElementSubmodelConnection submodel = new ElementSubmodelConnection(model1, SubmodelType.UNKNOWN);
    element.setSubmodel(submodel);
    project.addModel(model);

    projectDao.add(project);
    projectDao.flush();

    long count2 = modelDao.getCount();
    assertEquals(count + 2, count2);
  }

  @Test
  public void saveWithUniprotData() throws Exception {
    Model model = createModel();
    project.addModel(model);

    Species element = model.getElementByElementId("sa2");
    UniprotRecord uniprotRecord = new UniprotRecord();
    Structure s = new Structure();
    s.setPdbId("x");
    s.setChainId("y");
    List<Structure> list = new ArrayList<>();
    list.add(s);
    s.setUniprot(uniprotRecord);
    uniprotRecord.addStructures(list);
    uniprotRecord.setSpecies(element);
    element.getUniprots().add(uniprotRecord);

    modelDao.add(model);
    modelDao.delete(model);
  }

  @Test
  public void testAdd() throws Exception {
    GenericProtein sp = createProtein(testIdAlias);
    sp.setCharge(testChargeVal);
    sp.setInitialAmount(testInitialAmount);
    sp.setInitialConcentration(testInitialConcentration);
    sp.setName(testName);
    sp.setNotes(testNotes);
    sp.setOnlySubstanceUnits(testOnlySubstanceunits);

    Compartment parent = createCompartment("comp id");
    sp.setCompartment(parent);

    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.UNKNOWN, "c");
    sp.addMiriamData(md);

    elementDao.add(sp);

    Species sp2 = (Species) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getCharge(), sp2.getCharge());
    assertEquals(sp.getElementId(), sp2.getElementId());
    assertEquals(sp.getInitialAmount(), sp2.getInitialAmount());
    assertEquals(sp.getInitialConcentration(), sp2.getInitialConcentration());
    assertEquals(sp.getName(), sp2.getName());
    assertEquals(sp.getNotes(), sp2.getNotes());
    assertEquals(sp.hasOnlySubstanceUnits(), sp2.hasOnlySubstanceUnits());

    Compartment parent2 = sp2.getCompartment();
    assertNotNull(parent2);
    assertEquals("comp id", parent2.getElementId());

    assertNotNull(sp2.getMiriamData());

    MiriamData md2 = sp2.getMiriamData().iterator().next();
    assertNotNull(md2);
    assertEquals(md.getDataType(), md2.getDataType());
    assertEquals(md.getRelationType(), md2.getRelationType());
    assertEquals(md.getResource(), md2.getResource());

    elementDao.delete(sp);
    sp2 = (Species) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testProtein() throws Exception {
    Protein protein = createProtein(testIdAlias);
    Residue mr = new Residue();
    mr.setPosition(new Point2D.Double(10, 20));
    mr.setName("name");
    mr.setState(ModificationState.GLYCOSYLATED);
    protein.addResidue(mr);

    elementDao.add(protein);

    Protein sp2 = (Protein) elementDao.getById(protein.getId());
    assertNotNull(sp2);
    assertEquals(protein.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getModificationResidues());
    assertEquals(1, sp2.getModificationResidues().size());

    Residue copy = (Residue) sp2.getModificationResidues().get(0);
    assertEquals(copy.getPosition(), mr.getPosition());
    assertEquals(copy.getIdModificationResidue(), mr.getIdModificationResidue());
    assertEquals(copy.getName(), mr.getName());
    assertEquals(copy.getState(), mr.getState());

    elementDao.delete(sp2);
    sp2 = (Protein) elementDao.getById(protein.getId());
    assertNull(sp2);
  }

  @Test
  public void testRna() throws Exception {
    Rna sp = createRna(testIdAlias);
    ModificationSite mr = new ModificationSite();
    mr.setName("name");
    mr.setState(ModificationState.DONT_CARE);
    sp.addModificationSite(mr);

    elementDao.add(sp);
    elementDao.evict(sp);

    Rna sp2 = (Rna) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getRegions());
    assertEquals(1, sp2.getRegions().size());

    ModificationSite copy = (ModificationSite) sp2.getRegions().get(0);
    assertEquals(copy.getId(), mr.getId());
    assertEquals(copy.getName(), mr.getName());
    assertEquals(copy.getState(), mr.getState());

    elementDao.delete(sp2);
    sp2 = (Rna) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testAntisenseRna() throws Exception {
    AntisenseRna sp = createAntisenseRna(testIdAlias);

    ModificationSite mr = new ModificationSite();
    mr.setName("name");
    sp.addModificationSite(mr);

    elementDao.add(sp);
    elementDao.evict(sp);

    AntisenseRna sp2 = (AntisenseRna) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getRegions());
    assertEquals(1, sp2.getRegions().size());

    assertEquals(sp2.getRegions().get(0).getIdModificationResidue(), mr.getIdModificationResidue());
    assertEquals(sp2.getRegions().get(0).getName(), mr.getName());

    elementDao.delete(sp2);
    sp2 = (AntisenseRna) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testSynonymsInAlias() throws Exception {
    Protein protein = createProtein(testIdAlias);
    protein.addSynonym("Synonym");
    protein.addSynonym("Synonym A");
    protein.addSynonym("A");

    protein.addFormerSymbol("Sym");
    protein.addFormerSymbol("Sym A");
    protein.addFormerSymbol("DD");

    elementDao.add(protein);
    elementDao.flush();

    elementDao.evict(protein);
    Protein sp2 = (Protein) elementDao.getById(protein.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(protein.getSynonyms().size(), sp2.getSynonyms().size());

    for (int i = 0; i < protein.getSynonyms().size(); i++) {
      assertEquals(protein.getSynonyms().get(i), sp2.getSynonyms().get(i));
    }

    assertNotNull(sp2.getFormerSymbols());
    assertEquals(protein.getFormerSymbols().size(), sp2.getFormerSymbols().size());

    for (int i = 0; i < protein.getFormerSymbols().size(); i++) {
      assertEquals(protein.getFormerSymbols().get(i), sp2.getFormerSymbols().get(i));
    }

    elementDao.delete(sp2);
    sp2 = (Protein) elementDao.getById(protein.getId());
    assertNull(sp2);
  }

  @Test
  public void testChemicals() throws Exception {
    Chemical ion = createIon(testIdAlias);
    ion.setInChI("come inchi");
    ion.setInChIKey("keyyy");
    ion.setSmiles("smile");

    elementDao.add(ion);
    elementDao.flush();

    elementDao.evict(ion);
    Ion sp2 = (Ion) elementDao.getById(ion.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(ion.getSynonyms().size(), sp2.getSynonyms().size());

    assertEquals(ion.getSmiles(), sp2.getSmiles());
    assertEquals(ion.getInChIKey(), sp2.getInChIKey());
    assertEquals(ion.getInChI(), sp2.getInChI());

    elementDao.delete(sp2);
    sp2 = (Ion) elementDao.getById(ion.getId());
    assertNull(sp2);
  }

  @Test
  public void testPhenotype() throws Exception {
    Phenotype phenotype = createPhenotype(testIdAlias);

    elementDao.add(phenotype);
    elementDao.flush();

    elementDao.evict(phenotype);
    Phenotype sp2 = (Phenotype) elementDao.getById(phenotype.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(phenotype.getSynonyms().size(), phenotype.getSynonyms().size());

    elementDao.delete(sp2);
    sp2 = (Phenotype) elementDao.getById(phenotype.getId());
    assertNull(sp2);
  }

}
