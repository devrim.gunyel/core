package lcsb.mapviewer.persist.dao.map.layout.alias;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AntisenseRnaTest.class,
    ElementDaoTest.class,
    RnaTest.class,
})
public class AllAliasTests {

}
