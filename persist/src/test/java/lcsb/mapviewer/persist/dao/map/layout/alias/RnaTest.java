package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class RnaTest extends PersistTestFunctions {
  Logger logger = LogManager.getLogger(RnaTest.class);

  int identifierCounter = 0;

  String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    Project project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testRnaRegionInDb() throws Exception {
    Project project = new Project();
    project.setProjectId(projectId);

    Model model = createModel();

    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    project.addModel(model);
    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(projectId);
    assertNotNull(project2);
    assertFalse(project2.equals(project));
    assertEquals(project.getId(), project2.getId());

    Model model2 = new ModelFullIndexed(project2.getModels().iterator().next());

    Element sp = model.getElements().iterator().next();
    Rna ar = (Rna) sp;

    Element sp2 = model2.getElements().iterator().next();
    Rna ar2 = (Rna) sp2;

    projectDao.delete(project2);

    assertEquals(ar.getRegions().size(), ar2.getRegions().size());
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Rna rna = createRna("As");
    rna.addCodingRegion(new CodingRegion());
    model.addElement(rna);

    return model;
  }

}
