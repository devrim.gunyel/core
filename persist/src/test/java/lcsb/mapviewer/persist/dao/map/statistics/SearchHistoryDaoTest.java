package lcsb.mapviewer.persist.dao.map.statistics;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.statistics.SearchHistory;
import lcsb.mapviewer.model.map.statistics.SearchType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class SearchHistoryDaoTest extends PersistTestFunctions {

  @Autowired
  protected SearchHistoryDao searchHistoryDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    SearchHistory searchHistory = new SearchHistory();
    long count = searchHistoryDao.getCount();
    searchHistory.setIpAddress("0.0.0.0");
    searchHistory.setQuery("query ...");
    searchHistory.setType(SearchType.GENERAL);
    searchHistoryDao.add(searchHistory);
    long count1 = searchHistoryDao.getCount();
    assertEquals(count + 1, count1);
  }

}
