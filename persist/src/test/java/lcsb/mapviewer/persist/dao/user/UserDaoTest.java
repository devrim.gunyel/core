package lcsb.mapviewer.persist.dao.user;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.*;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.*;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class UserDaoTest extends PersistTestFunctions {

  protected Logger logger = LogManager.getLogger(UserDaoTest.class.getName());
  private String testLogin = "test_login123";
  private String testEmail = "a@a.pl";
  private String testPasswd = "pwd";
  private String testLogin2 = "test_login_tmp";

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddUpdateDelete() {
    User user = userDao.getUserByLogin(testLogin);
    assertNull(user);

    long counter = userDao.getCount();

    user = new User();
    user.setCryptedPassword("");
    user.setLogin(testLogin);
    user.addPrivilege(new Privilege(PrivilegeType.CAN_CREATE_OVERLAYS));
    userDao.add(user);

    long counter2 = userDao.getCount();
    assertEquals(counter + 1, counter2);

    user.setLogin(testLogin2);
    userDao.update(user);

    User user2 = userDao.getUserByLogin(testLogin2);
    assertNotNull(user2);

    userDao.delete(user);

    user2 = userDao.getUserByLogin(testLogin2);
    assertNull(user2);
    counter2 = userDao.getCount();
    assertEquals(counter, counter2);
  }

  @Test(expected = PropertyValueException.class)
  public void testTryUserWithNullPassword() {
    User user = new User();
    user.setLogin(testLogin);
    userDao.add(user);
  }

  @Test(expected = PropertyValueException.class)
  public void testTryUserWithNullLogin() {
    User user = new User();
    user.setCryptedPassword("ZX");
    userDao.add(user);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testTryUserWithExistingLogin() {
    User user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    userDao.add(user);

    User user2 = new User();
    user2.setLogin(testLogin);
    user2.setCryptedPassword("");
    userDao.add(user2);
  }

  @Test
  public void testAddDeleteAdd() {
    User user = userDao.getUserByLogin(testLogin);
    assertNull(user);

    long counter = userDao.getCount();

    user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    userDao.add(user);

    long counter2 = userDao.getCount();
    assertEquals(counter + 1, counter2);

    userDao.delete(user);

    User user2 = userDao.getUserByLogin(testLogin);
    assertNull(user2);

    user2 = new User();
    user2.setCryptedPassword("");
    user2.setLogin(testLogin);
    userDao.add(user2);

    assertNotNull(userDao.getUserByLogin(testLogin));

    userDao.delete(user2);
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    User user = new User();
    user.setCryptedPassword(testPasswd);
    user.setLogin(testLogin);
    userDao.add(user);
    User user2 = userDao.getUserByLogin(testLogin);
    assertNotNull(user2);
    assertEquals(user2.getId(), user.getId());
    assertEquals(user2.getLogin(), user.getLogin());
    assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
    user2 = userDao.getUserByLogin(testLogin2);
    assertNull(user2);
    userDao.delete(user);
  }

  @Test
  public void testGetUserByEmail() throws Exception {
    User user = new User();
    user.setCryptedPassword(testPasswd);
    user.setLogin(testLogin);
    user.setEmail(testEmail);
    user.addPrivilege(new Privilege(PrivilegeType.CAN_CREATE_OVERLAYS));
    userDao.add(user);
    User user2 = userDao.getUserByEmail(testEmail);
    assertNotNull(user2);
    assertEquals(user2.getId(), user.getId());
    assertEquals(user2.getLogin(), user.getLogin());
    assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
    user2 = userDao.getUserByEmail(testEmail + "sadas");
    assertNull(user2);
    userDao.delete(user);
  }

  @Test
  public void testGetAll() throws Exception {
    assertTrue(userDao.getAll().size() > 0);
  }

  @Test
  public void testUserWithAnnotatorSchema() throws Exception {
    User user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    UserAnnotationSchema uas = new UserAnnotationSchema();
    uas.setValidateMiriamTypes(true);
    UserClassAnnotators ca = new UserClassAnnotators();
    ca.setClassName(Species.class);
    AnnotatorData annotatorData = new AnnotatorData(String.class);
    ca.addAnnotator(annotatorData);
    uas.addClassAnnotator(ca);
    uas.addClassAnnotator(new UserClassAnnotators(String.class, new ArrayList<>()));
    UserClassValidAnnotations cva = new UserClassValidAnnotations();
    cva.setClassName(Reaction.class);
    cva.addValidMiriamType(MiriamType.HGNC);
    cva.addValidMiriamType(MiriamType.HGNC_SYMBOL);
    uas.addClassValidAnnotations(cva);
    uas.addClassValidAnnotations(new UserClassValidAnnotations(String.class, new ArrayList<>()));
    uas.addClassValidAnnotations(new UserClassValidAnnotations(Integer.class, new ArrayList<>()));
    user.setAnnotationSchema(uas);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(2, uas2.getClassAnnotators().size());
    assertEquals(3, uas2.getClassValidAnnotators().size());
    assertEquals(Species.class.getCanonicalName(), uas.getClassAnnotators().get(0).getClassName());
    assertEquals(annotatorData, uas.getClassAnnotators().get(0).getAnnotators().get(0));
    assertEquals(Reaction.class.getCanonicalName(), uas.getClassValidAnnotators().get(0).getClassName());
    assertEquals(MiriamType.HGNC, uas.getClassValidAnnotators().get(0).getValidMiriamTypes().get(0));
    assertEquals(MiriamType.HGNC_SYMBOL, uas.getClassValidAnnotators().get(0).getValidMiriamTypes().get(1));

    userDao.delete(user2);
  }

  @Test
  public void testUserWithAnnotatorSchemaGuiPreferences() throws Exception {
    User user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    UserAnnotationSchema uas = new UserAnnotationSchema();
    user.setAnnotationSchema(uas);
    UserGuiPreference option = new UserGuiPreference();
    option.setKey("key");
    option.setValue("val");
    uas.addGuiPreference(option);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(1, uas2.getGuiPreferences().size());

    userDao.delete(user2);
  }

  @Test
  public void testUserWithAnnotatorSchemaRequiredAnnotations() throws Exception {
    User user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    UserAnnotationSchema uas = new UserAnnotationSchema();
    user.setAnnotationSchema(uas);
    uas.addClassRequiredAnnotations(
        new UserClassRequiredAnnotations(String.class, new MiriamType[] { MiriamType.WIKIPEDIA }));
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(1, uas2.getClassRequiredAnnotators().size());

    userDao.delete(user2);
  }

  @Test
  public void testUserWithAnnotatorParams() throws Exception {
    User user = new User();
    user.setLogin(testLogin);
    user.setCryptedPassword("");
    UserAnnotationSchema uas = new UserAnnotationSchema();
    user.setAnnotationSchema(uas);
    UserClassAnnotators classAnnotators = new UserClassAnnotators(Integer.class);
    AnnotatorData annotatorData = new AnnotatorData(String.class);
    classAnnotators.addAnnotator(annotatorData);
    uas.addClassAnnotator(classAnnotators);
    AnnotatorConfigParameter param = new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER,
        "val");
    annotatorData.addAnnotatorParameter(param);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(1, uas2.getClassAnnotators().get(0).getAnnotators().get(0).getAnnotatorParams().size());

    userDao.delete(user2);
  }

}
