package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.*;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class LayoutDaoTest extends PersistTestFunctions {

  final static Logger logger = LogManager.getLogger(LayoutDaoTest.class);
  @Autowired
  private LayoutDao layoutDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  /**
   * After adding layouts to model.
   * 
   * @throws Exception
   */
  @Test
  public void testLayoutsWithData() throws Exception {
    Model model = createModel();

    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);

    modelDao.evict(model);
    projectDao.evict(project);

    Layout layout = new Layout();
    layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, "tmp"));
    layout.setTitle("temporary name");
    layout.setColorSchemaType(ColorSchemaType.GENERIC);

    byte[] data = "test".getBytes();
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(data);

    layout.setInputData(fileEntry);
    project.addLayout(layout);
    layoutDao.add(layout);

    layoutDao.evict(layout);

    layout = layoutDao.getById(layout.getId());

    assertEquals("test", new String(layout.getInputData().getFileContent(), StandardCharsets.UTF_8));

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  /**
   * After adding layouts to model.
   * 
   * @throws Exception
   */
  @Test
  public void testLayoutsWithoutData() throws Exception {
    Model model = createModel();
    String tempName = "temporary name";

    User user = createUser();

    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    projectDao.add(project);

    assertEquals(0, layoutDao.getLayoutsByProject(project).size());
    assertNull(layoutDao.getLayoutByName(project, tempName));

    Layout layout = new Layout();
    layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, "tmp"));
    layout.setColorSchemaType(ColorSchemaType.GENERIC);
    layout.setTitle(tempName);
    layout.setCreator(user);
    project.addLayout(layout);
    layoutDao.add(layout);

    modelDao.evict(model);
    projectDao.evict(project);
    layoutDao.evict(layout);

    layout = layoutDao.getById(layout.getId());

    assertEquals(1, layoutDao.getLayoutsByProject(project).size());
    assertNotNull(layoutDao.getLayoutByName(project, tempName));

    assertNull(layout.getInputData());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);

    userDao.delete(user);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Species alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    layer.addLayerRect(createRect());

    Reaction reaction = createReaction(alias2, alias);
    model.addReaction(reaction);

    Protein protein = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(protein);

    Residue mr = new Residue();
    mr.setName("mr");
    protein.addResidue(mr);

    protein.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

  @Test
  public void testGetLayoutsByProject() throws Exception {
    Model model = createModel();

    Project project = new Project("test_project_id");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    project.addModel(model);
    projectDao.add(project);

    List<Layout> result = layoutDao.getLayoutsByProject(project);
    assertEquals(0, result.size());

    Layout layout = new Layout();
    layout.setColorSchemaType(ColorSchemaType.GENERIC);
    layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, "tmp"));
    layout.setTitle("temporary name");

    byte[] data = "test".getBytes();
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(data);

    layout.setInputData(fileEntry);
    project.addLayout(layout);
    layoutDao.add(layout);

    result = layoutDao.getLayoutsByProject(project);
    assertEquals(1, result.size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

}
