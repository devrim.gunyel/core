package lcsb.mapviewer.persist.dao.graphics;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PolylineDataTest.class })
public class AllGraphicsDaoTests {

}
