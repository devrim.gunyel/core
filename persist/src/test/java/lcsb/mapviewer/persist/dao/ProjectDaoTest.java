package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.*;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.layout.*;
import lcsb.mapviewer.model.map.model.*;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ProjectDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  private Logger logger = LogManager.getLogger(ProjectDaoTest.class);
  private String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetProjectByName() throws Exception {
    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(projectId);
    assertNotNull(project2);
    assertFalse(project2.equals(project));
    assertEquals(project.getId(), project2.getId());

    projectDao.delete(project2);
  }

  @Test
  public void testGetProjectForModelId() throws Exception {
    Project project = new Project();
    project.setProjectId(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    Model model = new ModelFullIndexed(null);
    project.addModel(model);
    modelDao.add(model);
    modelDao.evict(model);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectForModelId(model.getId());
    assertNotNull(project2);
    assertFalse(project2.equals(project));
    assertEquals(project.getId(), project2.getId());

    modelDao.delete(modelDao.getById(model.getId()));
    projectDao.delete(project2);

    assertNull(projectDao.getProjectForModelId(model.getId()));
  }

  @Test
  public void testProjectExists() throws Exception {
    Project project = new Project(projectId);

    assertFalse(projectDao.isProjectExistsByName(projectId));

    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectDao.evict(project);

    assertTrue(projectDao.isProjectExistsByName(projectId));

    Project project2 = projectDao.getProjectByProjectId(projectId);

    projectDao.delete(project2);
  }

  @Test
  public void testCheckEqualityAfterReload() throws Exception {
    Project project = new Project(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);
    projectDao.flush();

    Model model = createModel();
    project.addModel(model);

    projectDao.update(project);
    projectDao.evict(project);

    ModelComparator comparator = new ModelComparator();

    Model model2 = new ModelFullIndexed(modelDao.getLastModelForProjectIdentifier(projectId, false));

    assertEquals(0, comparator.compare(model, model2));

    projectDao.delete(projectDao.getById(project.getId()));
  }

  @Test
  public void testCheckEqualityAfterReload2() throws Exception {
    Project project = new Project(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    Model model = createModel();
    project.addModel(model);

    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(projectId);

    ModelComparator comparator = new ModelComparator();

    Model fullModel1 = new ModelFullIndexed(project.getModels().iterator().next());
    Model fullModel2 = new ModelFullIndexed(project2.getModels().iterator().next());
    assertEquals(0, comparator.compare(fullModel1, fullModel2));

    projectDao.delete(project2);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    model.addElement(createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
    model.addElement(createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
    model.addElement(createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
    model.addElement(createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

    Species alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    model.addElement(createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1"));
    model.setWidth(2000);
    model.setHeight(2000);
    return model;
  }

  @Test
  public void testAddGetProjectWithOverviewImage() throws Exception {
    Project project = new Project(projectId);
    Model model = new ModelFullIndexed(null);
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test");
    OverviewModelLink oml = new OverviewModelLink();
    oml.setPolygon("10,10 20,20 20,100");
    oml.setxCoord(1);
    oml.setyCoord(2);
    oml.setZoomLevel(3);
    oml.setLinkedModel(model);
    oi.addLink(oml);
    project.addOverviewImage(oi);
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(projectId);
    assertNotNull(project2);

    OverviewImage oi2 = project2.getOverviewImages().get(0);
    OverviewModelLink oml2 = (OverviewModelLink) oi2.getLinks().get(0);

    assertEquals(oi.getFilename(), oi2.getFilename());
    assertEquals(oml.getPolygon(), oml2.getPolygon());
    assertEquals(oml.getxCoord(), oml2.getxCoord());
    assertEquals(oml.getyCoord(), oml2.getyCoord());
    assertEquals(oml.getZoomLevel(), oml2.getZoomLevel());
    assertNotNull(oml2.getPolygonCoordinates());

    projectDao.delete(project2);
  }

  /**
   * After adding model to db with creation warnings...
   * 
   * @throws Exception
   */
  @Test
  public void testCreationWarnings() throws Exception {
    Project project = new Project("test_project_id");
    ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("content");
    entry.setSeverity("WARN");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    assertEquals(1, project.getLogEntries().size());

    projectDao.add(project);

    projectDao.evict(project);
    Project project2 = projectDao.getById(project.getId());

    assertEquals(1, project2.getLogEntries().size());

    projectDao.delete(project2);
  }

  @Test
  public void testGetAll() throws Exception {
    long startTime = System.currentTimeMillis();
    double max = 10;

    logger.debug("---");
    for (int i = 0; i < max; i++) {
      projectDao.getAll();
    }
    long estimatedTime = System.currentTimeMillis() - startTime;
    logger.debug(estimatedTime / max);
  }

  @Test
  public void testLayoutsInProject() throws Exception {
    Project project = new Project("test_porject_id");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    Model model = createModel();
    project.addModel(model);
    projectDao.add(project);

    Layout layout = new Layout();
    layout.addDataOverlayImageLayer(new DataOverlayImageLayer(model, "tmp"));
    layout.setColorSchemaType(ColorSchemaType.GENERIC);
    layout.setTitle("temporary name");
    project.addLayout(layout);

    modelDao.evict(model);
    projectDao.evict(project);
    Project project2 = projectDao.getById(project.getId());

    assertEquals(1, project2.getLayouts().size());
    assertEquals("tmp",
        project2.getLayouts().iterator().next().getDataOverlayImageLayers().iterator().next().getDirectory());
    assertEquals("temporary name", project2.getLayouts().iterator().next().getTitle());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  @Test
  public void testGetNextId() throws Exception {
    long id = projectDao.getNextId();
    Project project = new Project(projectId);
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    projectDao.add(project);

    long id2 = projectDao.getNextId();
    assertNotEquals(id, id2);
  }

}
