package lcsb.mapviewer.persist.dao.map;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.persist.dao.map.layout.AllLayoutTests;
import lcsb.mapviewer.persist.dao.map.statistics.StatisticsAllTests;

@RunWith(Suite.class)
@SuiteClasses({ AllLayoutTests.class,
    CommentDaoTest.class,
    LayoutDaoTest.class,
    ModelDaoTest.class,
    StatisticsAllTests.class,
})
public class AllMapDaoTests {

}
