package lcsb.mapviewer.persist.dao.plugin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PluginDaoTest.class,
    PluginDataEntryDaoTest.class })
public class AllPluginTests {

}
