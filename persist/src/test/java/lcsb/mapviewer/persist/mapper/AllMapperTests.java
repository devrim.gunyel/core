package lcsb.mapviewer.persist.mapper;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ArrowTypeDataMapperTest.class, Point2DMapperTest.class })
public class AllMapperTests {

}
