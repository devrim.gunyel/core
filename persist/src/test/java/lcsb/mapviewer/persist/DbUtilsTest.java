package lcsb.mapviewer.persist;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.common.exception.InvalidStateException;

public class DbUtilsTest extends PersistTestFunctions {
  @Autowired
  protected DbUtils dbUtils;
  Logger logger = LogManager.getLogger(DbUtilsTest.class);

  @Test
  public void testCreateSession() throws Exception {
    assertFalse(dbUtils.isCustomSessionForCurrentThread());
    dbUtils.createSessionForCurrentThread();
    assertTrue(dbUtils.isCustomSessionForCurrentThread());
    assertEquals(1, dbUtils.getSessionCounter());
    dbUtils.closeSessionForCurrentThread();
    assertFalse(dbUtils.isCustomSessionForCurrentThread());
    assertEquals(0, dbUtils.getSessionCounter());
  }

  @Test(expected = InvalidStateException.class)
  public void testCreateInvalidSession() throws Exception {
    try {
      dbUtils.createSessionForCurrentThread();
      dbUtils.createSessionForCurrentThread();
    } finally {
      dbUtils.closeSessionForCurrentThread();
    }
  }

  @Test
  public void testAutoFlush() {
    boolean autoflush = dbUtils.isAutoFlush();
    dbUtils.setAutoFlush(false);
    assertFalse(dbUtils.isAutoFlush());
    dbUtils.setAutoFlush(autoflush);
  }

}
