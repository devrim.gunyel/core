package lcsb.mapviewer.persist;

import java.awt.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.UnitTestFailedWatcher;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.reaction.*;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.*;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.persist.dao.user.UserDao;

@Transactional
@Rollback(true)
@ContextConfiguration(classes = SpringPersistTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class PersistTestFunctions {

  protected static String ADMIN_BUILT_IN_LOGIN = "admin";

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  public double EPSILON = 1e-6;
  @Autowired
  protected ProjectDao projectDao;
  @Autowired
  protected ModelDao modelDao;
  @Autowired
  protected UserDao userDao;
  protected int identifierCounter = 0;
  PointTransformation pt = new PointTransformation();
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(PersistTestFunctions.class);
  private int zIndex = 0;

  protected User createUser() {
    User user = new User();
    user.setName("John");
    user.setSurname("Doe");
    user.setEmail("john.doe@uni.lu");
    user.setLogin("john.doe");
    user.setCryptedPassword("passwd");
    userDao.add(user);
    return user;
  }

  protected Compartment createCompartment(double x, double y, double width, double height, String compartmentId) {
    Compartment compartment = new Compartment(compartmentId);
    compartment.setX(x);
    compartment.setY(y);
    compartment.setZ(zIndex++);
    compartment.setWidth(width);
    compartment.setHeight(height);
    return compartment;
  }

  protected Compartment createCompartment(String compartmentId) {
    return createCompartment(10, 20, 100, 200, compartmentId);
  }

  protected GenericProtein createProtein(String proteinId) {
    return createProtein(0, 0, 100, 200, proteinId);
  }

  protected GenericProtein createProtein(double x, double y, double width, double height, String proteinId) {
    GenericProtein protein = new GenericProtein("s" + identifierCounter++);
    protein.setElementId(proteinId);
    protein.setX(x);
    protein.setY(y);
    protein.setZ(zIndex++);
    protein.setWidth(width);
    protein.setHeight(height);
    return protein;
  }

  protected Complex createComplex(double x, double y, double width, double height, String complexId) {
    Complex complex = new Complex(complexId);
    complex.setX(x);
    complex.setY(y);
    complex.setZ(zIndex++);
    complex.setWidth(width);
    complex.setHeight(height);
    return complex;
  }

  protected AntisenseRna createAntisenseRna(String aRnaId) {
    AntisenseRna antisenseRna = new AntisenseRna(aRnaId);
    antisenseRna.setX(1);
    antisenseRna.setY(2);
    antisenseRna.setZ(zIndex++);
    antisenseRna.setWidth(10);
    antisenseRna.setHeight(20);
    return antisenseRna;
  }

  protected Chemical createIon(String ionId) {
    Chemical ion = new Ion(ionId);
    ion.setZ(zIndex++);
    return ion;
  }

  protected Phenotype createPhenotype(String phenotypeId) {
    Phenotype phenotype = new Phenotype(phenotypeId);
    phenotype.setZ(zIndex++);
    return phenotype;
  }

  protected Rna createRna(String rnaId) {
    Rna rna = new Rna(rnaId);
    rna.setX(1);
    rna.setY(2);
    rna.setZ(zIndex++);
    rna.setWidth(10);
    rna.setHeight(20);
    return rna;
  }

  protected TransportReaction createReaction(Element reactantElement, Element productElement) {
    TransportReaction result = new TransportReaction("re" + identifierCounter++);
    addLines(reactantElement, productElement, result);
    return result;
  }

  private void addLines(Element reactantElement, Element productElement, Reaction result) {
    Product product = new Product(productElement);
    product.setLine(new PolylineData(productElement.getCenter(),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6)));
    result.addProduct(product);
    Reactant reactant = new Reactant(reactantElement);
    reactant.setLine(new PolylineData(productElement.getCenter(),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4)));
    result.addReactant(reactant);
    result.setZ(zIndex++);
    result.setLine(new PolylineData(pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6)));
  }

  protected ModulationReaction createModulationReaction(Element reactantElement, Element productElement) {
    ModulationReaction result = new ModulationReaction("re" + identifierCounter++);
    addLines(reactantElement, productElement, result);
    result.setZ(zIndex++);
    return result;
  }

  protected LayerRect createRect() {
    LayerRect lr = new LayerRect();
    lr.setColor(Color.YELLOW);
    lr.setZ(0);
    return lr;
  }

}
