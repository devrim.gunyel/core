package lcsb.mapviewer.persist;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import lcsb.mapviewer.persist.dao.AllDaoTests;
import lcsb.mapviewer.persist.mapper.AllMapperTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ApplicationContextLoaderTest.class,
    AllDaoTests.class,
    AllMapperTests.class,
    DbUtilsTest.class,
    InitialStateTest.class,
    SpringApplicationContextTest.class,

})
public class AllDbTests {

}
