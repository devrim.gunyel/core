/**
 * This package contains data structures and access objects for everything
 * stored in database.
 */
package lcsb.mapviewer.persist;
