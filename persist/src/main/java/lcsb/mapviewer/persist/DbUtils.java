package lcsb.mapviewer.persist;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.common.exception.InvalidStateException;

/**
 * This class contains a set of methods that allow to manage hibernate sessions
 * in multi-thread approach. There are two types of sessions used in the system:
 * <ul>
 * <li>default - created and managed by sessionFactory</li>
 * <li>custom - created and managed by this class, used only when new thread is
 * created manually.</li>
 * </ul>
 * Class extends {@link Observable}. Whenever session is created or closed
 * observers are notified.
 * 
 * @author Piotr Gawron
 * 
 */
@SuppressWarnings("deprecation")
@Service
public class DbUtils extends Observable {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger(DbUtils.class);
  /**
   * Static map containing opened custom sessions. This object is also used for
   * synchronization between threads when accessing informations about sessions.
   */
  private static Map<Long, Session> sessionForThread = new HashMap<Long, Session>();
  /**
   * This determines if every add/update/delete operation should be flushed (for
   * specific {@link Thread}). There are few drawbacks of this approach:
   * <ol>
   * <li>when autoflushing is set to false, then the data consistency could be
   * broken (even in the same thread/transaction)</li>,
   * <li>we have to automatically take care of the flushing, therefore there might
   * be some problems with long transactions.</li>
   * </ol>
   */
  private static Map<Long, Boolean> autoFlushForThread = new HashMap<>();
  /**
   * Hibernate session factory.
   */
  private SessionFactory sessionFactory;

  /**
   * Constructor
   * 
   * @param sessionFactory
   */
  @Autowired
  public DbUtils(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * Returns hibernate session for current thread.
   * 
   * @return session for the current thread
   */
  public Session getSessionForCurrentThread() {
    Long id = Thread.currentThread().getId();
    Session session = null;
    synchronized (sessionForThread) {
      session = sessionForThread.get(id);
    }
    if (session == null) {
      session = sessionFactory.getCurrentSession();
    }
    return session;
  }

  /**
   * Closes custom session for current thread.
   */
  public void closeSessionForCurrentThread() {
    Long id = Thread.currentThread().getId();
    Session session = null;
    synchronized (sessionForThread) {
      session = sessionForThread.get(id);
      sessionForThread.remove(id);
    }
    if (session != null) {
      logger.debug("Closing session for thread: " + id);
      session.getTransaction().commit();

      session.close();
      int counter = -1;
      synchronized (sessionForThread) {
        sessionForThread.remove(id);
        counter = sessionForThread.size();
      }
      synchronized (autoFlushForThread) {
        autoFlushForThread.remove(id);
      }
      setChanged();
      notifyObservers(counter);
      logger.debug("Session closed for thread: " + id);
    }
  }

  /**
   * Creates custom session for current thread.
   */
  public void createSessionForCurrentThread() {
    Long id = Thread.currentThread().getId();
    Session session = null;
    synchronized (sessionForThread) {
      session = sessionForThread.get(id);
    }

    // we cannot create two threads for one session
    if (session != null) {
      throw new InvalidStateException("Current thread already has an active session");
    } else {
      logger.debug("Creating session for thread: " + id);
      session = sessionFactory.openSession();
      logger.debug("Session opened: " + id);
      session.beginTransaction();
      logger.debug("Session started: " + id);
      Integer counter = -1;
      synchronized (sessionForThread) {
        sessionForThread.put(id, session);
        counter = sessionForThread.size();
      }
      synchronized (autoFlushForThread) {
        autoFlushForThread.put(id, true);
      }
      setChanged();
      notifyObservers(counter);
    }
  }

  /**
   * Returns true if custom session was opened for this thread.
   * 
   * @return <code>true</code> if custom session was created for this thread,
   *         <code>false</code> otherwise
   */
  public boolean isCustomSessionForCurrentThread() {
    Long id = Thread.currentThread().getId();
    Session session = null;
    synchronized (sessionForThread) {
      session = sessionForThread.get(id);
    }
    return session != null;

  }

  /**
   * Returns number of opened custom sessions.
   * 
   * @return number of sessions
   */
  public int getSessionCounter() {
    int result = -1;
    synchronized (sessionForThread) {
      result = sessionForThread.values().size();
    }
    return result;
  }

  /**
   * @return the sessionFactory
   * @see #sessionFactory
   */
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  /**
   * @param sessionFactory
   *          the sessionFactory to set
   * @see #sessionFactory
   */
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * Returns info if the flush is automatically done or not in current
   * {@link Thread}.
   * 
   * @return <code>true</code> if flush is automatically done, <code>false</code>
   *         otherwise
   * @see #autoFlushForThread
   */
  public boolean isAutoFlush() {
    Long id = Thread.currentThread().getId();
    Boolean result = true;
    synchronized (autoFlushForThread) {
      result = autoFlushForThread.get(id);
    }
    if (result == null) {
      logger.debug("autoFlush not set for thread: " + id + ". Setting true");
      synchronized (autoFlushForThread) {
        autoFlushForThread.put(id, true);
      }
      result = true;
    }
    return result;
  }

  /**
   * Set autoFlush for current {@link Thread}.
   * 
   * @param autoFlush
   *          the new autoflush value
   * @see #autoFlushForThread
   */
  public void setAutoFlush(boolean autoFlush) {
    Long id = Thread.currentThread().getId();
    synchronized (autoFlushForThread) {
      autoFlushForThread.put(id, autoFlush);
    }
  }

  public Dialect getDialect() {
    return ((SessionFactoryImpl) sessionFactory).getDialect();
  }
}
