package lcsb.mapviewer.persist.mapper;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.sql.*;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

import lcsb.mapviewer.model.graphics.*;

/**
 * This class allows to put {@link Point2D} objects into hibernate based models.
 * 
 * @author Piotr Gawron
 * 
 */
public class ArrowTypeDataMapper implements CompositeUserType {

  @Override
  public String[] getPropertyNames() {
    return new String[] { "val" };
  }

  @Override
  public Type[] getPropertyTypes() {
    return new Type[] { StringType.INSTANCE };
  }

  @Override
  public Object getPropertyValue(Object component, int property) {
    Object returnValue = null;
    final ArrowTypeData atd = (ArrowTypeData) component;
    if (0 == property) {

      returnValue = arrowTypeDataToString(atd);
    }
    return returnValue;
  }

  @Override
  public void setPropertyValue(Object component, int property, Object value) {
    final ArrowTypeData atd = (ArrowTypeData) component;
    if (0 == property) {
      assignSerializedProperties(atd, (String) value);
    }
  }

  @Override
  public Class<?> returnedClass() {
    return ArrowTypeData.class;
  }

  @Override
  public boolean equals(Object o1, Object o2) {
    boolean isEqual = false;
    if (o1 == o2) {
      isEqual = false;
    }
    if (null == o1 || null == o2) {
      isEqual = false;
    } else {
      isEqual = o1.equals(o2);
    }
    return isEqual;
  }

  @Override
  public int hashCode(Object x) {
    return x.hashCode();
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
      throws HibernateException, SQLException {
    ArrowTypeData atd = new ArrowTypeData();
    final String val = rs.getString(names[0]);
    if (!rs.wasNull()) {
      assignSerializedProperties(atd, val);
    }
    return atd;
  }

  @Override
  public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
      throws HibernateException, SQLException {
    if (value == null) {
      st.setNull(index, StringType.INSTANCE.sqlType());
    } else {
      final ArrowTypeData atd = (ArrowTypeData) value;
      st.setString(index, arrowTypeDataToString(atd));
    }
  }

  @Override
  public Object deepCopy(Object value) {
    if (value == null) {
      return null;
    }
    final ArrowTypeData receivedParam = (ArrowTypeData) value;
    final ArrowTypeData point = receivedParam.copy();
    return point;
  }

  @Override
  public boolean isMutable() {
    return true;
  }

  @Override
  public Serializable disassemble(Object value, SharedSessionContractImplementor session) throws HibernateException {
    return (Serializable) value;
  }

  @Override
  public Object assemble(Serializable cached, SharedSessionContractImplementor session, Object owner)
      throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(Object original, Object target, SharedSessionContractImplementor session, Object owner)
      throws HibernateException {
    return this.deepCopy(original);
  }

  private String arrowTypeDataToString(final ArrowTypeData atd) {
    return atd.getAngle() + ";" + atd.getLen() + ";" + atd.getArrowType().name() + ";" + atd.getArrowLineType();
  }

  private void assignSerializedProperties(ArrowTypeData atd, final String val) {
    final String[] values = val.split(";");
    atd.setAngle(Double.parseDouble(values[0]));
    atd.setLen(Double.parseDouble(values[1]));
    atd.setArrowType(ArrowType.valueOf(values[2]));
    atd.setArrowLineType(LineType.valueOf(values[3]));
  }

}
