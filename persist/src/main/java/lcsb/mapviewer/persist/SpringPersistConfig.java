package lcsb.mapviewer.persist;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "lcsb.mapviewer.persist" })
public class SpringPersistConfig {

  @Bean
  public ConfigurationHolder config() {
    return new ConfigurationHolder();
  }

  @Bean(destroyMethod = "close")
  public HikariDataSource dataSource(ConfigurationHolder config) {
    HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setJdbcUrl(config.getDbUri());
    dataSource.setUsername(config.getDbUsername());
    dataSource.setPassword(config.getDbPassword());
    dataSource.setIdleTimeout(30000);
    dataSource.setMaximumPoolSize(40);
    dataSource.setMinimumIdle(10);
    dataSource.setConnectionTestQuery("select 1");
    return dataSource;
  }

  @Bean
  public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
    Properties props = new Properties();
    props.setProperty(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
    props.setProperty(Environment.SHOW_SQL, "false");
    LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
    factoryBean.setDataSource(dataSource);
    factoryBean.setHibernateProperties(props);
    factoryBean.setPackagesToScan("lcsb.mapviewer.model");
    factoryBean.setImplicitNamingStrategy(new CustomImplicitNamingStrategy());
    factoryBean.setPhysicalNamingStrategy(new SpringPhysicalNamingStrategy());
    return factoryBean;
  }

  @Bean
  public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
    HibernateTransactionManager transactionManager = new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory);
    return transactionManager;
  }

  @Bean
  public DataSourceInitializer dataSourceInitializer(DataSource dataSource, DatabasePopulator databasePopulator) {
    DataSourceInitializer initializer = new DataSourceInitializer();
    initializer.setDataSource(dataSource);
    initializer.setDatabasePopulator(databasePopulator);
    initializer.setEnabled(true);
    return initializer;
  }

}
