package lcsb.mapviewer.persist;

import java.sql.Connection;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Service;

@Service
public class CustomDatabasePopulator implements DatabasePopulator {

  private ConfigurationHolder config;

  private DataSource dataSource;

  @Autowired
  public CustomDatabasePopulator(ConfigurationHolder config, DataSource dataSource) {
    this.config = config;
    this.dataSource = dataSource;
  }

  @Override
  public void populate(Connection connection) throws ScriptException {
    Flyway.configure().dataSource(config.getDbUri(), config.getDbUsername(), config.getDbPassword())
        .baselineVersion("12.1.0")
        .locations("classpath:lcsb.mapviewer.persist/migration", "classpath:db.migration")
        .outOfOrder(true)
        .baselineOnMigrate(true)
        .validateOnMigrate(false)
        .load()
        .migrate();
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

}
