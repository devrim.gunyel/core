package lcsb.mapviewer.persist;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Service;

@Service
@PropertySources({
    @PropertySource("classpath:db.properties"),
    @PropertySource(value = "file:/etc/minerva/db.properties", ignoreResourceNotFound = true)
})
public class ConfigurationHolder {

  @Value("${database.uri}")
  private String dbUri;

  @Value("${database.username}")
  private String dbUsername;

  @Value("${database.password}")
  private String dbPassword;

  public String getDbUri() {
    return dbUri;
  }

  public void setDbUri(String dbUri) {
    this.dbUri = dbUri;
  }

  public String getDbUsername() {
    return dbUsername;
  }

  public void setDbUsername(String dbUsername) {
    this.dbUsername = dbUsername;
  }

  public String getDbPassword() {
    return dbPassword;
  }

  public void setDbPassword(String dbPassword) {
    this.dbPassword = dbPassword;
  }
}
