package lcsb.mapviewer.persist.migration;

import java.sql.ResultSet;
import java.sql.Statement;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class V14_0_0_20190701__bcrypt_passwords extends BaseJavaMigration {

  @Override
  public void migrate(Context context) throws Exception {
    try (Statement select = context.getConnection().createStatement()) {
      try (ResultSet rows = select.executeQuery("select id, crypted_password from user_table")) {
        while (rows.next()) {
          int id = rows.getInt(1);
          String md5Password = rows.getString(2);
          String bcryptMd5Password = BCrypt.hashpw(md5Password, BCrypt.gensalt());
          try (Statement update = context.getConnection().createStatement()) {
            update.execute("update user_table set crypted_password = '" + bcryptMd5Password + "' where id = " + id);
          }
        }
      }
    }
  }

}
