package lcsb.mapviewer.persist.dao.map.statistics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.statistics.SearchHistory;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link SearchHistory} class.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class SearchHistoryDao extends BaseDao<SearchHistory> {
  /**
   * Default constructor.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(SearchHistoryDao.class);

  /**
   * Default constructor.
   */
  public SearchHistoryDao() {
    super(SearchHistory.class);
  }

}
