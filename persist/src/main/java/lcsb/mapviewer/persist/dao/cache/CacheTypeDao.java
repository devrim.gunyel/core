package lcsb.mapviewer.persist.dao.cache;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class CacheTypeDao extends BaseDao<CacheType> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(CacheTypeDao.class);

  /**
   * Default constructor.
   */
  public CacheTypeDao() {
    super(CacheType.class);
  }

  /**
   * Returns the cach type for the class name identified by a canonical name.
   * 
   * @param className
   *          canonical name of the class
   * @return type of cache that should be used by this cachable interface
   */
  public CacheType getByClassName(String className) {
    List<?> list = getElementsByParameter("className", className);
    if (list.size() == 0) {
      return null;
    }
    return (CacheType) list.get(0);
  }

}
