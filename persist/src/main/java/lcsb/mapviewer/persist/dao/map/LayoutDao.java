package lcsb.mapviewer.persist.dao.map;

import java.util.List;

import javax.persistence.criteria.*;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class LayoutDao extends BaseDao<Layout> {

  public LayoutDao() {
    super(Layout.class);
  }

  public List<Layout> getLayoutsByProject(Project project) {
    List<Layout> layouts = getElementsByParameter("project_id", project.getId());
    for (Layout layout : layouts) {
      refresh(layout);
    }
    return layouts;
  }

  public Layout getLayoutByName(Project project, String name) {
    List<Layout> layouts = getElementsByParameter("project_id", project.getId());
    for (Layout layout : layouts) {
      refresh(layout);
      if (layout.getTitle().equals(name)) {
        return layout;
      }
    }
    return null;
  }
}
