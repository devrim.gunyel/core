package lcsb.mapviewer.persist.dao.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class UploadedFileEntryDao extends BaseDao<UploadedFileEntry> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger(UploadedFileEntryDao.class);

  /**
   * Default constructor.
   */
  public UploadedFileEntryDao() {
    super(UploadedFileEntry.class, "removed");
  }

  @Override
  public void delete(UploadedFileEntry entry) {
    entry.setRemoved(true);
    update(entry);
  }

}
