package lcsb.mapviewer.persist.dao.map;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link Model} class.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class ModelDao extends BaseDao<ModelData> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(ModelDao.class);

  /**
   * Default constructor.
   */
  public ModelDao() {
    super(ModelData.class);
  }

  @Override
  public void delete(ModelData model) {
    if (model.getProject() != null) {
      model.getProject().removeModel(model);
    }
    model.setProject(null);
    super.delete(model);
  }

  /**
   * Return the latest model for the project with a given project identifier.
   * 
   * @param projectId
   *          identifier of the project
   * @param lazyLoad
   *          if <code>false</code>, the whole model (and all substructures)
   *          should be loaded at once, if <code>true</code> then some elements
   *          will be loaded on demand by hibernate proxy
   * @return the latest model for the project with a given name
   */
  public ModelData getLastModelForProjectIdentifier(String projectId, boolean lazyLoad) {
    @SuppressWarnings("unchecked")
    List<ModelData> list = getSession()
        .createQuery(
            "select model_t from " + getClazz().getSimpleName()
                + " model_t join model_t.project project_t where project_t.projectId = :project_id order by model_t.id desc")
        .setParameter("project_id", projectId).setMaxResults(1).list();
    if (list.size() > 0) {
      return list.get(0);
    } else {
      return null;
    }
  }

  /**
   * Adds model data to the database.
   * 
   * @param model
   *          object containing model data to add to database
   */
  public void add(Model model) {
    add(model.getModelData());
  }

  /**
   * Removes model data from the database.
   * 
   * @param model
   *          object containing model data
   */
  public void delete(Model model) {
    delete(model.getModelData());
  }

  /**
   * "Disconnects" model data from database. From this point on we cannot
   * update/delete it in the database.
   * 
   * @param model
   *          model containing model data to disconnect
   */
  public void evict(Model model) {
    evict(model.getModelData());
  }

  /**
   * Removes model data int the database.
   * 
   * @param model
   *          object containing model data
   */
  public void update(Model model) {
    update(model.getModelData());
  }
}
