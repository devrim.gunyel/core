package lcsb.mapviewer.persist.dao;

import java.math.BigInteger;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.dialect.Dialect;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Data access object class for Project objects.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class ProjectDao extends BaseDao<Project> {
  Logger logger = LogManager.getLogger(ProjectDao.class);

  /**
   * Default constructor.
   */
  public ProjectDao() {
    super(Project.class);
  }

  /**
   * Returns project with the given {@link Project#projectId project identifier} .
   * 
   * @param projectId
   *          {@link Project#projectId project identifier}
   * @return project for the given name
   */
  public Project getProjectByProjectId(String projectId) {
    Project project = getByParameter("projectId", projectId);

    return project;
  }

  /**
   * Returns project that contains model with a given id.
   * 
   * @param id
   *          id of the model
   * @return project that contains model with a given id
   */
  public Project getProjectForModelId(Integer id) {
    List<?> list = getSession()
        .createQuery("select project from " + ModelData.class.getSimpleName() + " model_t where model_t.id = :id")
        .setParameter("id", id).list();
    if (list.size() == 0) {
      return null;
    }
    return (Project) list.get(0);
  }

  /**
   * Returns information if the project with given name exists.
   * 
   * @param projectName
   *          name of the project
   * @return <code>true</code> if project with given name exists,
   *         <code>false</code> otherwise
   */
  public boolean isProjectExistsByName(String projectName) {
    @SuppressWarnings("unchecked")
    List<Long> list = getSession()
        .createQuery("select count(*) from " + getClazz().getSimpleName() + " where projectId = :projectId")
        .setParameter("projectId", projectName).list();
    return list.get(0) > 0;
  }

  public long getNextId() {
    Dialect dialect = getDialect();
    NativeQuery<?> sqlQuery = getSession().createNativeQuery(dialect.getSequenceNextValString("project_sequence"));

    return ((BigInteger) sqlQuery.getResultList().get(0)).longValue();

  }
}
