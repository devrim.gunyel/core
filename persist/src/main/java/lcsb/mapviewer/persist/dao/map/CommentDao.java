package lcsb.mapviewer.persist.dao.map;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for Comment objects.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class CommentDao extends BaseDao<Comment> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger(CommentDao.class);

  /**
   * Default constructor.
   */
  public CommentDao() {
    super(Comment.class);
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments are
   * limited by the parameters.
   * 
   * @param model
   *          in which model we want to find comments
   * @param pinned
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not visible on the map</li>
   *          <li>true - comments that are visible on the map</li>
   *          </ul>
   * @param deleted
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not deleted</li>
   *          <li>true - comments that are deleted</li>
   *          </ul>
   * @return list of comments that fulfill parameters constraints
   */
  public List<Comment> getCommentByModel(Model model, Boolean pinned, Boolean deleted) {
    return getCommentByModel(model.getId(), pinned, deleted);
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments are
   * limited by the parameters.
   * 
   * @param modelId
   *          in which model we want to find comments
   * @param pinned
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not visible on the map</li>
   *          <li>true - comments that are visible on the map</li>
   *          </ul>
   * @param deleted
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not deleted</li>
   *          <li>true - comments that are deleted</li>
   *          </ul>
   * @return list of comments that fulfill parameters constraints
   */
  private List<Comment> getCommentByModel(int modelId, Boolean pinned, Boolean deleted) {
    List<Comment> comments = getElementsByParameter("model_id", modelId);
    List<Comment> result = new ArrayList<Comment>();
    if (deleted != null) {
      for (Comment comment : comments) {
        if (comment.isDeleted() == deleted) {
          result.add(comment);
        }
      }
    } else {
      result = comments;
    }

    if (pinned == null) {
      return result;
    }
    comments = result;
    result = new ArrayList<Comment>();
    for (Comment comment : comments) {
      if (comment.isPinned() == pinned) {
        result.add(comment);
      }
    }
    return result;
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments are
   * limited by the parameters.
   * 
   * @param model
   *          in which model we want to find comments
   * @param pinned
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not visible on the map</li>
   *          <li>true - comments that are visible on the map</li>
   *          </ul>
   * @param deleted
   *          this parameter defines what kind of comments we want to get:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not deleted</li>
   *          <li>true - comments that are deleted</li>
   *          </ul>
   * @return list of comments that fulfill parameters constraints
   */
  public List<Comment> getCommentByModel(ModelData model, Boolean pinned, Boolean deleted) {
    return getCommentByModel(model.getId(), pinned, deleted);
  }

}
