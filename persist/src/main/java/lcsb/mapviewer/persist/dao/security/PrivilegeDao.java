package lcsb.mapviewer.persist.dao.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class PrivilegeDao extends BaseDao<Privilege> {

  public PrivilegeDao() {
    super(Privilege.class);
  }

  public Privilege getPrivilegeForTypeAndObjectId(PrivilegeType type, String objectId) {
    List<Privilege> privileges = getElementsByParameters(Arrays.asList(
        new Pair<>("type", type),
        new Pair<>("objectId", objectId)));
    if (privileges.size() > 1) {
      throw new IllegalStateException("Impossible DB state. Privileges are constrained to be unique.");
    } else if (privileges.size() == 1) {
      return privileges.get(0);
    } else {
      return new Privilege(type, objectId);
    }
  }

}
