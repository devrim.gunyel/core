package lcsb.mapviewer.persist.dao.graphics;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link PolylineDao} class.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class PolylineDao extends BaseDao<PolylineData> {

  /**
   * Default constructor.
   */
  public PolylineDao() {
    super(PolylineData.class);
  }

}
