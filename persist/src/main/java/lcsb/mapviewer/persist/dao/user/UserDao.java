package lcsb.mapviewer.persist.dao.user;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class UserDao extends BaseDao<User> {

  public UserDao() {
    super(User.class, "removed");
  }

  public User getUserByLogin(String login) {
    return getByParameter("login", login);
  }

  public User getUserByEmail(String email) {
    return getByParameter("email", email);
  }

  @Override
  public void delete(User object) {
    object.setRemoved(true);
    object.setLogin("[REMOVED]_" + object.getId() + "_" + object.getLogin());
    update(object);
  }
}
