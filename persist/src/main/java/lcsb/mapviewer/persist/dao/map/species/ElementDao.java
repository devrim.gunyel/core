package lcsb.mapviewer.persist.dao.map.species;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data Access Object class for Alias class.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class ElementDao extends BaseDao<Element> {

  /**
   * Default constructor.
   */
  public ElementDao() {
    super(Element.class);
  }

}
