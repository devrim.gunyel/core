package lcsb.mapviewer.persist.dao.plugin;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for {@link Plugin} objects.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class PluginDao extends BaseDao<Plugin> {
  /**
   * Default constructor.
   */
  public PluginDao() {
    super(Plugin.class);
  }

  public Plugin getByHash(String hash) {
    return getByParameter("hash", hash);
  }

}
