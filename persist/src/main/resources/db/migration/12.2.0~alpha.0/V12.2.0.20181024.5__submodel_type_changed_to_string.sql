-- change in layout
alter table submodel_connection_table add column type_string varchar(31);
update submodel_connection_table set type_string='DOWNSTREAM_TARGETS' where type=0;
update submodel_connection_table set type_string='PATHWAY' where type=1;
update submodel_connection_table set type_string='UNKNOWN' where type=2;

alter table submodel_connection_table drop column type;
alter table submodel_connection_table rename column type_string to type;
