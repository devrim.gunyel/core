alter table arrow_type_data add column arrow_type_string varchar(31);
update arrow_type_data set arrow_type_string='BLANK_CROSSBAR' where arrow_type=0;
update arrow_type_data set arrow_type_string='FULL_CROSSBAR' where arrow_type=1;
update arrow_type_data set arrow_type_string='DIAMOND' where arrow_type=2;
update arrow_type_data set arrow_type_string='BLANK' where arrow_type=3;
update arrow_type_data set arrow_type_string='CROSSBAR' where arrow_type=4;
update arrow_type_data set arrow_type_string='CIRCLE' where arrow_type=5;
update arrow_type_data set arrow_type_string='OPEN' where arrow_type=6;
update arrow_type_data set arrow_type_string='FULL' where arrow_type=7;
update arrow_type_data set arrow_type_string='NONE' where arrow_type=8;

alter table arrow_type_data drop column arrow_type;
alter table arrow_type_data rename column arrow_type_string to arrow_type;