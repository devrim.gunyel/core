alter table element_table add column substance_unit_comlex_type_id integer;
alter table element_table add foreign key (substance_unit_comlex_type_id) references sbml_unit_table(id);
alter table element_table rename column substance_units to substance_unit_raw_type;
