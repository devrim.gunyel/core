-- issue #461 - duplicates in reaction ids resolved
update reaction_table r set  idreaction=concat(r.idreaction,'_',r.iddb) from (select idreaction, model_iddb from reaction_table group by idreaction, model_iddb having count(*)>1) t where r.idreaction=t.idreaction and r.model_iddb=t.model_iddb;
