-- migrate info about abbreviation
ALTER TABLE alias_table ADD COLUMN abbreviation character varying(255);
update alias_table set abbreviation = (select species_table.abbreviation from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column abbreviation;

-- migrate info about formula
ALTER TABLE alias_table ADD COLUMN formula character varying(255);
update alias_table set formula = (select species_table.formula from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column formula;

-- migrate info about full name
ALTER TABLE alias_table ADD COLUMN fullname character varying(255);
update alias_table set fullname = (select species_table.fullname from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column fullname;

-- name
update alias_table set name = (select species_table.name from species_table where alias_table.idelementdb =  species_table.iddb) where name is null or name ='';

-- notes
ALTER TABLE alias_table ADD COLUMN notes text;
update alias_table set notes = (select species_table.notes from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column notes;

-- symbol
ALTER TABLE alias_table ADD COLUMN symbol character varying(255);
update alias_table set symbol = (select species_table.symbol from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column symbol;

-- charge
ALTER TABLE alias_table ADD COLUMN charge integer;
update alias_table set charge = (select species_table.charge from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column charge;

-- homodimer
ALTER TABLE alias_table ADD COLUMN homodimer integer;
update alias_table set homodimer = (select species_table.homodimer from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column homodimer;

-- hypothetical
ALTER TABLE alias_table ADD COLUMN hypothetical boolean;
update alias_table set hypothetical = (select species_table.hypothetical from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column hypothetical;

-- initial amount
ALTER TABLE alias_table ADD COLUMN initialamount integer;
update alias_table set initialamount = (select species_table.initialamount from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column initialamount;

-- initial concentration
ALTER TABLE alias_table ADD COLUMN initialconcentration integer;
update alias_table set initialconcentration = (select species_table.initialconcentration from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column initialconcentration;

--only substance unit
ALTER TABLE alias_table ADD COLUMN onlysubstanceunits boolean;
update alias_table set onlysubstanceunits = (select species_table.onlysubstanceunits from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column onlysubstanceunits;

-- position to compartment
ALTER TABLE alias_table ADD COLUMN positiontocompartment character varying(255);
update alias_table set positiontocompartment = (select species_table.positiontocompartment from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column positiontocompartment;

-- inchi
ALTER TABLE alias_table ADD COLUMN inchi text;
update alias_table set inchi = (select species_table.inchi from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column inchi;

-- inchi key
ALTER TABLE alias_table ADD COLUMN inchikey character varying(255);
update alias_table set inchikey = (select species_table.inchikey from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column inchikey;

-- smiles
ALTER TABLE alias_table ADD COLUMN smiles text;
update alias_table set smiles = (select species_table.smiles from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column smiles;

-- structural state
ALTER TABLE alias_table ADD COLUMN structuralstate character varying(255);
update alias_table set structuralstate = (select species_table.structuralstate from species_table where alias_table.idelementdb =  species_table.iddb);
alter table species_table drop column structuralstate;
