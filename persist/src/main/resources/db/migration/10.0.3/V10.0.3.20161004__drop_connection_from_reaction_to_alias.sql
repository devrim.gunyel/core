-- drop old species implementation

alter table node_table drop column element_iddb;
alter table alias_table drop column idelementdb;
drop table species_table;

alter table alias_table rename column aliasid to elementid;
