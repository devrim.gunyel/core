-- change alias types for new aliases implementation 
update alias_table set alias_type_db = 'ANTISENSE_RNA_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Antisense RNA');

--chemical_alias (should never happen)
update alias_table set alias_type_db = 'CHEMICAL_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Chemical');

update alias_table set alias_type_db = 'ION_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Ion');

update alias_table set alias_type_db = 'SIMPLE_MOLECULE_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='SimpleMolecule');

-- complex_alias (nothing should change)
update alias_table set alias_type_db = 'Complex Species Alias' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='COMPLEX_SPECIES');

update alias_table set alias_type_db = 'DEGRADED_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Degraded');

update alias_table set alias_type_db = 'DRUG_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Drug');

update alias_table set alias_type_db = 'GENE_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Gene');

update alias_table set alias_type_db = 'PHENTOYPE_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Phenotype');

-- protein_alias (this should never happen)
update alias_table set alias_type_db = 'PROTEIN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Protein');

update alias_table set alias_type_db = 'GENERIC_PROTEIN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='GENERIC_PROTEIN');

update alias_table set alias_type_db = 'ION_CHANNEL_PROTEIN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='ION_CHANNEL_PROTEIN');

update alias_table set alias_type_db = 'RECEPTOR_PROTEIN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='RECEPTOR_PROTEIN');

update alias_table set alias_type_db = 'TRUNCATED_PROTEIN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='TRUNCATED_PROTEIN');

update alias_table set alias_type_db = 'RNA_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='RNA');

update alias_table set alias_type_db = 'UNKNOWN_ALIAS' where iddb in (select alias_table.iddb from alias_table , species_table t where alias_table.idelementdb=t.iddb and t.species_type_db ='Unknown');

-- for artifitial compartment alias we have name in the title column so rename it (later on we will add names for other elements)
ALTER TABLE "alias_table" RENAME COLUMN "title" TO "name";
