CREATE SEQUENCE uniprot_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- 1:N mapping between element and its uniprot mappings, each of which is then used to map (PDB) structures to it
CREATE TABLE uniprot_table
(
  iddb integer NOT NULL DEFAULT nextval('uniprot_table_iddb_seq'::regclass),
  element_id integer NOT NULL,  
  uniprot_id character varying(255) NOT NULL,
  CONSTRAINT uniprot_pkey PRIMARY KEY (iddb),
  CONSTRAINT uniprot_element_fk FOREIGN KEY (element_id)
      REFERENCES public.element_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE SEQUENCE structure_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

/*
based on https://www.ebi.ac.uk/pdbe/api/doc/sifts.html best_structures API

		pdb_id: the PDB ID which maps to the UniProt ID
                chain_id: the specific chain of the PDB which maps to the UniProt ID
                coverage: the percent coverage of the entire UniProt sequence
                resolution: the resolution of the structure
                start: the structure residue number which maps to the start of the mapped sequence
                end: the structure residue number which maps to the end of the mapped sequence
                unp_start: the sequence residue number which maps to the structure start
                unp_end: the sequence residue number which maps to the structure end
                experimental_method: type of experiment used to determine structure
                tax_id: taxonomic ID of the protein's original organism
*/
CREATE TABLE structure_table
(
  iddb integer NOT NULL DEFAULT nextval('structure_table_iddb_seq'::regclass),
  uniprot_id integer NOT NULL,
  pdb_id character varying(255) NOT NULL, --should be character(4), but to be on the safe side...
  chain_id character varying(255) NOT NULL, --should be char(1), but for example 5T0C has has chains AK and BK
  struct_start integer,
  struct_end integer,
  unp_start integer,
  unp_end integer,  
  experimental_method character varying(255),
  coverage double precision,
  resolution double precision,
  tax_id integer,
  CONSTRAINT structure_pkey PRIMARY KEY (iddb),
  CONSTRAINT structure_uniprot_fk FOREIGN KEY (uniprot_id)
      REFERENCES public.uniprot_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);


