alter table element_table drop column if exists boundarycondition;
alter table element_table add boundarycondition boolean;

alter table element_table drop column if exists constant;
alter table element_table add constant boolean;

alter table element_table drop column if exists substanceunits;
alter table element_table add substanceunits varchar(255);

alter table reaction_table drop column if exists kineticlaw;

alter table reaction_table drop column if exists kinetics_iddb;
alter table reaction_table add kinetics_iddb integer;

drop table if exists kinetic_law_parameters;
drop table if exists kinetic_law_functions;
drop table if exists kinetic_law_elements;
drop table if exists sbml_function_arguments;
drop table if exists sbml_unit_factor;
DROP SEQUENCE if exists sbml_unit_factor_iddb_seq;
drop table if exists sbml_kinetics;
DROP SEQUENCE if exists sbml_kinetics_iddb_seq;
drop table if exists sbml_function;
DROP SEQUENCE if exists sbml_function_iddb_seq;
drop table if exists model_parameters;
drop table if exists sbml_parameter;
DROP SEQUENCE if exists sbml_parameter_iddb_seq;
drop table if exists sbml_unit;
DROP SEQUENCE if exists sbml_unit_iddb_seq;

--definition of sbml unit

CREATE SEQUENCE sbml_unit_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE sbml_unit
(
  iddb integer NOT NULL DEFAULT nextval('sbml_unit_iddb_seq'::regclass),
  unitid  varchar(255),  
  name  varchar(255),
  model_iddb integer NOT NULL,
  
  CONSTRAINT sbml_unit_pk PRIMARY KEY (iddb),
  CONSTRAINT sbml_unit_model_fk FOREIGN KEY (model_iddb)
      REFERENCES public.model_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

--definition of sbml parameter
CREATE SEQUENCE sbml_parameter_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE sbml_parameter
(
  iddb integer NOT NULL DEFAULT nextval('sbml_parameter_iddb_seq'::regclass),
  parameterid  varchar(255),  
  name  varchar(255),
	value  numeric,
	units_iddb integer,
  
  CONSTRAINT sbml_parameter_pk PRIMARY KEY (iddb),
  CONSTRAINT sbml_parameter_units_fk FOREIGN KEY (units_iddb)
      REFERENCES public.sbml_unit (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);


--definition of many to many relation between model and sbml parameter
CREATE TABLE model_parameters
(
  model_id integer NOT NULL,
  parameter_id integer NOT NULL,  
  CONSTRAINT model_parameters_unique UNIQUE (model_id, parameter_id),
  CONSTRAINT model_parameters_model_fk FOREIGN KEY (model_id)
      REFERENCES public.model_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT model_parameters_parameter_fk FOREIGN KEY (parameter_id)
      REFERENCES public.sbml_parameter (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

--definition of sbml function
CREATE SEQUENCE sbml_function_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE sbml_function
(
  iddb integer NOT NULL DEFAULT nextval('sbml_function_iddb_seq'::regclass),
  functionid  varchar(255),  
  name  varchar(255),
  definition  text,
  model_iddb integer NOT NULL,
  
  CONSTRAINT sbml_function_pk PRIMARY KEY (iddb),
  CONSTRAINT sbml_function_model_fk FOREIGN KEY (model_iddb)
      REFERENCES public.model_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

--definition of sbml kinetics
CREATE SEQUENCE sbml_kinetics_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE sbml_kinetics
(
  iddb integer NOT NULL DEFAULT nextval('sbml_function_iddb_seq'::regclass),
  definition  text,
  
  CONSTRAINT sbml_kinetics_pk PRIMARY KEY (iddb)
);

ALTER TABLE reaction_table ADD FOREIGN KEY (kinetics_iddb) REFERENCES sbml_kinetics(iddb);


--definition of sbml kinetics
CREATE SEQUENCE sbml_unit_factor_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE sbml_unit_factor
(
  iddb integer NOT NULL DEFAULT nextval('sbml_unit_factor_iddb_seq'::regclass),
  unit_iddb  integer not null,
  exponent  integer not null default 1,
  scale  integer not null default 0,
  multiplier  numeric not null default 1.0,
  unittype varchar(255), 
  
  CONSTRAINT sbml_unit_factor_pk PRIMARY KEY (iddb),
  CONSTRAINT sbml_unit_factor_unit_fk FOREIGN KEY (unit_iddb)
      REFERENCES public.sbml_unit (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
  
);

CREATE TABLE sbml_function_arguments
(
  idx integer NOT NULL,
  sbml_function_iddb integer not null,
  argument_name varchar(255), 
  
  CONSTRAINT sbml_function_arguments_sbml_function_fk FOREIGN KEY (sbml_function_iddb)
      REFERENCES public.sbml_function (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
  
);

CREATE TABLE kinetic_law_parameters
(
  kinetic_law_id integer NOT NULL,
  parameter_id integer not null,
  
  CONSTRAINT kinetic_law_parameters_unique UNIQUE (kinetic_law_id, parameter_id),
  CONSTRAINT kinetic_law_parameters_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id)
      REFERENCES public.sbml_kinetics (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT kinetic_law_parameters_parameter_fk FOREIGN KEY (parameter_id)
      REFERENCES public.sbml_parameter (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE kinetic_law_functions
(
  kinetic_law_id integer NOT NULL,
  function_id integer not null,
  
  CONSTRAINT kinetic_law_functions_unique UNIQUE (kinetic_law_id, function_id),
  CONSTRAINT kinetic_law_functions_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id)
      REFERENCES public.sbml_kinetics (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT kinetic_law_functions_function_fk FOREIGN KEY (function_id)
      REFERENCES public.sbml_function (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE kinetic_law_elements
(
  kinetic_law_id integer NOT NULL,
  element_id integer not null,
  
  CONSTRAINT kinetic_law_elements_unique UNIQUE (kinetic_law_id, element_id),
  CONSTRAINT kinetic_law_elements_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id)
      REFERENCES public.sbml_kinetics (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT kinetic_law_elements_element_fk FOREIGN KEY (element_id)
      REFERENCES public.element_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

