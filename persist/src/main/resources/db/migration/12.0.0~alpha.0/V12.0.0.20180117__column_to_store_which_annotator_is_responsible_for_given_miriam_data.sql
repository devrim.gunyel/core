-- column to store which annotator is responsible for given miriam data
alter table miriam_data_table add column annotator varchar(256) default null;
