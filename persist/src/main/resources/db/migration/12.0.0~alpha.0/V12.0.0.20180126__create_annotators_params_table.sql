CREATE SEQUENCE annotators_params_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE annotators_params_table
(
  iddb integer NOT NULL DEFAULT nextval('annotators_params_table_iddb_seq'::regclass),
  annotationschema_iddb integer,
  annotator_classname character varying(255) NOT NULL,  
  name character varying(255) NOT NULL,
  value character varying(255) NOT NULL,
  CONSTRAINT annotators_params_pkey PRIMARY KEY (iddb),
  CONSTRAINT annotators_params_user_annotation_schema_fk FOREIGN KEY (annotationschema_iddb)
      REFERENCES user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);