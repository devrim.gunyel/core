-- semantic zooming
alter table element_table add column zoomlevelvisibility character varying default null;
alter table reaction_table add column zoomlevelvisibility character varying default null;
