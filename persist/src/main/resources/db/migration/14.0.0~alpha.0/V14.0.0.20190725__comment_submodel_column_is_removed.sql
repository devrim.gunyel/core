--remove submodel_id column 
update comment_table set model_id = submodel_id where submodel_id is not null;
ALTER TABLE comment_table drop COLUMN submodel_id;

--comment must be attached to a model
delete from comment_table where model_id is null;
ALTER TABLE comment_table ALTER COLUMN model_id SET NOT NULL;

-- pinned should be not null
update comment_table set pinned = false where pinned is null;
ALTER TABLE comment_table ALTER COLUMN pinned SET NOT NULL;
