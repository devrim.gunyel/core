-- create temporary points for creating center line
alter table polyline_data_table add column first_point varchar;
alter table polyline_data_table add column second_point varchar;
alter table polyline_data_table add column one_before_last_point varchar;
alter table polyline_data_table add column last_point varchar;
update polyline_data_table set first_point = (select point_val from point_table where point_table.id = polyline_data_table.id and idx =0);
update polyline_data_table set second_point = (select point_val from point_table where point_table.id = polyline_data_table.id and idx =1);
update polyline_data_table set one_before_last_point = (select point_val from (select * from point_table where point_table.id = polyline_data_table.id order by idx desc limit 2) xxx order by idx limit 1);
update polyline_data_table set last_point = (select point_val from point_table where point_table.id = polyline_data_table.id order by idx desc limit 1);

alter table reaction_table add column start_point varchar;
alter table reaction_table add column end_point varchar;

alter table reaction_table add column reactant integer;
alter table reaction_table add column product integer;
alter table reaction_table add column input_operator integer;
alter table reaction_table add column output_operator integer;

--old version of computing first reactant, product, input and output operators (this version is ~100 slower than new one). This is an issue for big maps. For db with 30k reactions it toook 25minutes
--
--update reaction_table set reactant = (select id from reaction_node_table where node_type_db='REACTANT_NODE' and reaction_id=reaction_table.id order by id limit 1);
--update reaction_table set product = (select id from reaction_node_table where node_type_db='PRODUCT_NODE' and reaction_id=reaction_table.id order by id limit 1);
--update reaction_table set input_operator = (select id from reaction_node_table where node_type_db in ('AND_OPERATOR_NODE', 'ASSOCIATION_OPERATOR_NODE', 'NAND_OPERATOR_NODE', 'OR_OPERATOR_NODE', 'UNKNOWN_OPERATOR_NODE') and reaction_id=reaction_table.id order by id limit 1);
--update reaction_table set output_operator = (select id from reaction_node_table where node_type_db in ('DISSOCIATION_OPERATOR_NODE', 'SPLIT_OPERATOR_NODE', 'TRUNCATION_OPERATOR_NODE') and reaction_id=reaction_table.id order by id limit 1);

create table tmp_node (
  node_id integer,
  node_type_db varchar,
  reaction_id integer,
	CONSTRAINT tmp_fkey FOREIGN KEY (reaction_id)
      REFERENCES reaction_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
insert into tmp_node select min(id), node_type_db, reaction_id from reaction_node_table group by node_type_db, reaction_id;

insert into tmp_node select min(id), 'INPUT_NODE', reaction_id from reaction_node_table where node_type_db in ('AND_OPERATOR_NODE', 'ASSOCIATION_OPERATOR_NODE', 'NAND_OPERATOR_NODE', 'OR_OPERATOR_NODE', 'UNKNOWN_OPERATOR_NODE') group by  reaction_id;
insert into tmp_node select min(id), 'OUTPUT_NODE', reaction_id from reaction_node_table where node_type_db in ('DISSOCIATION_OPERATOR_NODE', 'SPLIT_OPERATOR_NODE', 'TRUNCATION_OPERATOR_NODE') group by  reaction_id;

update reaction_table set reactant = tmp_node.node_id from tmp_node where tmp_node.reaction_id=reaction_table.id and tmp_node.node_type_db = 'REACTANT_NODE';
update reaction_table set product = tmp_node.node_id from tmp_node where tmp_node.reaction_id=reaction_table.id and tmp_node.node_type_db = 'PRODUCT_NODE';
update reaction_table set input_operator = tmp_node.node_id from tmp_node where tmp_node.reaction_id=reaction_table.id and tmp_node.node_type_db = 'INPUT_NODE';
update reaction_table set output_operator = tmp_node.node_id from tmp_node where tmp_node.reaction_id=reaction_table.id and tmp_node.node_type_db = 'OUTPUT_NODE';

drop table tmp_node;

update reaction_table set start_point = (select one_before_last_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.reactant and polyline_data_table.id = line_id);
update reaction_table set end_point = (select second_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.product and polyline_data_table.id = line_id);

update reaction_table set start_point = (select one_before_last_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.input_operator and polyline_data_table.id = line_id) where reaction_type_db in ('BOOLEAN_LOGIC_GATE', 'HETERODIMER_REACTION');
update reaction_table set end_point = (select one_before_last_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.output_operator and polyline_data_table.id = line_id) where reaction_type_db in ('DISSOCIATION_REACTION', 'TRUNCATION_REACTION_REACTION');

update reaction_table set start_point = (select last_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.reactant and polyline_data_table.id = line_id) where reaction_type_db in ('NEGATIVE_INFLUENCE_REACTION', 'POSITIVE_INFLUENCE_REACTION', 'REDUCED_MODULATION_REACTION', 'REDUCED_PHYSICAL_STIMULATION', 'REDUCED_TRIGGER_REACTION', 'UNKNOWN_NEGATIVE_INFLUENCE', 'UNKNOWN_POSITIVE_INFLUENCE', 'U_REDUCED_MODULATION', 'U_REDUCED_PHYSICAL_STIM', 'U_REDUCED_TRIGGER');
update reaction_table set end_point = (select first_point from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.product and polyline_data_table.id = line_id) where reaction_type_db in ('NEGATIVE_INFLUENCE_REACTION', 'POSITIVE_INFLUENCE_REACTION', 'REDUCED_MODULATION_REACTION', 'REDUCED_PHYSICAL_STIMULATION', 'REDUCED_TRIGGER_REACTION', 'UNKNOWN_NEGATIVE_INFLUENCE', 'UNKNOWN_POSITIVE_INFLUENCE', 'U_REDUCED_MODULATION', 'U_REDUCED_PHYSICAL_STIM', 'U_REDUCED_TRIGGER');

alter table reaction_table add column color bytea;
alter table reaction_table add column line_type varchar;
alter table reaction_table add column line_width double precision;

alter table reaction_table add column line_id integer;

update reaction_table set color = (select color from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.product and polyline_data_table.id = line_id);
update reaction_table set line_type = (select type from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.product and polyline_data_table.id = line_id);
update reaction_table set line_width = (select width from reaction_node_table, polyline_data_table where reaction_node_table.id = reaction_table.product and polyline_data_table.id = line_id);

DO $$ 
DECLARE 
	reaction 	RECORD;
	last_id 	INT;
BEGIN
  FOR reaction IN SELECT * FROM reaction_table LOOP
	  insert into polyline_data_table (color, width, type, begin_atd, end_atd) values
	    (reaction.color, reaction.line_width, reaction.line_type, '2.74889357189106898;15;NONE;SOLID', '2.74889357189106898;15;NONE;SOLID') returning id into last_id;
    insert into point_table (id, point_val, idx) values
      (last_id, reaction.start_point, 0);
    insert into point_table (id, point_val, idx) values
      (last_id, reaction.end_point, 1);
    update reaction_table set line_id = last_id where id = reaction.id;
  END LOOP;
END $$;

alter table reaction_table drop column color;
alter table reaction_table drop column line_type;
alter table reaction_table drop column line_width;
alter table reaction_table drop column start_point;
alter table reaction_table drop column end_point;

alter table reaction_table drop column reactant;
alter table reaction_table drop column product;
alter table reaction_table drop column input_operator;
alter table reaction_table drop column output_operator;

alter table polyline_data_table drop column first_point;
alter table polyline_data_table drop column second_point;
alter table polyline_data_table drop column one_before_last_point;
alter table polyline_data_table drop column last_point;

alter table reaction_table alter column line_id set not null;
ALTER TABLE reaction_table ADD CONSTRAINT fk_reaction_line foreign key (line_id) references polyline_data_table (id);