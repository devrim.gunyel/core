update model_data_table set width = 800 where width is null;
update model_data_table set height = 600 where height is null;

ALTER TABLE model_data_table ALTER COLUMN width SET NOT NULL;
ALTER TABLE model_data_table ALTER COLUMN height SET NOT NULL;
