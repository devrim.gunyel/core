update configuration_option_table set type ='DEFAULT_READ_PROJECT' where type = 'DEFAULT_VIEW_PROJECT';
update configuration_option_table set type ='DEFAULT_WRITE_PROJECT' where type = 'DEFAULT_USER_PRIVILEGES';
update configuration_option_table set type ='DEFAULT_CAN_CREATE_OVERLAYS' where type = 'DEFAULT_CUSTOM_LAYOUTS';
update configuration_option_table set value = 'true' where type ='DEFAULT_CAN_CREATE_OVERLAYS' and value<>'0';
update configuration_option_table set value = 'false' where type ='DEFAULT_CAN_CREATE_OVERLAYS' and value='0';

