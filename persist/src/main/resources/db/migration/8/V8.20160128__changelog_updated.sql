insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (851, 'Data sets (layouts) visualized dynamically in the browser', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (851, 'Micro RNA database', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='stephan.gebel@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (851, 'Chemical database', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='stephan.gebel@uni.lu'));
  