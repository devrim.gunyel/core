INSERT INTO cache_type(iddb, validity, classname)
VALUES (16, 28, 'lcsb.mapviewer.annotation.services.ChemicalParser');
INSERT INTO cache_type(iddb, validity, classname)
VALUES (17, 28, 'lcsb.mapviewer.annotation.services.MeSHParser');
INSERT INTO cache_type(iddb, validity, classname)
VALUES (18, 28, 'lcsb.mapviewer.annotation.services.MiRNAParser');

AlTER TABLE project_table 
ADD column disease_iddb integer;
ALTER TABLE project_table
  ADD CONSTRAINT project_table_miriam FOREIGN KEY (disease_iddb)
      REFERENCES miriam_data_table (iddb);
UPDATE cache_type
SET  classname= 'lcsb.mapviewer.annotation.services.PubmedParser'
where iddb = 3;
