-- just in case if any point on the map has null coordinates (it should never happen)
update point_table set point_val = '0.0,0.0' where point_val = null;
alter table point_table alter column point_val set not null;