-- antisense rna uses string type

alter table antisense_rna_region_table add column type_string varchar(255);
update antisense_rna_region_table set type_string ='MODIFICATION_SITE' where type =0;
update antisense_rna_region_table set type_string ='CODING_REGION' where type =1;
update antisense_rna_region_table set type_string ='PROTEIN_BINDING_DOMAIN' where type =2;
alter table antisense_rna_region_table drop column type;
alter table antisense_rna_region_table rename COLUMN type_string to type;

-- antisense region doesn't have state
alter table antisense_rna_region_table drop column state;

--size is changed to width for antisense rna region
alter table antisense_rna_region_table add column width numeric(6,2);
update antisense_rna_region_table set width=element_table.width*antisense_rna_region_table.size from  element_table where element_table.iddb = antisense_rna_region_table.idspeciesdb and not size is null;
alter table antisense_rna_region_table drop column size;

-- unused CellDesigner 'side' parameter removed
alter table modification_residue_table drop column side;

-- unused CellDesigner 'size' parameter removed
alter table modification_residue_table drop column size;

-- changing modification residue angle into x,y coordinates
update modification_residue_table set angle=0 where angle>=2*pi();
alter table modification_residue_table add column position varchar(255);
--GENE
update modification_residue_table set position=concat(element_table.x+modification_residue_table.angle/(2*pi())*element_table.width,',',element_table.y) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENE_ALIAS';

--GENERIC PROTEIN
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+element_table.height/2-modification_residue_table.angle/(pi()/4)*element_table.height/2) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENERIC_PROTEIN_ALIAS' and angle<=pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width-(modification_residue_table.angle-pi()/4)/(pi()/2)*element_table.width,',',element_table.y) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENERIC_PROTEIN_ALIAS' and angle>pi()/4 and angle<=3*pi()/4;
update modification_residue_table set position=concat(element_table.x,',',element_table.y+((modification_residue_table.angle-3*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENERIC_PROTEIN_ALIAS' and angle>3*pi()/4 and angle<=5*pi()/4;
update modification_residue_table set position=concat(element_table.x+((modification_residue_table.angle-5*pi()/4)/(pi()/2)*element_table.width),',',element_table.y+element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENERIC_PROTEIN_ALIAS' and angle>5*pi()/4 and angle<=7*pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+element_table.height-((modification_residue_table.angle-7*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='GENERIC_PROTEIN_ALIAS' and angle>7*pi()/4;
--ION CHANNEL (it's the same as in generic protein - bug in old version)
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+element_table.height/2-modification_residue_table.angle/(pi()/4)*element_table.height/2) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='ION_CHANNEL_PROTEIN_ALIAS' and angle<=pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width-(modification_residue_table.angle-pi()/4)/(pi()/2)*element_table.width,',',element_table.y) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='ION_CHANNEL_PROTEIN_ALIAS' and angle>pi()/4 and angle<=3*pi()/4;
update modification_residue_table set position=concat(element_table.x,',',element_table.y+((modification_residue_table.angle-3*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='ION_CHANNEL_PROTEIN_ALIAS' and angle>3*pi()/4 and angle<=5*pi()/4;
update modification_residue_table set position=concat(element_table.x+((modification_residue_table.angle-5*pi()/4)/(pi()/2)*element_table.width),',',element_table.y+element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='ION_CHANNEL_PROTEIN_ALIAS' and angle>5*pi()/4 and angle<=7*pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+element_table.height-((modification_residue_table.angle-7*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='ION_CHANNEL_PROTEIN_ALIAS' and angle>7*pi()/4;
--RECEPTOR PROTEIN
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+(1-modification_residue_table.angle/(pi()/4))*2.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle<=pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width-(modification_residue_table.angle-pi()/4)/(pi()/4)*element_table.width/2,',',element_table.y+(modification_residue_table.angle-pi()/4)/(pi()/4)*1.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>pi()/4 and angle<=2*pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width/2-(modification_residue_table.angle-pi()/2)/(pi()/4)*element_table.width/2,',',element_table.y+1.0/5.0*element_table.height-(modification_residue_table.angle-pi()/2)/(pi()/4)*1.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>2*pi()/4 and angle<=3*pi()/4;
update modification_residue_table set position=concat(element_table.x,',',element_table.y+((modification_residue_table.angle-3*pi()/4)/(pi()/2)*4.0/5.0*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>3*pi()/4 and angle<=5*pi()/4;
update modification_residue_table set position=concat(element_table.x+(modification_residue_table.angle-5*pi()/4)/(pi()/4)*element_table.width/2,',',element_table.y+4.0/5.0*element_table.height+(modification_residue_table.angle-5*pi()/4)/(pi()/4)*1.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>5*pi()/4 and angle<=6*pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width/2+(modification_residue_table.angle-6*pi()/4)/(pi()/4)*element_table.width/2,',',element_table.y+element_table.height-(modification_residue_table.angle-6*pi()/4)/(pi()/4)*1.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>6*pi()/4 and angle<=7*pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+4.0/5.0*element_table.height-(modification_residue_table.angle-7*pi()/4)/(pi()/4)*2.0/5.0*element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='RECEPTOR_PROTEIN_ALIAS' and angle>7*pi()/4;
--TRUNCATED PROTEIN ALIAS
update modification_residue_table set position=concat(element_table.x+element_table.width,',',element_table.y+element_table.height/2-modification_residue_table.angle/(pi()/4)*element_table.height/2) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='TRUNCATED_PROTEIN_ALIAS' and angle<=pi()/4;
update modification_residue_table set position=concat(element_table.x+element_table.width-(modification_residue_table.angle-pi()/4)/(pi()/2)*element_table.width,',',element_table.y) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='TRUNCATED_PROTEIN_ALIAS' and angle>pi()/4 and angle<=3*pi()/4;
update modification_residue_table set position=concat(element_table.x,',',element_table.y+((modification_residue_table.angle-3*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='TRUNCATED_PROTEIN_ALIAS' and angle>3*pi()/4 and angle<=5*pi()/4;
update modification_residue_table set position=concat(element_table.x+((modification_residue_table.angle-5*pi()/4)/(pi()/2)*4.0/5.0*element_table.width),',',element_table.y+element_table.height) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='TRUNCATED_PROTEIN_ALIAS' and angle>5*pi()/4 and angle<=7*pi()/4;
update modification_residue_table set position=concat(element_table.x+4.0/5.0*element_table.width,',',element_table.y+element_table.height-((modification_residue_table.angle-7*pi()/4)/(pi()/2)*element_table.height)) from element_table where element_table.iddb = modification_residue_table.idspeciesdb and element_type_db='TRUNCATED_PROTEIN_ALIAS' and angle>7*pi()/4;
