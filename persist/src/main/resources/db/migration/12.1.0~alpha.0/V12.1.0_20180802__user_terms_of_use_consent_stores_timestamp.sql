-- user account stores info about timestamp of terms of service acceptance 
create table user_terms_of_use_consent (                                                                                            
	user_iddb integer not null,
	date timestamp without time zone,constraint user_terms_of_use_consent_user_fk foreign key (user_iddb)
	references public.user_table (iddb) match simple
	on update cascade on delete cascade
);

-- former acceptance is marked as from today
insert into user_terms_of_use_consent (date, user_iddb)  (select now(), iddb from user_table where terms_of_use_consent = true);
