alter table project_table add column owner_id integer;

-- match by email
update project_table set owner_id = (select id from user_table where email = notify_email and email<>'' limit 1) where owner_id is null;

-- assign admin owner to project without owner
update project_table set owner_id = (select id from user_table where id in (select user_id from user_privilege_map_table where privilege_id in (select id from privilege_table where type ='IS_ADMIN')) and removed =False limit 1) where owner_id is null;

-- in case there is no admin assign to any user that is not removed (shouldn't happen at all)
update project_table set owner_id = (select id from user_table  where removed =False limit 1) where owner_id is null;
