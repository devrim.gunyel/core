update element_table set font_size = 12 where font_size is null;
alter table element_table alter column font_size set not null;
