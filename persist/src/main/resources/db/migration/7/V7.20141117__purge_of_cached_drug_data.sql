--modification of MiriamData
alter table miriam_data_table add column reltype integer;
update miriam_data_table set reltype=0 where relationtype='bqmodel:is';
update miriam_data_table set reltype=1 where relationtype='bqmodel:isDescribedBy';
update miriam_data_table set reltype=2 where relationtype='bqbiol:is';
update miriam_data_table set reltype=3 where relationtype='bqbiol:hasPart';
update miriam_data_table set reltype=4 where relationtype='bqbiol:isPartOf';
update miriam_data_table set reltype=5 where relationtype='bqbiol:isVersionOf';
update miriam_data_table set reltype=6 where relationtype='bqbiol:hasVersion';
update miriam_data_table set reltype=7 where relationtype='bqbiol:isHomologTo';
update miriam_data_table set reltype=8 where relationtype='bqbiol:isDescribedBy';
update miriam_data_table set reltype=9 where relationtype='bqbiol:isEncodedBy';
update miriam_data_table set reltype=10 where relationtype='bqbiol:encodes';
update miriam_data_table set reltype=11 where relationtype='bqbiol:occures';
alter table miriam_data_table drop column relationtype;
alter table miriam_data_table rename column reltype to relationtype;

--clearing drugs (modification in Drug structure)
delete from cachequery where (type =2 or type =4) and query like 'drug:%';
delete from cachequery where (type =2 or type =4) and query like 'name:%';

alter table reaction_table drop column type;
