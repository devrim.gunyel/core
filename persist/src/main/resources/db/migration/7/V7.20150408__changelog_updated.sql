-- miriam parsing bug
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (459, 'Problem with some miriam entries in RDF format', 'BUG_FIX', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
