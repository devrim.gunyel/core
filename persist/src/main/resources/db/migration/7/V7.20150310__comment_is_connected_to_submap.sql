alter table feedback add column submodel_iddb integer;
alter table feedback add constraint fk_feedback_submodel foreign key (submodel_iddb) references model_table (iddb) match simple;
update feedback set submodel_iddb=model_iddb ;