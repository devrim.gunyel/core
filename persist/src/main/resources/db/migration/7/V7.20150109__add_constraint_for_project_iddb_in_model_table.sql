alter table model_table alter column project_iddb drop not null;
alter table submodel_connection_table drop column type;
alter table submodel_connection_table add column type integer;
update submodel_connection_table set type = 0;
alter table submodel_connection_table alter column type set not null;
