CREATE SEQUENCE cache_type_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10
  CACHE 1;
  
 CREATE TABLE cache_type
(
  iddb integer NOT NULL DEFAULT nextval('cache_type_iddb_seq'::regclass),
  validity integer,
  classname varchar,
  CONSTRAINT cache_type_pkey PRIMARY KEY (iddb)
);

INSERT INTO cache_type VALUES (1, 28, 'lcsb.mapviewer.reactome.utils.ReactomeConnector');
INSERT INTO cache_type VALUES (2, 28, 'lcsb.mapviewer.annotation.services.AnnotationRestService');
INSERT INTO cache_type VALUES (3, 28, 'lcsb.mapviewer.annotation.services.ChEMBLParser');
INSERT INTO cache_type VALUES (4, 365, 'lcsb.mapviewer.annotation.services.PubmedParser');
INSERT INTO cache_type VALUES (5, 28, 'lcsb.mapviewer.annotation.services.DrugbankHTMLParser');
INSERT INTO cache_type VALUES (6, 28, 'lcsb.mapviewer.annotation.services.IdConverter');
INSERT INTO cache_type VALUES (7, -1, 'lcsb.mapviewer.annotation.cache.MockCacheInterface');
INSERT INTO cache_type VALUES (8, 28, 'lcsb.mapviewer.annotation.services.ChebiBackend');
INSERT INTO cache_type VALUES (9, 28, 'lcsb.mapviewer.annotation.services.GoBackend');

