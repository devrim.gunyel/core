--modification of tables for recon annotations
--elements
alter table species_table add column abbreviation character varying(255);
alter table species_table add column formula character varying(255);

--reactions
alter table reaction_table add column symbol character varying(255);
alter table reaction_table add column abbreviation character varying(255);
alter table reaction_table add column formula character varying(255);
alter table reaction_table add column subsystem character varying(255);
alter table reaction_table add column geneproteinreaction character varying(255);
alter table reaction_table add column mechanicalconfidencescore integer;
alter table reaction_table add column upperbound double precision;
alter table reaction_table add column lowerbound double precision;

--and synonyms in the reaction table
CREATE TABLE reaction_synonyms
(
  iddb integer NOT NULL,
  idx integer NOT NULL,
  synonym character varying(255),
  CONSTRAINT reaction_synonyms_pkey PRIMARY KEY (iddb, idx),
  CONSTRAINT reaction_synonyms_fkey FOREIGN KEY (iddb)
      REFERENCES reaction_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

