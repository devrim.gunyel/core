-- galaxy connector
insert into external_user (name, surname, email) values ('Venkata', 'Satagopam', 'venkata.satagopam@uni.lu');
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (453, 'Galaxy connector', 'FUNCTIONALITY', null, (select iddb from external_user where email ='venkata.satagopam@uni.lu'));

-- miriam type information
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (454, 'Miriam types report', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
