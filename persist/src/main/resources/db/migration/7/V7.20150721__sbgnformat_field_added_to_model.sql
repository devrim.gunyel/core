-- information about display format
alter table model_table
add column sbgnformat boolean default false;
update model_table
set sbgnformat = false;
alter table model_table
alter column sbgnformat set not null;

insert into framework_version(version,"time", svnversion) values (6, timestamp'2015-07-21',510);
update framework_version set svnversion=514 where version = 6;