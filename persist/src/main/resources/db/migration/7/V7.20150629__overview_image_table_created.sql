-- tables that contain information about images with the overview of the map
DROP table IF EXISTS overview_link_table;
DROP sequence IF EXISTS overview_link_iddb_seq;
DROP table IF EXISTS overview_image_table;
DROP sequence IF EXISTS overview_image_iddb_seq;

CREATE SEQUENCE overview_image_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE overview_image_table
(
  iddb integer NOT NULL DEFAULT nextval('overview_image_iddb_seq'::regclass),
  model_iddb integer,
  filename character varying(255),
  width integer,
  height integer,

  CONSTRAINT overview_image_pkey PRIMARY KEY (iddb),
  CONSTRAINT overview_image_model_fk FOREIGN KEY (model_iddb)
      REFERENCES model_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE overview_link_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE overview_link_table
(
  iddb integer NOT NULL DEFAULT nextval('overview_link_iddb_seq'::regclass),
  overviewimage_iddb integer,
  polygon character varying,
  zoomlevel integer,
  ycoord integer,
  xcoord integer,
  link_type character varying(31) NOT NULL,
  linkedoverviewimage_iddb integer,
  linkedmodel_iddb integer,

  CONSTRAINT overview_link_pkey PRIMARY KEY (iddb),
  CONSTRAINT overview_link_overview_image_fk FOREIGN KEY (overviewimage_iddb)
      REFERENCES overview_image_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT overview_link_linked_overview_image_fk FOREIGN KEY (linkedoverviewimage_iddb)
      REFERENCES overview_image_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT overview_image_model_fk FOREIGN KEY (linkedmodel_iddb)
      REFERENCES model_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
  
)
WITH (
  OIDS=FALSE
);


-- map project directory (where all data will be stored on the server side)
alter table project_table add column directory character varying;