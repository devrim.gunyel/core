--refactorization of miriam data
alter table miriam_data_table add column datatype varchar(255);
update miriam_data_table set datatype = 'CAS' where datatypeuri='urn:miriam:cas';
update miriam_data_table set datatype = 'CHEBI' where datatypeuri='urn:miriam:obo.chebi';
update miriam_data_table set datatype = 'CHEBI' where datatypeuri='urn:miriam:chebi';
update miriam_data_table set datatype = 'CHEMBL_COMPOUND' where datatypeuri='urn:miriam:chembl.compound';
update miriam_data_table set datatype = 'CHEMBL_TARGET' where datatypeuri='urn:miriam:chembl.target';
update miriam_data_table set datatype = 'EC' where datatypeuri='urn:miriam:ec-code';
update miriam_data_table set datatype = 'EC' where datatypeuri='urn:miriam:enzyme.nomenclature';
update miriam_data_table set datatype = 'ENSEMBL' where datatypeuri='urn:miriam:ensembl';
update miriam_data_table set datatype = 'ENTREZ' where datatypeuri='urn:miriam:ncbigene';
update miriam_data_table set datatype = 'ENTREZ' where datatypeuri='urn:miriam:entrez.gene';
update miriam_data_table set datatype = 'GO' where datatypeuri='urn:miriam:obo.go';
update miriam_data_table set datatype = 'GO' where datatypeuri='urn:miriam:go';
update miriam_data_table set datatype = 'HGNC' where datatypeuri='urn:miriam:hgnc';
update miriam_data_table set datatype = 'HGNC_SYMBOL' where datatypeuri='urn:miriam:hgnc.symbol';
update miriam_data_table set datatype = 'HMDB' where datatypeuri='urn:miriam:hmdb';
update miriam_data_table set datatype = 'INTERPRO' where datatypeuri='urn:miriam:interpro';
update miriam_data_table set datatype = 'KEGG_COMPOUND' where datatypeuri='urn:miriam:kegg.compound';
update miriam_data_table set datatype = 'KEGG_GENES' where datatypeuri='urn:miriam:kegg.genes:hsa';
update miriam_data_table set datatype = 'KEGG_GENES' where datatypeuri='urn:miriam:kegg.genes';
update miriam_data_table set datatype = 'KEGG_PATHWAY' where datatypeuri='urn:miriam:kegg.pathway';
update miriam_data_table set datatype = 'KEGG_REACTION' where datatypeuri='urn:miriam:kegg.reaction';
update miriam_data_table set datatype = 'MESH_2012' where datatypeuri='urn:miriam:mesh';
update miriam_data_table set datatype = 'MESH_2012' where datatypeuri='urn:miriam:mesh.2012';
update miriam_data_table set datatype = 'MGD' where datatypeuri='urn:miriam:mgd';
update miriam_data_table set datatype = 'PANTHER' where datatypeuri like 'urn:miriam:panther.family%';
update miriam_data_table set datatype = 'PANTHER' where datatypeuri='urn:miriam:panther';
update miriam_data_table set datatype = 'PHARM' where datatypeuri='urn:miriam:pharmgkb.pathways';
update miriam_data_table set datatype = 'PUBCHEM' where datatypeuri='urn:miriam:pubchem.compound';
update miriam_data_table set datatype = 'PUBCHEM_SUBSTANCE' where datatypeuri='urn:miriam:pubchem.substance';
update miriam_data_table set datatype = 'PUBMED' where datatypeuri='urn:miriam:pubmed';
update miriam_data_table set datatype = 'REACTOME' where datatypeuri='urn:miriam:reactome';
update miriam_data_table set datatype = 'REFSEQ' where datatypeuri='urn:miriam:refseq';
update miriam_data_table set datatype = 'UNIPROT' where datatypeuri='urn:miriam:uniprot';
update miriam_data_table set datatype = 'WIKIPATHWAYS' where datatypeuri='urn:miriam:wikipathways';
update miriam_data_table set datatype = 'WIKIPEDIA' where datatypeuri='urn:miriam:wikipedia.en';
update miriam_data_table set datatype = 'UNKNOWN' where datatypeuri='urn:miriam:bind';
alter table miriam_data_table drop column datatypeuri;
