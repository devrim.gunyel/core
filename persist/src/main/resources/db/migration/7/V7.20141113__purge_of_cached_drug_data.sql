-- structure of drug changed, so we need to remove cached values
delete from cachequery where (type =2 or type =4) and query like 'drug:%';
delete from cachequery where (type =2 or type =4) and query like 'name:%';
insert into cache_type(iddb, validity, classname) values (9, 365, 'lcsb.mapviewer.annotation.services.MiriamConnector');
