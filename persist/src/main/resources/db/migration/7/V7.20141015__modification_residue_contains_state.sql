-- fix state in protein modifications
alter table modification_residue_table ADD COLUMN state_int integer;

update  modification_residue_table set state_int = 0 where state='phosphorylated';
update  modification_residue_table set state_int = 1 where state='acetylated';
update  modification_residue_table set state_int = 2 where state='ubiquitinated';
update  modification_residue_table set state_int = 3 where state='methylated';
update  modification_residue_table set state_int = 4 where state='hydroxylated';
update  modification_residue_table set state_int = 5 where state='myristoylated';
update  modification_residue_table set state_int = 6 where state='sulfated';
update  modification_residue_table set state_int = 7 where state='prenylated';
update  modification_residue_table set state_int = 8 where state='glycosylated';
update  modification_residue_table set state_int = 9 where state='palmytoylated';
update  modification_residue_table set state_int = 10 where state='unknown';
update  modification_residue_table set state_int = 11 where state='protonated';
update  modification_residue_table set state_int = 12 where state='don''t care';

alter table modification_residue_table drop column state;
alter table modification_residue_table rename state_int to state;

-- fix state in rna regions
alter table rna_region_table ADD COLUMN state_int integer;

update  rna_region_table set state_int = 0 where state='phosphorylated';
update  rna_region_table set state_int = 1 where state='acetylated';
update  rna_region_table set state_int = 2 where state='ubiquitinated';
update  rna_region_table set state_int = 3 where state='methylated';
update  rna_region_table set state_int = 4 where state='hydroxylated';
update  rna_region_table set state_int = 5 where state='myristoylated';
update  rna_region_table set state_int = 6 where state='sulfated';
update  rna_region_table set state_int = 7 where state='prenylated';
update  rna_region_table set state_int = 8 where state='glycosylated';
update  rna_region_table set state_int = 9 where state='palmytoylated';
update  rna_region_table set state_int = 10 where state='unknown';
update  rna_region_table set state_int = 11 where state='protonated';
update  rna_region_table set state_int = 12 where state='don''t care';

alter table rna_region_table drop column state;
alter table rna_region_table rename state_int to state;

-- fix state in antisense rna regions
alter table antisense_rna_region_table ADD COLUMN state_int integer;

update  antisense_rna_region_table set state_int = 0 where state='phosphorylated';
update  antisense_rna_region_table set state_int = 1 where state='acetylated';
update  antisense_rna_region_table set state_int = 2 where state='ubiquitinated';
update  antisense_rna_region_table set state_int = 3 where state='methylated';
update  antisense_rna_region_table set state_int = 4 where state='hydroxylated';
update  antisense_rna_region_table set state_int = 5 where state='myristoylated';
update  antisense_rna_region_table set state_int = 6 where state='sulfated';
update  antisense_rna_region_table set state_int = 7 where state='prenylated';
update  antisense_rna_region_table set state_int = 8 where state='glycosylated';
update  antisense_rna_region_table set state_int = 9 where state='palmytoylated';
update  antisense_rna_region_table set state_int = 10 where state='unknown';
update  antisense_rna_region_table set state_int = 11 where state='protonated';
update  antisense_rna_region_table set state_int = 12 where state='don''t care';

alter table antisense_rna_region_table drop column state;
alter table antisense_rna_region_table rename state_int to state;

--change names in antisense rna regions identifiers
alter table antisense_rna_region_table rename idAntisenseRnaRegionDb to iddb;
alter table antisense_rna_region_table rename id to idantisensernaregion;

--change primary key in antisense rna region and drop unnecessary objects
alter table antisense_rna_region_table drop CONSTRAINT  antisense_rna_region_table_pkey;
alter table antisense_rna_region_table add CONSTRAINT antisense_rna_region_table_pkey PRIMARY KEY (iddb);

alter table antisense_rna_region_table alter column idrnaregiondb set default null;
alter table antisense_rna_region_table drop constraint antisense_rna_region_table_idantisensernaregiondb_key;
alter table antisense_rna_region_table drop constraint fkc75381701e595a8a;
alter table antisense_rna_region_table drop constraint fkc7538170a4c0d273;

CREATE SEQUENCE antisense_rna_region_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10
  CACHE 1;

select setval('antisense_rna_region_table_iddb_seq', nextval('antisense_rna_region_table_idrnaregiondb_seq'));

ALTER TABLE antisense_rna_region_table
ALTER COLUMN iddb set DEFAULT nextval('antisense_rna_region_table_iddb_seq'::regclass);


alter table antisense_rna_region_table drop column idrnaregiondb;
drop  sequence if exists antisense_rna_region_table_idrnaregiondb_seq;
