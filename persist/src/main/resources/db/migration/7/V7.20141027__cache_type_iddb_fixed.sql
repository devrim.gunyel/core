--fix cache
update cache_type set iddb = 0 where classname='lcsb.mapviewer.reactome.utils.ReactomeConnector';
update cache_type set iddb = 1 where classname='lcsb.mapviewer.annotation.services.AnnotationRestService';
update cache_type set iddb = 2 where classname='lcsb.mapviewer.annotation.services.ChEMBLParser';
update cache_type set iddb = 3 where classname='lcsb.mapviewer.annotation.services.PubmedParser';
update cache_type set iddb = 4 where classname='lcsb.mapviewer.annotation.services.DrugbankHTMLParser';
update cache_type set iddb = 5 where classname='lcsb.mapviewer.annotation.services.IdConverter';
update cache_type set iddb = 6 where classname='lcsb.mapviewer.annotation.cache.MockCacheInterface';
update cache_type set iddb = 7 where classname='lcsb.mapviewer.annotation.services.ChebiBackend';
update cache_type set iddb = 8 where classname='lcsb.mapviewer.annotation.services.GoBackend';
