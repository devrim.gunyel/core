ALTER TABLE model_table
   ADD COLUMN idmodel character varying;

ALTER TABLE model_table
   ADD COLUMN metaid character varying;
  
create table history_table (
	iddb serial,
	query varchar,
	map varchar,
	timestamp timestamp without time zone,
	type integer,
	ipaddress varchar
);