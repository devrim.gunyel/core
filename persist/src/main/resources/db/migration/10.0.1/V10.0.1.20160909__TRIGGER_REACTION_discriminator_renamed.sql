-- fix on issue #8
update reaction_table set reaction_type_db='REDUCED_TRIGGER_REACTION' where reaction_type_db = 'TRIGGER_REACTION';
update reaction_table set reaction_type_db='REDUCED_PHYSICAL_STIMULATION' where reaction_type_db = 'PHYSICAL_STIMULATION_REACTION';

