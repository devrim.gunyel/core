-- add z-index to all elements in the database in similar way as they are drawn right now
alter table element_table add column z integer;
alter table reaction_table add column z integer;

--add artificial element outside compartment, so the update queries will always return non-null values
insert into element_table (id, element_type_db, visibility_level, width, height) values(-1, 'GENERIC_PROTEIN', 0, 1,1);

with v_element_table as
(
 select id, row_number() over (order by width*height desc, id) as rn from element_table where not element_type_db like '%COMPARTMENT%' and not element_type_db like '%PATHWAY%' and compartment_id is null and idcomplexdb is null
) 
update element_table set z =  v_element_table.rn
from v_element_table
where element_table.id = v_element_table.id;

with v_reaction_table as
(
 select id, row_number() over (order by id) as rn from reaction_table
) 
update reaction_table set z =  v_reaction_table.rn + (select max(z) from element_table)
from v_reaction_table
where reaction_table.id = v_reaction_table.id;

with v_element_table as
(
 select id, row_number() over (order by width*height desc, id) + (select max(z) from reaction_table)  as rn from element_table where compartment_id is null and idcomplexdb is null and z is null
) 
update element_table set z =  v_element_table.rn
from v_element_table
where element_table.id = v_element_table.id;


DO $$ 
DECLARE 
begin

  for i in 1..10 loop
    with v_element_table as
    (
     select id, row_number() over (order by width*height desc, id) + (select max(z) from element_table)  as rn from element_table where z is null and compartment_id in (select id from element_table where z is not null) and idcomplexdb is null
    ) 
    update element_table set z =  v_element_table.rn
    from v_element_table
    where element_table.id = v_element_table.id;
  end loop;

end;
$$
;

DO $$ 
DECLARE 
begin

  for i in 1..10 loop
    with v_element_table as
    (
     select id, row_number() over (order by width*height desc, id) + (select max(z) from element_table)  as rn from element_table where z is null and idcomplexdb in (select id from element_table where z is not null)
    ) 
    update element_table set z =  v_element_table.rn
    from v_element_table
    where element_table.id = v_element_table.id;
  end loop;

end;
$$
;

-- just in case 10 loops were not enough
update element_table set z = (select max(z) from element_table) where z is null;

ALTER TABLE element_table ALTER COLUMN z SET NOT NULL;
ALTER TABLE reaction_table ALTER COLUMN z SET NOT NULL;

--remove artificial element added at the beginning of the script
delete from element_table where id = -1;
