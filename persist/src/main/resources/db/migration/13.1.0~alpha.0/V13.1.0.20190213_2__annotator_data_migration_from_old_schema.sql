insert into annotator_data_table(user_class_annotators_id, order_index, annotator_class_name) select class_annotator_id, idx , annotator_name from class_annotator_annotators_table;

update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.BiocompendiumAnnotator' where annotator_class_name='Biocompendium';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator' where annotator_class_name='BRENDA';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator' where annotator_class_name='CAZy';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator' where annotator_class_name='Chebi';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.EnsemblAnnotator' where annotator_class_name='Ensembl';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.EntrezAnnotator' where annotator_class_name='Entrez Gene';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.GoAnnotator' where annotator_class_name='Gene Ontology';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.HgncAnnotator' where annotator_class_name='HGNC';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.KeggAnnotator' where annotator_class_name='KEGG';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.PdbAnnotator' where annotator_class_name='Protein Data Bank';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator' where annotator_class_name='STITCH';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator' where annotator_class_name='STRING';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator' where annotator_class_name='TAIR';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator' where annotator_class_name='Uniprot';
update annotator_data_table set annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.ReconAnnotator' where annotator_class_name='Recon annotator';

delete from annotator_data_table where not annotator_class_name like 'lcsb.mapviewer.annotation.services.annotators%';

drop table class_annotator_annotators_table;