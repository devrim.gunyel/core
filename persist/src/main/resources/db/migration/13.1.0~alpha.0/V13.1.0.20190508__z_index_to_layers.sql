-- add z-index to all layer elements
alter table layer_rect_table add column z integer;
alter table layer_oval_table add column z integer;
alter table layer_text_table add column z integer;


with v_layer_text_table as
(
 select id, row_number() over (order by width*height desc, id) as rn from layer_text_table
) 
update layer_text_table set z =  v_layer_text_table.rn
from v_layer_text_table
where layer_text_table.id = v_layer_text_table.id;

with v_layer_oval_table as
(
 select id, row_number() over (order by width*height desc, id) as rn from layer_oval_table
) 
update layer_oval_table set z =  v_layer_oval_table.rn
from v_layer_oval_table
where layer_oval_table.id = v_layer_oval_table.id;

with v_layer_rect_table as
(
 select id, row_number() over (order by width*height desc, id) as rn from layer_rect_table
) 
update layer_rect_table set z =  v_layer_rect_table.rn
from v_layer_rect_table
where layer_rect_table.id = v_layer_rect_table.id;


ALTER TABLE layer_rect_table ALTER COLUMN z SET NOT NULL;
ALTER TABLE layer_oval_table ALTER COLUMN z SET NOT NULL;
ALTER TABLE layer_text_table ALTER COLUMN z SET NOT NULL;
