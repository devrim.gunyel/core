delete from annotator_parameter_table where annotator_data_id in (select id  from annotator_data_table where user_class_annotators_id is null);
delete from annotator_data_table where user_class_annotators_id is null;
alter table annotator_data_table alter user_class_annotators_id set not null;
