delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.compartment.Compartment');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.BioEntity');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Chemical');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.reaction.Reaction');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Element');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Species');
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Protein');

delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.compartment.Compartment';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.BioEntity';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Chemical';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.reaction.Reaction';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Element';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Species';
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.species.Protein';

