alter table project_table add column creation_date timestamp without time zone;
update project_table set creation_date = (select min(creation_date) from model_data_table where project_table.id= model_data_table.project_id);
alter table model_data_table drop column creation_date;


