CREATE SEQUENCE glyph_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE glyph_table
(
  id integer NOT NULL DEFAULT nextval('glyph_sequence'::regclass),
  file_entry_id integer NOT NULL,
  project_id integer NOT NULL,

  CONSTRAINT glyph_table_pk PRIMARY KEY (id),
  CONSTRAINT glyph_table_file_entry_fk FOREIGN KEY (file_entry_id)
      REFERENCES file_entry_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT glyph_table_project FOREIGN KEY (project_id)
      REFERENCES project_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

alter table element_table add column glyph_id integer;
alter table element_table add constraint element_table_glyph_fk FOREIGN KEY (glyph_id)
      REFERENCES glyph_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

