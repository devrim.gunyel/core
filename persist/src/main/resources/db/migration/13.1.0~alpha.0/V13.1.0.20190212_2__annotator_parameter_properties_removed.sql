alter table annotator_parameter_table drop column annotator_class_name;
alter table annotator_parameter_table alter type drop not null;
alter table annotator_parameter_table alter value drop not null;