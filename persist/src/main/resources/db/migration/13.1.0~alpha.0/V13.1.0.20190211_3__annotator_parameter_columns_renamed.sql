alter table annotator_parameter_table rename parameter_type to type;
alter table annotator_parameter_table rename param_value to value;
alter table annotator_parameter_table add column type_distinguisher character varying(255);

update annotator_parameter_table set type_distinguisher = 'CONFIG_PARAMETER';
ALTER TABLE annotator_parameter_table ALTER COLUMN type_distinguisher SET NOT NULL;

