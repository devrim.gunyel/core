insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'Hiding left panel', 'BUG_FIX', null, 
  (select iddb from external_user where email ='alberto.noronha@uni.lu'));
  
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'Coordinates in url', 'BUG_FIX', null, 
  (select iddb from external_user where email ='alberto.noronha@uni.lu'));

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'Unexpected errors when uploading layouts', 'BUG_FIX', null, 
  (select iddb from external_user where email ='alberto.noronha@uni.lu'));

delete from cachequery where type = (select iddb from cache_type where classname = 'lcsb.mapviewer.annotation.services.DrugbankHTMLParser') and query like 'drug:%';

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'Corrupted drug names', 'BUG_FIX', null, 
  (select iddb from external_user where email ='alberto.noronha@uni.lu'));
