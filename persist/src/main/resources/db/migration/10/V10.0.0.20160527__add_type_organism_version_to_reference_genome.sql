--obligatory fields for reference genome table
alter table reference_genome alter column "type" set not null;
alter table reference_genome alter column organism_iddb set not null;
alter table reference_genome alter column "version" set not null;


--gene mapping files table for reference genome table
drop table if exists public.reference_genome_gene_mapping;
drop SEQUENCE if exists public.reference_genome_gene_mapping_iddb_seq;
CREATE SEQUENCE public.reference_genome_gene_mapping_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE public.reference_genome_gene_mapping
(
  iddb integer NOT NULL DEFAULT nextval('reference_genome_gene_mapping_iddb_seq'::regclass),
  referencegenome_iddb integer NOT NULL,
  name varchar(255) not null,
  sourceurl text not null,
  downloadprogress double precision,
  CONSTRAINT reference_genome_gene_mapping_pkey PRIMARY KEY (iddb),
  CONSTRAINT reference_genome_gene_mapping_reference_genome_fk FOREIGN KEY (referencegenome_iddb)
      REFERENCES public.reference_genome (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT reference_genome_gene_mapping_unique_name UNIQUE (referencegenome_iddb, name)
      

)
WITH (
  OIDS=FALSE
);
