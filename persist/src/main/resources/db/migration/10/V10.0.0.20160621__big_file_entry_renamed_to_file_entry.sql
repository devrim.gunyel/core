-- reorganizing files in the system
alter table big_file_entry rename to file_entry;

alter table file_entry add column filecontent bytea;
alter table file_entry add column file_type_db varchar(255);
alter table file_entry add column originalfilename varchar(255);

-- all files up to this point were big files
update file_entry set file_type_db = 'BIG_FILE_ENTRY';

--and now we move content of files from layout file to file_entry table
--tmp column
alter table file_entry add column layout_id_tmp integer;
--move content content
insert into file_entry (filecontent,file_type_db,originalfilename,layout_id_tmp) select inputdata, 'UPLOADED_FILE_ENTRY', 'layout_'||iddb||'.txt',iddb from layout where inputdata is not null;

--add foreign key
alter table layout add column file_entry_iddb integer;
ALTER TABLE layout ADD CONSTRAINT layout_file_constraint FOREIGN KEY (file_entry_iddb)
      REFERENCES public.file_entry (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--put data to foreign key      
update layout l set file_entry_iddb = (select iddb from file_entry ef where ef.layout_id_tmp = l.iddb) where l.inputdata is not null;

--remove unused columns 
alter table file_entry drop column layout_id_tmp;
alter table layout drop column inputdata;
